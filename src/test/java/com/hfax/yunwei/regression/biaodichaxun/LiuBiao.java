package com.hfax.yunwei.regression.biaodichaxun;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.yunwei.base.BaseTest;
/**
 * 
 * @author changwj
 * @Description 标的查询--流标
 */
public class LiuBiao extends BaseTest{
	@Test
		public void 流标(){
			this.loadPage();
			this.login();
		    this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[5]/ul/li[6]/div/span[4]")).click();
		    //等待页面加载
		    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[1]/span[1]")));
		    String text = this.driver.switchTo().frame(0).findElement(By.xpath("html/body/div[1]/div[2]/div[1]/div[1]")).getText();
		    System.out.println(text);
		    assertTrue("加载标的页面出错",text.equals("查询列表"));
	}
	@Test
	public void 流标成功(){
		
	}
}
