package com.hfax.yunwei.regression.biaodichaxun;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.base.Util;
import com.hfax.yunwei.base.BaseTest;
/**
 * 
 * @author changwj
 * @Description 资产录入
 */
public class 资产录入 extends BaseTest {

	@Test
	public void 新增标的(){
		this.loadPage();
		this.login();
		
		
		//点击项目维护
		this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[5]/ul/li[1]/div/span[4]")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		// 等待页面加载
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[1]/span[1]")));
		String text = this.driver.findElement(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[1]/span[1]")).getText();
		assertTrue("项目维护没有加载出来", text.equals("项目维护"));
		List<WebElement> findElements = this.driver.findElements(By.xpath("html/body/div[1]/div"));
		assertTrue("项目维护不能为空", findElements.size() > 0);
		String text2 = this.driver.switchTo().frame(0).findElement(By.xpath("html/body/div[1]/div[2]/div[1]/div[1]")).getText();
		assertTrue("加载查询列表窗口失败", text2.equals("查询列表"));

		//点击增加
		this.driver.findElement(By.xpath("html/body/div[1]/div[2]/div[2]/div/div/div[1]/table/tbody/tr/td[2]/a/span/span")).click();;
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='add_form']/h2[1]")));
		String text3 = this.driver.findElement(By.xpath(".//*[@id='add_form']/h2[1]")).getText();
		assertTrue("新增项目信息页面加载失败",text3.equals("项目基础信息"));
		
		// 新增项目信息  下拉菜单
		this.driver.findElement(By.xpath(".//*[@id='add_form']/table[1]/tbody/tr[1]/td[2]/span/input[1]")).click();
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("tooltip-content")));
		

		assertTrue("提示未出现",this.driver.findElement(By.className("tooltip-content")).getText().equals("该输入项为必输项"));
		
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='add_form']/table[1]/tbody/tr[1]/td[2]/span/span/span")));
		this.driver.findElement(By.xpath(".//*[@id='add_form']/table[1]/tbody/tr[1]/td[2]/span/span/span")).click();
		
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.className("combo-panel")));
		List<WebElement> findElements2 = this.driver.findElements(By.xpath("html/body/div[3]/div/div"));
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		findElements2.get(0).click();
		//项目名称
		this.driver.findElement(By.id("project_name")).sendKeys(this.projectName);
		//交易模式
		this.driver.findElement(By.xpath(".//*[@id='add_form']/table[1]/tbody/tr[2]/td[2]/span/input[1]")).click();
		 
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("tooltip-content")));
		
	
		assertTrue("提示未出现",this.driver.findElement(By.className("tooltip-content")).getText().equals("该输入项为必输项"));
		
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='add_form']/table[1]/tbody/tr[2]/td[2]/span/span/span")));
		this.driver.findElement(By.xpath(".//*[@id='add_form']/table[1]/tbody/tr[2]/td[2]/span/span/span")).click();
		
		List<WebElement> findElements3 = this.driver.findElements(By.xpath("html/body/div[4]/div/div"));
	
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		findElements3.get(0).click();
		//资产总额
		this.driver.findElement(By.id("loan_amout")).click();
		
		assertTrue("提示未出现",this.driver.findElement(By.className("tooltip-content")).getText().equals("该输入项为必输项"));
		
		this.driver.findElement(By.id("loan_amout")).sendKeys("10000");
		//资产成立日
		this.driver.findElement(By.xpath(".//*[@id='add_form']/table[1]/tbody/tr[2]/td[6]/span/input[1]")).click();
		
		assertTrue("提示未出现",this.driver.findElement(By.className("tooltip-content")).getText().equals("该输入项为必输项"));
		
		//资产成立日
		driver.findElement(By.xpath("/html/body/div[15]/div[2]/div/form/table[1]/tbody/tr[2]/td[6]/span/span/span")).click();
		String assignTime = Util.assignTime(-3, "yyyy,M,d");
		driver.findElement(By.xpath("//td[@abbr='"+assignTime+"']")).click();
		
		//资产到期日
		driver.findElement(By.cssSelector("#add_form > table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2) > span:nth-child(2) > span:nth-child(2) > span:nth-child(1)")).click();
		String assignTime2 = Util.assignTime(7, "yyyy,M,d");
		driver.findElement(By.xpath("/html/body/div[11]//child::node()[@abbr='"+assignTime2+"']")).click();
		
		//资产期限
		driver.findElement(By.id("loan_period")).sendKeys("3");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		//产品利率
		driver.findElement(By.id("year_intr")).sendKeys("13");
		
		//是否溢价
		driver.findElement(By.cssSelector("#add_form > table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2) > span:nth-child(2) > span:nth-child(2) > span:nth-child(1)")).click();
		driver.findElement(By.cssSelector("div.panel:nth-child(6) > div:nth-child(1) > div:nth-child(2)")).click();
		
		//还款方式
		driver.findElement(By.cssSelector("#add_form > table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(6) > span:nth-child(2) > span:nth-child(2) > span:nth-child(1)")).click();
		driver.findElement(By.cssSelector("div.panel:nth-child(7) > div:nth-child(1) > div:nth-child(1)")).click();
		
		//宽限天数
		driver.findElement(By.id("grace_period")).sendKeys("3");
		
		//到账日
		driver.findElement(By.cssSelector("#add_form > table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(4) > span:nth-child(2) > span:nth-child(2) > span:nth-child(1)")).click();
		String assignTime3 = Util.assignTime(7, "yyyy,M,d");
		driver.findElement(By.xpath("/html/body/div[12]//child::node()[@abbr='"+assignTime3+"']")).click();
		
		//投资标的类型
		driver.findElement(By.id("subject_type")).click();
		
		//项目概况标题1
		driver.findElement(By.id("desc_title1")).sendKeys(this.projectName);
		
		//项目概况内容1
		driver.findElement(By.id("desc_cont1")).sendKeys("项目概况内容1项目概况内容1");
		
		//产品投向情况介绍
		driver.findElement(By.id("prod_to_dc")).sendKeys("1111111");
		
		//保障类型
		driver.findElement(By.cssSelector("#add_form > table:nth-child(9) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > span:nth-child(2) > span:nth-child(2) > span:nth-child(1)")).click();
		driver.findElement(By.cssSelector("div.panel:nth-child(8) > div:nth-child(1) > div:nth-child(1)")).click();
		
		//托管银行
		driver.findElement(By.cssSelector("#add_form > table:nth-child(11) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > span:nth-child(2) > span:nth-child(2) > span:nth-child(1)")).click();
		driver.findElement(By.cssSelector("div.panel:nth-child(9) > div:nth-child(1) > div:nth-child(1)")).click();
		
		//银行账号
		driver.findElement(By.id("bank_no")).sendKeys("1231231231");
		
		//放款方式选择
		driver.findElement(By.cssSelector("#add_form > table:nth-child(11) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > span:nth-child(2) > span:nth-child(2) > span:nth-child(1)")).click();
		driver.findElement(By.cssSelector("div.panel:nth-child(10) > div:nth-child(1) > div:nth-child(1)")).click();
		
		//开户银行支行
		driver.findElement(By.id("open_bank")).sendKeys("12312312");
		
		
		//借款人
		driver.findElement(By.cssSelector("#showMemberList > span:nth-child(1) > span:nth-child(1)")).click();
		driver.findElement(By.cssSelector("#datagrid-row-r2-2-2 > td:nth-child(1) > div:nth-child(1) > input:nth-child(1)")).click();
		driver.findElement(By.cssSelector("#selectMember > span:nth-child(1) > span:nth-child(1)")).click();
		
		//保存
		driver.findElement(By.cssSelector("#save > span:nth-child(1) > span:nth-child(1)")).click();
		
		//确定
		driver.findElement(By.cssSelector(".messager-button > a:nth-child(1) > span:nth-child(1) > span:nth-child(1)")).click();
		
		//提交审核
		
	}
	
}
