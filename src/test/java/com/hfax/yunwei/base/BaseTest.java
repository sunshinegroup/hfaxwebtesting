package com.hfax.yunwei.base;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseTest {
	protected WebDriver driver; 
	protected FluentWait<WebDriver> wait;
	protected String hosturl;
	protected String uname; // Account username
	protected String passWord; // Account password
	protected String projectName;
	protected String amount;
	 protected boolean shouldExitOnTearDown;
	 
	 
	 
	@Before
	public void setup() throws Exception {
		Properties prop = new Properties();
		ClassLoader loader = BaseTest.class.getClassLoader();
		InputStream inputStream = loader.getResourceAsStream("webDriver.properties");
		BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream));
		prop.load(bf);

		this.hosturl = prop.getProperty("hosturl");
		this.uname = prop.getProperty("uname");
		this.passWord = prop.getProperty("passWord");
		this.projectName=prop.getProperty("projectName");
		this.amount=prop.getProperty("amount");
	    this.shouldExitOnTearDown = true;

		FirefoxProfile prof = new FirefoxProfile();
		prof.setPreference("browser.startup.homepage_override.mstone", "ignore");
		prof.setPreference("startup.homepage_welcome_url.additional", "about:blank");
		this.driver = new FirefoxDriver(prof);
		this.wait = new WebDriverWait(driver, 6000).withTimeout(5, TimeUnit.SECONDS).pollingEvery(100, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);
		//最大化浏览器
		driver.manage().window().maximize();
				
	}

	@After
	public void tearDown() {
        if (this.shouldExitOnTearDown) {
           // this.driver.quit();
        }
	}
	
	/**
	 * 读取主页
	 */
	protected void loadPage() {
		this.loadPage(this.hosturl);
	}

	/**
	 * 读取主页
	 */
	protected void loadPage(String hosturl) {
		this.driver.get(hosturl);
		assertTrue("读取主页失败",this.driver.getTitle().equals("登陆"));
	}

	/**
	 * 登录 
	 */
	protected void login() {
		this.driver.findElement(By.id("usercd")).sendKeys(this.uname);
		this.driver.findElement(By.id("password")).sendKeys(this.passWord);
		this.driver.findElement(By.id("dl")).click();
		
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[1]/div/div/div")));
		String text = this.driver.findElement(By.xpath("html/body/div[1]/div/div/div")).getText();
        assertTrue("登录失败",text.equals("运营管理平台"));
       
	}

	protected WebElement findDataGridTable() {
		String tableXPath = "//div[contains(@class,'layout-split-center')]//div[@class='panel datagrid']//div[@class='datagrid-view2']//div[@class='datagrid-body']//table";
		
		this.wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(tableXPath + "//tr")));
		return this.driver.findElement(By.xpath(tableXPath));
	}
	//确定查询列表的哪一行哪一列
	protected WebElement findValueAtIndexRow(int row, int column) {
		WebElement tableElement = this.findDataGridTable();
		List<WebElement> trs = tableElement.findElements(By.tagName("tr"));
		List<WebElement> tds = trs.get(row).findElements(By.tagName("td"));
		return tds.get(column);
	}
}
