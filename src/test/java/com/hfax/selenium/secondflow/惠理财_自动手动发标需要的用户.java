package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxywyAfterChongZhiTMS;
import SecondStage.Hfax.TestHuiSoXianUserAfterRechargeTMS;
import SecondStage.Hfax.Page.HuiUserRegister;
/**
 *  
 * @Description 寿险用户注册并打上寿险业务员标签的流程
 * Created by songxq on 12/07/16.
 * 
 * 
 */	
@RunWith(Suite.class)
@SuiteClasses({
	HuiUserRegister.class,
	TestHuiSoXianUserAfterRechargeTMS.class
})
public class 惠理财_自动手动发标需要的用户 {

}
