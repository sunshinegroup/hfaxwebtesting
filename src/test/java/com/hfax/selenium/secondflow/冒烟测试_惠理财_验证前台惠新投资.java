package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestAfterInvestHuiLiCaiNewHand;
import SecondStage.Hfax.TestHuiLiCaiManualTicketsUserBindingTickets;
import SecondStage.Hfax.TestInvestHuiLiCaiAboutManualTickets;
import SecondStage.Hfax.TestInvestHuiLiCaiNewHand;
/**
 *  
 * @Description  冒烟测试_惠理财_验证前台惠新投资
 * Created by songxq on 01/18/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	TestHuiLiCaiManualTicketsUserBindingTickets.class,
	TestInvestHuiLiCaiAboutManualTickets.class,
	TestInvestHuiLiCaiNewHand.class,
	TestAfterInvestHuiLiCaiNewHand.class
})
public class 冒烟测试_惠理财_验证前台惠新投资 {

}
