package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.ticket.卡券管理_充值_代金;
import SecondStage.ticket.卡券管理_充值_加息;
import SecondStage.ticket.卡券管理_充值_返现;
import SecondStage.ticket.卡券管理_寿险_返现;
import SecondStage.ticket.卡券管理_寿险续期_返现;
import SecondStage.ticket.卡券管理_投资_代金;
import SecondStage.ticket.卡券管理_投资_加息;
import SecondStage.ticket.卡券管理_投资_返现;
import SecondStage.ticket.卡券管理_投资成功_代金;
import SecondStage.ticket.卡券管理_投资成功_加息;
import SecondStage.ticket.卡券管理_注册_代金;
import SecondStage.ticket.卡券管理_注册_代金_12元;
import SecondStage.ticket.卡券管理_注册_代金_13元;
import SecondStage.ticket.卡券管理_注册_代金_14元;
import SecondStage.ticket.卡券管理_注册_加息;
import SecondStage.ticket.卡券管理_绑卡_代金;
import SecondStage.ticket.卡券管理_绑卡_加息;
import SecondStage.ticket.卡券管理_绑卡_返现;
import SecondStage.ticket.卡券管理_被邀请人认证_代金;
import SecondStage.ticket.卡券管理_被邀请人认证_加息;
import SecondStage.ticket.卡券管理_被邀请人认证_返现;
import SecondStage.ticket.卡券管理_认证_代金;
import SecondStage.ticket.卡券管理_认证_加息;
import SecondStage.ticket.卡券管理_认证_返现;
import SecondStage.ticket.卡券管理_邀请人被邀投资_代金;
import SecondStage.ticket.卡券管理_邀请人被邀投资_加息;
import SecondStage.ticket.卡券管理_邀请人被邀投资_返现;
import SecondStage.ticket.卡券管理_邀请人被邀认证_代金;
import SecondStage.ticket.卡券管理_邀请人被邀认证_加息;
import SecondStage.ticket.卡券管理_邀请人被邀认证_返现;

/**
 *  
 * @Description 惠理财卡券放
 * 
 *  
 * Created by songxq on 12/26/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	卡券管理_绑卡_代金.class,
	卡券管理_绑卡_返现.class,
	卡券管理_绑卡_加息.class,
	卡券管理_被邀请人认证_代金.class,
	卡券管理_被邀请人认证_返现.class,
	卡券管理_被邀请人认证_加息.class,
	卡券管理_充值_代金.class,
	卡券管理_充值_返现.class,
	卡券管理_充值_加息.class,
	卡券管理_认证_代金.class,
	卡券管理_认证_返现.class,
	卡券管理_认证_加息.class,
	卡券管理_寿险_返现.class,
	卡券管理_寿险续期_返现.class,
	卡券管理_投资_代金.class,
	卡券管理_投资_返现.class,
	卡券管理_投资_加息.class,
	卡券管理_邀请人被邀认证_代金.class,
	卡券管理_邀请人被邀认证_返现.class,
	卡券管理_邀请人被邀认证_加息.class,
	卡券管理_邀请人被邀投资_代金.class,
	卡券管理_邀请人被邀投资_返现.class,
	卡券管理_邀请人被邀投资_加息.class,
	卡券管理_注册_代金.class,
	卡券管理_注册_加息.class,
	卡券管理_注册_代金_12元.class,
	卡券管理_注册_代金_13元.class,
	卡券管理_注册_代金_14元.class,
	卡券管理_投资成功_代金.class,
	卡券管理_投资成功_加息.class,
})
public class 惠理财自动卡券发送 {

}
