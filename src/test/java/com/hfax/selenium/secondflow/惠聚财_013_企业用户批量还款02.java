package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资_惠聚财_投资惠聚财的产品全投;
import com.hfax.selenium.regression.console.惠聚财_募集成功;
import com.hfax.selenium.regression.console.惠聚财_综合管理系统;


import SecondStage.Hfax.惠聚财批量还款中个人账户金额;
import SecondStage.console_惠聚财.惠聚财_企业还款产品上架;
import SecondStage.console_惠聚财.惠聚财_企业还款产品审核;
import SecondStage.console_惠聚财.惠聚财_企业还款产品录入;
import SecondStage.console_惠聚财.惠聚财_企业还款募集成功;
/**
 *  
 * @Description 企业用户批量还款02
 * Created by songxq on 09/20/16.
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	//执行yue_insert.sh
		惠聚财_综合管理系统.class,
		惠聚财_企业还款产品录入.class,
		惠聚财_企业还款产品审核.class,
		惠聚财_企业还款产品上架.class,
		我的投资_惠聚财_投资惠聚财的产品全投.class,
		惠聚财_企业还款募集成功.class,
		惠聚财批量还款中个人账户金额.class,
})
public class 惠聚财_013_企业用户批量还款02 {

}
