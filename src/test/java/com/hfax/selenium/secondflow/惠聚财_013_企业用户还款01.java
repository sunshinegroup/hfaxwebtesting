package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资_惠聚财_投资惠聚财的产品全投;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠聚财_搜索已满标的产品;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠聚财_搜索还款中的产品;
import com.hfax.selenium.regression.console.惠聚财_产品上架;
import com.hfax.selenium.regression.console.惠聚财_产品审核;
import com.hfax.selenium.regression.console.惠聚财_产品录入;
import com.hfax.selenium.regression.console.惠聚财_募集成功;
import SecondStage.Hfax.企业还款前个人用户账号金额;
import SecondStage.Hfax.惠聚财还款中个人用户账号余额;
/**
 *  
 * @Description 
 * 企业用户还款01(此用例是企业单个标的还款的流程)
 * Created by songxq on 09/19/16.
 * 1.ftp上传还款试算表（以插库sql实现）
 * 2.运营管理平台，执行三个任务，产生还款对账表
 * 3.企业户登录后还款，选择还款标的，查看标的还款金额正确，输入交易密码，还款完成
 * 4.企业户可批量还款，选择多标同时还款，注意企业户可用余额够用
 * 5.还款完成，个人用户登录后，查看可用余额增加，投资总额减少，标的状态显示已还完
 * 6.查看短信及站内信显示正确（仅站内信）
 * (包括惠聚财_008 已付息，所有协议)
 * (包括惠聚财_015 还款成功---还款完成后，站内信立即发送)
 */
@RunWith(Suite.class)
@SuiteClasses({
	//cleanup_yuReSubject.sh 预热
	//cleanup_renZuSubject.sh 人组
	//cleanup_PiLiangYueSubject.sh 批量
	//cleanup_yueSubject.sh 惠聚财
	//cleanup_chuanYueSubject.sh 串标
	//cleanup_yueRules.sh 串标规则
	//cleanup_huijucaiTicket.sh 代金券和加息券清库
	//cleanup_useramount.sh 清库个人账户金额
	//cleanup_business_useramount.sh 清库企业用户账户金额
	//cleanup_sendmessage.sh 清除个人站内信
	//cleanup_businessuser_sendmessage.sh 清除企业用户站内信
	//yue_delete.bat
	惠聚财_产品录入.class,
	惠聚财_产品审核.class,
	惠聚财_产品上架.class,
	企业还款前个人用户账号金额.class,
	我的投资_惠聚财_投资惠聚财的产品全投.class,
	我的投资_惠聚财_搜索已满标的产品.class,
	惠聚财_募集成功.class,
	我的投资_惠聚财_搜索还款中的产品.class,
	惠聚财还款中个人用户账号余额.class
})
public class 惠聚财_013_企业用户还款01 {

}
