package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import SecondStage.Hfax.TesthlcsxXQywyAfterChongZhiTMS;
/**
 *  
 * @Description 惠理财用户打寿险【续期】业务员标签的验证
 * Created by songxq on 12/19/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	TesthlcsxXQywyAfterChongZhiTMS.class
})
public class 惠理财用户打寿险续期业务员的标签 {

}
