package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestManualTicketsUserBindingTickets;
import SecondStage.Hfax.TestManualTicketsUserCash;
import SecondStage.Hfax.TestManualTicketsUserReturnCash;
/**
 *  
 * @Description 冒烟测试_惠理财_验证前台手动发券 
 * Created by songxq on 01/18/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	TestManualTicketsUserCash.class,
	TestManualTicketsUserReturnCash.class,
	TestManualTicketsUserBindingTickets.class,
})
public class 冒烟测试_惠理财_验证前台手动发券 {

}
