package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.console.惠聚财_产品上架;
import com.hfax.selenium.regression.console.惠聚财_产品审核;
import com.hfax.selenium.regression.console.惠聚财_产品录入;
import SecondStage.Hfax.惠聚财追加购买;
/**
 *  
 * @Description 
 * 惠聚财追加购买
 * Created by songxq on 09/18/16.
 * 1.标的，输入金额，完成购买
 * 2.二次进入追加页面，输入金额，完成购买
 * 3.购买完成，查看账户余额显示正确
 * (包括惠聚财_08中的募集中，协议验证)
 */
@RunWith(Suite.class)
@SuiteClasses({
	//cleanup_yuReSubject.sh 预热
	//cleanup_renZuSubject.sh 人组
	//cleanup_PiLiangYueSubject.sh 批量
	//cleanup_yueSubject.sh 惠聚财
	//cleanup_chuanYueSubject.sh 串标
	//cleanup_yueRules.sh 串标规则
	//cleanup_yueSubject.sh   标的清库
	//cleanup_huijucaiTicket.sh 代金券和加息券清库
	//cleanup_useramount.sh 清库账户金额
	惠聚财_产品录入.class,
	惠聚财_产品审核.class,
	惠聚财_产品上架.class,
	惠聚财追加购买.class
})
public class 惠聚财_007 {

}
