package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHuiSouDongHomePage001;
import SecondStage.Hfax.TestHuiSouDongOtherPages002;
import SecondStage.Hfax.Page.HuiUserRegister;
import SecondStage.console_惠理财.手动发标_审核;
import SecondStage.console_惠理财.手动发标_新手标发布审核;
import SecondStage.console_惠理财.手动发标_新手标发布属性;
import SecondStage.console_惠理财.手动发标_新手标审核;
import SecondStage.console_惠理财.手动发标_新手标录入;
import SecondStage.console_惠理财.手动发标_设置发布属性;
import SecondStage.console_惠理财.手动发标_资产录入;
import SecondStage.console_惠理财.手动发标_项目发布审核;

/**
 *  
 * @Description 惠理财_手动发标验证流程
 * Created by songxq on 29/11/16.
 * 
 */

@RunWith(Suite.class)
@SuiteClasses({
	手动发标_资产录入.class,
	手动发标_审核.class,
	手动发标_设置发布属性.class,
	手动发标_项目发布审核.class,
	TestHuiSouDongOtherPages002.class	
})
public class 惠理财_手动发布验证流程 {

}
