package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestNewHuiSouDongHomePage003;
import SecondStage.Hfax.TestNewHuiSouDongOtherPages004;
import SecondStage.Hfax.Page.HuiUserRegister;
import SecondStage.console_惠理财.手动发标_新手标发布审核;
import SecondStage.console_惠理财.手动发标_新手标发布属性;
import SecondStage.console_惠理财.手动发标_新手标审核;
import SecondStage.console_惠理财.手动发标_新手标录入;
/**
 *  
 * @Description 新手标_手动发标验证流程
 * Created by songxq on 29/11/16.
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	手动发标_新手标录入.class,
	手动发标_新手标审核.class,
	手动发标_新手标发布属性.class,
	手动发标_新手标发布审核.class,
	TestNewHuiSouDongOtherPages004.class
})
public class 新手标_手动发布验证流程 {

}
