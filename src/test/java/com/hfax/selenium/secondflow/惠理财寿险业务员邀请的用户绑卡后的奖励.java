package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxywyGetRewardAfterInviteRegisterBangKa;
/**
 *  
 * @Description 惠理财寿险业务员邀请的用户绑卡后的奖励
 * Created by songxq on 12/16/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	//惠理财寿险用户邀请的用户绑卡后的奖励
	TestHlcsxywyGetRewardAfterInviteRegisterBangKa.class
})
public class 惠理财寿险业务员邀请的用户绑卡后的奖励 {

}
