package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.console.惠聚财_产品上架;
import com.hfax.selenium.regression.console.惠聚财_产品审核;
import com.hfax.selenium.regression.console.惠聚财_产品录入;
import com.hfax.selenium.regression.console.惠聚财_募集成功;
import SecondStage.Hfax.惠聚财满标搜索页列表页站内信;
import SecondStage.Hfax.惠聚财还款中搜索页列表页;
/**
 *  
 * @Description
 * 募集成功
 * Created by songxq on 09/18/16.
 * 1.申购满标后，标的状态显示满标
 * 2.点击募集成功，标的状态显示还款中
 * 3.发送短信及站内信（仅验证站内信）
 * （包括惠聚财_015中的 1.申购成功---站内信立即发送）
 * (包括惠聚财_08中的锁定期，协议验证)
 */

@RunWith(Suite.class)
@SuiteClasses({
	//cleanup_yuReSubject.sh 预热
	//cleanup_renZuSubject.sh 人组
	//cleanup_PiLiangYueSubject.sh 批量
	//cleanup_yueSubject.sh 惠聚财
	//cleanup_chuanYueSubject.sh 串标
	//cleanup_yueRules.sh 串标规则
	//cleanup_huijucaiTicket.sh 代金券和加息券清库
	//cleanup_useramount.sh 清库账户金额
	//cleanup_sendmessage.sh 清除站内信
	惠聚财_产品录入.class,
	惠聚财_产品审核.class,
	惠聚财_产品上架.class,
	惠聚财满标搜索页列表页站内信.class,
	惠聚财_募集成功.class,
	惠聚财还款中搜索页列表页.class
	
})
public class 惠聚财_010 {

}
