package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxywyAfterChongZhi;
import SecondStage.Hfax.Page.HuiLiCaiSxywyChongZhi;
import SecondStage.Hfax.Page.HuiLiCaiSxywyJiaoyiPassword;
/**
 *  
 * @Description 惠理财寿险业务员交易密码充值充值后的验证
 * Created by songxq on 12/14/16.
 * 
 * 
 */

@RunWith(Suite.class)
@SuiteClasses({
	HuiLiCaiSxywyJiaoyiPassword.class,
	HuiLiCaiSxywyChongZhi.class,
	TestHlcsxywyAfterChongZhi.class
})
public class 惠理财寿险业务员交易密码充值充值后 {

}
