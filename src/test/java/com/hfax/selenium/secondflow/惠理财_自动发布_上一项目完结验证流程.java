package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHuiAutoKxmhPage006;
import SecondStage.Hfax.TestHuiAutoListPages005_Hui;
import SecondStage.Hfax.TestHuiAutoListPages005_New;
import SecondStage.Hfax.TestHuiAutoListPages005_Te;
import SecondStage.console_惠理财.自动发布_上一项目完结_审核;
import SecondStage.console_惠理财.自动发布_上一项目完结_设置发布属性;
import SecondStage.console_惠理财.自动发布_上一项目完结_资产录入;
import SecondStage.console_惠理财.自动发布_上一项目完结_项目发布审核;
/**
 *  
 * @Description 惠理财_自动发布【上一项目完结】验证流程
 * Created by songxq on 30/11/16.
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	自动发布_上一项目完结_资产录入.class,
	自动发布_上一项目完结_审核.class,
	自动发布_上一项目完结_设置发布属性.class,
	自动发布_上一项目完结_项目发布审核.class,
	TestHuiAutoListPages005_New.class,
	TestHuiAutoListPages005_Hui.class,
	TestHuiAutoListPages005_Te.class,
//	TestHuiAutoKxmhPage006.class
})
public class 惠理财_自动发布_上一项目完结验证流程 {

}
