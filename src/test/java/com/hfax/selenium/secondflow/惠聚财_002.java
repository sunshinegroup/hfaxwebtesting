package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.惠聚财标的显示按照收益率和到期日排序;
import SecondStage.console_惠聚财.惠聚财_批量上架;
import SecondStage.console_惠聚财.惠聚财_批量审核;
import SecondStage.console_惠聚财.惠聚财_批量录入;
/**
 *  
 * @Description
 * 排序，收益率，页面元素显示正确
 * Created by songxq on 09/21/16.
 * 1.排序显示正确，同状态下，收益高的在前面显示，同收益，募集期结束日期由远及近显示
 * 2.页面元素显示正确（标的名称，项目特点，预期年化率，投资期限，起投金额，还款方式，项目总金额，投资进度条，标的状态）
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	//cleanup_yuReSubject.sh 预热
	//cleanup_renZuSubject.sh 人组
	//cleanup_PiLiangYueSubject.sh 批量---不需要清标
	//cleanup_yueSubject.sh 惠聚财
	//cleanup_chuanYueSubject.sh 串标
	//cleanup_yueRules.sh 串标规则 
//	惠聚财_批量录入.class,
	惠聚财_批量审核.class,
	惠聚财_批量上架.class,
	惠聚财标的显示按照收益率和到期日排序.class
})
public class 惠聚财_002 {

}
