package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.Page.HuiLiCaiInvestUserBangKa;
import SecondStage.Hfax.Page.HuiLiCaiInvestUserJiaoyiPassword;
import SecondStage.Hfax.Page.HuiLiCaiInvestUserRegister;
import SecondStage.Hfax.Page.ManualTicketsUserRegister;
import SecondStage.Hfax.Utils.Cleanup_HuiLiCaiInvestUserChongZhi;
import SecondStage.Hfax.Utils.Cleanup_Message_HuiLiCaiInvestUser;
/**
 *  
 * @Description 冒烟测试_惠理财_手动投资用户 
 * Created by songxq on 01/18/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	HuiLiCaiInvestUserRegister.class,
	HuiLiCaiInvestUserBangKa.class,
	HuiLiCaiInvestUserJiaoyiPassword.class,
	Cleanup_HuiLiCaiInvestUserChongZhi.class,
	Cleanup_Message_HuiLiCaiInvestUser.class,
	ManualTicketsUserRegister.class,
})
public class 冒烟测试_惠理财_手动投资用户 {

}
