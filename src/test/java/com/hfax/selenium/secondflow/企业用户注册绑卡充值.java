package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.business.企业注册;
import com.hfax.selenium.regression.business.企业验证账号信息;
import com.hfax.selenium.regression.console.企业用户审核;
import com.hfax.selenium.regression.console.企业资产录入;
import com.hfax.selenium.regression.console.过审查询;
import com.hfax.selenium.regression.console.项目发布审核;
import com.hfax.selenium.regression.console.项目审核;

import SecondStage.Hfax.企业用户充值;
import SecondStage.Hfax.企业用户绑卡;

/**
 *  
 * @Description 企业用户注册绑卡充值
 * Created by songxq on 09/19/16.
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	//cleanup_business_user.sh
	   企业注册.class,   //前台
	  企业验证账号信息.class,//前台
	  企业用户审核.class, //运营
//   企业资产录入.class,
//   项目审核.class,
//   过审查询.class,
//   项目发布审核.class,
	 企业用户绑卡.class,
	 企业用户充值.class
})
public class 企业用户注册绑卡充值 {

}
