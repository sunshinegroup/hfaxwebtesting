package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxywyInviteAfterRegister;
import SecondStage.Hfax.Page.HuiLiCaiSxywyInviteBangKa;
import SecondStage.Hfax.Page.HuiLiCaiSxywyInviteRegister;
import SecondStage.Hfax.Utils.Cleanup_Message_HuiLiCaisxywy;
/**
 *  切记执行：惠理财寿险业务员站内信的清库
 * @Description 惠理财寿险业务员邀请的用户注册以及注册后的验证
 * Created by songxq on 12/14/16.
 * 
 * 
 */

@RunWith(Suite.class)
@SuiteClasses({
	//惠理财寿险业务员站内信的清库
	Cleanup_Message_HuiLiCaisxywy.class,
	HuiLiCaiSxywyInviteRegister.class,
	TestHlcsxywyInviteAfterRegister.class,
})
public class 惠理财寿险业务员邀请的用户注册注册后 {

}
