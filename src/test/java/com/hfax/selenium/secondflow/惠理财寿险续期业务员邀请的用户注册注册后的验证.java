package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxXQywyInviteAfterRegister;
import SecondStage.Hfax.Page.HuiLiCaiSxXQywyInviteRegister;
import SecondStage.Hfax.Utils.Cleanup_Message_HuiLiCaisxXQywy;
/**
 * 
 * @Description 惠理财寿险【续期】业务员邀请的用户注册注册后的验证
 * Created by songxq on 12/20/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	//惠理财寿险续期业务员用户邀请的用户注册
	Cleanup_Message_HuiLiCaisxXQywy.class,
	HuiLiCaiSxXQywyInviteRegister.class,
	TestHlcsxXQywyInviteAfterRegister.class,
})
public class 惠理财寿险续期业务员邀请的用户注册注册后的验证 {

}
