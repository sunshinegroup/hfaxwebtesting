package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxywyAfterBangKa;
import SecondStage.Hfax.TestHlcsxywyAfterRegister;
import SecondStage.Hfax.Page.HuiLiCaiSxywyBangKa;
import SecondStage.Hfax.Page.HuiLiCaiSxywyRegister;
/**
 *  
 * @Description 惠理财寿险业务员注册以及注册后的验证
 * Created by songxq on 12/14/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	HuiLiCaiSxywyRegister.class,
	TestHlcsxywyAfterRegister.class,
	
})
public class 惠理财寿险业务员注册注册后的验证 {

}
