package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxXQywyAfterBangKa;
import SecondStage.Hfax.Page.HuiLiCaiSxXQywyBangKa;
import SecondStage.Hfax.Utils.Cleanup_Message_HuiLiCaisxXQywy;
/**
 * 
 * @Description 惠理财寿险【续期】业务员绑卡以及绑卡后的验证
 * Created by songxq on 12/19/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	Cleanup_Message_HuiLiCaisxXQywy.class,
	HuiLiCaiSxXQywyBangKa.class,
	TestHlcsxXQywyAfterBangKa.class
})
public class 惠理财寿险续期业务员绑卡绑卡后的验证 {

}
