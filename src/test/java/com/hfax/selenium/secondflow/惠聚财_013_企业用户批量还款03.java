package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资_惠聚财_搜索已还完的产品;
import com.hfax.selenium.regression.console.惠聚财_综合管理系统;

import SecondStage.Hfax.个人用户已还完的站内信;
import SecondStage.Hfax.个人用户批量已还完搜索产品;
import SecondStage.Hfax.个人用户批量已还完的站内信;
import SecondStage.Hfax.企业批量还款_惠聚财;
import SecondStage.Hfax.企业批量还款后个人用户账号金额;
import SecondStage.Hfax.企业用户已还完的站内信;
import SecondStage.Hfax.企业用户批量已还完的站内信;
import SecondStage.Hfax.企业用户批量还款前账户的余额;
import SecondStage.Hfax.企业用户批量还款后账户的余额;
import SecondStage.Hfax.企业用户还款前账户的余额;
import SecondStage.Hfax.企业用户还款后账户的余额;
import SecondStage.Hfax.企业还款_惠聚财;
import SecondStage.Hfax.企业还款后个人用户账号金额;
import SecondStage.console_惠聚财.惠聚财_综合管理系统02;
/**
 *  
 * @Description 企业用户批量还款03
 * Created by songxq on 09/20/16.
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	//执行yue_delete.sh
	//执行yue_insert_1.sh
	 惠聚财_综合管理系统02.class,
	 企业用户批量还款前账户的余额.class,
	 企业批量还款_惠聚财.class,
	 企业用户批量还款后账户的余额.class,
	 企业用户批量已还完的站内信.class,
	企业批量还款后个人用户账号金额.class,
	个人用户批量已还完搜索产品.class,
	个人用户批量已还完的站内信.class,
})
public class 惠聚财_013_企业用户批量还款03 {

}
