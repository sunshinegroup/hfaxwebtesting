package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.console_惠理财.自动发布_周_自动化投资_审核;
import SecondStage.console_惠理财.自动发布_周_自动化投资_设置发布属性;
import SecondStage.console_惠理财.自动发布_周_自动化投资_资产录入;
import SecondStage.console_惠理财.自动发布_周_自动化投资_项目发布审核;
/**
 *  
 * @Description 冒烟_惠理财_投资用户使用的标的
 * Created by songxq on 01/18/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	自动发布_周_自动化投资_资产录入.class,
	自动发布_周_自动化投资_审核.class,
	自动发布_周_自动化投资_设置发布属性.class,
	自动发布_周_自动化投资_项目发布审核.class,
})
public class 冒烟测试_惠理财_发标 {

}
