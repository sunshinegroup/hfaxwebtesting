package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import SecondStage.Hfax.TestHlcsxXQywyGetRewardAfterInviteRegisterBangKa;
/**
 *  
 * @Description 惠理财寿险【续期】业务员邀请的用户绑卡后的奖励
 * Created by songxq on 12/20/16.
 * 
 * 
 */

@RunWith(Suite.class)
@SuiteClasses({
	TestHlcsxXQywyGetRewardAfterInviteRegisterBangKa.class
})
public class 惠理财寿险续期业务员邀请的用户绑卡后的奖励 {

}
