package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxXQywyInviteAfterBangKa;
import SecondStage.Hfax.Page.HuiLiCaiSxXQywyInviteBangKa;
import SecondStage.Hfax.Utils.Cleanup_Message_HuiLiCaisxXQywyInvite;
/**
 * 
 * @Description 惠理财寿险【续期】业务员邀请的用户绑卡绑卡后的验证
 * Created by songxq on 12/20/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	//寿险续期业务员站内信清库
	Cleanup_Message_HuiLiCaisxXQywyInvite.class,
	HuiLiCaiSxXQywyInviteBangKa.class,
	TestHlcsxXQywyInviteAfterBangKa.class
	
})
public class 惠理财寿险续期业务员邀请的用户绑卡绑卡后的验证 {

}
