package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.惠聚财申购前做测评;
/**
 *  
 * @Description 
 * 申购前，做测评
 * Created by songxq on 09/18/16.
 * 1.第一次申购/不满足当前标的条件时做测评，点击立即测评（选择一种情况即可）
 * 2.11个题选择完成，显示测评结果
 * 3.点击去投资，跳转至惠聚财列表页
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	惠聚财申购前做测评.class
})
public class 惠聚财_003 {

}
