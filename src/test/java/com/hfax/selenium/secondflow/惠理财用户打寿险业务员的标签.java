package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
/**
 *  
 * @Description 惠理财用户打寿险业务员标签的验证
 * Created by songxq on 12/14/16.
 * 
 * 
 */

import SecondStage.Hfax.TestHlcsxywyAfterChongZhiTMS;
@RunWith(Suite.class)
@SuiteClasses({
	TestHlcsxywyAfterChongZhiTMS.class
})
public class 惠理财用户打寿险业务员的标签 {

}
