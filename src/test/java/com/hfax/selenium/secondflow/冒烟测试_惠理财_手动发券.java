package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.ticket_手动.手动卡券发放_上传用户列表_代金;
import SecondStage.ticket_手动.手动卡券发放_上传用户列表_返现;
import SecondStage.ticket_手动.手动卡券发放_兑换码_加息;
/**
 *  
 * @Description 冒烟测试_惠理财_手动发券
 * Created by songxq on 01/18/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	手动卡券发放_兑换码_加息.class,
	手动卡券发放_上传用户列表_代金.class,
	手动卡券发放_上传用户列表_返现.class,
})
public class 冒烟测试_惠理财_手动发券 {

}
