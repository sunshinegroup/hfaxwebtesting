package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHuiAutoKxmhPage012;
import SecondStage.Hfax.TestHuiAutoListPages011_Hui;
import SecondStage.Hfax.TestHuiAutoListPages011_New;
import SecondStage.Hfax.TestHuiAutoListPages011_Te;
import SecondStage.console_惠理财.自动发布_周_比例_审核;
import SecondStage.console_惠理财.自动发布_周_比例_设置发布属性;
import SecondStage.console_惠理财.自动发布_周_比例_资产录入;
import SecondStage.console_惠理财.自动发布_周_比例_项目发布审核;
/**
 *  
 * @Description 惠理财_自动发布-周-比例验证流程
 * Created by songxq on 29/11/16.
 * 
 */

@RunWith(Suite.class)
@SuiteClasses({
	自动发布_周_比例_资产录入.class,
	自动发布_周_比例_审核.class,
	自动发布_周_比例_设置发布属性.class,
	自动发布_周_比例_项目发布审核.class,
	TestHuiAutoListPages011_New.class,
	TestHuiAutoListPages011_Hui.class,
	TestHuiAutoListPages011_Te.class,
//	TestHuiAutoKxmhPage012.class,
})
public class 惠理财_自动发布_周_比例验证流程 {

}
