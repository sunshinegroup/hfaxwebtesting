package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.console.惠聚财_产品录入;

import SecondStage.Hfax.预热标的显示;
import SecondStage.console_惠聚财.惠聚财_批量录入;
import SecondStage.console_惠聚财.惠聚财_预热标上架;
import SecondStage.console_惠聚财.惠聚财_预热标审核;
/**
 *  
 * @Description 
 * 惠聚财预热标的展示
 * Created by songxq on 09/20/16.
 * 1.运营平台发标，设置上架时间时，选择上架时间及预览时间成功，设置成预热标
 * 2.查看预热标，在立即投资下显示 
 * 3.点击预热标进入预热标，点击申购，不可点击，置灰
 * (包括惠聚财_008中，预热中，协议验证)
 */

@RunWith(Suite.class)
@SuiteClasses({
	//cleanup_yuReSubject.sh 预热
	//cleanup_renZuSubject.sh 人组
	//cleanup_PiLiangYueSubject.sh 批量
	//cleanup_yueSubject.sh 惠聚财
	//cleanup_chuanYueSubject.sh 串标
	//cleanup_yueRules.sh 串标规则
//	惠聚财_批量录入.class,
	惠聚财_预热标审核.class,
	惠聚财_预热标上架.class,
	预热标的显示.class
})
public class 惠聚财_009_预热标的 {

}
