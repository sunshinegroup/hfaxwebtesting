package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxXQywyAfterRegister;
import SecondStage.Hfax.Page.HuiLiCaiSxXQywyRegister;
/**
 * 
 * @Description 惠理财寿险【续期】业务员注册以及注册后的验证
 * Created by songxq on 12/19/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	HuiLiCaiSxXQywyRegister.class,
	TestHlcsxXQywyAfterRegister.class
})
public class 惠理财寿险续期业务员注册注册后的验证 {

}
