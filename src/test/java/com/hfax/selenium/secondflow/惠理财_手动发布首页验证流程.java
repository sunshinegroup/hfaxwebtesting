package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHuiSouDongHomePage001;
/**
 *  
 * @Description 惠理财_手动发标【首页】验证流程
 * Created by songxq on 30/11/16.
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	TestHuiSouDongHomePage001.class,
})
public class 惠理财_手动发布首页验证流程 {

}
