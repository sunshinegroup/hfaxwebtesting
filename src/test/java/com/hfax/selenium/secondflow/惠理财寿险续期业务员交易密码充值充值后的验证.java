package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxXQywyAfterChongZhi;
import SecondStage.Hfax.Page.HuiLiCaiSxXQywyChongZhi;
import SecondStage.Hfax.Page.HuiLiCaiSxXQywyJiaoyiPassword;
/**
 * 
 * @Description 惠理财寿险【续期】业务员交易密码充值充值后的验证
 * Created by songxq on 12/19/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	HuiLiCaiSxXQywyJiaoyiPassword.class,
	HuiLiCaiSxXQywyChongZhi.class,
	TestHlcsxXQywyAfterChongZhi.class
})
public class 惠理财寿险续期业务员交易密码充值充值后的验证 {

}
