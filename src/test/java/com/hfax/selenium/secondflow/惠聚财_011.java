package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.console.惠聚财_产品上架;
import com.hfax.selenium.regression.console.惠聚财_产品审核;
import com.hfax.selenium.regression.console.惠聚财_产品录入;
import com.hfax.selenium.regression.console.惠聚财_募集失败;

import SecondStage.Hfax.惠聚财流标代金券加息券;
import SecondStage.Hfax.惠聚财流标站内信产品不展示账户余额;
import SecondStage.console_惠聚财.惠聚财_代金券发放;
import SecondStage.console_惠聚财.惠聚财_加息券发放;
/**
 *  
 * @Description 
 * 募集失败流标
 * Created by songxq on 09/18/16.
 * 1.用户申购标的时，输入合规金额，选择代金券+加息券，输入密码完成购买
 * 2.运营平台点击募集失败，变流标状态
 * 3.查看短信及站内信，pc惠聚财列表页内不显示标的（仅验证站内信及PC页面）
 * 4.查看可用余额有增加，代金券及加息券返还
 * (包括惠聚财_015中的流标---站内信立即发送)
 */
@RunWith(Suite.class)
@SuiteClasses({
	//cleanup_yuReSubject.sh 预热
	//cleanup_renZuSubject.sh 人组
	//cleanup_PiLiangYueSubject.sh 批量
	//cleanup_yueSubject.sh 惠聚财
	//cleanup_chuanYueSubject.sh 串标
	//cleanup_yueRules.sh 串标规则
	//cleanup_yueSubject.sh   标的清库
	//cleanup_huijucaiTicket.sh 代金券和加息券清库
	//cleanup_useramount.sh 清库账户金额
	//cleanup_sendmessage.sh 清除站内信
	惠聚财_产品录入.class,
	惠聚财_产品审核.class,
	惠聚财_产品上架.class,
	惠聚财_代金券发放.class,
	惠聚财_加息券发放.class,
	惠聚财流标代金券加息券.class,
	惠聚财_募集失败.class,
	惠聚财流标站内信产品不展示账户余额.class
})
public class 惠聚财_011 {

}
