package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxywyInviteAfterBangKa;
import SecondStage.Hfax.Page.HuiLiCaiSxywyInviteBangKa;
import SecondStage.Hfax.Utils.Cleanup_Message_HuiLiCaisxywyInviteUser;
/**
 *  
 * @Description 惠理财寿险业务员邀请的用户绑卡以及绑卡后的验证
 * Created by songxq on 12/16/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	//惠理财寿险用户邀请的用户站内信清库
	Cleanup_Message_HuiLiCaisxywyInviteUser.class,
	HuiLiCaiSxywyInviteBangKa.class,
	TestHlcsxywyInviteAfterBangKa.class,
})
public class 惠理财寿险业务员邀请的用户绑卡绑卡后 {

}
