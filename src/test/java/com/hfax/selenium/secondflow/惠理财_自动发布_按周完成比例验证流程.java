package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHuiAutoKxmhPage008;
import SecondStage.Hfax.TestHuiAutoListPages005_New;
import SecondStage.Hfax.TestHuiAutoListPages007_Hui;
import SecondStage.Hfax.TestHuiAutoListPages007_New;
import SecondStage.Hfax.TestHuiAutoListPages007_Te;
import SecondStage.console_惠理财.自动发布_按周完成比例_审核;
import SecondStage.console_惠理财.自动发布_按周完成比例_设置发布属性;
import SecondStage.console_惠理财.自动发布_按周完成比例_资产录入;
import SecondStage.console_惠理财.自动发布_按周完成比例_项目发布审核;
/**
 *  
 * @Description 惠理财_自动发布【按周完成比例】验证流程
 * Created by songxq on 29/11/16.
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	自动发布_按周完成比例_资产录入.class,
	自动发布_按周完成比例_审核.class,
	自动发布_按周完成比例_设置发布属性.class,
	自动发布_按周完成比例_项目发布审核.class,
	TestHuiAutoListPages007_New.class,
	TestHuiAutoListPages007_Hui.class,
	TestHuiAutoListPages007_Te.class,
//	TestHuiAutoKxmhPage008.class
})
public class 惠理财_自动发布_按周完成比例验证流程 {

}
