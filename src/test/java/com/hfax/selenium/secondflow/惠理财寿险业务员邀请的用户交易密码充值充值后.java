package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxywyInviteAfterChongZhi;
import SecondStage.Hfax.Page.HuiLiCaiSxywyInviteChongZhi;
import SecondStage.Hfax.Page.HuiLiCaiSxywyInviteJiaoyiPassword;
/**
 *  
 * @Description 惠理财寿险业务员邀请的用户交易密码充值充值后
 * Created by songxq on 12/16/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	//惠理财寿险用户邀请的用户交易密码
	HuiLiCaiSxywyInviteJiaoyiPassword.class,
	//惠理财寿险用户邀请的用户充值
	HuiLiCaiSxywyInviteChongZhi.class,
	//惠理财寿险用户邀请的用户充值后的验证
	TestHlcsxywyInviteAfterChongZhi.class
})
public class 惠理财寿险业务员邀请的用户交易密码充值充值后 {

}
