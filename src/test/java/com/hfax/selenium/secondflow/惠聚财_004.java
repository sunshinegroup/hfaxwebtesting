package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.console.惠聚财_产品上架;
import com.hfax.selenium.regression.console.惠聚财_产品审核;
import com.hfax.selenium.regression.console.惠聚财_产品录入;

import SecondStage.Hfax.惠聚财代金券申购;
import SecondStage.console_惠聚财.惠聚财_代金券发放;
/**
 *  
 * @Description 
 * 惠聚财代金券申购
 * Created by songxq on 09/18/16.
 * 1.选择标的，输入合规金额，选择代金券，输入密码完成申购
 * 2.再次进入标的，查看收益显示正确
 * 3.查看账户余额，可用余额减少
 */
@RunWith(Suite.class)
@SuiteClasses({
	//cleanup_yuReSubject.sh 预热
	//cleanup_renZuSubject.sh 人组
	//cleanup_PiLiangYueSubject.sh 批量
	//cleanup_yueSubject.sh 惠聚财
	//cleanup_chuanYueSubject.sh 串标
	//cleanup_yueRules.sh 串标规则
	//cleanup_huijucaiTicket.sh 代金券和加息券清库
	//cleanup_useramount.sh 清库账户金额
	惠聚财_产品录入.class,
	惠聚财_产品审核.class,
	惠聚财_产品上架.class,
	惠聚财_代金券发放.class,
	惠聚财代金券申购.class,
	
})
public class 惠聚财_004 {

}
