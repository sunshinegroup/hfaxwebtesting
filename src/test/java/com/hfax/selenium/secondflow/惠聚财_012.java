package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.惠聚财串行标的显示;
import SecondStage.console_惠聚财.惠聚财_串行上架;
import SecondStage.console_惠聚财.惠聚财_串行审核;
import SecondStage.console_惠聚财.惠聚财_串行录入;
import SecondStage.console_惠聚财.惠聚财_设置串行属性;
import SecondStage.console_惠聚财.惠聚财_设置串行规则;

/**
 *  
 * @Description
 * 串行标上线成功，可以申购
 * Created by songxq on 09/22/16.
 * 1.运营平台录入批量标的
 * 2.批量审核通过
 * 3.设置其中一个为上架产品
 * 4.设置串中产品属性
 * 5.上传串中标的表
 * 6.查看前台展示，标1申购满标后，标2自动上架，标2直至末标在规定时间内申购完成，完成上架显示（验证第一个标的截标后，第二个标的规定时间内正常显示）
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	//cleanup_yuReSubject.sh 预热
	//cleanup_renZuSubject.sh 人组
	//cleanup_PiLiangYueSubject.sh 批量
	//cleanup_yueSubject.sh 惠聚财
	//cleanup_chuanYueSubject.sh 串标
	//cleanup_yueRules.sh 串标规则 其中Auto01为串名
	惠聚财_串行录入.class,
	惠聚财_串行审核.class,
	惠聚财_串行上架.class,
	惠聚财_设置串行属性.class,
	惠聚财_设置串行规则.class,
	惠聚财串行标的显示.class
})
public class 惠聚财_012 {

}
