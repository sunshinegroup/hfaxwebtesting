package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestNewHuiSouDongHomePage003;
/**
 *  
 * @Description 新手标_手动发标【首页】验证流程
 * Created by songxq on 30/11/16.
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	TestNewHuiSouDongHomePage003.class,
})
public class 新手标_手动发布首页验证流程 {

}
