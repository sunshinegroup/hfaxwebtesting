package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.人组标的显示;
import SecondStage.console_惠聚财.惠聚财_人组验证产品上架;
import SecondStage.console_惠聚财.惠聚财_人组验证产品审核;
import SecondStage.console_惠聚财.惠聚财_批量上架;
import SecondStage.console_惠聚财.惠聚财_批量审核;
import SecondStage.console_惠聚财.惠聚财_批量录入;
/**
 *  
 * @Description 
 * 组内人员登录后可查看惠聚财标的
 * Created by songxq on 09/22/16.
 * 1.运营平台，惠聚财发标选择人组，发标成功
 * 2.组内人员登录pc，可查看到惠聚财标的
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	//cleanup_yuReSubject.sh 预热
	//cleanup_renZuSubject.sh 人组
	//cleanup_PiLiangYueSubject.sh 批量
	//cleanup_yueSubject.sh 惠聚财
	//cleanup_chuanYueSubject.sh 串标
	//cleanup_yueRules.sh 串标规则 其中Auto01为串名
	惠聚财_批量录入.class,
	惠聚财_人组验证产品审核.class,
	惠聚财_人组验证产品上架.class,
	人组标的显示.class
})
public class 惠聚财_001 {

}
