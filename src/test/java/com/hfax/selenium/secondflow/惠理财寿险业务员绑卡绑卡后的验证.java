package com.hfax.selenium.secondflow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import SecondStage.Hfax.TestHlcsxywyAfterBangKa;
import SecondStage.Hfax.Page.HuiLiCaiSxywyBangKa;
/**
 *  前提：需要执行清除站内信的sql
 *  clean_sendmessage_1.sql
 * @Description 惠理财寿险业务员绑卡以及绑卡后的验证
 * Created by songxq on 12/14/16.
 * 
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({
	HuiLiCaiSxywyBangKa.class,
	TestHlcsxywyAfterBangKa.class
})
public class 惠理财寿险业务员绑卡绑卡后的验证 {

}
