package com.hfax.selenium.rule;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;

/**
 * Created by philip on 4/1/16.
 */
public class BaseRule extends TestWatcher {
    public WebDriver driver;

    @Override
    protected void starting(Description description) {
        System.out.println("开始案例: " + description.getDisplayName());
    }

    @Override
    protected void finished(Description description) {
        System.out.println("完成案例: " + description.getDisplayName());
    }

    @Override
    protected void failed(Throwable e, Description description) {
        System.out.println("案例失败: " + e.getLocalizedMessage());
    }
}
