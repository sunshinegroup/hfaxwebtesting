package com.hfax.selenium.rule;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;

/**
 * Created by philip on 3/31/16.
 */
public class SuddenDeathRule extends BaseRule {
    @Override
    protected void failed(Throwable e, Description description) {
        super.failed(e, description);
        System.out.println("突然死亡, 测试无法继续");
        System.exit(255);
    }
}
