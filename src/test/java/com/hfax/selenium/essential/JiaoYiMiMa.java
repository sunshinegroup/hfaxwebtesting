package com.hfax.selenium.essential;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Map;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * 设置交易密码
 * Created by philip on 3/10/16.
 * Modify by songxq on 12/7/16
 */
public class JiaoYiMiMa extends SuddenDeathBaseTest {
	
	Map<String, String> envVars = System.getenv();
	//个人用户登录
	protected void userLogin(){
		this.loadPage();
        this.login();
	}
	//个人交易密码
	protected String userJiaoyimima(){
		return this.jiaoyipassword;
	}
		
    @Test
    public void jiaoYiMiMa() {
        userLogin();
        //点击我的账户
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	while(true){
        		if(IsElementPresent(By.linkText("我的账户"))){
        			break;
        		}
        	}
        	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("我的账户"))).click();	
        	
        }else{
        	
        	  this.driver.findElement(By.xpath("//div[@class='hfax-topnav-nav right']//a[5]")).click();
        
        }
        
        //等待页面加载
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@class='hfax-topnav-nav right']//a[5]")).isDisplayed();
            }
        });
        //IE菜单调整
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
        	 try {
        			Thread.sleep(5000);
        			this.driver.findElement(By.xpath("//ul[@id='account_leftMenu']//li[3]//a")).click();
                  } catch (InterruptedException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
                  }	 
         	
		 }
        else{	
        		this.leftNavigationMenu(3,"充值/提现");
        	}
        //点击充值
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@class='detail_list clearfix']//ul[@class='tab_top']//li[2]")).isDisplayed();
            }
        });
        //点击充值
        this.rightTabNavigationMenu(2,"充值");
         
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath(".//*[@id='dealPasswordDivTip']/p")).getText();
                return text.equalsIgnoreCase("您还没有设置交易密码！");
            }
        });
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='dealPasswordDivTip']/div/a[2]")).isDisplayed();
            }
        });
        //点击立即设置
        this.driver.findElement(By.xpath(".//*[@id='dealPasswordDivTip']/div/a[2]")).click();
        
        
        try {
			Thread.sleep(3000);
          } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
          }	 
        //等待发送验证码
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='clickCode']")).isDisplayed();
            }
        });
        //点击发送验证码
        this.driver.findElement(By.xpath(".//*[@id='clickCode']")).click();
        try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        //输入短信验证码
        this.driver.findElement(By.xpath(".//*[@id='code']")).clear();
        this.driver.findElement(By.xpath(".//*[@id='code']")).sendKeys("1234");
        
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	
        	//设置新交易密码
        	  wait.until(new ExpectedCondition<Boolean>() {
                  @Override
                  public Boolean apply(WebDriver webDriver) {
                      return webDriver.findElement(By.xpath("//form[@class='hfax-border-bottom']//ul[@class='form_info']//li[3]//input")).isDisplayed();
                  }
              });
        	//this.driver.findElement(By.xpath("//form[@class='hfax-border-bottom']//ul[@class='form_info']//li[3]//input")).clear();
            this.driver.findElement(By.xpath("//form[@class='hfax-border-bottom']//ul[@class='form_info']//li[3]//input")).sendKeys(userJiaoyimima());
        
        }else{
        	//设置新交易密码
        	this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).clear();
            this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).sendKeys(userJiaoyimima());
        }
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	 wait.until(new ExpectedCondition<Boolean>() {
                 @Override
                 public Boolean apply(WebDriver webDriver) {
                     return webDriver.findElement(By.xpath("//form[@class='hfax-border-bottom']//ul[@class='form_info']//li[4]//input")).isDisplayed();
                 }
             });
        	//确认新交易密码
            //this.driver.findElement(By.xpath("//form[@class='hfax-border-bottom']//ul[@class='form_info']//li[4]//input")).clear();
            this.driver.findElement(By.xpath("//form[@class='hfax-border-bottom']//ul[@class='form_info']//li[4]//input")).sendKeys(userJiaoyimima());
            //点击提交
            this.driver.findElement(By.xpath("//div[@class='detail_tab']//form//a[@id='btn_submit']")).click();
            
        }else{
        	//确认新交易密码
        	this.driver.findElement(By.xpath(".//*[@id='ResetConfirmPass']")).clear();
        	this.driver.findElement(By.xpath(".//*[@id='ResetConfirmPass']")).sendKeys(userJiaoyimima());
        	//点击提交
            this.driver.findElement(By.xpath(".//*[@id='btn_submit']")).click();
        }
        
        
        
        try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='know_al']")));
        assertTrue("提示信息错误",this.driver.findElement(By.xpath(".//*[@id='know_al']")).getText().trim().equalsIgnoreCase("我知道了"));
        if(this.driver.getPageSource().contains("密码设置成功"))
		 {
        	//点击我知道了
        	 this.driver.findElement(By.xpath(".//*[@id='know_al']")).click();
		 }else{
			 	fail("交易密码没有设置成功");
		 }
        
    }
}
