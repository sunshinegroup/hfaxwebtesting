package com.hfax.selenium.essential;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * 设置交易密码
 * Created by philip on 3/10/16.
 *
 */
public class JiaoYiMiMa extends SuddenDeathBaseTest {
    @Test
    public void jiaoYiMiMa() {
        this.loadPage();
        this.login();

        this.driver.findElement(By.id("li_5")).click();

        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[3]")).getText();
                return text.equalsIgnoreCase("交易密码管理");
            }
        });

        this.driver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[3]")).click();
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@id='setTranPwdBodyDivId']/form")).isDisplayed();
            }
        });

        this.driver.findElement(By.id("clickCode")).click();
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("code_voice_tip")).isDisplayed();
            }
        });

        this.driver.findElement(By.id("code")).sendKeys("1234");
        this.driver.findElement(By.id("ResetNewPass")).sendKeys(this.password);
        this.driver.findElement(By.id("ResetConfirmPass")).sendKeys(this.password);
        this.driver.findElement(By.id("btn_submit")).click();

        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.switchTo().alert().getText().equalsIgnoreCase("交易密码重置成功");
            }
        });

        this.driver.switchTo().alert().accept();
    }
}
