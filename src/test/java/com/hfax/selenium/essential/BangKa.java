package com.hfax.selenium.essential;

import com.hfax.selenium.base.SuddenDeathBaseTest;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 绑卡
 * Created by philip on 3/10/16.
 * Modify by songxq on 12/7/16
 */
public class BangKa extends SuddenDeathBaseTest {
	Map<String, String> envVars = System.getenv();
	//个人用户登录
	protected void userLogin()throws ClassNotFoundException, SQLException{
		this.loadPage();
        this.login();
	}
	//个人卡号
	protected String userCard(){
		return this.card;
	}
	//个人手机号
	protected String userPhone(){
		return this.phone;
	}
	 //个人真实姓名
	 protected String userRealName(){
		    return this.name;
	  }
    @Test
    public void bangKa() throws UnsupportedEncodingException, ClassNotFoundException, SQLException {
    	WebDriver windowIE = null;
        userLogin();
        
        //点击我的账户
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	
        	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("我的账户"))).click();	
        	
        }else{
        	//因为hub做的调整
        	while(true){
        		if(IsElementPresent(By.xpath("//div[@class='hfax-topnav-nav right']//a[5]"))){
        			break;
        		}
        	}
        	System.out.println("点击我的账户前");
        	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//a[5]"))).click();
        	System.out.println("点击我的账户后");
        
        }
        //等待页面加载
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@class='hfax-topnav-nav right']//a[5]")).isDisplayed();
            }
        });
   
        //IE菜单选择""
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
        	 try {
       			Thread.sleep(2000);
       		  	this.driver.findElement(By.xpath("//ul[@id='account_leftMenu']//li[3]//a")).click();
       		  	Thread.sleep(2000);
                 } catch (InterruptedException e) {
       			// TODO Auto-generated catch block
       			e.printStackTrace();
                 }	 
      		 
		 }else{
			 System.out.println("点击充值/提现前");
//			 this.newSelectMenu(3,"充值/提现");
			 this.leftNavigationMenu(3,"充值/提现");
			 System.out.println("点击充值/提现后");
		 }
       
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath("//div[@class='detail_list clearfix']//ul[@class='tab_top']//li[2]")).getText();
                return text.equalsIgnoreCase("充值");
            }
        });
        //点击充值
        System.out.println("点击充值前");
        this.rightTabNavigationMenu(2,"充值");
        System.out.println("点击充值后");
        
        //等待绑卡提示
        //等待页面加载
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.className("sure_btn")).isDisplayed();
            }
        });
        while(true){
        	if(IsElementPresent(By.className("sure_btn"))){
        		break;
        	}
        }
        if(this.driver.getPageSource().contains("请您先绑定银行卡！"))
        {
        	System.out.println("点击立即绑卡前");
        	//点击立即绑卡
        	this.driver.findElement(By.className("sure_btn")).click();
        	System.out.println("点击立即绑卡后");
        	try {
    			Thread.sleep(3000);
              } catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
              }	 
		 }else{
			 fail("绑卡提示不正确");
		 }
        
        //等待卡号
         new WebDriverWait(this.driver,20).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("bankCardIdUT1")).isDisplayed();
            }
        });
         
         //点击《认证支付服务协议》
         String currentWindow = this.driver.getWindowHandle();
         System.out.println("第一个窗口的信息：" + currentWindow);
         if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
		  try {
	  			Thread.sleep(2000);
	  			System.out.println("点击《认证支付服务协议》前");
	            } catch (InterruptedException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	            }	  
		 }
         this.driver.findElement(By.xpath("//p[@class='agreement hfax-agreement']//a")).click();
         if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
		  try {
	  			Thread.sleep(2000);
	  			System.out.println("点击《认证支付服务协议》后");
	            } catch (InterruptedException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	            }	  
		 }

		//弹出新窗口
		 Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
		 System.out.println("当前窗口数为："+ handles.size());
		 Iterator<String> it = handles.iterator();
		 while (it.hasNext()) { 
			 String win = (String)(it.next());
			 System.out.println("第二个窗口的信息为：" + win);
			 if (currentWindow.equals(win)){
				 continue;
    		}
		
		  driver.switchTo().window(win);//切换到新窗口
		  this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("html/body/h1/strong")).isDisplayed();
	            }
	        });
		  driver.switchTo().window(win).close();
	
		 }
			  

		 this.switchToWindow("惠金所");
        //输入卡号信息
         this.driver.findElement(By.id("bankCardIdUT1")).clear();
         this.driver.findElement(By.id("bankCardIdUT1")).sendKeys(userCard());
         
         //输入手机号
         this.driver.findElement(By.id("phoneNumberUT1")).clear();
         this.driver.findElement(By.id("phoneNumberUT1")).sendKeys(userPhone());
      
         this.wait.until(new ExpectedCondition<Boolean>() {
             @Override
             public Boolean apply(WebDriver webDriver) {
                 return webDriver.findElement(By.xpath("//input[@class='hfax-black hfax-set-black']")).isDisplayed();
             }
         });
         //IE调整
         String nameStart = userRealName().substring(0,1) + "**";
         System.out.println("nameStart:" + nameStart);
         if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	 try {
        			Thread.sleep(5000);
                  } catch (InterruptedException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
                  }	 
         	assertTrue("银行户名不正确", this.driver.findElement(By.xpath("//input[@class='hfax-black hfax-set-black']")).getAttribute("value").trim().equalsIgnoreCase(nameStart));
         }else{
       
        	 assertTrue("银行户名不正确", this.driver.findElement(By.xpath("//input[@class='hfax-black hfax-set-black']")).getAttribute("value").trim().equalsIgnoreCase(nameStart));
         }
       
         
         //点击发送验证码
         this.driver.findElement(By.id("sendAccountCode")).click();
         
         //输入验证码
         try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         this.driver.findElement(By.id("accountCode")).clear();
         this.driver.findElement(By.id("accountCode")).sendKeys("1234");
         
       //等待确定
         this.wait.until(new ExpectedCondition<Boolean>() {
             @Override
             public Boolean apply(WebDriver webDriver) {
                 return webDriver.findElement(By.className("charge_btn_big")).isDisplayed();
             }
         }); 
         //点击提交
         this.driver.findElement(By.className("charge_btn_big")).click();
        //等待确定
         this.wait.until(new ExpectedCondition<Boolean>() {
             @Override
             public Boolean apply(WebDriver webDriver) {
                 return webDriver.findElement(By.linkText("确定")).isDisplayed();
             }
         });
       if(this.driver.getPageSource().contains("绑卡成功！")){
    	   //点击确定
    	   this.driver.findElement(By.linkText("确定")).click();
    	   
       }else{
    	   	fail("绑卡没有成功");
       }
         
    }
}