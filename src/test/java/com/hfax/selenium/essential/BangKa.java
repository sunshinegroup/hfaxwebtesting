package com.hfax.selenium.essential;

import com.hfax.selenium.base.SuddenDeathBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.hfax.selenium.base.BaseTest;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * 绑卡
 * Created by philip on 3/10/16.
 *
 */
public class BangKa extends SuddenDeathBaseTest {
    @Test
    public void bangKa() {
        this.loadPage();
        this.login();

        this.driver.findElement(By.id("li_2")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[2]")).getText();
                return text.equalsIgnoreCase("充值");
            }
        });

        this.driver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[2]")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("html/body//div[@id='personInfo']/table")).isDisplayed();
            }
        });

        Select select = new Select(this.driver.findElement(By.id("bankName1")));
        select.selectByValue("0105");

        this.driver.findElement(By.id("bankCard1")).sendKeys(this.card);
        this.driver.findElement(By.id("bankCellPhone_")).sendKeys(this.phone);
        this.driver.findElement(By.id("jiaoyimima_")).sendKeys(this.password);
        this.driver.findElement(By.id("addbank")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.className("popBox")).isDisplayed();
            }
        });

        this.driver.findElement(By.id("checkSure")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("checkSuccess")).isDisplayed();
            }
        });
    }

}

