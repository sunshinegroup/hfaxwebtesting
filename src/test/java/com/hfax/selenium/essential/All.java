package com.hfax.selenium.essential;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * 必跑测试案例, 必须清库后先跑, 会创建账户, 身份认证, 设置交易密码, 绑卡, 充值
 *
 * Created by philip on 3/10/16.
 *
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ZhuCe.class, JiaoYiMiMa.class, BangKa.class, ChongZhi.class})
public class All {
}
