package com.hfax.selenium.essential;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

/**
 *
 * 注册
 * Created by philip on 3/8/16.
 *
 */
public class ZhuCe extends SuddenDeathBaseTest {
    @Test
    public void zhuCe() throws Exception {
        this.loadPage();

        this.driver.findElement(By.className("register_tab_but")).click();
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("regist-content")));

        this.driver.findElement(By.className("reg-del")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("regist-content")));

        // 第一步

        this.driver.findElement(By.id("userName")).sendKeys(this.username);
        this.driver.findElement(By.id("password")).sendKeys(this.password);
        this.driver.findElement(By.id("confirmPassword")).sendKeys(this.password);
        this.driver.findElement(By.id("code")).sendKeys("1234");

        this.driver.findElement(By.id("agre")).click();
        this.driver.findElement(By.id("btn_register")).click();

        // 第二步
        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//form[@id='mobilesend']/h2"), "尊敬的客户，为保障资金安全，现在将对您的手机号码进行验证。"));

        this.driver.findElement(By.id("phoneNum")).sendKeys(this.phone);
        this.driver.findElement(By.id("codeBtn")).click();

        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.id("code_voice_tip")).isDisplayed();
            }
        });

        this.driver.findElement(By.id("phoneCode")).sendKeys("1234");
        this.driver.findElement(By.id("ph_btn")).click();

        // 第三步
        wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.className("regOk"));
            }
        });

        this.driver.findElement(By.id("realName")).sendKeys(this.name);
        this.driver.findElement(By.id("idCard")).sendKeys(this.identification);
        this.driver.findElement(By.id("auth_btn")).click();

        // 等待登陆跳转
        wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.className("userName"));
            }
        });

        String usernameToConfirm = this.driver.findElement(By.className("userName")).getText();
        assertTrue("登陆无法确认", usernameToConfirm.equalsIgnoreCase(this.username));
    }
}
