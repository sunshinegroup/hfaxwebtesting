package com.hfax.selenium.essential;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Map;

/**
 *
 * 个人注册
 * Created by philip on 3/8/16.
 * modify by songxq on 25/10/16.
 *
 */
public class ZhuCe extends SuddenDeathBaseTest {
	Map<String, String> envVars = System.getenv();
	
	 //个人用户名
	  protected String userName() throws ClassNotFoundException, SQLException{
	        return this.username;
	    }
	  //个人登录密码
	  protected String userPassword(){
		  	return this.password;
	  }
	  //个人手机号
	  protected String userPhone(){
		  	return this.phone;
	  }
	  //个人真实姓名
	  protected String userRealName(){
		    return this.name;
	  }
	  //个人身份证号码
	  protected String userIdentification(){
		  return this.identification;
	  }
	  //邀请码
	  protected String userInvitationCode()throws ClassNotFoundException, SQLException{
		  return "";
	  }
    @Test
    public void zhuCe() throws Exception {
    	
    	this.loadPage();
    	
//      this.driver.findElement(By.className("register_tab_but")).click();
    	while(true){
    		if(IsElementPresent(By.linkText("快速注册"))){
    			break;
    		}
    	}
        this.driver.findElement(By.linkText("快速注册")).click();
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("regist-content")));

        this.driver.findElement(By.className("reg-del")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("regist-content")));

        // 第一步
        this.driver.findElement(By.id("userName")).sendKeys(userName());
        this.driver.findElement(By.id("password")).sendKeys(userPassword());
        this.driver.findElement(By.id("confirmPassword")).sendKeys(userPassword());
        //输入邀请码
        this.driver.findElement(By.id("inviteCode")).clear();
        this.driver.findElement(By.id("inviteCode")).sendKeys(userInvitationCode());;
        this.driver.findElement(By.id("code")).sendKeys("1234");
        
        //点击服务协议
        this.driver.findElement(By.linkText("《服务协议》")).click();
        //关闭服务协议
        this.driver.findElement(By.xpath("//div[@class='loginPopBox']//h2//span[@class='close']")).click();
        //点击隐私协议
        this.driver.findElement(By.linkText("《隐私规则》")).click();
        //关闭隐私协议
        this.driver.findElement(By.xpath("//div[@id='yinsi']//div[@class='loginPopBox']//h2//span")).click();
        this.driver.findElement(By.id("agre")).click();
        this.driver.findElement(By.id("btn_register")).click();
        try{
        	this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//form[@id='verifyPhoneForm']/h2"), "尊敬的客户，为保障资金安全，现在将对您的手机号码进行验证。"));

        	this.driver.findElement(By.id("phoneNum")).clear();
            this.driver.findElement(By.id("phoneNum")).sendKeys(userPhone());
            if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
            	 try {
           			Thread.sleep(5000);
           			this.driver.findElement(By.linkText("获取短信验证码")).click();
                     } catch (InterruptedException e) {
           			// TODO Auto-generated catch block
           			e.printStackTrace();
                     }	        	
            }else{
            	
            	this.driver.findElement(By.id("codeBtn")).click();
            	
            }
            wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    return driver.findElement(By.id("code_li")).isDisplayed();
                }
            });
            
            this.driver.findElement(By.id("smsVerifyCode")).clear();
            this.driver.findElement(By.id("smsVerifyCode")).sendKeys("1234");
            this.driver.findElement(By.id("ph_btn")).click();

            // 第三步
            wait.until(new ExpectedCondition<WebElement>() {
                @Override
                public WebElement apply(WebDriver webDriver) {
                    return webDriver.findElement(By.className("regOk"));
                }
            });

            Thread.sleep(3000);
            this.driver.findElement(By.id("realName")).clear();
            this.driver.findElement(By.id("realName")).sendKeys(userRealName());
            
            this.driver.findElement(By.id("idCard")).clear();
            this.driver.findElement(By.id("idCard")).sendKeys(userIdentification());
            this.driver.findElement(By.id("auth_btn")).click();

            // 等待登陆跳转
            wait.until(new ExpectedCondition<WebElement>() {
                @Override
                public WebElement apply(WebDriver webDriver) {
                    return webDriver.findElement(By.className("userName"));
                }
            });

            String usernameToConfirm = this.driver.findElement(By.className("userName")).getText();
            assertTrue("登陆无法确认", usernameToConfirm.equalsIgnoreCase(userName()));
        }
       catch(Exception e){
    	   if(IsElementPresent(By.xpath(".//*[@id='s_global_error']"))&&this.driver.findElement(By.xpath(".//*[@id='s_global_error']")).getText().trim().equals("用户名已被注册")){
           	System.out.println("此用户已经被注册");
           	fail("该用户已经被注册");
           	}
        
       }
    }
}
