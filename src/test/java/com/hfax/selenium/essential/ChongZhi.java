package com.hfax.selenium.essential;

import static org.junit.Assert.assertTrue;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;
import com.hfax.selenium.base.Util;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.Util;

/**
 *
 * 充值
 * Created by philip on 3/10/16.
 *
 */
public class ChongZhi extends SuddenDeathBaseTest {
    @Test
    public void chongZhi() {
        this.loadPage(this.backupUrl);
        this.login();

        this.driver.findElement(By.id("li_2")).click();

        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath("//div[@class='tabtil']/ul/li[2]")).getText();
                return text.equalsIgnoreCase("充值");
            }
        });

        this.driver.findElement(By.xpath("//div[@class='tabtil']/ul/li[2]")).click();
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("addrecharge")).isDisplayed();
            }
        });

        String bankName = this.driver.findElement(By.name("wbank")).getAttribute("value");
        assertTrue("没找到银行卡", bankName.contains("中国建设银行"));

        this.driver.findElement(By.id("money1")).sendKeys(this.amount);
        this.driver.findElement(By.id("dealpwd")).sendKeys(this.password);
        this.driver.findElement(By.id("addrecharge")).click();

        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("moneyjine")).isDisplayed();
            }
        });

        this.driver.findElement(By.xpath("//div[@id='cztxZhongTxtDiv2']/b/a")).click();

        try {
            this.driver.switchTo().alert().accept();
        } catch (Exception e) {}

        WebDriver cpPayDriver = this.switchToWindow("通联支付");
        String merchantName = cpPayDriver.findElement(By.xpath("//table/tbody/tr[2]/td[2]")).getText();
        assertTrue("商户名字不正确", merchantName.contains("通联支付"));

        String amount = cpPayDriver.findElement(By.xpath("//table/tbody/tr[5]/td[2]")).getText();
        assertTrue("金额不正确", amount.contains(this.amount));

        cpPayDriver.findElement(By.name("cardNo")).sendKeys(this.card);
        cpPayDriver.findElement(By.name("bpwd")).sendKeys(this.password);
        cpPayDriver.findElement(By.xpath("//table//a[1]")).click();

        WebDriverWait cpPayWait = new WebDriverWait(cpPayDriver, 5000);
        cpPayWait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@class='main']/span")).getText().equalsIgnoreCase("充值成功");
            }
        });

        cpPayDriver.close();

        this.driver = this.switchToWindow("惠金所");

        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("cztxTxt")).isDisplayed();
            }
        });

        this.driver.findElement(By.xpath("//div[@id='cztxTxt']//div[@class='popBox']//a[1]")).click();

        String updatedAccountSize = this.driver.findElement(By.xpath("//table/tbody/tr[4]/td[2]/strong")).getText();
        assertTrue("充值后金额显示不对", updatedAccountSize.contains(Util.getDecimalFormat(this.amount)));
    }
}
