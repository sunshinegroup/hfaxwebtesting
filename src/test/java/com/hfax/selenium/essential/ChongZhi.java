package com.hfax.selenium.essential;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;
import com.hfax.selenium.base.Util;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByLinkText;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 *
 * 充值
 * Created by philip on 3/10/16.
 * Modify by songxq on 12/7/16
 */
public class ChongZhi extends SuddenDeathBaseTest {
	Map<String, String> envVars = System.getenv();
	//个人用户登录
	protected void userLogin(){
			this.loadPage();
	        this.login();
		}
	//个人充值金额
	protected String userAmount(){
		return this.amount;
	}
	//个人交易密码
	protected String userJiaoyimima(){
			return this.jiaoyipassword;
	}
	//个人卡号
	protected String userCard(){
			return this.card;
	}
	//个人绑卡银行名称
	protected String userBankname(){
		return this.hlbankname;
	}
	//个人充值后的资金记录后的收入
	protected String userIncomeAmount(){
		return "10,000.00";
	}
	//个人充值后资金记录后的备注金额
	protected String userRemarkAmount(){
		return "[10000.00元]";
	}
	//验证个人充值后的资金记录
	protected void verifyRecordFunds(){
		boolean flag = true;
		List<WebElement> list = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr"));
		for(int i = 2; i < list.size(); i++){  
        //收入
        String Rechargeamount = this.driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr["+i+"]/td[5]")).getText().trim();
        //时间
        String Rechargetime = this.driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr["+i+"]/td[2]")).getText().trim();
        //操作类型
        String Rechargeaction = this.driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr["+i+"]/td[3]")).getText().trim();
        //备注
        String Rechargeremarks = this.driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr["+i+"]/td[4]/a")).getText().trim();
        if(Rechargeamount.equals(userIncomeAmount())&&Rechargetime.contains(this.getDate(0))&&Rechargeaction.equals("充值成功")&&Rechargeremarks.equals("充值成功,成功充值"+userRemarkAmount()))
        System.out.println("充值金额为："+Rechargeamount);
        System.out.println("充值后显示的时间为："+Rechargetime);
        System.out.println("实际时间为：" + this.getDate(0));
        System.out.println("充值后显示的操作类型为："+Rechargeaction);
        System.out.println("充值后显示的备注为："+Rechargeremarks);
        flag = false;
        break;
		}
		if(flag == true){
			fail("充值金额，时间，操作类型，备注不正确");
		}
	}
    @Test
    public void chongZhi() throws InterruptedException {
    	WebDriver windowIE = null;
    	userLogin();
        
        //点击我的账户
         this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//a[5]"))).click();
        
        //等待页面加载
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@class='hfax-topnav-nav right']//a[5]")).isDisplayed();
            }
        });
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
        	this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//ul[@id='account_leftMenu']//li[3]//a")).isDisplayed();
	             } });
        	 try {
 	  			Thread.sleep(2000);
 	  			System.out.println("点击充值提现前");
 	            } catch (InterruptedException e) {
 	  			// TODO Auto-generated catch block
 	  			e.printStackTrace();
 	            }	
	    	//点击充值提现
	    	this.driver.findElement(By.xpath("//ul[@id='account_leftMenu']//li[3]//a")).click();
	    	 try {
	 	  			Thread.sleep(2000);
	 	  			System.out.println("点击充值提现后");
	 	            } catch (InterruptedException e) {
	 	  			// TODO Auto-generated catch block
	 	  			e.printStackTrace();
	 	            }		
		 }else{

			 this.leftNavigationMenu(3,"充值/提现");
		 }

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath("//ul[@class='tab_top']//li[2]")).getText();
                return text.equalsIgnoreCase("充值");
            }
        });

        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//div[@class='detail_list clearfix']//ul[@class='tab_top']//li[2]")).isDisplayed();
	             } });
        	//点击充值
        	this.driver.findElement(By.xpath("//div[@class='detail_list clearfix']//ul[@class='tab_top']//li[2]")).click();
        	//等待页面加载
        	 this.wait.until(new ExpectedCondition<Boolean>() {
                 @Override
                 public Boolean apply(WebDriver webDriver) {
                     return !webDriver.findElement(By.xpath("//b[@id='bankInput']")).getText().trim().equals("请输入银行卡号");
                 }
             });
        	 
        }else{
        	this.rightTabNavigationMenu(2,"充值");
        }
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("bankInput")).isDisplayed();
            }
        });
        assertTrue("所属银行不正确", this.driver.findElement(By.id("bankInput")).getText().trim().contains(userBankname()));
        
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	 //输入充值金额
        	this.driver.findElement(By.id("money1")).sendKeys(userAmount());
        }else{
        	 //输入充值金额
            this.driver.findElement(By.id("money1")).clear();
            this.driver.findElement(By.id("money1")).sendKeys(userAmount());
        }
        
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	//输入交易密码
//        	this.driver.findElement(By.id("dealpwd")).clear();
        	this.driver.findElement(By.id("dealpwd")).sendKeys(userJiaoyimima());
        }else{
        	this.driver.findElement(By.id("dealpwd")).clear();
            this.driver.findElement(By.id("dealpwd")).sendKeys(userJiaoyimima());
        }
        
        //点击《认证支付服务协议》
        String currentWindow = this.driver.getWindowHandle();
        System.out.println("第一个窗口的信息：" + currentWindow);
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
		  try {
	  			Thread.sleep(2000);
	  			System.out.println("点击《认证支付服务协议》前");
	            } catch (InterruptedException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	            }	  
		 }
        //点击《认证支付服务协议》
		this.driver.findElement(By.xpath("//p[@class='agreement']//a")).click();
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
		  try {
	  			Thread.sleep(2000);
	  			System.out.println("点击《认证支付服务协议》后");
	            } catch (InterruptedException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	            }	  
		 }
				 
		//弹出新窗口
		 Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
		 System.out.println("当前窗口数为："+ handles.size());
		 Iterator<String> it = handles.iterator();
		 while (it.hasNext()) { 
			 String win = (String)(it.next());
			 System.out.println("第二个窗口的信息为：" + win);
			 if (currentWindow.equals(win)){
				 continue;
    		}
		
		  driver.switchTo().window(win);//切换到新窗口
		  this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("html/body/h1/strong")).isDisplayed();
	            }
	        });
		  driver.switchTo().window(win).close();
	
		 }

		this.switchToWindow("惠金所");
		 
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
			 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return webDriver.findElement(By.xpath("//div[@class='detail_tab']//form//a[@id='addrechargePrivate']")).isDisplayed();
		            }
		        });
			 //点击充值
			 this.driver.findElement(By.xpath("//div[@class='detail_tab']//form//a[@id='addrechargePrivate']")).click(); 
			 
		 }else{
			 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return webDriver.findElement(By.id("addrechargePrivate")).isDisplayed();
		            }
		        });
			 //点击充值
			 this.driver.findElement(By.id("addrechargePrivate")).click();
		        
		 }
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.id("moneyjine")).isDisplayed();
	            }
	        });
       this.driver.findElement(By.xpath("//div[@id='cztxZhongTxtDiv2']/b/a")).click();
     
       this.wait.until(new ExpectedCondition<Boolean>() {
           @Override
           public Boolean apply(WebDriver webDriver) {
               return webDriver.getWindowHandles().size() >= 2;
           }
       });
       
      
       for (String handler : this.driver.getWindowHandles()) {
    	   WebDriver wd = this.driver.switchTo().window(handler);
    	   
    	   if (wd.getTitle().equalsIgnoreCase("sender")) {
    		   System.out.println("代码源："+ this.driver.getPageSource());
    		   System.out.println("sender window is present: " + wd.getCurrentUrl());
    		   
    		   WebElement we = wd.switchTo().activeElement();
//    		   System.out.println("!!! " + we.getText());
     		   
    	        try
    	        {	
    	    	       FluentWait<WebDriver> w = new WebDriverWait(wd, 100).withTimeout(10, TimeUnit.SECONDS).pollingEvery(100, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);
        	        w.until(new ExpectedCondition<Boolean>() {
        	            @Override
        	            public Boolean apply(WebDriver webDriver) {
        	                return webDriver.switchTo().alert() != null;
        	            }
        	        });
    	        	  wd.switchTo().alert().accept();
    	    		   break;
    	        }
    	        catch (Exception e)
    	        {
    	        	Thread.sleep(3000);
    	            System.out.println("no alert......");
    	            break;
    	        }
    		  
    	       
    	   }
   	}
       
//     this.wait.until(ExpectedConditions.numberOfWindowsToBe(2));

        WebDriver cpPayDriver = this.switchToWindow("通联支付");
        cpPayDriver.manage().window().maximize();
//      FluentWait<WebDriver> cpPayWait = new WebDriverWait(cpPayDriver, 10000).withTimeout(10, TimeUnit.SECONDS).pollingEvery(100, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);    
        WebDriverWait waitPayDriver = new WebDriverWait(cpPayDriver, 5);
        while(true){
        	if(cpPayDriver.findElement(By.xpath("//table/tbody/tr[2]/td[2]")).isDisplayed()){
        	  break;	
        	}
        }
        waitPayDriver.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table/tbody/tr[2]/td[2]")));
       
        String merchantName = cpPayDriver.findElement(By.xpath("//table/tbody/tr[2]/td[2]")).getText();
        assertTrue("商户名字不正确", merchantName.contains("通联支付"));

        String amount = cpPayDriver.findElement(By.xpath("//table/tbody/tr[5]/td[2]")).getText();
        assertTrue("金额不正确", amount.contains(userAmount()));

        cpPayDriver.findElement(By.name("cardNo")).sendKeys(userCard());
        cpPayDriver.findElement(By.name("bpwd")).sendKeys(userJiaoyimima());
        cpPayDriver.findElement(By.xpath("//table//a[1]")).click();
        try {
			Thread.sleep(3000);
          } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
          }	 
        waitPayDriver.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@class='main']/span")).isDisplayed();
            }
        });
        assertTrue("充值不成功!!!",this.driver.findElement(By.xpath("//div[@class='main']/span")).getText().trim().equalsIgnoreCase("充值成功"));
        //点击去投资
        this.driver.findElement(By.xpath("//div[@class='main']/a")).click();
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
	  			while(true){
	  				
	  				if(this.driver.findElement(By.xpath(".//*[@id='tab_7']")).getAttribute("class").contains("active")){
	  					System.out.println("充值后去投资正确跳转！");
	  					break;
	  				}
	  			}
	            
		 }else{
        //等待惠理财tab激活
        waitPayDriver.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='tab_7']")).isDisplayed();
            }
        });
        assertTrue("充值后去投资没有正确跳转到惠理财", this.driver.findElement(By.xpath(".//*[@id='tab_7']")).getAttribute("class").contains("active"));
		 }
        cpPayDriver.close();

        this.driver = this.switchToWindow("惠金所");

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("cztxTxt")).isDisplayed();
            }
        });

        this.driver.findElement(By.xpath("//div[@id='cztxTxt']//div[@class='popBox']//a[1]")).click();
        
        //等待资金记录
        this.wait.until(new ExpectedCondition<Boolean>() {
          @Override
          public Boolean apply(WebDriver webDriver) {
              return webDriver.findElement(By.xpath("//ul[@class='tab_top']")).isDisplayed();
          }
      });

        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
       	this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='payAndDraw']//a")).isDisplayed();
	             } });
	    	//点击充值提现
	    	this.driver.findElement(By.xpath("//li[@id='payAndDraw']//a")).click();
		 }else{

			 this.leftNavigationMenu(3,"充值/提现");
		 }
        //等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
          @Override
          public Boolean apply(WebDriver webDriver) {
              return webDriver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td")).isDisplayed();
          } });
        
        //验证资金记录
       verifyRecordFunds();
        
        
    }
}
