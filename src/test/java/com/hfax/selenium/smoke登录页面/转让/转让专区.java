package com.hfax.selenium.smoke登录页面.转让;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.base.BaseTest;
/**
 * 
 * @author changwj
 * @Description	转让专区的登录    立即转让 ,转让截止
 */
public class 转让专区  extends BaseTest {
	
	public void  转让专区登录(){
		this.loadPage();
		this.login();
		this.navigateToPage("转让专区");
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[3]/div[1]/span")));
        String text = this.driver.findElement(By.xpath("html/body/div[3]/div[1]/span")).getText().replaceAll(">", "");
        assertTrue("获取转让专区页面失败", "转让专区".equalsIgnoreCase(text));
        List<WebElement> findElements = this.driver.findElements(By.xpath("//div[@class='listTabLeft']"));
        assertTrue("获取转让专区页面失败", findElements.size() > 0);
        for (int i = 0; i < findElements.size(); i++) {
			assertTrue("转让专区有误", !"".equals(findElements.get(i).getText().trim()));
		}
		
	}
	
	@Test
	public void 转让截止(){
		转让专区登录();
		List<WebElement> ids = this.driver.findElements(By.linkText("转让成功"));
		ids.get(1).click();
		String defaultWindow = driver.getWindowHandle();
		Set<String> windowHandles = this.driver.getWindowHandles();
		for (String str : windowHandles) {
			if (!defaultWindow.equalsIgnoreCase(str)) {
				driver.switchTo().window(str);
				break;
			}
		}

		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[5]/div[2]/b/b/div/div[2]/div/i")));
		String text = this.driver.findElement(By.xpath("html/body/div[5]/div[2]/b/b/div/div[2]/div/i")).getText();
		assertTrue("获取转让截止页面失败",text.equals("转让成功"));
	}
	@Test 
	public void 立即转让(){
		转让专区登录();
		List<WebElement> ids = this.driver.findElements(By.linkText("立即投资"));
		ids.get(0).click();
	}
	//小于起投金额
	@Test
	public void 小于起投金额(){
		立即转让();
		WebDriver popupDriver = this.switchToWindow("惠金所", true);
		popupDriver.findElement(By.id("auctionPrice")).sendKeys("100");
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='p1']")));
		String text = this.driver.findElement(By.xpath(".//*[@id='p1']")).getText();
		assertTrue("投资金额错误",text.equals("起投金额为1000元"));
	}
	//超过剩余可投金额
	@Test
	public void 超过剩余可投金额(){
		立即转让();
		WebDriver popupDriver = this.switchToWindow("惠金所", true);
		popupDriver.findElement(By.id("auctionPrice")).sendKeys("1200");
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='p1']")));
		String text = this.driver.findElement(By.xpath(".//*[@id='p1']")).getText();
		assertTrue("投资金额错误",text.equals("投资金额不可以超过剩余可投金额！"));
	}
	//转让成功  这个首先是得买完之后转让
//	@Test
//	public void 转让投资成功(){
//		立即转让();
//	}
}
