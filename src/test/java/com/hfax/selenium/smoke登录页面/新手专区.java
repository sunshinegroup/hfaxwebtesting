package com.hfax.selenium.smoke登录页面;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 新手专区  extends 投资理财{
	
	public void 新手专区登录(){
		this.loadPage();
		this.login();
		this.navigateToPage("新手专区");
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[3]/div[1]/span")));
      
        String text = this.driver.findElement(By.xpath("html/body/div[3]/div[1]/span")).getText().replaceAll(">", "");
        assertTrue("新手专区页失败", "新手专区产品列表".equalsIgnoreCase(text));
        List<WebElement> findElements = this.driver.findElements(By.xpath("//a[@class='listBox-title']"));
        assertTrue("获取阳光惠理财失败", findElements.size() > 0);
        //登录情况下进行投资
        List<WebElement> ids = this.driver.findElements(By.linkText("立即投资"));
		ids.get(0).click();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[5]/div[1]/a[2]")));
		String text2 = this.driver.findElement(By.xpath("html/body/div[5]/div[1]/a[2]")).getText();
		assertTrue("获取可投页面失败", "我要理财".equals(text2));
		
	}
	@Test
	public void 投资金额小于起投金额(){
		新手专区登录();
		moneyless();
	}
	@Test
	public void 投资金额超过项目金额(){
		新手专区登录();
		this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='errorMsg']")).isDisplayed();
            }
        });
	}
	@Test
	public void 账户余额不足(){
		新手专区登录();
		nomoney();
	}
	@Test
	public void 投资金额成功(){
		新手专区登录();
		success();
	}
//	@Test
//	public void  使用红包投资成功(){
//		新手专区登录();
//		redbagSuccess();
//	}
//	@Test
//	public void 投资金额与红包金额相等(){
//		新手专区登录();
//		moneyEqualredBag();
//	}
	
	
	
}
