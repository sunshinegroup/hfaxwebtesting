package com.hfax.selenium.smoke登录页面;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.base.BaseTest;

public class Common extends BaseTest{
	public void common(String amount){
		this.driver.findElement(By.id("amount")).sendKeys(amount);
	}
	public void 去投资(){
		this.driver.findElement(By.id("agre")).click();
		this.driver.findElement(By.xpath(".//*[@id='btnsave']/i")).click();
	}
	
	//投资金额小于起投金额   起投金额为0
	public void  moneyless(){
		common("99");
		去投资();
		this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("errorMsg")).isDisplayed();
            }
        });
	}
	
	//超额 .//*[@id='lackmoney']
	public void moneyabove(){
		common("10000000");
		去投资();
		this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                String text = webDriver.findElement(By.xpath(".//*[@id='investFail3']/div[2]/h2/strong")).getText();
	                return text.equalsIgnoreCase("投资未成功");
	            }
	        });
		this.driver.findElement(By.xpath(".//*[@id='investFail3']/div[2]/h2/span")).click();
	}
	
	//账户金额为0，未充值的情况下,余额不足   
	public void nomoney(){
		common("10000");
		去投资();
		this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='errorMsg']")).isDisplayed();
            }
        });
	}
	
	//投资金额成功   
	public void success(){
		common("100");
		去投资();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='marketingbanner']/div[1]")));
		
		String text = this.driver.findElement(By.xpath(".//*[@id='marketingbanner']/div[1]")).getText();
		assertTrue("获取产品信息页失败",text.equalsIgnoreCase("产品信息"));
		
		//确定投资
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("besureInvest")));
		this.driver.findElement(By.id("besureInvest")).click();
		
		this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath(".//*[@id='investSuccess']/div[2]/h2/strong")).getText();
                return text.equalsIgnoreCase("投资成功");
            }
        });
		//关闭按钮
		 this.driver.findElement(By.xpath(".//*[@id='investSuccess']/div[2]/b/a")).click();
	}
	
	 //默认勾上使用红包 投资金额必须大于红包金额(必须有红包)
	public void redbagSuccess(){
		common("1000");
		去投资();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='marketingbanner']/div[1]")));
		
		String text = this.driver.findElement(By.xpath(".//*[@id='marketingbanner']/div[1]")).getText();
		assertTrue("获取产品信息页失败",text.equalsIgnoreCase("产品信息"));
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("besureInvest")));
		this.driver.findElement(By.id("besureInvest")).click();
		this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath(".//*[@id='investSuccess']/div[2]/h2/strong")).getText();
                return text.equalsIgnoreCase("投资成功");
            }
        });
	}
	
	//必须在有红包的情况,投资金额与红包金额相等
	public void moneyEqualredBag(){
		common("100");
		去投资();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='marketingbanner']/div[1]")));
		
		String text = this.driver.findElement(By.xpath(".//*[@id='marketingbanner']/div[1]")).getText();
		assertTrue("获取产品信息页失败",text.equalsIgnoreCase("产品信息"));
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("besureInvest")));
		this.driver.findElement(By.id("besureInvest")).click();
		
		assertTrue("投资失败", this.driver.switchTo().alert().getText().contains("投资金额必须大于0"));
	    this.driver.switchTo().alert().accept();
	}
}
