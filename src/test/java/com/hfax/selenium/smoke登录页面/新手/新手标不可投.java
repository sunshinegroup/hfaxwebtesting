package com.hfax.selenium.smoke登录页面.新手;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

public class 新手标不可投 extends 新手专区 {
	@Test
	public void 已投过新手标(){
		新手专区登录();
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath(".//*[@id='errorMsg']")).isDisplayed();
			}
		});
		assertTrue("投资页面出现错误",this.driver.findElement(By.xpath(".//*[@id='errorMsg']"))!=null);
	}

}
