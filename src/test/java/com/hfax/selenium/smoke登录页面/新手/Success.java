package com.hfax.selenium.smoke登录页面.新手;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
public class Success extends 新手专区{
	//新手投标如果投过一次标，成功一次,所有标以后都不允许投
	@Test
	public void 投资金额成功(){
		新手专区登录();
		common("100");
		去投资();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='marketingbanner']/div[1]")));
		
		String text = this.driver.findElement(By.xpath(".//*[@id='marketingbanner']/div[1]")).getText();
		assertTrue("获取产品信息页失败",text.equalsIgnoreCase("产品信息"));
		
		//确定投资
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("besureInvest")));
		this.driver.findElement(By.id("besureInvest")).click();
		
		this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath(".//*[@id='investSuccess']/div[2]/h2/strong")).getText();
                return text.equalsIgnoreCase("投资成功");
            }
        });
		//关闭按钮
		 this.driver.findElement(By.xpath(".//*[@id='investSuccess']/div[2]/b/a")).click();
	}
	
}
