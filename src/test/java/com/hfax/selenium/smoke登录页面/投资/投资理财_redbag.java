package com.hfax.selenium.smoke登录页面.投资;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class 投资理财_redbag extends 投资理财{
	
	
	
	// 必须在有红包的情况,投资金额与红包金额相等
	@Test
	public void 投资金额与红包金额相等() {
		投资理财登录();
		common("100");
		去投资();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='marketingbanner']/div[1]")));

		String text = this.driver.findElement(By.xpath(".//*[@id='marketingbanner']/div[1]")).getText();
		assertTrue("获取产品信息页失败", text.equalsIgnoreCase("产品信息"));
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("besureInvest")));
		this.driver.findElement(By.id("besureInvest")).click();

		assertTrue("投资失败", this.driver.switchTo().alert().getText().contains("投资金额必须大于0"));
		this.driver.switchTo().alert().accept();
	}

	
	
	// 默认勾上使用红包 投资金额必须大于红包金额(必须有红包)
	@Test
	public void 使用红包投资成功() {
		投资理财登录();
		common("1000");
		去投资();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='marketingbanner']/div[1]")));

		String text = this.driver.findElement(By.xpath(".//*[@id='marketingbanner']/div[1]")).getText();
		assertTrue("获取产品信息页失败", text.equalsIgnoreCase("产品信息"));
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("besureInvest")));
		this.driver.findElement(By.id("besureInvest")).click();
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				String text = webDriver.findElement(By.xpath(".//*[@id='investSuccess']/div[2]/h2/strong")).getText();
				return text.equalsIgnoreCase("投资成功");
			}
		});
	}
}
