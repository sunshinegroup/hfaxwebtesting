package com.hfax.selenium.smoke登录页面.投资;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.base.BaseTest;

/**
 * 
 * @author changwj
 * @Description qa2环境测试 登录下的投资理财
 */
public class 投资理财 extends BaseTest {
	public void common(String amount) {
		this.driver.findElement(By.id("amount")).sendKeys(amount);
	}

	public void 去投资() {
		this.driver.findElement(By.id("agre")).click();
		this.driver.findElement(By.xpath(".//*[@id='btnsave']/i")).click();
	}

	public void 投资理财登录() {
		this.loadPage();
		this.login();
		this.navigateToPage("投资理财");
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[3]/div[1]/span")));
		String text = this.driver.findElement(By.xpath("html/body/div[3]/div[1]/span")).getText().replaceAll(">", "");
		assertTrue("获取首页投资理财失败", "惠理财产品列表".equalsIgnoreCase(text));
		List<WebElement> findElements = this.driver.findElements(By.xpath("//a[@class='listBox-title']"));
		assertTrue("获取阳光惠理财失败", findElements.size() > 0);

		// 登录情况下进行投资
		List<WebElement> ids = this.driver.findElements(By.linkText("立即投资"));
		ids.get(1).click();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[5]/div[1]/a[2]")));
		String text2 = this.driver.findElement(By.xpath("html/body/div[5]/div[1]/a[2]")).getText();
		assertTrue("获取可投页面失败", "我要理财".equals(text2));

	}

	@Test
	// 投资金额小于起投金额 起投金额为0
	public void 投资金额小于起投金额() {
		投资理财登录();
		common("99");
		去投资();
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return webDriver.findElement(By.id("errorMsg")).isDisplayed();
			}
		});
	}

	// 超额 
	@Test
	public void 投资金额超过剩余可投金额() {
		投资理财登录();
		common("10000000");
		去投资();
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				String text = webDriver.findElement(By.xpath(".//*[@id='investFail3']/div[2]/h2/strong")).getText();
				return text.equalsIgnoreCase("投资未成功");
			}
		});
		this.driver.findElement(By.xpath(".//*[@id='investFail3']/div[2]/h2/span")).click();
	}

	// 账户,余额不足
	@Test
	public void 余额不足() {
		投资理财登录();
		common("20000");
		去投资();
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath(".//*[@id='errorMsg']")).isDisplayed();
			}
		});
	}


	
	// 投资金额成功
	@Test
	public void 投资金额成功() {
		投资理财登录();
		common("100");
		去投资();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='marketingbanner']/div[1]")));
		String text = this.driver.findElement(By.xpath(".//*[@id='marketingbanner']/div[1]")).getText();
		assertTrue("获取产品信息页失败", text.equalsIgnoreCase("产品信息"));

		// 确定投资
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("besureInvest")));
		this.driver.findElement(By.id("besureInvest")).click();

		  
		// 关闭按钮
		this.driver.findElement(By.xpath(".//*[@id='investSuccess']/div[2]/b/a")).click();
	}
}
