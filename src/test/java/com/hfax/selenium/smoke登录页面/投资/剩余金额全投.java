package com.hfax.selenium.smoke登录页面.投资;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 剩余金额全投  extends 投资理财{
	
	
		//剩余金额全投   当剩余可投金额小于等于最少投资金额  出现剩余金额全投
		@Test
		public void  全投(){
			this.loadPage();
			this.login();
			this.navigateToPage("投资理财");
			this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[3]/div[1]/span")));
			String text = this.driver.findElement(By.xpath("html/body/div[3]/div[1]/span")).getText().replaceAll(">", "");
			assertTrue("获取首页投资理财失败", "惠理财产品列表".equalsIgnoreCase(text));
			List<WebElement> findElements = this.driver.findElements(By.xpath("//a[@class='listBox-title']"));
			assertTrue("获取阳光惠理财失败", findElements.size() > 0);

			// 登录情况下进行投资
			List<WebElement> ids = this.driver.findElements(By.linkText("立即投资"));
			ids.get(8).click();
			this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[5]/div[1]/a[2]")));
			String text2 = this.driver.findElement(By.xpath("html/body/div[5]/div[1]/a[2]")).getText();
			assertTrue("获取可投页面失败", "我要理财".equals(text2));
			this.driver.findElement(By.id("agre")).click();
			this.driver.findElement(By.xpath(".//*[@id='sysave']/i")).click();
			this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("besureInvest")));
			//确定投资
			this.driver.findElement(By.id("besureInvest")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver webDriver) {
					String text = webDriver.findElement(By.xpath(".//*[@id='investSuccess']/div[2]/h2/strong")).getText();
					return text.equalsIgnoreCase("投资成功");
				}
			});
			// 关闭按钮
			this.driver.findElement(By.xpath(".//*[@id='investSuccess']/div[2]/b/a")).click();
		}

}
