package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

public class 惠聚财_综合管理系统  extends BaseTest{
	@Test
    public void 综合管理系统对账流程() throws Exception{
		//登录综合管理系统
	    this.driver.get(this.yueUrl);
	    assertTrue("读取主页失败", this.driver.getTitle().equals("惠金所综合管理系统"));
	
	    this.driver.findElement(By.id("username")).sendKeys(this.yueUsername);
	    this.driver.findElement(By.id("password")).sendKeys(this.yuePassword);
	    this.driver.findElement(By.id("reSetStatus")).click();
	    this.driver.findElement(By.id("submit")).click();
	
	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='menu']/li[6]/a")));
	    
	    //执行任务1
	    //点击执行任务
        Actions builder = new Actions(driver);
        builder.moveToElement(driver.findElement(By.xpath(".//*[@id='menu']/li[6]/a"))).perform();
	    this.driver.findElement(By.xpath(".//*[@id='menu']/li[6]/ul/li[1]/a")).click();
	    
	    //等待页面加载
	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("tkgpid")));	    
	    //输入批量交易组
	    this.driver.findElement(By.id("tkgpid")).sendKeys("061");	    
	    //输入批量交易处理码
	    this.driver.findElement(By.id("tkprcd")).sendKeys("reblac");
	    //点击获取任务
	    this.driver.findElement(By.id("chck_btn")).click();
	    //等待任务出现
	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("tkname")));	   
	    //点击执行任务
	    this.driver.findElement(By.id("do_task_btn")).click();
	    Thread.sleep(3000);
	    
	    //对账
	    //点击对账信息查询
        builder.moveToElement(driver.findElement(By.xpath(".//*[@id='menu']/li[10]/a"))).perform();
        this.driver.findElement(By.xpath(".//*[@id='menu']/li[10]/ul/li[1]/a")).click();
        
        //等待页面加载
        new WebDriverWait(this.driver,10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='cust-form']/div[10]/button[1]")));
        new WebDriverWait(this.driver,10).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='blockUI blockOverlay']")));
 
	    //验证结果
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				String text;
				try{
			        //点击查询
			        driver.findElement(By.xpath(".//*[@id='cust-form']/div[9]/button[1]")).click();
					text= webDriver.findElement(By.xpath(".//*[@id='datatable_prod']/tbody/tr/td[1]")).getText();
				}catch(Exception e){
					text= webDriver.findElement(By.xpath(".//*[@id='main-content']/div/div/div[1]/div/span[2]")).getText();
				}
				return text.contains("Yuetestproject00");
			}
		});
		
		//点击可生产
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='blockUI blockOverlay']")));
		this.driver.findElement(By.xpath(".//*[@id='datatable_prod']/tbody/tr/td[18]/a")).click();
		
		//执行任务2
		//点击执行任务
        builder.moveToElement(driver.findElement(By.xpath(".//*[@id='menu']/li[6]/a"))).perform();
	    this.driver.findElement(By.xpath(".//*[@id='menu']/li[6]/ul/li[1]/a")).click();
	    
	    //等待页面加载
	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("tkgpid")));	    
	    //输入批量交易组
	    this.driver.findElement(By.id("tkgpid")).sendKeys("061");	    
	    //输入批量交易处理码
	    this.driver.findElement(By.id("tkprcd")).sendKeys("repgen");
	    //点击获取任务
	    this.driver.findElement(By.id("chck_btn")).click();
	    //等待任务出现
	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("tkname")));	   
	    //点击执行任务
	    this.driver.findElement(By.id("do_task_btn")).click();
	    Thread.sleep(3000);
	}
}
