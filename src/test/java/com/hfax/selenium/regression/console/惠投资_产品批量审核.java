package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 惠投资_产品批量审核  extends ConsoleBaseTest{
	  @Test
	    public void 批量审核() throws Exception {
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[3]/div/span[4]")));
	        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[3]/div/span[4]")).click();

	        //等待页面加载
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                driver.switchTo().defaultContent();
	                driver.switchTo().frame(0);
	                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
	                return text2.equalsIgnoreCase("1");
	            }
	        });

	        //检查新增项目位置
	        String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
	        if (textSearch.compareTo("projecttest001150") != 0) {
	            //查询项目
	            this.driver.findElement(By.id("subject_name")).sendKeys("projecttest0011");
	            this.driver.findElement(By.linkText("查询")).click();

	            this.wait.until(new ExpectedCondition<Boolean>() {
	                @Override
	                public Boolean apply(WebDriver webDriver) {
	                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
	                    return text2.equalsIgnoreCase("projecttest001150");
	                }
	            });

	            String text3 = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
	            assertTrue("查询功能不正常", text3.equals("projecttest001150"));
	        }
	        
	        //勾选项目
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //点击批量通过
	        this.driver.findElement(By.linkText("批量通过")).click();
	        
	        //处理弹窗
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	        
	        //下一页
	        this.driver.findElement(By.xpath(".//span[@class='l-btn-empty pagination-next']")).click();
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        
	        //第二页
	        //等待页面
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
                    return text2.equalsIgnoreCase("projecttest001140");
                }
            });
            
	        //勾选项目
            this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //点击批量通过
	        this.driver.findElement(By.linkText("批量通过")).click();
	        
	        //处理弹窗
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	        
	        //下一页
	        this.driver.findElement(By.xpath(".//span[@class='l-btn-empty pagination-next']")).click();
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        
	        //第三页
	        //等待页面
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
                    return text2.equalsIgnoreCase("projecttest001130");
                }
            });
            
	        //勾选项目
            this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //点击批量通过
	        this.driver.findElement(By.linkText("批量通过")).click();
	        
	        //处理弹窗
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	        
	        //下一页
	        this.driver.findElement(By.xpath(".//span[@class='l-btn-empty pagination-next']")).click();
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        
	        //第四页
	        //等待页面
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
                    return text2.equalsIgnoreCase("projecttest001120");
                }
            });
            
	        //勾选项目
            this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //点击批量通过
	        this.driver.findElement(By.linkText("批量通过")).click();
	        
	        //处理弹窗
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	        
	        //下一页
	        this.driver.findElement(By.xpath(".//span[@class='l-btn-empty pagination-next']")).click();
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        
	        //第五页
	        //等待页面
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
                    return text2.equalsIgnoreCase("projecttest001110");
                }
            });
            
	        //勾选项目
            this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //点击批量通过
	        this.driver.findElement(By.linkText("批量通过")).click();
	        
	        //处理弹窗
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	        
	        
	  }
}
