package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 惠投资_还款计划编辑   extends ConsoleBaseTest {
	@Test
    public void 还款计划编辑() throws Exception{
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[10]/div/span[4]")));
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[10]/div/span[4]")).click();

        //等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
                return text2.equalsIgnoreCase("1");
            }
        });

        //检查新增项目位置
        String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
        if (textSearch.compareTo(this.huiProjectName) != 0) {
            //查询项目
            this.driver.findElement(By.id("subject_name")).sendKeys(this.huiProjectName);
            this.driver.findElement(By.linkText("查询")).click();

            final String consoleName = this.huiProjectName;
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
                    return text2.equalsIgnoreCase(consoleName);
                }
            });

            String text3 = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
            assertTrue("查询功能不正常", text3.equals(this.huiProjectName));
        }
        
        //勾选项目
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")).click();
        
        //批量起息
        this.driver.findElement(By.linkText("批量起息")).click();

        //等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[16]/div/a[1]")).getText();
                return text5.equalsIgnoreCase("已起息");
            }
        });

        //勾选项目
        this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")).click();
        
        //批量敲出
        this.driver.findElement(By.linkText("批量敲出")).click();
     
        //等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[16]/div/a[2]")).getText();
                return text5.equalsIgnoreCase("已敲出");
            }
        });
	}
}
