package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * @author changwj
 * @Description 流标
 */
public class 流标 extends 项目查询 {
    //流标
    @Test
    public void 流标成功() {
    	项目查询信息();

        //判断状态
        assertTrue("状态不是投标中，不允许流标", findValueAtIndexRow(0, 14).getText().equals("投标中"));
        findValueAtIndexRow(0, 0).click();

        //点击流标
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        driver.findElement(By.linkText("流标")).click();
       
        //确定流标
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		driver.findElement(By.linkText("确定")).click();
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
		
        
    }
}