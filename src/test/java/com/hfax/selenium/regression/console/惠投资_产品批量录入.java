package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 惠投资_产品批量录入 extends ConsoleBaseTest{
    private void clickDownArrow(int rowNumber, int columnNumber) {
        this.driver.findElement(By.xpath(".//*[@id='edit_table']/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
    }

    private WebElement findTextField(int rowNumber, int columnNumber) {
        return this.driver.findElement(By.xpath(".//*[@id='edit_table']/tbody/tr[" + rowNumber +
                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
    }

    protected String lookupName() {
        return "中关村1号";
    }
    Map<String, String> envVars = System.getenv();
	@Test
	public void 批量录入() throws SQLException{
		//获取企业cd
	    Connection con = null;// 创建一个数据库连接
	    PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
	    ResultSet result = null;// 创建一个结果集对象
	    try
	    {
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        System.out.println("开始尝试连接数据库！");
	        String url = "jdbc:oracle:" + "thin:@10.10.84.48:1522/prod";//地址:库名
	        String user = "system";// 用户名
	        String password = "oracle";// 密码
	        con = DriverManager.getConnection(url, user, password);// 获取连接
	        System.out.println("连接成功！");
	        String sql = "select member_cd,account_no from sunif.sif_member where member_name='测试五'";// 预编译语句
	        pre = con.prepareStatement(sql);// 实例化预编译语句
	        result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数
	        while (result.next()){
	        String cd = result.getString("member_cd");
	        String no = result.getString("account_no");
	        System.out.println(cd);
	        System.out.println(no);

        
		    //编辑xls
	        String fileName = "gf_kd3333.xlsx";
	        ClassLoader loader = ConsoleBaseTest.class.getClassLoader();
	        File file = new File(loader.getResource(fileName).getFile());
	        assertTrue("无法读取文件: " + fileName, file != null);

	        String filePath = file.getAbsolutePath();
	        assertTrue("文件路径为空: " + fileName, filePath.length() > 0);
//	        String filePath = "D:\\webdirver\\hfaxwebtesting\\src\\test\\Resources\\gf_kd3333.xlsx";
	        
	        String img1 = this.file("image1.jpg");
	        String img2 = this.file("image2.jpg");
	        try {   
	            XSSFWorkbook xwb = new XSSFWorkbook(new FileInputStream(filePath));  
	  
		        XSSFSheet xSheet = xwb.getSheetAt(0);  
		        for(int j = 1; j < 51; j++){
		        XSSFRow xRow = xSheet.createRow(j);    
		        
		        xRow.createCell(0).setCellValue(huiBatchName + 1 + j); 
		        xRow.createCell(1).setCellValue(businessName); 
		        xRow.createCell(2).setCellValue(businessUsername);
		        xRow.createCell(3).setCellValue("自动化测试标的abc123");  
		        xRow.createCell(4).setCellValue("自动化测试");  
		        xRow.createCell(5).setCellValue("90");  
		        xRow.createCell(6).setCellValue("5.00%/年预期固定收益率+0.00%/年~35.00%/年预期浮动收益率");  
		        xRow.createCell(7).setCellValue("20160808");
		        xRow.createCell(8).setCellValue("20160815"); 
		        xRow.createCell(9).setCellValue("7"); 
		        xRow.createCell(10).setCellValue("自动化测试"); 
		        xRow.createCell(11).setCellValue("0.1"); 
		        xRow.createCell(12).setCellValue("5.00%/年预期固定收益率+0.00%/年~35.00%/年预期浮动收益率"); 
		        xRow.createCell(13).setCellValue("0"); 
		        xRow.createCell(14).setCellValue("20160815"); 
		        xRow.createCell(15).setCellValue("自动化测试"); 
		        xRow.createCell(16).setCellValue("5"); 
		        xRow.createCell(17).setCellValue("自动化测试123"); 
		        xRow.createCell(18).setCellValue("4001"); 
		        xRow.createCell(19).setCellValue("100"); 
		        xRow.createCell(20).setCellValue("上海中关惠资产管理有限公司"); 
		        xRow.createCell(21).setCellValue("1560120540002950"); 
		        xRow.createCell(22).setCellValue("南京银行珠江支行"); 
		        xRow.createCell(23).setCellValue("365"); 
		        xRow.createCell(24).setCellValue("100"); 
		        xRow.createCell(25).setCellValue("20160807"); 
		        xRow.createCell(26).setCellValue("2300"); 
		        xRow.createCell(27).setCellValue(cd); 
		        xRow.createCell(28).setCellValue(no); 
		        xRow.createCell(29).setCellValue("1"); 
		        xRow.createCell(30).setCellValue("1"); 
		        xRow.createCell(31).setCellValue("1"); 
		        xRow.createCell(32).setCellValue("10000"); 
		        xRow.createCell(33).setCellValue("1"); 
		        xRow.createCell(34).setCellValue("0"); 
		        xRow.createCell(35).setCellValue("热销"); 
		        xRow.createCell(36).setCellValue("热销"); 
		        xRow.createCell(37).setCellValue("2"); 
		        xRow.createCell(38).setCellValue("22"); 
		        xRow.createCell(39).setCellValue("3"); 
		        xRow.createCell(40).setCellValue("33"); 
		        xRow.createCell(41).setCellValue("1000"); 
		        xRow.createCell(42).setCellValue("N"); 
		        xRow.createCell(43).setCellValue(img1); 
		        xRow.createCell(44).setCellValue(img2); 
		        xRow.createCell(45).setCellValue("10000"); 
		        xRow.createCell(46).setCellValue("95"); 
		        xRow.createCell(47).setCellValue("60"); 
		        xRow.createCell(48).setCellValue("5"); 
		        xRow.createCell(49).setCellValue("35"); 
		        xRow.createCell(50).setCellValue("35"); 
		        xRow.createCell(51).setCellValue("266.15"); 
		        xRow.createCell(52).setCellValue("276.8"); 
		        xRow.createCell(53).setCellValue("239.54"); 
		        xRow.createCell(54).setCellValue("154.37"); 
		        xRow.createCell(55).setCellValue("4"); 
		        xRow.createCell(56).setCellValue("10"); 
		        xRow.createCell(57).setCellValue("42"); 
		        xRow.createCell(58).setCellValue("5"); 
		        xRow.createCell(59).setCellValue("10"); 
		        }
	            FileOutputStream out = new FileOutputStream(filePath);  
	            xwb.write(out);  
	            out.close();  
	  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }
	        }
	    	   
	    }
	    catch (Exception e)
	    {
	        e.printStackTrace();
	    }
	    finally
	    {
	        try
	        {
	            // 逐一将上面的几个对象关闭，因为不关闭的话会影响性能、并且占用资源
	            // 注意关闭的顺序，最后使用的最先关闭
	            if (result != null)
	                result.close();
	            if (pre != null)
	                pre.close();
	            if (con != null)
	                con.close();
	            System.out.println("数据库连接已关闭！");
	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
	    }
	    
	    //等待页面加载
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[2]/div/span[4]")));
        
        //点击产品录入
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[2]/div/span[4]")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[1]/span[1]")));

        // 等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.linkText("单个新增")).getText();
                return text2.equalsIgnoreCase("单个新增");
            }
        });
        
        //点击批量新增
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        this.driver.findElement(By.linkText("批量新增")).click();
        
        //上传文件
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("file_name")));
        String fileName = "gf_kd3333.xlsx";
        ClassLoader loader = ConsoleBaseTest.class.getClassLoader();
        File file = new File(loader.getResource(fileName).getFile());
        assertTrue("无法读取文件: " + fileName, file != null);

        String filePath = file.getAbsolutePath();
        assertTrue("文件路径为空: " + fileName, filePath.length() > 0);
//        String filePath = "D:\\webdirver\\hfaxwebtesting\\src\\test\\Resources\\gf_kd3333.xlsx";
        
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("file_name")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("file_name")));
        WebElement uploadField = this.driver.findElement(By.id("file_name"));
        ((JavascriptExecutor)this.driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", uploadField);
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("file_name")));
        uploadField.clear();

        
        ((JavascriptExecutor)this.driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", this.driver.findElement(By.id("fileToUpload")));
        this.driver.findElement(By.id("fileToUpload")).sendKeys(filePath);
        this.driver.findElement(By.xpath(".//*[@id='fileup']/button")).click();
        
        //弹窗确认
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
        this.driver.findElement(By.linkText("确定")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	}
	
}
	    
