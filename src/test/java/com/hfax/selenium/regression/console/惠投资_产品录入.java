package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 惠投资_产品录入 extends ConsoleBaseTest {
    private void clickDownArrow(int rowNumber, int columnNumber) {
        this.driver.findElement(By.xpath(".//*[@id='edit_table']/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
    }

    private WebElement findTextField(int rowNumber, int columnNumber) {
        return this.driver.findElement(By.xpath(".//*[@id='edit_table']/tbody/tr[" + rowNumber +
                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
    }

    protected String lookupName() {
        return "中关村1号";
    }
    Map<String, String> envVars = System.getenv();
    
    @Test
    public void 单个新增() throws Exception{
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[2]/div/span[4]")));
        
        //点击产品录入
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[2]/div/span[4]")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[1]/span[1]")));

        // 等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.linkText("单个新增")).getText();
                return text2.equalsIgnoreCase("单个新增");
            }
        });
        
        //点击增加
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        this.driver.findElement(By.linkText("单个新增")).click();
        
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='edit_table']/tbody/tr[1]/td[1]")));
        String text2 = driver.findElement(By.xpath(".//*[@id='edit_table']/tbody/tr[1]/td[1]")).getText();
        assertTrue("新增项目信息页面加载失败", text2.equalsIgnoreCase("产品类型："));
        
        //产品类型
        this.clickDownArrow(1, 2);
        WebElement textField = this.findTextField(1, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //模板类型
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='edit_table']/tbody/tr[2]/td[1]")));
        this.clickDownArrow(2, 2);
        textField = this.findTextField(2, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //公司名称（受托人）
        this.driver.findElement(By.id("company_name")).sendKeys(businessName);
        
        //惠金所用户名
        this.driver.findElement(By.id("member_name")).sendKeys(businessUsername);
        
        //定向委托投资标的
        this.driver.findElement(By.id("invest_subject")).sendKeys("自动化测试标的abc123");
        
        //发行机构
        this.driver.findElement(By.id("issue_mech")).sendKeys("自动化测试");
        
        //定向委托投资标的存续期
        this.driver.findElement(By.id("duration_period")).sendKeys("90");
        
        //定向委托投资标的预期收益率
        this.driver.findElement(By.id("intr_year")).sendKeys("7%");
        
        //定向委托投资收益起始日----当年下个月第三个星期二
        this.clickDownArrow(9, 3);
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("chrome")){
//            List<WebElement> label1 = this.driver.findElements(By.xpath("//div[@class='calendar-nextmonth']"));
//            label1.get(0).click();
//            List<WebElement> label2 = this.driver.findElements(By.xpath("//div[@class='calendar-body']/table/tbody/tr[3]/td[3]"));
//            label2.get(0).click();
        	
        	//日期固定为--------2017-01-02
        	List<WebElement> selectDate = this.driver.findElements(By.xpath("//div[@class='calendar-title']"));
        	selectDate.get(0).click();
        	//选择年份
        	List<WebElement> selectDate1 = this.driver.findElements(By.xpath("//input[@class='calendar-menu-year']"));
        	selectDate1.get(0).clear();
        	selectDate1.get(0).sendKeys("2017");
        	//选择月份（1月份）
        	List<WebElement> selectDate2 = this.driver.findElements(By.xpath("//td[@class = 'calendar-menu-month']"));
        	selectDate2.get(0).click();
        	//选择日期（2号）
        	this.driver.findElement(By.xpath("//td[@class='calendar-day' and @abbr='2017,1,2']")).click();
        	        	
        }else{
//            List<WebElement> label1 = this.driver.findElements(By.xpath("//div[@class='calendar-nextmonth']"));
//            label1.get(1).click();
//            List<WebElement> label2 = this.driver.findElements(By.xpath("//div[@class='calendar-body']/table/tbody/tr[3]/td[3]"));
//            label2.get(1).click();
        	
        	 //日期固定为--------2017-01-02
        	List<WebElement> selectDate = this.driver.findElements(By.xpath("//div[@class='calendar-title']"));
        	selectDate.get(1).click();
        	//选择年份
        	List<WebElement> selectDate1 = this.driver.findElements(By.xpath("//input[@class='calendar-menu-year']"));
        	selectDate1.get(1).clear();
        	selectDate1.get(1).sendKeys("2017");
        	//选择月份（1月份）
        	List<WebElement> selectDate2 = this.driver.findElements(By.xpath("//td[@class = 'calendar-menu-month']"));
        	selectDate2.get(0).click();
        	//选择日期（2号）
        	this.driver.findElement(By.xpath("//td[@class='calendar-day' and @abbr='2017,1,2']")).click();
        	
        }

        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
        this.driver.findElement(By.linkText("确定")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
        
        //定向委托投资收益到期日----下一年当月第三个星期一
        this.clickDownArrow(10, 3);
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("chrome")){
        	//芦志鹏
//            List<WebElement> label3 = this.driver.findElements(By.xpath("//div[@class='calendar-nextyear']"));
//            label3.get(1).click();
//            List<WebElement> label4 = this.driver.findElements(By.xpath("//div[@class='calendar-body']/table/tbody/tr[3]/td[2]"));
//            label4.get(1).click();
        	
        	//宋晓庆----20160711----日期固定修改
        	List<WebElement> selectDate3 = this.driver.findElements(By.xpath("//div[@class='calendar-title']"));
        	selectDate3.get(1).click();
        	//选择年份 
        	List<WebElement> selectDate1 = this.driver.findElements(By.xpath("//input[@class='calendar-menu-year']"));
        	selectDate1.get(1).clear();
        	selectDate1.get(1).sendKeys("2017");
        	//选择月份（12月份）
        	List<WebElement> selectDate5 = this.driver.findElements(By.xpath("//td[@class='calendar-menu-month']"));
        	for(int i = 0 ; i < selectDate5.size(); i++){
        		if(selectDate5.get(i).getText().equals("十二月")){
        			selectDate5.get(i).click();
        			break;
        			}
        		}
        	//选择日期（1号）
        	this.driver.findElement(By.xpath("//td[@class='calendar-day' and @abbr='2017,12,1']")).click();
        	
        }else{
        	 //芦志鹏
//            List<WebElement> label3 = this.driver.findElements(By.xpath("//div[@class='calendar-nextyear']"));
//            label3.get(2).click();
//            List<WebElement> label4 = this.driver.findElements(By.xpath("//div[@class='calendar-body']/table/tbody/tr[3]/td[2]"));
//            label4.get(2).click();
        	
        	//宋晓庆----20160711----日期固定修改
        	//日期固定为--------2017-12-01
        	List<WebElement> selectDate3 = this.driver.findElements(By.xpath("//div[@class='calendar-title']"));
        	selectDate3.get(2).click();
        	//选择年份 
        	List<WebElement> selectDate1 = this.driver.findElements(By.xpath("//input[@class='calendar-menu-year']"));
        	selectDate1.get(2).clear();
        	selectDate1.get(2).sendKeys("2017");
        	//选择月份（12月份）
        	List<WebElement> selectDate5 = this.driver.findElements(By.xpath("//td[@class='calendar-menu-month']"));
        	for(int i = 0 ; i < selectDate5.size(); i++){
        		if(selectDate5.get(i).getText().equals("十二月")){
        			selectDate5.get(i).click();
        			break;
        			}
        		}
        	//选择日期（1号）
        	this.driver.findElement(By.xpath("//td[@class='calendar-day' and @abbr='2017,12,1']")).click();
        }
        
        //托管机构
        this.driver.findElement(By.id("trust_organization")).sendKeys(businessName);
        
        //乙方管理费率
        this.driver.findElement(By.id("manage_intr")).sendKeys("7%");
        
        //甲方预期收益率（合同）
        this.driver.findElement(By.id("expect_rate")).sendKeys("7%");
        
        //宽限期
        this.driver.findElement(By.id("grace_period")).sendKeys("10");
        this.driver.findElement(By.id("map_product")).click();
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
        this.driver.findElement(By.linkText("确定")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
        
        //挂钩产品 
        this.driver.findElement(By.id("map_product")).sendKeys("自动化测试标的abc321");
        
        
        //委托人年化固定收益率
        this.driver.findElement(By.id("fixed_intr")).sendKeys("600");
        
        //挂钩产品解释
        this.driver.findElement(By.id("explain_subject")).sendKeys("111111111");
        
        //产品名称
        this.driver.findElement(By.id("subject_name")).sendKeys(huiProjectName);
        
        //委托人最低委托金额
        this.driver.findElement(By.id("min_amount")).sendKeys("100");
        
        //账户名称
        this.driver.findElement(By.id("acct_name")).sendKeys("上海中关惠资产管理有限公司");
        
        //账户号 
        this.driver.findElement(By.id("acct_cd")).sendKeys("1560120540002950");
    
        //开户银行
        this.driver.findElement(By.id("deposit_bank")).sendKeys("南京银行珠江支行");
        
        //年化单位
        this.clickDownArrow(26, 2);
        textField = this.findTextField(26, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //递增金额
        this.driver.findElement(By.id("increase_amount")).sendKeys("100");
        
        //募集结束时间
        this.clickDownArrow(28, 2);
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("chrome")){
            List<WebElement> label1 = this.driver.findElements(By.xpath("//div[@class='calendar-nextmonth']"));
            label1.get(2).click();
            List<WebElement> label5 = this.driver.findElements(By.xpath("//div[@class='calendar-body']/table/tbody/tr[3]/td[2]"));
            label5.get(2).click();
        }else{
            List<WebElement> label1 = this.driver.findElements(By.xpath("//div[@class='calendar-nextmonth']"));
            label1.get(4).click();
            List<WebElement> label5 = this.driver.findElements(By.xpath("//div[@class='calendar-body']/table/tbody/tr[3]/td[2]"));
            label5.get(4).click();
        }
        this.driver.findElement(By.linkText("确定")).click();
        
        //还款企业
        this.driver.findElement(By.xpath(".//*[@id='s2id_repay_company']/a/span[2]/b")).click();
        this.driver.findElement(By.id("s2id_autogen1_search")).sendKeys(businessLegalName);
        this.driver.findElement(By.id("s2id_autogen1_search")).sendKeys(Keys.ENTER);
        
        //还款方式
        this.clickDownArrow(31, 2);
        textField = this.findTextField(31, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //产品风险评级
//        this.clickDownArrow(32, 2);
//        textField = this.findTextField(32, 2);
//        textField.click();
//        textField.sendKeys(Keys.ARROW_DOWN);
//        textField.sendKeys(Keys.ENTER);
        
        //电子章
        this.clickDownArrow(33, 2);
        textField = this.findTextField(33, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //委托人定向委托投资委托总金额
        this.driver.findElement(By.id("subject_amount")).sendKeys("10000");
        
        //项目成立最低金额 
        this.driver.findElement(By.id("last_amount")).sendKeys("1000");
        
        //是否敲出
        this.clickDownArrow(45, 2);
        textField = this.findTextField(45, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //委托人总年化看涨收益率上限
        this.driver.findElement(By.id("totl_rise_rate")).sendKeys("700");
        
        //点击保存
        this.driver.findElement(By.linkText("保存")).click();
        
        
        //点击确定
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath("html/body/div[31]/div[2]/div[2]")).getText();
                return text5.equalsIgnoreCase("操作成功");
            }
        });
        this.driver.findElement(By.linkText("确定")).click();
    }
}
