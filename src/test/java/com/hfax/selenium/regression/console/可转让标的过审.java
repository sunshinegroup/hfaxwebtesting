package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * 
 * @author luzhipeng
 * @Description  可转让标的过审
 */

public class 可转让标的过审 extends ConsoleBaseTest {
	private void clickDownArrow(int columnNumber) {
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td["+columnNumber+"]/div/table/tbody/tr/td//span[contains(@class, 'combo-arrow')]")).click();
	}

	private WebElement findTextField(int columnNumber) {
		return this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td["+columnNumber+"]/div/table/tbody/tr/td//input[contains(@class, 'combo-text')]"));
	}

	/**
	 * @throws Exception
	 */
	@Test
	public void 设置发布属性() throws Exception{
	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[3]/div/span[4]")));
	    this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[3]/div/span[4]")).click();
		
	    //等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				driver.switchTo().defaultContent();
				driver.switchTo().frame(0);
				String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
				return text2.equalsIgnoreCase("1");
			}
		});
		
		//检查新增项目位置
		String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
		if (textSearch.compareTo(this.consoleProjectName)!=0){
			//查询项目
			this.driver.findElement(By.id("q_project_name")).sendKeys(this.consoleProjectName);
			this.driver.findElement(By.linkText("查询")).click();
				
			this.wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver webDriver) {
					String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
					return text2.equalsIgnoreCase(consoleProjectName);
				}
			});
		}
		
		//选择项目
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
		this.findValueAtIndexRow(0, 0).click();
		
		//点击设置发布属性----项目发布设置
		this.driver.findElement(By.linkText("设置发布属性")).click();
		//切换至父iframe
		this.driver.switchTo().defaultContent();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[3]/a[1]/span[1]")));
		//关闭过审项目查询
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				webDriver.findElement(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[2]")).click();
				String text2 = webDriver.findElement(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[1]/span[1]")).getText();
				//System.out.println(text2);
				return text2.equalsIgnoreCase("项目发布设置");
			}
		});
		//切换至项目发布设置frame
		this.driver.switchTo().frame(0);
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='addform']/div[1]/table/tbody/tr[2]/td[4]/span/span/span")));
		String text3 = this.driver.findElement(By.xpath(".//*[@id='addform']/div[1]/table/tbody/tr[1]/td[1]")).getText();
		assertTrue("项目发布设置页面加载失败", text3.equals("项目编号："));
	

		//总募集周期起始日期--2015-4-8
		this.driver.findElement(By.xpath(".//*[@id='addform']/div[1]/table/tbody/tr[4]/td[2]/span/span/span")).click();
//		this.driver.findElement(By.cssSelector(" div[class='calendar-prevyear']")).click();
//		this.driver.findElement(By.xpath("//div[@class='calendar-body']/table/tbody/tr[2]/td[4]")).click();
		
        this.driver.findElement(By.xpath(".//div[@class='calendar-title']/span")).click();
    	//选择年份
    	this.driver.findElement(By.xpath(".//input[@class='calendar-menu-year']")).clear();
    	this.driver.findElement(By.xpath(".//input[@class='calendar-menu-year']")).sendKeys("2015");
    	//选择月份（4月份）
    	this.driver.findElement(By.xpath(".//td[@abbr='4']")).click();
    	//选择日期（8号）
    	this.driver.findElement(By.xpath("//td[@abbr='2015,4,8']")).click();
	
		//设置发布模式
		WebElement addFormElement = this.driver.findElement(By.xpath(".//*[@id='addform']/div[1]/table/tbody"));
		addFormElement.findElement(By.xpath("tr[2]/td[4]//span[contains(@class, 'combo-arrow')]")).click();//点击下拉框

		WebElement textField = addFormElement.findElement(By.xpath("tr[2]/td[4]//input[contains(@class, 'combo-text')]"));
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);

		//是否立即发布
		addFormElement.findElement(By.xpath("tr[3]/td[2]//span[contains(@class, 'combo-arrow')]")).click();//点击下拉框
		textField = addFormElement.findElement(By.xpath("tr[3]/td[2]//input[contains(@class, 'combo-text')]"));
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);

		//总募集周期截止日期--2017-4-9
		this.driver.findElement(By.xpath(".//*[@id='addform']/div[1]/table/tbody/tr[4]/td[4]/span/span/span")).click();
//		List<WebElement> label1 = driver.findElements(By.xpath("//div[@class='calendar-header']/div[4]"));
//		label1.get(3).click();
//		List<WebElement> label2 = driver.findElements(By.xpath("//div[@class='calendar-body']/table/tbody/tr[2]/td[1]"));
//		label2.get(3).click();
		
        List<WebElement> label1 = driver.findElements(By.xpath(".//div[@class='calendar-title']/span"));
        label1.get(3).click();
    	//选择年份
        List<WebElement> label2 = driver.findElements(By.xpath(".//input[@class='calendar-menu-year']"));
        label2.get(3).clear();
        label2.get(3).sendKeys("2017");
    	//选择月份（4月份）
        List<WebElement> label3 = driver.findElements(By.xpath(".//td[@abbr='4']"));
        label3.get(1).click();
    	//选择日期（9号）
        driver.findElement(By.xpath("//td[@abbr='2017,4,9']")).click();
		this.driver.findElement(By.linkText("确定")).click();

		//标的处理
		addFormElement.findElement(By.xpath("tr[7]/td[2]//span[contains(@class, 'combo-arrow')]")).click();//点击下拉框
		textField = addFormElement.findElement(By.xpath("tr[7]/td[2]//input[contains(@class, 'combo-text')]"));
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);
		
		//销售模式
		this.driver.findElement(By.xpath(".//*[@id='addform']//table/tbody/tr[8]/td[2]//span[contains(@class, 'combo-arrow')]")).click();
		textField = this.driver.findElement(By.xpath(".//*[@id='addform']//table/tbody/tr[8]/td[2]//input[contains(@class, 'combo-text')]"));
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);


		//点击保存
		this.driver.findElement(By.xpath(".//*[@id='addform']/div[2]/a/span/span")).click();
		
		//点击确定
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		WebElement confirmButton = this.driver.findElement(By.linkText("确定"));
		confirmButton.click();
		try{
			this.driver.findElement(By.linkText("确定")).isDisplayed();
			this.driver.findElement(By.linkText("确定")).sendKeys(Keys.ENTER);
		}catch(Exception e){
		}


		//等待拆分规则
	    String text5 = this.driver.findElement(By.xpath(".//*[@id='div_splity']/div/div[1]/div[1]")).getText();
		assertTrue("拆分规则加载失败", text5.equals("设置拆分规则"));
		
		//点击增加
		this.driver.findElement(By.xpath(".//*[@id='div_splity']/div/div[2]/div[1]/table/tbody/tr/td[2]/a/span/span")).click();
		
		//等待内容加载
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")));
		
		//点击对勾
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")).click();
		
		//输入优先级
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[4]/div/table/tbody/tr/td/input[1]")).sendKeys("1");
		
		//输入标的名称
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[5]/div/table/tbody/tr/td/input")).sendKeys(this.consoleProjectName);
		
		//输入单个项目金额
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[6]/div/table/tbody/tr/td/input[1]")).sendKeys("10000");
		
		//输入起投金额
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[7]/div/table/tbody/tr/td/input[1]")).sendKeys("100");
		
		//输入投递递增金额

//		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[8]/div/table/tbody/tr/td/input[1]")).sendKeys("100");
		
		//输入最大投资金额
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[8]/div/table/tbody/tr/td/input[1]")).sendKeys("10000");
		                                 
		//选择销售模式
		this.clickDownArrow(9);
		textField = this.findTextField(9);
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);

		//输入基础收益率
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[10]/div/table/tbody/tr/td/input[1]")).sendKeys(this.consoleRate);
		
		//选择是否前台展示
		this.clickDownArrow(13);
		textField = this.findTextField(13);
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);
		
		//选择惠理财分类
		this.clickDownArrow(14);
		textField = this.findTextField(14);
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);

		//选择转让属性--可以转让
		this.clickDownArrow(15);
		textField = this.findTextField(15);
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);

	    //转让锁定期
	    this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[16]/div/table/tbody/tr/td/input[1]")).sendKeys("1");
		
		//选择是否热卖
		this.clickDownArrow(17);
		textField = this.findTextField(17);
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);
		
		//选择是否使用优惠活动
		this.clickDownArrow(18);
		textField = this.findTextField(18);
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);
	
		//点击保存
		this.driver.findElement(By.xpath(".//*[@id='div_splity']/div/div[2]/div[1]/table/tbody/tr/td[10]/a/span/span")).click();
		 
		//点击确定
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		this.driver.findElement(By.linkText("确定")).click();
		
		//点击对勾
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")).click();

		//点击项目发布渠道
		this.driver.findElement(By.xpath(".//*[@id='div_splity']/div/div[2]/div[1]/table/tbody/tr/td[8]/a/span/span")).click();
		
		//等待页面加载----点击增加
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='channelWin']/div/div/div/div/div[1]/table/tbody/tr/td[2]/a/span/span")));
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
		this.driver.findElement(By.xpath(".//*[@id='channelWin']/div/div/div/div/div[1]/table/tbody/tr/td[2]/a/span/span")).click();
		
		//点击对勾
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[1]/div/input")));
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[1]/div/input")).click();

		//选择渠道编号
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[4]/div/table/tbody/tr/td//span[contains(@class, 'combo-arrow')]")).click();
		textField = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[4]/div/table/tbody/tr/td//input[contains(@class, 'combo-text')]"));
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);

		//点击保存
		this.driver.findElement(By.xpath(".//*[@id='channelWin']/div/div/div/div/div[1]/table/tbody/tr/td[8]/a/span/span")).click();
		
		//点击确定
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		this.driver.findElement(By.linkText("确定")).click();
		
		//点击关闭
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
		this.driver.findElement(By.xpath("//a[@class='panel-tool-close']")).click();

		//点击提交审核
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//a[@class='panel-tool-close']")));
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
		this.driver.findElement(By.xpath(".//*[@id='pass']/span/span")).click();

		//点击确定
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		this.driver.findElement(By.linkText("确定")).click();
	}
}