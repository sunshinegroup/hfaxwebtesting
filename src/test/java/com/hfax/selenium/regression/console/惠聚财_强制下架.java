package com.hfax.selenium.regression.console;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class 惠聚财_强制下架 extends ConsoleBaseTest{
	public void clickTab(int number)
	{
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[" + number + "]/div/span[4]")));
		this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[" + number + "]/div/span[4]")).click();
	}
	@Test
	public void 强制下架() {
		//点击强制下架
		clickTab(5);
		
		//产品名称
		 new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	 driver.switchTo().defaultContent();
	            	 driver.switchTo().frame(0);
	                return webDriver.findElement(By.xpath("//input[@id='subject_name1']")).isDisplayed();
	            }
	        });
		 
		 //选择惠投资
		 WebElement select = this.driver.findElement(By.xpath(".//*[@id='search_form']/table/tbody/tr[1]/td[2]/span/span/span"));
	     select.click();
	     WebElement inputbox = this.driver.findElement(By.xpath(".//*[@id='search_form']/table/tbody/tr[1]/td[2]/span/input[1]"));
	     inputbox.sendKeys(Keys.ARROW_DOWN);
	     inputbox.sendKeys(Keys.ENTER);
		 
		//输入产品名称
		this.driver.findElement(By.xpath("//input[@id='subject_name1']")).clear();
		this.driver.findElement(By.xpath("//input[@id='subject_name1']")).sendKeys(this.yueProjectName);
		
		WebElement firstPro = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div"));
		
		if(firstPro.getText().equalsIgnoreCase(this.yueProjectName)){
			//选择第一个产品
			this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")).click();
			
			//选择强制下架
			this.driver.findElement(By.xpath("html/body/div[1]/div[2]/div[2]/div/div/div[1]/table/tbody/tr/td[2]/a/span/span")).click();
			
			//点击确定
			this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("确定")));
			this.driver.findElement(By.linkText("确定")).click();
		}else{
			//点击查询
			this.driver.findElement(By.linkText("查询")).click();
			
			//找到第一个需要下架的产品
		new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				String text = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
				return text.equalsIgnoreCase(yueProjectName);
			}
		});
		//选择第一个产品
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")).click();
		
		//选择强制下架
		this.driver.findElement(By.xpath("html/body/div[1]/div[2]/div[2]/div/div/div[1]/table/tbody/tr/td[2]/a/span/span")).click();
		
		//点击确定
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("确定")));
		this.driver.findElement(By.linkText("确定")).click();
		}
	}

}
