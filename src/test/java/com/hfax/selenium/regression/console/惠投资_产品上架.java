package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 惠投资_产品上架  extends ConsoleBaseTest {
    private void clickDownArrow(int rowNumber, int columnNumber) {
        this.driver.findElement(By.xpath(".//*[@id='show_form']/table/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
    }

    private WebElement findTextField(int rowNumber, int columnNumber) {
        return this.driver.findElement(By.xpath(".//*[@id='show_form']/table/tbody/tr[" + rowNumber +
                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
    }
    
	  @Test
	    public void 产品上架 () throws Exception {
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[4]/div/span[4]")));
	        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[4]/div/span[4]")).click();

	        //等待页面加载
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                driver.switchTo().defaultContent();
	                driver.switchTo().frame(0);
	                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
	                return text2.equalsIgnoreCase("1");
	            }
	        });

	        //检查新增项目位置
	        String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
	        if (textSearch.compareTo(this.huiProjectName) != 0) {
	            //查询项目
	            this.driver.findElement(By.id("subject_name1")).sendKeys(this.huiProjectName);
	            this.driver.findElement(By.linkText("查询")).click();

	            final String consoleName = this.huiProjectName;
	            this.wait.until(new ExpectedCondition<Boolean>() {
	                @Override
	                public Boolean apply(WebDriver webDriver) {
	                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
	                    return text2.equalsIgnoreCase(consoleName);
	                }
	            });

	            String text3 = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
	            assertTrue("查询功能不正常", text3.equals(this.huiProjectName));
	        }
	        
	        //勾选项目
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")).click();
	        
	        //设置上架信息
	        this.driver.findElement(By.linkText("设置上架信息")).click();
	        
	        //等待弹出页
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='show_form']/table/tbody/tr[1]/td[1]")));
	        
	        //发售渠道
	        this.clickDownArrow(5, 2);
	        WebElement textField = this.findTextField(5, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        this.driver.findElement(By.xpath(".//*[@id='addGoundInfo']/div/div/div[2]/div/div")).click();
	        
	        //点击立即上架
	        this.driver.findElement(By.id("phtime")).click();
	        
	        //特定人组
	        this.driver.findElement(By.id("s2id_autogen2")).sendKeys("公共组");
	        this.driver.findElement(By.id("s2id_autogen2")).sendKeys(Keys.ENTER);
	        
	        //是否热卖
	        this.clickDownArrow(6, 2);
	        textField = this.findTextField(6, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ENTER);
	        
	        //点击提交
	        this.driver.findElement(By.linkText("提交")).click();
	        
	        //点击确定
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	String text5 = driver.findElement(By.xpath("html/body/div[15]/div[2]/div[2]")).getText();
	                return text5.equalsIgnoreCase("执行成功");
	            }
	        });
	        this.driver.findElement(By.linkText("确定")).click();
	  }
}
