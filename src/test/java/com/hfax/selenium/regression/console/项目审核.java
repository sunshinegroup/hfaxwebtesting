package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 项目审核 extends ConsoleBaseTest {
    @Test
    public void 审核() throws Exception {
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[2]/div/span[4]")));
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[2]/div/span[4]")).click();

        //等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[7]/div/a")).getText();
                return text2.equalsIgnoreCase("我要审核");
            }
        });

        //检查新增项目位置
        String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
        if (textSearch.compareTo(this.consoleProjectName) != 0) {
            //查询项目
            this.driver.findElement(By.id("q_project_name")).sendKeys(this.consoleProjectName);
            this.driver.findElement(By.linkText("查询")).click();

            final String consoleName = this.consoleProjectName;
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
                    return text2.equalsIgnoreCase(consoleName);
                }
            });

            String text3 = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
            assertTrue("查询功能不正常", text3.equals(this.consoleProjectName));
        }
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        WebElement el = this.findValueAtIndexRow(0, 6).findElement(By.tagName("a"));
        el.click();
        el.sendKeys(Keys.ENTER);

        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='pro_ok']/span/span")));

        String text4 = this.driver.findElement(By.xpath(".//*[@id='pro_ok']/span/span")).getText();

        assertTrue("项目信息审核页面加载失败", text4.equals("审核通过"));
        
        this.driver.findElement(By.xpath(".//*[@id='pro_ok']/span/span")).click();
        //输入审核意见
        this.driver.findElement(By.id("eff_desc")).sendKeys("lzp123");

        //点击提交
        this.driver.findElement(By.xpath(".//*[@id='pro_ok']/span/span")).click();

        //点击确定
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("确定")));
        this.driver.findElement(By.linkText("确定")).click();
        
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath("html/body/div[18]/div[2]/div[2]")).getText();
                return text5.equalsIgnoreCase("审核已通过");
            }
        });
        //点击确定
        this.driver.findElement(By.linkText("确定")).click();
        
    }

}