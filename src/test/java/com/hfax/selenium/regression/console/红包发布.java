package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * @author changwj
 * @Description 手动发送红包
 */
public class 红包发布 extends ConsoleBaseTest {
    private void clickDownArrow(int tableNumber, int rowNumber, int columnNumber) {
        this.driver.findElement(By.xpath(".//*[@id='show_form']/table[" + tableNumber + "]/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
    }

    private WebElement findTextField(int tableNumber, int rowNumber, int columnNumber) {
        return this.driver.findElement(By.xpath(".//*[@id='show_form']/table[" + tableNumber + "]/tbody/tr[" + rowNumber +
                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
    }

    @Test
    public void 卡券手动发送() throws Exception {
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[5]/ul/li[4]/div/span[4]")));

        //点击卡券手动发送
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[5]/ul/li[4]/div/span[4]")).click();

        // 等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
                return text2.equalsIgnoreCase("1");
            }
        });

        //点击增加
        this.driver.findElement(By.linkText("增加")).click();

        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='show_form']/table[1]/tbody/tr[1]/td[1]")));
        String text2 = driver.findElement(By.xpath(".//*[@id='show_form']/table[1]/tbody/tr[1]/td[1]")).getText().substring(0, 5);
        assertTrue("新增项目信息页面加载失败", text2.equalsIgnoreCase("投资券编号"));

        //投资券名称
        this.driver.findElement(By.id("ticket_name")).sendKeys(this.consoleProjectName);
        
        //发送对象选择
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='show_form']/table[1]/tbody/tr[2]/td[4]/span/span/span")));
        WebElement textField = this.findTextField(1, 2, 4);
        this.clickDownArrow(1, 2, 4);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);


        //投资券类型
        this.clickDownArrow(1, 2, 2);
        textField = this.findTextField(1, 2, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);

        //是否开启状态
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='show_form']/table[1]/tbody/tr[13]/td[2]/span/span/span")));

        this.clickDownArrow(1, 13, 2);
        textField = this.findTextField(1, 13, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);

        // 上传
        this.updateFile("filetxt", "hongbao.xls");
        this.updateFile("fileToUpload", "hongbao.xls");

        this.driver.findElement(By.xpath(".//*[@id='fileup']/button")).click();

        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'messager-window')]")));
        this.driver.findElement(By.xpath("//div[contains(@class, 'messager-window')]//span[@class='l-btn-left']")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@class, 'messager-window')]")));

        //获取来源
        this.driver.findElement(By.id("remark")).sendKeys("获取来源");

        //使用条件说用
        this.driver.findElement(By.id("use_condition")).sendKeys("使用条件说用");

        //点击发送
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("发送")));

        this.driver.findElement(By.linkText("发送")).click();

        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'messager-window')]")));
        this.driver.findElement(By.xpath("//div[contains(@class, 'messager-window')]//span[@class='l-btn-left']")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@class, 'messager-window')]")));
    }
}