package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 企业用户审核 extends ConsoleBaseTest {
	//公司名称
	protected String businessName(){
		return this.businessName;
	}
	@Test
	public void 审核(){
		//点击机构用户注册审核
		this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[8]/ul/li[1]/div/span[4]")).click();

		//等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				driver.switchTo().defaultContent();
				driver.switchTo().frame(0);
				String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
				return text2.equalsIgnoreCase("1");
			}
		});
		
		//检查新增项目位置
		String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[5]/div")).getText();
		if (textSearch.compareTo(businessName())!=0){
			//查询项目
			this.driver.findElement(By.id("qry_comame")).sendKeys(businessName());
			this.driver.findElement(By.linkText("查询")).click();

//			final String businessName = this.businessName;
			this.wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver webDriver) {
					String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[5]/div")).getText();
					return text2.equalsIgnoreCase(businessName());
				}
			});
			String text3 = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[5]/div")).getText();
			assertTrue("查询功能不正常", text3.equals(businessName()));
		}
		
		//点击审核
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
		this.driver.findElement(By.id("compInfoCheck")).click();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[4]/div[1]/div[1]")));
		String text4 = this.driver.findElement(By.xpath("html/body/div[4]/div[1]/div[1]")).getText();
		assertTrue("企业用户注册信息审核页面加载失败", text4.equals("企业用户注册信息审核"));
		
		//输入企业简称
		this.driver.findElement(By.id("comp-syns")).sendKeys(businessName());
		
		//输入企业描述
		this.driver.findElement(By.id("comp-remark")).sendKeys(businessName());
		
		//点击审核通过
		this.driver.findElement(By.id("audit-success")).click();
		
		//输入审核备注
		this.driver.findElement(By.id("remark")).sendKeys(businessName());
		
		//点击确认
		this.driver.findElement(By.id("audit")).click();
		
		//点击确定
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("确定")));
		this.driver.findElement(By.linkText("确定")).click();
	}
}