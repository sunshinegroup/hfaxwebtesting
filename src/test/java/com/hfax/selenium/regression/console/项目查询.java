
package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;


/**
 * @author changwj
 * @Description 项目查询
 */
public class 项目查询 extends ConsoleBaseTest {
    @Test
    public void 项目查询信息() {
        // 点击项目查询
	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[7]/div/span[4]")));
        driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[7]/div/span[4]")).click();
        
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				driver.switchTo().defaultContent();
				driver.switchTo().frame(0);
				String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
				return text2.equalsIgnoreCase("1");
			}
		});

        // 按照项目名称查询
		this.driver.findElement(By.id("project_name")).sendKeys(this.consoleProjectName);
		this.driver.findElement(By.id("project_name")).click();
		this.driver.findElement(By.id("project_name")).sendKeys(Keys.TAB);
		
		this.driver.findElement(By.xpath(".//*[@id='qry']/span/span")).click();
			
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
				return text2.equalsIgnoreCase(consoleProjectName);
			}
		});

        List<WebElement> findElements = driver.findElements(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div"));
        assertTrue("获取查询结果失败", findElements.size() > 0);

        final String projectName = this.consoleProjectName;
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text2 = findValueAtIndexRow(0, 2).getText();
                return text2.equals(projectName);
            }
        });
    }
}
