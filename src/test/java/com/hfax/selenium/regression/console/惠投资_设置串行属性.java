package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 惠投资_设置串行属性 extends ConsoleBaseTest{
    private void clickDownArrow(int rowNumber, int columnNumber) {
        this.driver.findElement(By.xpath(".//*[@id='pro_form']/table/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
    }

    private WebElement findTextField(int rowNumber, int columnNumber) {
        return this.driver.findElement(By.xpath(".//*[@id='pro_form']/table/tbody/tr[" + rowNumber +
                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
    }
	
	 @Test
	    public void 设置串行属性 () throws Exception {
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[6]/div/span[4]")));
	        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[6]/div/span[4]")).click();

	        //等待页面加载
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                driver.switchTo().defaultContent();
	                driver.switchTo().frame(0);
	                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
	                return text2.equalsIgnoreCase("1");
	            }
	        });

	        //检查新增项目位置
	        String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
	        if (textSearch.compareTo("projecttest001110") != 0) {
	            //查询项目
	            this.driver.findElement(By.id("subject_name1")).sendKeys("projecttest0011");
	            this.driver.findElement(By.linkText("查询")).click();

	            this.wait.until(new ExpectedCondition<Boolean>() {
	                @Override
	                public Boolean apply(WebDriver webDriver) {
	                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
	                    return text2.equalsIgnoreCase("projecttest001110");
	                }
	            });

	            String text3 = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
	            assertTrue("查询功能不正常", text3.equals("projecttest001110"));
	        }
	        
	        //勾选项目
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //设置上架信息
	        this.driver.findElement(By.linkText("设置串行产品属性")).click();
	        
	        //等待弹出页
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("s2id_autogen2")));
	        
	        //特定人组
	        this.driver.findElement(By.id("s2id_autogen2")).sendKeys("公共组");
	        this.driver.findElement(By.id("s2id_autogen2")).sendKeys(Keys.ENTER);
	        
	        //是否热卖
	        this.clickDownArrow(5, 2);
	        WebElement textField = this.findTextField(5, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ENTER);
	        
	        //发售渠道
	        this.clickDownArrow(4, 2);
	        textField = this.findTextField(4, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        this.clickDownArrow(4, 2);
	        
	        //点击提交
	        this.driver.findElement(By.linkText("提交")).click();
	        
	        //点击确定
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	        
	        //下一页
	        this.driver.findElement(By.xpath(".//span[@class='l-btn-empty pagination-next']")).click();
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        
	        //第二页
	        //等待页面
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
                    return text2.equalsIgnoreCase("projecttest001119");
                }
            });
            
	        //勾选项目
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //设置上架信息
	        this.driver.findElement(By.linkText("设置串行产品属性")).click();
	        
	        //等待弹出页
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("s2id_autogen2")));
	        
	        //是否热卖
	        this.clickDownArrow(5, 2);
	        textField = this.findTextField(5, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ENTER);
	        
	        //发售渠道
	        this.clickDownArrow(4, 2);
	        textField = this.findTextField(4, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        this.clickDownArrow(4, 2);
	        
	        //点击提交
	        this.driver.findElement(By.linkText("提交")).click();
	        
	        //点击确定
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	        
	        //下一页
	        this.driver.findElement(By.xpath(".//span[@class='l-btn-empty pagination-next']")).click();
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        
	        //第三页
	        //等待页面
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
                    return text2.equalsIgnoreCase("projecttest001129");
                }
            });
            
	        //勾选项目
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //设置上架信息
	        this.driver.findElement(By.linkText("设置串行产品属性")).click();
	        
	        //等待弹出页
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("s2id_autogen2")));
	        
	        //是否热卖
	        this.clickDownArrow(5, 2);
	        textField = this.findTextField(5, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ENTER);
	        
	        //发售渠道
	        this.clickDownArrow(4, 2);
	        textField = this.findTextField(4, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        this.clickDownArrow(4, 2);
	        
	        //点击提交
	        this.driver.findElement(By.linkText("提交")).click();
	        
	        //点击确定
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	        
	        //下一页
	        this.driver.findElement(By.xpath(".//span[@class='l-btn-empty pagination-next']")).click();
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        
	        //第四页
	        //等待页面
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
                    return text2.equalsIgnoreCase("projecttest001139");
                }
            });
            
	        //勾选项目
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //设置上架信息
	        this.driver.findElement(By.linkText("设置串行产品属性")).click();
	        
	        //等待弹出页
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("s2id_autogen2")));
	        
	        //是否热卖
	        this.clickDownArrow(5, 2);
	        textField = this.findTextField(5, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ENTER);
	        
	        //发售渠道
	        this.clickDownArrow(4, 2);
	        textField = this.findTextField(4, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        this.clickDownArrow(4, 2);
	        
	        //点击提交
	        this.driver.findElement(By.linkText("提交")).click();
	        
	        //点击确定
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	        
	        //下一页
	        this.driver.findElement(By.xpath(".//span[@class='l-btn-empty pagination-next']")).click();
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        
	        //第五页
	        //等待页面
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
                    return text2.equalsIgnoreCase("projecttest001149");
                }
            });
            
	        //勾选项目
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //设置上架信息
	        this.driver.findElement(By.linkText("设置串行产品属性")).click();
	        
	        //等待弹出页
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("s2id_autogen2")));
	        
	        //是否热卖
	        this.clickDownArrow(5, 2);
	        textField = this.findTextField(5, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ENTER);
	        
	        //发售渠道
	        this.clickDownArrow(4, 2);
	        textField = this.findTextField(4, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        this.clickDownArrow(4, 2);
	        
	        //点击提交
	        this.driver.findElement(By.linkText("提交")).click();
	        
	        //点击确定
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	  }
}
