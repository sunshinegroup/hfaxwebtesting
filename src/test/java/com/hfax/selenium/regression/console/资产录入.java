package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import java.awt.event.KeyEvent;
import java.util.List;


import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;


/**
 * @author changwj
 * @Description 资产录入
 */
public class 资产录入 extends ConsoleBaseTest {
    private void clickDownArrow(int tableNumber, int rowNumber, int columnNumber) {
        this.driver.findElement(By.xpath(".//*[@id='add_form']/table[" + tableNumber + "]/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
    }

    private WebElement findTextField(int tableNumber, int rowNumber, int columnNumber) {
        return this.driver.findElement(By.xpath(".//*[@id='add_form']/table[" + tableNumber + "]/tbody/tr[" + rowNumber +
                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
    }

    protected String lookupName() {
        return "liang100010";
    }

    @Test
    public void 新增标的() throws Exception {
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[1]/div/span[4]")));

        //点击项目维护
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[1]/div/span[4]")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[1]/span[1]")));
        // 等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.linkText("增加")).getText();
                return text2.equalsIgnoreCase("增加");
            }
        });  
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        
        //点击增加
        this.driver.findElement(By.linkText("增加")).click();

        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='add_form']/h2[1]")));
        String text2 = driver.findElement(By.xpath(".//*[@id='add_form']/h2[1]")).getText();
        assertTrue("新增项目信息页面加载失败", text2.equalsIgnoreCase("项目基础信息"));

        //产品分类
        this.clickDownArrow(1, 1, 2);
        WebElement textField = this.findTextField(1, 1, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);

        //项目名称
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("project_name")));
//        this.driver.findElement(By.id("project_name")).click();
        this.driver.findElement(By.id("project_name")).sendKeys(this.consoleProjectName);

        //交易模式
        this.clickDownArrow(1, 2, 2);
        textField = this.findTextField(1, 2, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);

        //资产总额 
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("loan_amout")));
//        this.driver.findElement(By.id("loan_amout")).click();
        this.driver.findElement(By.id("loan_amout")).sendKeys("10000");

        //资产成立日 	  2015-4-7
        this.driver.findElement(By.xpath(".//*[@id='add_form']/table[1]/tbody/tr[2]/td[6]/span/span/span")).click();
//        this.driver.findElement(By.cssSelector(" div[class='calendar-prevyear']")).click();
//        this.driver.findElement(By.xpath("//div[@class='calendar-body']/table/tbody/tr[2]/td[3]")).click();
        
        this.driver.findElement(By.xpath(".//div[@class='calendar-title']/span")).click();
    	//选择年份
    	this.driver.findElement(By.xpath(".//input[@class='calendar-menu-year']")).clear();
    	this.driver.findElement(By.xpath(".//input[@class='calendar-menu-year']")).sendKeys("2015");
    	//选择月份（4月份）
    	this.driver.findElement(By.xpath(".//td[@abbr='4']")).click();
    	//选择日期（7号）
    	this.driver.findElement(By.xpath("//td[@abbr='2015,4,7']")).click();
    	
        //资产到期日 	  2017-4-18
        this.driver.findElement(By.xpath(".//*[@id='add_form']/table[1]/tbody/tr[3]/td[2]/span/span/span")).click();
//        List<WebElement> label1 = driver.findElements(By.xpath("//div[@class='calendar-nextyear']"));
//        label1.get(1).click();
//        List<WebElement> label2 = driver.findElements(By.xpath("//div[@class='calendar-body']/table/tbody/tr[3]/td[3]"));
//        label2.get(1).click();
        
        List<WebElement> label1 = driver.findElements(By.xpath(".//div[@class='calendar-title']/span"));
        label1.get(1).click();
    	//选择年份
        List<WebElement> label2 = driver.findElements(By.xpath(".//input[@class='calendar-menu-year']"));
        label2.get(1).clear();
        label2.get(1).sendKeys("2017");
    	//选择月份（4月份）
        List<WebElement> label3 = driver.findElements(By.xpath(".//td[@abbr='4']"));
        label3.get(1).click();
    	//选择日期（18号）
        driver.findElement(By.xpath("//td[@abbr='2017,4,18']")).click();
        
        
        
        //产品利率
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("year_intr")));
        this.driver.findElement(By.id("year_intr")).sendKeys("13");

//        //还款
//        this.clickDownArrow(1, 4, 6);
//        textField = this.findTextField(1, 4, 6);
//        textField.click();
//        textField.sendKeys(Keys.ARROW_DOWN);
//        textField.sendKeys(Keys.ENTER);

        //宽限天数
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("grace_period")));
        this.driver.findElement(By.id("grace_period")).sendKeys("3");

        //投资标的类型
//        this.driver.findElement(By.id("subject_type")).click();

        //项目概况标题1
        this.driver.findElement(By.id("desc_title1")).sendKeys(this.consoleProjectName);

        //项目概况内容1
        this.driver.findElement(By.id("desc_cont1")).sendKeys("项目概况内容1项目概况内容1");

        //是否溢价
        this.clickDownArrow(1, 4, 2);
        textField = this.findTextField(1, 4, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_UP);
        textField.sendKeys(Keys.ENTER);

        //产品投向情况介绍
        this.driver.findElement(By.id("prod_to_dc")).sendKeys("1111111");

        //保障类型
        this.clickDownArrow(4, 1, 2);
        textField = this.findTextField(4, 1, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);

        //托管银行
        this.driver.findElement(By.id("grace_period")).click();//修正托管银行点击无响应
        this.clickDownArrow(5, 1, 2);
        textField = this.findTextField(5, 1, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);

        //银行账号
        this.driver.findElement(By.id("bank_no")).sendKeys("1231231231");

        //放款方式选择
        this.clickDownArrow(5, 2, 2);
        textField = this.findTextField(5, 2, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);

        //开户银行支行
        this.driver.findElement(By.id("open_bank")).sendKeys("12312312");

        //查询
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='showMemberList']/span/span")));

        //刷新查询列表
        driver.findElement(By.id("membcd")).sendKeys("111111111");
        driver.findElement(By.xpath(".//*[@id='showMemberList']/span/span")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[18]/div[1]/div[2]/a")));
        driver.findElement(By.xpath("html/body/div[18]/div[1]/div[2]/a")).click();
        
        //输入企业名称
        final String lookupName = this.lookupName();
        driver.findElement(By.id("membcd")).sendKeys(lookupName);

        //点击查询
        driver.findElement(By.xpath(".//*[@id='showMemberList']/span/span")).click();

        //等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[2]/div")).getText();
                return text2.equalsIgnoreCase(lookupName);
            }
        });
        
        this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[9]/div")).click();

        //保存

        this.driver.findElement(By.linkText("确定")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
        this.driver.findElement(By.linkText("保存")).click();

        //点击确定
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
        this.driver.findElement(By.linkText("确定")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));


        //检查新增项目位置
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
        String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
        if (textSearch.compareTo(this.consoleProjectName) != 0) {
            //查询项目
            this.driver.findElement(By.id("q_project_name")).sendKeys(this.consoleProjectName);
            this.driver.findElement(By.linkText("查询")).click();

            final String consoleProjectName = this.consoleProjectName;
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
                    return text2.equalsIgnoreCase(consoleProjectName);
                }
            });
        }
        findValueAtIndexRow(0, 0).click();
        //提交审核
        this.driver.findElement(By.linkText("提交审核")).click();

        //点击确定
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
        this.driver.findElement(By.linkText("确定")).click();
        
        //点击确定
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath("html/body/div[21]/div[2]/div[2]")).getText();
                return text5.equalsIgnoreCase("操作已完成");
            }
        });
        this.driver.findElement(By.linkText("确定")).click();
    }
}