package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * @author changwj
 * @Description 截标
 */
public class 截标 extends 项目查询 {
    @Test
    public void 截标成功() {
        项目查询信息();

        //判断状态
        assertTrue("状态不是投标中，不允许流标", findValueAtIndexRow(0, 14).getText().equals("投标中"));
        findValueAtIndexRow(0, 0).click();

        //点击截标
        this.driver.findElement(By.linkText("截标")).click();

        //确定截标
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		driver.findElement(By.linkText("确定")).click();
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));

        //点击确定
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		driver.findElement(By.linkText("确定")).click();
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
    }
}
