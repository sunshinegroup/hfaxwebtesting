package com.hfax.selenium.regression.console;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.business.BusinessBaseTest;


public class 惠投资_还款计划录入  extends ConsoleBaseTest  {
    @Test
    public void 还款计划录入() throws Exception{
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[9]/div/span[4]")));
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[9]/div/span[4]")).click();

        //等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
                return text2.equalsIgnoreCase("1");
            }
        });

        //检查新增项目位置
        String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
        if (textSearch.compareTo(this.huiProjectName) != 0) {
            //查询项目
            this.driver.findElement(By.id("subject_name")).sendKeys(this.huiProjectName);
            this.driver.findElement(By.linkText("查询")).click();

            final String consoleName = this.huiProjectName;
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
                    return text2.equalsIgnoreCase(consoleName);
                }
            });

            String text3 = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
            assertTrue("查询功能不正常", text3.equals(this.huiProjectName));
        }
        
        //勾选项目
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")).click();
        
        //获取产品编号
        String id = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
        
        //获取文件
        String fileName = "new.xlsx";
        ClassLoader loader = ConsoleBaseTest.class.getClassLoader();
        File file = new File(loader.getResource(fileName).getFile());
        assertTrue("无法读取文件: " + fileName, file != null);

        String filePath = file.getAbsolutePath();
        assertTrue("文件路径为空: " + fileName, filePath.length() > 0);

        try {   
            XSSFWorkbook xwb = new XSSFWorkbook(new FileInputStream(filePath));  
  
	        XSSFSheet xSheet = xwb.getSheetAt(0);  
	        XSSFRow xRow = xSheet.createRow(1);    
	        
	        xRow.createCell(0).setCellValue(huiProjectName); 
	        xRow.createCell(1).setCellValue(huiProjectName); 
	        xRow.createCell(2).setCellValue(id);
	        xRow.createCell(3).setCellValue("2500");  
	        xRow.createCell(4).setCellValue("3000");  
	        xRow.createCell(5).setCellValue("600");  
	        xRow.createCell(6).setCellValue("55500");  
	        xRow.createCell(7).setCellValue("100");
	        xRow.createCell(8).setCellValue("0"); 
  
            FileOutputStream out = new FileOutputStream(filePath);  
            xwb.write(out);  
            out.close();  
  
        } catch (Exception e) {  
            e.printStackTrace();  
        }   
        
        //点击上传还款计划
        this.driver.findElement(By.linkText("上传还款计划")).click();
        
        //输入上传你路径
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("file_name")));
        WebElement uploadField = this.driver.findElement(By.id("file_name"));
        ((JavascriptExecutor)this.driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", uploadField);
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("file_name")));
        uploadField.clear();
        ((JavascriptExecutor)this.driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", this.driver.findElement(By.id("fileToUpload")));
        this.driver.findElement(By.id("fileToUpload")).sendKeys(filePath);
        this.driver.findElement(By.xpath(".//*[@id='fileup']/button")).click();
        
        //弹窗确认
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath("html/body/div[12]/div[2]/div[1]")).getText();
                return text5.equalsIgnoreCase("文件上传成功");
            }
        });
        //点击确定
        this.driver.findElement(By.linkText("确定")).click();
        
        //点击确定
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("html/body/div[12]/div[2]/div[1]")));
		this.driver.findElement(By.xpath(".//*[@id='save']/span/span[1]")).click();

        //勾选项目
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[1]/div/input")));
        this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[1]/div/input")).click();
        
        //点击确定
        List<WebElement> label1 = driver.findElements(By.xpath(".//*[@id='save']/span/span[1]"));
        label1.get(1).click();
        
        //弹窗确认
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath("html/body/div[12]/div[2]/div[2]")).getText();
                return text5.equalsIgnoreCase("上传成功");
            }
        });
        this.driver.findElement(By.linkText("确定")).click();
    }
} 
