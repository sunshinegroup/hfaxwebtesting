package com.hfax.selenium.regression.console;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;
import com.hfax.selenium.regression.business.BusinessBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * 
 * @author changwj
 * @Description 测试登录
 */
public class ConsoleBaseTest extends SuddenDeathBaseTest {
    @Override
    public void setup() throws Exception {
        super.setup();
        this.consoleLogin();
    }
    /**
	 * 查找通用表格
     */
	protected WebElement findDataGridTable() {
		String tableXPath = "//div[contains(@class,'layout-split-center')]//div[@class='panel datagrid']//div[@class='datagrid-view2']//div[@class='datagrid-body']//table";

		this.wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(tableXPath + "//tr")));
		return this.driver.findElement(By.xpath(tableXPath));
	}

	/**
	 * 确定查询列表的哪一行哪一列
	 */
	protected WebElement findValueAtIndexRow(int row, int column) {
		WebElement tableElement = this.findDataGridTable();
		List<WebElement> trs = tableElement.findElements(By.tagName("tr"));
		List<WebElement> tds = trs.get(row).findElements(By.tagName("td"));
		return tds.get(column);
	}

	protected void updateFile(String elementId, String fileName) {
		WebElement uploadField = this.driver.findElement(By.id(elementId));

		((JavascriptExecutor)this.driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", uploadField);

		ClassLoader loader = BusinessBaseTest.class.getClassLoader();
		File file = new File(loader.getResource(fileName).getFile());
		assertTrue("无法读取文件: " + fileName, file != null);

		String filePath = file.getAbsolutePath();
		assertTrue("文件路径为空: " + fileName, filePath.length() > 0);
		
		System.out.println(filePath);
		uploadField.sendKeys(filePath);
	}
    protected String file(String fileName){
        ClassLoader loader = ConsoleBaseTest.class.getClassLoader();
        File file = new File(loader.getResource(fileName).getFile());
        assertTrue("无法读取文件: " + fileName, file != null);

        String filePath = file.getAbsolutePath();
        assertTrue("文件路径为空: " + fileName, filePath.length() > 0);
		
        return filePath;       
    }
    
	/**
	 * 选择之后日期
	 */
    protected void afterDate(int thirdDiv , int days){
        Date dNow = new Date();   //当前时间
      	Date dAfter = new Date();
      	Calendar calendar = Calendar.getInstance(); //得到日历
      	calendar.setTime(dNow);//把当前时间赋给日历
      	SimpleDateFormat year=new SimpleDateFormat("yyyy"); //设置年份格式
      	SimpleDateFormat month=new SimpleDateFormat("M"); //设置月份格式
      	SimpleDateFormat sdf=new SimpleDateFormat("yyyy,M,d"); //设置时间格式
      	
      	System.out.println(days);
     	calendar.add(Calendar.DAY_OF_MONTH, +days);  //加若干天后
      	dAfter = calendar.getTime();   //得到加后时间

      	String dateYear = year.format(dAfter); //格式加后年份
      	String dateMonth = month.format(dAfter); //格式化加后月份
      	System.out.println(days + "天后年份为：" + dateYear);
      	System.out.println(days + "天后月份为： " + dateMonth);
      	
      	String defaultStartDate2 = sdf.format(dAfter);    //格式化加后日期
      	String date2 = "//td[@abbr='" + defaultStartDate2 + "']"; //确认日期xpath
      	System.out.println("日期地址为：" + date2);
      	
      	//点击选择年份
      	this.driver.findElement(By.xpath("html/body/div[" + thirdDiv + "]/div/div[1]/div/div[1]/div[5]/span")).click();
      	//输入年份
      	this.driver.findElement(By.xpath("html/body/div[" + thirdDiv + "]/div/div[1]/div/div[2]/div/div[1]/span[2]/input")).clear();
      	this.driver.findElement(By.xpath("html/body/div[" + thirdDiv + "]/div/div[1]/div/div[2]/div/div[1]/span[2]/input")).sendKeys(dateYear);
      	
      	//选择月份
      	this.driver.findElement(By.xpath("html/body/div[" + thirdDiv + "]//td[@abbr='" + dateMonth + "']")).click();
      	
      	//点击日期
      	this.driver.findElement(By.xpath(date2)).click();
	}
    
	/**
	 * 选择之前日期
	 */
    protected void beforeDate(int thirdDiv , int days){
        Date dNow = new Date();   //当前时间
      	Date dAfter = new Date();
      	Calendar calendar = Calendar.getInstance(); //得到日历
      	calendar.setTime(dNow);//把当前时间赋给日历
      	SimpleDateFormat year=new SimpleDateFormat("yyyy"); //设置年份格式
      	SimpleDateFormat month=new SimpleDateFormat("M"); //设置月份格式
      	SimpleDateFormat sdf=new SimpleDateFormat("yyyy,M,d"); //设置时间格式
      	
      	System.out.println(days);
     	calendar.add(Calendar.DAY_OF_MONTH, -days);  //减若干天后
      	dAfter = calendar.getTime();   //得到减后时间

      	String dateYear = year.format(dAfter); //格式减后年份
      	String dateMonth = month.format(dAfter); //格式化减后月份
      	System.out.println(days + "天前年份为：" + dateYear);
      	System.out.println(days + "天前月份为： " + dateMonth);
      	
      	String defaultStartDate2 = sdf.format(dAfter);    //格式化减后日期
      	String date2 = "//td[@abbr='" + defaultStartDate2 + "']"; //确认日期xpath
      	System.out.println("日期地址为：" + date2);
      	
      	//点击选择年份
      	this.driver.findElement(By.xpath("html/body/div[" + thirdDiv + "]/div/div[1]/div/div[1]/div[5]/span")).click();
      	//输入年份
      	this.driver.findElement(By.xpath("html/body/div[" + thirdDiv + "]/div/div[1]/div/div[2]/div/div[1]/span[2]/input")).clear();
      	this.driver.findElement(By.xpath("html/body/div[" + thirdDiv + "]/div/div[1]/div/div[2]/div/div[1]/span[2]/input")).sendKeys(dateYear);
      	
      	//选择月份
      	this.driver.findElement(By.xpath("html/body/div[" + thirdDiv + "]//td[@abbr='" + dateMonth + "']")).click();
      	
      	//点击日期
      	this.driver.findElement(By.xpath(date2)).click();
	}
}
