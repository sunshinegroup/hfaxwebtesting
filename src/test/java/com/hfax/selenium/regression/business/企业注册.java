package com.hfax.selenium.regression.business;

import com.hfax.selenium.base.SuddenDeathBaseTest;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class 企业注册 extends SuddenDeathBaseTest {
	//企业用户名
	protected String businessUsername(){
		return this.businessUsername;
	}
	//企业用户登录密码
	protected String businessPassword(){
		return this.businessPassword;
	}
    @Override
    public void setup() throws Exception { 
        super.setup();
        this.loadPage(businessRegistrationUrl, "注册");
    }

    @Test
    public void 注册() throws Exception {
        //输入注册信息
    	this.driver.findElement(By.id("userName")).clear();
        this.driver.findElement(By.id("userName")).sendKeys(businessUsername());
        System.out.println("企业用户注册的用户名为：" + businessUsername());
        this.driver.findElement(By.id("password")).clear();
        this.driver.findElement(By.id("password")).sendKeys(businessPassword());
        System.out.println("企业用户注册的密码为：" + businessPassword());
        this.driver.findElement(By.id("confirmPassword")).clear();
        this.driver.findElement(By.id("confirmPassword")).sendKeys(businessPassword());
        this.driver.findElement(By.id("dealpwd")).clear();
        this.driver.findElement(By.id("dealpwd")).sendKeys(businessPassword());
        this.driver.findElement(By.id("dealpwdCon")).clear();
        this.driver.findElement(By.id("dealpwdCon")).sendKeys(businessPassword());
        this.driver.findElement(By.id("code")).clear();
        this.driver.findElement(By.id("code")).sendKeys("1234");
        this.driver.findElement(By.xpath(".//*[@id='btn_register']/img")).click();
        
        Thread.sleep(3000);
        //等待警告框弹出
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
		        Alert alert = driver.switchTo().alert();  
		        String str = alert.getText();  
		        System.out.println(str);
				return str.equalsIgnoreCase("您已注册成功，请登录系统并完善账户信息。");
			}
		});
		driver.switchTo().alert().accept();
    }
}