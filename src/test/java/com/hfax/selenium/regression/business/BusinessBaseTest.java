package com.hfax.selenium.regression.business;

import com.hfax.selenium.base.SuddenDeathBaseTest;
import com.hfax.selenium.regression.console.ConsoleBaseTest;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.io.File;

import static org.junit.Assert.assertTrue;

/**
 * Created by philip on 4/27/16.
 */
public class BusinessBaseTest extends SuddenDeathBaseTest {
    @Override
    public void setup() throws Exception {
        super.setup();
        this.loadPage();
        this.businessLogin();
    }

    /*
     * 上传文件--企业验证账号信息
     */
    protected void updateFile(String elementId, String fileName) {
        WebElement uploadField = this.driver.findElement(By.id(elementId));

        ((JavascriptExecutor)this.driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", uploadField);

        ClassLoader loader = BusinessBaseTest.class.getClassLoader();
        File file = new File(loader.getResource(fileName).getFile());
        assertTrue("无法读取文件: " + fileName, file != null);

        String filePath = file.getAbsolutePath();
        assertTrue("文件路径为空: " + fileName, filePath.length() > 0);

        uploadField.sendKeys(filePath);
    }
    

	 //获取元素内容
   protected String getElementText(String strXpath){
   	try{
   		String text = this.driver.findElement(By.xpath(strXpath)).getText().trim();
   		return text;
   	}catch (Exception e){
   		e.printStackTrace();
   	}
   	return null;
   	}
}
