package com.hfax.selenium.regression.business;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class 企业已发布的借款 extends BusinessBaseTest {
	@Override
	public void setup() throws Exception {
		super.setup();
		this.selectMenu(8, "已发布的借款");
	}

    private static String[] expectedTitles = {"标题", "还款方式", "金额（元）", "年利率", "期限", "发布时间", "进度", "状态"};

    private void verifyTableColumns(String[] expectedTitles) {
        List<WebElement> headers = this.driver.findElements(By.xpath("//div[@class='biaoge']//table//th"));
        List<WebElement> cleanedHeaders = new ArrayList<WebElement>();

        for (int i = 0; i < headers.size(); i ++) {
            if (headers.get(i).isDisplayed()) {
                cleanedHeaders.add(headers.get(i));
            }
        }

        assertTrue("表列数不对", cleanedHeaders.size() == expectedTitles.length);
        for (int i = 0; i < cleanedHeaders.size(); i ++) {
            String expectedTitle = expectedTitles[i];
            String headerString = cleanedHeaders.get(i).getText();
            assertTrue("表列 "+i+" 抬头不对: " + expectedTitle + " / " + headerString, expectedTitle.equalsIgnoreCase(headerString));
        }
    }

    private void enterDatesAndVerifyNoResult() {
        this.driver.findElement(By.id("publishTimeStart")).sendKeys("2015-12-12");
        this.driver.findElement(By.id("publishTimeEnd")).sendKeys("2015-12-13");
        this.driver.findElement(By.xpath("//form[@id='searchForm']//a[@id='search']")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElements(By.xpath("//div[@class='biaoge']//table/tbody/tr")).size() == 2;
            }
        });

        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td"), "暂无数据"));
    }

	// 审核中的借款
	@Test
	public void 审核中的借款() {
        this.selectSubMenu(1, "审核中的借款");
        this.enterDatesAndVerifyNoResult();
        this.verifyTableColumns(expectedTitles);
	}

	// 招标中的借款
	@Test
	public void 招标中的借款() {
        this.selectSubMenu(2, "招标中的借款");
        this.enterDatesAndVerifyNoResult();
        this.verifyTableColumns(expectedTitles);
	}

	// 流标中的借款
	@Test
	public void 流标中的借款() {
        this.selectSubMenu(3, "流标的借款");
        this.enterDatesAndVerifyNoResult();
        this.verifyTableColumns(expectedTitles);
	}
}