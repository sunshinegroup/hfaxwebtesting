package com.hfax.selenium.regression.business;

import static org.junit.Assert.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.*;
import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
/**
 *  
 * @Description 企业还款管理_惠理财
 * Created by songxq on 22/8/16.
 */
public class 企业还款管理_惠理财 extends BusinessBaseTest{
	
	private WebElement clickRowCol(int rowNumber, int columnNumber) {
		try{
			WebElement rc = this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]"));
			return rc;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
		
    }
	 @Override
	    public void setup() throws Exception {
	        super.setup();
	        this.selectMenu(7, "还款管理");
	    }
	 
	@Test
	public void test001_成功借款() {
		 //点击成功借款
		 this.selectSubMenu(1, "成功借款");
		 
		 //页面加载等待
		 this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//td[@align='center']"));
	            }
	        });
		 
		 List<WebElement> list = this.driver.findElements(By.xpath("//td[@align='center']"));
		 //验证借款金额
		 System.out.println("【成功借款页】的借款金额为:" + list.get(2).getText().trim());
		 assertTrue("【成功借款页】的借款金额不正确：",list.get(2).getText().trim().equals("10000.00元"));
		 
		 //验证年利率
		 System.out.println("【成功借款页】的年利率为:" + list.get(3).getText().trim());
		 assertTrue("【成功借款页】的年利率不正确：",list.get(3).getText().trim().equals("700.00%"));
		 
		 //验证还款期限
		 System.out.println("【成功借款页】的还款期限为:" + list.get(4).getText().trim());
		 assertTrue("【成功借款页】的还款期限不正确：",list.get(4).getText().trim().equals("8天"));
		 
		 //验证借款时间
//		 System.out.println("【成功借款页】的借款时间为:" + list.get(5).getText().trim());
//		 assertTrue("【成功借款页】的借款时间不正确：",list.get(5).getText().trim().equals("20160811"));
		 
		 //验证应还本息
		 System.out.println("【成功借款页】的应还本息为:" + list.get(6).getText().trim());
		 assertTrue("【成功借款页】的应还本息不正确：",list.get(6).getText().trim().equals("￥ 11534.25"));
		 
		 //验证已还本息
		 System.out.println("【成功借款页】的已还本息为:" + list.get(7).getText().trim());
		 assertTrue("【成功借款页】的已还本息不正确：",list.get(7).getText().trim().equals("￥0.00"));
		 
		 //验证未还本息
		 System.out.println("【成功借款页】的未还本息为:" + list.get(8).getText().trim());
		 assertTrue("【成功借款页】的未还本息不正确：",list.get(8).getText().trim().equals("￥11534.25"));
		 
		 //验证标的状态
		 System.out.println("【成功借款页】的标的状态为:" + list.get(9).getText().trim());
		 assertTrue("【成功借款页】的标的状态不正确：",list.get(9).getText().trim().equals("还款中"));
		 
		 
	}
	@Test
	public void test002_正在还款的借款(){
		//点击正在还款的借款
		this.selectSubMenu(2, "正在还款的借款");
		
		 //页面加载等待
		 this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//td[@align='center']"));
	            }
	        });
		 
		 List<WebElement> list = this.driver.findElements(By.xpath("//td[@align='center']"));
		 //验证借款金额
		 System.out.println("【正在还款的借款页】的借款金额为:" + list.get(2).getText().trim());
		 assertTrue("【正在还款的借款页】的借款金额不正确：",list.get(2).getText().trim().equals("10000.00元"));
		 
		 //验证年利率
		 System.out.println("【正在还款的借款页】的年利率为:" + list.get(3).getText().trim());
		 assertTrue("【正在还款的借款页】的年利率不正确",list.get(3).getText().trim().equals("700.00%"));
		 
		 //验证还款期限
		 System.out.println("【正在还款的借款页】的还款期限为:" + list.get(4).getText().trim());
		 assertTrue("【正在还款的借款页】的还款期限不正确",list.get(4).getText().trim().equals("8天"));
		 
		 //验证还款日期
		 System.out.println("【正在还款的借款页】的还款日期为:" + list.get(5).getText().trim());
		 assertTrue("【正在还款的借款页】的还款日期不正确",list.get(5).getText().trim().equals("20170418"));
		 
		 //验证应还本息
		 System.out.println("【正在还款的借款页】的应还本息为:" + list.get(9).getText().trim());
		 assertTrue("【正在还款的借款页】的应还本息不正确",list.get(9).getText().trim().equals("￥11534.25"));
		 
		 //验证已还本息
		 System.out.println("【正在还款的借款页】的已还本息为:" + list.get(10).getText().trim());
		 assertTrue("【正在还款的借款页】的已还本息不正确",list.get(10).getText().trim().equals("￥0.00"));
		 
		 //验证待还本息
		 System.out.println("【正在还款的借款页】的待还本息为:" + list.get(11).getText().trim());
		 assertTrue("【正在还款的借款页】的待还本息不正确",list.get(11).getText().trim().equals("￥11534.25"));
		 
		 //操作
		 System.out.println("【正在还款的借款页】的操作为:" + list.get(12).getText().trim());
		 assertTrue("【正在还款的借款页】的操作不正确",list.get(12).getText().trim().equals("还款明细"));
		 
		
	}
	@Test
	public void test003_还款明细账(){
		this.selectSubMenu(4, "还款明细账");
		 //页面加载等待
		 this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//td[@align='center']"));
	            }
	        });
		 
		 List<WebElement> list = this.driver.findElements(By.xpath("//td[@align='center']"));
		 //还款日期
		 System.out.println("【还款明细账页】的还款日期为:" + list.get(2).getText().trim());
		 assertTrue("【还款明细账页】的还款日期不正确",list.get(2).getText().trim().equals("20170418"));
		 
		 //实际还款日期
		 System.out.println("【还款明细账页】的实际还款日期为:" + list.get(3).getText().trim());
		 assertTrue("【还款明细账页】的实际还款日期不正确",list.get(3).getText().trim().equals(""));
		 
		 //本期应还本息
		 System.out.println("【还款明细账页】的本期应还本息为:" + list.get(4).getText().trim());
		 assertTrue("【还款明细账页】的本期应还本息不正确",list.get(4).getText().trim().equals("￥11534.25"));
		 
		 //利息
		 System.out.println("【还款明细账页】的利息为:" + list.get(5).getText().trim());
		 assertTrue("【还款明细账页】的利息不正确",list.get(5).getText().trim().equals("￥1534.25"));
		 
		 //逾期罚款
		 System.out.println("【还款明细账页】的逾期罚款为:" + list.get(6).getText().trim());
		 assertTrue("【还款明细账页】的逾期罚款不正确",list.get(6).getText().trim().equals("￥0.00"));
		 
		 //逾期天数
		 System.out.println("【还款明细账页】的逾期天数为:" + list.get(7).getText().trim());
		 assertTrue("【还款明细账页】的逾期天数不正确",list.get(7).getText().trim().equals("0"));
		 
		 //还款状态
		 System.out.println("【还款明细账页】的还款状态为:" + list.get(8).getText().trim());
		 assertTrue("【还款明细账页】的还款状态不正确",list.get(8).getText().trim().equals("未偿还"));
		 		
	}
	@Test
	public void test004_正在还款中的借款_还款明细_还款() throws InterruptedException{
		//点击正在还款的借款
		 this.selectSubMenu(2, "正在还款的借款");
		 
		//页面加载等待
		 this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//td[@align='center']"));
	            }
	        });
		 
		
		 //点击还款明细
		 this.driver.findElement(By.linkText("还款明细")).click();
		 
		 //页面加载等待
		 this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.linkText("还款"));
	            }
	        });
		 
		 //验证借款金额
		 System.out.println("【正在还款的借款页】--还款明细中:"+ clickRowCol(2,2).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中借款金额不正确:",clickRowCol(2,2).getText().trim().equals("借款金额：￥10000.00"));
		 
		 //验证借款利率
		 System.out.println("【正在还款的借款页】--还款明细中:"+ clickRowCol(3,1).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中借款利率不正确:",clickRowCol(3,1).getText().trim().equals("借款利率：700.00%"));
		 
		 //验证借款期限
		 System.out.println("【正在还款的借款页】--还款明细中:"+ clickRowCol(3,2).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中借款期限不正确:",clickRowCol(3,2).getText().trim().equals("借款期限：8天"));
		 
		 //还款方式
		 System.out.println("【正在还款的借款页】--还款明细中:"+ clickRowCol(4,1).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中还款方式不正确:",clickRowCol(4,1).getText().trim().equals("还款方式： 一次性还款"));
		 
		 //起息日期
		 System.out.println("【正在还款的借款页】--还款明细中:"+ clickRowCol(4,2).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中起息时间不正确:",clickRowCol(4,2).getText().trim().equals("起息日期：20170410"));
		 
		 //借款时间
//		 System.out.println("【正在还款的借款页】--还款明细中:"+ clickRowCol(4,3).getText().trim());
//		 assertTrue("【正在还款的借款页】--还款明细中借款时间不正确:",clickRowCol(4,3).getText().trim().equals("借款时间：20160811"));
		 
		 //点击还款
		 this.driver.findElement(By.linkText("还款")).click();
		 
		 //页面加载等待
		 this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath(".//*[@id='jbox-content']/div/div/div/div/div/div/table/tbody/tr[6]/td[2]/strong"));
	            }
	        });
	        System.out.println("需还总额为: "+ this.driver.findElement(By.xpath(".//*[@id='jbox-content']/div/div/div/div/div/div/table/tbody/tr[6]/td[2]/strong")).getText().trim());
	        assertTrue("需还总额不正确", this.driver.findElement(By.xpath(".//*[@id='jbox-content']/div/div/div/div/div/div/table/tbody/tr[6]/td[2]/strong")).getText().trim().equals("11534.25元"));
		    //点击关闭
			this.driver.findElement(By.xpath(".//*[@id='jbox']/table/tbody/tr[2]/td[2]/div/a")).click();
		 
		
		
	}
	

}
