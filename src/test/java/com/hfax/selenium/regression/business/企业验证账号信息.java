package com.hfax.selenium.regression.business;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.assertTrue;

import java.util.Map;

//登陆——企业验证账号信息
public class 企业验证账号信息 extends BusinessBaseTest {
	Map<String, String> envVars = System.getenv();
	//企业用户手机号
	protected String businessPhone(){
		return this.businessPhone;
	}
	//企业法人姓名
	protected String businessLegalName(){
		return this.businessLegalName;
	}
	//企业法人身份证号
	protected String businessIdentification(){
		return this.businessIdentification;
	}
	//公司名称
	protected String businessName(){
		return this.businessName;
	}
	//公司营业执照号
	protected String businessLicense(){
		return this.businessLicense;
	}
	//组织机构代码
	protected String businessCode(){
		return this.businessCode;
	}
	//基本户开户许可证核准号
	protected String businessPermit(){
		return this.businessPermit;
	}
	//企业用户名
	protected String businessUserName(){
		return this.businessUsername;
	}
    @Test
    public void 验证() throws Exception {
        //输入验证码
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("yanzheng0")));
        this.driver.findElement(By.id("yanzheng0")).sendKeys("1234");
        //输入手机号
        this.driver.findElement(By.id("cellphone")).sendKeys(businessPhone());
        //点击获取验证码
        this.driver.findElement(By.id("clickCode5")).click();
        //手机验证码
        this.driver.findElement(By.id("vilidataNum")).sendKeys("1234");
        //输入真实姓名，编码调整
        this.driver.findElement(By.id("realName")).sendKeys(businessLegalName());
        //输入您的证件类型
        Select selectCertificate = new Select(driver.findElement(By.id("idType")));
        selectCertificate.selectByVisibleText("其他");
        //输入身份证号
        this.driver.findElement(By.id("idNo")).sendKeys(businessIdentification());
        //输入公司姓名
        //编码调整
        String strBusinessname = new String(businessName().getBytes(),"UTF-8");
        this.driver.findElement(By.id("busname")).sendKeys(strBusinessname);
       
        //选择公司所在地
        Select selectProvince = new Select(driver.findElement(By.id("provncode")));
        selectProvince.selectByVisibleText("北京");


        //输入营业执照号
        this.driver.findElement(By.id("buslicen")).sendKeys(businessLicense());
        //输入组织机构代码
        this.driver.findElement(By.id("organcode")).sendKeys(businessCode());
        //输入基本户开户许可证核准号
        this.driver.findElement(By.id("permitnumber")).sendKeys(businessPermit());
        //输入法人姓名
        //编码调整
        this.driver.findElement(By.id("legalname")).sendKeys(businessLegalName());
        //法人证件类型
        Select selectCertificateType = new Select(driver.findElement(By.id("leglidType")));
        selectCertificateType.selectByVisibleText("其他");
        
        //输入法人身份证
        this.driver.findElement(By.id("leglidno")).sendKeys(businessIdentification());

        this.updateFile("imgname", "image2.jpg");

        this.updateFile("imgname1", "image2.jpg");

        this.updateFile("imgname2", "image2.jpg");

        this.updateFile("imgname3", "image2.jpg");

        this.updateFile("imgname4", "image2.jpg");

        this.updateFile("imgname5", "image2.jpg");
        
        Select selectCity = new Select(driver.findElement(By.id("citycode")));
        selectCity.selectByVisibleText("海淀");

        //点击确认提交
        this.driver.findElement(By.id("jc_btn")).click();
        
        //点击确定
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
		        Alert alert = driver.switchTo().alert();  
		        String str = alert.getText();  
				return str.trim().contains("企业信息保存成功");
			}
		});
        this.driver.switchTo().alert().accept();

        //验证页面切换
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	 try {
        			Thread.sleep(2000);
        			//title是BUG,已经通知功能那边
//        			 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[4]/div/div[1]")));      
//                   assertTrue("验证账号信息失败", this.driver.getTitle().contains("惠金所"));
        			this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='regOkTip']")));
                	assertTrue("验证企业账号信息失败", this.driver.findElement(By.xpath("//div[@class='regOkTip']")).getText().trim().contains("您好，您注册的账号为 "+businessUserName()+",账户注册申请已提交！"));
                	
                  } catch (InterruptedException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
                  }	        	       		
        	
        }else{
        	
        	//title是BUG,已经通知功能那边
//        	  this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[4]/div/div[1]")));  
//            assertTrue("验证账号信息失败", this.driver.getTitle().contains("惠金所"));
       
        	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='regOkTip']")));
        	assertTrue("验证企业账号信息失败", this.driver.findElement(By.xpath("//div[@class='regOkTip']")).getText().trim().contains("您好，您注册的账号为 "+businessUserName()+",账户注册申请已提交！"));
        																														
        }
      
        
        
   
    }
}