package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;
import org.junit.runners.MethodSorters;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;
import com.hfax.selenium.rule.SuddenDeathRule;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 惠理财投资产品信息验证页 extends AccountBaseTest{
	/**
	 * @Description 惠理财投资产品信息验证页
	 * Created by songxq on 9/8/16.
	 */

	
	protected WebElement clickOnSubmenu(String title) {
	        WebElement submenu = this.driver.findElement(By.id("listTab-box"));
	        submenu.findElement(By.linkText(title)).click();

	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'惠理财')]")));

	        List<WebElement> elements = this.driver.findElements(By.className("list-control"));

	        for (WebElement el : elements) {
	            if (el.isDisplayed()) return el;
	        }

	        assertTrue("无内容显示: " + title, false);

	        return null;
	    }
	
	//验证客户投资理财产品
	@Test
	public void test001_客户投资理财产品() throws AWTException {
	    //进入惠理财页面
	    this.navigateToPage("投资理财");
	    
	  //页面加载等待
    	new WebDriverWait(this.driver,20).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[@class='benefit-sider-percent']")));
    	
    	List<WebElement> listFirst = this.driver.findElements(By.xpath("//p[@class='benefit-sider-percent']"));
    	//验证预期年化利率---20160809
    	 assertTrue("惠理财投资页【预期年化利率】不正确",listFirst.get(0).getText().trim().equals("700.00%"));
    	 System.out.println("惠理财投资页【预期年化利率】为：" + listFirst.get(0).getText().trim());
    	 
    	 List<WebElement> listSecond = this.driver.findElements(By.xpath("//p[@class='benefit-sider-date']"));
    	 //验证投资期限
    	 assertTrue("惠理财投资页【投资期限】不正确",listSecond.get(0).getText().trim().equals("8 天"));
    	 System.out.println("惠理财投资页【投资期限】为: "+ listSecond.get(0).getText().trim());
    	 
    	 //验证起投金额
    	 assertTrue("惠理财投资页【起投金额】不正确",listSecond.get(1).getText().trim().equals("1百元"));
    	 System.out.println("惠理财投资页【起投金额】为: " + listSecond.get(1).getText().trim());
    	 
    	 //验证还款方式
    	 assertTrue("惠理财投资页【还款方式】不正确",listSecond.get(2).getText().trim().equals("一次性还款"));
    	 System.out.println("惠理财投资页【还款方式】为: " + listSecond.get(2).getText().trim());
    	 
    	 //验证项目总金额---20160809
    	 WebElement listAmount = this.driver.findElements(By.xpath("//p[@class='total-mon listAmount']/i")).get(0);
    	 assertTrue("惠理财投资页【项目总金额】不正确: ",listAmount.getText().trim().equals("1万元"));
    	 System.out.println("惠理财投资页【项目总金额】为: " + listAmount.getText().trim());

	    
	    WebElement content = this.clickOnSubmenu("惠理财");
	    
	    //点击第一个产品
	    WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
        firstItem.findElement(By.xpath("div[2]/div[2]/a")).click();
        
        
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='balanceTitle']")));
        
        //验证项目金额---20160808
        assertTrue("惠理财投资详细页【项目金额】不正确: ",this.driver.findElement(By.xpath("//li[@class='amount']/i/strong")).getText().trim().equals("10,000.00"));
        System.out.println("惠理财投资详细页【项目金额】为: " + this.driver.findElement(By.xpath("//li[@class='amount']/i/strong")).getText());
        
        //验证预期年化收益率
       assertTrue("惠理财投资详细页【预期年化收益率】不正确: ",this.driver.findElement(By.xpath("//li[@class='interest']/i/strong")).getText().trim().equals("700.00"));
       System.out.println("惠理财投资详细页【预期年化收益率】为: "+this.driver.findElement(By.xpath("//li[@class='interest']/i/strong")).getText().trim() + "%");
        
        //验证投资期限
       	assertTrue("惠理财投资详细页【投资期限】不正确: ",this.driver.findElement(By.xpath("//li[@class='term']/i/strong")).getText().trim().equals("8"));
       	System.out.println("惠理财投资详细页【投资期限】为: "+ this.driver.findElement(By.xpath("//li[@class='term']/i/strong")).getText()+"天");
        
        //验证计息日
        assertTrue("惠理财投资详细页【计息日】不正确: ",this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[2]/li[3]/em")).getText().trim().equals("2017-04-10"));
        System.out.println("惠理财投资详细页【计息日】为: " + this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[2]/li[3]/em")).getText());
       
        //验证收益方式(前台已经写死没有标签)
//        System.out.println("新手专区投资详细页收益方式: "+this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[2]/li[4]/span")).getText().trim());
//        assertTrue("新手专区投资详细页收益方式不正确：",this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[2]/li[4]/span")).getText().trim().equals("一次性还本付息"));
        
        
        //验证起投金额
        assertTrue("惠理财投资详细页【起投金额】不正确：",this.driver.findElement(By.xpath("//li/em[@class='qitou']")).getText().trim().equals("1百元"));
        System.out.println("惠理财投资详细页【起投金额】为: " + this.driver.findElement(By.xpath("//li/em[@class='qitou']")).getText());
        
        //验证最大投资金额---20160808
        assertTrue("惠理财投资详细页【最大投资金额】不正确: ",this.driver.findElements(By.xpath("//li/em[@class='qitou']")).get(1).getText().trim().equals("1万元"));
        System.out.println("惠理财投资详细页【最大投资金额】为: " + this.driver.findElements(By.xpath("//li/em[@class='qitou']")).get(1).getText());

        //验证剩余可投金额
        assertTrue("剩余可投金额不存在", this.driver.findElement(By.xpath("//div[@class='balanceTitle']")).isDisplayed());

        //验证项目概况
        assertTrue("项目概况不存在", this.driver.findElement(By.id("one1")).isDisplayed());
        this.driver.findElement(By.id("one1")).click();

        //验证投资说明书
        assertTrue("投资说明书不存在", this.driver.findElement(By.id("one2")).isDisplayed());
        this.driver.findElement(By.id("one2")).click();

        //验证风险提示书
        assertTrue("风险提示书不存在", this.driver.findElement(By.id("one3")).isDisplayed());
        this.driver.findElement(By.id("one3")).click();

        //验证投资管理合同
        assertTrue("风险提示书不存在", this.driver.findElement(By.id("one4")).isDisplayed());
        this.driver.findElement(By.id("one4")).click();

        WebElement amount = this.driver.findElement(By.xpath(".//*[@id='amount']"));
        assertTrue("投资理财金额不存在", amount.isDisplayed());
        amount.clear();
        amount.sendKeys(this.userremaininginvestamount);
        
        
        //验证预期收益----20160808
        assertTrue("惠理财投资详细页【预期收益】不存在" ,this.driver.findElement(By.xpath(".//*[@id='interests']")).getText().trim().equals("15.34"));
        System.out.println("惠理财投资详细页【预期收益】为: " + this.driver.findElement(By.xpath(".//*[@id='interests']")).getText());

        this.driver.findElement(By.xpath(".//*[@id='agre']")).click();

        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("去投资")));
        this.driver.findElement(By.linkText("去投资")).click();
        
        //验证实际投资本金(元)-----20160808
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='sjtzbj']")));
        assertTrue("惠理财确认页【实际投资本金】不正确: ",this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim().equals("100.00"));
        System.out.println("惠理财确认页【实际投资本金(元)】为: " + this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim());
 
       //验证预期收益率(%)
        assertTrue("惠理财确认页【总预期收益率(%)】不正确: ",this.driver.findElement(By.xpath(".//*[@id='yqsyl']")).getText().trim().equals("700.00"));
        System.out.println("惠理财确认页【总预期收益率(%)】: " + this.driver.findElement(By.xpath(".//*[@id='yqsyl']")).getText()+"%");
        
        
        //总预期收益(元)
        assertTrue("惠理财确认页【总预期收益(元)】不正确: ",this.driver.findElement(By.xpath(".//*[@id='yqsy']")).getText().trim().equals("15.34"));
        System.out.println("惠理财确认页【总预期收益(元)】: " + this.driver.findElement(By.xpath(".//*[@id='yqsy']")).getText().trim());
        
        //实际支付金额(元)-----20160808
        assertTrue("惠理财确认页【实际支付金额(元)】不正确: ",this.driver.findElement(By.id("sjzfje")).getText().trim().equals("100.00"));
        System.out.println("惠理财确认页【实际支付金额(元)】:"+this.driver.findElement(By.id("sjzfje")).getText().trim());
        
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='tpwdid']")));
        //投资红包
        if(this.IsElementPresent(By.xpath("//div[@id='usecoupon']/div/input[1]")))
        {
        	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='usecoupon']/div/input[1]")));
        	this.driver.findElement(By.xpath("//div[@id='usecoupon']/div/input[1]")).click();
        	this.driver.findElement(By.xpath("//input[@id='tpwdid']")).clear();
        	this.driver.findElement(By.xpath("//input[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
        	}
        else{
        	//输入交易密码
        	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='tpwdid']")));
        	this.driver.findElement(By.xpath("//input[@id='tpwdid']")).clear();
        	this.driver.findElement(By.xpath("//input[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
        }
        
        //点击确定投资
        new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@id='besurebutton']/button[1]")).isDisplayed();
            }
        });
          this.driver.findElement(By.xpath("//div[@id='besurebutton']/button[1]")).click();
          
        //投资成功pop窗口
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@id='investSuccess']/div[2]/p")).isDisplayed();
            }
        });
        assertTrue("投资成功跳出的窗口不存在",this.driver.findElement(By.xpath("//div[@id='investSuccess']/div[2]/p")).isDisplayed());
        
        //验证本项目投资金额
        assertTrue("惠理财确认提示框【本项目投资金额】不正确: ",this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim().equals("100"));
        System.out.println("惠理财确认提示框【本项目投资金额】:" + this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim() + "元");
       //验证本项目累计投资金额
        assertTrue("惠理财确认提示框【本项目累计投资金额】不正确: ",this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim().equals("100.00"));
        System.out.println("惠理财确认提示框【本项目累计投资金额】:" + this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim() + "元");
        //验证本项目累计投资预计收益
        assertTrue("惠理财确认提示框【本项目累计投资预计收益】不正确: ",this.driver.findElement(By.xpath(".//*[@id='forpay']")).getText().trim().equals("15.34"));
        System.out.println("惠理财确认提示框【本项目累计投资预计收益】:"+ this.driver.findElement(By.xpath(".//*[@id='forpay']")).getText().trim() + "元");
        
        //关闭pop窗口
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='investSuccess']/div[2]/b/a")));
        this.driver.findElement(By.xpath("//div[@id='investSuccess']/div[2]/b/a")).click();
       
	}

	
	//验证惠理财页面投资的产品
	@Test 
	public void test002_验证惠理财页面投资的产品(){
	   //等待我的主页
       this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("我的主页")));
	  
      //我的投资-》惠理财
	  this.selectMenu(10, "我的投资");
	  this.selectSubMenu(1, "惠理财");
	  
	  //验证我的账户有投资的产品存在
	  this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//tr//td[@align='center']//a[@target='_blank']")));
	  List<WebElement> list = this.driver.findElements(By.xpath("//tr//td[@align='center']//a[@target='_blank']")); 
	  assertTrue("不存在惠理财的产品",list.size()>0);
	 
	}
	
	//搜索惠理财页面的投资产品
	@Test
	public void test003_搜索惠理财页面的投资产品(){
		//我的投资->惠理财
		this.selectMenu(10, "我的投资");
		this.selectSubMenu(1, "惠理财");
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("borrowstatus")));
		
		//输入起息时间
		assertTrue("起息时间不存在",this.driver.findElement(By.id("interestDateStart")).isDisplayed());
	    JavascriptExecutor removeAttribute = (JavascriptExecutor)this.driver;
        removeAttribute.executeScript("var setDate=document.getElementById(\"interestDateStart\");setDate.removeAttribute('readonly');");
        Date date = new Date();
        String datestart = this.setDate(date, -1);
        this.driver.findElement(By.xpath(".//*[@id='interestDateStart']")).sendKeys(datestart);
        
        //输入到期时间
        assertTrue("到期时间不存在",this.driver.findElement(By.id("interestDateEnd")).isDisplayed());
        JavascriptExecutor removeAttributeend = (JavascriptExecutor)this.driver;
        removeAttributeend.executeScript("var setDate=document.getElementById(\"interestDateEnd\");setDate.removeAttribute('readonly');");
        String dateend = this.setDate(date, 730);
        this.driver.findElement(By.xpath(".//*[@id='interestDateEnd']")).sendKeys(dateend);
        
        //下拉菜单，选择正在找标中
        assertTrue("请选择下拉框不存在",this.driver.findElement(By.id("borrowstatus")).getText().contains("--请选择--"));
        Select select = new Select(this.driver.findElement(By.id("borrowstatus")));
        for (int i = 0; i < select.getOptions().size(); i++) {
            if(select.getOptions().get(i).getText().equals("正在招标中"))
            {
            	select.getOptions().get(i).click();
            	break;
            }
        }
        
        //点击搜索
        assertTrue("搜索不存在",this.driver.findElement(By.id("search")).isDisplayed());
        this.driver.findElement(By.id("search")).click();
        
        //验证惠理财产品列表不为空
        List<WebElement> list = this.driver.findElements(By.xpath("//tr//td[@align='center']//a[@target='_blank']")); 
  	    assertTrue("不存在惠理财产品",list.size()>0);
  	    
  	  //验证本金（元）-----20160809
        assertTrue("【惠理财搜索页面】本金（元）不正确",this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[2]")).getText().trim().equals("100.00"));
        System.out.println("【惠理财搜索页面】本金（元）为: " + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[2]")).getText().trim() + "元");
        
        //验证预计收益（元）
        assertTrue("【惠理财搜索页面】预计收益（元）不正确",this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[3]")).getText().trim().equals("15.34"));
        System.out.println("【惠理财搜索页面】预计收益(元)为: " + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[3]")).getText().trim()+ "元");
        
        //验证起息时间
        assertTrue("【惠理财搜索页面】起息时间不正确",this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[5]")).getText().trim().equals("20170410"));
        System.out.println("【惠理财搜索页面】起息时间为:" + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[5]")).getText().trim());
        
        //验证到期时间 
        assertTrue("【惠理财搜索页面】到期时间不正确",this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[6]")).getText().trim().equals("20170418"));
        System.out.println("【惠理财搜索页面】到期时间为:" + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[6]")).getText().trim());
        
        //验证状态-----20160809
        assertTrue("【惠理财搜索页面】状态不正确",this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[7]")).getText().trim().equals("正在招标中"));
        System.out.println("【惠理财搜索页面】状态为:" + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[7]")).getText().trim());
        
  	    //关键字随机验证,点击搜索
  	    assertTrue("关键字不存在",this.driver.findElement(By.id("titles")).isDisplayed());
  	    this.driver.findElement(By.id("titles")).clear();
  	    this.driver.findElement(By.id("titles")).sendKeys("1111111");
  	    this.driver.findElement(By.id("search")).click();
  	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//tr[@align='center']/td")));
  	    String message = this.driver.findElement(By.xpath("//tr[@align='center']/td")).getText().trim();
	    assertTrue("存在数据产品数据不正确",message.equals("暂无数据"));
	}
	  
	
}
