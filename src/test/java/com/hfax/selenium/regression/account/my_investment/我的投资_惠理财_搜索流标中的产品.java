package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 我的投资_惠理财_搜索流标中的产品 extends 我的投资_惠理财_搜索惠理财的产品 {

	/**
	 * @Description 我的投资_惠理财_搜索惠理财的产品
	 * Created by songxq on 30/5/16.
	 */

	@Test
	public void 搜索流标中的产品() {
		//等待我的主页
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("我的主页")));
        
        //我的投资->惠理财
        this.selectMenu(10, "我的投资");
        this.selectSubMenu(1, "惠理财");
        
         //选择正在招标中的产品
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("borrowstatus")));
        assertTrue("请选择下拉框不存在",this.driver.findElement(By.id("borrowstatus")).getText().contains("--请选择--"));
        this.searchFinancePro("流标");
        
        //点击搜索
        assertTrue("搜索不存在",this.driver.findElement(By.id("search")).isDisplayed());
        this.driver.findElement(By.id("search")).click();
        
        //验证惠理财产品列表不为空
        List<WebElement> list = this.driver.findElements(By.xpath("//tr//td[@align='center']//a[@target='_blank']")); 
  	    assertTrue("不存在惠理财产品",list.size()>0);
       
	}

}
