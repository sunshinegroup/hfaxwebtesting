package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;
import org.junit.runners.MethodSorters;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;
import com.hfax.selenium.rule.SuddenDeathRule;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 我的投资_惠理财_投资理财产品 extends AccountBaseTest{
	/**
	 * @Description 我的投资_惠理财_投资理财产品
	 * Created by songxq on 26/4/16.
	 */

	
	protected WebElement clickOnSubmenu(String title) {
	        WebElement submenu = this.driver.findElement(By.id("listTab-box"));
	        submenu.findElement(By.linkText(title)).click();

	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'惠理财')]")));

	        List<WebElement> elements = this.driver.findElements(By.className("list-control"));

	        for (WebElement el : elements) {
	            if (el.isDisplayed()) return el;
	        }

	        assertTrue("无内容显示: " + title, false);

	        return null;
	    }
	
	//验证客户投资理财产品
	@Test
	public void test001_客户投资理财产品() throws AWTException {
	    //进入惠理财页面
	    this.navigateToPage("投资理财");
	    WebElement content = this.clickOnSubmenu("惠理财");
	    
	    //点击第一个产品
	    WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
        firstItem.findElement(By.xpath("div[2]/div[2]/a")).click();
        
        
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='balanceTitle']")));

        //验证剩余可投金额
        assertTrue("剩余可投金额不存在", this.driver.findElement(By.xpath("//div[@class='balanceTitle']")).isDisplayed());

        //验证项目概况
        assertTrue("项目概况不存在", this.driver.findElement(By.id("one1")).isDisplayed());
        this.driver.findElement(By.id("one1")).click();

        //验证投资说明书
        assertTrue("投资说明书不存在", this.driver.findElement(By.id("one2")).isDisplayed());
        this.driver.findElement(By.id("one2")).click();

        //验证风险提示书
        assertTrue("风险提示书不存在", this.driver.findElement(By.id("one3")).isDisplayed());
        this.driver.findElement(By.id("one3")).click();

        //验证投资管理合同
        assertTrue("风险提示书不存在", this.driver.findElement(By.id("one4")).isDisplayed());
        this.driver.findElement(By.id("one4")).click();

        WebElement amount = this.driver.findElement(By.xpath(".//*[@id='amount']"));
        assertTrue("投资理财金额不存在", amount.isDisplayed());
        amount.clear();
        amount.sendKeys(this.userinvestamount);

        this.driver.findElement(By.xpath(".//*[@id='agre']")).click();

        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("去投资")));
        this.driver.findElement(By.linkText("去投资")).click();
        
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='tpwdid']")));
        //投资红包
        if(this.IsElementPresent(By.xpath("//div[@id='usecoupon']/div/input[1]")))
        {
        	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='usecoupon']/div/input[1]")));
        	this.driver.findElement(By.xpath("//div[@id='usecoupon']/div/input[1]")).click();
        	this.driver.findElement(By.xpath("//input[@id='tpwdid']")).clear();
        	this.driver.findElement(By.xpath("//input[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
        	}
        else{
        	//输入交易密码
        	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='tpwdid']")));
        	this.driver.findElement(By.xpath("//input[@id='tpwdid']")).clear();
        	this.driver.findElement(By.xpath("//input[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
        }
        
        //点击确定投资
        new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@id='besurebutton']/button[1]")).isDisplayed();
            }
        });
          this.driver.findElement(By.xpath("//div[@id='besurebutton']/button[1]")).click();
          
        //投资成功pop窗口
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@id='investSuccess']/div[2]/p")).isDisplayed();
            }
        });
        assertTrue("投资成功跳出的窗口不存在",this.driver.findElement(By.xpath("//div[@id='investSuccess']/div[2]/p")).isDisplayed());
        
        //关闭pop窗口
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='investSuccess']/div[2]/b/a")));
        this.driver.findElement(By.xpath("//div[@id='investSuccess']/div[2]/b/a")).click();
       
	}

	
	//验证惠理财页面投资的产品
	@Test 
	public void test002_验证惠理财页面投资的产品(){
	   //等待我的主页
       this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("我的主页")));
	  
      //我的投资-》惠理财
	  this.selectMenu(10, "我的投资");
	  this.selectSubMenu(1, "惠理财");
	  
	  //验证我的账户有投资的产品存在
	  this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//tr//td[@align='center']//a[@target='_blank']")));
	  List<WebElement> list = this.driver.findElements(By.xpath("//tr//td[@align='center']//a[@target='_blank']")); 
	  assertTrue("不存在惠理财的产品",list.size()>0);
	 
	}
	
	//搜索惠理财页面的投资产品
	@Test
	public void test003_搜索惠理财页面的投资产品(){
		//我的投资->惠理财
		this.selectMenu(10, "我的投资");
		this.selectSubMenu(1, "惠理财");
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("borrowstatus")));
		
		//输入起息时间
		assertTrue("起息时间不存在",this.driver.findElement(By.id("interestDateStart")).isDisplayed());
	    JavascriptExecutor removeAttribute = (JavascriptExecutor)this.driver;
        removeAttribute.executeScript("var setDate=document.getElementById(\"interestDateStart\");setDate.removeAttribute('readonly');");
        Date date = new Date();
        String datestart = this.setDate(date, -1);
        this.driver.findElement(By.xpath(".//*[@id='interestDateStart']")).sendKeys(datestart);
        
        //输入到期时间
        assertTrue("到期时间不存在",this.driver.findElement(By.id("interestDateEnd")).isDisplayed());
        JavascriptExecutor removeAttributeend = (JavascriptExecutor)this.driver;
        removeAttributeend.executeScript("var setDate=document.getElementById(\"interestDateEnd\");setDate.removeAttribute('readonly');");
        String dateend = this.setDate(date, 730);
        this.driver.findElement(By.xpath(".//*[@id='interestDateEnd']")).sendKeys(dateend);
        
        //下拉菜单，选择正在找标中
        assertTrue("请选择下拉框不存在",this.driver.findElement(By.id("borrowstatus")).getText().contains("--请选择--"));
        Select select = new Select(this.driver.findElement(By.id("borrowstatus")));
        for (int i = 0; i < select.getOptions().size(); i++) {
            if(select.getOptions().get(i).getText().equals("正在招标中"))
            {
            	select.getOptions().get(i).click();
            	break;
            }
        }
        
        //点击搜索
        assertTrue("搜索不存在",this.driver.findElement(By.id("search")).isDisplayed());
        this.driver.findElement(By.id("search")).click();
        
        //验证惠理财产品列表不为空
        List<WebElement> list = this.driver.findElements(By.xpath("//tr//td[@align='center']//a[@target='_blank']")); 
  	    assertTrue("不存在惠理财产品",list.size()>0);
        
  	    //关键字随机验证,点击搜索
  	    assertTrue("关键字不存在",this.driver.findElement(By.id("titles")).isDisplayed());
  	    this.driver.findElement(By.id("titles")).clear();
  	    this.driver.findElement(By.id("titles")).sendKeys("1111111");
  	    this.driver.findElement(By.id("search")).click();
  	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//tr[@align='center']/td")));
  	    String message = this.driver.findElement(By.xpath("//tr[@align='center']/td")).getText().trim();
	    assertTrue("存在数据产品数据不正确",message.equals("暂无数据"));
	}
	  
	
}
