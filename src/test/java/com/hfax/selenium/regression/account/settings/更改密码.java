package com.hfax.selenium.regression.account.settings;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;
/**
 * 
 * @author changwj
 * @Description 我的账户--用户设置--设置新密码
 */
public class 更改密码 extends AccountBaseTest{
	private void setupFormFields(String oldPassword, String newPassword, String confirmPassword) {
		this.driver.findElement(By.id("oldPassword")).sendKeys(oldPassword);
		this.driver.findElement(By.id("newPassword")).sendKeys(newPassword);
		this.driver.findElement(By.id("confirmPassword")).sendKeys(confirmPassword);
		this.driver.findElement(By.className("bcbtn")).click();
	}

	private String waitForErrorMessage() {
		WebDriverWait wait = new WebDriverWait(driver, 3000);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return webDriver.findElement(By.id("s_tip")).isDisplayed();
			}
		});

		return this.driver.findElement(By.id("s_tip")).getText();
	}

	/**
	 * 用户设置---登录密码管理   
	 * 校验新密码是否为空
	 * 提示:旧密码错误
	 */
	@Test
	public void 校验新密码是否为空() {
		用户设置();
		this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/ul/li[2]")).click();
		String text = this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div/table/tbody/tr[1]/th")).getText();
		assertTrue("获取登录密码失败", "登录密码修改".equals(text));

		this.setupFormFields(this.password, "", "");
		String errorMessage = this.waitForErrorMessage();

	    assertTrue("错误信息不对", errorMessage.equals("请完整填写密码信息"));
	}

	/**
	 * 用户设置-- 校验新密码是否符合规则
	 * 新密码:123
	 * 确认密码:123
	 * 提示:密码长度必须为8-16个数字与大写或者小写字母的组合
	 */
	@Test
	public void 校验密码是否符合规则() {
		用户设置();
		this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/ul/li[2]")).click();
		String text = this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div/table/tbody/tr[1]/th")).getText();
		assertTrue("获取登录密码失败", "登录密码修改".equals(text));

        this.setupFormFields(this.password, "123", "123");
        String errorMessage = this.waitForErrorMessage();

	    assertTrue("错误信息不对", errorMessage.equalsIgnoreCase("密码长度必须为8-16个数字与大写或者小写字母的组合"));
		
	}

	/**
	 * 用户设置--校验新密码与确认密码是否一致
	 * 新密码:11111111a
	 * 确认密码:q00000
	 * 提示:两次密码不一致
	 */
	@Test
	public void 校验新确认密码是否一致(){
		用户设置();
		this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/ul/li[2]")).click();
		String text = this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div/table/tbody/tr[1]/th")).getText();
		assertTrue("获取登录密码失败", "登录密码修改".equals(text));

        this.setupFormFields(this.password, this.password + "1", this.password + "2");
        String errorMessage = this.waitForErrorMessage();

		assertTrue("错误信息不对", errorMessage.equalsIgnoreCase("两次密码不一致"));
	}

	/**
	 * 
	 * 设置新密码成功
	 */
	@Test
	public void 设置新密码(){
		用户设置();
		this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/ul/li[2]")).click();
		String text = this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div/table/tbody/tr[1]/th")).getText();
		assertTrue("获取登录密码失败", "登录密码修改".equals(text));

        this.setupFormFields(this.password, this.password + "1", this.password + "1");

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.switchTo().alert().getText().equalsIgnoreCase("修改密码成功");
            }
        });

        this.driver.switchTo().alert().accept();

        this.setupFormFields(this.password + "1", this.password, this.password);

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.switchTo().alert().getText().equalsIgnoreCase("修改密码成功");
            }
        });

        this.driver.switchTo().alert().accept();


	}
}
