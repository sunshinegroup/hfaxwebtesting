package com.hfax.selenium.regression.account.transfer;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.base.SuddenDeathBaseTest;
import com.hfax.selenium.regression.account.AccountBaseTest;
import com.hfax.selenium.rule.SuddenDeathRule;

public class 投资转让_已转入的项目查看   extends SuddenDeathBaseTest{

	/**
	 * @Description 投资转让_已转入的项目查看
	 * Created by songxq on 29/4/16.
	 */
	
	@Override
	public void setup() throws Exception {
		super.setup();
		this.loadPage();
		this.transferLogin();
    }
	@Test
	public void 已转入的项目查看() throws Exception {
		//投资转让->已转入的项目
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("转让专区")));
		 this.selectMenu(14, "投资转让");
		 this.selectSubMenu(4, "已转入的项目");
		 
		//加载已转入的项目
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='con_one_4']/div[1]/h2")));
		assertTrue("用户没有【已转入的项目】", this.driver.findElement(By.xpath("//div[@id='con_one_4']/div[1]/h2")).isDisplayed());
		
		//项目详细信息
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='hover']")));
		assertTrue("用户没有【已转入的项目】中有详细信息", this.driver.findElement(By.xpath("//span[@class='hover']")).isDisplayed());
		WebElement detail = this.driver.findElement(By.xpath("//span[@class='hover']"));
		//点击项目详细信息
		detail.click();
		//关闭详细信息
		detail.click();
		
		//验证转让协议
		assertTrue("用户没有【已转入的项目】中有转让协议", this.driver.findElement(By.linkText("转让协议")).isDisplayed());
		WebElement protocol =  this.driver.findElement(By.linkText("转让协议"));
		//点击转让协议
		protocol.click();
	}

}
