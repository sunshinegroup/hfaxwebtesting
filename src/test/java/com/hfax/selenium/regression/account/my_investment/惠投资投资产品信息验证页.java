package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 惠投资投资产品信息验证页 extends AccountBaseTest{

	/**
	 * @Description 惠投资投资产品信息验证页
	 * Created by songxq on 9/8/16.
	 * 
	 */
	public void  searchFinanceProhui(String proName){
	Select select = new Select(this.driver.findElement(By.id("borrowstatus")));
    for (int i = 0; i < select.getOptions().size(); i++) {
        if(select.getOptions().get(i).getText().equals(proName))
        {
        	select.getOptions().get(i).click();
        	break;
        }
     }
  }

	@Test
	public void test001_客户投资惠投资的产品() {
		this.navigateToPage("新手专区");
		//点击惠投资
		WebElement content = this.clickOnSubmenu("惠投资");
        WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
        
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='display_6']/div/div[2]")));
        
    	 WebElement yearRateFirst = this.driver.findElement(By.xpath("//p[@class='benefit-sider-percent benefit-gufu']"));
    	//验证预期年化利率---20160809
    	 assertTrue("惠投资投资页【预期年化利率】不正确",yearRateFirst.getText().trim().equals("6,00.00%~7,00.00%"));
    	 System.out.println("惠投资投资页【预期年化利率】为：" +yearRateFirst.getText().trim());
    	 
    	 List<WebElement> listSecond = this.driver.findElements(By.xpath("//p[@class='benefit-sider-date']"));
    	 //验证投资期限
    	 assertTrue("惠投资投资页【投资期限】不正确",listSecond.get(0).getText().trim().equals("333 天"));
    	 System.out.println("惠投资投资页【投资期限】为: "+ listSecond.get(0).getText().trim());
    	 
    	 //验证起投金额
    	 assertTrue("惠投资投资页【起投金额】不正确",this.driver.findElement(By.xpath("//p[@class='benefit-sider-date']/i")).getText().trim().equals("1百元"));
    	 System.out.println("惠投资投资页【起投金额】为: " + this.driver.findElement(By.xpath("//p[@class='benefit-sider-date']/i")).getText().trim());
    	 
    	 //验证还款方式
    	 List<WebElement> list = this.driver.findElements(By.xpath("//p[@class='benefit-sider-date']/span"));
    	 assertTrue("惠投资投资页【还款方式】不正确",list.get(1).getText().trim().equals("一次性还款"));
    	 System.out.println("惠投资投资页【还款方式】为: " + list.get(1).getText().trim());
    	 
    	 //验证项目总金额---20160809
    	 WebElement listAmount = this.driver.findElements(By.xpath("//p[@class='total-mon listAmount']/i")).get(0);
    	 assertTrue("惠投资投资页【项目总金额】不正确: ",listAmount.getText().trim().equals("1万元"));
    	 System.out.println("惠投资投资页【项目总金额】为: " + listAmount.getText().trim());
        
         //点击惠投资第一个产品
        assertTrue("惠投资第一个产品不存在", this.driver.findElement(By.xpath("//div[@id='display_6']/div/div[2]")).isDisplayed());
        firstItem.findElement(By.xpath("i/div/div[2]/a")).click();
        
        //项目介绍
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[@id='one1']")));
       
       //验证项目金额---20160808
        assertTrue("惠投资投资详细页【项目金额】不正确: ",this.driver.findElement(By.xpath("//strong[@class='object-money']")).getText().trim().equals("10,000.00"));
        System.out.println("惠投资投资详细页【项目金额】为: " + this.driver.findElement(By.xpath("//strong[@class='object-money']")).getText());
        
        //验证预期年化收益率
       assertTrue("惠投资投资详细页【预期年化收益率】不正确: ",this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[1]/li[2]/i/strong")).getText().trim().equals("600.00%~700.00%"));
       System.out.println("惠投资投资详细页【预期年化收益率】为: "+this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[1]/li[2]/i/strong")).getText().trim());
        
        //验证投资期限
       	assertTrue("惠投资投资详细页【投资期限】不正确: ",this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[1]/li[3]/i/strong")).getText().trim().equals("333"));
       	System.out.println("惠投资投资详细页【投资期限】为: "+ this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[1]/li[3]/i/strong")).getText()+"天");
        
        //验证计息日
        assertTrue("惠投资投资详细页【计息日】不正确: ",this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[2]/li[3]/em")).getText().trim().equals("2017-01-02"));
        System.out.println("惠投资投资详细页【计息日】为: " + this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[2]/li[3]/em")).getText());
       
        //验证收益方式(前台已经写死没有标签)
//        System.out.println("新手专区投资详细页收益方式: "+this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[2]/li[4]/span")).getText().trim());
//        assertTrue("新手专区投资详细页收益方式不正确：",this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[2]/li[4]/span")).getText().trim().equals("一次性还本付息"));
        
        
        //验证起投金额
        assertTrue("惠投资投资详细页【起投金额】不正确：",this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[1]/li[4]/i/strong")).getText().trim().equals("100.00"));
        System.out.println("惠投资投资详细页【起投金额】为: " + this.driver.findElement(By.xpath("html/body/div[5]/div[2]/div[1]/ul[1]/li[4]/i/strong")).getText());
        
        //验证累计投资上限---20160808
        assertTrue("惠投资投资详细页【累计投资上限】不正确: ",this.driver.findElement(By.xpath("//em[@class='qitou']")).getText().trim().equals("1万元"));
        System.out.println("惠投资投资详细页【累计投资上限】为: " + this.driver.findElement(By.xpath("//em[@class='qitou']")).getText());
        
        //项目介绍
        this.driver.findElement(By.xpath("//strong[@id='one1']")).click();
        assertTrue("项目介绍不存在", this.driver.findElement(By.xpath("//strong[@id='one1']")).isDisplayed());
        
        //投资说明书
        this.driver.findElement(By.xpath("//strong[@id='one2']")).click();
        assertTrue("投资说明书不存在", this.driver.findElement(By.xpath("//strong[@id='one2']")).isDisplayed());
        
        //风险提示书
        this.driver.findElement(By.xpath("//strong[@id='one3']")).click();
        assertTrue("风险提示书不存在", this.driver.findElement(By.xpath("//strong[@id='one3']")).isDisplayed());
        
        //投资管理合同
        this.driver.findElement(By.xpath("//strong[@id='one4']")).click();
        assertTrue("投资管理合同不存在", this.driver.findElement(By.xpath("//strong[@id='one4']")).isDisplayed());
        
        //定位到输入金额文本框
         WebElement target = this.driver.findElement(By.xpath("//div[@id='content']/ul/li/strong"));
        ((JavascriptExecutor)this.driver).executeScript("arguments[0].scrollIntoView();", target);
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='content']/ul/li/strong")));
        
        //输入投资金额
        assertTrue("投资金额输入框不存在", this.driver.findElement(By.xpath("//input[@id='amount']")).isDisplayed());
        WebElement input = this.driver.findElement(By.xpath("//input[@id='amount']"));
        input.clear();
        input.sendKeys(this.userremaininginvestamount);
        
        //勾选已阅读并同意《定向委托投资管理协议》
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='agre']")));
        assertTrue("《定向委托投资管理协议》不存在",this.driver.findElement(By.xpath("//input[@id='agre']")).isDisplayed());
        this.driver.findElement(By.xpath("//input[@id='agre']")).click();
        
        //点击去投资
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@id='btnsave']/i")));
        
        //验证预期收益----20160808
        assertTrue("惠投资投资详细页【预期收益】不存在" ,this.driver.findElement(By.xpath(".//*[@id='interests']")).getText().trim().equals("555.00 ~ 647.50"));
        System.out.println("惠投资投资详细页【预期收益】为: " + this.driver.findElement(By.xpath(".//*[@id='interests']")).getText());
        this.driver.findElement(By.xpath("//a[@id='btnsave']/i")).click();
        
      //等待交易密码
        new WebDriverWait(this.driver,15).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return !webDriver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim().equals("0.00");
            }
        });
        
      //验证实际投资本金(元)-----20160808
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='sjtzbj']")));
        assertTrue("惠投资确认页【实际投资本金(元)】不正确: ",this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim().equals("100.00"));
        System.out.println("惠投资确认页【实际投资本金(元)】为: " + this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim());
 
       //验证预期收益率(%)
        assertTrue("惠投资确认页【总预期收益率(%)】不正确: ",this.driver.findElement(By.xpath(".//*[@id='yqsyl']")).getText().trim().equals("600.00%~700.00%"));
        System.out.println("惠投资确认页【总预期收益率(%)】: " + this.driver.findElement(By.xpath(".//*[@id='yqsyl']")).getText()+"%");
        
        
        //总预期收益(元)
        assertTrue("惠投资确认页【总预期收益(元)】不正确: ",this.driver.findElement(By.xpath(".//*[@id='yqsy']")).getText().trim().equals("555.00~647.50"));
        System.out.println("惠投资确认页【总预期收益(元)】: " + this.driver.findElement(By.xpath(".//*[@id='yqsy']")).getText().trim());
        
        //实际支付金额(元)-----20160808
        assertTrue("惠投资确认页【实际支付金额(元)】不正确: ",this.driver.findElement(By.id("sjzfje")).getText().trim().equals("100.00"));
        System.out.println("惠投资确认页【实际支付金额(元)】:"+this.driver.findElement(By.id("sjzfje")).getText().trim());
        
         //投资红包
        if(this.IsElementPresent(By.xpath("//div[@id='usecoupon']/div/input[1]")))
        {
        	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='usecoupon']/div/input[1]")));
        	this.driver.findElement(By.xpath("//div[@id='usecoupon']/div/input[1]")).click();
        	 //验证实际投资金额
            new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
    			@Override
    			public Boolean apply(WebDriver webDriver) {
    				String text = webDriver.findElement(By.xpath(".//*[@id='sjzfje']")).getText();
    				return text.equalsIgnoreCase(userremaininginvestamount);
    			}
    		});
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).clear();
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
        	}
        else{
        	//输入交易密码
        	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tpwdid']")));
        	
        	 //验证实际投资金额
            new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
    			@Override
    			public Boolean apply(WebDriver webDriver) {
    				String text = webDriver.findElement(By.xpath(".//*[@id='sjzfje']")).getText();
    				return text.equalsIgnoreCase(userremaininginvestamount);
    			}
    		});
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).clear();
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
        }
        
              
        //点击确定投资
        new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='besureInvest']")).isDisplayed();
            }
        });
          this.driver.findElement(By.xpath(".//*[@id='besureInvest']")).click();
        
        //投资成功pop窗口
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='investSuccess_new']")).isDisplayed();
            }
        });
        
        //验证本次投资金额
        assertTrue("惠投资确认提示框【本次投资金额】不正确: ",this.driver.findElement(By.xpath(".//*[@id='inveamount_new']")).getText().trim().equals("100.00"));
        System.out.println("惠投资确认提示框【本次投资金额】:" + this.driver.findElement(By.xpath(".//*[@id='inveamount_new']")).getText().trim() + "元");
        
        //验证本项目累计投资金额
        assertTrue("惠投资确认提示框【本项目累计投资金额】:",this.driver.findElement(By.xpath(".//*[@id='investCount_new']")).getText().trim().equals("100.00"));
        System.out.println("惠投资确认提示框【本项目累计投资金额】:" + this.driver.findElement(By.xpath(".//*[@id='inveamount_new']")).getText().trim() + "元");
        
        //等待查看申请
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='investSuccess_new']/p/a[1]")));
        
        //验证浏览更多项目
        assertTrue("惠投资成功后浏览更多项目",this.driver.findElement(By.xpath(".//*[@id='investSuccess_new']/p/a[1]")).isDisplayed());
        
        //验证查看申请
        assertTrue("惠投资成功后查看申请",this.driver.findElement(By.xpath(".//*[@id='investSuccess_new']/p/a[2]")).isDisplayed());
        
        //点击查看申请
        this.driver.findElement(By.xpath(".//*[@id='investSuccess_new']/p/a[2]")).click();
       

	}
	@Test
	public void test002_客户搜索惠投资的产品() {
		//等待我的主页
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("我的主页")));
        
        //我的投资->惠投资
        this.selectMenu(10, "我的投资");
        this.selectSubMenu(4, "惠投资");
        
         //选择正在招标中的产品
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("borrowstatus")));
        assertTrue("请选择下拉框不存在",this.driver.findElement(By.id("borrowstatus")).getText().contains("--请选择--"));
        this.searchFinanceProhui("正在招标中");
        
        //点击搜索
        assertTrue("搜索不存在",this.driver.findElement(By.id("search")).isDisplayed());
        this.driver.findElement(By.id("search")).click();
        
        //验证惠投资产品列表不为空
        List<WebElement> list = this.driver.findElements(By.xpath("//tr//td[@align='center']//a[@target='_blank']")); 
  	    assertTrue("不存在惠投资产品",list.size()>0);
  	    
  	    
  	  //验证本金（元）-----20160809
        assertTrue("【惠投资搜索页面】本金（元）不存在",this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[2]")).getText().trim().equals("100.00"));
        System.out.println("【惠投资搜索页面】本金（元）为: " + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[2]")).getText().trim() + "元");
        
        //验证预计收益（元）
        assertTrue("【惠投资搜索页面】预计收益（元）不存在",this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[3]")).getText().trim().equals("555.00~647.50"));
        System.out.println("【惠投资搜索页面】预计收益(元)为: " + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[3]")).getText().trim()+ "元");
        
        //验证起息时间
        assertTrue("【惠投资搜索页面】起息时间不存在",this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[5]")).getText().trim().equals("20170102"));
        System.out.println("【惠投资搜索页面】起息时间为:" + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[5]")).getText().trim());
        
        //验证到期时间 
        assertTrue("【惠投资搜索页面】到期时间不存在",this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[6]")).getText().trim().equals("20171201"));
        System.out.println("【惠投资搜索页面】到期时间为:" + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[6]")).getText().trim());
        
        //验证状态-----20160809
        assertTrue("【惠投资搜索页面】状态不存在",this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[7]")).getText().trim().equals("正在招标中"));
        System.out.println("【惠投资搜索页面】状态为:" + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/table[1]/tbody/tr[2]/td[7]")).getText().trim());
		
	}

}
