package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 我的投资_惠聚财_上架产品展示  extends AccountBaseTest{

	/**
	 * @Description  我的投资_惠聚财_上架产品展示
	 * Created by songxq on 18/8/16.
	 */
	@Test
	public void 上架产品展示() {
		this.navigateToPage("新手专区");
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
        
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a")));
       
       //获取第一产品的text
       assertTrue("惠聚财上架产品没有展示", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a")).getText().trim().equalsIgnoreCase(this.yueProjectName));
       
	}

}
