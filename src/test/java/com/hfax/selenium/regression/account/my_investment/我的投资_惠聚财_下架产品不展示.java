package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 我的投资_惠聚财_下架产品不展示 extends AccountBaseTest {

	/**
	 * @Description  我的投资_惠聚财_下架产品不展示 
	 * Created by songxq on 18/8/16.
	 */
	@Test
	public void 不展示的产品() {
		this.navigateToPage("新手专区");
		//点击惠投资
		WebElement content = this.clickOnSubmenu("惠聚财");
        
        new WebDriverWait(this.driver,15).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div")));
        
        //验证惠投资第一个产品
        try{
        	new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a")));
        	 //获取第一产品的text
            String firstProText1 = this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a")).getText();
           if(firstProText1.trim().equalsIgnoreCase(this.yueProjectName)){
        	   fail("惠聚财的产品没有下架");
           }
        }
        catch(Exception e){
        	   new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div")));
        	   String firstProText2 = this.driver.findElement(By.xpath(".//*[@id='display_15']/div")).getText();
               if(firstProText2.trim().contains("暂无信息")){
            	   assertTrue("惠聚财产品没有下架", firstProText2.trim().contains("暂无信息"));
               }
           }
		}
        
}
