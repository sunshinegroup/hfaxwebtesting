package com.hfax.selenium.regression.account.transfer;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.account.AccountBaseTest;
import com.hfax.selenium.rule.SuddenDeathRule;

public class 投资转让_可转让的项目转让  extends AccountBaseTest{
	/**
	 * @Description 投资转让_可转让的项目转让
	 * Created by songxq on 29/4/16.
	 */
	
	
	@Test
	public void 可转让的项目转让(){
		//投资转让->可转让的项目
		 this.selectMenu(14, "投资转让");
		 this.selectSubMenu(2, "可转让的项目");
		 
		 //加载可转让的项目
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='con_one_2']/div[1]/h2")));
		 assertTrue("用户没有【可转让的项目】", this.driver.findElement(By.xpath("//div[@id='con_one_2']/div[1]/h2")).isDisplayed());
		 
		 //验证第一个项目的转让
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='con_one_2']/div[1]/ul/li[5]/a[1]")));
		 WebElement transferbutton = this.driver.findElement(By.xpath("//div[@id='con_one_2']/div[1]/ul/li[5]/a[1]"));
		 assertTrue("用户【可转让的项目】中没有转让按钮", transferbutton.isDisplayed());
		 
		 //点击转让
		 transferbutton.click();
		 
		 //验证项目定价转让的窗口
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='attornOk']/div[2]/div")));
		assertTrue("用户【可转让的项目】中项目定价转让的窗口不存在", this.driver.findElement(By.xpath("//div[@id='attornOk']/div[2]/div")).isDisplayed());
		
		//输入出让综合收益率
		WebElement inputRate = this.driver.findElement(By.xpath("//input[@id='colligateYield_DJ']"));
		inputRate.clear();
		inputRate.sendKeys("10");
		
		//输入交易密码
		WebElement inputPassword = this.driver.findElement(By.xpath("//input[@id='dealpwd']"));
		inputPassword.clear();
		inputPassword.sendKeys(this.jiaoyipassword);
		
		//勾选我已同意并且签署转让协议
		WebElement agree = this.driver.findElement(By.xpath("//input[@id='agre']"));
		agree.click();
		
		//验证确认转让
		assertTrue("用户【可转让的项目】中项目定价转让的窗口不存在", this.driver.findElement(By.xpath("//a[@id='debt_ok1']")).isEnabled());
		//点击确认
		this.driver.findElement(By.xpath("//a[@id='debt_ok1']")).click();
		
		//弹出操作转让成功的pop窗口
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),转让操作成功)]")));
		//点击我知道了
		this.driver.findElement(By.linkText("我知道了")).click();
		
		
	}
}
