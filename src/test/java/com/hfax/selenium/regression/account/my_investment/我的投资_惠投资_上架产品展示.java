package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 我的投资_惠投资_上架产品展示  extends AccountBaseTest{

	/**
	 * @Description  我的投资_惠投资_上架产品展示
	 * Created by songxq on 13/6/16.
	 */
	@Test
	public void 上架产品展示() {
		this.navigateToPage("新手专区");
		//点击惠投资
		WebElement content = this.clickOnSubmenu("惠投资");
        
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_6']/div/div[2]/div/div[1]/a")));
       
       //获取第一产品的text
       assertTrue("惠投资上架产品没有展示", this.driver.findElement(By.xpath(".//*[@id='display_6']/div/div[2]/div/div[1]/a")).getText().equalsIgnoreCase(this.huiProjectName));
       
	}

}
