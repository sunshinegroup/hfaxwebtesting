package com.hfax.selenium.regression.account.finance;

import com.hfax.selenium.regression.account.AccountBaseTest;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

/**
 * Created by philip on 3/24/16.
 */
public class 充值 extends AccountBaseTest {
    @Override
    public void setup() throws Exception {
        super.setup();
        this.充值提现();
    }

    @Test
    public void 充值页面检查() {
        this.selectSubMenu(2, "充值");

        String cardNumber = this.driver.findElement(By.xpath("//div[@class='biaoge2']/table/tbody/tr[3]/td[3]")).getText();
        assertTrue("卡号前四位错误", cardNumber.substring(0, 4).equalsIgnoreCase(this.card.substring(0, 4)));
        assertTrue("卡号后四位错误", cardNumber.substring(cardNumber.length() - 4, cardNumber.length()).equalsIgnoreCase(this.card.substring(this.card.length() - 4, this.card.length())));

        List<WebElement> promptMessages = this.driver.findElements(By.xpath("//span[@class='formtips']"));
        assertTrue("第一条提醒信息不正确", promptMessages.get(0).getText().contains("由于第三方支付公司限制，从2015年10月1日起我平台只支持网银支付，具体恢复时间请关注网站公告"));
        assertTrue("第二条提醒信息不正确", promptMessages.get(1).getText().contains("以下是绑定的银行卡信息，如果没有银行卡请先进行充值银行卡设置"));

        String amount = this.driver.findElement(By.xpath("//div[@class='biaoge2']/table/tbody/tr[4]/td[2]/strong")).getText();
        assertTrue("金额不能为空", amount.replace("元", "").trim().length() > 0);
    }

    @Test
    public void 充值为空失败() {
        this.selectSubMenu(2, "充值");

        this.driver.findElement(By.id("addrecharge")).click();
        this.wait.until(ExpectedConditions.alertIsPresent());

        String errorMessage = "请输入充值金额";
        assertTrue("Alert信息提示不正确", this.driver.switchTo().alert().getText().equalsIgnoreCase(errorMessage));
        this.driver.switchTo().alert().accept();

        assertTrue("输入提醒信息没出现", this.driver.findElement(By.id("money_tip1")).isDisplayed());
        assertTrue("错误提示不正确", this.driver.findElement(By.id("money_tip1")).getText().equalsIgnoreCase(errorMessage));
    }

    @Test
    public void 充值非法输入失败() {
        this.selectSubMenu(2, "充值");

        this.driver.findElement(By.id("money1")).sendKeys("abc");
        this.driver.findElement(By.id("addrecharge")).click();
        this.wait.until(ExpectedConditions.alertIsPresent());

        String errorMessage = "请输入正确的充值金额，必须为大于0的数字";
        assertTrue("Alert信息提示不正确", this.driver.switchTo().alert().getText().equalsIgnoreCase(errorMessage));
        this.driver.switchTo().alert().accept();

        assertTrue("输入提醒信息没出现", this.driver.findElement(By.id("money_tip1")).isDisplayed());
        assertTrue("错误提示不正确", this.driver.findElement(By.id("money_tip1")).getText().equalsIgnoreCase(errorMessage));
    }

    @Test
    public void 充值密码输入失败() {
        this.selectSubMenu(2, "充值");

        this.driver.findElement(By.id("money1")).sendKeys("100.01");
        this.driver.findElement(By.id("dealpwd")).sendKeys("abc");
        this.driver.findElement(By.id("addrecharge")).click();

        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='pwd_tip']")));
        assertTrue("错误提示不正确", this.driver.findElement(By.id("pwd_tip")).getText().contains("交易密码输入不正确，请重试或点击"));

        this.driver.findElement(By.xpath("//span[@id='pwd_tip']/b/a")).click();
        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='tabtil']/ul/li[@class='on']"), "交易密码管理"));
    }
}
