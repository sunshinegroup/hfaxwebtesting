package com.hfax.selenium.regression.account.transfer;

import static org.junit.Assert.assertTrue;

import com.hfax.selenium.regression.account.AccountBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;


/**
 * 
 * @author changwj
 * @Description 我的账户--投资转让
 */
public class 投资转让 extends AccountBaseTest {
	@Override
	public void setup() throws Exception {
		super.setup();
		this.selectMenu(14, "投资转让");
	}

	// 转让中的项目
	@Test
	public void 转让中的项目() {
        this.selectSubMenu(1, "转让中的项目");

        String totalCountString = this.driver.findElement(By.xpath("//div[@class='attornTop']/p")).getText().replace(" ", "");
        assertTrue("共完成信息不正确: " + totalCountString, totalCountString.matches("您在惠金所共完成[0-9]+笔转让交易"));

        String transferredCountString = this.driver.findElement(By.xpath("//div[@class='attornTop']/span[1]")).getText().replace(" ", "").replace("\n","");
        assertTrue("已转出金额信息不正确 " + transferredCountString, transferredCountString.matches("已转出金额[0-9]+\\.[0-9]+"));

        String transferredInCountString = this.driver.findElement(By.xpath("//div[@class='attornTop']/span[2]")).getText().replace(" ", "").replace("\n","");
        assertTrue("已转出金额信息不正确 " + transferredInCountString, transferredInCountString.matches("已转入金额[0-9]+\\.[0-9]+"));
	}

	// 可转让的项目
	@Test
	public void 可转让的项目() {
        this.selectSubMenu(2, "可转让的项目");
	}

	// 已转出的项目
	@Test
	public void 已转出的项目() {
        this.selectSubMenu(3, "已转出的项目");
	}

	// 已转让的项目
	@Test
	public void 已转入的项目() {
        this.selectSubMenu(4, "已转入的项目");
	}

}
