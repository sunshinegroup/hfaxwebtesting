package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 我的投资_惠投资_投资后账户金额  extends AccountBaseTest{
	/**
	 * @Description  我的投资_惠投资_投资后账户金额 
	 * Created by songxq on 21/6/16.
	 */
	@Test
	public void 投资后账户金额 () {
		
		 String amountRemaining1 = "￥ 555,000.01"; //账户余额
		 String amount1 = "￥ 555,555.01";          //账户总资产
		 String freezeAmount1 = "￥ 100.00";        //冻结金额
		
		//等待头像图片
		new WebDriverWait(this.driver,5).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@class='pic_info']/div[@class='pic']/img")).isDisplayed();
            }
        });
		
		//验证账户余额
		assertTrue("账户余额不正确",this.driver.findElement(By.xpath("//div[@class='zh_hz clearfix']/ul/li[1]/p")).getText().equals(amountRemaining1));
		 
		//验证账户总资产
		assertTrue("账户总资产不正确",this.driver.findElement(By.xpath("//div[@class='zh_hz clearfix']/ul/li[2]/p")).getText().equals(amount1));
		 
		//验证冻结金额
		assertTrue("冻结金额不正确",this.driver.findElement(By.xpath("//div[@class='zh_hz clearfix']/ul/li[4]/p")).getText().equals(freezeAmount1));
	}

}
