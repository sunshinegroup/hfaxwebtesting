package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.console.项目查询;

public class 我的投资_惠理财项目的截标  extends 项目查询{

	
     @Test
	 public void 截标成功() {
		 	项目查询信息();
	        
	        //查询项目名称
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='project_name']")));
	        this.driver.findElement(By.xpath("//input[@id='project_name']")).clear();
	        this.driver.findElement(By.xpath("//input[@id='project_name']")).sendKeys(this.consoleProjectName);
	        //点击查询
	        this.driver.findElement(By.linkText("查询"));
	        
	        //选择查询出来的第一个产品
	        new WebDriverWait(this.driver,10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tr[@id='datagrid-row-r1-2-0']/td[1]/div/input")));
	        findValueAtIndexRow(0, 0).click();
	        
	        //点击截标
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("截标")));
	        this.driver.findElement(By.linkText("截标")).click();

	        //确定截标
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
			driver.findElement(By.linkText("确定")).click();
			this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));

	        //点击确定
			this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
			driver.findElement(By.linkText("确定")).click();
			this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	    }

}
