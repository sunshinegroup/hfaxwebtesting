package com.hfax.selenium.regression.account.coupons;

import static org.junit.Assert.assertTrue;

import java.util.List;

import com.hfax.selenium.regression.account.AccountBaseTest;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author changwj
 * @Description 我的账户--我的投资券
 */
public class 我的投资劵 extends AccountBaseTest {
	@Override
	public void setup() throws Exception {
		super.setup();
		this.我的投资劵();
	}

	/**
	 * 未使用的投资券
     */
	@Test
	public void 投资券页面() {
        this.selectSubMenu(1, "未使用的投资券");

        String couponCountString = this.driver.findElement(By.xpath("//div[@class='ticketTop']/p/strong")).getText();
        int couponCount = new Integer(couponCountString).intValue();
        assertTrue("未使用投资券数目错误", couponCount >= 0);

        String unusedCouponMessage = this.driver.findElement(By.xpath("//div[@class='ticketTop']/p")).getText();
        assertTrue("未使用投资券信息错误: " + unusedCouponMessage, unusedCouponMessage.contains("枚优惠券未使用"));

        List<WebElement> elements = this.driver.findElements(By.xpath("//div[@class='ticketTop']/span/b"));
        int i = 1;
        for (WebElement element : elements) {
            int couponCount2 = new Integer(element.getText()).intValue();
            assertTrue("右侧s第 "+ i +" 个投资券数目错误: " + element.getText(), couponCount2 >= 0);
            i ++;
        }
        
        //验证投资券信息
        List<WebElement> labal = this.driver.findElements(By.xpath("//div[@class='ticketscontent']/ul/li/span"));
        assertTrue("可投资于加载错误", labal.get(0).getText().equalsIgnoreCase("可投资于："));
        
        //验证有效期加载
        assertTrue("有效期加载错误", labal.get(1).getText().equalsIgnoreCase("有效期："));
        
        //验证获得来源加载
        assertTrue("获得来源加载错误", labal.get(2).getText().equalsIgnoreCase("获得来源："));
        
        //验证使用条件加载
        assertTrue("使用条件加载错误", labal.get(3).getText().equalsIgnoreCase("使用条件："));
        
        //验证投资券图片加载
        String img = this.driver.findElement(By.xpath("//div[@class='amountImg']/img")).getAttribute("src");
        String img_check = this.url + "/images/tamount.jpg";
        assertTrue("投资券图片加载错误", img.equalsIgnoreCase(img_check));
        
        //验证已使用投资券页面
        this.selectSubMenu(2, "已使用的投资券");
        
        //验证失效的投资券
        this.selectSubMenu(3, "失效的投资券");
	}

	/**
	 * 未使用的投资券--去投资
	 * 需要投资券
	 * @author changwj
	 */
	@Test
	public void 通过投资券去投资(){
        this.selectSubMenu(1, "未使用的投资券");
        //点击立即投资
        WebElement investButton = this.driver.findElement(By.xpath("//div[@class='ticketTop']/p/a"));
        assertTrue("立即投资按钮文字不正确", investButton.getText().equalsIgnoreCase("立即投资"));

        investButton.click();
        assertTrue("无法读取投资理财页面", this.driver.getTitle().contains("投资理财"));
        this.driver.navigate().back();
        
        //点击去投资
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='ticketstitle']/a")));
        WebElement investButton2 = this.driver.findElement(By.xpath("//div[@class='ticketstitle']/a"));
        assertTrue("去投资按钮文字不正确", investButton2.getText().equalsIgnoreCase("去投资"));
        
        investButton2.click();
        assertTrue("无法读取投资理财页面", this.driver.getTitle().contains("投资理财"));
	}

    @Test
    public void 绑定投资券为空失败() {
        this.selectSubMenu(1, "未使用的投资券");

        WebElement bindButton = this.driver.findElement(By.id("bindtickets"));
        assertTrue("绑定按钮文字不正确", bindButton.getText().equalsIgnoreCase("绑定投资券"));

        bindButton.click();
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("bind")));

        this.driver.findElement(By.xpath("//div[@id='bind']/h3/b/a")).click();
        this.wait.until(ExpectedConditions.alertIsPresent());

        assertTrue("错误提示不正确", this.driver.switchTo().alert().getText().equalsIgnoreCase("兑换码不能为空"));
        this.driver.switchTo().alert().accept();

        WebElement errorElement = this.driver.findElement(By.id("s_ticketsNo"));
        assertTrue("错误提示没显示", errorElement.isDisplayed());
        assertTrue("错误信息不正确", errorElement.getText().equalsIgnoreCase("请输入投资券兑换码"));

        WebElement errorElement2 = this.driver.findElement(By.id("s_checkNo"));
        assertTrue("错误提示没显示", errorElement2.isDisplayed());
        assertTrue("错误信息不正确", errorElement2.getText().equalsIgnoreCase("请输入验证码"));
    }

    @Test
    public void 绑定投资券不存在() {
        this.selectSubMenu(1, "未使用的投资券");

        WebElement bindButton = this.driver.findElement(By.id("bindtickets"));
        assertTrue("绑定按钮文字不正确", bindButton.getText().equalsIgnoreCase("绑定投资券"));

        bindButton.click();
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("bind")));

        this.driver.findElement(By.id("ticketsNo")).sendKeys("123xyz");
        this.driver.findElement(By.id("checkNo")).sendKeys("123");

        this.driver.findElement(By.xpath("//div[@id='bind']/h3/b/a")).click();

		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
		        Alert alert = driver.switchTo().alert();  
		        String str = alert.getText();  
				return str.equalsIgnoreCase("兑换码不存在");
			}
		});
        this.driver.switchTo().alert().accept();
    }
}
