package com.hfax.selenium.regression.account.充值提现卡变更流程;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import static org.junit.Assert.assertTrue;

/**
 * Created by philip on 4/5/16.
 * refacted by wangmingqiang 5/3/16 重构登陆功能
 */
public class 忘记密码 extends SuddenDeathBaseTest {
	
    @Override
    public void setup() throws Exception {
    	super.setup();
    	Properties prop = new Properties();
		ClassLoader loader = BaseTest.class.getClassLoader();
		InputStream inputStream = loader
				.getResourceAsStream("卡变更webDriver.properties");
		BufferedReader bf = new BufferedReader(new InputStreamReader(
				inputStream));
		prop.load(bf);

		this.url = prop.getProperty("url");
		this.username = prop.getProperty("username");
		this.repassword = prop.getProperty("repassword");
		this.phone = prop.getProperty("phone");
		this.name = prop.getProperty("name");
		this.shouldExitOnTearDown = true;
        this.loadPage();
    }

    @Test
    public void 忘记密码功能(){
    	forgetpassword(this.phone,this.username,this.repassword);
   }
    
    /**
     * 
     * 功能:忘记密码密码更改
     * 1.phone 用户手机号
     * 2.username 用户名
     * 3.repassword 更改密码
     * 
     */
    protected void forgetpassword(String phone,String username,String repassword){
    	

        this.driver.findElement(By.className("login_tab_but")).click();
        //忘记密码连接校验
        this.driver.findElement(By.linkText("忘记密码")).click();  
        //立即判断新打开窗口句柄，存在窗口未及时打开情况，所以增加延迟后再切换窗口
        try {
        	Thread.sleep(1000);
        } catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
        }

        String currentWindow = this.driver.getWindowHandle();//获取当前窗口句柄
        Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
        Iterator<String> it = handles.iterator();

        String nextWindow = it.next();
        if (currentWindow.equals(nextWindow) ) {
    	nextWindow = it.next();
        }
        
      //切换到密码修改页面
        this.driver.switchTo().window(nextWindow);
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='codeBtn']")).isDisplayed();
            }
            });
        
        this.driver.findElement(By.xpath(".//*[@id='codeBtn']")).click();    
        assertTrue("忘记密码链接测试失败", this.driver.findElement(By.xpath("//a[@id='codeBtn']")).getText().equalsIgnoreCase("获取短信验证码"));
 
	this.driver.findElement(By.xpath(".//*[@id='phoneNum']")).sendKeys(phone);
	
	this.driver.findElement(By.xpath(".//*[@id='codeBtn']")).click();
    this.wait.until(new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver webDriver) {
            return driver.findElement(By.xpath(".//*[@id='code_voice_tip']")).getText().contains("短信验证码已发送至您的手机中");
        }
        });
    
	this.driver.findElement(By.xpath(".//*[@id='phoneCode']")).sendKeys("1234");
	//下一步
	this.driver.findElement(By.xpath(".//*[@id='ph_btn']")).click();
    this.wait.until(new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver webDriver) {
            return driver.findElement(By.xpath("//ul[@class='textBox']/li[1]/strong")).getText().equals("设置新密码");
        }
        });
    driver.findElement(By.id("password")).sendKeys(repassword);
    driver.findElement(By.id("confirmPassword")).sendKeys(repassword);
    
    driver.findElement(By.xpath(".//*[@id='ret_btn']")).click();
    this.wait.until(new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver webDriver) {
        	return webDriver.switchTo().alert().getText().contains("密码重置");
        }
        });
    String result = this.driver.switchTo().alert().getText();
    assertTrue("密码重置失败提示信息:"+result, result.contains("密码重置成功"));
    //密码重置弹出框确认
    this.driver.switchTo().alert().accept();
    
    //登陆页面跳转
    this.wait.until(new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver webDriver) {
            	return webDriver.findElement(By.xpath("//div[@class='topBox']/p")).getText().equals("登录");
        }
        });
    assertTrue("密码重置成功登录页面跳转失败", this.driver.findElement(By.xpath("//div[@class='topBox']/p")).getText().equals("登录"));

    //密码修改后登陆验证
	this.driver.findElement(By.id("email")).clear();
	this.driver.findElement(By.id("password")).clear();
    this.driver.findElement(By.id("email")).sendKeys(username);
    this.driver.findElement(By.id("password")).sendKeys(repassword);
    this.driver.findElement(By.id("code_temp")).sendKeys("1234");
	this.driver.findElement(By.xpath(".//*[@id='btn_login']")).click();

    this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='userName']")));
    assertTrue("登录功能失败", this.driver.findElement(By.xpath("//a[@class='userName']")).getText().equals(this.username));
 
    }

}
    
