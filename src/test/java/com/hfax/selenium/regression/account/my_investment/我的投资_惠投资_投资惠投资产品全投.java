package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 我的投资_惠投资_投资惠投资产品全投 extends AccountBaseTest{

	/**
	 * @Description 我的投资_惠投资_投资惠投资产品全投
	 * Created by songxq on 14/6/16.
	 */
	@Test
	public void 我的投资_惠投资_投资惠投资产品全投() {
		this.navigateToPage("新手专区");
		//点击惠投资
		WebElement content = this.clickOnSubmenu("惠投资");
        WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
        
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='display_6']/div/div[2]")));
        //点击惠投资第一个产品
        assertTrue("惠投资第一个产品不存在", this.driver.findElement(By.xpath("//div[@id='display_6']/div/div[2]")).isDisplayed());
        firstItem.findElement(By.xpath("i/div/div[2]/a")).click();
        
        //项目介绍
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[@id='one1']")));
        this.driver.findElement(By.xpath("//strong[@id='one1']")).click();
        assertTrue("项目介绍不存在", this.driver.findElement(By.xpath("//strong[@id='one1']")).isDisplayed());
        
        //投资说明书
        this.driver.findElement(By.xpath("//strong[@id='one2']")).click();
        assertTrue("投资说明书不存在", this.driver.findElement(By.xpath("//strong[@id='one2']")).isDisplayed());
        
        //风险提示书
        this.driver.findElement(By.xpath("//strong[@id='one3']")).click();
        assertTrue("风险提示书不存在", this.driver.findElement(By.xpath("//strong[@id='one3']")).isDisplayed());
        
        //投资管理合同
        this.driver.findElement(By.xpath("//strong[@id='one4']")).click();
        assertTrue("投资管理合同不存在", this.driver.findElement(By.xpath("//strong[@id='one4']")).isDisplayed());
        
        //定位到输入金额文本框
         WebElement target = this.driver.findElement(By.xpath("//div[@id='content']/ul/li/strong"));
        ((JavascriptExecutor)this.driver).executeScript("arguments[0].scrollIntoView();", target);
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='content']/ul/li/strong")));
        
        //输入投资金额
        assertTrue("投资金额输入框不存在", this.driver.findElement(By.xpath("//input[@id='amount']")).isDisplayed());
        WebElement input = this.driver.findElement(By.xpath("//input[@id='amount']"));
        input.clear();
        input.sendKeys(this.userallinvestamount);
        
        //勾选已阅读并同意《定向委托投资管理协议》
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='agre']")));
        assertTrue("《定向委托投资管理协议》不存在",this.driver.findElement(By.xpath("//input[@id='agre']")).isDisplayed());
        this.driver.findElement(By.xpath("//input[@id='agre']")).click();
        
        //点击去投资
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@id='btnsave']/i")));
        this.driver.findElement(By.xpath("//a[@id='btnsave']/i")).click();
        
        //等待交易密码
        new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='tpwdid']")).isDisplayed();
            }
        });
      
        
        //投资红包
        if(this.IsElementPresent(By.xpath("//div[@id='usecoupon']/div/input[1]")))
        {
        	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='usecoupon']/div/input[1]")));
        	this.driver.findElement(By.xpath("//div[@id='usecoupon']/div/input[1]")).click();
        	 //验证实际投资金额
            new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
    			@Override
    			public Boolean apply(WebDriver webDriver) {
    				String text = webDriver.findElement(By.xpath(".//*[@id='sjzfje']")).getText();
    				return text.equalsIgnoreCase(userallinvestamount);
    			}
    		});
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).clear();
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
        	}
        else{
        	//输入交易密码
        	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tpwdid']")));
        	 //验证实际投资金额
            new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
    			@Override
    			public Boolean apply(WebDriver webDriver) {
    				String text = webDriver.findElement(By.xpath(".//*[@id='sjzfje']")).getText();
    				return text.equalsIgnoreCase(userallinvestamount);
    			}
    		});
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).clear();
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
        }
        
       
        
        //点击确定投资
        new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='besureInvest']")).isDisplayed();
            }
        });
          this.driver.findElement(By.xpath(".//*[@id='besureInvest']")).click();
        
        //投资成功pop窗口
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='investSuccess_new']")).isDisplayed();
            }
        });
        
        //等待查看申请
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='investSuccess_new']/p/a[1]")));
        
        //验证浏览更多项目
        assertTrue("惠投资成功后浏览更多项目",this.driver.findElement(By.xpath(".//*[@id='investSuccess_new']/p/a[1]")).isDisplayed());
        
        //验证查看申请
        assertTrue("惠投资成功后查看申请",this.driver.findElement(By.xpath(".//*[@id='investSuccess_new']/p/a[2]")).isDisplayed());
        
        //点击查看申请
        this.driver.findElement(By.xpath(".//*[@id='investSuccess_new']/p/a[2]")).click();
       

	}

}
