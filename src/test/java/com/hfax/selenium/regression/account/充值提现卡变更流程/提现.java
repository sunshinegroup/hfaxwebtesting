package com.hfax.selenium.regression.account.充值提现卡变更流程;

import com.hfax.selenium.base.SuddenDeathBaseTest;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Keys;

/**
 * 提现 Created by wangmingqiang on 4/27/16.
 * 
 */
public class 提现 extends SuddenDeathBaseTest {
	static	protected String getcash = "1.01";
	
	protected void supersetup() throws Exception{
	super.setup();
	}
	
	@Override
	public void setup() throws Exception {
	        super.setup();
	        this.loadPage();
	        this.login();
	    }
	
	@Test
	public void 提现功能() {
		withdraw(false,getcash,this.password);

	}
    /**
     * 
     * 功能:提现资金记录查询
     * 1.getcash 提现金额
     * 
     */
	 private void 资金记录查询(String getcash){
		 
			// 资金记录查询
			this.selectSubMenu(1, "资金记录");
			 this.wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td[3]")));
			 
			String recordname = this.driver.findElement(
					By.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td[3]"))
					.getText();
			assertTrue("提现成功资金记录未找到，或者顺序异常", recordname.equals("提现成功"));
			String recordgetcash = this.driver.findElement(
					By.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td[6]"))
					.getText();
			assertTrue("提现金额不正确", recordgetcash.equals(getcash));
			System.out.println("---------‘提现资金记录’校验成功--------");
			}
	 
	 
	    /**
	     * 
	     * 功能:提现功能实现
	     * 1.flag 全部提现标记      true————全额提现，false————按金额提现
	     * 2.getcash 提现金额
	     * 3.password 交易密码
	     * @throws InterruptedException 
	     * 
	     */	 
	public void withdraw(Boolean flag,String getcash,String password){
		this.selectMenu(2, "充值提现");
		System.out.println("---------我的账户——>充值提现--------");
		this.selectSubMenu(3, "提现");
		System.out.println("---------我的账户——>充值提现——>提现--------");
		
		if(flag){
		// 获取可提现余额总数用于后续提现
		getcash = this.driver.findElement(
				By.xpath("//tbody/tr[5]/td[2]/strong")).getText().replace(",","");;
		getcash = getcash.substring(0, getcash.length() - 2);
		}
		System.out.println("---------获取提现总额成功--------");
		// 可提现余额检查
		String visiblemoney = this.driver.findElement(
				By.xpath("//tbody/tr[5]/td[2]/strong")).getText().replace(",","");;
		float fvismoney = Float.valueOf(visiblemoney.substring(0,
				visiblemoney.length() - 2));
		assertTrue("可提现余额为0不能进行提现", fvismoney > 0);
		System.out.println("---------'可提现余额'检查成功--------");

		// 账户余额检查(需大于可提现余额)
		String totalmoney = this.driver.findElement(
				By.xpath("//tbody/tr[4]/td[2]/strong")).getText().replace(",","");;
		float ftotmoney = Float.valueOf(totalmoney.substring(0,
				totalmoney.length() - 2));

		assertTrue("可提现余额大于账户余额", ftotmoney - fvismoney >= 0);
		System.out.println("---------'账户余额'金额检查成功--------");

		// 错误信息校验
		// 提现金额错误信息校验
		this.driver.findElement(By.xpath(".//*[@id='dealMoney']")).sendKeys(
				Keys.TAB);
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath(".//*[@id='money_tip']")));
		String errormsg = this.driver.findElement(
				By.xpath(".//*[@id='money_tip']")).getText();
		assertTrue("提现金额错误信息校验失败", errormsg.equals("提现金额不能为空"));
		System.out.println("---------‘提现金额为空’错误信息校验成功--------");

		// 交易密码错误信息校验
		this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).sendKeys(
				Keys.TAB);
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath(".//*[@id='pwd_tip']")));
		errormsg = this.driver.findElement(By.xpath(".//*[@id='pwd_tip']"))
				.getText();
		assertTrue("交易密码错误信息校验失败", errormsg.equals("交易密码不能为空"));
		System.out.println("---------‘交易密码错误’错误信息校验成功--------");

		// 空值提交校验
		this.driver.findElement(By.xpath(".//*[@id='btn_submit']")).click();
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath(".//*[@id='money_tip']")));
		errormsg = this.driver.findElement(By.xpath(".//*[@id='money_tip']"))
				.getText();
		assertTrue("空值提交校验失败", errormsg.equals("请输入正确的提现金额，必须为大于0的数字"));
		System.out.println("---------‘空值提现金额提交’错误信息校验成功--------");

		// 提现功能
		this.driver.findElement(By.xpath(".//*[@id='dealMoney']")).sendKeys(
				getcash);
		this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).sendKeys(
				password);
		this.driver.findElement(By.xpath(".//*[@id='clickCode']")).click();
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath(".//*[@id='code_voice_tip']")));
		errormsg = this.driver.findElement(
				By.xpath(".//*[@id='code_voice_tip']")).getText();
		assertTrue("短信验证码提示功能验证失败", errormsg.contains("短信验证码已发送至您的手机中"));
		System.out.println("---------‘短信验证码’校验成功--------");		
		this.driver.findElement(By.xpath(".//*[@id='code']")).sendKeys("1234");
		this.driver.findElement(By.xpath(".//*[@id='btn_submit']")).click();
		// this.wait.until(ExpectedConditions.alertIsPresent());
		new WebDriverWait(driver, 35000).until(ExpectedConditions
				.alertIsPresent());
		assertTrue(
				"提现功能失败，失败信息:" + this.driver.switchTo().alert().getText(),
				this.driver.switchTo().alert().getText()
						.contains("提现申请已成功")||this.driver.switchTo().alert().getText()
						.contains("申请提现成功"));
		this.driver.switchTo().alert().accept();
		System.out.println("---------‘提现功能’校验成功--------");
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//tbody/tr[4]/td[2]/strong")));
		// 可用余额、提现余额金额判断
		String leftusedcash = this.driver.findElement(
				By.xpath("//tbody/tr[4]/td[2]/strong")).getText().replace(",","");;
		String leftgotcash = this.driver.findElement(
				By.xpath("//tbody/tr[5]/td[2]/strong")).getText().replace(",","");;
		float leftused = Float.valueOf(leftusedcash.substring(0,
				leftusedcash.length() - 2));
		float leftgot = Float.valueOf(leftgotcash.substring(0,
				leftgotcash.length() - 2));

		assertTrue("扣款(可用余额)金额检查失败",
				((ftotmoney - Float.valueOf(getcash) -leftused==0)||(ftotmoney - Float.valueOf(getcash) -leftused>0.0000001 && ftotmoney - Float.valueOf(getcash) -leftused<0.0000001)));
		assertTrue("扣款(可提现余额)金额检查失败",
				((fvismoney - Float.valueOf(getcash) -leftgot==0)||(fvismoney - Float.valueOf(getcash) -leftgot>0.0000001 && fvismoney - Float.valueOf(getcash) -leftgot<0.0000001)));

		//提现记录查询
		资金记录查询(getcash);
	}
	
}
