package com.hfax.selenium.regression.account.settings;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 用户详情信息 extends AccountBaseTest{

	/**
     * @Description 用户详情信息 
     * Created by songxq on 24/8/16.
     * 
     */

	@Test
	public void 用户详情信息验证() {
		//点击用户设置
		用户设置();
		
		//点击用户详情信息
		this.selectSubMenu(1,"用户详情信息");
		List<WebElement> list = this.driver.findElements(By.xpath("//div[@class='pw_edit pw_edit2']//form//ul/li"));
		
		//验证真实姓名
		assertTrue("真实姓名不存在",list.get(0).getText().length() > 0);
		
		//验证身份证号
		assertTrue("身份证号不存在",list.get(1).getText().length() > 0);
		
		
	}

}
