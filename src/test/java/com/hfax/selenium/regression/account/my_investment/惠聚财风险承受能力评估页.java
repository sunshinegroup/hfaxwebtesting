package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 惠聚财风险承受能力评估页 extends AccountBaseTest {

	/**
	 *  
	 * @Description 惠聚财风险承受能力评估页
	 * Created by songxq on 17/8/16.
	 * 
	 */
	
	 protected void clickRadioButton(int number) {
		 try{
			this.driver.findElement(By.xpath("//div[@class='risk-issue-box'][" + number + "]/p[1]/input[@value='0']")).click();
		  }catch(Exception e){
			  e.printStackTrace();
		  }
	    }
	 
	@Test
	public void 惠聚财风险承受能力评估页 () {
		this.navigateToPage("新手专区");
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
		
		new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.linkText("服务介绍>>")));
		
		//获取当前窗口句柄
		 String currentWindow = this.driver.getWindowHandle();
		//点击服务介绍
		this.driver.findElement(By.linkText("服务介绍>>")).click();
		
		Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
		 Iterator<String> it = handles.iterator();
		 while (it.hasNext()) {
		 if (currentWindow == it.next()) {
		 continue;
		 }
		  WebDriver window = driver.switchTo().window(it.next());//切换到新窗口
		
		 //等待进行测评
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	String text = driver.findElement(By.linkText("进行测评>")).getText();
	                return text.equalsIgnoreCase("进行测评>");
	            }
	        });
	      //点击进行测评
	        this.driver.findElement(By.linkText("进行测评>")).click();
	        
	       //等待页面加载 
	        this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.className("risk-issue-box"));
	            }
	        });
	        
	        //1.选择您的投资经验（包括证券、期货、基金、信托、互联网理财产品等）时间是：选择A
	        //2.您是否熟悉互联网平台销售的产品？选择A
	        //3.您是否曾在互联网平台进行过投资？如有，距离上一次投资的时间？选择A
	        //4.您目前的资金配置是？选择A
	        //5.您的证券投资风格？选择A
	        //6.您倾向于投资以下哪种投资品种？选择A
	        //7.您对互联网平台销售的产品交易的风险态度如何？选择A
	        //8.您的投资年收益预期？选择A
	        //9.您的预期投资期限是？ 选择A
	        //10.您在互联网平台投资交易的交易金额占您总金融资产的比例？选择Ａ
	        //11.您能承受的投资本金浮亏比例？选择A
	        List<WebElement> list = this.driver.findElements(By.xpath("//div[@class='risk-issue-box']"));
	        System.out.println("list的长度为:"+ list.size());
	        for(int j = 1; j <= list.size(); j++)
	        {	
	        	clickRadioButton(j);
	        }
	        
	        //点击提交问卷
	        this.driver.findElement(By.linkText("提交问卷")).click();
	        
	        //等待页面加载 
	        this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath(".//*[@id='success2']/a"));
	            }
	        });
	        
	        //点击弹出框
	        if(this.driver.getPageSource().contains("立即去投资"))
			 {
	        	//立即去投资
	        	 this.driver.findElement(By.xpath(".//*[@id='success2']/a")).click();
			 }else{
				 	fail("惠聚财风险承受能力评估页答题失败！");
			 }
	        
	        //等待跳转到惠聚财的页面
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	String text = driver.findElement(By.xpath(".//*[@id='tab_15']")).getText();
	                return text.trim().equalsIgnoreCase("惠聚财");
	            }
	        });        
	        assertTrue("惠聚财页面不存在", this.driver.findElement(By.xpath(".//*[@id='tab_15']")).isDisplayed());
		 }
	}

}
