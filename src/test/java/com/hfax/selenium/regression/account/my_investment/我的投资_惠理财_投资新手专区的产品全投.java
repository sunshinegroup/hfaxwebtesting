package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 我的投资_惠理财_投资新手专区的产品全投  extends AccountBaseTest{
	
	 protected WebElement clickOnSubmenu(String title) {
	        WebElement submenu = this.driver.findElement(By.id("listTab-box"));
	        submenu.findElement(By.linkText(title)).click();

	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'新手专区')]")));
	       
	        List<WebElement> elements = this.driver.findElements(By.className("list-control"));

	        for (WebElement el : elements) {
	            if (el.isDisplayed()) return el;
	        }

	        assertTrue("无内容显示: " + title, false);

	        return null;
	    }

	@Test
	public void 投资新手专区的产品全投 () {
		this.navigateToPage("新手专区");
        WebElement content = this.clickOnSubmenu("新手专区");
        WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);

        firstItem.findElement(By.xpath("div[2]/div[2]/a")).click();

        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='balanceTitle']")));

        //验证剩余可投金额
        assertTrue("剩余可投金额不存在", this.driver.findElement(By.xpath("//div[@class='balanceTitle']")).isDisplayed());

        //验证项目概况
        assertTrue("项目概况不存在", this.driver.findElement(By.id("one1")).isDisplayed());
        this.driver.findElement(By.id("one1")).click();

        //验证投资说明书
        assertTrue("投资说明书不存在", this.driver.findElement(By.id("one2")).isDisplayed());
        this.driver.findElement(By.id("one2")).click();

        //验证风险提示书
        assertTrue("风险提示书不存在", this.driver.findElement(By.id("one3")).isDisplayed());
        this.driver.findElement(By.id("one3")).click();

        //验证投资管理合同
        assertTrue("风险提示书不存在", this.driver.findElement(By.id("one4")).isDisplayed());
        this.driver.findElement(By.id("one4")).click();

        WebElement amount = this.driver.findElement(By.xpath(".//*[@id='amount']"));
        assertTrue("投资理财金额不存在", amount.isDisplayed());
        amount.clear();
        amount.sendKeys("10000");

        this.driver.findElement(By.xpath(".//*[@id='agre']")).click();
        //确定投资
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@id='btnsave']/i")));

        this.driver.findElement(By.xpath("//a[@id='btnsave']/i")).click();

        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("besureInvest")));
        
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("tpwdid")));
        this.driver.findElement(By.id("tpwdid")).sendKeys(this.jiaoyipassword);
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("besureInvest")));
        this.driver.findElement(By.id("besureInvest")).click();

        WebElement popupBox = this.driver.findElement(By.xpath("//div[@id='investSuccess']"));
        this.wait.until(ExpectedConditions.visibilityOf(popupBox));
        popupBox.findElement(By.xpath("//a[@class='close']")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@id='investSuccess']")));
	}

}
