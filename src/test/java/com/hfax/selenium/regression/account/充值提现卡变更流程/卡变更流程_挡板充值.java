package com.hfax.selenium.regression.account.充值提现卡变更流程;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.junit.Test;

import com.hfax.selenium.base.BaseTest;

/**
 * Created by philip on 3/24/16. 
 * update by wangmingqiang
 * 合并充值前信息校验，增加卡变更后充值功能，充值后金额检查
 */
public class 卡变更流程_挡板充值 extends 挡板充值 {
	
	 @Override
	    public void setup() throws Exception {
	        super.supersetup();
	        Properties prop = new Properties();
	    	ClassLoader loader = BaseTest.class.getClassLoader();
	    	InputStream inputStream = loader
	    			.getResourceAsStream("卡变更webDriver.properties");
	    	BufferedReader bf = new BufferedReader(new InputStreamReader(
	    			inputStream));
	    	prop.load(bf);
	    	//this.recard = prop.getProperty("recard");
	    	this.card = prop.getProperty("card");
	    	this.username = prop.getProperty("username");
	    	this.password = prop.getProperty("password");
	        this.loadPage();
	        this.login();
	    }
	@Test
	@Override
	public void 充值功能() {
    	recharge(this.card,recharge,this.password);
  }

}