package com.hfax.selenium.regression.account.充值提现卡变更流程;

import java.util.Iterator;
import java.util.Set;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertTrue;

/**
 * Created by philip on 4/5/16.
 * refacted by wangmingqiang 5/3/16 重构登陆功能
 */
public class 登陆 extends SuddenDeathBaseTest {
    @Override
    public void setup() throws Exception {
        super.setup();
        this.loadPage();
        this.driver.findElement(By.className("login_tab_but")).click();
    	this.driver.findElement(By.id("email")).clear();
    	this.driver.findElement(By.id("password")).clear();
    	this.driver.findElement(By.id("code_temp")).clear();

    }

    @Test
    public void 登陆功能(){
    	login(this.username,this.password);
   }
    
    /**
     * 
     * 功能:登录页面校验/登录功能
     * 1.username 用户名
     * 2.password 密码
     */
     public void login(String username,String password){
    	//无输入时错误信息提示校验
        this.driver.findElement(By.id("btn_login")).click();
        WebElement usernameElement = this.driver.findElement(By.id("s_email"));
        WebElement passwordElement = this.driver.findElement(By.id("s_password"));
        WebElement codeElement = this.driver.findElement(By.id("s_code"));

        assertTrue("用户名错误信息没显示", usernameElement.isDisplayed());
        assertTrue("密码错误信息没显示", passwordElement.isDisplayed());
        assertTrue("验证码错误信息没显示", codeElement.isDisplayed());
        assertTrue("用户名错误信息不正确", usernameElement.getText().equalsIgnoreCase("请输用户名！"));
        assertTrue("密码错误信息不正确", passwordElement.getText().equalsIgnoreCase("请输入密码！"));
        assertTrue("用户名错误信息不正确", codeElement.getText().equalsIgnoreCase("请输入验证码！"));

        //用户名不存在错误信息校验
        this.driver.findElement(By.id("email")).sendKeys("p@p.com");
        this.driver.findElement(By.id("password")).sendKeys("fakepasswd");
        this.driver.findElement(By.id("code_temp")).sendKeys("1234");
        this.driver.findElement(By.id("btn_login")).click();

        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("dataMsgId")));
        assertTrue("'用户名不存在'错误提示信息不正确", this.driver.findElement(By.id("dataMsgId")).getText().equalsIgnoreCase("用户名不存在"));
        
        //密码错误提示信息校验
    	this.driver.findElement(By.id("email")).clear();
       	this.driver.findElement(By.id("password")).clear();
        this.driver.findElement(By.id("email")).sendKeys(username);
        this.driver.findElement(By.id("password")).sendKeys("fakepasswd");
        this.driver.findElement(By.id("code_temp")).sendKeys("1234");
        this.driver.findElement(By.id("btn_login")).click();
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("dataMsgId")));
        assertTrue("'密码错误'错误提示信息不正确", this.driver.findElement(By.id("dataMsgId")).getText().equalsIgnoreCase("密码错误"));
        
    //免费注册链接检验
	this.driver.findElement(By.linkText("免费注册")).click();  
    this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[17]/div[@class='reg-con-t']")));
    assertTrue("免费注册链接测试失败", this.driver.findElement(By.xpath("//div[17]/div[@class='reg-con-t']")).getText().equalsIgnoreCase("惠金所投资流程"));

    //登陆功能校验
    this.driver.findElement(By.id("email")).sendKeys(username);
    this.driver.findElement(By.id("password")).sendKeys(password);
    this.driver.findElement(By.id("code_temp")).sendKeys("1234");
	this.driver.findElement(By.xpath(".//*[@id='btn_login']")).click();

    this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='userName']")));
    assertTrue("登录功能失败", this.driver.findElement(By.xpath("//a[@class='userName']")).getText().equals(this.username));
 
    }

}
    
