package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.assertTrue;


import com.hfax.selenium.regression.account.AccountBaseTest;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.hfax.selenium.base.Util;

import java.util.List;
import java.util.Map;


/**
 * 
 * @author changwj
 * @Description 我的账户--我的投资
 */
public class 我的投资 extends AccountBaseTest {
	Map<String, String> envVars = System.getenv();
	@Override
	public void setup() throws Exception {
		super.setup();
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		{
			this.selectMenuIE(10,"我的投资");
		 }else{
			 this.selectMenu(10, "我的投资");
		 }
	}

    private void verifyTableHeaders() {
        List<WebElement> headers = this.driver.findElement(By.xpath("//div[@class='biaoge']/table")).findElements(By.xpath("tbody/tr/th"));
        String[] expectedHeaders = new String[] {"项目名称", "本金(元)", "预计收益(元)", "已到账收益(元)", "起息时间", "到期时间", "状态", "操作"};

        assertTrue("抬头列数不对", expectedHeaders.length == headers.size());

        for (int i = 0; i < expectedHeaders.length; i ++) {
            assertTrue("抬头列 " + i + " 文字不正确", expectedHeaders[i].equalsIgnoreCase(headers.get(i).getText()));
        }
    }

	// 我的投资--惠理财
    @Ignore
	@Test
	public void 惠理财() {
        this.selectSubMenu(1, "惠理财");
        this.verifyTableHeaders();
	}

	// 我的投资--票据宝
	@Ignore
	@Test
	public void 票据宝() {
        this.selectSubMenu(2, "票据宝");
        this.verifyTableHeaders();
	}

	// 我的投资--橙牛汽车钱包
	@Ignore
	@Test
	public void 橙牛汽车钱包() {
        this.selectSubMenu(3, "橙牛汽车钱包");
        this.verifyTableHeaders();
	}
	
	// 我的投资---惠投资
	@Ignore
	@Test
	public void 惠投资(){
		this.selectSubMenu(4, "惠投资");
        this.verifyTableHeaders();
	}
	// 我的投资---惠聚财
	@Test
	public void 惠聚财(){
		this.selectSubMenu(5, "惠聚财");
        this.verifyTableHeaders();
	}
	
	
}
