package com.hfax.selenium.regression.account.充值提现卡变更流程;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import com.hfax.selenium.base.BaseTest;
import org.junit.Test;

/**
*
* 绑卡
* Created by philip on 3/10/16.
* refacted by wangmingqiang 4/29/16 .
*/
public class 卡变更流程_绑卡 extends 绑卡{
	 @Override
	    public void setup() throws Exception {
	        super.supersetup();
	        Properties prop = new Properties();
	    	ClassLoader loader = BaseTest.class.getClassLoader();
	    	InputStream inputStream = loader
	    			.getResourceAsStream("卡变更webDriver.properties");
	    	BufferedReader bf = new BufferedReader(new InputStreamReader(
	    			inputStream));
	    	prop.load(bf);
	    	this.username = prop.getProperty("username");
	    	this.bankname = prop.getProperty("bankname");
	    	this.bankid = prop.getProperty("bankid");
	    	this.card = prop.getProperty("card");
	    	this.phone = prop.getProperty("phone");
	    	this.name = prop.getProperty("name");
	    	this.password = prop.getProperty("password");
	        this.loadPage();
	        this.login();
	    }
	@Test
   public void 绑卡功能(){
   	tieCard(this.bankname,this.bankid,this.card,this.phone,this.name,this.password);    
   	}
  
}
