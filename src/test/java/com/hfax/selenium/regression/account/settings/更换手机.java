package com.hfax.selenium.regression.account.settings;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.regression.account.AccountBaseTest;
/**
 * 
 * @author changwj
 * @Description  改变手机号码
 */
public class 更换手机 extends AccountBaseTest{
    @Override
    public void setup() throws Exception {
        super.setup();

        用户设置();
        this.driver.findElement(By.id("li_bp")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[1]/table/tbody/tr[1]/th")).isDisplayed();
            }
        });

        String text = this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[1]/table/tbody/tr[1]/th")).getText();
        assertTrue("读取更改手机页面失败", text.equals("手机绑定"));
    }

    private void setupForm(String idCard, String tradePwd, String newPhone, String code) {
        this.driver.findElement(By.id("idCard")).sendKeys(idCard);
        this.driver.findElement(By.id("tradePwd")).sendKeys(tradePwd);
        this.driver.findElement(By.id("newPhone")).sendKeys(newPhone);
        this.driver.findElement(By.id("reClickCode_")).sendKeys(code);
    }

    private void waitForMessage() {
        this.wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("idCard_msg"));
            }
        });
    }

    /**
     *
     *更换手机 身份证号为空
     *提示信息:身份证号号码不能为空！
     */
    @Test
    public void 身份证号不能为空() {
        this.setupForm("", "", "","");
        this.driver.findElement(By.xpath(".//*[@id='mobileChange']/table/tbody/tr[7]/td[2]/a")).click();
        this.waitForMessage();

        String err = this.driver.findElement(By.id("idCard_msg")).getText();
        assertTrue("错误信息不正确", err.equalsIgnoreCase("身份证号码不能为空"));
    }

    /**
     *
     *更换手机--校验交易密码
     *提示信息:初始交易密码为帐号密码
     */
    @Test
    public void 初始交易密码为账号密码(){
        this.setupForm(this.identification, "123", "", "");
        this.driver.findElement(By.xpath(".//*[@id='mobileChange']/table/tbody/tr[7]/td[2]/a")).click();
        this.waitForMessage();

        String err= this.driver.findElement(By.id("tradePwd_msg")).getText();
        assertTrue("错误信息不正确", err.equalsIgnoreCase("初始交易密码为帐号密码"));
    }

    /**
     * 更换手机 未输入手机号
     * 提示:请填写手机号 newPhone_msg
     */
    @Test
    public void 未输入手机号() {
        this.setupForm(this.identification, this.password, "", "");
        this.driver.findElement(By.id("reClickCode_")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("newPhone_msg")).isDisplayed();
            }
        });

        String errtext= this.driver.findElement(By.id("newPhone_msg")).getText();
        assertTrue("错误信息不正确", errtext.equalsIgnoreCase("请填写手机号码！"));
    }

    /**
     *
     * newPhone 新手机号 设置成功
     * @throws Exception
     * @throws InterruptedException
     */
    @Test
    public void 设置新手机号() throws Exception{
        this.setupForm(this.identification, this.password, this.phone.substring(0, this.phone.length() - 2) + "00", "");
        this.driver.findElement(By.id("reClickCode_")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//*[@id='code_voice_tip']")).isDisplayed();
            }
        });

        this.driver.findElement(By.id("checkCode")).sendKeys("1111");
        this.driver.findElement(By.xpath(".//a[text()='手机变更']")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.switchTo().alert().getText().contains("操作成功");
            }
        });

        this.driver.switchTo().alert().accept();

        // 重新改回原来的配置手机
        this.setupForm(this.identification, this.password, this.phone, "");
        this.driver.findElement(By.id("reClickCode_")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//*[@id='code_voice_tip']")).isDisplayed();
            }
        });

        this.driver.findElement(By.id("checkCode")).sendKeys("1111");
        this.driver.findElement(By.xpath(".//a[text()='手机变更']")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.switchTo().alert().getText().contains("操作成功");
            }
        });

        this.driver.switchTo().alert().accept();
    }

    /**
     * 校验该手机号是否已被注册
     * 提示:该手机号已被注册
     */
    @Test
    public void 检测手机号是否已经注册() {
        this.setupForm(this.identification, this.password, this.phone, null);
        this.driver.findElement(By.id("reClickCode_")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.switchTo().alert() != null;
            }
        });

        String text = this.driver.switchTo().alert().getText();
        assertTrue("错误信息不正确", text.equalsIgnoreCase("该手机号码已注册"));
        this.driver.switchTo().alert().accept();
    }
}
