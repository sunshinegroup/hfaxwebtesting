package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class 新手专区还款协议验证页  extends 我的投资_惠理财_搜索惠理财的产品{

	 /**
     * @Description 新手专区还款搜索验证页
     * Created by songxq on 9/8/16.
     */

	@Test
	public void 新手专区还款协议验证() {
		//等待我的主页
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("我的主页")));
        
        //我的投资->惠理财
        this.selectMenu(10, "我的投资");
        this.selectSubMenu(1, "惠理财");
        
         //选择正在招标中的产品
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("borrowstatus")));
        assertTrue("请选择下拉框不存在",this.driver.findElement(By.id("borrowstatus")).getText().contains("--请选择--"));
        this.searchFinancePro("还款中");
        
        //点击搜索
        assertTrue("搜索不存在",this.driver.findElement(By.id("search")).isDisplayed());
        this.driver.findElement(By.id("search")).click();
        
        //验证惠理财产品列表不为空
        List<WebElement> list = this.driver.findElements(By.xpath("//tr//td[@align='center']//a[@target='_blank']")); 
  	    assertTrue("不存在惠理财产品",list.size()>0);
  	    
  	    //查看协议等待
  	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("查看协议")));
  	    
  	    String currentWindow = driver.getWindowHandle();//获取当前窗口句柄
  	     //点击查看协议
  	    this.driver.findElement(By.linkText("查看协议")).click();
  	    
  	    Set<String> handles = driver.getWindowHandles();//获取所有窗口句柄
  	    	Iterator<String> it = handles.iterator();
  	    	while (it.hasNext()) {
  	    		if (currentWindow == it.next()) {
  	    		continue;
		 }
		 WebDriver window = driver.switchTo().window(it.next());//切换到新窗口
  	    
  	   //定向委托投资标的成立日
  	  new WebDriverWait(this.driver,15).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='con_one_4']/div[2]/div/table/tbody/tr[9]/td[1]")));
  	    
  	  //验证定向委托投资标的成立日的日期
  	  assertTrue("【查看还款协议页】的【定向验证定向委托投资标的成立日】不正确",this.driver.findElement(By.xpath(".//*[@id='con_one_4']/div[2]/div/table/tbody/tr[9]/td[2]")).getText().trim().equals("2015年04月07日"));
  	  System.out.println("【查看还款协议页】的【定向验证定向委托投资标的成立日】为："+this.driver.findElement(By.xpath(".//*[@id='con_one_4']/div[2]/div/table/tbody/tr[9]/td[2]")).getText().trim());
  	  
  	  //定向委托投资标的到期日的日期
  	  assertTrue("【查看还款协议页】的【定向委托投资标的到期日】不正确",this.driver.findElement(By.xpath(".//*[@id='con_one_4']/div[2]/div/table/tbody/tr[9]/td[4]")).getText().trim().equals("2017年04月18日"));
  	  System.out.println("【查看还款协议页】的【定向委托投资标的到期日】为："+ this.driver.findElement(By.xpath(".//*[@id='con_one_4']/div[2]/div/table/tbody/tr[9]/td[4]")).getText().trim());
  	  
  	  //定向委托投资收益起始日的日期
  	  assertTrue("【查看还款协议页】的【定向委托投资收益起始日】不正确",this.driver.findElement(By.xpath(".//*[@id='con_one_4']/div[2]/div/table/tbody/tr[12]/td[2]")).getText().trim().equals("2017年04月10日"));
  	  System.out.println("【查看还款协议页】的【定向委托投资收益起始日】为："+ this.driver.findElement(By.xpath(".//*[@id='con_one_4']/div[2]/div/table/tbody/tr[12]/td[2]")).getText().trim());  
  	  
  	 //定向委托投资收益到期日的日期
  	  assertTrue("【查看还款协议页】的【定向委托投资收益到期日】不正确",this.driver.findElement(By.xpath(".//*[@id='con_one_4']/div[2]/div/table/tbody/tr[12]/td[4]")).getText().trim().equals("2017年04月18日"));
  	  System.out.println("【查看还款协议页】的【定向委托投资收益到期日】为："+ this.driver.findElement(By.xpath(".//*[@id='con_one_4']/div[2]/div/table/tbody/tr[12]/td[4]")).getText().trim());
  	  
  	  //定向委托投资收益获得期的日期
  	  assertTrue("【查看还款协议页】的【定向委托投资收益获得期】不正确",this.driver.findElement(By.xpath(".//*[@id='con_one_4']/div[2]/div/table/tbody/tr[13]/td[2]")).getText().trim().equals("8天"));
      System.out.println("【查看还款协议页】的【定向委托投资收益获得期】为："+this.driver.findElement(By.xpath(".//*[@id='con_one_4']/div[2]/div/table/tbody/tr[13]/td[2]")).getText().trim()); 
  	  }
	}

}
