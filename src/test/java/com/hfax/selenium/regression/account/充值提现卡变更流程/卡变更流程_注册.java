package com.hfax.selenium.regression.account.充值提现卡变更流程;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import com.hfax.selenium.base.BaseTest;
import org.junit.Test;

/**
 * 
 * refacted by wangmingqiang  5/3/16 重构注册功能，提供注册基类
 */
public class 卡变更流程_注册 extends 注册{
	 @Override
	    public void setup() throws Exception {
	        super.supersetup();
	        Properties prop = new Properties();
	    	ClassLoader loader = BaseTest.class.getClassLoader();
	    	InputStream inputStream = loader
	    			.getResourceAsStream("卡变更webDriver.properties");
	    	BufferedReader bf = new BufferedReader(new InputStreamReader(
	    			inputStream));
	    	prop.load(bf);
	    	this.username = prop.getProperty("username");
	    	this.password = prop.getProperty("password");
	    	this.phone = prop.getProperty("phone");
	    	this.name = prop.getProperty("name");
	    	this.identification = prop.getProperty("identification");
	    	this.loadPage();
	    }
	
	
    @Test
    public void 注册流程() throws Exception {
      
    	regist(this.username,this.password,this.phone,this.name,this.identification);
    }
    
}
