package com.hfax.selenium.regression.account.充值提现卡变更流程;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

/**
 *
 * 设置交易密码
 * Created by philip on 3/10/16.
 *
 */
public class 卡变更流程_重置交易密码 extends 重置交易密码 {
	   @Override
	    public void setup() throws Exception {
	        super.supersetup();
	        Properties prop = new Properties();
	    	ClassLoader loader = BaseTest.class.getClassLoader();
	    	InputStream inputStream = loader
	    			.getResourceAsStream("卡变更webDriver.properties");
	    	BufferedReader bf = new BufferedReader(new InputStreamReader(
	    			inputStream));
	    	prop.load(bf);
	    	this.username = prop.getProperty("username");
	    	this.password = prop.getProperty("password");
	        this.loadPage();
	        this.login();
	    }
	   
    @Test
    public void 重置交易密码流程() {
    	redealpass(this.password);
    }
    
    protected void redealpass(String password) {

    	 
        this.driver.findElement(By.id("li_5")).click();

        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[3]")).getText();
                return text.equalsIgnoreCase("交易密码管理");
            }
        });
        System.out.println("-----------我的账户——>用户设置-------------");
        this.driver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[3]")).click();
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@id='setTranPwdBodyDivId']/form")).isDisplayed();
            }
        });
        System.out.println("-----------我的账户——>用户设置——>交易密码管理-------------");
        this.driver.findElement(By.id("clickCode")).click();
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("code_voice_tip")).isDisplayed();
            }
        });
        System.out.println("-----------我的账户——>用户设置——>交易密码管理——>短信验证码已发-------------");
        this.driver.findElement(By.id("code")).sendKeys("1234");
        this.driver.findElement(By.id("ResetNewPass")).sendKeys(password);
        this.driver.findElement(By.id("ResetConfirmPass")).sendKeys(password);
        this.driver.findElement(By.id("btn_submit")).click();
        System.out.println("-----------我的账户——>用户设置——>交易密码管理——>修改提交-------------");
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.switchTo().alert().getText().equalsIgnoreCase("交易密码重置成功");
            }
        });

        this.driver.switchTo().alert().accept();
        
    }
}
