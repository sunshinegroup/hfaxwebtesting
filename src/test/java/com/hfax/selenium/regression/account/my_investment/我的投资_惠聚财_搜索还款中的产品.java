package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class 我的投资_惠聚财_搜索还款中的产品  extends 我的投资_惠理财_搜索惠理财的产品{

	/**
	 * @Description 我的投资_惠聚财_搜索还款中的产品
	 * Created by songxq on 18/8/16.
	 */
	Map<String, String> envVars = System.getenv();
	@Test
	public void 搜索还款中的产品() {
		//等待我的主页
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("我的主页")));
        
        //我的投资->惠聚财
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			 this.selectMenuIE(10, "我的投资");
		 }else{
			 this.selectMenu(10, "我的投资");
		 }
        this.selectSubMenu(5, "惠聚财");
        
         //选择还款中的产品
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("borrowstatus")));
        assertTrue("请选择下拉框不存在",this.driver.findElement(By.id("borrowstatus")).getText().contains("--请选择--"));
        this.searchFinancePro("还款中");
        
        //点击搜索
        assertTrue("搜索不存在",this.driver.findElement(By.id("search")).isDisplayed());
        this.driver.findElement(By.id("search")).click();
        
        //验证惠聚财产品列表不为空
        List<WebElement> list = this.driver.findElements(By.xpath("//tr//td[@align='center']//a[@target='_blank']")); 
  	    assertTrue("不存在惠聚财的产品",list.size()>0);
  	    
  	    
  	    //验证查看协议
  	    this.wait.until(new ExpectedCondition<Boolean>() {
  	    	@Override
  	    	public Boolean apply(WebDriver webDriver) {
          	String text = driver.findElement(By.linkText("查看协议")).getText();
              return text.trim().equalsIgnoreCase("查看协议");
  	    	}
  	    });
  	    
  	    //获取当前窗口句柄
		 String currentWindow = this.driver.getWindowHandle();
  	    //点击查看协议
  	    this.driver.findElement(By.linkText("查看协议")).click();
  	    Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
		 Iterator<String> it = handles.iterator();
		 while (it.hasNext()) {
		 if (currentWindow == it.next()) {
		 continue;
		 }
		  WebDriver window = driver.switchTo().window(it.next());//切换到新窗口
		  window.close();
		 }
		 this.switchToWindow("惠金所");
	
  	    
  	    //验证搜索页的本金(元)
  	    List<WebElement> list1 = this.driver.findElements(By.xpath("//td[@align='center']"));
  	    System.out.println("【惠聚财搜索页】的本金(元)为:"+ list1.get(1).getText().trim());
  	    assertTrue("惠聚财搜索页】的本金(元)不正确",list1.get(1).getText().trim().equals("10,000.00"));
  	    
  	    //验证搜索页的预计收益(元)
  	    System.out.println("【惠聚财搜索页】的预计收益(元)为:"+ list1.get(2).getText().trim());
  	    assertTrue("【惠聚财搜索页】的预计收益(元)不正确",list1.get(2).getText().trim().equals("64,750.00"));
  	    
  	    //验证搜索页的起息时间
  	    System.out.println("【惠聚财搜索页】的起息时间为:"+ list1.get(4).getText().trim());
	    assertTrue("【惠聚财搜索页】的起息时间不正确",list1.get(4).getText().trim().equals("20170102"));
  	    
  	   //验证搜索页的到期时间
	    System.out.println("【惠聚财搜索页】的到期时间为:"+ list1.get(5).getText().trim());
	    assertTrue("【惠聚财搜索页】的到期时间不正确",list1.get(5).getText().trim().equals("20171201"));
  	    
  	    //验证搜索页的标的状态
	    System.out.println("【惠聚财搜索页】的标的状态为:"+ list1.get(6).getText().trim());
	    assertTrue("【惠聚财搜索页】的标的状态不正确",list1.get(6).getText().trim().equals("还款中"));
	    
  	         
	}

}
