package com.hfax.selenium.regression.account.充值提现卡变更流程;


import com.hfax.selenium.base.SuddenDeathBaseTest;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by philip on 3/24/16.
 * refacted by wangmingqiang 4/29/16 重构充值功能提供充值基类,充值前信息校验，增加充值功能，充值后金额检查
 */
public class 挡板充值 extends SuddenDeathBaseTest {
	static protected String recharge = "10000.01";
	
	   @Override
	    public void setup() throws Exception {
	        super.setup();
	        this.loadPage();
	        this.login();
	    }

		protected void supersetup() throws Exception{
			super.setup();
		}
		
    @Test
    public void 充值功能() {
    	recharge(this.card,amount,this.password);
    }
    
    /**
     * 
     * 功能:绑卡信息校验
     * 1.cardno 卡号
     * 
     */
    public void 绑卡信息校验(String cardno){
    	//绑卡信息校验
        String cardNumber = this.driver.findElement(By.xpath("//div[@class='biaoge2']/table/tbody/tr[3]/td[3]")).getText();
        assertTrue("卡号前四位错误", cardNumber.substring(0, 4).equalsIgnoreCase(cardno.substring(0, 4)));
        assertTrue("卡号后四位错误", cardNumber.substring(cardNumber.length() - 4, cardNumber.length()).equalsIgnoreCase(cardno.substring(cardno.length() - 4,cardno.length())));
    }
    
    /**
     * 
     * 功能:充值功能实现
     * 1.cardno 卡号
     * 2.recharge 充值金额
     * 3.交易密码
     * 
     */
    protected void recharge(String cardno,String recharge,String password ){
        this.selectMenu(2, "充值提现");
    	this.selectSubMenu(2, "充值");
    	
    	//绑卡信息校验
    	绑卡信息校验(cardno);
    	
        //充值提示信息校验
        List<WebElement> promptMessages = this.driver.findElements(By.xpath("//span[@class='formtips']"));
        assertTrue("第一条提醒信息不正确", promptMessages.get(0).getText().contains("由于第三方支付公司限制，从2015年10月1日起我平台只支持网银支付，具体恢复时间请关注网站公告"));
        assertTrue("第二条提醒信息不正确", promptMessages.get(1).getText().contains("以下是绑定的银行卡信息，如果没有银行卡请先进行充值银行卡设置"));

        //充值金额可以为空取消校验
        //String amount = this.driver.findElement(By.xpath("//div[@class='biaoge2']/table/tbody/tr[4]/td[2]/strong")).getText();
        //assertTrue("金额不能为空", amount.replace("元", "").trim().length() > 0);
   
        //空输入时充值错误信息校验
        this.driver.findElement(By.id("addrecharge")).click();
        this.wait.until(ExpectedConditions.alertIsPresent());
        String errorMessage = "请输入充值金额";
        assertTrue("空输入时充值错误信息校验--Alert信息提示不正确", this.driver.switchTo().alert().getText().equalsIgnoreCase(errorMessage));
        this.driver.switchTo().alert().accept();
        //充值金额错误提醒校验
        assertTrue("输入提醒信息没出现", this.driver.findElement(By.id("money_tip1")).isDisplayed());
        assertTrue("错误提示不正确:"+this.driver.findElement(By.id("money_tip1")).getText(), this.driver.findElement(By.id("money_tip1")).getText().equalsIgnoreCase(errorMessage));
        System.out.println("-----------我的账户——>充值提现——>充值——>空输入时充值错误信息校验通过-------------");
        //充值金额错误类型输入提示信息校验
        this.driver.findElement(By.id("money1")).sendKeys("abc");
        this.driver.findElement(By.id("addrecharge")).click();
        this.wait.until(ExpectedConditions.alertIsPresent());
        errorMessage = "请输入正确的充值金额，必须为大于0的数字";
        assertTrue("Alert信息提示不正确", this.driver.switchTo().alert().getText().equalsIgnoreCase(errorMessage));
        this.driver.switchTo().alert().accept();
        assertTrue("输入提醒信息没出现", this.driver.findElement(By.id("money_tip1")).isDisplayed());
        assertTrue("错误提示不正确"+this.driver.findElement(By.id("money_tip1")).getText(), this.driver.findElement(By.id("money_tip1")).getText().equalsIgnoreCase(errorMessage));
        System.out.println("-----------我的账户——>充值提现——>充值——>充值金额错误类型输入提示信息校验通过-------------");
        //错误交易密码输入校验
        this.driver.findElement(By.id("money1")).clear();
        this.driver.findElement(By.id("money1")).sendKeys(recharge);
        this.driver.findElement(By.id("dealpwd")).sendKeys("abc");
        this.driver.findElement(By.id("addrecharge")).click();
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//span[@id='pwd_tip']")).getText().contains("交易密码输入不正确");
            }
        });
//        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='pwd_tip']")));
//        assertTrue("错误提示不正确"+this.driver.findElement(By.id("pwd_tip")).getText(), this.driver.findElement(By.id("pwd_tip")).getText().contains("交易密码输入不正确"));
        System.out.println("-----------我的账户——>充值提现——>充值——>错误交易密码输入校验通过-------------");
        //找回密码链接校验
        this.driver.findElement(By.xpath("//span[@id='pwd_tip']/b/a")).click();
        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='tabtil']/ul/li[@class='on']"), "交易密码管理"));
        //返回充值页面
        this.driver.navigate().back();   
        System.out.println("-----------我的账户——>充值提现——>充值——>找回密码链接校验通过-------------");
        //充值功能
        
        String premoney = this.driver.findElement(By.xpath("//table/tbody/tr[4]/td[2]/strong")).getText().replace(",","");
        float  fpremoney = Float.valueOf(premoney.substring(0,premoney.length()-2));
        this.driver.findElement(By.xpath(".//*[@id='money1']")).sendKeys(recharge);
        this.driver.findElement(By.id("dealpwd")).sendKeys(password);
        this.driver.findElement(By.id("addrecharge")).click();
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("moneyjine")).isDisplayed();
            }
        });
        
        String currentWindow = this.driver.getWindowHandle();//获取当前窗口句柄用于通联窗口结束后回切
       
        //跳转网银页面
        this.driver.findElement(By.xpath("//div[@id='cztxZhongTxtDiv2']/b/a")).click();
        try {
            this.driver.switchTo().alert().accept();
        } catch (Exception e) {
        	e.printStackTrace();
        }        
        //两个页面切换通联页面        
        Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
        Iterator<String> it = handles.iterator();

        String nextWindow = it.next();
        if (currentWindow.equals(nextWindow) ) {
        	nextWindow = it.next();
        }
        this.driver.switchTo().window(nextWindow);
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
        public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='btn']")).getAttribute("value").contains("网关支付提交(前台后台回调)");
            }
        });
        System.out.println("-----------我的账户——>充值提现——>充值——>跳转通联挡板成功-------------");
        
        this.driver.findElement(By.xpath(".//*[@id='btn']")).click();
        
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
        public Boolean apply(WebDriver webDriver) {
                return webDriver.switchTo().alert().getText().contains("回调成功");
            }
        });
        this.driver.switchTo().alert().accept();
        this.driver.close();
        System.out.println("-----------我的账户——>充值提现——>充值——>挡板充值成功-------------");
        
        //切回惠金所充值页面
        this.driver.switchTo().window(currentWindow);
        
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("cztxTxt")).isDisplayed();
            }
        });
        System.out.println("-----------我的账户——>充值提现——>充值——>网银页面回切成功-------------");
        
        this.driver.findElement(By.xpath("//div[@id='cztxTxt']//div[@class='popBox']//a[1]")).click();

        String latermoney = this.driver.findElement(By.xpath("//table/tbody/tr[4]/td[2]/strong")).getText().replace(",","");
        
        float  flatermoney = Float.valueOf(latermoney.substring(0,latermoney.length()-2));  
        //     System.out.println("---------------flatermoney="+flatermoney+"----------fpremoney"+fpremoney+"------------flatermoney-fpremoney"+(flatermoney-fpremoney)+"-----------------compare="+Float.compare(flatermoney,fpremoney));
        
        //充值前后金额对比
        assertTrue("充值后金额不正确",(flatermoney-fpremoney>0.0000001&&fpremoney-flatermoney<-0.0000001));
        System.out.println("-----------我的账户——>充值提现——>充值——>充值金额对比失败-------------");
        
        	资金记录查询(recharge);
    }
    
    /**
     * 
     * 功能:充值资金记录查询
     * 1.recharge 充值金额
     * 
     */
    private void 资金记录查询(String recharge){
    //资金记录查询
    this.selectSubMenu(1, "资金记录");
    String recordname = this.driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td[3]")).getText().replace(",","");
    assertTrue("充值成功资金记录未找到",recordname.equals("充值成功"));
    String getcash = this.driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td[5]")).getText().replace(",","");
    assertTrue("充值金额不正确",getcash.equals(recharge));   
    System.out.println("-----------我的账户——>充值提现——>充值资金记录查询成功-------------");

    }
}