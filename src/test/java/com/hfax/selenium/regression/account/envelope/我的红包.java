package com.hfax.selenium.regression.account.envelope;

import static org.junit.Assert.assertTrue;

import java.util.List;

import com.hfax.selenium.regression.account.AccountBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
/**
 * 
 * @author changwj
 * @Description 我的账户--我的红包
 */
public class 我的红包 extends AccountBaseTest {
    private void verifyTableHeaders(String[] expectedHeaders, List<WebElement> headers) {
        assertTrue("抬头列数不对", expectedHeaders.length == headers.size());

        for (int i = 0; i < expectedHeaders.length; i ++) {
            assertTrue("抬头列 " + i + " 文字不正确", expectedHeaders[i].equalsIgnoreCase(headers.get(i).getText()));
        }
    }
    
	@Test
	public void 我的红包() {
        this.selectMenu(3, "我的红包");
        
        //验证表格加载是否正确
        List<WebElement> tds = this.driver.findElements(By.xpath("//div[@class='biaoge']/table/tbody/tr[1]/th"));
        this.verifyTableHeaders(new String[] {"红包金额", "获取来源", "获得时间","发放时间","状态"}, tds);
	}


}
