package com.hfax.selenium.regression.account.account;

import com.hfax.selenium.base.BaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertTrue;

/**
 * Created by philip on 4/5/16.
 */
public class 登陆 extends BaseTest {
    @Override
    public void setup() throws Exception {
        super.setup();

        this.shouldExitOnTearDown = false;
        this.loadPage();
        this.driver.findElement(By.className("login_tab_but")).click();
    }

    @Test
    public void 登陆失败无输入() {
        this.driver.findElement(By.id("btn_login")).click();

        WebElement usernameElement = this.driver.findElement(By.id("s_email"));
        WebElement passwordElement = this.driver.findElement(By.id("s_password"));
        WebElement codeElement = this.driver.findElement(By.id("s_code"));

        assertTrue("用户名错误信息没显示", usernameElement.isDisplayed());
        assertTrue("密码错误信息没显示", passwordElement.isDisplayed());
        assertTrue("验证码错误信息没显示", codeElement.isDisplayed());

        assertTrue("用户名错误信息不正确", usernameElement.getText().equalsIgnoreCase("请输用户名！"));
        assertTrue("密码错误信息不正确", passwordElement.getText().equalsIgnoreCase("请输入密码！"));
        assertTrue("用户名错误信息不正确", codeElement.getText().equalsIgnoreCase("请输入验证码！"));
    }

    @Test
    public void 登录失败用户名不存在() {
        this.driver.findElement(By.id("email")).sendKeys("p@p.com");
        this.driver.findElement(By.id("password")).sendKeys("fakepasswd");
        this.driver.findElement(By.id("code_temp")).sendKeys("1234");

        this.driver.findElement(By.id("btn_login")).click();

        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("dataMsgId")));
        assertTrue("错误信息不正确", this.driver.findElement(By.id("dataMsgId")).getText().equalsIgnoreCase("用户名不存在"));
    }
}
