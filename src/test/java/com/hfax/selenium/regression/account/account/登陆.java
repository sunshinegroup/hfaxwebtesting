package com.hfax.selenium.regression.account.account;

import java.util.Iterator;
import java.util.Set;

import com.hfax.selenium.base.BaseTest;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertTrue;

/**
 * Created by philip on 4/5/16.
 */
public class 登陆 extends BaseTest {
    @Override
    public void setup() throws Exception {
        super.setup();
        this.loadPage();
        this.driver.findElement(By.className("login_tab_but")).click();
    	this.driver.findElement(By.id("email")).clear();
    	this.driver.findElement(By.id("password")).clear();
    	this.driver.findElement(By.id("code_temp")).clear();

    }

    @Test
    public void 登陆失败无输入() {
        this.driver.findElement(By.id("btn_login")).click();

        WebElement usernameElement = this.driver.findElement(By.id("s_email"));
        WebElement passwordElement = this.driver.findElement(By.id("s_password"));
        WebElement codeElement = this.driver.findElement(By.id("s_code"));

        assertTrue("用户名错误信息没显示", usernameElement.isDisplayed());
        assertTrue("密码错误信息没显示", passwordElement.isDisplayed());
        assertTrue("验证码错误信息没显示", codeElement.isDisplayed());

        assertTrue("用户名错误信息不正确", usernameElement.getText().equalsIgnoreCase("请输用户名！"));
        assertTrue("密码错误信息不正确", passwordElement.getText().equalsIgnoreCase("请输入密码！"));
        assertTrue("用户名错误信息不正确", codeElement.getText().equalsIgnoreCase("请输入验证码！"));
    }

   @Test
    public void 登录失败用户名不存在() {
        this.driver.findElement(By.id("email")).sendKeys("p@p.com");
        this.driver.findElement(By.id("password")).sendKeys("fakepasswd");
        this.driver.findElement(By.id("code_temp")).sendKeys("1234");

        this.driver.findElement(By.id("btn_login")).click();

        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("dataMsgId")));
        assertTrue("'用户名不存在'错误提示信息不正确", this.driver.findElement(By.id("dataMsgId")).getText().equalsIgnoreCase("用户名不存在"));
    }
    
    @Test
    public void 密码错误提示(){
    	
        this.driver.findElement(By.id("email")).sendKeys(this.username);
        this.driver.findElement(By.id("password")).sendKeys("fakepasswd");
        this.driver.findElement(By.id("code_temp")).sendKeys("1234");

        this.driver.findElement(By.id("btn_login")).click();
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("dataMsgId")));
        assertTrue("'密码错误'错误提示信息不正确", this.driver.findElement(By.id("dataMsgId")).getText().equalsIgnoreCase("密码错误"));
    }


@Test
public void 登陆_忘记密码链接(){

    this.driver.findElement(By.linkText("忘记密码")).click();  
    //立即判断新打开窗口句柄，存在窗口未及时打开情况，所以增加延迟后再切换窗口
    try {
		Thread.sleep(1000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

    String currentWindow = this.driver.getWindowHandle();//获取当前窗口句柄
    Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
    Iterator<String> it = handles.iterator();

    String nextWindow = it.next();
    if (currentWindow.equals(nextWindow) ) {
    	nextWindow = it.next();
    }
    this.driver.switchTo().window(nextWindow);//切换到新窗口
    this.driver.findElement(By.xpath(".//*[@id='codeBtn']")).click();    
    assertTrue("忘记密码链接测试失败", this.driver.findElement(By.xpath("//a[@id='codeBtn']")).getText().equalsIgnoreCase("获取短信验证码"));
    }
 
@Test
public void 登陆_免费注册链接(){
	
	 this.driver.findElement(By.linkText("免费注册")).click();  
    this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[17]/div[@class='reg-con-t']")));
    assertTrue("免费注册链接测试失败", this.driver.findElement(By.xpath("//div[17]/div[@class='reg-con-t']")).getText().equalsIgnoreCase("惠金所投资流程"));
}

@Test
public void 登陆_登录功能按钮(){
	
    this.driver.findElement(By.id("email")).sendKeys(this.username);
    this.driver.findElement(By.id("password")).sendKeys(this.password);
    this.driver.findElement(By.id("code_temp")).sendKeys("1234");

	 this.driver.findElement(By.xpath(".//*[@id='btn_login']")).click();

    
    this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='userName']")));
    assertTrue("登录功能失败", this.driver.findElement(By.xpath("//a[@class='userName']")).getText().equals(this.username));
}


}
    
