package com.hfax.selenium.regression.account.settings;

import static org.junit.Assert.assertTrue;

import javax.xml.xpath.XPath;

import org.cyberneko.html.xercesbridge.XercesBridge;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

/**
 * 
 * @author changwj
 * @Description 用户设置--
 */
public class 银行卡设置 extends AccountBaseTest {
	private void load(){
		用户设置();
		this.driver.findElement(By.id("li_tx")).click();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath(".//*[@id='bankInfo']/div/table/tbody/tr[1]/th[1]"));
			}
		});
		
		String text = this.driver.findElement(By.xpath(".//*[@id='bankInfo']/div/table/tbody/tr[1]/th[1]")).getText();
		
    	assertTrue("获取银行户名失败",text.equals("银行户名"));
		
	}

	/**
	 * 判断用户名是否与读取的用户名一致
	 */
	@Test
	public void 判断用户名是否一致(){
        load();
		String name = this.driver.findElement(By.xpath(".//*[@id='bankInfo']/div/table/tbody/tr[2]/td[1]")).getText();
		assertTrue("银行户名不一致", name.equals(this.name));
	}

    /**
     * 判断卡号是否一致
     */
    @Test
    public void 判断卡号是否一致(){
        load();
        String text = this.driver.findElement(By.xpath(".//*[@id='bankInfo']/div/table/tbody/tr[2]/td[3]")).getText();

        String lastFourDigits = text.substring(text.length()-4, text.length());
        String firstFourDigits = text.substring(0,4);

        String expectedLastFourDigits = this.card.substring(this.card.length()-4, this.card.length());
        String expectedFirstFourDigits = this.card.substring(0,4);

        assertTrue("卡号不一致", lastFourDigits.equalsIgnoreCase(expectedLastFourDigits) && firstFourDigits.equalsIgnoreCase(expectedFirstFourDigits));
    }
}
