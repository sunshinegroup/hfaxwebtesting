package com.hfax.selenium.regression.account.充值提现卡变更流程;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import org.junit.Test;

import com.hfax.selenium.base.BaseTest;

/**
 * 提现 Created by wangmingqiang on 4/27/16.
 * 
 */
public class 卡变更流程_全额提现 extends 提现 {

	@Override
	public void setup() throws Exception {
		super.supersetup();
		Properties prop = new Properties();
	    	ClassLoader loader = BaseTest.class.getClassLoader();
	    	InputStream inputStream = loader
	    			.getResourceAsStream("卡变更webDriver.properties");
	    	BufferedReader bf = new BufferedReader(new InputStreamReader(
	    			inputStream));
	    	prop.load(bf);
	    	this.username = prop.getProperty("username");
	    	this.password = prop.getProperty("password");	
	        this.loadPage();
	        this.login();
	}
	
	@Test
	@Override
	public void 提现功能() {
		withdraw(true,getcash,this.password);

	}
	
}
