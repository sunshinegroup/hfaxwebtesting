package com.hfax.selenium.regression.account.friends;

import static org.junit.Assert.assertTrue;

import com.hfax.selenium.regression.account.AccountBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;

/**
 * 
 * @author changwj
 * @Description 我的账户--我的好友
 */
public class 我的好友 extends AccountBaseTest {
	@Test
	public void 我的好友() {
		this.selectMenu(6, "我的好友");
	}
}
