package com.hfax.selenium.regression.account.settings;

import static org.junit.Assert.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;

import org.cyberneko.html.xercesbridge.XercesBridge;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

/**
 * 
 * @author changwj
 * @Description 银行卡设置
 * @Modify by songxq 24/8/16
 * 
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 充值后银行卡的设置 extends AccountBaseTest {
	
	private void load(){
		用户设置();

		//点击银行卡设置
		this.selectSubMenu(6,"银行卡设置");
		
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath(".//*[@id='bankInfo']/div/table/tbody/tr[1]/th[1]"));
			}
		});
		
		String text = this.driver.findElement(By.xpath(".//*[@id='bankInfo']/div/table/tbody/tr[1]/th[1]")).getText();
		
    	assertTrue("获取银行户名失败",text.equals("银行户名"));
		
	}

	/**
	 * 判断用户名是否与读取的用户名一致
	 */
	@Test
	public void test001_判断用户名是否一致(){
        load();
		String name = this.driver.findElement(By.xpath(".//*[@id='bankInfo']/div/table/tbody/tr[2]/td[1]")).getText();
		assertTrue("银行户名不一致", name.equals(this.name));
	}

    /**
     * 判断卡号是否一致
     */

    @Test
    public void test002_判断卡号是否一致(){
        load();
        String text = this.driver.findElement(By.xpath(".//*[@id='bankInfo']/div/table/tbody/tr[2]/td[3]")).getText();

        String lastFourDigits = text.substring(text.length()-4, text.length());
        String firstFourDigits = text.substring(0,4);

        String expectedLastFourDigits = this.card.substring(this.card.length()-4, this.card.length());
        String expectedFirstFourDigits = this.card.substring(0,4);

        assertTrue("卡号不一致", lastFourDigits.equalsIgnoreCase(expectedLastFourDigits) && firstFourDigits.equalsIgnoreCase(expectedFirstFourDigits));
    }
  
     @Test
     public void test003_验证充值后的银行卡变更(){
    	 	load();

    		//点击变更
		 	this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	webDriver.findElement(By.linkText("变更")).click();
	            	String text;
	            	try{
	            		text = webDriver.findElement(By.xpath(".//*[@id='cantbeChange']/div[2]/div/a")).getText().trim();
	            		
	            		}catch(Exception e){
	         
	            		text = webDriver.findElement(By.linkText("变更")).getText().trim();
	            	}
	            	return text.equalsIgnoreCase("我知道了");
	            	}
	        });
    	 
     	//等待页面加载
     	new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
   	 		@Override
   	 		public Boolean apply(WebDriver webDriver) {
   	 			return webDriver.findElement(By.xpath(".//*[@id='cantbeChange']/div[2]/p")).isDisplayed();
   	 		
   	 		}
   	 	});
     	
     	if(this.driver.getPageSource().contains("您的待收本息金额或账户可用余额不为零，暂时无法进行")){
     		//验证提示信息
     		System.out.println("输出提示信息为：" + this.getElementText(".//*[@id='cantbeChange']/div[2]/p"));
     		assertTrue("点击变更后的提示信息不正确",this.getElementText(".//*[@id='cantbeChange']/div[2]/p").equals("您的待收本息金额或账户可用余额不为零，暂时无法进行"+
     		"\n"+"银行卡变更，如有疑问请联系客服热线 400-015-8800"+"\n"+"（工作日8:30~21:00）"));   
     		//点击我知道了
     		this.driver.findElement(By.xpath(".//*[@id='cantbeChange']/div[2]/div/a")).click();
     	}
     	else{
     		   fail("变更后提示不正确！");
     	}
     	
     	
    	 
     } 
    
}
