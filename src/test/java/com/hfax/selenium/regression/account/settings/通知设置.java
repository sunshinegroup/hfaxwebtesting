package com.hfax.selenium.regression.account.settings;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;
/**
 * 
 * @author changwj
 * @Description 通知设置
 */
public class 通知设置 extends AccountBaseTest{
	/**
	 * 检测默认的对勾是否被选中
	 */
	@Test
	public void 通知设置检测(){
		用户设置();
		this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/ul/li[5]")).click();

		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div/table/tbody/tr[1]/th"));
			}
		});
		String text = this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div/table/tbody/tr[1]/th")).getText();
		assertTrue("选择通知方式失败",text.equals("选择通知方式"));
				
		//List<WebElement> findElements = driver.findElements(By.xpath("//input[@type='checkbox']"));
		assertTrue("少勾一个",null != driver.findElement(By.id("message")).getAttribute("checked") || !driver.findElement(By.id("message")).getAttribute("checked").trim().equals("checked"));
		assertTrue("少勾一个",null != driver.findElement(By.id("messageReceive")).getAttribute("checked") || !driver.findElement(By.id("messageReceive")).getAttribute("checked").trim().equals("checked"));
		assertTrue("少勾一个",null != driver.findElement(By.id("messageDeposit")).getAttribute("checked") || !driver.findElement(By.id("messageDeposit")).getAttribute("checked").trim().equals("checked"));
		assertTrue("少勾一个",null != driver.findElement(By.id("messageBorrow")).getAttribute("checked") || !driver.findElement(By.id("messageBorrow")).getAttribute("checked").trim().equals("checked"));
		assertTrue("少勾一个",null != driver.findElement(By.id("messageRecharge")).getAttribute("checked") || !driver.findElement(By.id("messageRecharge")).getAttribute("checked").trim().equals("checked"));
		assertTrue("少勾一个",null != driver.findElement(By.id("messageChange")).getAttribute("checked") || !driver.findElement(By.id("messageChange")).getAttribute("checked").trim().equals("checked"));
		assertTrue("少勾一个",null != driver.findElement(By.id("note")).getAttribute("checked") || !driver.findElement(By.id("note")).getAttribute("checked").trim().equals("checked"));
		assertTrue("少勾一个",null != driver.findElement(By.id("noteDeposit")).getAttribute("checked") || !driver.findElement(By.id("noteDeposit")).getAttribute("checked").trim().equals("checked"));
		assertTrue("少勾一个",null != driver.findElement(By.id("noteRecharge")).getAttribute("checked") || !driver.findElement(By.id("noteRecharge")).getAttribute("checked").trim().equals("checked"));
	}
}
