package com.hfax.selenium.regression.account.transfer;

import static org.junit.Assert.*;

import java.awt.AWTException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;
import com.hfax.selenium.rule.SuddenDeathRule;
/**
 * @Description 投资转让_用户投标
 * Created by songxq on 29/4/16.
 */

public class 投资转让_用户投资惠理财 extends AccountBaseTest{

	
	protected WebElement clickOnSubmenu(String title) {
        WebElement submenu = this.driver.findElement(By.id("listTab-box"));
        submenu.findElement(By.linkText(title)).click();

        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'惠理财')]")));

        List<WebElement> elements = this.driver.findElements(By.className("list-control"));

        for (WebElement el : elements) {
            if (el.isDisplayed()) return el;
        }

        assertTrue("无内容显示: " + title, false);

        return null;
    }

	//验证客户投资理财产品
	@Test
	public void 客户投资理财产品() throws AWTException {
		 //进入惠理财页面
		this.navigateToPage("投资理财");
		WebElement content = this.clickOnSubmenu("惠理财");
    
		//点击第一个产品
		WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
		firstItem.findElement(By.xpath("div[2]/div[2]/a")).click();
    
    
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='balanceTitle']")));

		//验证剩余可投金额
		assertTrue("剩余可投金额不存在", this.driver.findElement(By.xpath("//div[@class='balanceTitle']")).isDisplayed());

		//验证项目概况
		assertTrue("项目概况不存在", this.driver.findElement(By.id("one1")).isDisplayed());
		this.driver.findElement(By.id("one1")).click();

		//验证投资说明书
		assertTrue("投资说明书不存在", this.driver.findElement(By.id("one2")).isDisplayed());
		this.driver.findElement(By.id("one2")).click();

		//验证风险提示书
		assertTrue("风险提示书不存在", this.driver.findElement(By.id("one3")).isDisplayed());
		this.driver.findElement(By.id("one3")).click();

		//验证投资管理合同
		assertTrue("风险提示书不存在", this.driver.findElement(By.id("one4")).isDisplayed());
		this.driver.findElement(By.id("one4")).click();

		WebElement amount = this.driver.findElement(By.xpath(".//*[@id='amount']"));
		assertTrue("投资理财金额不存在", amount.isDisplayed());
		amount.clear();
		amount.sendKeys(this.userremaininginvestamount);

		this.driver.findElement(By.xpath(".//*[@id='agre']")).click();

		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("去投资")));
		this.driver.findElement(By.linkText("去投资")).click();
    
		new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='tpwdid']")));
		//投资红包
		if(IsElementPresent(By.xpath("//div[@id='usecoupon']/div/input[1]")))
		{   
			new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='usecoupon']/div/input[1]")));
			this.driver.findElement(By.xpath("//div[@id='usecoupon']/div/input[1]")).click();
			this.driver.findElement(By.xpath("//input[@id='tpwdid']")).clear();
			this.driver.findElement(By.xpath("//input[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
    	}
		else{
    	//输入交易密码
		    new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='tpwdid']")));
    	    this.driver.findElement(By.xpath("//input[@id='tpwdid']")).clear();
    	    this.driver.findElement(By.xpath("//input[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
		}
		
		//点击确定投资
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@id='besurebutton']/button[1]")).isDisplayed();
	            }
	        });
        this.driver.findElement(By.xpath("//div[@id='besurebutton']/button[1]")).click();
    
		//投资成功pop窗口
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='investSuccess']/div[2]/p")));
		assertTrue("投资成功跳出的窗口不存在",this.driver.findElement(By.xpath("//div[@id='investSuccess']/div[2]/p")).isDisplayed());
    
		//关闭pop窗口
		this.driver.findElement(By.xpath("//div[@id='investSuccess']/div[2]/b/a")).click();
   
	}


}
