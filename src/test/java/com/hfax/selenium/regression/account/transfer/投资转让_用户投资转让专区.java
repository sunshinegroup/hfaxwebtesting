package com.hfax.selenium.regression.account.transfer;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.base.SuddenDeathBaseTest;
import com.hfax.selenium.regression.account.AccountBaseTest;
import com.hfax.selenium.rule.SuddenDeathRule;

public class 投资转让_用户投资转让专区 extends SuddenDeathBaseTest{

	/**
	 * @Description 投资转让_已转入的项目查看
	 * Created by songxq on 4/5/16.
	 */
	

	@Override
	public void setup() throws Exception {
		super.setup();
		this.loadPage();
		this.transferLogin();
    }
	@Test
	public void 用户投资转让专区() throws Exception{
	
		//用户进入转让专区
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("转让专区")));
	    this.navigateToPage("转让专区");
		
		 //验证用户正常跳转到转让专区
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='navBox']")));
		 assertTrue("用户没有跳转到转让专区", this.driver.findElement(By.xpath("//div[@class='navBox']")).isDisplayed());
		 
		 
		 //验证转让专区第一个产品的立即投资存在
		 WebElement invest = this.driver.findElement(By.linkText("立即投资"));
		 assertTrue("用户没有找到第一个转让专区立即投资的产品", invest.isDisplayed());
		 String currentWindow = driver.getWindowHandle();//获取当前窗口句柄
		 //点击立即投资
		 invest.click();
		 Set<String> handles = driver.getWindowHandles();//获取所有窗口句柄
		 Iterator<String> it = handles.iterator();
		 while (it.hasNext()) {
		 if (currentWindow == it.next()) {
		 continue;
		 }
		 WebDriver window = driver.switchTo().window(it.next());//切换到新窗口
		 //验证转让协议存在
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='agre']")));
		 WebElement protocol = this.driver.findElement(By.xpath("//input[@id='agre']"));
		 assertTrue("转让协议没有出现", protocol.isDisplayed());
		 
		 //点击项目概况
		 assertTrue("项目概况没有出现", this.driver.findElement(By.xpath("//strong[@id='one1']")).isDisplayed());
		 this.driver.findElement(By.xpath("//strong[@id='one1']"));
		 
		 //点击转让协议
		 assertTrue("转让协议没有出现", this.driver.findElement(By.xpath("//strong[@id='one2']")).isDisplayed());
		 this.driver.findElement(By.xpath("//strong[@id='one2']"));
		 
		 //点击已阅读并同意
		 this.driver.findElement(By.xpath("//input[@id='agre']")).click();
		 
		 //点击我要投资
		 this.driver.findElement(By.linkText("我要投资")).click();
		 
		 //滚轮向下
		 ((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		 //验证惠金所交易密码存在
		 assertTrue("惠金所交易密码不存在", this.driver.findElement(By.xpath("//input[@id='tpwdid']")).isDisplayed());
		 WebElement ps = this.driver.findElement(By.xpath("//input[@id='tpwdid']"));
		 //输入交易密码
		 ps.clear();
		 ps.sendKeys(this.transferjiaoyipassword);
		 //点击确定投资
		 assertTrue("确认投资不存在", this.driver.findElement(By.xpath("//button[@id='besureInvest']")).isDisplayed());
		 this.driver.findElement(By.xpath("//button[@id='besureInvest']")).click();
		 
		 //验证投资成功后跳转的窗口
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("关闭")));
		 //关闭窗口
    	 this.driver.findElement(By.linkText("关闭")).click();
		 
		}
		 

		 
	}

}
