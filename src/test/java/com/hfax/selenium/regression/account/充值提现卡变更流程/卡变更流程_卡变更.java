package com.hfax.selenium.regression.account.充值提现卡变更流程;

import static org.junit.Assert.assertEquals;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * 银行卡变更 Created by wangmingqiang on 4/27/16.
 *
 */
public class 卡变更流程_卡变更 extends SuddenDeathBaseTest {
	 @Override
	    public void setup() throws Exception {
	        super.setup();
	        Properties prop = new Properties();
	    	ClassLoader loader = BaseTest.class.getClassLoader();
	    	InputStream inputStream = loader
	    			.getResourceAsStream("卡变更webDriver.properties");
	    	BufferedReader bf = new BufferedReader(new InputStreamReader(
	    			inputStream));
	    	prop.load(bf);
	    	this.username=prop.getProperty("username");
	    	this.password=prop.getProperty("password");
	    	this.repassword=prop.getProperty("repassword");
	    	this.phone=prop.getProperty("phone");
	    	this.name=prop.getProperty("name");
	    	this.identification=prop.getProperty("identification");
	    	this.card=prop.getProperty("card");
	    	this.amount=prop.getProperty("amount");
	    	
	    	this.bankname=prop.getProperty("bankname");
	    	this.bankid=prop.getProperty("bankid");
			this.recard=prop.getProperty("recard");
			this.rebankname=prop.getProperty("rebankname");
			this.rebankid=prop.getProperty("rebankid");
	        this.loadPage();
	        this.login();
	    }
	
	@Test
	public void 银行卡变更功能() {

		bankcardchange(this.name,this.phone,this.password,this.rebankname,this.rebankid,this.recard,this.bankname);
	}
	
	
    /**
     * 
     * 功能:充值资金记录查询
     * 1.username 真实姓名
     * 2.phone 卡变更手机号
     * 3.password 交易密码
     * 4.rebankname 变更银行名称
     * 5.rebankid 变更银行ID
     * 6.recard 变更银行卡号
     * 7.bankname 历史银行名称
     * 
     */
	
	public void bankcardchange(String name,String phone,String password,String rebankname,String rebankid,String recard,String bankname){
		// 用户设置
		this.driver.findElement(By.id("li_5")).click();

		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				String text = webDriver.findElement(
						By.xpath(".//*[@id='li_tx']")).getText();
				return text.equalsIgnoreCase("银行卡设置");
			}
		});
		System.out.println("-----------我的账户——>用户设置-------------");
		
		// 银行卡设置
		this.driver.findElement(By.xpath(".//*[@id='li_tx']")).click();
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				// 银行卡变更连接加载
				return webDriver
						.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td[5]/a")).getAttribute("href")
						.contains("javascript:changeBankInfos('");
			}
		});
		System.out.println("-----------我的账户——>用户设置——>银行卡设置-------------");

		// 经验证直接点击'变更'连接基本无法正常打开变更银行卡窗口，换JS方式打开与前台触发方式相同
		// this.driver.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td[5]/a")).click();

		String changedata = this.driver.findElement(
				By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td[5]/a")).getAttribute("href");
		String[] data = changedata.substring(27, changedata.length() - 1).split(",");
		// 打开变更链接
		((JavascriptExecutor) this.driver).executeScript("changeBankInfos("+ data[0] + ",'"+name+"'," + data[2] + ");");
		

		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				// 银行卡变更视窗是否加载
				return webDriver
						.findElement(
								By.xpath(".//*[@id='changeBankDiv']/div[2]/h2/strong"))
						.getText().equals("变更银行卡");
			}
		});
		System.out.println("-----------我的账户——>用户设置——>银行卡设置——>银行卡变更(显示区)-------------");
		// 选择银行类型下拉框
		Boolean flag = true;
		int count=0;
		do{
		Select select = new Select(this.driver.findElement(By.id("bankName_")));
		select.selectByValue(rebankid);
		this.driver.findElement(By.id("bankCard_")).clear();
		this.driver.findElement(By.id("bankCard_")).sendKeys(recard);
		
		this.driver.findElement(By.id("bankCellPhone")).clear();
		this.driver.findElement(By.id("bankCellPhone")).sendKeys(phone);
		
		this.driver.findElement(By.id("jiaoyimimak_")).clear();
		this.driver.findElement(By.id("jiaoyimimak_")).sendKeys(password);
		
		this.driver.findElement(By.id("changebank")).click();
		String errmsg = this.driver.findElement(By.xpath(".//*[@id='bk1_tip_']")).getText();
		flag = !errmsg.equals("");
//		System.out.println("-----------flag="+flag+"-------errmsg="+errmsg+"-----");
		count++;
		}while(flag&&count<3);

		// 等待确认变更视窗
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return webDriver
						.findElement(
								By.xpath(".//*[@id='cztxTxtChange']/div[2]/h2/strong"))
						.getText().equals("银行卡变更");
			}
		});
		
		// 点击确认变更按钮
		this.driver.findElement(By.xpath(".//*[@id='changeCheckSure']"))
				.click();
		System.out.println("-----------我的账户——>用户设置——>银行卡设置——>银行卡变更(显示区)——>确认变更-------------");
		// 等待变更成功视窗
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return webDriver
						.findElement(
								By.xpath(".//*[@id='changeCardCheckingResult']"))
						.getText().equals("变更成功");
			}
		});
		System.out.println("-----------我的账户——>用户设置——>银行卡设置——>变更成功-------------");
		// 点击充值成功按钮回到原页面
		this.driver.findElement(
				By.xpath(".//*[@id='changeCardCheckingResult']")).click();

		// 判断银行卡信息加载
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return webDriver.findElement(
						By.xpath(".//*[@id='bankInfo']/div/table"))
						.isDisplayed();
			}
		});
		System.out.println("-----------我的账户——>用户设置——>银行卡设置——>变更成功——>变更卡信息加载成功-------------");
		// 判断变更银行卡信息
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return (webDriver.findElement(By.xpath(".//*[@id='bankInfo']/div/table")).isDisplayed())&&
					   (webDriver.findElement(By.xpath(".//*[@id='bankInfoHis']/div/table")).isDisplayed());
			}
		});

		String showname = this.driver.findElement(
				By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td[1]"))
				.getText();
		String showbankname = this.driver.findElement(
				By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td[2]"))
				.getText();
		String showcardno = this.driver.findElement(
				By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td[3]"))
				.getText();
		StringBuffer showcardno2 = new StringBuffer(showcardno.substring(0, 4))
				.append(showcardno.substring(showcardno.length() - 4,
						showcardno.length()));
		StringBuffer realcardno = new StringBuffer(recard.substring(0, 4))
				.append(recard.substring(recard.length() - 4,
						recard.length()));
		String showcardstate = this.driver.findElement(
				By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td[4]"))
				.getText();

		assertEquals("绑卡信息_用户姓名不正确", name, showname);
		assertEquals("绑卡信息_银行名称不正确", rebankname, showbankname);
		assertEquals("绑卡信息_卡号信息不正确", realcardno.toString(),
				showcardno2.toString());
		assertEquals("绑卡信息_绑卡状态不正确", "已签约已验证", showcardstate);

		// 判断历史绑卡信息
		String hisbankname = this.driver
				.findElement(
						By.xpath(".//*[@id='bankInfoHis']/div/table/tbody/tr[2]/td[2]"))
				.getText();
		assertEquals("绑卡信息_历史银行名称不正确", bankname, hisbankname);
	}

}
