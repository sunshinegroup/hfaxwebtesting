package com.hfax.selenium.regression.account.充值提现卡变更流程;

import com.hfax.selenium.base.SuddenDeathBaseTest;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.*;

/**
 *
 * 企业注册
 * Created by philip on 3/8/16.
 * refacted by wangmingqiang  5/3/16 重构注册功能，提供注册基类
 */
public class 注册 extends SuddenDeathBaseTest {
	
	@Override
	public void setup() throws Exception {
	        super.setup();
	        this.loadPage();
	    }

	protected void supersetup() throws Exception{
	super.setup();
	}
	
    @Test
    public void 注册流程() throws Exception {
      
    	regist(this.username,this.password,this.phone,this.name,this.identification);
    }
    
    /**
     * 
     * 功能:注册功能
     * 1.username 用户名
     * 2.password 登陆密码/交易密码
     * 3.phone 电话号码
     * 4.name 姓名
     * 5.identification 身份证号码
     * 
     */
    
    protected void regist(String username,String password,String phone,String name,String identification){
    	this.driver.findElement(By.className("register_tab_but")).click();
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("regist-content")));
        this.driver.findElement(By.className("reg-del")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("regist-content")));

        // 第一步注册信息
        this.driver.findElement(By.id("userName")).sendKeys(username);
        this.driver.findElement(By.id("password")).sendKeys(password);
        this.driver.findElement(By.id("confirmPassword")).sendKeys(password);
        //图片验证码
        this.driver.findElement(By.id("code")).sendKeys("1234");
        this.driver.findElement(By.id("agre")).click();
        this.driver.findElement(By.id("btn_register")).click();

        // 第二步手机验证
        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//form[@id='mobilesend']/h2"), "尊敬的客户，为保障资金安全，现在将对您的手机号码进行验证。"));
        this.driver.findElement(By.id("phoneNum")).sendKeys(phone);
        this.driver.findElement(By.id("codeBtn")).click();

        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.id("code_voice_tip")).isDisplayed();
            }
        });
        //手机验证码
        this.driver.findElement(By.id("phoneCode")).sendKeys("1234");
        this.driver.findElement(By.id("ph_btn")).click();

        // 第三步身份验证
        wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.className("regOk"));
            }
        });

        this.driver.findElement(By.id("realName")).sendKeys(name);
        this.driver.findElement(By.id("idCard")).sendKeys(identification);
        this.driver.findElement(By.id("auth_btn")).click();

        // 等待登陆跳转
        wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.className("userName"));
            }
        });

        String usernameToConfirm = this.driver.findElement(By.className("userName")).getText();
        assertTrue("登陆无法确认", usernameToConfirm.equalsIgnoreCase(username));
    }
}
