package com.hfax.selenium.regression.account.transfer;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.account.AccountBaseTest;
import com.hfax.selenium.rule.SuddenDeathRule;

public class 投资转让_已转出的项目查看  extends AccountBaseTest{

	/**
	 * @Description 投资转让_可转让的项目查看
	 * Created by songxq on 29/4/16.
	 */

	@Test
	public void 已转出的项目查看(){
		
		//投资转让->已转出额项目
		 this.selectMenu(14, "投资转让");
		 this.selectSubMenu(3, "已转出的项目");
		 
		 //加载已转出项目
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='con_one_3']/div[1]/h2")));
		 assertTrue("用户没有【已转出的项目】", this.driver.findElement(By.xpath("//div[@id='con_one_3']/div[1]/h2")).isDisplayed());
		 
		 //验证查看详情
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("查看详情")));
		 WebElement detail = this.driver.findElement(By.linkText("查看详情"));
		 assertTrue("用户【已转出的项目】中没有查看详情",detail.isDisplayed());
		 
		 //获取当前窗口句柄
		 String currentWindow = this.driver.getWindowHandle();
		 
		 //点击查看详情
		 detail.click();
		 
		//弹出新窗口
		 Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
		 Iterator<String> it = handles.iterator();
		 while (it.hasNext()) {
		 if (currentWindow == it.next()) {
		 continue;
		 }
		  WebDriver window = driver.switchTo().window(it.next());//切换到新窗口
		  this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("查看协议")));
		  System.out.println("查看协议");
		  WebElement protocol = this.driver.findElement(By.linkText("查看协议"));
		  assertTrue("用户【已转出的项目】中没有查看协议",protocol.isDisplayed());
		  protocol.click();
		  window.close();//关闭新窗口
		 }
		 
		 
	}

}
