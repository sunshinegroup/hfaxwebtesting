package com.hfax.selenium.regression.account.messages;

import static org.junit.Assert.*;


import com.hfax.selenium.regression.account.AccountBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * 
 * @author changwj
 * @Description 我的账户--站内信
 */
public class 站内信 extends AccountBaseTest {
	@Test
	public void 查看站内信() {
        this.selectMenu(4, "站内信");

        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("html/body/div[3]/div/div[2]/div[1]/ul/li"), "系统消息"));
        this.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='biaoge']/table/tbody/tr[2]/td[3]/a")));

        this.driver.findElement(By.xpath("//div[@id='biaoge']/table/tbody/tr[2]/td[3]/a")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("re_biaoge2")));

        String sender = this.driver.findElement(By.xpath("//div[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong")).getText();
        assertTrue("发信人不正确", sender.equalsIgnoreCase("管理员"));

        String recipient = this.driver.findElement(By.xpath("//div[@id='re_biaoge2']/table/tbody/tr[2]/td[2]")).getText();
        assertTrue("收件人不正确", recipient.equalsIgnoreCase(this.username));

        this.driver.findElement(By.xpath("//div[@id='re_biaoge2']//a[@class='scbtn']")).click();
        this.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='biaoge']/table/tbody/tr[2]/td[3]/a")));
	}
}
