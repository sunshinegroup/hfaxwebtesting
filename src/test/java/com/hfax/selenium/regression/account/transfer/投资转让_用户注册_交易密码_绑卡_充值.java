package com.hfax.selenium.regression.account.transfer;

import static org.junit.Assert.*;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.SuddenDeathBaseTest;
import com.hfax.selenium.regression.account.AccountBaseTest;
import com.hfax.selenium.rule.SuddenDeathRule;
/**
 * @Description 投资转让_转让中的项目
 * Created by songxq on 4/5/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 投资转让_用户注册_交易密码_绑卡_充值  extends SuddenDeathBaseTest{
	
	@Before
	public void setup() throws Exception {
		super.setup();
	}
    //注册
	@Test
	public void test001_注册() {
		this.loadPage();
    	
//      this.driver.findElement(By.className("register_tab_but")).click();
      this.driver.findElement(By.linkText("快速注册")).click();
      this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("regist-content")));

      this.driver.findElement(By.className("reg-del")).click();
      this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("regist-content")));

      // 第一步

      this.driver.findElement(By.id("userName")).sendKeys(this.transferusername);
      this.driver.findElement(By.id("password")).sendKeys(this.transferpassword);
      this.driver.findElement(By.id("confirmPassword")).sendKeys(this.transferpassword);
      this.driver.findElement(By.id("code")).sendKeys("1234");

      this.driver.findElement(By.id("agre")).click();
      this.driver.findElement(By.id("btn_register")).click();

      // 第二步
      this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//form[@id='mobilesend']/h2"), "尊敬的客户，为保障资金安全，现在将对您的手机号码进行验证。"));

      this.driver.findElement(By.id("phoneNum")).sendKeys(this.transferphone);
      this.driver.findElement(By.id("codeBtn")).click();

      wait.until(new ExpectedCondition<Boolean>() {
          @Override
          public Boolean apply(WebDriver webDriver) {
              return driver.findElement(By.id("code_voice_tip")).isDisplayed();
          }
      });

      this.driver.findElement(By.id("phoneCode")).sendKeys("1234");
      this.driver.findElement(By.id("ph_btn")).click();

      // 第三步
      wait.until(new ExpectedCondition<WebElement>() {
          @Override
          public WebElement apply(WebDriver webDriver) {
              return webDriver.findElement(By.className("regOk"));
          }
      });

      this.driver.findElement(By.id("realName")).sendKeys(this.transfername);
      this.driver.findElement(By.id("idCard")).sendKeys(this.transferidentification);
      this.driver.findElement(By.id("auth_btn")).click();

      // 等待登陆跳转
      wait.until(new ExpectedCondition<WebElement>() {
          @Override
          public WebElement apply(WebDriver webDriver) {
              return webDriver.findElement(By.className("userName"));
          }
      });

      String usernameToConfirm = this.driver.findElement(By.className("userName")).getText();
      assertTrue("登陆无法确认", usernameToConfirm.equalsIgnoreCase(this.transferusername));
    }
	//交易密码
	@Test 
	  public void test002_交易密码() {
		 this.loadPage();
//       this.driver.findElement(By.className("login_tab_but")).click();
		 this.driver.findElement(By.linkText("登录")).click();
		 this.driver.findElement(By.id("email")).sendKeys(this.transferusername);
		 this.driver.findElement(By.id("password")).sendKeys(this.transferpassword);
		 this.driver.findElement(By.id("code_temp")).sendKeys("1234");
		 this.driver.findElement(By.id("btn_login")).click();
		 
		  this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("li_5")));
	      this.driver.findElement(By.id("li_5")).click();
	        
	        wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                String text = webDriver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[3]")).getText();
	                return text.equalsIgnoreCase("交易密码管理");
	            }
	        });
	        //交易密码管理
	        this.driver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[3]")).click();
	        //等待发送验证码
	        wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath(".//*[@id='clickCode']")).isDisplayed();
	            }
	        });
	        //点击发送验证码
	        this.driver.findElement(By.xpath(".//*[@id='clickCode']")).click();
	        //输入短信验证码
	        this.driver.findElement(By.xpath(".//*[@id='code']")).clear();
	        this.driver.findElement(By.xpath(".//*[@id='code']")).sendKeys("1234");
	        
	        //设置新交易密码
	        this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).clear();
	        this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).sendKeys(this.transferjiaoyipassword);
	        
	        //确认新交易密码
	        this.driver.findElement(By.xpath(".//*[@id='ResetConfirmPass']")).clear();
	        this.driver.findElement(By.xpath(".//*[@id='ResetConfirmPass']")).sendKeys(this.transferjiaoyipassword);
	        
	        //点击确定
	        this.driver.findElement(By.linkText("确定")).click();
	        
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='iKnow']")));
	        assertTrue("提示信息错误",this.driver.findElement(By.xpath(".//*[@id='iKnow']")).getText().trim().equalsIgnoreCase("我知道了"));
	        if(this.driver.getPageSource().contains("密码设置成功"))
			 {
	        	//点击我知道了
	        	 this.driver.findElement(By.xpath(".//*[@id='iKnow']")).click();
			 }else{
				 	fail("交易密码没有设置成功");
			 }
	        
    }
	//绑卡
	@Test 
	 public void test003_绑卡() {
		 	this.loadPage();
//	        this.driver.findElement(By.className("login_tab_but")).click();
	    this.driver.findElement(By.linkText("登录")).click();
		this.driver.findElement(By.id("email")).sendKeys(this.transferusername);
		this.driver.findElement(By.id("password")).sendKeys(this.transferpassword);
		this.driver.findElement(By.id("code_temp")).sendKeys("1234");
		this.driver.findElement(By.id("btn_login")).click();
		
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("li_2")));
        this.driver.findElement(By.id("li_2")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[2]")).getText();
                return text.equalsIgnoreCase("充值");
            }
        });
        //点击充值
        this.driver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[2]")).click();
        
        //等待绑卡提示
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("sure_btn")));
        if(this.driver.getPageSource().contains("请您先绑定银行卡！"))
        {
        	//点击立即绑卡
        	this.driver.findElement(By.className("sure_btn")).click();
		 }else{
			 fail("绑卡提示不正确");
		 }
        
        //等待卡号
         new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("bankCardIdUT1")).isDisplayed();
            }
        });
         
        //输入卡号信息
         this.driver.findElement(By.id("bankCardIdUT1")).clear();
         this.driver.findElement(By.id("bankCardIdUT1")).sendKeys(this.transfercard);
         
         //输入手机号
         this.driver.findElement(By.id("phoneNumberUT1")).clear();
         this.driver.findElement(By.id("phoneNumberUT1")).sendKeys(this.transferphone);
      
         this.wait.until(new ExpectedCondition<Boolean>() {
             @Override
             public Boolean apply(WebDriver webDriver) {
                 return webDriver.findElement(By.xpath(".//*[@id='newBankCardForm']/ul/li[1]/i")).isDisplayed();
             }
         });
        assertTrue("银行户名不正确", this.driver.findElement(By.xpath(".//*[@id='newBankCardForm']/ul/li[1]/i")).getText().trim().equalsIgnoreCase(this.transfername));
         
         //点击发送验证码
         this.driver.findElement(By.id("sendAccountCode")).click();
         
         //输入验证码
         this.driver.findElement(By.id("accountCode")).clear();
         this.driver.findElement(By.id("accountCode")).sendKeys("1234");
         
         //点击提交
         this.driver.findElement(By.className("btnFlont")).click();
         
        //等待确定
         this.wait.until(new ExpectedCondition<Boolean>() {
             @Override
             public Boolean apply(WebDriver webDriver) {
                 return webDriver.findElement(By.xpath(".//*[@id='bindSuccess']/div[2]/div/a[2]")).isDisplayed();
             }
         });
       if(this.driver.getPageSource().contains("绑卡成功！")){
    	   //点击确定
    	   this.driver.findElement(By.xpath(".//*[@id='bindSuccess']/div[2]/div/a[2]")).click();
    	   
       }else{
    	   	fail("绑卡没有成功");
       }
	    }
	 //充值
	 @Test
	    public void test004_充值() {
		 	this.loadPage(this.backupUrl, "惠金所");
//	        this.driver.findElement(By.className("login_tab_but")).click();
	        this.driver.findElement(By.linkText("登录")).click();
			this.driver.findElement(By.id("email")).sendKeys(this.transferusername);
			this.driver.findElement(By.id("password")).sendKeys(this.transferpassword);
			this.driver.findElement(By.id("code_temp")).sendKeys("1234");
			this.driver.findElement(By.id("btn_login")).click();
			
			this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("li_2")));
	        this.driver.findElement(By.id("li_2")).click();

	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                String text = webDriver.findElement(By.xpath("//div[@class='tabtil']/ul/li[2]")).getText();
	                return text.equalsIgnoreCase("充值");
	            }
	        });
	        //点击充值
	        this.driver.findElement(By.xpath("//div[@class='tabtil']/ul/li[2]")).click();
	        
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.id("bankInput")).isDisplayed();
	            }
	        });
	        assertTrue("所属银行不正确", this.driver.findElement(By.id("bankInput")).getAttribute("value").contains("中国建设银行"));
	        
	        //输入充值金额
	        this.driver.findElement(By.id("money1")).clear();
	        this.driver.findElement(By.id("money1")).sendKeys(this.transferamount);
	        
	        //输入交易密码
	        this.driver.findElement(By.id("dealpwd")).clear();
	        this.driver.findElement(By.id("dealpwd")).sendKeys(this.transferjiaoyipassword);
	        
	        //点击提交
	        this.driver.findElement(By.id("addrecharge")).click();
	 

	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.id("moneyjine")).isDisplayed();
	            }
	        });

	        this.driver.findElement(By.xpath("//div[@id='cztxZhongTxtDiv2']/b/a")).click();

	        try {
	            this.driver.switchTo().alert().accept();
	        } catch (Exception e) {}

	        this.wait.until(ExpectedConditions.numberOfWindowsToBe(2));

	        WebDriver cpPayDriver = this.switchToWindow("通联支付");
	        FluentWait<WebDriver> cpPayWait = new WebDriverWait(cpPayDriver, 10000).withTimeout(10, TimeUnit.SECONDS).pollingEvery(100, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);

	        cpPayWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table/tbody/tr[2]/td[2]")));

	        String merchantName = cpPayDriver.findElement(By.xpath("//table/tbody/tr[2]/td[2]")).getText();
	        assertTrue("商户名字不正确", merchantName.contains("通联支付"));

	        String amount = cpPayDriver.findElement(By.xpath("//table/tbody/tr[5]/td[2]")).getText();
	        assertTrue("金额不正确", amount.contains(this.transferamount));

	        cpPayDriver.findElement(By.name("cardNo")).sendKeys(this.transfercard);
	        cpPayDriver.findElement(By.name("bpwd")).sendKeys(this.transferjiaoyipassword);
	        cpPayDriver.findElement(By.xpath("//table//a[1]")).click();

	        cpPayWait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@class='main']/span")).getText().equalsIgnoreCase("充值成功");
	            }
	        });

	        cpPayDriver.close();

	        this.driver = this.switchToWindow("惠金所");

	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.id("cztxTxt")).isDisplayed();
	            }
	        });

	        this.driver.findElement(By.xpath("//div[@id='cztxTxt']//div[@class='popBox']//a[1]")).click();
	        
//	        //等待提现
//	        this.wait.until(new ExpectedCondition<Boolean>() {
//	            @Override
//	            public Boolean apply(WebDriver webDriver) {
//	                return webDriver.findElement(By.xpath("//div[@class='tabtil']/ul/li[3]")).isDisplayed();
//	            }
//	        });
//	        
//	       //点击提现
//	        this.driver.findElement(By.xpath("//div[@class='tabtil']/ul/li[3]")).click();
//	        //金额验证
//	        this.wait.until(new ExpectedCondition<Boolean>() {
//	            @Override
//	            public Boolean apply(WebDriver webDriver) {
//	                return webDriver.findElement(By.xpath("//div[@class='pw_edit pw_edit2']/p[@class='account_tip']/em")).isDisplayed();
//	            }
//	        });
//	        
//	        String  Rechargeamount = this.driver.findElement(By.xpath("//div[@class='pw_edit pw_edit2']/p[@class='account_tip']/em")).getText();
//	        assertTrue("充值不成功", Rechargeamount.length() > 0);
	        
	        //等待资金记录
	        this.wait.until(new ExpectedCondition<Boolean>() {
	          @Override
	          public Boolean apply(WebDriver webDriver) {
	              return webDriver.findElement(By.xpath("//div[@class='tabtil']//ul/li[1]")).isDisplayed();
	          }
	      });
	      
	        this.selectSubMenu(1,"资金记录");
	        System.out.println("充值金额的显示为:" + this.driver.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td")).getText().trim());
	        String Rechargeamount = this.driver.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td")).getText().trim();
	        assertTrue("充值不成功", Rechargeamount.length() > 0);
	    }
	 
		@After
		public void tearDown() {
	        if (this.shouldExitOnTearDown) {
	           this.driver.quit();
	        }
		}

}
