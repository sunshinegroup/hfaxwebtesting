package com.hfax.selenium.regression.account.transfer;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.account.AccountBaseTest;
import com.hfax.selenium.rule.SuddenDeathRule;
public class 投资转让_可转让的项目查看  extends AccountBaseTest{

	/**
	 * @Description 投资转让_可转让的项目查看
	 * Created by songxq on 29/4/16.
	 */
	
	@Test
	public void 可转让的项目查看() {
		//投资转让->可转让的项目
		 this.selectMenu(14, "投资转让");
		 this.selectSubMenu(2, "可转让的项目");
		 
		 //加载可转让的项目
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='con_one_2']/div[1]/h2")));
		 assertTrue("用户没有【可转让的项目】", this.driver.findElement(By.xpath("//div[@id='con_one_2']/div[1]/h2")).isDisplayed());
		 
		 //验证第一个项目的转让
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='con_one_2']/div[1]/ul/li[5]/a[1]")));
		 WebElement transferbutton = this.driver.findElement(By.xpath("//div[@id='con_one_2']/div[1]/ul/li[5]/a[1]"));
		 assertTrue("用户【可转让的项目】中没有转让按钮", transferbutton.isDisplayed());
		 
		 //点击转让
		 transferbutton.click();
		 
		 //验证项目定价转让的窗口
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='attornOk']/div[2]/div")));
		assertTrue("用户【可转让的项目】中项目定价转让的窗口不存在", this.driver.findElement(By.xpath("//div[@id='attornOk']/div[2]/div")).isDisplayed());
		
		//关闭项目定价转让的窗口
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("关闭")));
		//点击关闭窗口
		this.driver.findElement(By.linkText("关闭")).click();
		
	}
	
	

}
