package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 我的投资_惠投资_投资产品信息 extends AccountBaseTest{

	/**
	 * @Description 我的投资_惠投资_投资产品信息  
	 * Created by songxq on 20/6/16.
	 */
	@Test
	public void 投资产品信息() {
		this.navigateToPage("新手专区");
		//点击惠投资
		WebElement content = this.clickOnSubmenu("惠投资");
		
	    WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
	    new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='display_6']/div/div[2]")));
	    //点击惠投资第一个产品
	    assertTrue("惠投资第一个产品不存在", this.driver.findElement(By.xpath("//div[@id='display_6']/div/div[2]")).isDisplayed());
	    firstItem.findElement(By.xpath("i/div/div[2]/a")).click();
	    
	    //等待金额输入
	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='amount']")));
	    
	    //项目金额
	    WebElement proAmount = this.driver.findElements(By.xpath("//strong[@class='object-money']")).get(0);
	    assertTrue("项目金额不对",proAmount.getText().trim().equals(this.huiProAmount));
	    
	    //预期年化收益率
	    WebElement yearRate = this.driver.findElement(By.xpath("//li[@class='obj-shouyi']/i/strong"));
	    assertTrue("预期年化收益率不对",yearRate.getText().trim().equals(this.huiYearRate));
	    
	    //投资期限
	    Pattern pattern = Pattern.compile("^3[0-9]{2}$");
	    WebElement day = this.driver.findElements(By.xpath("//strong[@class='object-money']")).get(1);
	    Matcher matcher = pattern.matcher(day.getText().trim());
	    boolean matchDay = matcher.matches();
	    assertTrue("投资期限不对",matchDay = true);
	    
	    //起投金额
	    WebElement amount = this.driver.findElements(By.xpath("//strong[@class='object-money']")).get(2);
	    assertTrue("起投金额不对",amount.getText().trim().equals(this.huiAmount));
	    
	    //输入投资金额
        assertTrue("投资金额输入框不存在", this.driver.findElement(By.xpath("//input[@id='amount']")).isDisplayed());
        WebElement input = this.driver.findElement(By.xpath("//input[@id='amount']"));
        input.clear();
        input.sendKeys(this.userremaininginvestamount);
        
        //勾选已阅读并同意《定向委托投资管理协议》
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='agre']")));
        assertTrue("《定向委托投资管理协议》不存在",this.driver.findElement(By.xpath("//input[@id='agre']")).isDisplayed());
        this.driver.findElement(By.xpath("//input[@id='agre']")).click();
        
        
        //预期收益
        WebElement expectRate = this.driver.findElement(By.xpath(".//*[@id='interests']"));
        //验证预期收益
//        assertTrue("预期收益金额不对",expectRate.getText().replace(" ","").equals(this.huiExpectRate));
   
        
        //点击去投资
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@id='btnsave']/i")));
        this.driver.findElement(By.xpath("//a[@id='btnsave']/i")).click();
        
        //验证实际支付金额（元）
        new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				String text = webDriver.findElement(By.xpath(".//*[@id='sjzfje']")).getText();
				return text.equalsIgnoreCase(userremaininginvestamount);
			}
		});
        
        //实际投资本金(元)
         WebElement actualInvestMoney =  this.driver.findElement(By.xpath(".//*[@id='sjtzbj']"));
        //验证实际投资本金(元)
         assertTrue("实际投资本金(元)不对",actualInvestMoney.getText().trim().equals(this.userremaininginvestamount));

         //总预期收益率(%)
         WebElement allExpectRate = this.driver.findElement(By.xpath(".//*[@id='yqsyl']"));
        //验证总预期收益率(%)
         assertTrue("验证总预期收益率(%)不对",allExpectRate.getText().trim().equals(this.huiYearRate));

         
         //总预期收益(元)
         WebElement allExpectMoney = this.driver.findElement(By.xpath(".//*[@id='yqsy']"));
         //验证总预期收益(元)
//         assertTrue("验证总预期收益(元)不对",allExpectMoney.getText().trim().equals(this.huiExpectRate));
	}

}
