package com.hfax.selenium.regression.account.my_investment;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 我的投资_惠聚财_投资惠聚财的产品全投  extends AccountBaseTest{

	/**
	 *  
	 * @Description 我的投资_惠聚财_投资惠聚财的产品全投
	 * Created by songxq on 18/8/16.
	 */
	@Test
	public void 我的投资_惠聚财_投资惠聚财的产品全投(){
		this.navigateToPage("定期理财");
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
        WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
        
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")));
        //等待惠聚财第一个产品
        assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")).isDisplayed());
        
        //验证预期年化利率
        System.out.println("【惠聚财投资页】预期年化利率为: " + this.driver.findElement(By.xpath("//p[@class='benefit-sider-percent']")).getText().trim());
        assertTrue("【惠聚财投资页】预期年化利率不正确",this.driver.findElement(By.xpath("//p[@class='benefit-sider-percent']")).getText().trim().equals("700.00%"));
        
        //验证投资期限
        List<WebElement> list = this.driver.findElements(By.xpath("//p[@class='benefit-sider-date']"));
        System.out.println("【惠聚财投资页】投资期限为：" + list.get(0).getText().trim());
        assertTrue("【惠聚财投资页】投资期限不正确:",list.get(0).getText().trim().equals("333 天"));
        
        
        //验证起投金额
        System.out.println("【惠聚财投资页】起投金额为: "+ list.get(1).getText().trim());
        assertTrue("【惠聚财投资页】起投金额不正确",list.get(1).getText().trim().equals("1百元"));
        
        //验证还款方式
        System.out.println("【惠聚财投资页】还款方式为:"+ list.get(2).getText().trim());
        assertTrue("【惠聚财投资页】还款方式不正确",list.get(2).getText().trim().equals("一次性还款"));
        
        //验证项目总金额
        System.out.println("【惠聚财投资页】项目总金额为:"+ this.driver.findElement(By.xpath("//p[@class='total-mon listAmount']/i")).getText().trim());
        assertTrue("【惠聚财投资页】项目总金额不正确",this.driver.findElement(By.xpath("//p[@class='total-mon listAmount']/i")).getText().trim().equals("1万元"));
        
        firstItem.findElement(By.xpath("div[2]/div[2]/a")).click();
        
        //等待输入金额
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='amount']")));
        
        //验证项目金额
        List<WebElement> listSec = this.driver.findElements(By.xpath("//strong[@class='object-money']"));
        System.out.println("【惠聚财投资详细页】项目金额为:" + listSec.get(0).getText().trim());
        assertTrue("【惠聚财投资详细页】项目金额不正确: ",listSec.get(0).getText().trim().equals("10,000.00"));
       
        //验证预期年化收益率
        System.out.println("【惠聚财投资详细页】预期年化收益率为:" + this.driver.findElement(By.xpath("//li[@class='obj-shouyi interest']/i/strong")).getText().trim());
        assertTrue("【惠聚财投资详细页】预期年化收益率不正确",this.driver.findElement(By.xpath("//li[@class='obj-shouyi interest']/i/strong")).getText().trim().equals("700.00%"));
        
        //验证投资期限
        System.out.println("【惠聚财投资详细页】投资期限为:" + this.driver.findElement(By.xpath("//li[@class='obj-money']/i/strong")).getText().trim());
        assertTrue("【惠聚财投资详细页】投资期限不正确",this.driver.findElement(By.xpath("//li[@class='obj-money']/i/strong")).getText().trim().equals("333"));
        
        //验证起投金额
        System.out.println("【惠聚财投资详细页】起投金额为:" + listSec.get(1).getText().trim());
        assertTrue("【惠聚财投资详细页】起投金额 不正确",listSec.get(1).getText().trim().equals("100.00"));
        
        //验证收益方式
        System.out.println("【惠聚财投资详细页】收益方式为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[2]/em")).getText().trim());
        assertTrue("【惠聚财投资详细页】收益方式不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[2]/em")).getText().trim().equals("一次性还本付息"));
        
        //验证产品风险等级
        System.out.println("【惠聚财投资详细页】产品风险等级为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[3]/em")).getText().trim());
        assertTrue("【惠聚财投资详细页】产品风险等级不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[3]/em")).getText().trim().equals("低风险"));
        
        //验证投资人承受条件
        System.out.println("【惠聚财投资详细页】投资人承受条件为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[4]/em")).getText().trim());
        assertTrue("【惠聚财投资详细页】投资人承受条件不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[4]/em")).getText().trim().equals("保守型"));
       
        //验证递增金额
        System.out.println("【惠聚财投资详细页】递增金额为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[7]/em")).getText().trim());
        assertTrue("【惠聚财投资详细页】递增金额不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[7]/em")).getText().trim().equals("100元"));
        
       //验证项目概况
        assertTrue("【惠聚财投资详细页】项目概况不存在",this.driver.findElement(By.id("one1")).getText().trim().equals("项目概况"));
        this.driver.findElement(By.id("one1")).click();
        
        //验证网上开户协议
        assertTrue("【惠聚财投资详细页】网上开户协议不存在",this.driver.findElement(By.id("one2")).getText().trim().equals("网上开户协议"));
        this.driver.findElement(By.id("one2")).click();
        
        //验证产品说明书
        assertTrue("【惠聚财投资详细页】产品说明书不存在",this.driver.findElement(By.id("one3")).getText().trim().equals("产品说明书"));
        this.driver.findElement(By.id("one3")).click();
        
         //验证风险揭示书
        assertTrue("【惠聚财投资详细页】风险揭示书不存在",this.driver.findElement(By.id("one4")).getText().trim().equals("风险揭示书"));
        this.driver.findElement(By.id("one4")).click();
        
        //验证产品认购协议
        assertTrue("【惠聚财投资详细页】产品认购协议不存在",this.driver.findElement(By.id("one5")).getText().trim().equals("产品认购协议"));
        this.driver.findElement(By.id("one5")).click();
        
        //验证投资金额下面的网上开户协议
        assertTrue("【惠聚财投资详细页】投资金额下的网上开户协议不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc2']")).getText().trim().equals("《网上开户协议》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc2']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
        
        //验证投资金额下面的产品说明书
        assertTrue("【惠聚财投资详细页】投资金额下的产品说明书不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc3']")).getText().trim().equals("《产品说明书》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc3']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
        
        //验证投资金额下面的风险揭示书
        assertTrue("【惠聚财投资详细页】投资金额下的风险揭示书不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc4']")).getText().trim().equals("《风险揭示书》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc4']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
        
        //验证投资金额下面的产品认购协议
        assertTrue("【惠聚财投资详细页】投资金额下的产品认购协议不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc1']")).getText().trim().equals("《产品认购协议》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc1']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
        
        
        //输入投资金额
        this.driver.findElement(By.xpath(".//*[@id='amount']")).clear();
        this.driver.findElement(By.xpath(".//*[@id='amount']")).sendKeys("10000");
       
        
        //验证投资金额下的预期收益
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='interests']")));
        assertTrue("【惠聚财投资详细页】投资金额下预期收益不正确",this.driver.findElement(By.xpath(".//*[@id='interests']")).getText().trim().equals("64,750.00"));
        
        
        //点击已同意
        this.driver.findElement(By.xpath(".//*[@id='agre']")).click();
        
        //点击去投资
        this.driver.findElement(By.linkText("去投资")).click();
        
        //等待实际投资本金(元)
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText();
                return text.trim().equalsIgnoreCase("10,000.00");
            }
        });
        //验证实际投资本金(元)
        System.out.println("【惠聚财确认页】实际投资本金(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim());
        assertTrue("【惠聚财确认页】实际投资本金(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim().equals("10,000.00"));
        
        //验证总预期收益率(%)
        System.out.println("【惠聚财确认页】总预期收益率(%)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim() + "%");
        assertTrue("【惠聚财确认页】总预期收益率(%)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim().equals("700.00"));
        
        //验证总预期收益(元)
        System.out.println("【惠聚财确认页】总预期收益(元)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim());
        assertTrue("【惠聚财确认页】总预期收益(元)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim().equals("64,750.00"));
        
        //验证实际支付金额(元)
        System.out.println("【惠聚财确认页】实际支付金额(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim());
        assertTrue("【惠聚财确认页】实际支付金额(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim().equals("10,000.00"));
        
        
        //输入交易密码
        this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).clear();
        this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).sendKeys(this.jiaoyipassword);
        
        
        //点击确定投资
        this.driver.findElement(By.xpath(".//*[@id='besureInvest']")).click();
        
        
        //等待确认框
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[1]")).getText();
                return text.trim().equalsIgnoreCase("浏览更多项目");
            }
        });
        
//        //验证确认框本次投资金额
//        System.out.println("【惠聚财确认框】本次投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim());
//        assertTrue("【惠聚财确认框】本次投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim().equals("10,000.00"));
//        
//        //验证本项目累计投资金额
//        System.out.println("【惠聚财确认框】本项目累计投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim());
//        assertTrue("【惠聚财确认框】本项目累计投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim().equals("10,000.00"));
        
        //验证查看申请是否存在
        assertTrue("【惠聚财确认框】查看申请不存在:",this.driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[2]")).isDisplayed());
        //点击查看申请
        this.driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[2]")).click();
	}
}
