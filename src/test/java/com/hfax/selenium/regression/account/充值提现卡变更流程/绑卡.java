package com.hfax.selenium.regression.account.充值提现卡变更流程;

import static org.junit.Assert.assertEquals;

import com.hfax.selenium.base.SuddenDeathBaseTest;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;

/**
*
* 绑卡
* Created by philip on 3/10/16.
* refacted by wangmingqiang 4/29/16 .
*/
public class 绑卡 extends SuddenDeathBaseTest{
	   @Override
	    public void setup() throws Exception {
	        super.setup();
	        this.loadPage();
	        this.login();
	    }
	
	@Test
   public void 绑卡功能(){
   	tieCard(this.bankname,this.bankid,this.card,this.phone,this.name,this.password);    
   	}
  
	protected void supersetup() throws Exception{
		super.setup();
	}
   
   /**
   *
   * 绑卡功能类
   * 接收参数
   * 1.绑卡银行名称
   * 2.银行ID
   * 3.银行卡号
   * 4.预留电话
   * 5.用户真实姓名
   * 6.交易密码
   * 
   */
   
   
   protected void tieCard(String bankname,String bankid,String cardno,String phoneno,String username,String password) {

       this.driver.findElement(By.id("li_2")).click();

       this.wait.until(new ExpectedCondition<Boolean>() {
           @Override
           public Boolean apply(WebDriver webDriver) {
               String text = webDriver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[2]")).getText();
               return text.equalsIgnoreCase("充值");
           }
       });
       System.out.println("-----------我的账户——>充值提现-------------");
       this.driver.findElement(By.xpath("html/body//div[@class='tabtil']/ul/li[2]")).click();
       this.wait.until(new ExpectedCondition<Boolean>() {
           @Override
           public Boolean apply(WebDriver webDriver) {
               return webDriver.findElement(By.id("bankName1")).isDisplayed();
           }
       });
       System.out.println("-----------我的账户——>充值提现——>充值-------------");

       Select select = new Select(this.driver.findElement(By.id("bankName1")));
       select.selectByValue(bankid);
       this.driver.findElement(By.id("bankCard1")).sendKeys(cardno);
       this.driver.findElement(By.id("bankCellPhone_")).sendKeys(phoneno);
       this.driver.findElement(By.id("jiaoyimima_")).sendKeys(password);
       this.driver.findElement(By.id("addbank")).click();

       this.wait.until(new ExpectedCondition<Boolean>() {
           @Override
           public Boolean apply(WebDriver webDriver) {
               return webDriver.findElement(By.className("popBox")).isDisplayed();
           }
       });
       System.out.println("-----------我的账户——>充值提现——>充值——>选择卡信息-------------");
       this.driver.findElement(By.id("checkSure")).click();

       this.wait.until(new ExpectedCondition<Boolean>() {
           @Override
           public Boolean apply(WebDriver webDriver) {
               return webDriver.findElement(By.id("checkSuccess")).isDisplayed();
           }
       });
       System.out.println("-----------我的账户——>充值提现——>充值——>绑卡-------------");
       //增加绑卡信息校验       
       this.driver.findElement(By.id("checkSuccess")).click();
       this.wait.until(new ExpectedCondition<Boolean>() {
           @Override
           public Boolean apply(WebDriver webDriver) {
               return webDriver.findElement(By.xpath(".//*[@id='bankInfo']/div/table")).isDisplayed();
           }
       });
       
       String showname = this.driver.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td[1]")).getText();
       String showbankname = this.driver.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td[2]")).getText();
       String showcardno = this.driver.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td[3]")).getText();
       StringBuffer showcardno2 = new StringBuffer(showcardno.substring(0,4)).append(showcardno.substring(showcardno.length()-4, showcardno.length()));
       StringBuffer realcardno = new StringBuffer(cardno.substring(0,4)).append(cardno.substring(cardno.length()-4, cardno.length()));
       String showcardstate = this.driver.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td[4]")).getText();
       System.out.println("-----------我的账户——>充值提现——>充值——>卡信息校验-------------");
       assertEquals("绑卡信息_用户姓名不正确",username,showname);
       assertEquals("绑卡信息_银行名称不正确",bankname,showbankname);
       assertEquals("绑卡信息_卡号信息不正确",realcardno.toString(),showcardno2.toString());
       assertEquals("绑卡信息_绑卡状态不正确","已签约已验证",showcardstate);

   }

}
