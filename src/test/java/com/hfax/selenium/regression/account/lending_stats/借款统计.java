package com.hfax.selenium.regression.account.lending_stats;

import static org.junit.Assert.assertTrue;

import com.hfax.selenium.regression.account.AccountBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * 
 * @author changwj
 * @Description 我的账户--企业借款统计
 */
public class 借款统计 extends AccountBaseTest {
    private void verifyTableHeaders(String[] expectedHeaders, List<WebElement>headers) {
        assertTrue("抬头列数不对", expectedHeaders.length == headers.size());

        for (int i = 0; i < expectedHeaders.length; i ++) {
            assertTrue("抬头列 " + i + " 文字不正确", expectedHeaders[i].equalsIgnoreCase(headers.get(i).getText()));
        }
    }

    private void verifyTableCellValueIsNumeric(List<WebElement>cells) {
        for (WebElement cell : cells) {
            String numberString = cell.getText().replace("￥", "").trim();
            Double numberInDouble = new Double(numberString);
            assertTrue("数据不是数字 " + numberString + " | " + numberInDouble, numberInDouble >= 0.0f);
        }
    }

//	@Test
	public void 借款统计() {
		this.selectMenu(9, "企业借款统计");

        List<WebElement> tables = this.driver.findElements(By.tagName("table"));
        assertTrue("还款统计抬头不正确", tables.get(0).findElement(By.xpath("tbody/tr[1]/th")).getText().equalsIgnoreCase("还款统计"));

        List<WebElement> tds = tables.get(0).findElements(By.xpath("tbody/tr[2]/td"));
        this.verifyTableHeaders(new String[] {"成功借款总额", "已还本息", "成功借款数", "待还本息", "正常还清笔数", "未还清笔数"}, tds);
        this.verifyTableCellValueIsNumeric(tables.get(0).findElements(By.xpath("tbody/tr[3]/td")));

        assertTrue("还款统计抬头不正确", tables.get(1).findElement(By.xpath("tbody/tr[1]/th")).getText().equalsIgnoreCase("逾期统计"));

        tds = tables.get(1).findElements(By.xpath("tbody/tr[2]/td"));
        this.verifyTableHeaders(new String[] {"逾期本息", "逾期次数", "逾期罚款", "严重逾期次数"}, tds);
        this.verifyTableCellValueIsNumeric(tables.get(1).findElements(By.xpath("tbody/tr[3]/td")));
    }

}
