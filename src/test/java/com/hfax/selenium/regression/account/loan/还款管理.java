package com.hfax.selenium.regression.account.loan;

import static org.junit.Assert.assertTrue;

import com.hfax.selenium.regression.account.AccountBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author changwj
 * @Description 企业还款管理
 */
public class 还款管理 extends AccountBaseTest {
    @Override
    public void setup() throws Exception {
        super.setup();
        this.selectMenu(7, "企业还款管理");
    }

    private void verifyTableColumns(String[] expectedTitles) {
        List<WebElement> headers = this.driver.findElements(By.xpath("//div[@class='biaoge']//table//th"));
        List<WebElement> cleanedHeaders = new ArrayList<WebElement>();

        for (int i = 0; i < headers.size(); i ++) {
            if (headers.get(i).isDisplayed()) {
                cleanedHeaders.add(headers.get(i));
            }
        }

        assertTrue("表列数不对", cleanedHeaders.size() == expectedTitles.length);
        for (int i = 0; i < cleanedHeaders.size(); i ++) {
            String expectedTitle = expectedTitles[i];
            String headerString = cleanedHeaders.get(i).getText();
            assertTrue("表列 "+i+" 抬头不对: " + expectedTitle + " / " + headerString, expectedTitle.equalsIgnoreCase(headerString));
        }
    }

	// 成功借款
//	@Test
	public void 成功借款暂无记录() {
        this.selectSubMenu(1, "成功借款");

        this.setCalendarFromDate("2015-12-12");
        this.setCalendarToDate("2015-12-13");
        this.driver.findElement(By.xpath("//form[@id='searchForm']//a[@id='btn_search']")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElements(By.xpath("//div[@class='biaoge']//table/tbody/tr")).size() == 2;
            }
        });

        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td"), "暂无记录"));

        String[] expectedTitles = {"标题", "借款金额", "年利率", "还款期限", "借款时间", "应还本息", "已还本息", "未还本息", "状态"};
        this.verifyTableColumns(expectedTitles);
	}

	// 正在还款的借款
//	@Test
	public void 正在还款的借款暂无记录() {
        this.selectSubMenu(2, "正在还款的借款");

        this.setCalendarFromDate("2015-12-12");
        this.setCalendarToDate("2015-12-13");
        this.driver.findElement(By.id("btn_search")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElements(By.xpath("//div[@class='biaoge']//table/tbody/tr")).size() == 2;
            }
        });

        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td"), "暂无记录"));

        String[] expectedTitles = {"标题", "借款金额", "年利率", "还款期限", "还款日期", "应还本息", "已还本息", "待还本息", "操作"};
        this.verifyTableColumns(expectedTitles);
	}

	// 已还完的借款
//	@Test
	public void 已还完的借款暂无记录() {
        this.selectSubMenu(3, "已还完的借款");

        this.setCalendarFromDate("2015-12-12");
        this.setCalendarToDate("2015-12-13");
        this.driver.findElement(By.id("btn_search")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElements(By.xpath("//div[@class='biaoge']//table/tbody/tr")).size() == 2;
            }
        });

        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td"), "暂无记录"));
        String[] expectedTitles = {"标题", "借款金额", "年利率", "还款期限", "借款时间", "应还本息", "已还本息", "已还逾期罚息", "操作"};
        this.verifyTableColumns(expectedTitles);
	}

	// 还款明细账
//	@Test
	public void 还款明细账暂无记录() {
        this.selectSubMenu(4, "还款明细账");

        this.setCalendarFromDate("2015-12-12");
        this.setCalendarToDate("2015-12-13");
        this.driver.findElement(By.id("btn_search")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElements(By.xpath("//div[@class='biaoge']//table/tbody/tr")).size() == 2;
            }
        });

        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td"), "暂无记录"));
        String[] expectedTitles = {"标题", "第几期", "还款日期", "实际还款日期", "本期应还本息", "利息", "逾期罚款", "逾期天数", "还款状态"};
        this.verifyTableColumns(expectedTitles);
	}
}
