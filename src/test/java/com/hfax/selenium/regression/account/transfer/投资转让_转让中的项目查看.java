package com.hfax.selenium.regression.account.transfer;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.account.AccountBaseTest;
import com.hfax.selenium.rule.SuddenDeathRule;

public class 投资转让_转让中的项目查看 extends AccountBaseTest{
	/**
	 * @Description 投资转让_转让中的项目
	 * Created by songxq on 29/4/16.
	 */

	
   //转让的项目
	@Test
	public void 转让中的项目() {
		//投资转让->转让中的项目
		 this.selectMenu(14, "投资转让");
		 this.selectSubMenu(1, "转让中的项目");
		 
		 //加载转让中的项目
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='attornIng ding']/h2")));
		 assertTrue("用户没有【转让中的项目】", this.driver.findElement(By.xpath("//div[@class='attornIng ding']/h2")).isDisplayed());
		 
		 //展开第一个可以转让的项目
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='hover']")));
		 this.driver.findElement(By.xpath("//span[@class='hover']")).click();
		 
		 //验证展开后的信息
		  WebElement element = this.driver.findElement(By.xpath("//div[@class='attornIng ding hover']/ul[2]"));
		  assertTrue("用户【转让中的项目】没有详细信息",element.isDisplayed());
		 
		  //取消转让
		  WebElement canceltransfer = this.driver.findElement(By.xpath("//div[@id='con_one_1']/div[1]/h2/a"));
		  assertTrue("用户【转让中的项目】不存在取消转让",canceltransfer.isDisplayed()); 
		
		  //点击取消转让
		  canceltransfer.click();
		  
		  //弹出确认取消转让的pop窗口
		  this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='PopForm']")));
		  assertTrue("用户【转让中的项目】确认取消窗口不存在",this.driver.findElement(By.xpath("//div[@class='PopForm']")).isDisplayed());
		  
		  //关闭取消转让的窗口
		  this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("关闭")));
		  this.driver.findElement(By.linkText("关闭")).click();
	}


}
