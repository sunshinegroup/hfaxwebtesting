package com.hfax.selenium.regression.account.settings;

import static org.junit.Assert.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.regression.account.AccountBaseTest;

/**
 * @Description 用户充值前银行卡的设置
 * Created by songxq on 26/8/16.
 * 
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 充值前银行卡的设置 extends AccountBaseTest {


	  @Test
	  public void test001_验证状态和操作中的提交和取消(){
	    	//点击用户设置
	    	用户设置();
	    	
	    	//点击银行卡设置
			this.selectSubMenu(6,"银行卡设置");
	    	
	    	//验证状态
	    	assertTrue("用户银行卡设置中状态不正确",this.getElementText(".//*[@id='bankInfo']/div/table/tbody/tr[2]/td[4]").equals("已签约已验证"));
	    	
	    	//验证操作
	    	assertTrue("操作显示不正确",this.getElementText(".//*[@id='bankInfo']/div/table/tbody/tr[2]/td[5]/a").equals("变更"));
	    	
	    	//点击变更
		 	this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	webDriver.findElement(By.linkText("变更")).click();
	            	String text;
	            	try{
	            		text = webDriver.findElement(By.xpath(".//*[@id='changeBankDiv']/div/div[1]/form/ul/li[1]/label")).getText().trim();
	            		
	            	}catch(Exception e){
	            		text = webDriver.findElement(By.linkText("变更")).getText().trim();
	            	}
	            	return text.equalsIgnoreCase("银行户名：");
	            	}
	        });
	    
	    	
	    	//等待页面加载
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return webDriver.findElement(By.id("changeOldBankCard")).isDisplayed();
	             }
	         });
	    	 
	    	 //验证银行户名
	    	 assertTrue("银行户名显示不正确",this.driver.findElement(By.id("changeOldBankName")).getText().trim().equals(this.name));
	    	 
	    	 //验证银行卡号
	    	 Pattern pattern = Pattern.compile("^[0-9]{4}\\s{1}\\*{4}\\s{1}\\*{4}\\s{1}[0-9]{4}$");
	 	     WebElement bankCard = this.driver.findElement(By.xpath(".//*[@id='changeOldBankCard']"));
	 	     Matcher matcher = pattern.matcher(bankCard.getText().trim());
	 	     boolean blnBankCard = matcher.matches();
	 	     assertTrue("银行卡号不正确",blnBankCard = true);
	 	     
	 	     //点击提交
	 	     this.driver.findElement(By.linkText("提交")).click();
	 	     
	 	     //验证提交后的提示信息
	 	     assertTrue("提交后的提示信息不正确",this.getElementText(".//*[@id='bankUT1error_b']").equals("您输入的银行卡号格式有误，请核对后重新提交"));
	 	     
	 	     //点击取消
	 	     this.driver.findElement(By.linkText("取消")).click();
	    	 
	 	     //等待页面加载
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return webDriver.findElement(By.linkText("变更")).isDisplayed();
	             }
	         });
	    	 assertTrue("点击取消后跳转的页面不正确",this.driver.findElement(By.linkText("变更")).isDisplayed());
	    	
	    	}
	
	    @Test
	    public void test002_验证发送验证码和认证支付服务协议() throws InterruptedException{
	    	//点击用户设置
	    	用户设置();
	    	
	    	//点击银行卡设置
			this.selectSubMenu(6,"银行卡设置");
	    	
	        
			//点击变更
		 	this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	webDriver.findElement(By.linkText("变更")).click();
	            	String text;
	            	try{
	            		text = webDriver.findElement(By.xpath(".//*[@id='changeBankDiv']/div/div[1]/form/ul/li[1]/label")).getText().trim();
	            		
	            	}catch(Exception e){
	            		text = webDriver.findElement(By.linkText("变更")).getText().trim();
	            	}
	            	return text.equalsIgnoreCase("银行户名：");
	            	}
	        });
	    
	    	
	   	 	//点击发送验证码
	   	 	this.driver.findElement(By.xpath(".//*[@id='sendAccountCode_b']")).click();
	   	 	
	   	 	
	   	 	this.wait.until(new ExpectedCondition<Boolean>() {
	   	 		@Override
	   	 		public Boolean apply(WebDriver webDriver) {
	   	 			return webDriver.findElement(By.xpath(".//*[@id='phoneNumberUT1error_b']")).isDisplayed();
	   	 		}
	   	 	});
	   	 	
	   	 	//验证发送验证码后的提示信息
	   	 	assertTrue("新卡号后面的提示信息不正确",this.getElementText(".//*[@id='phoneNumberUT1error_b']").equals("您输入的手机号格式有误，请核对后重新提交"));
	   	 	
	   	 	//点击认证支付协议
	   	 	this.driver.findElement(By.xpath(".//*[@id='changeBankDiv']/div/div[1]/form/ul/li[6]/p[2]/a")).click();
	   	 	

	    }
	    @Test 
	    public void test003_验证新卡号和手机号(){
	    	//点击用户设置
	    	用户设置();
	    	
	    	//点击银行卡设置
			this.selectSubMenu(6,"银行卡设置");
	    	
	        
			//点击变更
		 	this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	webDriver.findElement(By.linkText("变更")).click();
	            	String text;
	            	try{
	            		text = webDriver.findElement(By.xpath(".//*[@id='changeBankDiv']/div/div[1]/form/ul/li[1]/label")).getText().trim();
	            		
	            	}catch(Exception e){
	            		text = webDriver.findElement(By.linkText("变更")).getText().trim();
	            	}
	            	return text.equalsIgnoreCase("银行户名：");
	            	}
	        });
		 	//输入位数少一位的新卡号
		 	this.driver.findElement(By.xpath(".//*[@id='bankCardIdUT1_b']")).clear();
	        System.out.println("输出少一位的卡号为：" + this.card.substring(0,this.card.length() - 1));
		 	this.driver.findElement(By.xpath(".//*[@id='bankCardIdUT1_b']")).sendKeys(this.card.substring(0,this.card.length() - 1));
		 	
		 	//输入手机号
		 	this.driver.findElement(By.xpath(".//*[@id='phoneNumberUT1_b']")).clear();
		 	this.driver.findElement(By.xpath(".//*[@id='phoneNumberUT1_b']")).sendKeys(this.phone);
		 	
		 	//点击发送验证码
		 	this.driver.findElement(By.xpath(".//*[@id='sendAccountCode_b']")).click();
		 	
		 	//输入验证码
		 	this.driver.findElement(By.xpath(".//*[@id='accountCode_b']")).clear();
		 	this.driver.findElement(By.xpath(".//*[@id='accountCode_b']")).sendKeys("1234");
		 	
		 	//点击提交
		 	this.driver.findElement(By.linkText("提交")).click();
		 	
		 	
			//等待页面加载
		 	this.wait.until(new ExpectedCondition<Boolean>() {
		           @Override
		           public Boolean apply(WebDriver webDriver) {
		               String text = webDriver.findElement(By.xpath(".//*[@id='bankUT1error_b']")).getText().trim();
		               return text.equalsIgnoreCase("您输入的卡号暂不支持绑定，请更换其它储蓄卡");
		           }
		       });
		 	//验证新卡号后面的提示信息
		    assertTrue("新卡号后面的提示信息不正确",this.getElementText(".//*[@id='bankUT1error_b']").equals("您输入的卡号暂不支持绑定，请更换其它储蓄卡"));
		    
		    //输入新卡号
		 	this.driver.findElement(By.xpath(".//*[@id='bankCardIdUT1_b']")).clear();
	        System.out.println("输出新的卡号为：" + this.card.substring(0,this.card.length() - 2) + "00");
		 	this.driver.findElement(By.xpath(".//*[@id='bankCardIdUT1_b']")).sendKeys(this.card.substring(0,this.card.length() - 2) + "00");
		 	
		 	//输入少一位的手机号
		 	this.driver.findElement(By.xpath(".//*[@id='phoneNumberUT1_b']")).clear();
		 	System.out.println("输出少一位的手机号为: " + this.phone.substring(0,this.phone.length() - 1));
		 	this.driver.findElement(By.xpath(".//*[@id='phoneNumberUT1_b']")).sendKeys(this.phone.substring(0,this.phone.length() - 1));
		 	
		 	//点击提交
		 	this.driver.findElement(By.linkText("提交")).click();
		 	
		 	//等待页面加载
		 	this.wait.until(new ExpectedCondition<Boolean>() {
		           @Override
		           public Boolean apply(WebDriver webDriver) {
		               String text = webDriver.findElement(By.xpath(".//*[@id='phoneNumberUT1error_b']")).getText().trim();
		               return text.equalsIgnoreCase("您输入的手机号格式有误，请核对后重新提交");
		           }
		       });
		 	//验证手机号后面的提示信息
		    assertTrue("手机号后面的提示信息不正确",this.getElementText(".//*[@id='phoneNumberUT1error_b']").equals("您输入的手机号格式有误，请核对后重新提交"));
		 	 
		    }
	    
	    @Test 
	    public void test004_正确修改卡号(){
	    	//点击用户设置
	    	用户设置();
	    	
	    	//点击银行卡设置
			this.selectSubMenu(6,"银行卡设置");
	    	
	        
			//点击变更
		 	this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	webDriver.findElement(By.linkText("变更")).click();
	            	String text;
	            	try{
	            		text = webDriver.findElement(By.xpath(".//*[@id='changeBankDiv']/div/div[1]/form/ul/li[1]/label")).getText().trim();
	            		
	            	}catch(Exception e){
	            		text = webDriver.findElement(By.linkText("变更")).getText().trim();
	            	}
	            	return text.equalsIgnoreCase("银行户名：");
	            	}
	        });
		 	
		 	 //输入新卡号
		 	this.driver.findElement(By.xpath(".//*[@id='bankCardIdUT1_b']")).clear();
	        System.out.println("输出新的卡号为：" + this.card.substring(0,this.card.length() - 2) + "00");
		 	this.driver.findElement(By.xpath(".//*[@id='bankCardIdUT1_b']")).sendKeys(this.card.substring(0,this.card.length() - 2) + "00");
		 	
		 	//输入手机号
		 	this.driver.findElement(By.xpath(".//*[@id='phoneNumberUT1_b']")).clear();
		 	this.driver.findElement(By.xpath(".//*[@id='phoneNumberUT1_b']")).sendKeys(this.phone);
		 	
		 	//点击发送验证码
		 	this.driver.findElement(By.xpath(".//*[@id='sendAccountCode_b']")).click();
		 	
		 	//输入验证码
		 	this.driver.findElement(By.xpath(".//*[@id='accountCode_b']")).clear();
		 	this.driver.findElement(By.xpath(".//*[@id='accountCode_b']")).sendKeys("1234");
		 	
		 	//点击提交
		 	this.driver.findElement(By.linkText("提交")).click();
		 	
		 	
		 	//等待页面加载
		 	this.wait.until(new ExpectedCondition<Boolean>() {
		           @Override
		           public Boolean apply(WebDriver webDriver) {
		               String text = webDriver.findElement(By.xpath(".//*[@id='unknowError']/div[2]/div/a")).getText().trim();
		               return text.equalsIgnoreCase("我知道了");
		           }
		       });
		 	 if(driver.getPageSource().contains("我知道了"))
			 {
				 assertTrue("修改卡号后提示信息不正确",this.getElementText(".//*[@id='unknowErrorMessage']").equals("银行卡无效，请联系发卡行"));
				 //点击我知道了
				 this.driver.findElement(By.xpath(".//*[@id='unknowError']/div[2]/div/a")).click();
			 }
			 else{
				 
				 fail("修改卡号失败！");
			 }
			 
		 	
	    	
	    }

}
