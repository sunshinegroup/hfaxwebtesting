package com.hfax.selenium.regression.account.finance;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.hfax.selenium.base.Util;
import com.hfax.selenium.regression.account.AccountBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.internal.Streams;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author changwj
 * @Description 我的账户--资金记录
 */
public class 资金记录 extends AccountBaseTest {
    @Override
    public void setup() throws Exception {
        super.setup();
        this.充值提现();
    }

    /**
     * @Description 资金记录 资金记录
     */
    @Test
    public void 充值提现查询无结果() {
        // 资金记录
        this.setCalendarFromDate("2015-12-12");
        this.setCalendarToDate("2015-12-13");

        // 下拉菜单
        Select selectCity = new Select(this.driver.findElement(By.xpath(".//*[@id='momeyType']")));
        for (int i = 0; i < selectCity.getOptions().size(); i++) {
            selectCity.getOptions().get(2).click();
        }

        this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div[2]/div[1]/a")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElements(By.xpath("//div[@id='fundRecord']/table/tbody/tr")).size() == 2;
            }
        });

        String message = this.driver.findElement(By.xpath("//div[@id='fundRecord']/table/tbody/tr[2]/td")).getText();
        assertTrue("提示信息不正确", message.equalsIgnoreCase("暂无信息"));
    }

    @Test
    public void 充值提现查询有结果() {
        // 资金记录
        this.setCalendarFromDate("2015-12-12");
        this.setCalendarToDate(Util.assignTime(+1, "yyyy-MM-dd"));

        // 下拉菜单
        Select selectCity = new Select(this.driver.findElement(By.xpath(".//*[@id='momeyType']")));
        for (int i = 0; i < selectCity.getOptions().size(); i++) {
            selectCity.getOptions().get(2).click();
        }

        this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div[2]/div[1]/a")).click();

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//div[@id='fundRecord']/table/tbody/tr[2]/td[5]")).getText().length() > 0;
            }
        });

        assertTrue("资金记录不正确", this.driver.findElement(By.xpath("//div[@id='fundRecord']/table/tbody/tr[2]/td[5]")).getText().equalsIgnoreCase(Util.getDecimalFormat(this.amount)));
    }
}
