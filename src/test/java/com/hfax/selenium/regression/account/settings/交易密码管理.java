package com.hfax.selenium.regression.account.settings;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.account.AccountBaseTest;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 交易密码管理 extends AccountBaseTest{

	/**
     * @Description 交易密码管理  
     * Created by songxq on 24/8/16.
     * 
     */

	@Test
	public void test001_验证交易密码提示信息() {
		//点击用户设置
		 用户设置();
				
		//点击用户详情信息
		this.selectSubMenu(3,"交易密码管理");
		
		//验证文本提示信息
		this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                String text = webDriver.findElement(By.xpath("//div[@class='biaoge2']//div[@class='pw_edit pw_edit2']/p")).getText().trim();
	                return text.equalsIgnoreCase("重置您的交易密码");
	            }
	        });
		
		//点击确定
		this.driver.findElement(By.id("btn_submit")).click();
		
		//验证短信验证码
		assertTrue("短信验证码后的提示不正确",this.getElementText(".//*[@id='code_voice_red']").equals("请填写短信验证码"));
		
		//验证请填写本人身份证号码
		System.out.println("身份证后的提示信息为：" + this.getElementText(".//*[@id='identity_red']"));
		assertTrue("身份证后的提示不正确",this.getElementText(".//*[@id='identity_red']").equals("请填写本人身份证号码"));
		
//		//验证原交易密码后的提示信息
//		assertTrue("原交易密码后的提示信息不正确",this.getElementText(".//*[@id='oldPwd_red']").equals("请填写交易密码"));
		
		//验证新交易密码后的提示信息
		assertTrue("新交易密码后的提示信息不正确",this.getElementText(".//*[@id='onePwd_red']").equals("请填写交易密码"));
		
		//验证确认新密码后的提示信息
		assertTrue("确认新交易密码后的提示信息不正确",this.getElementText(".//*[@id='twoPwd_red']").equals("请填写确认新交易密码"));
	}
	

	@Test 
	public void test002_验证输入数据后的提示信息(){
		//点击用户设置
		 用户设置();
				
		//点击用户详情信息
		this.selectSubMenu(3,"交易密码管理");
		
		//验证文本提示信息
		this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text = webDriver.findElement(By.xpath("//div[@class='biaoge2']//div[@class='pw_edit pw_edit2']/p")).getText().trim();
                return text.equalsIgnoreCase("重置您的交易密码");
            }
        });
		
		//点击发送验证码
		this.driver.findElement(By.xpath(".//*[@id='clickCode']")).click();
		
		//输入验证码
		this.driver.findElement(By.xpath(".//*[@id='code']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='code']")).sendKeys("1234");
		
		//输入身份证号
		this.driver.findElement(By.xpath(".//*[@id='identity']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='identity']")).sendKeys(this.identification);
		
		
		//点击确定
		this.driver.findElement(By.xpath(".//*[@id='btn_submit']")).click();
		
		
		//验证新交易密码后的提示信息
		assertTrue("新交易密码后的提示信息不正确",this.getElementText(".//*[@id='onePwd_red']").equals("请填写交易密码"));
				
//		//验证确认新密码后的提示信息
//		assertTrue("确认新交易密码后的提示信息不正确",this.getElementText(".//*[@id='twoPwd_red']").equals("请填写确认新交易密码"));
				
	}
	
	//边界值验证

	@Test
	public void test003_验证交易密码填写规则(){
		//点击用户设置
		 用户设置();
				
		//点击用户详情信息
		this.selectSubMenu(3,"交易密码管理");
		
		//验证文本提示信息
		this.wait.until(new ExpectedCondition<Boolean>() {
           @Override
           public Boolean apply(WebDriver webDriver) {
               String text = webDriver.findElement(By.xpath("//div[@class='biaoge2']//div[@class='pw_edit pw_edit2']/p")).getText().trim();
               return text.equalsIgnoreCase("重置您的交易密码");
           }
       });
		
		//点击发送验证码
		this.driver.findElement(By.xpath(".//*[@id='clickCode']")).click();
				
		//输入验证码
		this.driver.findElement(By.xpath(".//*[@id='code']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='code']")).sendKeys("1234");
				
		//输入身份证号
		this.driver.findElement(By.xpath(".//*[@id='identity']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='identity']")).sendKeys(this.identification);
				
		
		//输入新的交易密码，验证7个字符
		this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).clear();
	    this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).sendKeys("s123456");
	    
		//点击确定
		this.driver.findElement(By.id("btn_submit")).click();
		
		//验证新交易密码后的提示信息
		assertTrue("新交易密码后的提示信息不正确",this.getElementText(".//*[@id='onePwd_red']").equals("8—16位数字及大写、小写字母组合，允许特殊字符,.+-_@#!"));
				
		//点击发送验证码
		this.driver.findElement(By.xpath(".//*[@id='clickCode']")).click();
				
		//输入验证码
		this.driver.findElement(By.xpath(".//*[@id='code']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='code']")).sendKeys("1234");
				
		//输入身份证号
		this.driver.findElement(By.xpath(".//*[@id='identity']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='identity']")).sendKeys(this.identification);
				
		
		//输入新的交易密码，验证17个字符
		this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).clear();
	    this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).sendKeys("Hh123456789012345");
	    
	   //点击确定
	   this.driver.findElement(By.id("btn_submit")).click();
	   
	   //验证新交易密码后的提示信息
	   assertTrue("新交易密码后的提示信息不正确",this.getElementText(".//*[@id='onePwd_red']").equals("8—16位数字及大写、小写字母组合，允许特殊字符,.+-_@#!"));
	 				
		
	}

	@Test
	public void test004_验证两次输入的交易密码不一致(){
		//点击用户设置
		 用户设置();
				
		//点击用户详情信息
		this.selectSubMenu(3,"交易密码管理");
		
		//验证文本提示信息
		this.wait.until(new ExpectedCondition<Boolean>() {
		           @Override
		           public Boolean apply(WebDriver webDriver) {
		               String text = webDriver.findElement(By.xpath("//div[@class='biaoge2']//div[@class='pw_edit pw_edit2']/p")).getText().trim();
		               return text.equalsIgnoreCase("重置您的交易密码");
		           }
		       });
		//点击发送验证码
		this.driver.findElement(By.xpath(".//*[@id='clickCode']")).click();
						
		//输入验证码
		this.driver.findElement(By.xpath(".//*[@id='code']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='code']")).sendKeys("1234");
						
		//输入身份证号
		this.driver.findElement(By.xpath(".//*[@id='identity']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='identity']")).sendKeys(this.identification);
	
		//输入新的交易密码
		this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).sendKeys("Hh12345678");
		
		//再次输入新的交易密码
		this.driver.findElement(By.xpath(".//*[@id='ResetConfirmPass']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='ResetConfirmPass']")).sendKeys("Hh12345679");
		
		//点击确定
		this.driver.findElement(By.id("btn_submit")).click();
		
		//验证提示信息
		System.out.println("两次密码输入的不一致的提示信息为:" + this.getElementText(".//*[@id='twoPwd_red']"));
		assertTrue("确认新密码后的提示信息不正确",this.getElementText(".//*[@id='twoPwd_red']").equals("两次密码不一致"));
	 }
	
	@Test
	public void test005_正确修改交易密码(){
		//点击用户设置
		 用户设置();
				
		//点击用户详情信息
		this.selectSubMenu(3,"交易密码管理");
		
		//验证文本提示信息
		this.wait.until(new ExpectedCondition<Boolean>() {
	           @Override
	           public Boolean apply(WebDriver webDriver) {
	               String text = webDriver.findElement(By.xpath("//div[@class='biaoge2']//div[@class='pw_edit pw_edit2']/p")).getText().trim();
	               return text.equalsIgnoreCase("重置您的交易密码");
	           }
	       });
		//点击发送验证码
		this.driver.findElement(By.xpath(".//*[@id='clickCode']")).click();
								
		//输入验证码
		this.driver.findElement(By.xpath(".//*[@id='code']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='code']")).sendKeys("1234");
								
		//输入身份证号
		this.driver.findElement(By.xpath(".//*[@id='identity']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='identity']")).sendKeys(this.identification);
			
		//输入新的交易密码
		this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).sendKeys("Hh123457");
				
		//再次输入新的交易密码
		this.driver.findElement(By.xpath(".//*[@id='ResetConfirmPass']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='ResetConfirmPass']")).sendKeys("Hh123457");
				
		//点击确定
		this.driver.findElement(By.id("btn_submit")).click();
				
		
		//等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath(".//*[@id='tipIK']/p")).isDisplayed();
			}
		});
		
		 if(driver.getPageSource().contains("我知道了"))
		 {
			 assertTrue("修改交易密码后提示信息不正确",this.getElementText(".//*[@id='tipIK']/p").equals("交易密码重置成功"));
		
		 }
		 else{
			 
			 fail("修改交易密码失败！");
		 }
		 
		 //点击我知道了
		 this.driver.findElement(By.linkText("我知道了")).click();
		 
		//点击发送验证码
		this.driver.findElement(By.xpath(".//*[@id='clickCode']")).click();
									
		//输入验证码
		this.driver.findElement(By.xpath(".//*[@id='code']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='code']")).sendKeys("1234");
									
		//输入身份证号
		this.driver.findElement(By.xpath(".//*[@id='identity']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='identity']")).sendKeys(this.identification);
				
		//输入新的交易密码
		this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='ResetNewPass']")).sendKeys(this.jiaoyipassword);
					
		//再次输入新的交易密码
		this.driver.findElement(By.xpath(".//*[@id='ResetConfirmPass']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='ResetConfirmPass']")).sendKeys(this.jiaoyipassword);
					
		//点击确定
		this.driver.findElement(By.id("btn_submit")).click();
		
		//等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
					@Override
					public Boolean apply(WebDriver webDriver) {
						return webDriver.findElement(By.xpath(".//*[@id='tipIK']/p")).isDisplayed();
					}
		});
				
		
		 if(driver.getPageSource().contains("我知道了"))
		 {
			 assertTrue("修改交易密码后提示信息不正确",this.getElementText(".//*[@id='tipIK']/p").equals("交易密码重置成功"));
		 }
		 else{
			 
			 fail("修改交易密码失败！");
		 }
		 
		 //点击我知道了
		 this.driver.findElement(By.linkText("我知道了")).click();
	}
	

}
