package com.hfax.selenium.regression.account.home;

import static org.junit.Assert.assertTrue;

import com.hfax.selenium.regression.account.AccountBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

/**
 * 
 * @author changwj
 * @Description 我的账户---我的主页
 */ 
public class 我的主页 extends AccountBaseTest {
    // 我的主页
    @Test
    public void 我的主页() {
        this.selectMenu(1, "我的主页");

        String username = this.driver.findElement(By.xpath("//div[@class='xx_info']/table/tbody/tr[1]/td/span")).getText();
        assertTrue("用户名不对", username.equalsIgnoreCase(this.username));

        String inviteCode = this.driver.findElement(By.xpath("//div[@class='xx_info']/table/tbody/tr[4]/td")).getText();
        assertTrue("邀请码不能为空", inviteCode.split(" ")[0].trim().length() > 0);

        String formInviteCode = this.driver.findElement(By.xpath("//input[@id='inviteCode']")).getAttribute("value");
        assertTrue("表格里的邀请码不匹配", inviteCode.split(" ")[0].trim().equalsIgnoreCase(formInviteCode));

        List<WebElement> elements = this.driver.findElements(By.xpath("//div[contains(@class, 'zh_hz')]/ul/li/p"));
        int i = 0;
        for (WebElement element : elements) {
            assertTrue("第" + i + "列信息不全", element.getText().replace("￥", "").trim().length() > 0);
            i ++;
        }

        String productName = this.driver.findElement(By.xpath("//div[@class='assetRight']/table/tbody/tr[2]/td[1]/a")).getText();
        assertTrue("产品名称不匹配", productName.equalsIgnoreCase("惠理财"));
    }

    @Test
    public void 邀请好友失败() {
        this.selectMenu(1, "我的主页");

        this.driver.findElement(By.xpath("//div[@class='xx_info']/table/tbody/tr[4]/td/span")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.className("yaoqing")));

        this.driver.findElement(By.xpath("//div[@class='yaoqing']/h3/b/a")).click();
        assertTrue("姓名错误提示不正确", this.driver.findElement(By.id("s_inviterName")).getText().equalsIgnoreCase("请输入你的姓名！"));
        assertTrue("被邀请人姓名错误提示不正确", this.driver.findElement(By.id("s_inviteeName")).getText().equalsIgnoreCase("请输入被邀请人的姓名！"));
        assertTrue("手机错误提示不正确", this.driver.findElement(By.id("s_inviteePhone")).getText().equalsIgnoreCase("请正确填写手机号码！"));
    }

    @Test
    public void 邀请好友() {
        this.selectMenu(1, "我的主页");

        this.driver.findElement(By.xpath("//div[@class='xx_info']/table/tbody/tr[4]/td/span")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.className("yaoqing")));
        this.driver.findElement(By.id("inviterName")).sendKeys(this.name);
        this.driver.findElement(By.id("inviteeName")).sendKeys(this.name);
        this.driver.findElement(By.id("inviteePhone")).sendKeys(this.phone);

        this.driver.findElement(By.xpath("//div[@class='yaoqing']/h3/b/a")).click();
        this.wait.until(ExpectedConditions.alertIsPresent());

        assertTrue("邀请不成功", this.driver.switchTo().alert().getText().equalsIgnoreCase("邀请已发送成功！"));
        this.driver.switchTo().alert().accept();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("yaoqing")));
    }
}