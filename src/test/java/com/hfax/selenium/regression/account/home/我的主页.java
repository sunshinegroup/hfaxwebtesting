package com.hfax.selenium.regression.account.home;

import static org.junit.Assert.assertTrue;

import com.hfax.selenium.regression.account.AccountBaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

/**
 * 
 * @author changwj
 * @Description 我的账户---我的主页
 */ 
public class 我的主页 extends AccountBaseTest {
    // 我的主页
    @Test
    public void 我的主页() {
        this.selectMenu(1, "我的主页");

        String username = this.driver.findElement(By.xpath("//div[@class='xx_info']/table/tbody/tr[1]/td/span")).getText();
        assertTrue("用户名不对", username.equalsIgnoreCase(this.username));

        String inviteCode = this.driver.findElement(By.xpath("//div[@class='xx_info']/table/tbody/tr[4]/td")).getText();
        assertTrue("邀请码不能为空", inviteCode.split(" ")[0].trim().length() > 0);

        String formInviteCode = this.driver.findElement(By.xpath("//input[@id='inviteCode']")).getAttribute("value");
        assertTrue("表格里的邀请码不匹配", inviteCode.split(" ")[0].trim().equalsIgnoreCase(formInviteCode));

        List<WebElement> elements = this.driver.findElements(By.xpath("//div[contains(@class, 'zh_hz')]/ul/li/p"));
        int i = 0;
        for (WebElement element : elements) {
            assertTrue("第" + i + "列信息不全", element.getText().replace("￥", "").trim().length() > 0);
            i ++;
        }

        String productName = this.driver.findElement(By.xpath("//div[@class='assetRight']/table/tbody/tr[2]/td[1]/a")).getText();
        assertTrue("产品名称不匹配", productName.equalsIgnoreCase("惠理财"));
        
        //验证头像是否加载
        String pic = this.driver.findElement(By.xpath("//div[@class='pic']/img")).getAttribute("src");
        String pic_check = this.backupUrl + "/images/ico_49.png";
        assertTrue("头像加载失败",pic.equalsIgnoreCase(pic_check));
        
        //验证账户余额悬停提示
        Actions builder = new Actions(driver);
        builder.moveToElement(driver.findElement(By.xpath("//div[@class='zh_hz clearfix']/ul/li[1]/div/div"))).perform();
        String tooltip1 = this.driver.findElement(By.xpath("//div[@class='zh_hz clearfix']/ul/li[1]/div/div/div")).getText();
        assertTrue("账户余额悬停提示加载错误", tooltip1.equalsIgnoreCase("账户内投资人可自由支配的资金及冻结资金"));
        
        //验证账户总资产悬停提示
        builder.moveToElement(driver.findElement(By.xpath("//div[@class='zh_hz clearfix']/ul/li[2]/div/div"))).perform();
        String tooltip2 = this.driver.findElement(By.xpath("//div[@class='zh_hz clearfix']/ul/li[2]/div/div/div")).getText();
        assertTrue("账户总资产悬停提示加载错误", tooltip2.equalsIgnoreCase("账户总资产 = 可用余额 + 投资中冻结金额 + 提现中冻结金额 + 待收本金 + 待收收益"));
        
        //验证累计净收益悬停提示
        builder.moveToElement(driver.findElement(By.xpath("//div[@class='zh_hz clearfix']/ul/li[3]/div/div"))).perform();
        String tooltip3 = this.driver.findElement(By.xpath("//div[@class='zh_hz clearfix']/ul/li[3]/div/div/div")).getText();
        assertTrue("累计净收益悬停提示加载错误", tooltip3.equalsIgnoreCase("投资人在惠金所所获得的所有投资收益以及其他收益（包括但不限于推广活动收入等）"));

        //点击充值提现
        this.driver.findElement(By.xpath("//div[@class='mid']/ul/li[1]/a/img")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='tabtil']/ul/li[1]")));
        String link1 = this.driver.findElement(By.xpath("//div[@class='tabtil']/ul/li[1]")).getText();
        assertTrue("充值提现链接错误错误", link1.equalsIgnoreCase("资金记录"));
        this.driver.navigate().back();
        
        //点击我要理财
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='mid']/ul/li[2]/a/img")));
        this.driver.findElement(By.xpath("//div[@class='mid']/ul/li[2]/a/img")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='navBox']/span")));
        String link2 = this.driver.getTitle();  
        assertTrue("我要理财链接错误错误", link2.equalsIgnoreCase("投资理财-惠金所-阳光保险集团成员"));
        this.driver.navigate().back();
        
        //点击我要还款
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='mid']/ul/li[3]/a/img")));
        this.driver.findElement(By.xpath("//div[@class='mid']/ul/li[3]/a/img")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='tabtil']/ul/li[2]")));
        String link3 = this.driver.findElement(By.xpath("//div[@class='tabtil']/ul/li[2]")).getText();
        assertTrue("我要还款链接错误错误", link3.equalsIgnoreCase("正在还款的借款"));
        this.driver.navigate().back();
        
        //点击待收款
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='mid']/ul/li[4]/a/img")));
        this.driver.findElement(By.xpath("//div[@class='mid']/ul/li[4]/a/img")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='lab_1']")));
        String link4 = this.driver.findElement(By.xpath(".//*[@id='lab_1']")).getText();
        assertTrue("我要还款链接错误错误", link4.equalsIgnoreCase("惠理财"));
        this.driver.navigate().back();
        
        //点击资金流水
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='mid']/ul/li[5]/a/img")));
        this.driver.findElement(By.xpath("//div[@class='mid']/ul/li[5]/a/img")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='tabtil']/ul/li[1]")));
        String link5 = this.driver.findElement(By.xpath("//div[@class='tabtil']/ul/li[1]")).getText();
        assertTrue("资金流水链接错误错误", link5.equalsIgnoreCase("资金记录"));
        this.driver.navigate().back();
        
        //检查资产详情表格
        List<WebElement> elements2 = this.driver.findElements(By.xpath("//div[@class='assetRight']/table/tbody/tr/th"));
        int i2 = 0;
        for (WebElement element : elements2) {
            assertTrue("第" + i2 + "列信息不全", element.getText().trim().length() > 0);
            i2 ++;
        }
        
        //点击安全退出
        this.driver.findElement(By.linkText("安全退出")).click();
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='topBox']/span/a[1]")));
        String link6 = this.driver.findElement(By.xpath("//div[@class='topBox']/span/a[1]")).getText();
        assertTrue("安全退出链接错误", link6.contains("登录"));
    }

    @Test
    public void 邀请好友失败() {
        this.selectMenu(1, "我的主页");
        
        //添加等待
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath("//div[@class='xx_info']/table/tbody/tr[4]/td/span")).getText();
                return text.trim().equalsIgnoreCase("立即邀请好友");
            }
        });  
        System.out.println("邀请好友链接上的文字为：" + this.getElementText("//div[@class='xx_info']/table/tbody/tr[4]/td/span"));
        
        this.driver.findElement(By.xpath("//div[@class='xx_info']/table/tbody/tr[4]/td/span")).click();
        //this.wait.until(ExpectedConditions.presenceOfElementLocated(By.className("yaoqing")));
        //添加等待
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath("//div[@class='yaoqing']/h3/b/a")).getText();
                return text.trim().equalsIgnoreCase("发送短信");
            }
        }); 
        System.out.println("点击邀请好友后的链接界面上的信息为：" + this.getElementText("//div[@class='yaoqing']/h3/b/a"));
        this.driver.findElement(By.xpath("//div[@class='yaoqing']/h3/b/a")).click();
        
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='invitePop']/div[2]/h2")));
        assertTrue("姓名错误提示不正确", this.driver.findElement(By.id("s_inviterName")).getText().equalsIgnoreCase("请输入你的姓名！"));
        assertTrue("被邀请人姓名错误提示不正确", this.driver.findElement(By.id("s_inviteeName")).getText().equalsIgnoreCase("请输入被邀请人的姓名！"));
        assertTrue("手机错误提示不正确", this.driver.findElement(By.id("s_inviteePhone")).getText().equalsIgnoreCase("请正确填写手机号码！"));
    }

    @Test
    public void 邀请好友() {
        this.selectMenu(1, "我的主页");

        //添加等待
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath("//div[@class='xx_info']/table/tbody/tr[4]/td/span")).getText();
                return text.trim().equalsIgnoreCase("立即邀请好友");
            }
        });  
        System.out.println("邀请好友链接上的文字为：" + this.getElementText("//div[@class='xx_info']/table/tbody/tr[4]/td/span"));
        this.driver.findElement(By.xpath("//div[@class='xx_info']/table/tbody/tr[4]/td/span")).click();
        //this.wait.until(ExpectedConditions.presenceOfElementLocated(By.className("yaoqing")));
        //添加等待
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath("//div[@class='yaoqing']/h3/b/a")).getText();
                return text.trim().equalsIgnoreCase("发送短信");
            }
        }); 
        System.out.println("点击邀请好友后的链接界面上的信息为：" + this.getElementText("//div[@class='yaoqing']/h3/b/a"));
        this.driver.findElement(By.id("inviterName")).sendKeys(this.name);
        this.driver.findElement(By.id("inviteeName")).sendKeys(this.name);
        this.driver.findElement(By.id("inviteePhone")).sendKeys(this.phone);

        this.driver.findElement(By.xpath("//div[@class='yaoqing']/h3/b/a")).click();
        this.wait.until(ExpectedConditions.alertIsPresent());

        assertTrue("邀请不成功", this.driver.switchTo().alert().getText().equalsIgnoreCase("邀请已发送成功！"));
        this.driver.switchTo().alert().accept();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("yaoqing")));
    }
}