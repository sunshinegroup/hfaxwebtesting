package com.hfax.selenium.regression.account.settings;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;
/**
 * 
 * @author changwj
 * @Description 我的账户--用户设置--设置新密码
 * @Modify songxq 24/8/2016
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 登录密码管理 extends AccountBaseTest{
	
	private void setupFormFields(String oldPassword, String newPassword, String confirmPassword) {
		this.driver.findElement(By.id("oldPassword")).sendKeys(oldPassword);
		this.driver.findElement(By.id("newPassword")).sendKeys(newPassword);
		this.driver.findElement(By.id("confirmPassword")).sendKeys(confirmPassword);
		this.driver.findElement(By.className("bcbtn")).click();
	}

	private String waitForErrorMessage() {
		
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return webDriver.findElement(By.id("s_tip")).isDisplayed();
			}
		});

		return this.driver.findElement(By.id("s_tip")).getText();
	}
   
	//验证界面提示文本是否正确
	@Test
	public void test001_校验登录密码管理界面() {
		//点击用户设置
		 用户设置();
		 
		//点击登录密码管理
		 this.selectSubMenu(2,"登录密码管理");
		 List<WebElement> list = this.driver.findElements(By.xpath("//span[@class='txt']"));
		 
		 //验证文本输入您现在的账号密码
		 assertTrue("原密码后的文本提示不正确",list.get(0).getText().trim().equals("输入您现在的帐号密码"));
		 
		 //验证文本输入您的新密码
		 assertTrue("新密码后的文本提示不正确",list.get(1).getText().trim().equals("输入您的新密码"));
		 
		 //验证文本再次输入您的新密码
		 assertTrue("确认新密码的文本提示不正确",list.get(2).getText().trim().equals("请再次输入您的新密码"));
		 
		 //点击提交
		 this.driver.findElement(By.xpath("//a[@class='bcbtn']")).click();
		
		//验证提交后提示信息是否正确
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                String text = webDriver.findElement(By.xpath("//span[@id = 's_tip']")).getText().trim();
	                return text.equalsIgnoreCase("请完整填写密码信息");
	            }
	        });
		 
	}
	
	/**
	 * 用户设置---登录密码管理   
	 * 校验新密码是否为空
	 * 提示:请完整填写密码信息
	 */
	@Test
	public void test002_校验新密码是否为空() {
		//点击用户设置
		 用户设置();
		 
		//点击登录密码管理
		this.selectSubMenu(2,"登录密码管理");
		
		String text = this.driver.findElement(By.xpath("//tr//th[@align = 'left']")).getText();
		assertTrue("获取登录密码失败", "登录密码修改".equals(text));

		this.setupFormFields(this.password, "", "");
		String errorMessage = this.waitForErrorMessage();

	    assertTrue("提示密码错误信息不对", errorMessage.equals("请完整填写密码信息"));
	}

	/**
	 * 用户设置-- 校验新密码是否符合规则
	 * 新密码:123
	 * 确认密码:123
	 * 提示:密码长度必须为8-16个数字与大写或者小写字母的组合
	 */
	@Test
	public void test003_校验密码是否符合规则() {
		//点击用户设置
		用户设置();
		
		//点击登录密码管理
		this.selectSubMenu(2,"登录密码管理");
	
		String text = this.driver.findElement(By.xpath("//tr//th[@align = 'left']")).getText();
		assertTrue("获取登录密码失败", "登录密码修改".equals(text));

        this.setupFormFields(this.password, "123", "123");
        String errorMessage = this.waitForErrorMessage();

	    assertTrue("错误信息不对", errorMessage.equalsIgnoreCase("密码长度必须为8-16个数字与大写或者小写字母的组合"));
		
	}

	/**
	 * 用户设置--校验新密码与确认密码是否一致
	 * 新密码:a1111111111
	 * 确认密码:a1111111112
	 * 提示:两次密码不一致
	 */
	@Test
	public void test004_校验新确认密码是否一致(){
		//点击用户设置
		用户设置();
		
		//点击登录密码管理
		this.selectSubMenu(2,"登录密码管理");
		
		String text = this.driver.findElement(By.xpath("//tr//th[@align = 'left']")).getText();
		assertTrue("获取登录密码失败", "登录密码修改".equals(text));

        this.setupFormFields(this.password, this.password + "1", this.password + "2");
        String errorMessage = this.waitForErrorMessage();

		assertTrue("错误信息不对", errorMessage.equalsIgnoreCase("两次密码不一致"));
	}

	/**
	 * 
	 * 设置新密码成功
	 */
	@Test
	public void test005_设置新密码(){
		//点击用户设置
		用户设置();
		
		//点击登录密码管理
		this.selectSubMenu(2,"登录密码管理");
		
		String text = this.driver.findElement(By.xpath("//tr//th[@align = 'left']")).getText();
		assertTrue("获取登录密码失败", "登录密码修改".equals(text));

        this.setupFormFields(this.password, this.password + "1", this.password + "1");

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.switchTo().alert().getText().equalsIgnoreCase("修改密码成功");
            }
        });

        this.driver.switchTo().alert().accept();

        this.setupFormFields(this.password + "1", this.password, this.password);

        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.switchTo().alert().getText().equalsIgnoreCase("修改密码成功");
            }
        });

        this.driver.switchTo().alert().accept();


	}
}
