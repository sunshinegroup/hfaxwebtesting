package com.hfax.selenium.regression.account;

import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.Util;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by philip on 3/17/16.
 */
public class AccountBaseTest extends BaseTest {
    @Override
    public void setup() throws Exception {
        super.setup();
        this.loadPage();
        this.login();
        this.navigateToPage("我的账户");
    }

    public void 用户设置() {
        this.selectMenu(5, "用户设置");
    }

    public void 充值提现() {
        this.selectMenu(2, "充值提现");
    }

    public void 我的投资劵() {
        this.selectMenu(16, "我的投资券");
    }
    
    //设置时间
    public static String setDate(Date date,int i){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, +i);
        
        Date datetime = calendar.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(datetime);
        return dateString;
       }
    
    //获取元素内容
    protected String getElementText(String strXpath){
    	try{
    		String text = this.driver.findElement(By.xpath(strXpath)).getText().trim();
    		return text;
    	}catch (Exception e){
    		e.printStackTrace();
    	}
    	return null;
    }
     //搜索投资的产品
  		public void  searchFinancePro(String proName){
  			Select select = new Select(this.driver.findElement(By.id("borrowstatus")));
  			for (int i = 0; i < select.getOptions().size(); i++) {
  				if(select.getOptions().get(i).getText().equals(proName))
  				{
  					select.getOptions().get(i).click();
  					break;
          }
       }
    }
}
