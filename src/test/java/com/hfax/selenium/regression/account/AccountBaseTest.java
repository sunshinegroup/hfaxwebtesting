
package com.hfax.selenium.regression.account;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.base.Util;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by philip on 3/17/16.
 */
public class AccountBaseTest extends BaseTest {
    @Override
    public void setup() throws Exception {
        super.setup();
        this.loadPage();
        this.login();
        this.navigateToPage("我的账户");
    }

    protected void selectMenu(int index, String titleToVerify) {
        this.driver.findElement(By.id("li_" + index)).click();
        String selectedMenuTitle = this.driver.findElement(By.xpath("//div[@class='l_nav']//li[contains(@class, 'on')]/a")).getText();
        assertTrue("选中菜单名字不正确: " + selectedMenuTitle, selectedMenuTitle.equalsIgnoreCase(titleToVerify));
    }

    protected void selectSubMenu(int index, String titleToVerify) {
        this.driver.findElement(By.xpath("//div[@class='tabtil']/ul/li["+ index +"]")).click();
        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='tabtil']/ul/li[contains(@class, 'on')]"), titleToVerify));
    }

    public void 用户设置() {
        this.selectMenu(5, "用户设置");
    }

    public void 充值提现() {
        this.selectMenu(2, "充值提现");
    }

    public void 我的投资劵() {
        this.selectMenu(16, "我的投资券");
    }
}
