package com.hfax.selenium.smoke.静态页面;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.hfax.selenium.base.BaseTest;
import org.openqa.selenium.By;

/**
 * 
 * @author changwj
 * @Description 导航栏我的账户
 */
public class 我的账户 extends BaseTest{
	@Test
	public void 我的账户() {
		this.loadPage();
		this.driver.findElement(By.linkText("我的账户")).click();
		assertTrue("读取我的账户失败", driver.getTitle().equalsIgnoreCase("登录"));
	}
}
