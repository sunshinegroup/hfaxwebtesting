package com.hfax.selenium.smoke.静态页面;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.hfax.selenium.base.BaseTest;

public class 投资理财 extends BaseTest {
	/**
	 * 
	 * @author changwj
	 * @Description  导航栏投资理财
	 * 投资理财未登录情况, 需要标的
	 * 失败:请先登录用户
	 */
	@Test
	public void 投资理财未登录() {
		this.loadPage();
		navigateToPage("投资理财");

        List<WebElement> list = this.driver.findElements(By.xpath("//div[contains(@class, 'listBox-Info')]"));
        assertTrue("投资理财内容不能为空", list.size() > 0);
	}
}
