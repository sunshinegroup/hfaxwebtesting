package com.hfax.selenium.smoke.静态页面;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

public class 新手专区 extends BaseTest {
	/**
	 * 导航栏新手专区 未登录情况下新手专区需要标的 未成功情况：请先登录用户
	 * 
	 */

	@Test
	public void 新手专区未登录() {
		this.loadPage();
		this.navigateToPage("新手专区");

		List<WebElement> findElements = this.driver.findElements(By.className("listTabBox"));
		assertTrue("新手专区不能为空", findElements.size() > 0);
	}
}
