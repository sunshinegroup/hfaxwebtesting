package com.hfax.selenium.smoke.静态页面;

import static org.junit.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

public class 新手专区 extends BaseTest {
	/**
	 * @Description 新手专区静态页面验证
	 * Created by songxq on 21/4/16.
	 */
    @Override
	public void setup() throws Exception {
		super.setup();
        this.loadPage();
        this.navigateToPage("新手专区");
	}
	@Test
	public void 新手专区未登录() {
		List<WebElement> findElements = this.driver.findElements(By.xpath("//div[@class='listBox-Info clearfix']"));
		assertTrue("新手专区不能为空", findElements.size() > 0);
	}
	
	//验证新手专区的title
	public void 验证新手专区的title(){
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tab_5']")));
		WebElement title = this.driver.findElement(By.xpath(".//*[@id='tab_5']"));
		String text = this.driver.findElement(By.xpath(".//*[@id='tab_5']")).getAttribute("text");
		assertTrue("新手专区title", text.trim().equals("新手专区"));
	}
	
	//验证惠金所logo
	@Test
	public void 新手专区惠金所logo(){
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//strong/a[contains(@href,'toIndexListInit.do')]"));
			}
		});	
		this.driver.findElement(By.xpath("//strong/a[contains(@href,'toIndexListInit.do')]")).click();
		WebElement logo = this.driver.findElement(By.xpath("//strong/a[contains(@href,'toIndexListInit.do')]"));		
		assertTrue("惠金所logo存在",logo.isDisplayed());
	}
	
	//验证新手专区访问首页
	@Test
	public void 新手专区站内访问路径首页(){
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@class='navBox']//a[contains(@href,'toIndexListInit')]"));
			}
		});	
		this.driver.findElement(By.xpath("//div[@class='navBox']//a[contains(@href,'toIndexListInit')]")).click();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//li[@class='first']//a[@class='hover']"));
			}
		});	
		WebElement homepage = this.driver.findElement(By.xpath("//li[@class='first']//a[@class='hover']"));
		assertTrue("新手专区首页正常跳转",homepage.isDisplayed());
	}
	
	//验证新手专区标语图片正常显示
	@Test
	public void 新手专区标语图片(){
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@class='listBanner-box']/img"));
			}
		});	
		WebElement pic = this.driver.findElements(By.xpath("//div[@class='listBanner-box']/img")).get(0);
		assertTrue("新手专区标语图片正常显示",pic.isDisplayed());
	}
	
	//验证新手专区的产品链接
	@Ignore
	@Test
	public void 新手专区验证产品列表链接(){
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath(".//*[@id='display_5']"));
			}
		});		
		List<WebElement> list = this.driver.findElements(By.xpath("//div[@id='display_7']//div[@class='listBig-Box']//div[@class='listBox-Info clearfix']"));
		assertTrue("产品个数不能为0",list.size() > 0 );
		for(int i = 2; i <=list.size()+1; i++)
		{ 
			String s = String.valueOf(i);			
     		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_5']/div/div[1]/img")));
     		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_5']/div/div["+ s +"]/div[1]/div[1]/a")));
     		String currentWindow = this.driver.getWindowHandle();//获取当前窗口句柄
     		this.driver.findElement(By.xpath(".//*[@id='display_5']/div/div["+ s +"]/div[1]/div[1]/a")).click();
    		Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
			Iterator<String> it = handles.iterator();
			while (it.hasNext()) {
			if (currentWindow == it.next()) {
				continue;
				}
			WebDriver window = this.driver.switchTo().window(it.next());//切换到新窗口
			this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("//a[@id='tab_5']")));
			WebElement newzone = this.driver.findElement(By.xpath("//*[@id='index7']/a"));
			assertTrue("产品列表正常跳转",newzone.isDisplayed());
			window.close();//关闭新窗口
			}
			this.driver.switchTo().window(currentWindow);//回到原来页面
			
		}
	}
	
	//验证新手专区立即投资
	@Test
	public void 新手专区立即投资(){
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath(".//*[@id='display_5']/div/div[2]/div[2]/div[2]/a"));
			}
		});	
		this.driver.findElement(By.xpath(".//*[@id='display_5']/div/div[2]/div[2]/div[2]/a")).click();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='amount']")));
		WebElement textbox = this.driver.findElement(By.xpath("//input[@id='amount']"));
		assertTrue("立即投资正常跳转",textbox.isDisplayed());
	}
	
	//验证新手专区页面翻页功能，首页，末页，下一页，跳转
    @Test
	public void 新手专区验证页面页码跳转() throws AWTException, InterruptedException{
    	
		//滚轮向下,验证页码首页，下一页
		((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("下一页")));
		this.driver.findElement(By.linkText("下一页")).click();
		((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement firstpage = this.driver.findElement(By.linkText("首页"));
		assertTrue("页面页码首页存在",firstpage.isDisplayed());
		
		//验证页码末页
		((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("上一页")));
		this.driver.findElement(By.linkText("上一页")).click();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("末页")));
		this.driver.findElement(By.linkText("末页")).click();
		((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("末页")));
		WebElement backpage = this.driver.findElement(By.linkText("首页"));
		assertTrue("页面页码首页存在",backpage.isDisplayed());
		
		//验证页码跳转
   	    ((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("curPageText")));
		List<WebElement> inputbox =driver.findElements(By.cssSelector("#curPageText"));
		inputbox.get(0).clear();
		inputbox.get(0).sendKeys("2");
		this.driver.findElement(By.linkText("跳转")).click();
		WebElement shouye = this.driver.findElement(By.linkText("上一页"));
		assertTrue("页码正常跳转",shouye.isDisplayed());
		
    }
	
}
