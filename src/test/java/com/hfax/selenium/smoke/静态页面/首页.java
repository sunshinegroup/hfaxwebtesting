package com.hfax.selenium.smoke.静态页面;

import java.util.List;
import java.util.Set;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.hfax.selenium.base.BaseTest;
import static org.junit.Assert.assertTrue;

/**
 * Created by philip on 2/24/16.
 * 
 * @author changwj
 * update by wangmingqiang 4/27/16 add 增加注册、登陆按钮、转让专区
 * @description 首页链接
 */

public class 首页 extends BaseTest {
	@Override
	public void setup() throws Exception {
		super.setup();
        this.loadPage();
	}

	private void verifyPageLoad(String title) {
		String selectedMenuText = this.driver.findElement(By.xpath("//div[contains(@class,'nav_l')]")).findElement(By.xpath("ul/li[@class='on']/a")).getText().trim();
		assertTrue("页面读取失败: " + selectedMenuText, selectedMenuText.equalsIgnoreCase(title));
	}

	// 登录按钮
	@Test

	public void 首页验证() {
		
		wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//p[@class='news_tit']/span/a")).isDisplayed();
            }
        });
		System.out.println("-----------新手区加载成功-------------");
		List<WebElement> findElements = this.driver.findElements(By.xpath("//p[@class='news_tit']/span/a"));
		assertTrue("获取'新客专区'失败", findElements.size() > 0);
		for (int i = 0; i < findElements.size(); i++) {
			assertTrue("新手标信息有误", !"".equals(findElements.get(i).getText().trim()));
		}
		System.out.println("-----------首页——>新手专区产品列表验证通过-------------");

		findElements = this.driver.findElements(By.xpath(".//*[@id='scroll']/ul/li[1]/p[1]/a"));
		assertTrue("获取'惠投资'失败", findElements.size() > 0);
		for (int i = 0; i < findElements.size(); i++) {
			assertTrue("阳光'惠投资'信息有误", !"".equals(findElements.get(i).getText().trim()));
		}
		System.out.println("-----------首页——>阳光'惠投资'列表验证通过-------------");		
		
		
		findElements = this.driver.findElements(By.xpath("//div[@class='side-list']/div[@class='hjs-list-info clearfix']/div[@class='hjs-listinfo-left']/div/a"));
		assertTrue("获取'惠理财'失败", findElements.size() > 0);
		for (int i = 0; i < findElements.size(); i++) {
			assertTrue("阳光'惠理财'信息数量有误", !"".equals(findElements.get(i).getText().trim()));
		}
		System.out.println("-----------首页——>阳光'惠理财'产品列表验证通过-------------");		
	
		findElements = this.driver.findElements(By.xpath("//div[@class='side-list transfer-list']/div[@class='hjs-list-info clearfix']/div[@class='hjs-listinfo-left']/div/a[@class='info-title']"));
		assertTrue("获取'转让专区'产品信息失败", findElements.size() > 0);
		for (int i = 0; i < findElements.size(); i++) {
			assertTrue("阳光'转让专区'信息数量有误", !"".equals(findElements.get(i).getText().trim()));
		}
		System.out.println("-----------首页——>转让产品列表验证通过-------------");
		
		String currenthandle =  driver.getWindowHandle();
		this.driver.findElement(By.xpath("//div[@class='login']/div/p/a[1]")).click();
		
		Set<String> handles = driver.getWindowHandles();
		for (String handle : handles) {
			if(handle.equals(currenthandle))
				continue;
			else
				driver.switchTo().window(handle);
				
		}
		
		assertTrue("首页_登录按钮测试失败", driver.getTitle().equals("登录"));
		System.out.println("-----------首页——>登录按钮验证通过-------------");
		
		this.driver.navigate().back();
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='login']/div/p/a[2]")));

		this.driver.findElement(By.xpath("//div[@class='login']/div/p/a[2]")).click();
		wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.getTitle().equals("注册");
            }
        });

		assertTrue("首页_立即注册按钮测试失败", driver.getTitle().equals("注册"));
		System.out.println("-----------首页——>注册按钮验证通过-------------");	
	

	}
	/*
	//常见问题--更多
	@Test
	public void 常见问题更多(){
		this.driver.findElement(By.xpath("html/body/div[8]/div/div[1]/h2/a")).click();

		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='常见问题']"));
			}
		});
		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='常见问题']"));
		assertTrue("常见问题失败",findElement != null);
	}

	//媒体报道--更多
	@Test
	public void 媒体报道更多(){
		this.driver.findElement(By.xpath("html/body/div[8]/div/div[2]/h2/a")).click();

		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='媒体报道']"));
			}
		});
		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='媒体报道']"));
		assertTrue("媒体报道失败",findElement != null);
	}

	//公司动态--更多
	@Test
	public void 公司动态更多(){
		this.driver.findElement(By.xpath("html/body/div[8]/div/div[3]/h2/a")).click();

        this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='公司动态']"));
			}
		});

		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='公司动态']"));
		assertTrue("公司动态失败",findElement != null);
	}

	// 页尾关于我们
	@Test
	public void 页尾关于我们() {
		this.driver.findElement(By.xpath("//div[@class='footTop']/div/a[1]")).click();

		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='关于我们']"));
			}
		});
		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='关于我们']"));
		assertTrue("关于我们失败",findElement != null);
	}

	// 页尾帮助中心
	@Test
	public void 页尾帮助中心() {
		this.driver.findElement(By.xpath("//div[@class='footTop']/div/a[3]")).click();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='常见问题']"));
			}
		});
		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='常见问题']"));
		assertTrue("帮助中心失败",findElement != null);
	}

	// 页尾最新公告
	@Test
	public void 页尾最新公告() {
		this.driver.findElement(By.xpath("//div[@class='footTop']/div/a[5]")).click();

		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='最新公告']"));
			}
		});

		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='最新公告']"));
		assertTrue("最新公告失败",findElement != null);
	}

	// 页尾安全保障
	@Test
	public void 页尾安全保障() {
		this.driver.findElement(By.xpath("//div[@class='footTop']/div/a[2]")).click();
		this.verifyPageLoad("信息安全保障");
	}

	// 页尾媒体报道
	@Test
	public void 页尾媒体报道() {
		this.driver.findElement(By.xpath("html/body/div[12]/div/div[1]/div[1]/a[4]")).click();
        this.verifyPageLoad("媒体报道");
	}

	// 惠金所官方微信
	@Test
	public void 惠金所官方微信() {
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@class='weixin']/img"));
			}
		});

		this.driver.findElement(By.xpath("//div[@class='weixin']/img")).click();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@id='wexin']"));
			}
		});
		WebElement returnEle = this.driver.findElement(By.xpath("//div[@id='wexin']"));
		assertTrue("官方微信失败", returnEle.isDisplayed());
	}

	// 页尾在线客服{不在首要测试功能点已改版}
//	@Test
	public void 页尾在线客服() {
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@class='kefuPhone']/a"));
			}
		});
		this.driver.findElement(By.xpath("//div[@class='kefuPhone']/a")).click();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@id='kefuPop']"));
			}
		});
		WebElement returnEle = this.driver.findElement(By.xpath("//div[@id='kefuPop']"));
		assertTrue("获取客服失败", returnEle.isDisplayed());
	}

	// 惠金所App移动客户端
	@Test
	public void 惠金所App移动客户端() {
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@class='xiazai-app-box']"));
			}
		});
		this.driver.findElement(By.xpath("//div[@class='xiazai-app-box']")).click();

		WebDriver popupDriver = this.switchToWindow("惠金所", true);
		String iOSLink = popupDriver.findElement(By.xpath("//li[@class='back1']/p/span/img")).getAttribute("src");
		assertTrue("读取惠金所App移动客户端失败",iOSLink.contains("iosMaPC"));
		String androidLink = popupDriver.findElement(By.xpath("//li[@class='back1']/p/span[2]/img")).getAttribute("src");
		assertTrue("读取惠金所App移动客户端失败",androidLink.contains("apkMaPC"));
	}

	@Test
	public void iphone客户端下载() {
		惠金所App移动客户端();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//a[@href='javascript:iosUrl()']"));
			}
		});

		assertTrue("点击ios客户端下载页面失败！", this.driver.findElement(By.xpath("//a[@href='javascript:iosUrl()']")).isDisplayed());
	}

	@Test
	public void android客户端下载() {
		惠金所App移动客户端();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//a[@href='javascript:androidUrl()']"));
			}
		});

		assertTrue("点击android客户端下载页面失败！", this.driver.findElement(By.xpath("//a[@href='javascript:androidUrl()']")).isDisplayed());
	}
*/
}
