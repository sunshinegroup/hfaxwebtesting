package com.hfax.selenium.smoke.静态页面;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;
import static org.junit.Assert.assertTrue;

/**
 * Created by philip on 2/24/16.
 * 
 * @author changwj
 * @description 首页链接
 */

public class 首页 extends BaseTest {
	@Override
	public void setup() throws Exception {
		super.setup();
        this.loadPage();
	}

	private void verifyPageLoad(String title) {
		String selectedMenuText = this.driver.findElement(By.xpath("//div[contains(@class,'nav_l')]")).findElement(By.xpath("ul/li[@class='on']/a")).getText().trim();
		assertTrue("页面读取失败: " + selectedMenuText, selectedMenuText.equalsIgnoreCase(title));
	}

	// 新手专区
	@Test
	public void 新手专区() {
		List<WebElement> findElements = this.driver.findElements(By.xpath("//div[@class='indexNovice']/div/h3/a"));
		assertTrue("获取新手标失败", findElements.size() > 0);
		for (int i = 0; i < findElements.size(); i++) {
			assertTrue("新手标信息有误", !"".equals(findElements.get(i).getText().trim()));
		}
	}

	// 阳光惠理财
	@Test
	public void 阳光惠理财() {
		List<WebElement> findElements = this.driver.findElements(By.xpath("//div[@class='inListBox inBox1']/div/div/h3/a"));
		assertTrue("获取阳光惠理财失败", findElements.size() > 0);
		for (int i = 0; i < findElements.size(); i++) {
			assertTrue("阳光惠理财信息有误", !"".equals(findElements.get(i).getText().trim()));
		}
	}
	
	//常见问题--更多
	@Test
	public void 常见问题更多(){
		this.driver.findElement(By.xpath("html/body/div[8]/div/div[1]/h2/a")).click();

		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='常见问题']"));
			}
		});
		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='常见问题']"));
		assertTrue("常见问题失败",findElement != null);
	}

	//媒体报道--更多
	@Test
	public void 媒体报道更多(){
		this.driver.findElement(By.xpath("html/body/div[8]/div/div[2]/h2/a")).click();

		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='媒体报道']"));
			}
		});
		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='媒体报道']"));
		assertTrue("媒体报道失败",findElement != null);
	}

	//公司动态--更多
	@Test
	public void 公司动态更多(){
		this.driver.findElement(By.xpath("html/body/div[8]/div/div[3]/h2/a")).click();

        this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='公司动态']"));
			}
		});

		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='公司动态']"));
		assertTrue("公司动态失败",findElement != null);
	}

	// 页尾关于我们
	@Test
	public void 页尾关于我们() {
		this.driver.findElement(By.xpath("//div[@class='footTop']/div/a[1]")).click();

		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='关于我们']"));
			}
		});
		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='关于我们']"));
		assertTrue("关于我们失败",findElement != null);
	}

	// 页尾帮助中心
	@Test
	public void 页尾帮助中心() {
		this.driver.findElement(By.xpath("//div[@class='footTop']/div/a[3]")).click();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='常见问题']"));
			}
		});
		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='常见问题']"));
		assertTrue("帮助中心失败",findElement != null);
	}

	// 页尾最新公告
	@Test
	public void 页尾最新公告() {
		this.driver.findElement(By.xpath("//div[@class='footTop']/div/a[5]")).click();

		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='最新公告']"));
			}
		});

		WebElement findElement=this.driver.findElement(By.xpath("//*[@id='showcontent']/h3/em[text()='最新公告']"));
		assertTrue("最新公告失败",findElement != null);
	}

	// 页尾安全保障
	@Test
	public void 页尾安全保障() {
		this.driver.findElement(By.xpath("//div[@class='footTop']/div/a[2]")).click();
		this.verifyPageLoad("信息安全保障");
	}

	// 页尾媒体报道
	@Test
	public void 页尾媒体报道() {
		this.driver.findElement(By.xpath("html/body/div[12]/div/div[1]/div[1]/a[4]")).click();
        this.verifyPageLoad("媒体报道");
	}

	// 惠金所官方微信
	@Test
	public void 惠金所官方微信() {
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@class='weixin']/img"));
			}
		});

		this.driver.findElement(By.xpath("//div[@class='weixin']/img")).click();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@id='wexin']"));
			}
		});
		WebElement returnEle = this.driver.findElement(By.xpath("//div[@id='wexin']"));
		assertTrue("官方微信失败", returnEle.isDisplayed());
	}

	// 页尾在线客服
	@Test
	public void 页尾在线客服() {
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@class='kefuPhone']/a"));
			}
		});
		this.driver.findElement(By.xpath("//div[@class='kefuPhone']/a")).click();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@id='kefuPop']"));
			}
		});
		WebElement returnEle = this.driver.findElement(By.xpath("//div[@id='kefuPop']"));
		assertTrue("获取客服失败", returnEle.isDisplayed());
	}

	// 惠金所App移动客户端
	@Test
	public void 惠金所App移动客户端() {
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//div[@class='xiazai-app-box']"));
			}
		});
		this.driver.findElement(By.xpath("//div[@class='xiazai-app-box']")).click();

		WebDriver popupDriver = this.switchToWindow("惠金所", true);
		String iOSLink = popupDriver.findElement(By.xpath("//li[@class='back1']/p/span/img")).getAttribute("src");
		assertTrue("读取惠金所App移动客户端失败",iOSLink.contains("iosMaPC"));
		String androidLink = popupDriver.findElement(By.xpath("//li[@class='back1']/p/span[2]/img")).getAttribute("src");
		assertTrue("读取惠金所App移动客户端失败",androidLink.contains("apkMaPC"));
	}

	@Test
	public void iphone客户端下载() {
		惠金所App移动客户端();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//a[@href='javascript:iosUrl()']"));
			}
		});

		assertTrue("点击ios客户端下载页面失败！", this.driver.findElement(By.xpath("//a[@href='javascript:iosUrl()']")).isDisplayed());
	}

	@Test
	public void android客户端下载() {
		惠金所App移动客户端();
		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.xpath("//a[@href='javascript:androidUrl()']"));
			}
		});

		assertTrue("点击android客户端下载页面失败！", this.driver.findElement(By.xpath("//a[@href='javascript:androidUrl()']")).isDisplayed());
	}
}
