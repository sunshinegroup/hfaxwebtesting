package com.hfax.selenium.smoke.静态页面;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

import java.util.List;

/**
 * 
 * @author changwj
 * @Description 导航栏关于我们未登录
 */
public class 关于我们 extends BaseTest {
	@Override
	public void setup() throws Exception {
		super.setup();
        this.loadPage();
        this.navigateToPage("关于我们");
	}

    private void selectSubMenu(String name) {
        this.driver.findElement(By.xpath("//div[contains(@class,'nav_l')]")).findElement(By.linkText(name)).click();
        String selectedMenuText = this.driver.findElement(By.xpath("//div[contains(@class,'nav_l')]")).findElement(By.xpath("//ul/li[@class='on']/a")).getText();
        assertTrue("从菜单选取的页面没有读出来 " + name, name.equalsIgnoreCase(selectedMenuText));

        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//h3[@class='tit1']/em"), name));
    }

	@Test
	public void 关于我们() {
        this.selectSubMenu("关于我们");

        this.wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[@id='showcontent']//img")));
        List<WebElement> images = this.driver.findElements(By.xpath("//div[@id='showcontent']//img"));
        assertTrue("关于图片应该有9张: " + images.size(), images.size() == 9);
	}

	@Test
	public void 技术保障() {
        this.selectSubMenu("技术保障");

        String content = this.driver.findElement(By.id("showcontent")).getText();
        assertTrue("内容不能为空", content.trim().length() > 0);
	}

	@Test
	public void 信息安全保障() {
        this.selectSubMenu("信息安全保障");

        String content = this.driver.findElement(By.id("showcontent")).getText();
        assertTrue("内容不能为空", content.trim().length() > 0);
	}

	@Test
	public void 常见问题() {
        this.selectSubMenu("常见问题");

        List<WebElement> list = this.driver.findElements(By.xpath("//div[@class='aboutSetTab']/ul[@id='setTab']/li"));

        String[] listNames = new String[] {"新手入门", "设置管理", "我要理财", "资金管理", "疑难解答", "投资券使用规则"};
        assertTrue("常见问题应该有六大类: " + list.size(), list.size() == listNames.length);

        for (int i = 0; i < list.size(); i++) {
            assertTrue("类别名称不正确: " + list.get(i).getText(), list.get(i).getText().equalsIgnoreCase(listNames[i]));
        }
	}

	@Test
	public void 媒体报道() {
        this.selectSubMenu("媒体报道");

        List<WebElement> list = this.driver.findElements(By.xpath("//div[contains(@class, 'about_td')]/ul/li"));
        assertTrue("媒体报道页面不能为空", list.size() > 0);

        for (WebElement item : list) {
            String content = item.findElement(By.className("mt_txt")).getText();
            assertTrue("媒体新闻不能为空", content.trim().length() > 0);
        }
	}

	@Test
	public void 公司动态() {
        this.selectSubMenu("公司动态");

        List<WebElement> list = this.driver.findElements(By.xpath("//div[@class='newslist']/ul/li"));
        assertTrue("公司动态页面不能为空", list.size() > 0);
	}

	@Test
	public void 最新公告() {
        this.selectSubMenu("最新公告");

        List<WebElement> list = this.driver.findElements(By.xpath("//div[@class='newslist']/ul/li"));
        assertTrue("最新公告页面不能为空", list.size() > 0);
	}
}
