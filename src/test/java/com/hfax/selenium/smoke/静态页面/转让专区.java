package com.hfax.selenium.smoke.静态页面;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

/**
 * 
 * @author changwj
 * @Description 导航栏转让专区，需要标的
 */
public class 转让专区 extends BaseTest {
	@Test
	public void 转让专区未登录() {
		this.loadPage();

        this.navigateToPage("转让专区");
        List<WebElement> list = this.driver.findElements(By.className("listTabBox"));
        assertTrue("转让专区内容不能为空", list.size() > 0);
	}
}
