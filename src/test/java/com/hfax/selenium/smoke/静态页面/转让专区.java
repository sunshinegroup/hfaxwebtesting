package com.hfax.selenium.smoke.静态页面;

import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

/**
 * @Description 转让专区静态页面验证
 * Created by songxq on 25/4/16.
 */
public class 转让专区 extends BaseTest {

	@Override
	public void setup() throws Exception {
		super.setup();
        this.loadPage();
        this.navigateToPage("转让专区");
	}
	@Test
	public void 转让专区未登录() {
        List<WebElement> list = this.driver.findElements(By.className("listTabBox"));
        assertTrue("转让专区内容不能为空", list.size() > 0);
	}
	//验证惠金所logo
		@Test
		public void 转让专区惠金所logo(){
			this.wait.until(new ExpectedCondition<WebElement>() {
				@Override
				public WebElement apply(WebDriver webDriver) {
					return webDriver.findElement(By.xpath("//a[contains(@href,'toIndexListInit')]"));
				}
			});	
			this.driver.findElements(By.xpath("//a[contains(@href,'toIndexListInit')]")).get(0).click();
			WebElement logo = this.driver.findElements(By.xpath("//a[contains(@href,'toIndexListInit')]")).get(0);
			assertTrue("惠金所logo存在",logo.isDisplayed());
	  }
    
	//验证转让专区的产品链接
		@Ignore
		@Test
		public void 转让专区验证产品列表链接(){
			this.wait.until(new ExpectedCondition<WebElement>() {
				@Override
				public WebElement apply(WebDriver webDriver) {
					return webDriver.findElement(By.xpath("//div[@class='listMainBox attorn on']"));
				}
			});		
			List<WebElement> list = this.driver.findElements(By.xpath("//a[contains(text(),'惠理财-转让')]"));
			assertTrue("产品个数不能为0",list.size() > 0 );
			for(int i = 1; i <=list.size(); i++)
			{ 
				String s = String.valueOf(i);			
	     		String currentWindow = this.driver.getWindowHandle();//获取当前窗口句柄
	     		this.driver.findElement(By.xpath("html/body/div[3]/div[2]/div/div["+ s +"]/div[1]/h3/a")).click();
	    		Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
				Iterator<String> it = handles.iterator();
				while (it.hasNext()) {
				if (currentWindow == it.next()) {
					continue;
					}
				WebDriver window = this.driver.switchTo().window(it.next());//切换到新窗口
				WebElement newzone = this.driver.findElement(By.xpath("//div[@class='balanceTitle']"));
				assertTrue("产品列表正常跳转",newzone.isDisplayed());
				window.close();//关闭新窗口
				}
				this.driver.switchTo().window(currentWindow);//回到原来页面
				
			}
		}
		
		//验证转让专区的立即投资和转让成功按钮和链接
		@Test
		public void 转让专区立即投资和转让成功(){
			List<WebElement> list = this.driver.findElements(By.className("listTabBox"));
			assertTrue("产品个数不能为0",list.size() > 0 );
			for(int i = 1; i <=list.size(); i++)
			{ 
				String s = String.valueOf(i);			
	     		String text = this.driver.findElement(By.xpath("html/body/div[3]/div[2]/div/div[" + s + "]/div[2]/ul[2]/li/a")).getAttribute("text");
	    		if(text.trim().equals("立即投资"))
	    		{
	    			String currentWindow = this.driver.getWindowHandle();//获取当前窗口句柄
		    		this.driver.findElement(By.xpath("html/body/div[3]/div[2]/div/div[" + s + "]/div[2]/ul[2]/li/a")).click();
	    			Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
					Iterator<String> it = handles.iterator();
					while (it.hasNext()) {
					if (currentWindow == it.next()) {
						continue;
						}
					WebDriver window = this.driver.switchTo().window(it.next());//切换到新窗口
					this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='balanceTitle']")));
					WebElement newzone = this.driver.findElement(By.xpath("//div[@class='balanceTitle']"));
					assertTrue("立即投资正常跳转",newzone.isDisplayed());
					window.close();//关闭新窗口
					}
					this.driver.switchTo().window(currentWindow);//回到原来页面
	    		}
	    		else if(text.trim().equals("转让成功"))
	    		{
	    			String currentWindow = this.driver.getWindowHandle();//获取当前窗口句柄
	    			this.driver.findElement(By.xpath("html/body/div[3]/div[2]/div/div[" + s + "]/div[2]/ul[2]/li/a")).click();
	    			Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
					Iterator<String> it = handles.iterator();
					while (it.hasNext()) {
					if (currentWindow == it.next()) {
						continue;
						}
					WebDriver window = this.driver.switchTo().window(it.next());//切换到新窗口
					this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='balanceTitle']")));
					WebElement newzone = this.driver.findElement(By.xpath("//div[@class='balanceTitle']"));
					assertTrue("转让成功正常跳转",newzone.isDisplayed());
					window.close();//关闭新窗口
					}
					this.driver.switchTo().window(currentWindow);//回到原来页面
	    			
	    		}	
	    		
			}
		}
	
}
