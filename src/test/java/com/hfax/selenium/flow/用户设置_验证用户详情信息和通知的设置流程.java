package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import com.hfax.selenium.regression.account.settings.用户详情信息;
import com.hfax.selenium.regression.account.settings.通知设置;
/**
 * @Description 用户设置_验证用户详情信息和通知的设置流程 
 * Created by songxq on 24/8/16.
 */


@RunWith(Suite.class)
@SuiteClasses({
	用户详情信息.class,
	通知设置.class
})
public class 用户设置_验证用户详情信息和通知的设置流程 {

}
