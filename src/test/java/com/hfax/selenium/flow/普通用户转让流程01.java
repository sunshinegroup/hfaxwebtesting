package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.transfer.投资转让_可转让的项目查看;
import com.hfax.selenium.regression.account.transfer.投资转让_可转让的项目转让;
import com.hfax.selenium.regression.account.transfer.投资转让_已转入的项目查看;
import com.hfax.selenium.regression.account.transfer.投资转让_已转出的项目查看;
import com.hfax.selenium.regression.account.transfer.投资转让_用户投资惠理财;
import com.hfax.selenium.regression.account.transfer.投资转让_用户投资转让专区;
import com.hfax.selenium.regression.account.transfer.投资转让_用户注册_交易密码_绑卡_充值;
import com.hfax.selenium.regression.account.transfer.投资转让_转让中的项目查看;
import com.hfax.selenium.regression.business.企业借款统计;
import com.hfax.selenium.regression.business.企业已发布的借款;
import com.hfax.selenium.regression.business.企业注册;
import com.hfax.selenium.regression.business.企业还款管理;
import com.hfax.selenium.regression.business.企业验证账号信息;
import com.hfax.selenium.regression.console.企业用户审核;
import com.hfax.selenium.regression.console.企业资产录入;
import com.hfax.selenium.regression.console.可转让标的过审;
import com.hfax.selenium.regression.console.资产录入;
import com.hfax.selenium.regression.console.过审查询;
import com.hfax.selenium.regression.console.项目发布审核;
import com.hfax.selenium.regression.console.项目审核;
import com.hfax.selenium.regression.console.项目查询;
import com.hfax.selenium.regression.console.截标;
import com.hfax.selenium.flow.*;
/**
 * 
 * @author songxq
 * @Description  普通用户转让流程
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
 //A用户
//    普通用户注册绑卡充值流程.class,
//B用户
   投资转让_用户注册_交易密码_绑卡_充值.class,
    资产录入.class,
    项目审核.class,
    可转让标的过审.class,
    项目发布审核.class,
 //A用户
    投资转让_用户投资惠理财.class,
    截标.class,
 //执行修改锁定期的sql 
 //cleanup_updatelockdate.bat
 
})
public class 普通用户转让流程01 {}

