package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资新手专区产品;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资理财产品;
/**
 * 
 * @author songxq
 * @Description 普通用户投资新手专区流程
 */
@RunWith(Suite.class)
@SuiteClasses({
	新手标发标流程.class,
	 我的投资.class,
	 我的投资_惠理财_投资新手专区产品.class
	//执行cleanup_new_project清库脚本
})
public class 普通用户投资新手专区流程 {

}
