package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.hfax.selenium.regression.console.新手标过审;
import com.hfax.selenium.regression.console.资产录入;
import com.hfax.selenium.regression.console.项目发布审核;
import com.hfax.selenium.regression.console.项目审核;

/**
 * 
 * @author luzhipeng
 * @Description  新手标发标流程
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        资产录入.class,
        项目审核.class,
        新手标过审.class,
        项目发布审核.class})
public class 新手标发标流程 {

}
