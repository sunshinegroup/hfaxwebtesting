package com.hfax.selenium.flow;

import com.hfax.selenium.regression.business.*;
import com.hfax.selenium.regression.console.企业用户审核;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import com.hfax.selenium.regression.console.*;


/**
 * Created by luzhipeng on 4/25/16.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        企业注册.class,
        企业验证账号信息.class,
        企业用户审核.class,
        企业资产录入.class,
        项目审核.class,
        过审查询.class,
        项目发布审核.class
})
public class 企业用户注册流程 {}