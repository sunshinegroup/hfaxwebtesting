package com.hfax.selenium.flow;

import com.hfax.selenium.essential.BangKa;
import com.hfax.selenium.essential.ChongZhi;
import com.hfax.selenium.essential.JiaoYiMiMa;
import com.hfax.selenium.essential.ZhuCe;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * 必跑测试案例, 必须清库后先跑, 会创建账户, 身份认证, 设置交易密码, 绑卡, 充值
 *
 * Created by philip on 3/10/16.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ZhuCe.class,BangKa.class,JiaoYiMiMa.class,ChongZhi.class})
public class 普通用户注册绑卡充值流程 {
}
