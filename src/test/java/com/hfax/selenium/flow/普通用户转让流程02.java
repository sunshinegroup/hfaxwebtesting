package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.transfer.投资转让_可转让的项目查看;
import com.hfax.selenium.regression.account.transfer.投资转让_可转让的项目转让;
import com.hfax.selenium.regression.account.transfer.投资转让_已转入的项目查看;
import com.hfax.selenium.regression.account.transfer.投资转让_已转出的项目查看;
import com.hfax.selenium.regression.account.transfer.投资转让_用户投资转让专区;
import com.hfax.selenium.regression.account.transfer.投资转让_用户注册_交易密码_绑卡_充值;
import com.hfax.selenium.regression.account.transfer.投资转让_转让中的项目查看;

@RunWith(Suite.class)
@SuiteClasses({
 //A用户
 投资转让_可转让的项目查看.class,
投资转让_可转让的项目转让.class,
 投资转让_转让中的项目查看.class,
 //B用户
投资转让_用户投资转让专区.class,    
投资转让_已转入的项目查看.class,
 //A用户
   投资转让_已转出的项目查看.class
 //执行清除用户A，B的清库脚本
 //cleanup_tranferuser.bat  
})
public class 普通用户转让流程02 {

}
