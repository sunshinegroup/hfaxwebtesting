package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.settings.充值后银行卡的设置;
/**
 * @Description 用户设置_充值后银行卡的设置
 * Created by songxq on 29/8/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	充值后银行卡的设置.class
})
public class 用户设置_充值后银行卡的设置流程 {

}
