package com.hfax.selenium.flow;

import com.hfax.selenium.regression.account.充值提现卡变更流程.挡板充值;
import com.hfax.selenium.regression.account.充值提现卡变更流程.注册;
import com.hfax.selenium.regression.account.充值提现卡变更流程.绑卡;
import com.hfax.selenium.regression.account.充值提现卡变更流程.重置交易密码;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * 必跑测试案例, 必须清库后先跑, 会创建账户, 身份认证, 设置交易密码, 绑卡, 充值
 *
 * Created by philip on 3/10/16.
 *
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({注册.class, 
	重置交易密码.class, 
	绑卡.class, 
	挡板充值.class})
public class 普通用户注册绑卡充值流程_挡板 {
}
