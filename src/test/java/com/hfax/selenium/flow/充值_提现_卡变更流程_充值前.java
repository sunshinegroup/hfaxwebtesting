package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import com.hfax.selenium.regression.account.充值提现卡变更流程.卡变更流程_充值2;
import com.hfax.selenium.regression.account.充值提现卡变更流程.卡变更流程_注册;
import com.hfax.selenium.regression.account.充值提现卡变更流程.卡变更流程_绑卡;
import com.hfax.selenium.regression.account.充值提现卡变更流程.卡变更流程_重置交易密码;

/**
 *
 * 必跑测试案例, 必须清库后先跑, 会创建账户, 身份认证, 设置交易密码, 绑卡, 充值
 *
 * Created by wangmingqiang on 5/25/16.
 *涉及提现、卡变更、卡变更后充值、忘记密码该流程顺序不能改变
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
	卡变更流程_注册.class,
	卡变更流程_重置交易密码.class,
	卡变更流程_绑卡.class,
	卡变更流程_充值2.class
})
public class 充值_提现_卡变更流程_充值前 {
}
