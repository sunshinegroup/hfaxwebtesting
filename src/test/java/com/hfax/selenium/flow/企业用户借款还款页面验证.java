package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.hfax.selenium.regression.business.企业借款统计;
import com.hfax.selenium.regression.business.企业已发布的借款;
import com.hfax.selenium.regression.business.企业还款管理;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        企业还款管理.class,
        企业借款统计.class,
        企业已发布的借款.class
})
public class 企业用户借款还款页面验证 {

}
