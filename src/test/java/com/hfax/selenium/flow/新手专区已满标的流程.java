package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资新手专区的产品全投;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_搜索已满标的产品;
/**
 * @Description 新手专区已满标的流程
 * Created by songxq on 27/5/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	    //执行新手表的清库脚本，cleanup_new_project.sh
		//执行清标脚本cleanup_project.sh
		新手标发标流程.class,
		我的投资_惠理财_投资新手专区的产品全投.class,
		我的投资_惠理财_搜索已满标的产品.class
})
public class 新手专区已满标的流程 {

}
