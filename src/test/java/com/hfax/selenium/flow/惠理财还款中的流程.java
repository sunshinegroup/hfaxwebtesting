package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资理财产品;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资理财剩余金额全投;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_搜索还款中的产品;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财项目的截标;
import com.hfax.selenium.regression.business.企业还款管理_惠理财;

/**
 * @Description 惠理财还款中的流程
 * Created by songxq on 27/5/16.
 * Modify by songxq on 8/23/2016
 */
@RunWith(Suite.class)
@SuiteClasses({
	  //执行清标脚本cleanup_project.sh
		 普通标的发标流程.class,
		 我的投资_惠理财_投资理财产品.class,
		 我的投资_惠理财_投资理财剩余金额全投.class, 
		我的投资_惠理财项目的截标.class,
		 我的投资_惠理财_搜索还款中的产品.class,
		 企业还款管理_惠理财.class
})
public class 惠理财还款中的流程 {

}
