package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import com.hfax.selenium.regression.account.settings.交易密码管理;
import com.hfax.selenium.regression.account.settings.登录密码管理;
/**
 * @Description 用户设置_验证登录密码和交易密码管理的流程 
 * Created by songxq on 24/8/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	登录密码管理.class,
	交易密码管理.class
})
public class 用户设置_验证登录密码和交易密码管理的流程 {

}
