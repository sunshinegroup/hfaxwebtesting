package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资_惠聚财_投资惠聚财的产品全投;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠聚财_搜索已满标的产品;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠聚财_搜索还款中的产品;
import com.hfax.selenium.regression.console.惠聚财_产品上架;
import com.hfax.selenium.regression.console.惠聚财_产品审核;
import com.hfax.selenium.regression.console.惠聚财_产品录入;
import com.hfax.selenium.regression.console.惠聚财_募集成功;
/**
 * @Description  惠聚财企业还款流程
 * Created by songxq on 2/9/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	惠聚财_产品录入.class,
	惠聚财_产品审核.class,
	惠聚财_产品上架.class,
	我的投资_惠聚财_投资惠聚财的产品全投.class,
	我的投资_惠聚财_搜索已满标的产品.class,
	惠聚财_募集成功.class,
	我的投资_惠聚财_搜索还款中的产品.class,
	
})
public class 惠聚财企业用户还款流程01 {

}
