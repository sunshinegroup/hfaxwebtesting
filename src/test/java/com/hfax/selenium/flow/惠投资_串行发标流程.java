package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.hfax.selenium.regression.console.惠投资_串行产品上架;
import com.hfax.selenium.regression.console.惠投资_产品批量审核;
import com.hfax.selenium.regression.console.惠投资_产品批量录入;
import com.hfax.selenium.regression.console.惠投资_设置串行属性;
import com.hfax.selenium.regression.console.惠投资_设置串行规则;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	惠投资_产品批量录入.class,
	惠投资_产品批量审核.class,
	惠投资_串行产品上架.class,
	惠投资_设置串行属性.class,
	惠投资_设置串行规则.class
})
public class 惠投资_串行发标流程 {}
