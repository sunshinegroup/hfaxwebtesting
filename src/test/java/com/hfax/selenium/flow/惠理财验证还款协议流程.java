package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.惠理财还款协议验证页;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资理财产品;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资理财剩余金额全投;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财项目的截标;
/**
 * @Description 惠理财验证还款协议流程
 * Created by songxq on 10/8/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	 普通标的发标流程.class,
	 我的投资_惠理财_投资理财产品.class,
	 我的投资_惠理财_投资理财剩余金额全投.class, 
	我的投资_惠理财项目的截标.class,
	惠理财还款协议验证页.class
})
public class 惠理财验证还款协议流程 {

}
