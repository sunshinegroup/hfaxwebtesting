package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资理财产品;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资理财剩余金额全投;
import com.hfax.selenium.regression.console.资产录入;
import com.hfax.selenium.regression.console.过审查询;
import com.hfax.selenium.regression.console.项目发布审核;
import com.hfax.selenium.regression.console.项目审核;
import com.hfax.selenium.regression.console.项目查询;
/**
 * 
 * @author songxq
 * @Description  普通用户投资理财流程
 */
@RunWith(Suite.class)
@SuiteClasses({
            普通标的发标流程.class,
             我的投资.class,
             我的投资_惠理财_投资理财产品.class,
             我的投资_惠理财_投资理财剩余金额全投.class
})
public class 普通用户投资理财流程 {
   
}
