package com.hfax.selenium.flow;

import com.hfax.selenium.regression.console.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * 
 * @author luzhipeng
 * @Description  可转让可转让标的发标流程的过审
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        资产录入.class,
        项目审核.class,
        可转让标的过审.class,
        项目发布审核.class,
        项目查询.class})
public class 可转让标的发标流程 {}