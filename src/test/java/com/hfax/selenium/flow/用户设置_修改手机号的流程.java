package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.settings.更换手机;
/**
 * @Description 用户设置_修改手机号的流程
 * Created by songxq on 29/8/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	更换手机.class
})
public class 用户设置_修改手机号的流程 {

}
