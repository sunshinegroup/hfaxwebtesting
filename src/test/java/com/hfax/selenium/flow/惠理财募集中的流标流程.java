package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资理财产品;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资理财剩余金额全投;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_搜索流标中的产品;
import com.hfax.selenium.regression.console.流标;
/**
 * @Description 惠理财募集中的流标流程
 * Created by songxq on 30/5/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	//执行清标脚本cleanup_project.sh
	普通标的发标流程.class,
             我的投资_惠理财_投资理财产品.class,
             流标.class,
             我的投资_惠理财_搜索流标中的产品.class        
})
public class 惠理财募集中的流标流程 {

}
