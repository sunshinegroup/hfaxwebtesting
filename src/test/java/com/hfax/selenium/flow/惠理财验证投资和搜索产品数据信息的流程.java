package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.惠理财投资产品信息验证页;
/**
 * @Description 惠理财验证投资和搜索产品数据信息的流程
 * Created by songxq on 9/8/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	 普通标的发标流程.class,
	 惠理财投资产品信息验证页.class
})
public class 惠理财验证投资和搜索产品数据信息的流程 {

}
