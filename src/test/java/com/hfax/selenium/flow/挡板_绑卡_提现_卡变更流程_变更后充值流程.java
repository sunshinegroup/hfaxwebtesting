package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import com.hfax.selenium.regression.account.充值提现卡变更流程.卡变更流程_挡板充值;
import com.hfax.selenium.regression.account.充值提现卡变更流程.卡变更流程_注册;
import com.hfax.selenium.regression.account.充值提现卡变更流程.卡变更流程_绑卡;
import com.hfax.selenium.regression.account.充值提现卡变更流程.卡变更流程_重置交易密码;
import com.hfax.selenium.regression.account.充值提现卡变更流程.忘记密码;
import com.hfax.selenium.smoke.静态页面.首页;

/**
 *
 * 必跑测试案例, 必须清库后先跑, 会创建账户, 身份认证, 设置交易密码, 绑卡, 充值
 *
 * Created by wangmingqiang on 4/29/16.
 *涉及提现、卡变更、卡变更后充值、忘记密码该流程顺序不能改变
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
	卡变更流程_注册.class,
	卡变更流程_重置交易密码.class,
	卡变更流程_绑卡.class,
//	提现挡板目前为开发完成，暂时不支持挡板提现
//	卡变更流程_全额提现.class,
//无法进行挡板全额提现，不能进行卡变更
//	卡变更流程_卡变更.class,
	首页.class,
	卡变更流程_挡板充值.class,
	忘记密码.class	
})
public class 挡板_绑卡_提现_卡变更流程_变更后充值流程 {
}
