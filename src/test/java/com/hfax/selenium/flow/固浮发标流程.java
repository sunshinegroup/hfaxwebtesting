package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.hfax.selenium.regression.account.my_investment.我的投资_惠投资_投资惠投资产品全投;
import com.hfax.selenium.regression.business.企业还款管理_惠投资;
import com.hfax.selenium.regression.console.惠投资_产品上架;
import com.hfax.selenium.regression.console.惠投资_产品审核;
import com.hfax.selenium.regression.console.惠投资_产品录入;
import com.hfax.selenium.regression.console.惠投资_募集成功;
import com.hfax.selenium.regression.console.惠投资_实际还款;
import com.hfax.selenium.regression.console.惠投资_还款计划复核;
import com.hfax.selenium.regression.console.惠投资_还款计划录入;
import com.hfax.selenium.regression.console.惠投资_还款计划编辑;



@RunWith(Suite.class)
@Suite.SuiteClasses({
	惠投资_产品录入.class,
	惠投资_产品审核.class,
	惠投资_产品上架.class,
	我的投资_惠投资_投资惠投资产品全投.class,
	惠投资_募集成功.class,
	惠投资_还款计划录入.class,
	惠投资_还款计划编辑.class,
	惠投资_实际还款.class,
	惠投资_还款计划复核.class,
	企业还款管理_惠投资.class
	})
public class 固浮发标流程 {

}
