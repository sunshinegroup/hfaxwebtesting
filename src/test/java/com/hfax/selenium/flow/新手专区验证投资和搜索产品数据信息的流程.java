package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.新手专区投资产品信息验证页;

/**
 * @Description 新手专区验证投资和搜索产品数据信息的流程
 * Created by songxq on 9/8/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	新手标发标流程.class,
	新手专区投资产品信息验证页.class,
})
public class 新手专区验证投资和搜索产品数据信息的流程 {

}
