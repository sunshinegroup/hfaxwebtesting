package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资_惠投资_投资前账户金额;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠投资_投资后账户金额;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠投资_投资惠投资的产品;
import com.hfax.selenium.regression.console.惠投资_产品上架;
import com.hfax.selenium.regression.console.惠投资_产品审核;
import com.hfax.selenium.regression.console.惠投资_产品录入;


/**
 * @Description 惠投资投资前后账户金额验证流程
 * Created by songxq on 21/6/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	//cleanup_tranferuser.bat
	惠投资_产品录入.class,
	惠投资_产品审核.class,
	惠投资_产品上架.class,
	我的投资_惠投资_投资前账户金额.class,
	我的投资_惠投资_投资惠投资的产品.class,
	我的投资_惠投资_投资后账户金额.class
	
})
public class 惠投资投资前后账户金额验证流程 {

}
