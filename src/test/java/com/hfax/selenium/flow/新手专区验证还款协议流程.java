package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财_投资新手专区的产品全投;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠理财项目的截标;
import com.hfax.selenium.regression.account.my_investment.新手专区还款协议验证页;
/**
 * @Description 新手专区验证还款协议流程
 * Created by songxq on 10/8/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	新手标发标流程.class,
	我的投资_惠理财_投资新手专区的产品全投.class,
	我的投资_惠理财项目的截标.class,
	新手专区还款协议验证页.class
})
public class 新手专区验证还款协议流程 {

}
