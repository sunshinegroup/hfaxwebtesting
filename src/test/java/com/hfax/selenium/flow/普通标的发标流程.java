package com.hfax.selenium.flow;

import com.hfax.selenium.regression.console.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by philip on 4/13/16.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        资产录入.class,
        项目审核.class,
        过审查询.class,
        项目发布审核.class,
        项目查询.class})
public class 普通标的发标流程 {}