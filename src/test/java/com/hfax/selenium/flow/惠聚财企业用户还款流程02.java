package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.business.企业还款管理_惠聚财;
import com.hfax.selenium.regression.console.惠聚财_综合管理系统;
/**
 * @Description  惠聚财企业还款流程
 * Created by songxq on 2/9/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	//执行yue_insert.sh
	惠聚财_综合管理系统.class,
	企业还款管理_惠聚财.class
})
public class 惠聚财企业用户还款流程02 {

}
