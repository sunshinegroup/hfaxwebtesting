package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.我的投资_惠投资_投资惠投资产品全投;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠投资_搜索已满标的产品;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠投资_搜索还款中的产品;
import com.hfax.selenium.regression.console.惠投资_产品上架;
import com.hfax.selenium.regression.console.惠投资_产品审核;
import com.hfax.selenium.regression.console.惠投资_产品录入;
import com.hfax.selenium.regression.console.惠投资_募集成功;
/**
 * 
 * @author songxq
 * @Description 惠投资还款中的流程
 */
@RunWith(Suite.class)
@SuiteClasses({
	惠投资_产品录入.class,
	惠投资_产品审核.class,
	惠投资_产品上架.class,
	我的投资_惠投资_投资惠投资产品全投.class,
	我的投资_惠投资_搜索已满标的产品.class,
	惠投资_募集成功.class,
	我的投资_惠投资_搜索还款中的产品.class
})
public class 惠投资还款中的流程 {

}
