package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.惠投资还款协议验证页;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠投资_投资惠投资产品全投;
import com.hfax.selenium.regression.account.my_investment.我的投资_惠投资_搜索已满标的产品;
import com.hfax.selenium.regression.console.惠投资_产品上架;
import com.hfax.selenium.regression.console.惠投资_产品审核;
import com.hfax.selenium.regression.console.惠投资_产品录入;
import com.hfax.selenium.regression.console.惠投资_募集成功;
/**
 * @Description 惠投资验证还款协议流程
 * Created by songxq on 10/8/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	惠投资_产品录入.class,
	惠投资_产品审核.class,
	惠投资_产品上架.class,
	我的投资_惠投资_投资惠投资产品全投.class,
	我的投资_惠投资_搜索已满标的产品.class,
	惠投资_募集成功.class,
	惠投资还款协议验证页.class
})
public class 惠投资验证还款协议流程 {

}
