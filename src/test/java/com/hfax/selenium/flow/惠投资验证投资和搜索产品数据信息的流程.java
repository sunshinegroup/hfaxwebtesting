package com.hfax.selenium.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.hfax.selenium.regression.account.my_investment.惠投资投资产品信息验证页;
import com.hfax.selenium.regression.console.惠投资_产品上架;
import com.hfax.selenium.regression.console.惠投资_产品审核;
import com.hfax.selenium.regression.console.惠投资_产品录入;
/**
 * @Description 惠投资验证投资和搜索产品数据信息的流程
 * Created by songxq on 9/8/16.
 */
@RunWith(Suite.class)
@SuiteClasses({
	惠投资_产品录入.class,
	惠投资_产品审核.class,
	惠投资_产品上架.class,
	惠投资投资产品信息验证页.class
})
public class 惠投资验证投资和搜索产品数据信息的流程 {

}
