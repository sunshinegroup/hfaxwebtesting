package com.hfax.selenium.base;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Ops extends BaseTest{
	/**
	 * @throws InterruptedException 
	 * @Description 跑P的流程
	 * Created by songxq on 6/7/16.
	 */

	@Test
	public void 跑批脚本() throws InterruptedException {
		//打开跑P链接
		 this.driver.get(this.opsUrl);
		 
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath(".//*[@id='lg']")).isDisplayed();
	            }
	        });
		 //输入用户名
		 this.driver.findElement(By.xpath(".//*[@id='userid']")).sendKeys(this.opsUserName);
		 //输入密码
		 this.driver.findElement(By.xpath(".//*[@id='passwd']")).sendKeys(this.opsPassWord);
		
		 //重置
		 this.driver.findElement(By.xpath(".//*[@id='cz']")).click();
		 
		 
		 //等待弹出框
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.linkText("确定")).isDisplayed();
	            }
	        });
		 //点击确定
		 this.driver.findElement(By.linkText("确定")).click();
		 
		 Thread.sleep(2000);
		 
		 //登录
		 this.driver.findElement(By.xpath(".//*[@id='lg']")).click();
		 
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.linkText("日终批量")).isDisplayed();
	            }
	        });
		 //点击日终批量
		 this.driver.findElement(By.linkText("日终批量")).click();
		 
		 //等待核心换日前流程
         this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	driver.switchTo().defaultContent();
	            	driver.switchTo().frame(0);
	                return webDriver.findElement(By.xpath(".//*[@id='dayTree']/li[1]/div/span[3]")).isDisplayed();
	            }
	        });
		 //点击核心换日前流程
		 this.driver.findElement(By.xpath(".//*[@id='dayTree']/li[1]/div/span[3]")).click();
		 
		 Thread.sleep(3000);
		 List<WebElement> frames = driver.findElements(By.tagName("iframe"));
		 for(WebElement frame:frames){
			 this.driver.switchTo().frame(frame);
			 if(driver.getPageSource().contains("提交"))
			 {
				 new WebDriverWait(this.driver,3).until(new ExpectedCondition<Boolean>() {
			            @Override
			            public Boolean apply(WebDriver webDriver) {
			            	return webDriver.findElement(By.xpath(".//*[@id='dcntoolbar']/span[1]/a[1]/span/span")).isDisplayed();
			            }
			        });
				 //点击提交
				this.driver.findElement(By.xpath(".//*[@id='dcntoolbar']/span[1]/a[1]/span/span")).click();
				 break;
			 }else{
				 this.driver.switchTo().defaultContent();
			 }
		 }
		 driver.switchTo().defaultContent();
     	 driver.switchTo().frame(0);
     	 //等待确认提示框
		 new WebDriverWait(this.driver,5).until(ExpectedConditions.presenceOfElementLocated(By.linkText("确定")));
		 //点击确定
	     this.driver.findElement(By.linkText("确定")).click();

	     //等待核心换日及换日后流程
         this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	driver.switchTo().defaultContent();
	            	driver.switchTo().frame(0);
	                return webDriver.findElement(By.xpath(".//*[@id='dayTree']/li[2]/div/span[3]")).isDisplayed();
	            }
	        });
		 //点击核心换日及换日后流程
		 this.driver.findElement(By.xpath(".//*[@id='dayTree']/li[2]/div/span[3]")).click();
		 //点击核心换日前流程
		 this.driver.findElement(By.xpath(".//*[@id='dayTree']/li[1]/div/span[3]")).click();
		 
		 List<WebElement> frames1 = driver.findElements(By.tagName("iframe"));
		 for(WebElement frame1:frames1){
			 this.driver.switchTo().frame(frame1);
			 if(driver.getPageSource().contains("提交"))
			 {
				 new WebDriverWait(this.driver,3).until(new ExpectedCondition<Boolean>() {
			            @Override
			            public Boolean apply(WebDriver webDriver) {
			            	return webDriver.findElement(By.xpath(".//*[@id='dcntoolbar']/span[1]/a[1]/span/span")).isDisplayed();
			            }
			        });
				 //点击提交
				this.driver.findElement(By.xpath(".//*[@id='dcntoolbar']/span[1]/a[1]/span/span")).click();
				 break;
			 }else{
				 this.driver.switchTo().defaultContent();
			 }
		 }
		 
		 driver.switchTo().defaultContent();
     	 driver.switchTo().frame(0);
     	 //等待确认提示框
		 new WebDriverWait(this.driver,5).until(ExpectedConditions.presenceOfElementLocated(By.linkText("确定")));
		 //点击确定
	     this.driver.findElement(By.linkText("确定")).click();
	     
	     //人工等待
	     Thread.sleep(30000);
	     
	     List<WebElement> frames2 = driver.findElements(By.tagName("iframe"));
		 for(WebElement frame1:frames2){
			 this.driver.switchTo().frame(frame1);
			 if(driver.getPageSource().contains("分钟后开始换日"))
			 {
				 new WebDriverWait(this.driver,3).until(new ExpectedCondition<Boolean>() {
			            @Override
			            public Boolean apply(WebDriver webDriver) {
			            	return webDriver.findElement(By.xpath(".//*[@id='changetime']")).isDisplayed();
			            }
			        });
				//输入1分钟后开始换日
				 this.driver.findElement(By.xpath(".//*[@id='changetime']")).clear();
				 this.driver.findElement(By.xpath(".//*[@id='changetime']")).sendKeys("1");
				 new WebDriverWait(this.driver,3).until(new ExpectedCondition<Boolean>() {
			            @Override
			            public Boolean apply(WebDriver webDriver) {
			            	return webDriver.findElement(By.xpath(".//*[@id='dcntoolbar']/span[1]/a[1]/span/span")).isDisplayed();
			            }
			        });
				 //点击提交
				 this.driver.findElement(By.xpath(".//*[@id='dcntoolbar']/span[1]/a[1]/span/span")).click();
				 break;
			 }else{
				 this.driver.switchTo().defaultContent();
			 }
		 }
		 driver.switchTo().defaultContent();
     	 driver.switchTo().frame(0);
     	 //等待确认提示框
		 new WebDriverWait(this.driver,5).until(ExpectedConditions.presenceOfElementLocated(By.linkText("确定")));
		 //点击确定
	     this.driver.findElement(By.linkText("确定")).click();
		 //人工等待
	     Thread.sleep(70000);
	}
}
