package com.hfax.selenium.base;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 *
 * Util class for various common functions
 *
 * Created by philip on 3/8/16.
 *
 */
public class Util {
    private static int getNum(int start,int end) {
        return (int)(Math.random()*(end-start+1)+start);
    }

    private static String[] telFirst="134,135,136,137,138,139,150,151,152,157,158,159,130,131,132,155,156,133,153".split(",");

    public static String getPhoneNumber() {
        int index=getNum(0,telFirst.length-1);
        String first=telFirst[index];
        String second=String.valueOf(getNum(1,888)+10000).substring(1);
        String third=String.valueOf(getNum(1,9100)+10000).substring(1);
        return first+second+third;
    }

    public static String getRandomName() throws Exception {
        String ret="";
        for(int i=0;i<3;i++){
            String str = null;
            int hightPos, lowPos; // 定义高低位
            Random random = new Random();
            hightPos = (176 + Math.abs(random.nextInt(39))); //获取高位值
            lowPos = (161 + Math.abs(random.nextInt(93))); //获取低位值
            byte[] b = new byte[2];
            b[0] = (new Integer(hightPos).byteValue());
            b[1] = (new Integer(lowPos).byteValue());

            ret+=new String(b, "GBk");
        }
        return ret;
    }

    public static String getRandomIdNumber() {
        return new IdCardGenerator().generate();
    }

    public static String getDecimalFormat(String numberString) {
        DecimalFormat formatter = new DecimalFormat("###,###,###.00");
        return formatter.format(new Double(numberString));
    }

    /**
     * @author changwj
     * @param dayAmount 距离现在的天数 昨天-1   明天+1
     * @param formatStr 转换成的日期字符串格式  如2016-05-07    yyyy-MM-dd
     * @return 计算后的按格式转换好的日期字符串信息
     */
    public static String assignTime(int dayAmount, String formatStr){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, dayAmount);
        Date time = calendar.getTime();

        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat(formatStr);
        String format = simpleDateFormat.format(time);

        return format;                      
    }
}
