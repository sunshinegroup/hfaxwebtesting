package com.hfax.selenium.base;

import java.io.BufferedReader;



import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hfax.selenium.regression.business.BusinessBaseTest;
import com.hfax.selenium.rule.BaseRule;

import org.apache.commons.exec.OS;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.openqa.selenium.*;
import java.net.URL;
import org.openqa.selenium.remote.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Base class for all junit derived tests
 * <p>
 * Config file is read from webDriver.properties and exposed through protected
 * variables. Several common instance methods are also implemented here.
 * <p>
 * Created by philip on 3/8/16.
 * Modify by songxq on 15/11/16
 * Modify by songxq on 2/5/17
 */
public class BaseTest {
    protected WebDriver driver; // Internal browser driver
    protected FluentWait<WebDriver> wait;
    protected String url; // Base url for target test host
    protected String backupUrl; // Secondary base url for making deposit (常亮前置写死)
    protected String username; // Account username
    protected String password; // Account password
    protected String jiaoyipassword; //trade password
    protected String phone; // Account holder phone #
    protected String identification; // Account holder id #
    protected String name; // Account holder name (中文)
    protected String card; // Account holder bank card (只能绑一张)
    protected String amount; // 充值金额

    protected String consoleUrl; // 运营前台地址
    protected String consoleUsername; // 运营登陆账户
    protected String consolePassword; // 运营登陆密码
    protected String consoleProjectName; // 标地名字
    protected String consoleRate; // 标利率

    protected String businessRegistrationUrl; // 企业注册地址
    protected String businessUsername; // 企业用户名
    protected String businessPassword; // 企业密码
    protected String businessName; // 企业名字
    protected String businessLegalName; // 企业法人名字
    protected String businessIdentification; // 企业法人身份证号
    protected String businessCode; // 企业组织代码
    protected String businessLicense; // 企业营业执照号
    protected String businessPermit; // 企业许可证号
    protected String businessPhone;//企业手机号
    protected String businessAmount; //企业用户充值金额
    protected String businessBankCard; //企业用户银行卡号

    protected String bankname; // 初始银行卡银行名称
    protected String bankid; // 初始银行ID
    protected String recard; // 变更银行卡号
    protected String rebankname; // 变更银行名称
    protected String rebankid; // 变更银行ID
    protected String repassword; //充值密码
    
    protected String transferusername; //转让用户名
	protected String transferpassword; //转让用户密码
	protected String transferjiaoyipassword;//转让用户交易密码
	protected String transferphone;  //转让用户电话
	protected String transferidentification; //转让用户身份证号
	protected String transfername;  //转让用户姓名
	protected String transfercard;  //转让用户银行卡号
	protected String transferamount; //转让用户金额
	
	protected String userinvestamount;//普通用户投资金额
	protected String userremaininginvestamount;//普通用户剩余投资金额
	protected String userallinvestamount;//普通用户全额投资金额
	
    protected String huiProjectName; //惠投资项目名称
    protected String huiBatchName;
    protected String huiProAmount; //惠投资项目金额
    protected String huiYearRate; //惠投资预期年化收益率
    protected String huiDay; //惠投资投资期限
    protected String huiAmount; //惠投资起投金额
    protected String huiExpectRate; //惠投资预期收益
    
    protected String opsUrl;//跑P的地址
    protected String opsUserName;//跑P用户名
    protected String opsPassWord;//跑P密码
    
    protected String yueProjectName;//粤股交项目名称
    protected String yueCode;//粤股交产品代码
    
    protected boolean shouldExitOnTearDown;
    protected String osName;
    protected String yueUrl;
    protected String yueUsername; // 综合管理系统登陆账户
    protected String yuePassword; // 综合管理系统登陆密码
    
    protected String hlUsername; //惠理财登录用户名
    protected String hlPassword; //惠理财登录密码
    protected String hlJiaoyipassword; //惠理财用户交易密码
    protected String hlPhone; //惠理财用户手机号
    protected String hlName; //惠理财用户名
    protected String hlIdentification; //惠理财用户身份证号
    protected String hlCard; //惠理财用户卡号
    protected String hlAmount; //惠理财用户充值金额
    protected String hlbankname;//惠理财用户绑卡对应的银行名称
    
    protected String hlcsxywyUsername; //惠理财寿险业务员登录的用户名
    protected String hlcsxywyPassword; //惠理财寿险业务员登录的密码
    protected String hlcsxywyJiaoyipassword; //惠理财寿险业务员的交易密码
    protected String hlcsxywyPhone; //惠理财寿险业务员的手机号
    protected String hlcsxywyName; //惠理财寿险业务员的用户名
    protected String hlcsxywyIdentification; //惠理财寿险业务员的身份证号
    protected String hlcsxywyCard; //惠理财寿险业务员的银行卡号
    protected String hlcsxywyAmount; //惠理财寿险业务员的用户充值金额
    protected String hlcsxywyBankname;//惠理财寿险业务员用户绑卡对应的银行名称
    protected String hlcsxywyBankid;//惠理财寿险业务员银行id
    
    protected String hlcsxywyInviteUsername; //惠理财寿险业务员邀请的用户登录的用户名
    protected String hlcsxywyInvitePassword; //惠理财寿险业务员邀请的用户登录的密码
    protected String hlcsxywyInviteJiaoyipassword; //惠理财寿险业务员邀请的用户的交易密码
    protected String hlcsxywyInvitePhone; //惠理财寿险业务员邀请的用户的手机号
    protected String hlcsxywyInviteName; //惠理财寿险业务员邀请的用户的用户名
    protected String hlcsxywyInviteIdentification; //惠理财寿险业务员邀请的用户的身份证号
    protected String hlcsxywyInviteCard; //惠理财寿险业务员邀请的用户的银行卡号
    protected String hlcsxywyInviteAmount; //惠理财寿险业务员邀请的用户充值金额
    protected String hlcsxywyInviteBankname;//惠理财寿险业务员邀请的用户绑卡对应的银行名称
    protected String hlcsxywyInviteBankid;//惠理财寿险业务员邀请的用户银行id
    
    protected String hlcsxXQywyUsername; //惠理财寿险续期业务员登录的用户名
    protected String hlcsxXQywyPassword; //惠理财寿险续期业务员登录的密码
    protected String hlcsxXQywyJiaoyipassword; //惠理财寿险续期业务员的交易密码
    protected String hlcsxXQywyPhone; //惠理财寿险续期业务员的手机号
    protected String hlcsxXQywyName; //惠理财寿险续期业务员的用户名
    protected String hlcsxXQywyIdentification; //惠理财寿险续期业务员的身份证号
    protected String hlcsxXQywyCard; //惠理财寿险续期业务员的银行卡号
    protected String hlcsxXQywyAmount; //惠理财寿险续期业务员的用户充值金额
    protected String hlcsxXQywyBankname;//惠理财寿险续期业务员用户绑卡对应的银行名称
    protected String hlcsxXQywyBankid;//惠理财寿险续期业务员银行id
    
    protected String hlcsxXQywyInviteUsername; //惠理财寿险续期业务员邀请的用户登录的用户名
    protected String hlcsxXQywyInvitePassword; //惠理财寿险续期业务员邀请的用户登录的密码
    protected String hlcsxXQywyInviteJiaoyipassword; //惠理财寿险续期业务员邀请的用户的交易密码
    protected String hlcsxXQywyInvitePhone; //惠理财寿险续期业务员邀请的用户的手机号
    protected String hlcsxXQywyInviteName; //惠理财寿险续期业务员邀请的用户的用户名
    protected String hlcsxXQywyInviteIdentification; //惠理财寿险续期业务员邀请的用户的身份证号
    protected String hlcsxXQywyInviteCard; //惠理财寿险续期业务员邀请的用户的银行卡号
    protected String hlcsxXQywyInviteAmount; //惠理财寿险续期业务员邀请的用户的用户充值金额
    protected String hlcsxXQywyInviteBankname;//惠理财寿险续期业务员邀请的用户用户绑卡对应的银行名称
    protected String hlcsxXQywyInviteBankid;//惠理财寿险续期业务员邀请的用户银行id
    
    protected String manualticketsUsername; //手动卡券用户名
    protected String manualticketsPassword; //手动卡券用户密码
    protected String manualticketsJiaoyipassword; //手动卡券用户交易密码
    protected String manualticketsPhone; //手动卡券用户手机号
    protected String manualticketsName;  //手动卡券用户的姓名
    protected String manualticketsIdentification; //手动卡券用户的身份证号
    protected String manualticketsCard; //手动卡券用户的银行卡号
    protected String manualticketsAmount; //手动卡券用户的充值金额
    protected String manualticketsBankid; //手动卡券用户绑卡对应的银行名称
    protected String manualticketsBankname; //手动卡券用户的银行id
    
    protected String huilicaiinvestUsername; //投资惠理财的用户名
    protected String huilicaiinvestPassword; //投资惠理财的用户密码
    protected String huilicaiinvestJiaoyipassword; //投资惠理财的用户交易密码
    protected String huilicaiinvestPhone;	//投资惠理财的用户手机号
    protected String huilicaiinvestName;	//投资惠理财的用户姓名
    protected String huilicaiinvestIdentification; //投资惠理财的用户身份证号
    protected String huilicaiinvestCard;	//投资惠理财的用户银行卡号
    protected String huilicaiinvestAmount;	//投资惠理财的用户充值金额
    protected String huilicaiinvestBankid;	//投资惠理财的用户银行id
    protected String huilicaiinvestBankname; //投资 惠理财的用户绑卡对应的银行名称
    
    //数据库地址，用户名，密码
    protected String dbUrl;
    protected String dbUser;
    protected String dbPassword;
    
    //惠理财开心麻花url
    protected String kxmhUrl;
    
    Logger logger = Logger.getAnonymousLogger();
    @Rule
    public BaseRule rule = new BaseRule();
    
    @Before
    public void setup() throws Exception {
    	Map<String, String> envVars = System.getenv();
        Properties prop = new Properties();
        ClassLoader loader = BaseTest.class.getClassLoader();
        InputStream inputStream;
        if(envVars.containsKey("ENV") && envVars.get("ENV").equalsIgnoreCase("pre"))
        {
        	inputStream = loader.getResourceAsStream("pre.properties");
        	      
        }
        else if(envVars.containsKey("ENV") && envVars.get("ENV").equalsIgnoreCase("qa1"))
        {
        	 inputStream = loader.getResourceAsStream("qa1.properties");
        }
        else if(envVars.containsKey("ENV") && envVars.get("ENV").equalsIgnoreCase("qa2"))
        {
        	 inputStream = loader.getResourceAsStream("qa2.properties");
        }
        else if(envVars.containsKey("ENV") && envVars.get("ENV").equalsIgnoreCase("qa3")){
        	
        	inputStream = loader.getResourceAsStream("qa3.properties");
        }
        else if(envVars.containsKey("ENV") && envVars.get("ENV").equalsIgnoreCase("qa4")){
        	
        	inputStream = loader.getResourceAsStream("qa4.properties");
        }
        else if(envVars.containsKey("ENV") && envVars.get("ENV").equalsIgnoreCase("qa5")){
        	
        	inputStream = loader.getResourceAsStream("qa5.properties");
        	
        }
        else if(envVars.containsKey("ENV") && envVars.get("ENV").equalsIgnoreCase("s1"))
        {
        	 inputStream = loader.getResourceAsStream("s1.properties");
        }
        else if(envVars.containsKey("ENV") && envVars.get("ENV").equalsIgnoreCase("s7")){
        	
        	 inputStream = loader.getResourceAsStream("s7.properties");
        }
        else
        {
        	inputStream = loader.getResourceAsStream("webDriver.properties");
        	
        }
        BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream));
        prop.load(bf);
        System.out.println("当前的环境为：" + envVars.get("ENV"));
        this.url = prop.getProperty("url");
        this.username = prop.getProperty("username");
        this.password = prop.getProperty("password");
        this.jiaoyipassword = prop.getProperty("jiaoyipassword");
        this.phone = prop.getProperty("phone");
        this.identification = prop.getProperty("identification");
        this.name = prop.getProperty("name");
        this.card = prop.getProperty("card");
        this.backupUrl = prop.getProperty("backup_url");
        this.amount = prop.getProperty("amount");

        // Console
        this.consoleUrl = prop.getProperty("console.url");
        this.consoleUsername = prop.getProperty("console.username");
        this.consolePassword = prop.getProperty("console.password");
        this.consoleProjectName = prop.getProperty("console.project_name");
        this.consoleRate = prop.getProperty("console.rate");

        // Business
        this.businessRegistrationUrl = prop.getProperty("business.registration_url");
        this.businessUsername = prop.getProperty("business.username");
        this.businessPassword = prop.getProperty("business.password");
        this.businessName = prop.getProperty("business.name");
        this.businessLegalName = new String(prop.getProperty("business.legal_name").getBytes(),"UTF-8");
        this.businessIdentification = prop.getProperty("business.identification");
        this.businessCode = prop.getProperty("business.code");
        this.businessLicense = prop.getProperty("business.license");
        this.businessPermit = prop.getProperty("business.permit");
        this.businessPhone = prop.getProperty("business.phone");
        this.businessAmount = prop.getProperty("business.amount");
        this.businessBankCard = prop.getProperty("business.bankcard");

        this.bankname = prop.getProperty("bankname");
        this.bankid = prop.getProperty("bankid");
        this.recard = prop.getProperty("recard");
        this.rebankid = prop.getProperty("rebankid");
        this.rebankname = prop.getProperty("rebankname");
        
        //transfer
        this.transferusername = prop.getProperty("transferusername");
		this.transferpassword = prop.getProperty("transferpassword");
		this.transferjiaoyipassword = prop.getProperty("transferjiaoyipassword");
		this.transferphone = prop.getProperty("transferphone");
		this.transferidentification= prop.getProperty("transferidentification");
		this.transfername = prop.getProperty("transfername");
		this.transfercard= prop.getProperty("transfercard");
		this.transferamount = prop.getProperty("transferamount");
		
		//user
		this.userinvestamount = prop.getProperty("user.investamount");
        this.userremaininginvestamount= prop.getProperty("user.remaininginvestamount");
        this.userallinvestamount = prop.getProperty("user.allinvestamount");		
		
        //BenefitInvest
		this.huiProjectName = prop.getProperty("hui.project_name");
		this.huiBatchName = prop.getProperty("hui.batch_name");
		this.huiProAmount  = prop.getProperty("hui.proAmount");
		this.huiYearRate  = prop.getProperty("hui.yearRate");
		this.huiDay  = prop.getProperty("hui.day"); 
		this.huiAmount = prop.getProperty("hui.amount");
		this.huiExpectRate = prop.getProperty("hui.expectRate");
		
		//跑P
		this.opsUrl = prop.getProperty("Ops.url");
		this.opsUserName = prop.getProperty("Ops.username");
		this.opsPassWord = prop.getProperty("Ops.password");
		
		//粤股交
		this.yueProjectName = prop.getProperty("yue.project_name");
		this.yueCode = prop.getProperty("yue.code");
		this.yueUrl = prop.getProperty("yue.url");
        this.yueUsername = prop.getProperty("yue.username");
        this.yuePassword = prop.getProperty("yue.password");
        
        //惠理财
        this.hlUsername = prop.getProperty("hlusername");
        this.hlPassword = prop.getProperty("hlpassword");
        this.hlJiaoyipassword = prop.getProperty("hljiaoyipassword");
        this.hlPhone = prop.getProperty("hlphone");
        this.hlName = prop.getProperty("hlname");
        this.hlIdentification = prop.getProperty("hlidentification");
        this.hlCard = prop.getProperty("hlcard");
        this.hlAmount = prop.getProperty("hlamount");
        this.kxmhUrl = prop.getProperty("kxmh.url");
        this.hlbankname = prop.getProperty("bankname");
        
        //惠理财寿险业务员
        this.hlcsxywyUsername = prop.getProperty("hlcsxywy.username");
        this.hlcsxywyPassword = prop.getProperty("hlcsxywy.password");
        this.hlcsxywyJiaoyipassword = prop.getProperty("hlcsxywy.jiaoyipassword");
        this.hlcsxywyPhone = prop.getProperty("hlcsxywy.phone");
        this.hlcsxywyName = prop.getProperty("hlcsxywy.name");
        this.hlcsxywyIdentification = prop.getProperty("hlcsxywy.identification");
        this.hlcsxywyCard = prop.getProperty("hlcsxywy.card");
        this.hlcsxywyAmount = prop.getProperty("hlcsxywy.amount");
        this.hlcsxywyBankname = prop.getProperty("hlcsxywy.bankname");
        this.hlcsxywyBankid =  prop.getProperty("hlcsxywy.bankid");
        
        //惠理财寿险业务员邀请的用户
        this.hlcsxywyInviteUsername = prop.getProperty("hlcsxywyinvite.username");
        this.hlcsxywyInvitePassword = prop.getProperty("hlcsxywyinvite.password");
        this.hlcsxywyInviteJiaoyipassword = prop.getProperty("hlcsxywyinvite.jiaoyipassword");
        this.hlcsxywyInvitePhone = prop.getProperty("hlcsxywyinvite.phone");
        this.hlcsxywyInviteName = prop.getProperty("hlcsxywyinvite.name");
        this.hlcsxywyInviteIdentification = prop.getProperty("hlcsxywyinvite.identification");
        this.hlcsxywyInviteCard = prop.getProperty("hlcsxywyinvite.card");
        this.hlcsxywyInviteAmount = prop.getProperty("hlcsxywyinvite.amount");
        this.hlcsxywyInviteBankname = prop.getProperty("hlcsxywyinvite.bankname");
        this.hlcsxywyInviteBankid =  prop.getProperty("hlcsxywyinvite.bankid");
        
        //惠理财寿险续期业务员
        this.hlcsxXQywyUsername = prop.getProperty("hlcsxXQywy.username");
        this.hlcsxXQywyPassword = prop.getProperty("hlcsxXQywy.password");
        this.hlcsxXQywyJiaoyipassword = prop.getProperty("hlcsxXQywy.jiaoyipassword");
        this.hlcsxXQywyPhone = prop.getProperty("hlcsxXQywy.phone");
        this.hlcsxXQywyName = prop.getProperty("hlcsxXQywy.name");
        this.hlcsxXQywyIdentification = prop.getProperty("hlcsxXQywy.identification");
        this.hlcsxXQywyCard = prop.getProperty("hlcsxXQywy.card");
        this.hlcsxXQywyAmount = prop.getProperty("hlcsxXQywy.amount");
        this.hlcsxXQywyBankname = prop.getProperty("hlcsxXQywy.bankname");
        this.hlcsxXQywyBankid =  prop.getProperty("hlcsxXQywy.bankid");
        
        //惠理财寿险续期业务员邀请的用户
        this.hlcsxXQywyInviteUsername = prop.getProperty("hlcsxXQywyinvite.username");
        this.hlcsxXQywyInvitePassword = prop.getProperty("hlcsxXQywyinvite.password");
        this.hlcsxXQywyInviteJiaoyipassword = prop.getProperty("hlcsxXQywyinvite.jiaoyipassword");
        this.hlcsxXQywyInvitePhone = prop.getProperty("hlcsxXQywyinvite.phone");
        this.hlcsxXQywyInviteName = prop.getProperty("hlcsxXQywyinvite.name");
        this.hlcsxXQywyInviteIdentification = prop.getProperty("hlcsxXQywyinvite.identification");
        this.hlcsxXQywyInviteCard = prop.getProperty("hlcsxXQywyinvite.card");
        this.hlcsxXQywyInviteAmount = prop.getProperty("hlcsxXQywyinvite.amount");
        this.hlcsxXQywyInviteBankname = prop.getProperty("hlcsxXQywyinvite.bankname");
        this.hlcsxXQywyInviteBankid =  prop.getProperty("hlcsxXQywyinvite.bankid");
        
        //手动卡券用户
        this.manualticketsUsername = prop.getProperty("manualtickets.username");
        this.manualticketsPassword = prop.getProperty("manualtickets.password");
        this.manualticketsJiaoyipassword = prop.getProperty("manualtickets.jiaoyipassword");
        this.manualticketsPhone = prop.getProperty("manualtickets.phone");
        this.manualticketsName = prop.getProperty("manualtickets.name");
        this.manualticketsIdentification = prop.getProperty("manualtickets.identification");
        this.manualticketsCard = prop.getProperty("manualtickets.card");
        this.manualticketsAmount = prop.getProperty("manualtickets.amount");
        this.manualticketsBankid = prop.getProperty("manualtickets.bankid");
        this.manualticketsBankname = prop.getProperty("manualtickets.bankname");
        
        //投资惠理财的用户
        this.huilicaiinvestUsername = prop.getProperty("huilicaiinvest.username");
        this.huilicaiinvestPassword = prop.getProperty("huilicaiinvest.password");
        this.huilicaiinvestJiaoyipassword = prop.getProperty("huilicaiinvest.jiaoyipassword"); 
        this.huilicaiinvestPhone = prop.getProperty("huilicaiinvest.phone");
        this.huilicaiinvestName = prop.getProperty("huilicaiinvest.name");
        this.huilicaiinvestIdentification = prop.getProperty("huilicaiinvest.identification");
        this.huilicaiinvestCard = prop.getProperty("huilicaiinvest.card");
        this.huilicaiinvestAmount = prop.getProperty("huilicaiinvest.amount");
        this.huilicaiinvestBankid = prop.getProperty("huilicaiinvest.bankid");
        this.huilicaiinvestBankname = prop.getProperty("huilicaiinvest.bankname");
        
        
        //数据库
        this.dbUrl = prop.getProperty("db.url");
        this.dbUser = prop.getProperty("db.user");
        this.dbPassword = prop.getProperty("db.password");
				
		this.shouldExitOnTearDown = true;
		this.osName = System.getProperty("os.name");
		
		
		
		//输出当前浏览器的类型
		System.out.println("当前的浏览器为：" + envVars.get("BROWSERTYPE"));
		 
	   if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
	   {

		    //非hub的调整
		    //设置IE的配置
		    ClassLoader loaderie = BaseTest.class.getClassLoader();
			File fileie = new File(loaderie.getResource("IEDriverServer_x64.exe").getFile());
			System.setProperty("webdriver.ie.driver", fileie.getAbsolutePath());
			DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
			ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			ieCapabilities.setCapability("ignoreZoomSetting", true);
			ieCapabilities.setCapability("nativeEvents",false);
			ieCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS,true);
			ieCapabilities.setCapability("unexpectedAlertBehaviour", "accept");
			ieCapabilities.setCapability("ignoreProtectedModeSettings", true);
			ieCapabilities.setCapability("disable-popup-blocking", true);
			ieCapabilities.setCapability("enablePersistentHover", true);
			ieCapabilities.setCapability("requireWindowFocus", true);
			ieCapabilities.setCapability("EnableNativeEvents", false);
			ieCapabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,false);
		   
			this.driver = new InternetExplorerDriver(ieCapabilities);  
			
		   //ie_hub调整
		//	DesiredCapabilities abilities = DesiredCapabilities.internetExplorer();
		//	this.driver = new RemoteWebDriver( new URL("http://10.88.4.200:4444/wd/hub"), abilities);
		   
		   
	   }else if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("chrome"))
	   {
		   ClassLoader loaderchrome = BaseTest.class.getClassLoader();
		   if(osName.substring(0,3).equalsIgnoreCase("win")){
			   File filechrome = new File(loaderchrome.getResource("chromedriver_win32.exe").getFile());
			   System.setProperty("webdriver.chrome.driver", filechrome.getAbsolutePath());
	           this.driver = new ChromeDriver();
		   }else{
			    File filechrome = new File(loaderchrome.getResource("chromedriver_linux64").getFile());
			    System.setProperty("webdriver.chrome.driver", filechrome.getAbsolutePath());
		        this.driver = new ChromeDriver();
		   }
            
       }
	   else   
	   { 
		   // Setup ff driver and disable caching
		   	FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("browser.cache.disk.enable", false);
			profile.setPreference("browser.cache.memory.enable", false);
			profile.setPreference("browser.cache.offline.enable", false);
			profile.setPreference("network.http.use-cache", false); 
			profile.setPreference("browser.startup.homepage_override.mstone", "ignore");
			profile.setPreference("startup.homepage_welcome_url.additional", "about:blank");
			
			//非hub调整
			this.driver = new FirefoxDriver(profile);

			//firefox_hub调整
//			DesiredCapabilities abilities = DesiredCapabilities.firefox();
//			abilities.setCapability("firefox_profile", profile);
//			this.driver = new RemoteWebDriver( new URL("http://10.88.4.200:4444/wd/hub"), abilities);
//			
			
	    }
		
       
        this.driver.manage().window().maximize();
        this.wait = new WebDriverWait(driver, 10000).withTimeout(10, TimeUnit.SECONDS).pollingEvery(100, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);

           if (envVars.containsKey("DEBUG") && envVars.get("DEBUG").equalsIgnoreCase("1")) {
            this.shouldExitOnTearDown = false;
        }
       
    }

    @After
    public void tearDown() {
        if (this.shouldExitOnTearDown) {
        	Map<String, String> envVars = System.getenv();
        	if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        		this.driver.close();
        		this.driver.quit();
            }else
            {
            	this.driver.quit();
            }
        }
    }

    /**
     * 读取主页
     */
    protected void loadPage() {
        this.loadPage(this.url, "首页");
    }

    /**
     * 读取主页
     */
    protected void loadPage(String url, String title) {
        this.driver.get(url);
        System.out.println(this.driver.getTitle());
//        assertTrue("读取主页失败", this.driver.getTitle().contains(title));
    }

    /**
     * 登录
     */
    protected void login() {
//        this.driver.findElement(By.linkText("登录")).click();
    	this.driver.findElement(By.linkText("我的账户")).click();
    	this.wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("password"));
            }
        });

        this.driver.findElement(By.id("email")).sendKeys(this.username);
        this.driver.findElement(By.id("password")).sendKeys(this.password);
        this.driver.findElement(By.id("code_temp")).sendKeys("1234");
        this.driver.findElement(By.id("btn_login")).click();
        try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//        this.wait.until(new ExpectedCondition<WebElement>() {
//            @Override
//            public WebElement apply(WebDriver webDriver) {
//                return webDriver.findElement(By.className("userName"));
//            }
//        });
//       
//        String name = this.driver.findElement(By.className("userName")).getText();
//        assertTrue("登录失败", name.equalsIgnoreCase(this.username));
        while(true){
        	if(IsElementPresent(By.xpath("//i[@id='phone']"))){
        		break;
        	}
        }
    }
    
    /**
     * 惠理财用户登录
     */
    protected void HLlogin() {
    //控制浏览器类型
   	 Map<String, String> envVars = System.getenv();
    //等待页面加载
    this.wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.linkText("我的账户"));
            }
       });
  	this.driver.findElement(By.linkText("我的账户")).click();
  	this.wait.until(new ExpectedCondition<WebElement>() {
          @Override
          public WebElement apply(WebDriver webDriver) {
              return webDriver.findElement(By.id("password"));
          }
      });

      this.driver.findElement(By.id("userName")).sendKeys(this.hlUsername);
      this.driver.findElement(By.id("password")).sendKeys(this.hlPassword);
      this.driver.findElement(By.id("code_temp")).sendKeys("1234");
     // this.driver.findElement(By.id("btn_login")).click();
      if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
 	     
    	  try {
    		 this.driver.findElement(By.xpath("//div[@id='btn_login1']")).click();
  			Thread.sleep(3000);
            } catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
            }	  
      }else{
    	  try {
   			Thread.sleep(3000);
             } catch (InterruptedException e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
             }	  
    	  this.driver.findElement(By.id("btn_login1")).click();  
    	  try {
   			Thread.sleep(3000);
             } catch (InterruptedException e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
             }	  
      }
 

      this.wait.until(new ExpectedCondition<WebElement>() {
          @Override
          public WebElement apply(WebDriver webDriver) {
              return webDriver.findElement(By.xpath("//i[@id='phone']"));
          }
      });
      if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
  	     while(true){
    	  if(!this.driver.findElement(By.xpath("//i[@id='phone']")).getText().trim().equals("")&&IsElementPresent(By.xpath("//i[@id='phone']"))){
    		break;  
    	  }
  	     }
      }
      String userPhone = this.driver.findElement(By.xpath("//i[@id='phone']")).getText();
      String userPhoneStart = this.hlPhone.substring(0, 3);
      System.out.println("手机号的前三位：" + userPhoneStart );
      String userPhoneEnd = this.hlPhone.substring(7,11);
      System.out.println("手机号的后四位：" + userPhoneEnd);
      assertTrue("登录失败", userPhone.equalsIgnoreCase(userPhoneStart+"****"+userPhoneEnd));
      
  }
    	    
    /**
     * 惠理财寿险业务员登录
     *  
     */
    protected void HLClogin(String hlcusername,String hlcpassword,String hlcphone){
    	//控制浏览器类型
    	 Map<String, String> envVars = System.getenv();
    	 
        //等待页面加载
        this.wait.until(new ExpectedCondition<WebElement>() {
                @Override
                public WebElement apply(WebDriver webDriver) {
                    return webDriver.findElement(By.linkText("我的账户"));
                }
           });
      	this.driver.findElement(By.linkText("我的账户")).click();
      	this.wait.until(new ExpectedCondition<WebElement>() {
              @Override
              public WebElement apply(WebDriver webDriver) {
                  return webDriver.findElement(By.id("password"));
              }
          });

          this.driver.findElement(By.id("userName")).sendKeys(hlcusername);
          this.driver.findElement(By.id("password")).sendKeys(hlcpassword);
          this.driver.findElement(By.id("code_temp")).sendKeys("1234");

          if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	     
        	  try {
        		 this.driver.findElement(By.xpath("//div[@id='btn_login1']")).click();
      			Thread.sleep(5000);
                } catch (InterruptedException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
                }	  
          }else{
        	  try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	  this.driver.findElement(By.id("btn_login1")).click();  
        	  try {
  				Thread.sleep(2000);
  			} catch (InterruptedException e) {
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  			}
          }
     
          this.wait.until(new ExpectedCondition<WebElement>() {
              @Override
              public WebElement apply(WebDriver webDriver) {
                  return webDriver.findElement(By.xpath("//i[@id='phone']"));
              }
          });
          while(true){
        	  if(!this.driver.findElement(By.xpath("//i[@id='phone']")).getText().trim().equals("")&&IsElementPresent(By.xpath("//i[@id='phone']"))){
        		break;  
        	  }
      	     }
          String userPhone = this.driver.findElement(By.xpath("//i[@id='phone']")).getText();
          String userPhoneStart = hlcphone.substring(0, 3);
          System.out.println("手机号的前三位：" + userPhoneStart );
          String userPhoneEnd = hlcphone.substring(7,11);
          System.out.println("手机号的后四位：" + userPhoneEnd);
          assertTrue("登录失败", userPhone.equalsIgnoreCase(userPhoneStart+"****"+userPhoneEnd));
      }
    
    /**
     * 企业用户登陆
     */
    protected void businessLogin() {

    	this.driver.findElement(By.linkText("我的账户")).click();
    	
    	this.wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("userName"));
            }
        });
    	this.driver.findElement(By.id("userName")).clear();
        this.driver.findElement(By.id("userName")).sendKeys(this.businessUsername);
        System.out.println("企业用户的用户名为：" + this.businessUsername);
        this.driver.findElement(By.id("password")).clear();
        this.driver.findElement(By.id("password")).sendKeys(this.businessPassword);
        System.out.println("企业用户的密码为：" + this.businessPassword);
        this.driver.findElement(By.id("code_temp")).sendKeys("1234");
        System.out.println("企业用户点击登录按钮前");
        try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        this.driver.findElement(By.id("btn_login1")).click();
        try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println("企业用户点击登录按钮后");
        new WebDriverWait(driver,10).until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("//i[@id='phone']"));
            }
        });
        System.out.println("用户姓名验证前");
        String name = this.driver.findElement(By.xpath("//i[@id='phone']")).getText();
        System.out.println("name:" + name);
        assertTrue("登录失败", name.equalsIgnoreCase(this.businessUsername));
    }

    /**
     * 运营平台登陆
     */
    protected void consoleLogin() {
        this.driver.get(this.consoleUrl);
        assertTrue("读取主页失败", this.driver.getTitle().equals("登陆"));

        this.driver.findElement(By.id("usercd")).sendKeys(this.consoleUsername);
        this.driver.findElement(By.id("password")).sendKeys(this.consolePassword);
        this.driver.findElement(By.id("dl")).click();

        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[1]/div/div/div")));
        String text = this.driver.findElement(By.xpath("html/body/div[1]/div/div/div")).getText();
        assertTrue("登录失败", text.equals("运营管理平台"));
    }
    
    /**
     * 
     * 转让用户登录
     */
    protected void transferLogin() {
//        this.driver.findElement(By.linkText("登录")).click();
    	this.driver.findElement(By.linkText("我的账户")).click();
    	this.wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("password"));
            }
        });
        this.driver.findElement(By.id("email")).sendKeys(this.transferusername);
        this.driver.findElement(By.id("password")).sendKeys(this.transferpassword);
        this.driver.findElement(By.id("code_temp")).sendKeys("1234");
        this.driver.findElement(By.id("btn_login")).click();

        this.wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.className("userName"));
            }
        });

        String name = this.driver.findElement(By.className("userName")).getText();
        assertTrue("登录失败", name.equalsIgnoreCase(this.transferusername));
    }
    
    /**
     * 惠金所综合管理系统的登录
     * 
     */
   public void  tmsLogin(){
	   this.driver.get(this.yueUrl);
	   //等待页面加载
	   this.wait.until(new ExpectedCondition<WebElement>() {
           @Override
           public WebElement apply(WebDriver webDriver) {
               return webDriver.findElement(By.xpath(".//*[@id='username']"));
           }
       });
	   this.driver.findElement(By.xpath(".//*[@id='username']")).clear();
	   this.driver.findElement(By.xpath(".//*[@id='username']")).sendKeys(this.yueUsername);
	   this.driver.findElement(By.xpath(".//*[@id='password']")).clear();
	   this.driver.findElement(By.xpath(".//*[@id='password']")).sendKeys(this.yuePassword);
	   //点击充值按钮
	   this.driver.findElement(By.xpath(".//*[@id='reSetStatus']")).click();
	   
	   //再次输入用户名和密码
	   this.driver.findElement(By.xpath(".//*[@id='username']")).clear();
	   this.driver.findElement(By.xpath(".//*[@id='username']")).sendKeys(this.yueUsername);
	   this.driver.findElement(By.xpath(".//*[@id='password']")).clear();
	   this.driver.findElement(By.xpath(".//*[@id='password']")).sendKeys(this.yuePassword);
	   
	   //点击登录
	   this.driver.findElement(By.xpath(".//*[@id='submit']")).click();
	   
	   //等待页面加载
	   this.wait.until(new ExpectedCondition<WebElement>() {
           @Override
           public WebElement apply(WebDriver webDriver) {
               return webDriver.findElement(By.xpath(".//*[@id='menu']"));
           }
       });
	  	   
   }
    
    public void navigateToPage(String name) {
    	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(name)));
    	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(name)));
        this.driver.findElement(By.linkText(name)).click();
        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='topBox']/ul/li/a[contains(@class, 'hover')]"), name));
        
    }
   //首页导航菜单切换
   public void newnavigateToPage(int index,String name){
	   while(true){
		   if(IsElementPresent(By.linkText(name))){
			   break;
		   }
	   }
	   this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(name)));
   	   this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(name)));
       this.driver.findElement(By.linkText(name)).click();
       this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']/li[@id='index"+index+"'"+"]"+"/a[contains(@class, 'active')]"), name));
   }

    /**
     * 处理不同window之间的跳转
     */
    protected WebDriver switchToWindow(String title) {
        return this.switchToWindow(title, false);
    }

    protected WebDriver switchToWindow(String title, Boolean exactMatch) {
        Set<String> handles = this.driver.getWindowHandles(); // 获取所有存在的视窗

        for (String handle : handles) { // 通过title比对
            WebDriver driver = this.driver.switchTo().window(handle);
            if (!exactMatch && driver.getTitle().contains(title))
                return driver; // 返回找到的视窗驱动
            else if (exactMatch && driver.getTitle().equalsIgnoreCase(title))
                return driver; //返回完整匹配的视窗驱动
        }

        return this.driver;
    }
//重构后菜单重写
   /**
	 * 
    *  left navigation menu (no submenu)
    *
    * @param   index    menu index number
    *                
    * @param   strTtile  menu title info
    *                 
    */
	public void leftNavigationMenu(int index,String strTitle){
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='account_leftMenu']//li["+index+"]")));
		 this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='account_leftMenu']//li["+index+"]"))).click();
		 try{
		 this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//ul[@id='account_leftMenu']//li["+index+"]"),strTitle));
		 }catch(Exception e){
			logger.log(Level.WARNING,"leftNavigationMenu error info:" + e);
		 }  
		  
}
	 /**
	 * 
    * left navigation menu (contain submenu)
    *
    * @param   index    menu index number
    *                
    * @param   strTtile  menu title info
    *                 
    */
	//左侧导航菜单（有子菜单）
	public void leftNavigationSubMenu(int index,int subindex,String strSubTitle){
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='account_leftMenu']//li["+index+"]")));
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='account_leftMenu']//li["+index+"]"))).click();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='account_leftMenu']//li["+index+"]"+"//dt//dl["+subindex+"]")));
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='account_leftMenu']//li["+index+"]"+"//dt//dl["+subindex+"]"))).click();
		try{
		this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//ul[@id='account_leftMenu']//li["+index+"]"+"//dt//dl["+subindex+"]"),strSubTitle));
		}catch(Exception e){
			logger.log(Level.WARNING,"leftNavigationSubMenu error info:" + e);
		}
	}
	//右侧导航菜单tab_top(eg.投资转让右侧，充值/提现右侧)
	public void rightTabNavigationMenu(int index,String strTitle){
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='detail_list clearfix']//ul[@class='tab_top']//li["+index+"]")));
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='detail_list clearfix']//ul[@class='tab_top']//li["+index+"]"))).click();
		try{
		this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='detail_list clearfix']//ul[@class='tab_top']//li["+index+"]"),strTitle));
		}catch(Exception e){
			logger.log(Level.WARNING,"rightTabNavigationMenu error info:" + e);
		}
	}
	
	//右侧导航菜单tab_top hfax-tab-top(eg.投资券右侧,我的投资右侧)
	public void rightHfaxTabNavigationMenu(int index,String strTitle){
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@class='tab_top hfax-tab-top']//li["+index+"]")));
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='tab_top hfax-tab-top']//li["+index+"]"))).click();
		try{
		this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//ul[@class='tab_top hfax-tab-top']//li["+index+"]"),strTitle));
		}catch(Exception e){
			logger.log(Level.WARNING,"rightHfaxTabNavigationMenu error info:" + e);
		}
	}
	
  //右侧导航菜单tab_top clearfix(eg.用户设置右侧)
	public void rightClearfixTabNavigationMenu(int index,String strTitle){
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@class='tab_top clearfix']//li["+index+"]")));
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='tab_top clearfix']//li["+index+"]"))).click();
		try{
		this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//ul[@class='tab_top clearfix']//li["+index+"]"),strTitle));
		}catch(Exception e){
			logger.log(Level.WARNING,"rightClearfixTabNavigationMenu error info:" + e);
		}
	}
	
	//右侧导航菜单repay_list(eg.红包右侧)
	public void rightRepayListTabNavigationMenu(int index,String strTitle){
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@class='repay_list']//li["+index+"]")));
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='repay_list']//li["+index+"]"))).click();
		try{
		this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//ul[@class='repay_list']//li["+index+"]"),strTitle));
		}catch(Exception e){
			logger.log(Level.WARNING,"rightRepayListTabNavigationMenu error info:" + e);
		}
	}
	
	//顶部导航菜单，eg.首页，活期理财，定期理财，转让专区，我的账户，关于我们
	public void topNavigationMenu(int index,String strTitle){
		//hfax-topnav-nav right
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//li["+index+"]")));
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//li["+index+"]"))).click();
		try{
			new WebDriverWait(driver,10).until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//a["+index+"]"),strTitle));
		}catch(Exception e){
			logger.log(Level.WARNING,"topNavigationMenu error info:" + e);
		}
		
	}
	//顶部导航菜单，定期理财
	public void topNavigationMenu1(int index,String strTitle){
			//hfax-topnav-nav right
			this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//li["+index+"]")));
			this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//li["+index+"]"))).click();
			try{
				new WebDriverWait(driver,10).until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//li["+index+"]"),strTitle));
			}catch(Exception e){
				logger.log(Level.WARNING,"topNavigationMenu1 error info:" + e);
			}
			
		}
	//用户登录后顶部导航菜单，，eg.首页，活期理财,转让专区，我的账户，关于我们
	public void loginTopNavigationMenu(int index,String strTitle){
		//hfax-topnav-nav right
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//a["+index+"]")));
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//a["+index+"]"))).click();
		try{
		 new WebDriverWait(driver, 10).until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//a["+index+"]"),strTitle));
		}catch(Exception e){
			logger.log(Level.WARNING,"loginTopNavigationMenu error info:" + e);
		}
		
	}
	
	//用户登录后顶部导航菜单，定期理财
	public void loginTopNavigationMenu1(int index,String strTitle){
		//hfax-topnav-nav right
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//a["+index+"]")));
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//a["+index+"]"))).click();
		try{
		 new WebDriverWait(driver, 10).until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='hfax-topnav-nav right']//li["+index+"]"),strTitle));
		}catch(Exception e){
			logger.log(Level.WARNING,"loginTopNavigationMenu1 error info:" + e);
		}
		
	}
	
    protected void selectMenu(int index, String titleToVerify) {
    	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("li_" + index))).click();
//        this.driver.findElement(By.id("li_" + index)).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='l_nav']//li[contains(@class, 'on')]/a")));
        String selectedMenuTitle = this.driver.findElement(By.xpath("//div[@class='l_nav']//li[contains(@class, 'on')]/a")).getText();
        assertTrue("选中菜单名字不正确: " + selectedMenuTitle + ", 应该为: " + titleToVerify, selectedMenuTitle.equalsIgnoreCase(titleToVerify));
    }
    //左侧导航菜单
    protected void newSelectMenu(int index,String title){
    	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='hfax-account-left left']//ul//li"+"["+index+"]"))).click();
    	this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='hfax-account-left left']//ul//li"+"["+index+"]"), title));
    	
    }
    //左侧导航子菜单
    protected void newSelectSubMenu(int index,String title){
    	new WebDriverWait(this.driver,8).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='hfax-account-left left']/ul/li["+index+"]"))).click();
    	new WebDriverWait(this.driver,8).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='hfax-account-left left']/ul/li["+index+"]"+"/dt/dl/a[contains(text(),"+"'"+title+"'"+")]"))).click();
    	this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='hfax-account-left left']/ul/li["+index+"]"+"/dt/dl/a[contains(text(),"+title+")and @class='active']"), title));
    }
    //右侧tab菜单
    protected void newSelectTab(int index,String title){
    	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='tab_top hfax-tab-top']//li["+index+"]"))).click();
    	this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//ul[@class='tab_top hfax-tab-top']//li["+index+"]"), title));
    }
    //右侧tab_1菜单,eg.充值/提现中的（资金记录，充值，提现）
    protected void newSelectTab_1(int index,String title){
    	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='tab_top']//li["+index+"]"))).click();
    	this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//ul[@class='tab_top']//li["+index+"]"), title));
    }
  
    //IE菜单的选择
    protected void selectMenuIE(int index, String titleToVerify){
    	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@id="+ "\'"+"li_"+index+"\'"+"]/a")));
    	this.driver.findElement(By.xpath("//li[@id="+ "\'"+"li_"+index+"\'"+"]/a")).click();
        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//li[@id="+"\'"+"li_"+index+"\'"+"and @class='on']/a"), titleToVerify));   	
    }
    //IE企业借款管理菜单选择
    protected void selectLoanMenuIE(int index, String titleToVerify){
    	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@id="+ "\'"+"li_"+index+"\'"+"]/a")));
    	this.driver.findElement(By.xpath("//li[@id="+ "\'"+"li_"+index+"\'"+"]/a")).click();
        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//li[@id="+"\'"+"li_"+index+"\'"+"and @class='fir on']/a"), titleToVerify));   	
    }
    
    protected void selectSubMenu(int index, String titleToVerify) {
        this.driver.findElement(By.xpath("//div[@class='tabtil']/ul/li[" + index + "]")).click();
        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='tabtil']/ul/li[contains(@class, 'on')]"), titleToVerify));
    }
    
    protected void setCalendarFromDate(String date) {
        JavascriptExecutor removeAttribute = (JavascriptExecutor) this.driver;
        removeAttribute.executeScript("var setDate=document.getElementById(\"startTime\");setDate.removeAttribute('readonly');");
        this.driver.findElement(By.xpath(".//*[@id='startTime']")).sendKeys(date);
    }

    protected void setCalendarToDate(String date) {
        JavascriptExecutor removeAttribute = (JavascriptExecutor) this.driver;
        removeAttribute.executeScript("var setDate=document.getElementById(\"endTime\");setDate.removeAttribute('readonly');");
        this.driver.findElement(By.xpath(".//*[@id='endTime']")).sendKeys(date);
    }
    /*
     * 判断元素是否存在
     */
    protected boolean IsElementPresent(By by) {
    	try {
            this.driver.findElement(by);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    /**
     * 子菜单的选择 ，
     * eg.惠理财，新手专区，惠理财，惠投资，惠聚财
     */
    protected WebElement clickOnSubmenu(String title) {
        WebElement submenu = this.driver.findElement(By.id("listTab-box"));
   
        submenu.findElement(By.linkText(title)).click();
        
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.className("list-control")));
        List<WebElement> elements = this.driver.findElements(By.className("list-control"));

        for (WebElement el : elements) {
        	this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            if (el.isDisplayed()) return el;
        }
        assertTrue("无内容显示: " + title, false);
        return null;
    }
    /**
     * 
     *  获取当前的日期
     */
    protected String getCurrentDate(){
    	Date dNow = new Date();   //当前时间
      	Calendar calendar = Calendar.getInstance(); //得到日历
      	calendar.setTime(dNow);//把当前时间赋给日历
      	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-M-d"); //设置时间格式
      	String defaultDate = sdf.format(dNow);  
      	return defaultDate;
      	
    }
    /**
     *  获取N天后的日期
     */
    protected String getDate(int number){
    	Calendar calendar = Calendar.getInstance(); //得到日历
    	Date dNow = new Date(); 
    	Date dAfter = new Date();
    	calendar.setTime(dNow);//把当前时间赋给日历
      	calendar.add(Calendar.DAY_OF_MONTH,number);  
      	dAfter = calendar.getTime(); 
      	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd"); //设置时间格式
      	String defaultDate = sdf.format(dAfter);    //格式化N天
      	return defaultDate;
    }
    /**
     * 中文日期的转换
     */
    protected String getChineseDate(int number){
    	Calendar calendar = Calendar.getInstance(); //得到日历
    	Date dNow = new Date(); 
    	Date dAfter = new Date();
    	calendar.setTime(dNow);//把当前时间赋给日历
      	calendar.add(Calendar.DAY_OF_MONTH,number);  
      	dAfter = calendar.getTime(); 
      	SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日"); //设置时间格式
      	String defaultDate = sdf.format(dAfter);    //格式化N天
      	System.out.println(defaultDate);
      	return defaultDate;
    }
    /**
     * 验证提示文本是否正确
     */
	public void verifyTextInfo(String xpath,String content){
		WebElement ele = this.driver.findElement(By.xpath(xpath));
		while(true){
			if(IsElementPresent(By.xpath(xpath))){
				break;
			}
		}
		if(ele.getText().trim().equals(content)){
			System.out.println("数据显示正确" + ele.getText().trim());
		}else{
			fail("数据显示不正确");
		}
	}
	/**
	 * 计算两个日期相隔的天数
	 * @throws ParseException 
	 * 
	 */
	 public int calcIntervalDay(String dateFirst,String dateSecond) throws ParseException{
	
	            //时间转换类
	            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	            Date date1 = sdf.parse(dateFirst);
	            Date date2 = sdf.parse(dateSecond);
	            //将转换的两个时间对象转换成Calendard对象
	            Calendar can1 = Calendar.getInstance();
	            can1.setTime(date1);
	            Calendar can2 = Calendar.getInstance();
	            can2.setTime(date2);
	            //拿出两个年份
	            int year1 = can1.get(Calendar.YEAR);
	            int year2 = can2.get(Calendar.YEAR);
	            //天数
	            int days = 0;
	            Calendar can = null;
	            //如果can1 < can2
	            //减去小的时间在这一年已经过了的天数
	            //加上大的时间已过的天数
	            if(can1.before(can2)){
	                days -= can1.get(Calendar.DAY_OF_YEAR);
	                days += can2.get(Calendar.DAY_OF_YEAR);
	                can = can1;
	            }else{
	                days -= can2.get(Calendar.DAY_OF_YEAR);
	                days += can1.get(Calendar.DAY_OF_YEAR);
	                can = can2;
	            }
	            for (int i = 0; i < Math.abs(year2-year1); i++) {
	                //获取小的时间当前年的总天数
	                days += can.getActualMaximum(Calendar.DAY_OF_YEAR);
	                //再计算下一年。
	                can.add(Calendar.YEAR, 1);
	            }
	            return days;
			
	    }
	  	//获取元素内容
	    protected String getElementText(String strXpath){
	    	try{
	    		String text = this.driver.findElement(By.xpath(strXpath)).getText().trim();
	    		return text;
	    	}catch (Exception e){
	    		e.printStackTrace();
	    	}
	    	return null;
	    }
}