
package com.hfax.selenium.base;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.rule.BaseRule;

/**
 *
 * Base class for all junit derived tests
 *
 * Config file is read from webDriver.properties and exposed through protected
 * variables. Several common instance methods are also implemented here.
 *
 * Created by philip on 3/8/16.
 *
 */
public class BaseTest {
	protected WebDriver driver; // Internal browser driver
	protected FluentWait<WebDriver> wait;
	protected String url; // Base url for target test host
	protected String backupUrl; // Secondary base url for making deposit
								// (常亮前置写死)
	protected String username; // Account username
	protected String password; // Account password
	protected String phone; // Account holder phone #
	protected String identification; // Account holder id #
	protected String name; // Account holder name (中文)
	protected String card; // Account holder bank card (只能绑一张)
	protected String amount; // 充值金额
    protected boolean shouldExitOnTearDown;

	@Rule
	public BaseRule rule = new BaseRule();

	@Before
	public void setup() throws Exception {
		Properties prop = new Properties();
		ClassLoader loader = BaseTest.class.getClassLoader();
		InputStream inputStream = loader.getResourceAsStream("webDriver.properties");
		BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream));
		prop.load(bf);

		this.url = prop.getProperty("url");
		this.username = prop.getProperty("username");
		this.password = prop.getProperty("password");
		this.phone = prop.getProperty("phone");
		this.identification = prop.getProperty("identification");
		this.name = prop.getProperty("name");
		this.card = prop.getProperty("card");
		this.backupUrl = prop.getProperty("backup_url");
		this.amount = prop.getProperty("amount");
        this.shouldExitOnTearDown = true;

		// Setup ff driver and disable caching
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.cache.disk.enable", false);
		profile.setPreference("browser.cache.memory.enable", false);
		profile.setPreference("browser.cache.offline.enable", false);
		profile.setPreference("network.http.use-cache", false);
		profile.setPreference("browser.startup.homepage_override.mstone", "ignore");
		profile.setPreference("startup.homepage_welcome_url.additional", "about:blank");

		this.driver = new FirefoxDriver(profile);
		this.wait = new WebDriverWait(driver, 5000).withTimeout(5, TimeUnit.SECONDS).pollingEvery(100, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);

		Map<String, String> envVars = System.getenv();

		if (envVars.containsKey("DEBUG") && envVars.get("DEBUG").equalsIgnoreCase("1")) {
			this.shouldExitOnTearDown = false;
		}
	}

	@After
	public void tearDown() {
        if (this.shouldExitOnTearDown) {
           this.driver.quit();
        }
	}

	/**
	 * 读取主页
	 */
	protected void loadPage() {
		this.loadPage(this.url);
	}

	/**
	 * 读取主页
	 */
	protected void loadPage(String url) {
		this.driver.get(url);
		assertTrue("读取主页失败", this.driver.getTitle().contains("惠金所"));
	}

	/**
	 * 登录
	 */
	protected void login() {
		this.driver.findElement(By.className("login_tab_but")).click();

		this.driver.findElement(By.id("email")).sendKeys(this.username);
		this.driver.findElement(By.id("password")).sendKeys(this.password);
		this.driver.findElement(By.id("code_temp")).sendKeys("1234");
		this.driver.findElement(By.id("btn_login")).click();

		this.wait.until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver webDriver) {
				return webDriver.findElement(By.className("userName"));
			}
		});

		String name = this.driver.findElement(By.className("userName")).getText();
		assertTrue("登录失败", name.equalsIgnoreCase(this.username));
	}

    public void navigateToPage(String name) {
        this.driver.findElement(By.linkText(name)).click();
        this.wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='topBox']/ul/li/a[contains(@class, 'hover')]"), name));
    }

	/**
	 * 处理不同window之间的跳转
	 */
	protected WebDriver switchToWindow(String title) {
		return this.switchToWindow(title, false);
	}
	
	protected WebDriver switchToWindow(String title, Boolean exactMatch) {
		Set<String> handles = this.driver.getWindowHandles(); // 获取所有存在的视窗
		
		for (String handle : handles) { // 通过title比对
			WebDriver driver = this.driver.switchTo().window(handle);
			if (!exactMatch &&  driver.getTitle().contains(title))
				return driver; // 返回找到的视窗驱动
			else if (exactMatch && driver.getTitle().equalsIgnoreCase(title))
				return driver; //返回完整匹配的视窗驱动
		}

		return this.driver;
	}

    protected void setCalendarFromDate(String date) {
        JavascriptExecutor removeAttribute = (JavascriptExecutor)this.driver;
        removeAttribute.executeScript("var setDate=document.getElementById(\"startTime\");setDate.removeAttribute('readonly');");
        this.driver.findElement(By.xpath(".//*[@id='startTime']")).sendKeys(date);
    }

    protected void setCalendarToDate(String date) {
        JavascriptExecutor removeAttribute = (JavascriptExecutor)this.driver;
        removeAttribute.executeScript("var setDate=document.getElementById(\"endTime\");setDate.removeAttribute('readonly');");
        this.driver.findElement(By.xpath(".//*[@id='endTime']")).sendKeys(date);
    }
}