package SecondStage.ticket_手动;

import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

public class 手动卡券发放_兑换码_加息 extends ConsoleBaseTest{
    private void clickDownArrow(int tableNumber, int rowNumber, int columnNumber) {
        this.driver.findElement(By.xpath(".//*[@id='show_form']/table[" + tableNumber + "]/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
    }

    private WebElement findTextField(int tableNumber, int rowNumber, int columnNumber) {
        return this.driver.findElement(By.xpath(".//*[@id='show_form']/table[" + tableNumber + "]/tbody/tr[" + rowNumber +
                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
    }
    protected String name() {
        return "兑换加息-自动化测试";
    }
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='11']")).click();
    }
	@Test
    public void 上传用户列表_代金() throws Exception {
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[5]/ul/li[4]/div/span[4]")));

        //点击卡券手动发送
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[5]/ul/li[4]/div/span[4]")).click();

        // 等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.linkText("增加")).getText();
                return text2.equalsIgnoreCase("增加");
            }
        });

        //点击增加
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        this.driver.findElement(By.linkText("增加")).click();

        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='show_form']/table[1]/tbody/tr[1]/td[1]")));
        String text2 = driver.findElement(By.xpath(".//*[@id='show_form']/table[1]/tbody/tr[1]/td[1]")).getText().substring(0, 5);
        assertTrue("新增项目信息页面加载失败", text2.equalsIgnoreCase("投资券编号"));

        //投资券名称
        this.driver.findElement(By.id("ticket_name")).sendKeys(name());
        
        //发送对象选择
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='show_form']/table[1]/tbody/tr[2]/td[4]/span/span/span")));
        WebElement textField = this.findTextField(1, 2, 4);
        this.clickDownArrow(1, 2, 4);
        textField.click();
        textField.sendKeys(Keys.ARROW_UP);
        textField.sendKeys(Keys.ENTER);


        //投资券类型
        this.clickDownArrow(1, 2, 2);
        textField = this.findTextField(1, 2, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_UP);
        textField.sendKeys(Keys.ENTER);
        
        //适用商品大类
        this.clickDownArrow(1, 4, 4);
        textField = this.findTextField(1, 4, 4);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ARROW_UP);
        textField.sendKeys(Keys.ARROW_UP);
        this.driver.findElement(By.id("ticket_name")).click();
        
        //使用渠道
        this.clickDownArrow(1, 4, 2);
        textField = this.findTextField(1, 4, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);	
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ARROW_DOWN);
//        textField.sendKeys(Keys.ENTER);	
        this.driver.findElement(By.id("ticket_name")).click();

        //兑换码有效日起始日----当天
		this.driver.findElement(By.xpath(".//*[@id='show_form']/table[1]/tbody/tr[8]/td[2]/span/span/span")).click();
      	Date dNow = new Date();   //当前时间
      	Date dAfter = new Date();
      	Calendar calendar = Calendar.getInstance(); //得到日历
      	calendar.setTime(dNow);//把当前时间赋给日历
      	SimpleDateFormat sdf=new SimpleDateFormat("yyyy,M,d"); //设置时间格式
      	String defaultStartDate0 = sdf.format(dNow);    //格式化当天日期
      	String date = "//td[@abbr='" + defaultStartDate0 + "']";
      	System.out.println(date);
      	
      	List<WebElement> xDate = this.driver.findElements(By.xpath(date));
    	xDate.get(4).click();
      	
      	//兑换码有效日结束日----十天后
      	this.driver.findElement(By.xpath(".//*[@id='show_form']/table[1]/tbody/tr[8]/td[4]/span/span/span")).click();
      	calendar.setTime(dNow);//把当前时间赋给日历
      	calendar.add(Calendar.DAY_OF_MONTH, +10);  //加10天
      	dAfter = calendar.getTime();   //得到10天后时间

      	String defaultStartDate = sdf.format(dAfter);    //格式化后10天
      	
      	String date2 = "//td[@abbr='" + defaultStartDate + "']";
      	System.out.println(date2);
      	List<WebElement> selectDate = this.driver.findElements(By.xpath("//div[@class = 'calendar-nextmonth']"));
        try{
	      	//选择当月
        	List<WebElement> xDate2 = this.driver.findElements(By.xpath(date2));
        	xDate2.get(5).click();
        }catch(Exception e){
          	//选择下个月
          	selectDate.get(5).click();

          	this.driver.findElement(By.xpath(date2)).click();	
        }
      	
        //收益率
        this.driver.findElement(By.id("ticket_rate")).sendKeys("1.9");
        
        //兑换码数量
        this.driver.findElement(By.id("code_amt")).sendKeys("5");
        
        //生效时间
        this.clickDownArrow(1, 10, 2);
        textField = this.findTextField(1, 10, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //失效时间
        this.clickDownArrow(1, 11, 2);
        textField = this.findTextField(1, 11, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //失效天数
        this.driver.findElement(By.id("invalid_days")).sendKeys("5");
        
        //是否开启状态
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='show_form']/table[1]/tbody/tr[13]/td[2]/span/span/span")));

        this.clickDownArrow(1, 13, 2);
        textField = this.findTextField(1, 13, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);

        //获取来源
        this.driver.findElement(By.id("remark")).sendKeys("兑换");

        //使用条件说用
        this.driver.findElement(By.id("use_condition")).sendKeys("无");

        //点击发送
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("发送")));

        this.driver.findElement(By.linkText("发送")).click();

        // 等待弹窗
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text2 = webDriver.findElement(By.xpath("html/body/div[22]/div[2]/div[2]")).getText();
                return text2.equalsIgnoreCase("您创建的投资券面值超出阈值，请确认是否继续？");
            }
        });

        this.driver.findElement(By.xpath("//div[contains(@class, 'messager-window')]//span[@class='l-btn-left']")).click();
        
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text2 = webDriver.findElement(By.xpath("html/body/div[22]/div[2]/div[2]")).getText();
                return text2.equalsIgnoreCase("操作成功");
            }
        });
//        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'messager-window')]")));
        this.driver.findElement(By.xpath("//div[contains(@class, 'messager-window')]//span[@class='l-btn-left']")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@class, 'messager-window')]")));
	}
}
