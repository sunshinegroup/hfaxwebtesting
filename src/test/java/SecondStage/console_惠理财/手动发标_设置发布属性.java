package SecondStage.console_惠理财;

import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

public class 手动发标_设置发布属性 extends ConsoleBaseTest {
	private void clickDownArrow(int rowNumber, int columnNumber) {
		this.driver.findElement(By.xpath(".//*[@id='addform']//table/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
	}

	private WebElement findTextField(int rowNumber, int columnNumber) {
		return this.driver.findElement(By.xpath(".//*[@id='addform']//table/tbody/tr[" + rowNumber +
				"]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
	}

	private void clickDownArrowByColumn(int columnNumber) {
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td["+columnNumber+"]/div/table/tbody/tr/td//span[contains(@class, 'combo-arrow')]")).click();
	}

	private WebElement findTextFieldByColumn(int columnNumber) {
		return this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td["+columnNumber+"]/div/table/tbody/tr/td//input[contains(@class, 'combo-text')]"));

	}
    protected String projectName() {
        return "惠手动定向溢价";
    }

	/**
	 * @throws Exception
	 */
	@Test
	public void 设置发布属性() throws Exception{
	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[3]/div/span[4]")));
	    this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[3]/div/span[4]")).click();
		
	    //等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				driver.switchTo().defaultContent();
				driver.switchTo().frame(0);
				String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
				return text2.equalsIgnoreCase("1");
			}
		});
		
		//检查新增项目位置
		String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
		if (textSearch.compareTo(projectName())!=0){
			//查询项目
			this.driver.findElement(By.id("q_project_name")).sendKeys(projectName());
			this.driver.findElement(By.linkText("查询")).click();
				
			this.wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver webDriver) {
					String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
					return text2.equalsIgnoreCase(projectName());
				}
			});
		}

		//选择项目
		this.findValueAtIndexRow(0, 0).click();
		
		//点击设置发布属性----项目发布设置
		this.driver.findElement(By.xpath(".//*[@class='l-btn-text icon-preferences l-btn-icon-left']")).click();
		//切换至父iframe
		this.driver.switchTo().defaultContent();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[3]/a[1]/span[1]")));
		//关闭过审项目查询
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				webDriver.findElement(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[2]")).click();
				String text2 = webDriver.findElement(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[1]/span[1]")).getText();
				//System.out.println(text2);
				return text2.equalsIgnoreCase("项目发布设置");
			}
		});

		//切换至项目发布设置frame
		this.driver.switchTo().frame(0);
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='addform']/div[1]/table/tbody/tr[2]/td[4]/span/span/span")));
		String text3 = this.driver.findElement(By.xpath(".//*[@id='addform']/div[1]/table/tbody/tr[1]/td[1]")).getText();
		//System.out.println(text3);
		assertTrue("项目发布设置页面加载失败", text3.equals("项目编号："));
	

		//总募集周期起始日期--当天
		this.driver.findElement(By.xpath(".//*[@id='addform']/div[1]/table/tbody/tr[4]/td[2]/span/span/span")).click();
      	Date dNow = new Date();   //当前时间
      	Date dAfter = new Date();
      	Calendar calendar = Calendar.getInstance(); //得到日历
      	calendar.setTime(dNow);//把当前时间赋给日历
      	SimpleDateFormat sdf=new SimpleDateFormat("yyyy,M,d"); //设置时间格式
      	String defaultStartDate0 = sdf.format(dNow);    //格式化当天日期
      	String date = "//td[@abbr='" + defaultStartDate0 + "']";
      	System.out.println(date);
      	
      	this.driver.findElement(By.xpath(date)).click();

		//设置发布模式
		this.clickDownArrow(2, 4);
		WebElement textField = this.findTextField(2, 4);
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);
		
		//是否立即发布
		this.clickDownArrow(3, 2);
		textField = this.findTextField(3, 2);
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);

	    
		//总募集周期截止日期--两天后
		this.driver.findElement(By.xpath(".//*[@id='addform']/div[1]/table/tbody/tr[4]/td[4]/span/span/span")).click();
      	calendar.setTime(dNow);//把当前时间赋给日历
      	calendar.add(Calendar.DAY_OF_MONTH, +2);  //加2天
      	dAfter = calendar.getTime();   //得到2天后时间

      	String defaultStartDate = sdf.format(dAfter);    //格式化后2天
      	
      	String date2 = "//td[@abbr='" + defaultStartDate + "']";
      	System.out.println(date2);
      	List<WebElement> selectDate = this.driver.findElements(By.xpath("//div[@class = 'calendar-nextmonth']"));
        try{
	      	//选择当月
        	List<WebElement> xDate2 = this.driver.findElements(By.xpath(date2));
        	xDate2.get(3).click();
        	this.driver.findElement(By.linkText("确定")).click();
        }catch(Exception e){
          	//选择下个月
          	selectDate.get(3).click();

          	this.driver.findElement(By.xpath(date2)).click();	
          	this.driver.findElement(By.linkText("确定")).click();
        }
		
		//标的处理
		this.clickDownArrow(7, 2);
		textField = this.findTextField(7, 2);
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);
		
		//销售模式
		this.clickDownArrow(8, 2);
		textField = this.findTextField(8, 2);
		textField.click();
		textField.sendKeys(Keys.ARROW_DOWN);
		textField.sendKeys(Keys.ENTER);
		
		//点击保存
		this.driver.findElement(By.xpath(".//*[@id='addform']/div[2]/a/span/span")).click();

		//点击确定
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		WebElement confirmButton = this.driver.findElement(By.linkText("确定"));
		confirmButton.click();
		try{
			this.driver.findElement(By.linkText("确定")).isDisplayed();
			this.driver.findElement(By.linkText("确定")).sendKeys(Keys.ENTER);
		}catch(Exception e){
		}

		//等待拆分规则
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='div_splity']/div/div[1]/div[1]")));
	    String text5 = this.driver.findElement(By.xpath(".//*[@id='div_splity']/div/div[1]/div[1]")).getText();
		assertTrue("拆分规则加载失败", text5.equals("设置拆分规则"));
		
		//点击增加
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='window-mask']")));
		this.driver.findElement(By.xpath(".//*[@id='div_splity']/div/div[2]/div[1]/table/tbody/tr/td[2]/a/span/span")).click();
		
		//等待内容加载
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")));
		
		//点击对勾
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")).click();
		
		//输入优先级
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[4]/div/table/tbody/tr/td/input[1]")).sendKeys("1");
		
		//输入标的名称
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[5]/div/table/tbody/tr/td/input")).sendKeys(projectName());
		
		//输入单个项目金额
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[6]/div/table/tbody/tr/td/input[1]")).sendKeys("10000");
		
		//输入起投金额
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[7]/div/table/tbody/tr/td/input[1]")).sendKeys("1000");
		
		//输入投递递增金额
//		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[8]/div/table/tbody/tr/td/input[1]")).sendKeys("100");

		//输入最大投资金额
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[8]/div/table/tbody/tr/td/input[1]")).sendKeys("10000");

		//选择销售模式
		this.clickDownArrowByColumn(9);
		WebElement textFieldByColumn = this.findTextFieldByColumn(9);
		textFieldByColumn.click();
		textFieldByColumn.sendKeys(Keys.ARROW_DOWN);
		textFieldByColumn.sendKeys(Keys.ARROW_DOWN);
		textFieldByColumn.sendKeys(Keys.ENTER);

		//输入基础收益率
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[10]/div/table/tbody/tr/td/input[1]")).sendKeys("28");
		
		//输入促销收益率
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[11]/div/table/tbody/tr/td/input[1]")).sendKeys("0.5");
	
		//输入管理费率
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[12]/div/table/tbody/tr/td/input[1]")).sendKeys("1");
		
		//选择是否前台展示
		this.clickDownArrowByColumn(13);
		textFieldByColumn = this.findTextFieldByColumn(13);
		textFieldByColumn.click();
		textFieldByColumn.sendKeys(Keys.ARROW_DOWN);
		textFieldByColumn.sendKeys(Keys.ENTER);

		//选择惠理财分类
		this.clickDownArrowByColumn(14);
		textFieldByColumn = this.findTextFieldByColumn(14);
		textFieldByColumn.click();
		textFieldByColumn.sendKeys(Keys.ARROW_DOWN);
		textFieldByColumn.sendKeys(Keys.ENTER);

		//选择转让属性
		this.clickDownArrowByColumn(15);
		textFieldByColumn = this.findTextFieldByColumn(15);
		textFieldByColumn.click();
		textFieldByColumn.sendKeys(Keys.ARROW_DOWN);
		textFieldByColumn.sendKeys(Keys.ENTER);

	    //转让锁定期
	    this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[16]/div/table/tbody/tr/td/input[1]")).sendKeys("1");
	    
		//选择是否热卖
		this.clickDownArrowByColumn(17);
		textFieldByColumn = this.findTextFieldByColumn(17);
		textFieldByColumn.click();
		textFieldByColumn.sendKeys(Keys.ARROW_UP);
		textFieldByColumn.sendKeys(Keys.ENTER);
		
		//选择是否使用优惠活动
		this.clickDownArrowByColumn(18);
		textFieldByColumn = this.findTextFieldByColumn(18);
		textFieldByColumn.click();
		textFieldByColumn.sendKeys(Keys.ARROW_DOWN);
		textFieldByColumn.sendKeys(Keys.ENTER);

		//项目特点1
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[19]/div/table/tbody/tr/td/input")).sendKeys("项目特点1");
		
		//项目特点描述1
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[20]/div/table/tbody/tr/td/input")).sendKeys("项目特点描述1");
		
		//项目特点2
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[21]/div/table/tbody/tr/td/input")).sendKeys("项目特点2");
		
		//项目特点描述2
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[22]/div/table/tbody/tr/td/input")).sendKeys("项目特点描述2");
		
		//项目特点3
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[23]/div/table/tbody/tr/td/input")).sendKeys("项目特点3");
		
		//项目特点描述3
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[24]/div/table/tbody/tr/td/input")).sendKeys("项目特点描述3");
		
		//点击保存
		this.driver.findElement(By.xpath(".//*[@id='div_splity']/div/div[2]/div[1]/table/tbody/tr/td[10]/a/span/span")).click();
		 
		//点击确定
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		this.driver.findElement(By.linkText("确定")).click();
		
		//点击对勾
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='window-mask']")));
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div/input")).click();

		//点击项目发布渠道
		this.driver.findElement(By.xpath(".//*[@id='div_splity']/div/div[2]/div[1]/table/tbody/tr/td[8]/a/span/span")).click();

		//等待页面加载----点击增加
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='channelWin']/div/div/div/div/div[1]/table/tbody/tr/td[2]/a/span/span")));
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
		this.driver.findElement(By.xpath(".//*[@id='channelWin']/div/div/div/div/div[1]/table/tbody/tr/td[2]/a/span/span")).click();
		
		//点击对勾
//		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[1]/div/input")));
//		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[1]/div/input")).click();

		//选择渠道编号
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[4]/div/table/tbody/tr/td//span[contains(@class, 'combo-arrow')]")).click();
		textFieldByColumn = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[4]/div/table/tbody/tr/td//input[contains(@class, 'combo-text')]"));
		textFieldByColumn.click();
		textFieldByColumn.sendKeys(Keys.ARROW_DOWN);
		textFieldByColumn.sendKeys(Keys.ARROW_DOWN);
		textFieldByColumn.sendKeys(Keys.ARROW_DOWN);
		textFieldByColumn.sendKeys(Keys.ENTER);

		//点击保存
		this.driver.findElement(By.xpath(".//*[@id='channelWin']/div/div/div/div/div[1]/table/tbody/tr/td[8]/a/span/span")).click();
		
		//点击确定
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		this.driver.findElement(By.linkText("确定")).click();
		
		//等待页面加载----点击增加
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
		this.driver.findElement(By.xpath(".//*[@id='channelWin']/div/div/div/div/div[1]/table/tbody/tr/td[2]/a/span/span")).click();
		
		//选择渠道编号
		this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[4]/div/table/tbody/tr/td//span[contains(@class, 'combo-arrow')]")).click();
		textFieldByColumn = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[4]/div/table/tbody/tr/td//input[contains(@class, 'combo-text')]"));
		textFieldByColumn.click();
		textFieldByColumn.sendKeys(Keys.ARROW_DOWN);
		textFieldByColumn.sendKeys(Keys.ARROW_DOWN);
		textFieldByColumn.sendKeys(Keys.ENTER);

		//点击保存
		this.driver.findElement(By.xpath(".//*[@id='channelWin']/div/div/div/div/div[1]/table/tbody/tr/td[8]/a/span/span")).click();
		
		//点击确定
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		this.driver.findElement(By.linkText("确定")).click();
		
		//点击关闭
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
		this.driver.findElement(By.xpath("//a[@class='panel-tool-close']")).click();

		//点击提交审核
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//a[@class='panel-tool-close']")));
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
		this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='window-mask']")));
		this.driver.findElement(By.xpath(".//*[@id='pass']/span/span")).click();

		//点击确定
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
		this.driver.findElement(By.linkText("确定")).click();
	}
}
