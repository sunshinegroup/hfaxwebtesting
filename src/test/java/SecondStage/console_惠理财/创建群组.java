package SecondStage.console_惠理财;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.Keys;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

public class 创建群组 extends ConsoleBaseTest{
    protected String groupName() {
        return "寿险";
    }
	@Test
	public void 创建群组() throws Exception{
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[6]/ul/li[4]/div/span[4]")));
	    this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[6]/ul/li[4]/div/span[4]")).click();
		
	    //等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				driver.switchTo().defaultContent();
				driver.switchTo().frame(0);
				String text2 = webDriver.findElement(By.xpath(".//*[@id='add']/span/span")).getText();
				return text2.equalsIgnoreCase("新增");
			}
		});
		
		//检查群组是否存在
        this.driver.findElement(By.id("group_name")).sendKeys(groupName());
        this.driver.findElement(By.linkText("查询")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        
        try{
//        	String text2 = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[1]/div")).getText();
//        	assertTrue("", text2.equals("寿险"));
            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[1]/div")).getText();
                    return text2.equalsIgnoreCase(groupName());
                }
            });
        	System.out.println("寿险群组已存在。");
        }catch(Exception e){
        	System.out.println("寿险群组不存在，开始创建。");
        
			//点击新增
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.driver.findElement(By.linkText("新增")).click();
	        
	        //输入群组名称
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("group_names")));
	        this.driver.findElement(By.id("group_names")).sendKeys(groupName());
	        
	        //输入群组说明
	        this.driver.findElement(By.id("remarks")).sendKeys(groupName());
	        
	        //输入累计购买次数
	        this.driver.findElement(By.id("limcnt")).sendKeys("1");
	        
	        //输入累计购买金额限制
	        this.driver.findElement(By.id("limtrm")).sendKeys("5000");
	        
	        //输入人员类别
	        this.driver.findElement(By.id("s2id_autogen1")).sendKeys(groupName());
	        this.driver.findElement(By.id("s2id_autogen1")).sendKeys(Keys.ENTER);
	        
	        //集合类型
	        this.driver.findElement(By.xpath(".//*[@id='add_form']/table/tbody/tr[7]/td[2]/span/span/span")).click();
	        this.driver.findElement(By.xpath(".//*[@id='add_form']/table/tbody/tr[7]/td[2]/span/input[1]")).sendKeys(Keys.UP);
	        this.driver.findElement(By.xpath(".//*[@id='add_form']/table/tbody/tr[7]/td[2]/span/input[1]")).sendKeys(Keys.ENTER);
	        
	        //点击保存
	        this.driver.findElement(By.linkText("确定")).click();
	        
	        //点击确定
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	String text5 = driver.findElement(By.xpath("html/body/div[23]/div[2]/div[2]")).getText();
	                return text5.equalsIgnoreCase("操作成功");
	            }
	        });
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.driver.findElement(By.xpath("html/body/div[23]/div[2]/div[4]/a/span/span")).click();
        }
	}
}
