package SecondStage.console_惠理财;

import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

public class 手动发标_新手标录入 extends ConsoleBaseTest{
	 private void clickDownArrow(int tableNumber, int rowNumber, int columnNumber) {
	        this.driver.findElement(By.xpath(".//*[@id='add_form']/table[" + tableNumber + "]/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
	    }

	    private WebElement findTextField(int tableNumber, int rowNumber, int columnNumber) {
	        return this.driver.findElement(By.xpath(".//*[@id='add_form']/table[" + tableNumber + "]/tbody/tr[" + rowNumber +
	                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
	    }

	    protected String lookupName() {
	        return "liang100010";
	    }
	    protected String projectName() {
	        return "新手收益非溢价";
	    }


	    @Test
	    public void 新增标的() throws Exception {
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[1]/div/span[4]")));

	        //点击项目维护
	        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[1]/div/span[4]")).click();
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[1]/span[1]")));
	        // 等待页面加载
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                driver.switchTo().defaultContent();
	                driver.switchTo().frame(0);
	                String text2 = webDriver.findElement(By.linkText("增加")).getText();
	                return text2.equalsIgnoreCase("增加");
	            }
	        });  
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='window-mask']")));
	        
	        //点击增加
	        this.driver.findElement(By.linkText("增加")).click();

	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='add_form']/h2[1]")));
	        String text2 = driver.findElement(By.xpath(".//*[@id='add_form']/h2[1]")).getText();
	        assertTrue("新增项目信息页面加载失败", text2.equalsIgnoreCase("项目基础信息"));

	        //产品分类
	        this.clickDownArrow(1, 1, 2);
	        WebElement textField = this.findTextField(1, 1, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ENTER);

	        //项目名称
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("project_name")));
//	        this.driver.findElement(By.id("project_name")).click();
	        this.driver.findElement(By.id("project_name")).sendKeys(projectName());

	        //交易模式
	        this.clickDownArrow(1, 2, 2);
	        textField = this.findTextField(1, 2, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_UP);
	        textField.sendKeys(Keys.ENTER);

	        //资产总额 
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("loan_amout")));
//	        this.driver.findElement(By.id("loan_amout")).click();
	        this.driver.findElement(By.id("loan_amout")).sendKeys("10000");

	        //资产成立日 	  选择当前日期前1天
	        this.clickDownArrow(1, 2, 6);
	        textField = this.findTextField(1, 2, 6);
	        textField.click();
	        this.beforeDate(10,1);
		    
	        //资产到期日 	  选择当前日期+100天
	        this.clickDownArrow(1, 3, 2);
	        textField = this.findTextField(1, 3, 2);
	        textField.click();
	        this.afterDate(11,100);
	        
	        //产品利率
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("year_intr")));
	        this.driver.findElement(By.id("year_intr")).sendKeys("29");

	        //宽限天数
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("grace_period")));
	        this.driver.findElement(By.id("grace_period")).sendKeys("3");

	        //项目概况标题1
	        this.driver.findElement(By.id("desc_title1")).sendKeys("项目概况标题1");

	        //项目概况内容1
	        this.driver.findElement(By.id("desc_cont1")).sendKeys("项目概况内容1");

	        //是否溢价
	        this.clickDownArrow(1, 4, 2);
	        textField = this.findTextField(1, 4, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_UP);
	        textField.sendKeys(Keys.ENTER);

	        //产品投向情况介绍
	        this.driver.findElement(By.id("prod_to_dc")).sendKeys("产品投向情况介绍");

	        //保障类型————担保
	        this.clickDownArrow(4, 1, 2);
	        textField = this.findTextField(4, 1, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ENTER);
	        
	        //托管银行
	        this.driver.findElement(By.id("grace_period")).click();//修正托管银行点击无响应
	        this.clickDownArrow(5, 1, 2);
	        textField = this.findTextField(5, 1, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ENTER);

	        //银行账号
	        this.driver.findElement(By.id("bank_no")).sendKeys("6211234567890");

	        //开户银行支行
	        this.driver.findElement(By.id("open_bank")).sendKeys("中国工商银行五道口");

	        //查询
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='showMemberList']/span/span")));

	        //刷新查询列表
	        driver.findElement(By.id("membcd")).sendKeys("111111111");
	        driver.findElement(By.xpath(".//*[@id='showMemberList']/span/span")).click();
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[18]/div[1]/div[2]/a")));
	        driver.findElement(By.xpath("html/body/div[18]/div[1]/div[2]/a")).click();
	        
	        //输入企业名称
	        final String lookupName = this.lookupName();
	        driver.findElement(By.id("membcd")).sendKeys(lookupName);

	        //点击查询
	        driver.findElement(By.xpath(".//*[@id='showMemberList']/span/span")).click();

	        //等待页面加载
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[2]/div")).getText();
	                return text2.equalsIgnoreCase(lookupName);
	            }
	        });
	        
	        this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r2-2-0']/td[9]/div")).click();

	        //保存
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("保存")).click();
	        
	        this.wait.until(new ExpectedCondition<Boolean>() {
         @Override
         public Boolean apply(WebDriver webDriver) {
         	String text5 = driver.findElement(By.xpath("html/body/div[21]/div[2]/div[2]")).getText();
             return text5.equalsIgnoreCase("操作已完成");
         	}
	        });

	        //点击确定
	        this.driver.findElement(By.linkText("确定")).click();
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));


	        //检查新增项目位置
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='window-mask']")));
	        String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
	        if (textSearch.compareTo(projectName()) != 0) {
	            //查询项目
	            this.driver.findElement(By.id("q_project_name")).sendKeys(projectName());
	            this.driver.findElement(By.linkText("查询")).click();

	            final String consoleProjectName = projectName();
	            this.wait.until(new ExpectedCondition<Boolean>() {
	                @Override
	                public Boolean apply(WebDriver webDriver) {
	                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
	                    return text2.equalsIgnoreCase(consoleProjectName);
	                }
	            });
	        }
	        findValueAtIndexRow(0, 0).click();
	        //提交审核
	        this.driver.findElement(By.linkText("提交审核")).click();

	        //点击确定
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
	        this.driver.findElement(By.linkText("确定")).click();
	        
	        //点击确定
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	String text5 = driver.findElement(By.xpath("html/body/div[21]/div[2]/div[2]")).getText();
	                return text5.equalsIgnoreCase("操作已完成");
	            }
	        });
	        this.driver.findElement(By.linkText("确定")).click();
	    }
}