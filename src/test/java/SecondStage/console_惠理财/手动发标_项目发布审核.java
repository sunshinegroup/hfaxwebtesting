package SecondStage.console_惠理财;

import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

import SecondStage.Hfax.Page.DBquery;

public class 手动发标_项目发布审核 extends ConsoleBaseTest {
    protected String projectName() {
        return "惠手动定向溢价";
    }
    @Test
    public void 发布审核() throws ClassNotFoundException, SQLException {
        // 项目发布审核
	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[5]/div/span[4]")));
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[3]/ul/li[5]/div/span[4]")).click();

		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				driver.switchTo().defaultContent();
				driver.switchTo().frame(0);
				String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
				return text2.equalsIgnoreCase("1");
			}
		});

        //查询项目
		String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
		if (textSearch.compareTo(projectName())!=0){
			//查询项目
			String cd = new DBquery().queryData(dbUrl, dbUser, dbPassword, "select project_cd from sunif.sif_project where project_name = '" + projectName() + "' order by project_cd desc", "project_cd");
			System.out.println("项目编号：" + cd);
            this.driver.findElement(By.id("q_project_cd")).sendKeys(cd);
            this.driver.findElement(By.linkText("查询")).click();
				
			this.wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver webDriver) {
					String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
					return text2.equalsIgnoreCase(projectName());
				}
			});
            String text3 = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
            assertTrue("查询功能不正常", text3.equals(projectName()));
		}

        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")));

        List<WebElement> findElements = driver.findElements(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div"));
        assertTrue("获取查询结果失败", findElements.size() > 0);

        //我要审核
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='window-mask']")));
        this.driver.findElement(By.linkText("我要审核")).click();

        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[9]/div[1]/div[1]")));
        String text3 = this.driver.findElement(By.xpath("html/body/div[9]/div[1]/div[1]")).getText();
        assertTrue("加载项目发布信息审核页面失败", text3.equals("项目发布信息审核"));

        this.driver.findElement(By.id("eff_desc")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.className("tooltip-content")));
        assertTrue("错误提示未加载出来", this.driver.findElement(By.className("tooltip-content")).getText().equals("该输入项为必输项"));

        this.driver.findElement(By.xpath(".//*[@id='tt']/div[1]/div[3]/ul/li[3]/a")).click();
        this.driver.findElement(By.id("eff_desc")).sendKeys("OK");
        this.driver.findElement(By.linkText("审核通过")).click();

        //点击确定
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.panel:nth-child(16) > div:nth-child(1) > div:nth-child(1)")));
        assertTrue("加载弹框信息错误", this.driver.findElement(By.cssSelector("div.panel:nth-child(16) > div:nth-child(1) > div:nth-child(1)")).getText().equals("请确认"));
        this.driver.findElement(By.cssSelector(".messager-button > a:nth-child(1) > span:nth-child(1) > span:nth-child(1)")).click();
    
        //点击确定
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath("html/body/div[15]/div[2]/div[2]")).getText();
                return text5.equalsIgnoreCase("审核已通过");
            }
        });
        this.driver.findElement(By.linkText("确定")).click();
    }
}

