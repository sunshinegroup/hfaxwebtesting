package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.hfax.selenium.essential.ZhuCe;
/**
 *  
 * @Description 惠理财寿险续期业务员注册
 * Created by songxq on 12/16/16.
 * 
 */
public class HuiLiCaiSxXQywyRegister extends ZhuCe{

	 //惠理财寿险续期业务员登录用户名
	 @Override
	  protected String userName() {
	        return this.hlcsxXQywyUsername;
	    }
	  //惠理财寿险续期业务员登录密码
	 @Override
	  protected String userPassword(){
		  	return this.hlcsxXQywyPassword;
	  }
	  //惠理财寿险续期业务员手机号
	 @Override
	  protected String userPhone(){
		  	return this.hlcsxXQywyPhone;
	  }
	  //惠理财寿险续期业务员姓名
	 @Override
	  protected String userRealName(){
		    return this.hlcsxXQywyName;
	  }
	  //惠理财寿险续期业务员身份证号
	 @Override
	  protected String userIdentification(){
		  return this.hlcsxXQywyIdentification;
	  }
	  
	  //惠理财寿险续期业务员邀请码
	  protected String userInvitationCode()throws ClassNotFoundException, SQLException{
		  return "";
	  }
}
