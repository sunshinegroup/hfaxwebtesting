package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.hfax.selenium.essential.BangKa;
/**
 *  
 * @Description 惠理财寿险业务员绑卡
 * Created by songxq on 12/14/16.
 * 
 */
public class HuiLiCaiSxywyBangKa extends BangKa{
	//清除惠理财寿险业务员的站内信
	 DBquery db = new DBquery();
	
	 
	 //惠理财寿险业务员登录
	   @Override
		protected void userLogin() throws ClassNotFoundException, SQLException{
		   db.updateData(this.dbUrl,this.dbUser,this.dbPassword,"delete from cbmain_user.knb_mail b where b.custpt ="+"'"+this.hlcsxywyUsername+"'");
			this.loadPage();
	        this.HLClogin(this.hlcsxywyUsername, this.hlcsxywyPassword,this.hlcsxywyPhone);
		}
	    //惠理财寿险业务员卡号
	   @Override
		protected String userCard(){
			return this.hlcsxywyCard;
		}
		//惠理财寿险业务员手机号
	   @Override
		protected String userPhone(){
			return this.hlcsxywyPhone;
		}
		 //惠理财寿险业务员姓名
	    @Override
		 protected String userRealName(){
			    return this.hlcsxywyName;
		 }

}
