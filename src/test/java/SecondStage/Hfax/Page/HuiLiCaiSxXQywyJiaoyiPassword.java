package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.JiaoYiMiMa;
/**
 *  
 * @Description 惠理财寿险【续期】业务员交易密码
 * Created by songxq on 12/19/16.
 * 
 */
public class HuiLiCaiSxXQywyJiaoyiPassword extends JiaoYiMiMa{

			//惠理财寿险【续期】业务员登录
			@Override
			protected void userLogin(){
				this.loadPage();
			    this.HLClogin(this.hlcsxXQywyUsername,this.hlcsxXQywyPassword,this.hlcsxXQywyPhone);
			}
			//惠理财寿险【续期】业务员交易密码
			@Override
			protected String userJiaoyimima(){
				return this.hlcsxXQywyJiaoyipassword;
			}

}
