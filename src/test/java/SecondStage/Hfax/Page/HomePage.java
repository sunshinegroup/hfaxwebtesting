package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;

import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.regression.account.AccountBaseTest;


/**
 *  
 * @Description 首页
 * Created by songxq on 8/11/16.
 * 
 */
public class HomePage extends BasePage{
	
	
	public HomePage(WebDriver driver,FluentWait<WebDriver> wait) {
		super(driver,wait);
		
	}

	//验证收益惠投资的UI
	public void verifyGftext() throws Exception{

		WebElement gftextfirst = this.driver.findElement(By.xpath("//div[@class='carousel_htz']/h2"));
		System.out.println("首页惠投资文本1为：" + gftextfirst.getText().trim());
		assertTrue("首页惠投资文本1内容显示不正确",gftextfirst.getText().trim().equals("惠投资"));
		
		List<WebElement> list = this.driver.findElements(By.xpath("//p[@class='intro']")); 
		System.out.println("首页惠投资文本2为：" + list.get(0).getText().trim());
		assertTrue("首页惠投资文本2内容显示不正确",list.get(0).getText().trim().contains("浮动收益有追求 固定收益有保障")); 
		
		WebElement gftextthird = this.driver.findElement(By.xpath("//div[@class='carousel_htz']/p/span/a"));
		System.out.println("首页惠投资文本3为：" + gftextthird.getText().trim());
        assertTrue("首页惠投资文本3内容显示不正确",gftextthird.getText().trim().equals("更多"));		
	
	}
	//点击首页更多
	public void clickGDButton(){
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@class='carousel_htz']/p/span/a")).isDisplayed();
	            }
	        });
		this.driver.findElement(By.xpath("//div[@class='carousel_htz']/p/span/a")).click();
	}
	
	//获取首页惠理财产品显示名称
	public String getProName(int number,String xpath){
		List<WebElement> list = this.driver.findElements(By.xpath(xpath)); 
		String proName = list.get(number).getText().trim();
		return proName;
	}
	//获取首页新手标产品名称
	public String getNewProName(String xpath){
		String text = this.driver.findElement(By.xpath(xpath)).getText().trim();
		return text;
	}
	//惠理财首页，产品名称
	public void verifyProName(String proName,String dbproName){
		if(proName.equals(dbproName)){
			System.out.println(proName);
			System.out.println(dbproName);
			assertTrue("首页显示的产品名称不正确",proName.equals(dbproName));
		}else{
			fail("首页产品名称不正确");
		}
		
	}
	//惠理财首页项目特点
	public void verifyProtitle(int number,String text,String xpath1,String xpath2){
		List<WebElement> list = this.driver.findElements(By.xpath(xpath1));
		 WebElement item = list.get(number).findElement(By.xpath(xpath2));
		 System.out.println("项目特点文本为：" + item.getText().trim());
		 if(item.getText().trim().equals(text)){
			 System.out.println("项目特点文本为：" + item.getText().trim());
		 }else{
			 fail("项目特点信息不正确");
		 }
	}
	//惠理财产品详细信息，eg.预计年化收益率,投资期限,起投金额,进度条，标的状态
	public void verifyProDetails(int number,String text,String xpath){
		
		List<WebElement> list = this.driver.findElements(By.xpath(xpath));
		System.out.println(list.get(number).getText().trim());
		if(list.get(number).getText().trim().equals(text)){
			System.out.println("信息显示正确");
		}else{
			fail("信息显示不正确");
		}
	}
	
	
	
	

}
