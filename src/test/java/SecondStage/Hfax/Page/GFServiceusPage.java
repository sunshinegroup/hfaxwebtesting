package SecondStage.Hfax.Page;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;

/**
 *  
 * @Description 固浮服务页
 * Created by songxq on 10/11/16.
 * 
 */
public class GFServiceusPage extends BasePage{

	public GFServiceusPage(WebDriver driver, FluentWait<WebDriver> wait) {
		super(driver, wait);
	}
	
	//验证固浮服务页的文本信息
	public void verifyText(){
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//a[@class='hjc_btn']")).isDisplayed();
	            }
	        });
		System.out.println("固浮服务页的面包屑为：" + this.driver.findElement(By.xpath("//div[@class='navBox nav_box']")).getText().trim());
		assertTrue("固浮服务页没有正常跳转",this.driver.findElement(By.xpath("//div[@class='navBox nav_box']")).getText().trim().equals("首页> 惠投资产品"));
         
	}


	


	
	

}
