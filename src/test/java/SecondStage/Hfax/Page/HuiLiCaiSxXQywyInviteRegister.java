package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.hfax.selenium.essential.ZhuCe;
/**
 *  
 * @Description 惠理财寿险【续期】业务员邀请的用户注册
 * Created by songxq on 12/20/16.
 * 
 */
public class HuiLiCaiSxXQywyInviteRegister extends ZhuCe{
	
	 DBquery db = new DBquery();
	 //惠理财寿险续期业务员邀请的用户登录用户名
	 @Override
	  protected String userName() {
	        return this.hlcsxXQywyInviteUsername;
	    }
	  //惠理财寿险续期业务员邀请的用户登录密码
	 @Override
	  protected String userPassword(){
		  	return this.hlcsxXQywyInvitePassword;
	  }
	  //惠理财寿险续期业务员邀请的用户手机号
	 @Override
	  protected String userPhone(){
		  	return this.hlcsxXQywyInvitePhone;
	  }
	  //惠理财寿险续期业务员邀请的用户姓名
	 @Override
	  protected String userRealName(){
		    return this.hlcsxXQywyInviteName;
	  }
	  //惠理财寿险续期业务员邀请的用户身份证号
	 @Override
	  protected String userIdentification(){
		  return this.hlcsxXQywyInviteIdentification;
	  }
	  
	  //惠理财寿险续期业务员邀请的用户邀请码
	  protected String userInvitationCode()throws ClassNotFoundException, SQLException{
		  String inviteCode = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select invi_code from sunif.sif_member a inner join sunif.sif_member_code b on a.member_cd = b.member_cd where a.member_alias ="+"'"+this.hlcsxXQywyUsername+"'","invi_code");
		  System.out.println("寿险【续期】业务员邀请的用户邀请码为："+inviteCode);
		  return inviteCode;
	  }
}
