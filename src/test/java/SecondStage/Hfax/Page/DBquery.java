package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

import com.hfax.selenium.regression.console.ConsoleBaseTest;
/**
 *  
 * @Description 数据库查询
 * Created by songxq on 15/11/16.
 * 
 */

public class DBquery {
	
	public String queryData(String url,String user,String password,String sql,String sqlcolumn) throws ClassNotFoundException, SQLException{
    Connection con = null;// 创建一个数据库连接
    PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
    ResultSet result = null;// 创建一个结果集对象
   
		Class.forName("oracle.jdbc.driver.OracleDriver");
        System.out.println("开始尝试连接数据库！");
        con = DriverManager.getConnection(url, user, password);// 获取连接
        System.out.println("连接成功！");
        pre = con.prepareStatement(sql);// 实例化预编译语句
        result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数
        String resultName = null;
        while (result.next()){
        	resultName = result.getString(sqlcolumn);
        	System.out.println("查询结果为："+resultName);
    	  break; 
        }
        if (result != null)
            result.close();
        if (pre != null)
            pre.close();
        if (con != null)
            con.close();
        System.out.println("数据库连接已关闭！");
        return resultName;
   
 }
	public void updateData(String url,String user,String password,String sql) throws ClassNotFoundException, SQLException{
	    Connection con = null;// 创建一个数据库连接
	    PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
	    int result;// 创建一个结果集对象
	   
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        System.out.println("开始尝试连接数据库！");
	        con = DriverManager.getConnection(url, user, password);// 获取连接
	        System.out.println("连接成功！");
	        pre = con.prepareStatement(sql);// 实例化预编译语句
	        result = pre.executeUpdate();
	       
	        System.out.println("删除的条数为：" + result);
	        if (pre != null)
	            pre.close();
	        if (con != null)
	            con.close();
	        System.out.println("数据库连接已关闭！");
	      
	   
	 }
}
