package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.ChongZhi;
/**
 *  
 * @Description 惠理财寿险业务员充值
 * Created by songxq on 12/14/16.
 * 
 */
public class HuiLiCaiSxywyChongZhi  extends ChongZhi{

	 //惠理财寿险业务员登录
	   @Override
		protected void userLogin(){
				this.loadPage();
		        this.HLClogin(this.hlcsxywyUsername,this.hlcsxywyPassword,this.hlcsxywyPhone);
			}
	  //惠理财寿险业务员充值金额
	   @Override
		protected String userAmount(){
			return this.hlcsxywyAmount;
		}
	   //惠理财寿险业务员交易密码
	   @Override
		protected String userJiaoyimima(){
				return this.hlcsxywyJiaoyipassword;
		}
	   //惠理财寿险业务员用户卡号
	   @Override
		protected String userCard(){
				return this.hlcsxywyCard;
		}
	   //惠理财寿险业务员绑卡使用的银行
	   @Override
		protected String userBankname(){
			return this.hlcsxywyBankname;
		}
	   //个人充值后的资金记录后的收入
	   @Override
	   protected String userIncomeAmount(){
			return "10,000.00";
		}
	   //个人充值后资金记录后的备注金额
		protected String userRemarkAmount(){
			return "[10000.00元]";
		}
	   

}
