package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.JiaoYiMiMa;
/**
 *  
 * @Description 惠理财投资用户交易密码
 * Created by songxq on 01/10/17.
 * 
 */
public class HuiLiCaiInvestUserJiaoyiPassword extends JiaoYiMiMa{

		//惠理财投资的用户登录
		@Override
		protected void userLogin(){
			this.loadPage();
			this.HLClogin(this.huilicaiinvestUsername,this.huilicaiinvestPassword,this.huilicaiinvestPhone);
		}
		//惠理财投资的用户交易密码
		@Override
		protected String userJiaoyimima(){
			return this.huilicaiinvestJiaoyipassword;
		}
}
