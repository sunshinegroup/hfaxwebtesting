package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
/**
 *  
 * @Description 我的红包
 * Created by songxq on 21/11/16.
 * 
 */
public class MyRedBagPage extends BasePage{

	public MyRedBagPage(WebDriver driver, FluentWait<WebDriver> wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}
	//验证数据表信息
	public void verifyTableInfo(String[] expectedHeaders, List<WebElement> headers) {
	System.out.println("expectedHeaders:" + String.valueOf(expectedHeaders.length));
	System.out.println("headers:" + String.valueOf(headers.size()));
    assertTrue("列数不对", expectedHeaders.length == headers.size());
    for (int i = 0; i < expectedHeaders.length; i ++) {
    	System.out.println(headers.get(i).getText().trim());
        assertTrue("列 " + i + " 文字不正确", expectedHeaders[i].equalsIgnoreCase(headers.get(i).getText().trim()));
    }
}

}
