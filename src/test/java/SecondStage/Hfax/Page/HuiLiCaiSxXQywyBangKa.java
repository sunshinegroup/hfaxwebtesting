package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.BangKa;
/**
 *  
 * @Description 惠理财寿险【续期】业务员绑卡
 * Created by songxq on 19/12/16.
 * 
 */
public class HuiLiCaiSxXQywyBangKa extends BangKa{

	 //惠理财寿险【续期】业务员登录
	   @Override
		protected void userLogin(){
			this.loadPage();
	        this.HLClogin(this.hlcsxXQywyUsername,this.hlcsxXQywyPassword,this.hlcsxXQywyPhone);;
		}
	    //惠理财寿险【续期】业务员用户卡号
	   @Override
		protected String userCard(){
			return this.hlcsxXQywyCard;
		}
		//惠理财寿险【续期】业务员用户手机号
	   @Override
		protected String userPhone(){
			return this.hlcsxXQywyPhone;
		}
		 //惠理财寿险【续期】业务员用户真实姓名
	    @Override
		 protected String userRealName(){
	 		return this.hlcsxXQywyName;
		 }

}
