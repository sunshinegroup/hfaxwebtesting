package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hfax.selenium.essential.ZhuCe;

/**
 *  
 * @Description 惠理财用户注册
 * Created by songxq on 8/11/16.
 * 
 */

public class HuiUserRegister extends ZhuCe{

	 //惠理财登录用户
	 @Override
	  protected String userName() {
	        return this.hlUsername;
	    }
	  //惠理财登录密码
	 @Override
	  protected String userPassword(){
		  	return this.hlPassword;
	  }
	  //惠理财登录用户手机号
	 @Override
	  protected String userPhone(){
		  	return this.hlPhone;
	  }
	  //惠理财用户姓名
	 @Override
	  protected String userRealName(){
		    return this.hlName;
	  }
	  //惠理财身份证号
	 @Override
	  protected String userIdentification(){
		  return this.hlIdentification;
	  }
	  
	  //惠理财邀请码
	  protected String userInvitationCode()throws ClassNotFoundException, SQLException{
		  return "";
	  }
}
