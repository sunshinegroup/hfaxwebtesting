package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
/**
 *  
 * @Description 用户设置
 * Created by songxq on 21/11/16.
 * 
 */
public class UserInfoPage extends BasePage{

	public UserInfoPage(WebDriver driver, FluentWait<WebDriver> wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}

	//文本信息验证
	public void verifLetterDetailInfo(String xpath,String content,int number){
		List<WebElement> list = this.driver.findElements(By.xpath(xpath));
		System.out.println(list.get(number).getText().trim());
		if(list.get(number).getText().trim().equals(content)){
			System.out.println("相关数据显示正确:" + list.get(number).getText().trim());
		}else{
			fail("相关数据显示不正确");
		}
	}
	//验证信息
	public void verifyInfo(String xpath,String content){
		System.out.println("界面信息为："+this.driver.findElement(By.xpath(xpath)).getText().trim());
		assertTrue("信息显示不正确",this.driver.findElement(By.xpath(xpath)).getText().trim().equals(content));
	}
}
