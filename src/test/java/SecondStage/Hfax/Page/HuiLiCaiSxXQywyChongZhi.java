package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.ChongZhi;
/**
 *  
 * @Description 惠理财寿险【续期】业务员充值
 * Created by songxq on 19/12/16.
 * 
 */
public class HuiLiCaiSxXQywyChongZhi  extends ChongZhi{

	 //惠理财寿险【续期】业务员登录
	   @Override
		protected void userLogin(){
				this.loadPage();
		        this.HLClogin(this.hlcsxXQywyUsername,this.hlcsxXQywyPassword,this.hlcsxXQywyPhone);
			}
	  //惠理财寿险【续期】业务员充值金额
	   @Override
		protected String userAmount(){
			return this.hlcsxXQywyAmount;
		}
	   //惠理财寿险【续期】业务员交易密码
	   @Override
		protected String userJiaoyimima(){
				return this.hlcsxXQywyJiaoyipassword;
		}
	   //惠理财寿险【续期】业务员用户卡号
	   @Override
		protected String userCard(){
				return this.hlcsxXQywyCard;
		}
	   //惠理财寿险【续期】业务员绑卡使用的银行
	   @Override
		protected String userBankname(){
			return this.hlcsxywyBankname;
		}

}
