package SecondStage.Hfax.Page;

import static org.junit.Assert.*;


import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
/**
 *  
 * @Description 充值提现
 * Created by songxq on 10/11/16.
 * 
 */
public class MyQueryCashRecordListPage extends BasePage{

	public MyQueryCashRecordListPage(WebDriver driver, FluentWait<WebDriver> wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}

	//验证数据表信息
		public void verifyTableInfo(String[] expectedHeaders, List<WebElement> headers,int number) {
	    for (int i = 0; i < number; i ++) {
	    	System.out.println(headers.get(i).getText().trim());
	        assertTrue("列 " + i + " 文字不正确", headers.get(i).getText().trim().contains(expectedHeaders[i]));
	    }
	}
}
