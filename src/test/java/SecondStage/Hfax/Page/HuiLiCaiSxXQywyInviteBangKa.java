package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.BangKa;
/**
 *  
 * @Description 惠理财寿险续期业务员【邀请的用户】绑卡
 * Created by songxq on 12/05/16.
 * 
 */
public class HuiLiCaiSxXQywyInviteBangKa extends BangKa{

	 //惠理财寿险续期业务员【邀请的用户】登录
	   @Override
		protected void userLogin(){
			this.loadPage();
	        this.HLClogin(this.hlcsxXQywyInviteUsername,this.hlcsxXQywyInvitePassword,this.hlcsxXQywyInvitePhone);;
		}
	    //惠理财寿险续期业务员【邀请的用户】用户卡号
	   @Override
		protected String userCard(){
			return this.hlcsxXQywyInviteCard;
		}
		//惠理财寿险续期业务员【邀请的用户】用户手机号
	   @Override
		protected String userPhone(){
			return this.hlcsxXQywyInvitePhone;
		}
		 //惠理财寿险续期业务员【邀请的用户】用户真实姓名
	    @Override
		 protected String userRealName(){
			    return this.hlcsxXQywyInviteName;
		 }


}
