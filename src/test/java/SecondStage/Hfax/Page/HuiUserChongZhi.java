package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.ChongZhi;

/**
 *  
 * @Description 惠理财用户充值
 * Created by songxq on 8/11/16.
 * 
 */

public class HuiUserChongZhi extends ChongZhi{
	
	   //惠理财用户登录
	   @Override
		protected void userLogin(){
				this.loadPage();
		        this.HLlogin();
			}
		//惠理财用户充值金额
	   @Override
		protected String userAmount(){
			return this.hlAmount;
		}
		//惠理财用户交易密码
	   @Override
		protected String userJiaoyimima(){
				return this.hlJiaoyipassword;
		}
		//惠理财用户卡号
	   @Override
		protected String userCard(){
				return this.hlCard;
		}
	   //惠理财用户绑卡使用的银行
	   @Override
		protected String userBankname(){
			return this.hlcsxywyBankname;
		}
	
}

