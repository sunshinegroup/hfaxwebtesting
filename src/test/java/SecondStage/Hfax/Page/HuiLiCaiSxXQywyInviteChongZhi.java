package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.ChongZhi;

public class HuiLiCaiSxXQywyInviteChongZhi extends ChongZhi{
/**
 *  
 * @Description 惠理财寿险【续期】业务员【邀请的用户】充值
 * Created by songxq on 12/21/16.
 * 
 */
	 //惠理财寿险【续期】业务员【邀请的用户】用户登录
	   @Override
		protected void userLogin(){
				this.loadPage();
				this.HLClogin(this.hlcsxXQywyInviteUsername,this.hlcsxXQywyInvitePassword,this.hlcsxXQywyInvitePhone);
		}
	  //惠理财寿险【续期】业务员【邀请的用户】用户充值金额
	   @Override
		protected String userAmount(){
			return this.hlcsxXQywyInviteAmount;
		}
		//惠理财寿险【续期】业务员【邀请的用户】用户交易密码
	   @Override
		protected String userJiaoyimima(){
				return this.hlcsxXQywyInviteJiaoyipassword;
		}
		//惠理财寿险【续期】业务员【邀请的用户】用户卡号
	   @Override
		protected String userCard(){
				return this.hlcsxXQywyInviteCard;
		}
	   //惠理财寿险【续期】业务员【邀请的用户】用户绑卡使用的银行
	   @Override
		protected String userBankname(){
			return this.hlcsxXQywyInviteBankname;
		}
	
}
