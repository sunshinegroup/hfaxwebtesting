package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.JiaoYiMiMa;
/**
 *  
 * @Description 惠理财【邀请的用户】交易密码
 * Created by songxq on 12/05/16.
 * 
 */
public class HuiLiCaiSxywyInviteJiaoyiPassword extends JiaoYiMiMa{

	//惠理财【邀请的用户】用户登录
	@Override
	protected void userLogin(){
		this.loadPage();
		this.HLClogin(this.hlcsxywyInviteUsername,this.hlcsxywyInvitePassword,this.hlcsxywyInvitePhone);
	}
	//惠理财【邀请的用户】用户交易密码
	@Override
	protected String userJiaoyimima(){
		return this.hlcsxywyInviteJiaoyipassword;
	}

}
