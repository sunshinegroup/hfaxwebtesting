package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
/**
 *  
 * @Description 惠理财投列表页
 * Created by songxq on 15/11/16.
 * 
 */
public class HuiLiCaiFinanceListPage extends BasePage{

	public HuiLiCaiFinanceListPage(WebDriver driver, FluentWait<WebDriver> wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}

	//验证列表页产品信息
	public void verifyProDetail(String text,String xpath){
		WebElement proInfo = this.driver.findElement(By.xpath(xpath));
		while(true){
			if(IsElementPresent(By.xpath(xpath))){
				this.log("需验证的控件元素存在");
				break;
			}
		}
		System.out.println(proInfo.getText().trim());
		if(proInfo.getText().trim().equals(text)){
			System.out.println("信息显示正确");
		}else{
			fail("信息显示不正确");
		}
	}
	//点击立即投资
	public void clickButton(String xpath){
		this.driver.findElement(By.xpath(xpath)).click();
	}

}
