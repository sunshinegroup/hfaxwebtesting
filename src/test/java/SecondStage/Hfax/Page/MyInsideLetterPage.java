package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *  
 * @Description 站内信
 * Created by songxq on 21/11/16.
 * 
 */
public class MyInsideLetterPage extends BasePage{

	public MyInsideLetterPage(WebDriver driver, FluentWait<WebDriver> wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}

	//验证数据表信息
	public void verifyTableInfo(String[] expectedHeaders, List<WebElement> headers) {
	    assertTrue("列数不对", expectedHeaders.length == headers.size());
	    for (int i = 0; i < expectedHeaders.length; i ++) {
	    	System.out.println(headers.get(i).getText().trim());
	        assertTrue("列 " + i + " 文字不正确", expectedHeaders[i].equalsIgnoreCase(headers.get(i).getText().trim()));
	    }
	}
	
	//点击操作
	public void clickTitle(String xpath){
		this.driver.findElement(By.xpath(xpath)).click();
	}
	
	//等待页面加载
	public void waitPage(String xpath,int wait){
			new WebDriverWait(this.driver,wait).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		}
	
	//文本信息验证
	public void verifLetterDetailInfo(String xpath,String content,int number){
		List<WebElement> list = this.driver.findElements(By.xpath(xpath));
		System.out.println(list.get(number).getText().trim());
		if(list.get(number).getText().trim().equals(content)){
			System.out.println("相关数据显示正确:" + list.get(number).getText().trim());
		}else{
			fail("相关数据显示不正确");
		}
	}
	//单个文本信息验证
	public void verifLetterInfo(String xpath,String content){
		String text = this.driver.findElement(By.xpath(xpath)).getText().trim();
		System.out.println("页面文本信息为："+text);
		if(text.equals(content)){
			System.out.println("相关数据显示正确:" + text);
		}else{
			fail("相关数据显示不正确");
		}
	}
	public void verifyInfo(String xpath,String content){
		String text = this.driver.findElement(By.xpath(xpath)).getText().trim();
		System.out.println("页面文本信息为："+text);
		assertTrue("信息显示不正确",this.driver.findElement(By.xpath(xpath)).getText().trim().contains(content));
	}
	

}
