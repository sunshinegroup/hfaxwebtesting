package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.ChongZhi;
/**
 *  
 * @Description 惠理财【邀请的用户】充值
 * Created by songxq on 12/05/16.
 * 
 */

public class HuiLiCaiSxywyInviteChongZhi extends ChongZhi{

	 //惠理财【邀请的用户】用户登录
	   @Override
		protected void userLogin(){
				this.loadPage();
				this.HLClogin(this.hlcsxywyInviteUsername,this.hlcsxywyInvitePassword,this.hlcsxywyInvitePhone);
		}
	  //惠理财【邀请的用户】用户充值金额
	   @Override
		protected String userAmount(){
			return this.hlcsxywyInviteAmount;
		}
		//惠理财【邀请的用户】用户交易密码
	   @Override
		protected String userJiaoyimima(){
				return this.hlcsxywyInviteJiaoyipassword;
		}
		//惠理财【邀请的用户】用户卡号
	   @Override
		protected String userCard(){
				return this.hlcsxywyInviteCard;
		}
	   //惠理财【邀请的用户】用户绑卡使用的银行
	   @Override
		protected String userBankname(){
			return this.hlcsxywyInviteBankname;
		}
	

}
