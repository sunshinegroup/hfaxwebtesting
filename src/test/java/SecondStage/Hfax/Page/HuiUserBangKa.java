package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.BangKa;
/**
 *  
 * @Description 惠理财用户绑卡
 * Created by songxq on 8/11/16.
 * 
 */
public class HuiUserBangKa extends BangKa{

	 //惠理财用户登录
	   @Override
		protected void userLogin(){
			this.loadPage();
	        this.HLlogin();
		}
	    //惠理财用户卡号
	   @Override
		protected String userCard(){
			return this.hlCard;
		}
		//惠理财用户手机号
	   @Override
		protected String userPhone(){
			return this.hlPhone;
		}
		 //惠理财用户真实姓名
	    @Override
		 protected String userRealName(){
			    return this.hlName;
		 }
}
