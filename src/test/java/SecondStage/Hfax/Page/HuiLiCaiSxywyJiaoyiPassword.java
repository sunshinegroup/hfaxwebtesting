package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.JiaoYiMiMa;
/**
 *  
 * @Description 惠理财寿险业务员交易密码
 * Created by songxq on 12/14/16.
 * 
 */
public class HuiLiCaiSxywyJiaoyiPassword extends JiaoYiMiMa{

		//惠理财寿险业务员登录
			@Override
			protected void userLogin(){
				this.loadPage();
		        this.HLClogin(this.hlcsxywyUsername,this.hlcsxywyPassword,this.hlcsxywyPhone);
			}
		//惠理财寿险业务员交易密码
			@Override
			protected String userJiaoyimima(){
				return this.hlcsxywyJiaoyipassword;
		}

}
