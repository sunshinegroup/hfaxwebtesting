package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.hfax.selenium.essential.ZhuCe;
/**
 *  
 * @Description 惠理财投资用户注册
 * Created by songxq on 01/10/17.
 * 
 */
public class HuiLiCaiInvestUserRegister extends ZhuCe{
	 //惠理财投资用户登录用户
	 @Override
	  protected String userName() {
	        return this.huilicaiinvestUsername;
	    }
	  //惠理财投资用户登录密码
	 @Override
	  protected String userPassword(){
		  	return this.huilicaiinvestPassword;
	  }
	  //惠理财投资用户手机号
	 @Override
	  protected String userPhone(){
		  	return this.huilicaiinvestPhone;
	  }
	  //惠理财投资用户姓名
	 @Override
	  protected String userRealName(){
		    return this.huilicaiinvestName;
	  }
	  //惠理财投资用户身份证号
	 @Override
	  protected String userIdentification(){
		  return this.huilicaiinvestIdentification;
	  }
	  
	  //惠理财投资用户邀请码
	  protected String userInvitationCode()throws ClassNotFoundException, SQLException{
		  return "";
	  }
}
