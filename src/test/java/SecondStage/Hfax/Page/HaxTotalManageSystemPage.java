package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *  
 * @Description 惠金所综合管理系统
 * Created by songxq on 23/11/16.
 * 
 */
public class HaxTotalManageSystemPage extends BasePage{

	public HaxTotalManageSystemPage(WebDriver driver, FluentWait<WebDriver> wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}
	//按钮点击操作
	public void clickButton(String xpath){
		this.driver.findElement(By.xpath(xpath)).click();
	}
	//等待页面加载
	public void waitPage(String xpath,int wait){
		new WebDriverWait(this.driver,wait).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	}
	//模拟鼠标操作
	public void moveToElement(String xpath){
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElement(By.xpath(xpath))).perform();
	}
	
	//输入内容
	public void inputInfo(String xpath,String content){
		this.driver.findElement(By.xpath(xpath)).clear();
		this.driver.findElement(By.xpath(xpath)).sendKeys(content);
	}
	//获取内容
	public String getInfo(String xpath){
		String content = this.driver.findElement(By.xpath(xpath)).getText().trim();
		return content;
	}
	//等待页面加载
	public void waitVisbilityPage(String xpath,int wait){
		new WebDriverWait(this.driver,wait).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
	}
	
}
