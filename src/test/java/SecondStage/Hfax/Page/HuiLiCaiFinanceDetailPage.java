package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;
/**
 *  
 * @Description 惠理财投资详情页
 * Created by songxq on 8/11/16.
 * 
 */
public class HuiLiCaiFinanceDetailPage extends BasePage{
	
	
	Map<String, String> envVars = System.getenv();
	public HuiLiCaiFinanceDetailPage(WebDriver driver, FluentWait<WebDriver> wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}
	
	//等待页面加载
	public void waitPage(String xpath,int wait){
		new WebDriverWait(this.driver,wait).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	}
	//模糊匹配内容
	public void verifyProDetail(String text,String xpath){
		WebElement proInfo = this.driver.findElement(By.xpath(xpath));
		System.out.println(proInfo.getText().trim());
		if(proInfo.getText().trim().contains(text)){
			System.out.println("信息显示正确");
		}else{
			fail("信息显示不正确");
		}
	}
	//获取行列的文本数据
	 public String findTextField(int rowNumber, int columnNumber) {
	        return this.driver.findElement(By.xpath(".//*[@id='con_one_4']/div[2]/div/table/tbody/tr[" + rowNumber +
	                "]/td[" + columnNumber + "]")).getText().trim();
	    }
	 //文本验证
	 public void verifyText(String textFirst,String textSecond){
		 if(textFirst.trim().equals(textSecond)){
			 System.out.println(textSecond);
		 }else{
			 fail("文本信息不正确");
		 }
	 }
	 //输入信息的操作
		public void inputInfo(String xpath,String content){
			if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
				this.driver.findElement(By.xpath(xpath)).sendKeys(content);
			}else{
				this.driver.findElement(By.xpath(xpath)).clear();
				this.driver.findElement(By.xpath(xpath)).sendKeys(content);
			}
			
		}
	//验证信息
		public void verifyInfo(String xpath,String content){
			System.out.println("界面信息为："+this.driver.findElement(By.xpath(xpath)).getText().trim());
			assertTrue("信息显示不正确",this.driver.findElement(By.xpath(xpath)).getText().trim().equals(content));
		}
	//相关页面的验证信息
	public void verifyPageInfo(String pageName,String xpath,String content){
			System.out.println(pageName+":"+this.driver.findElement(By.xpath(xpath)).getText().trim());
			assertTrue(pageName+"显示不正确",this.driver.findElement(By.xpath(xpath)).getText().trim().equals(content));
			}
}
