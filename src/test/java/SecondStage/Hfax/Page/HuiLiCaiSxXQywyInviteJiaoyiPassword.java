package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hfax.selenium.essential.JiaoYiMiMa;
/**
 *  
 * @Description 惠理财寿险续期业务员【邀请的用户】交易密码
 * Created by songxq on 12/20/16.
 * 
 */
public class HuiLiCaiSxXQywyInviteJiaoyiPassword extends JiaoYiMiMa{

		//惠理财寿险续期业务员【邀请的用户】用户登录
		@Override
		protected void userLogin(){
			this.loadPage();
			this.HLClogin(this.hlcsxXQywyInviteUsername,this.hlcsxXQywyInvitePassword,this.hlcsxXQywyInvitePhone);
		}
		//惠理财寿险续期业务员【邀请的用户】用户交易密码
		@Override
		protected String userJiaoyimima(){
			return this.hlcsxXQywyInviteJiaoyipassword;
		}

}
