package SecondStage.Hfax.Page;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;

/**
 *  
 * @Description 我的投资券
 * Created by songxq on 10/11/16.
 * 
 */
public class MyInvestTicketPage extends BasePage{

	Map<String, String> envVars = System.getenv();
	public MyInvestTicketPage(WebDriver driver, FluentWait<WebDriver> wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}
	
	/**  
	 * @Description 验证惠金所投资券文本信息
	 * @param fristnumber:未使用的优惠券
	 * @param secondnumber:所获得的投资券
	 * @param thirdnumber:已使用的枚数
	 * @param money:赚取的金额
	 */
	public void verifyTicketInfo(String fristnumber,String secondnumber,String thirdnumber,String money){
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
	            }
	        });
		 System.out.println("投资券未使用的个数为："+this.getElementText("//b[@class='hfax-orange']"));
		 assertTrue("投资券未使用的个数不正确",this.getElementText("//b[@class='hfax-orange']").equals(fristnumber));
		 System.out.println("投资券未使用提示的文本为："+this.getElementText("//div[@class='hfax-invest-left left']//p[@class='hfax-left-cur']"));
		 System.out.println("warning log:" + this.getElementText("//div[@class='hfax-invest-left left']//p[@class='hfax-left-cur']"));
		 assertTrue("投资券未使用提示的文本1不正确",this.getElementText("//div[@class='hfax-invest-left left']//p[@class='hfax-left-cur']").equals("当前有 "+fristnumber+" 枚投资券未使用"));
		 assertTrue("投资券未使用提示的文本2不正确",this.getElementText("//div[@class='hfax-invest-left left']//p[@class='hfax-left-contact']//a[@class='hfax-repeat-color']").equals("立即去投资"));
		 
		 List<WebElement> list = this.driver.findElements(By.xpath("//div[@class='hfax-invest-middle left']/p[@class='hfax-left-cur']/strong")); 
		 System.out.println("获得的投资券个数为：" + list.get(0).getText().trim());
		 assertTrue("获得的投资券个数不正确",list.get(0).getText().trim().equals(secondnumber));
		 
		 System.out.println("使用的枚数为：" + list.get(1).getText().trim());
		 assertTrue("使用的枚数不正确",list.get(1).getText().trim().equals(thirdnumber));
		 
		 System.out.println("赚取的元数为：" + this.getElementText("//div[@class='hfax-invest-middle left']//p[@class='hfax-left-contact']/strong"));
		 assertTrue("赚取的元数不正确",this.getElementText("//div[@class='hfax-invest-middle left']//p[@class='hfax-left-contact']/strong").equals(money));
		 assertTrue("绑定投资券信息不正确",this.getElementText(".//*[@id='bindtickets']").equals("绑定投资券"));
		
	}
		//验证代金券和加息券的个数
		public void verifyCouponInfo(int number){
			List<WebElement> list = this.driver.findElements(By.xpath("//div[@class='amountImg']")); 
			if(list.size() == number){
				System.out.println("代金券和加息券的个数正确：" + list.size());
			}else{
				fail("显示的代金券和加息券个数不正确");
			}
		}
		//验证代金券和加息券的相关数据,eg.可投资于，有效期，获得来源，使用条件
		public void verifyCouponDetailInfo(String xpath,String content,int number){
			while(true){
				if(IsElementPresent(By.xpath(xpath))){
					this.log("需验证的控件元素存在");
					break;
				}
			}
			List<WebElement> list = this.driver.findElements(By.xpath(xpath));
			System.out.println(list.get(number).getText().trim());
			if(list.get(number).getText().trim().equals(content)){
				System.out.println("代金券和加息券相关数据显示正确:" + list.get(number).getText().trim());
			}else{
				fail("代金券和加息券相关数据显示不正确");
			}
		}
		//点击操作
		public void clickButton(String xpath){
			this.driver.findElement(By.xpath(xpath)).click();
		}
		
		//输入信息的操作
		public void inputInfo(String xpath,String content){
			if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
				//this.driver.findElement(By.xpath(xpath)).clear();
				this.driver.findElement(By.xpath(xpath)).sendKeys(content);
			}else{
				this.driver.findElement(By.xpath(xpath)).clear();
				this.driver.findElement(By.xpath(xpath)).sendKeys(content);
			}
			
		}
		//验证信息
		public void verifyInfo(String xpath,String content){
			System.out.println("界面信息为："+this.driver.findElement(By.xpath(xpath)).getText().trim());
			assertTrue("信息显示不正确",this.driver.findElement(By.xpath(xpath)).getText().trim().equals(content));
		}
		
}
