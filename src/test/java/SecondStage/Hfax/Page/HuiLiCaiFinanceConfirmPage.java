package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;
/**
 *  
 * @Description 惠理财投资确认页
 * Created by songxq on 01/10/17.
 * 
 */
public class HuiLiCaiFinanceConfirmPage extends BasePage{

	Map<String, String> envVars = System.getenv();
	public HuiLiCaiFinanceConfirmPage(WebDriver driver, FluentWait<WebDriver> wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}

	//点击操作
	public void clickButton(String xpath){
		this.driver.findElement(By.xpath(xpath)).click();
	}
	
	//输入信息的操作
	public void inputInfo(String xpath,String content){
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
			this.driver.findElement(By.xpath(xpath)).sendKeys(content);
		}else{
			this.driver.findElement(By.xpath(xpath)).clear();
			this.driver.findElement(By.xpath(xpath)).sendKeys(content);
		}
		
	}
	//验证信息
	public void verifyInfo(String xpath,String content){
		System.out.println("界面信息为："+this.driver.findElement(By.xpath(xpath)).getText().trim());
		assertTrue("信息显示不正确",this.driver.findElement(By.xpath(xpath)).getText().trim().equals(content));
	}
	//验证属性，属性值
	public void verifyElementAttribute(String xpath,String attribute,String value){
		if(this.driver.findElement(By.xpath(xpath)).getAttribute(attribute).equals(value)){
			System.out.println("属性值为："+ this.driver.findElement(By.xpath(xpath)).getAttribute(attribute));
		}else{
			fail("属性值不正确");
		}
	}
	//验证模糊匹配
		public void verifyVagueInfo(String xpath,String content){
			System.out.println("界面信息为："+this.driver.findElement(By.xpath(xpath)).getText().trim());
			assertTrue("信息显示不正确",this.driver.findElement(By.xpath(xpath)).getText().trim().contains(content));
		}
}
