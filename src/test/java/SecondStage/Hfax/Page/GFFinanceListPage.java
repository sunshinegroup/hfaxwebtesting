package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;
/**
 *  
 * @Description 固浮列表页
 * Created by songxq on 8/11/16.
 * 
 */
public class GFFinanceListPage extends BasePage{

	public GFFinanceListPage(WebDriver driver,FluentWait<WebDriver> wait) {
		super(driver,wait);
		// TODO Auto-generated constructor stub
	}
	
	
	//验证点击惠投资列表中的立即投资跳转到详情列表页
	public void verifyInvestskippage() throws Exception{
		
		//等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='tab_6']")).isDisplayed();
            }
        });
		 new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tab_6']")));
	    //点击列表页惠投资
	    this.driver.findElement(By.xpath(".//*[@id='tab_6']")).click();
		WebElement content = this.clickOnSubmenu("惠投资");
		WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
	    new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='display_6']/div/div[2]")));
	    //点击惠投资第一个产品
	    assertTrue("惠投资第一个产品不存在", this.driver.findElement(By.xpath("//div[@id='display_6']/div/div[2]")).isDisplayed());
	    firstItem.findElement(By.xpath("i/div/div[2]/a")).click();
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='amount']")).isDisplayed();
            }
        });
	    assertTrue("没有跳转到惠投资详细页",this.driver.findElement(By.xpath("//div[@class='balanceTitle']/span")).getText().trim().equals("剩余可投金额："));

	}
	
	//验证点击惠投资列表中的产品名称跳转到详情列表页
	public void verifyProNameskippage() throws Exception{
		
		//等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='tab_6']")).isDisplayed();
            }
        });
		 new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tab_6']")));
	    //点击列表页惠投资
	    this.driver.findElement(By.xpath(".//*[@id='tab_6']")).click();
		WebElement content = this.clickOnSubmenu("惠投资");
		WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
	    new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='display_6']/div/div[2]")));
	    assertTrue("惠投资第一个产品不存在", this.driver.findElement(By.xpath("//div[@id='display_6']/div/div[2]")).isDisplayed());
	    //获取当前窗口句柄
		 String currentWindow = this.driver.getWindowHandle();
	    //点击产品名称
	    this.driver.findElement(By.xpath("//a[@class='listBox-title']")).click();
	    //弹出新窗口
		 Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
		 Iterator<String> it = handles.iterator();
		 while (it.hasNext()) {
		 if (currentWindow == it.next()) {
		 continue;
		 }
		  WebDriver window = driver.switchTo().window(it.next());//切换到新窗口
		  break;
		 }
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='amount']")).isDisplayed();
            }
        });
	    assertTrue("没有跳转到惠投资详细页",this.driver.findElement(By.xpath("//div[@class='balanceTitle']/span")).getText().trim().equals("剩余可投金额："));

	}
	
	//验证列表页跳转
	 public void verifyGFFinancelistpage(){
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath(".//*[@id='tab_6']")).isDisplayed();
	            }
	        });
		 System.out.println("惠投资列表属性为：" + this.driver.findElement(By.xpath(".//*[@id='tab_6']")).getAttribute("class"));
		 assertTrue("没有跳转到惠投资列表页",this.driver.findElement(By.xpath(".//*[@id='tab_6']")).getAttribute("class").trim().equals("active"));
		 }
	 
	 //点击惠投资列表页
	 public void clickGFFinancelistButton(String textfirst,String textsecond,String xpathfirst,String xpathsecond){
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath(".//*[@id='tab_6']")).isDisplayed();
	            }
	        });
		 //点击惠投资列表页
		 this.driver.findElement(By.xpath(".//*[@id='tab_6']")).click();
		 System.out.println("惠投资列表页的文本为：" + this.driver.findElement(By.xpath("//div[@class='topBox']/span/a[@href="+"'"+xpathfirst+"'"+"]")).getText().trim());
		 System.out.println("惠投资列表页的文本为：" + this.driver.findElement(By.xpath("//div[@class='topBox']/span/a[@href="+"'"+xpathsecond+"'"+"]")).getText().trim());
		 assertTrue("惠投资列表页快速注册提示",this.driver.findElement(By.xpath("//div[@class='topBox']/span/a[@href="+"'"+xpathfirst+"'"+"]")).getText().trim().equals(textfirst));
		 assertTrue("惠投资列表页登录提示",this.driver.findElement(By.xpath("//div[@class='topBox']/span/a[@href="+"'"+xpathsecond+"'"+"]")).getText().trim().equals(textsecond));
				 		 
	 }
	 
	 //点击惠投资列表页
	 public void clickGFFinancelistButton(){
			//等待页面加载
			 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return webDriver.findElement(By.xpath(".//*[@id='tab_6']")).isDisplayed();
		            }
		        });
			 //点击惠投资列表页
			 this.driver.findElement(By.xpath(".//*[@id='tab_6']")).click();
					 		 
		 }
	 
	 //点击服务按钮
	 public void clickServiceButton(){
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//a[@class='htz_btn']")).isDisplayed();
	            }
	        });
		 this.switchWindows("//a[@class='htz_btn']");
		 
	 }
	 
	 //当列表页产品数目大于等于1，小于等于10的时候，页码显示为
	 public void verifyProAmount(String pagecount){
		 List<WebElement> list = this.driver.findElements(By.xpath("//a[@class='listBox-title']"));
		 System.out.println("惠投资列表页产品个数为：" + list.size());
		 if(list.size() < 1){
			 System.out.println("暂时没有惠投资的产品");
		 }
		 else if(list.size() >= 1 && list.size() <= 10){
			 System.out.println("惠投资列表页显示的页码为：" + this.getElementText("//div[@class='pageDivClass']"));
			 assertTrue("惠投资列表页页码显示不正确",this.getElementText("//div[@class='pageDivClass']").equals(pagecount));
		 }else{
			 fail("惠投资列表页显示的产品数目不正确");
		 }
	 }
	//验证首页和下一页
	 public void verifyPageDisplay(){
		 	//滚轮向下,验证页码首页，下一页
			((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("下一页")));
			this.driver.findElement(By.linkText("下一页")).click();
			((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			WebElement firstpage = this.driver.findElement(By.linkText("首页"));
			assertTrue("页面页码首页存在",firstpage.isDisplayed());
	 }
	 //点击首页
	 public void clickFirstButton(){
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.linkText("首页")).isDisplayed();
	            }
	        });
		 this.driver.findElement(By.linkText("首页")).click();
	 }
	 //验证首页惠投资的商品
	 public void verifyProDetail(String proName){
		 boolean blnflag = false;
		 List<WebElement> list = this.driver.findElements(By.xpath("//a[@class='listBox-title']"));
		 for(int i = 0; i < list.size(); i++){
			 if(list.get(i).getText().trim() == proName){
				 System.out.println("惠投资的产品为：" + list.get(i).getText().trim());
				 blnflag = true;
				 break;
			 }
		 }
	 }

}
