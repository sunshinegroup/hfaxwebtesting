package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.hfax.selenium.essential.ZhuCe;

/**
 *  
 * @Description 手动卡券用户注册
 * Created by songxq on 01/09/16.
 * 
 */
public class ManualTicketsUserRegister extends ZhuCe{

		 //手动卡券登录用户
		 @Override
		  protected String userName() {
		        return this.manualticketsUsername;
		    }
		  //手动卡券登录密码
		 @Override
		  protected String userPassword(){
			  	return this.manualticketsPassword;
		  }
		  //手动卡券用户手机号
		 @Override
		  protected String userPhone(){
			  	return this.manualticketsPhone;
		  }
		  //手动卡券用户姓名
		 @Override
		  protected String userRealName(){
			    return this.manualticketsName;
		  }
		  //手动卡券身份证号
		 @Override
		  protected String userIdentification(){
			  return this.manualticketsIdentification;
		  }
		  
		  //手动卡券邀请码
		  protected String userInvitationCode()throws ClassNotFoundException, SQLException{
			  return "";
		  }
}



