package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *  
 * @Description 
 * Created by songxq on 9/11/16.
 * 
 */

public class BasePage {

	   protected WebDriver driver;
	   protected FluentWait<WebDriver> wait;

	   Map<String, String> envVars = System.getenv();
	   public BasePage(WebDriver driver,FluentWait<WebDriver> wait) {
	        this.driver = driver;
	 	    this.wait = wait;
	    }
       
	   /**
	     * 子菜单的选择 ，
	     * eg.惠理财，新手专区，惠理财，惠投资，惠聚财
	     */
	    protected WebElement clickOnSubmenu(String title) {
	        WebElement submenu = this.driver.findElement(By.id("listTab-box"));
	        submenu.findElement(By.linkText(title)).click();
	        
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.className("list-control")));
	        List<WebElement> elements = this.driver.findElements(By.className("list-control"));

	        for (WebElement el : elements) {
	        	this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	            if (el.isDisplayed()) return el;
	        }
	        assertTrue("无内容显示: " + title, false);
	        return null;
	    }
	    /**
	     * 窗口跳转
	     *
	     */
	    public void switchWindows(String xpath){
	    	 //获取当前窗口句柄
			 String currentWindow = this.driver.getWindowHandle();
			 System.out.println("当前窗口：" + currentWindow);
			 this.driver.findElement(By.xpath(xpath)).click();
				if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			//弹出新窗口
			 Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
			 Iterator<String> it = handles.iterator();
			 while (it.hasNext()) {
				 String win = (String)(it.next());
				 System.out.println("第二个窗口：" + win);
				 if (currentWindow.equals(win)){
					 continue;
	    		}
			   driver.switchTo().window(win);//切换到新窗口
			  break;
			 }
	    }
	    
	    /*
	     * 获取元素内容
	     */
	    protected String getElementText(String strXpath){
	    	try{
	    		String text = this.driver.findElement(By.xpath(strXpath)).getText().trim();
	    		return text;
	    	}catch (Exception e){
	    		e.printStackTrace();
	    	}
	    	return null;
	    }
	    
	    /*
	     *  输出日志
	     */
	    public void log(String message) {
	        System.out.println("input info: " + message);
	    }
	    
	    /*
	     *  点击操作
	     */
  		public void clickButton(String xpath){
  			this.driver.findElement(By.xpath(xpath)).click();
  		}
  	
  		 /*
  	     * 判断元素是否存在
  	     */
  	    protected boolean IsElementPresent(By by) {
  	    	try {
  	            this.driver.findElement(by);
  	            return true;
  	        } catch (Exception e) {
  	            return false;
  	        }
  	    }
	   
}
