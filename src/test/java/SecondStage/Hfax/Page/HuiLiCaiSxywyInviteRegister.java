package SecondStage.Hfax.Page;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.hfax.selenium.essential.ZhuCe;
/**
 *  
 * @Description 惠理财寿险业务员邀请的用户
 * Created by songxq on 12/14/16.
 * 
 */
public class HuiLiCaiSxywyInviteRegister extends ZhuCe{

	DBquery db = new DBquery();
	 //惠理财寿险业务员邀请的用户登录名
	 @Override
	  protected String userName() throws ClassNotFoundException, SQLException {
		
	        return this.hlcsxywyInviteUsername;
	    }
	  //惠理财寿险业务员邀请的用户登录密码
	 @Override
	  protected String userPassword(){
		  	return this.hlcsxywyInvitePassword;
	  }
	  //惠理财寿险业务员邀请的用户手机号
	 @Override
	  protected String userPhone(){
		  	return this.hlcsxywyInvitePhone;
	  }
	  //惠理财寿险业务员邀请的用户姓名
	 @Override
	  protected String userRealName(){
		    return this.hlcsxywyInviteName;
	  }
	  //惠理财寿险业务员邀请的用户身份证号
	 @Override
	  protected String userIdentification(){
		  return this.hlcsxywyInviteIdentification;
	  }
	  
	  //惠理财寿险业务员邀请的用户邀请码
	 @Override
	  protected String userInvitationCode() throws ClassNotFoundException, SQLException{
		  String inviteCode = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select invi_code from sunif.sif_member a inner join sunif.sif_member_code b on a.member_cd = b.member_cd where a.member_alias ="+"'"+this.hlcsxywyUsername+"'","invi_code");
		  System.out.println("寿险业务员邀请的用户邀请码为："+inviteCode);
		  return inviteCode;
	  }

}
