package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 企业还款后个人用户账号金额 extends AccountBaseTest{
	protected String afterTnvestAmount; //投资后账户余额
	protected String afterAvailableAmount; //投资后可用余额
	protected String afterFreezeAmount; //投资后冻结的金额
	protected String afterAllAmount;//投资后账户总资产
	Map<String, String> envVars = System.getenv();
	
	@Test
	public void test001_企业还款后个人用户账号金额 () {

		 
       //等待页面加载
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='index5']/a")).isDisplayed();
            }
        });
        //点击我的账户
        this.driver.findElement(By.xpath(".//*[@id='index5']/a")).click();
        
        //等待页面加载
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p")).isDisplayed();
            }
        });
		//投资后我的账户-->账户余额
		System.out.println("我的账户投资后【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p"));
		afterTnvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p");
		assertTrue("我的账户投资后【账户余额】不正确", afterTnvestAmount.equals("￥ 619,750.00"));
		 
		//投资后我的账户-->账户总资产
		System.out.println("我的账户投资后【账户总资产】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p"));
		afterAllAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p");
		assertTrue("我的账户投资后【账户总资产】不正确", afterAllAmount.equals("￥ 619,750.00"));
		 
		//验证累计净收益
		System.out.println("我的账户投资后【累计净收益】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[3]/p"));
		String  profit = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[3]/p");
		assertTrue("我的账户投资后【累计净收益】不正确", profit.equals("￥ 64,750.00"));
		
		//投资后我的账户-->冻结金额
		System.out.println("我的账户投资后【冻结金额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p"));
		afterFreezeAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p");
		assertTrue("我的账户投资后【冻结金额】不正确", afterFreezeAmount.equals("￥ 0.00"));
		 
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			 this.selectMenuIE(2, "充值提现");
		 }else{
			 this.selectMenu(2, "充值提现");
		 }
			//等待页面加载
			 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath("//div[@class='tabtil']/ul/li")).isDisplayed();
		            }
		        });
			 
		//投资后【账户余额】
		System.out.println("投资后【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]"));
		afterTnvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]");
		assertTrue("投资后【账户余额】不正确", afterTnvestAmount.equals("￥619,750.00"));
			
		//投资后【可用余额】
		System.out.println("投资后【可用余额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]"));
		afterAvailableAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]");
		assertTrue("投资后【可用余额】为", afterAvailableAmount.equals("￥619,750.00"));
			 
		//投资后【冻结金额】
		System.out.println("投资后【冻结金额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]"));
		afterFreezeAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]");
		assertTrue("投资后【冻结金额】为", afterFreezeAmount.equals("￥0.00"));
	}

}
