package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.DBquery;
import SecondStage.Hfax.Page.HuiLiCaiFinanceConfirmPage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceDetailPage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceListPage;
import SecondStage.Hfax.Page.MyInsideLetterPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;

	/**
	 *  
	 * @Description 投资惠理财的新手标
	 * Created by songxq on 01/16/17.
	 * 
	 */
public class TestInvestHuiLiCaiNewHand  extends BaseTest{

	private MyRedBagPage bagpage; //我的红包
	private MyInvestTicketPage ticket;//我的投资券
	private MyInsideLetterPage myletter;//站内信
	private MyQueryCashRecordListPage cashrecord; //充值提现
	private HuiLiCaiFinanceListPage hlc; //惠理财投资列表页
	private HuiLiCaiFinanceDetailPage fd; //惠理财投资详细页
	private HuiLiCaiFinanceConfirmPage cp; //惠理财投资确认页
	DBquery db = new DBquery();
	
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        bagpage = new MyRedBagPage(driver,wait); 
        ticket = new MyInvestTicketPage(driver,wait);
        myletter = new MyInsideLetterPage(driver,wait);
        cashrecord = new MyQueryCashRecordListPage(driver,wait);
        hlc = new HuiLiCaiFinanceListPage(driver,wait);
        fd = new HuiLiCaiFinanceDetailPage(driver,wait);
        cp = new HuiLiCaiFinanceConfirmPage(driver,wait);
    }
	@Test
	public void testInvestHuiLiCaiNewHand() throws ClassNotFoundException, SQLException, InterruptedException,ArithmeticException{
		Map<String, String> envVars = System.getenv();
		String strPath = null;
		this.loadPage();
        this.HLClogin(this.huilicaiinvestUsername,this.huilicaiinvestPassword,this.huilicaiinvestPhone);
        //跳转到定期理财
        this.loginTopNavigationMenu1(3,"定期理财");
  		//跳转到新手专区
 		this.clickOnSubmenu("新手专区");
 		//等待页面加载
		fd.waitPage(".//*[@id='display_7']/div/div[2]", 5);
		String proInvestName;
		String proName;
		 while(true){ 
			 this.clickOnSubmenu("新手专区");
			 proName = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_name from sunif.sif_project_info where subject_name like '惠理财-自动化投资-新-周%' and intr_year='28'","subject_name");
			 System.out.println("惠理财-自动化投资-新-周的名称为："+ proName);
			 Thread.sleep(2000);
			if(proName != null&&!proName.equals("")){ 
				this.clickOnSubmenu("新手专区");
			break;
			}
		 }
		 proInvestName = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_name from sunif.sif_project_info where subject_name like '惠理财-自动化投资-新-周%' and intr_year='28'","subject_name");
		 for(int i = 0; i < 100; i++){
				Set<String> set = new LinkedHashSet<String>();
				set.add(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[3]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[4]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[5]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[6]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[7]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[8]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[9]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[10]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[11]/div[1]/div[1]/a");
				for(Object element: set){
				     if(this.getElementText(element.toString()).equals(proName)){
				    	 strPath = element.toString();
				         break;
				     }
				  }
				if(strPath != null && this.getElementText(strPath).equals(proName)){
					break;
				}else{
				((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
				 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("下一页")));
				 this.driver.findElement(By.linkText("下一页")).click();
				 new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a")));
				 assertTrue("新手专区翻页后第一个产品没有显示",this.getElementText(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a")!=null);
				}
			 }
		 String strPathElement = strPath.substring(0,strPath.length() - 16);
		 System.out.println("截取后的为：" + strPathElement);	 		 
		 
		 String currentWindow = this.driver.getWindowHandle();
		 System.out.println("第一个窗口的信息：" + currentWindow);
		 hlc.clickButton(strPathElement+"/div[2]/div[2]/a");
		 
		 Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
		 System.out.println("当前窗口数为："+ handles.size());
		 Iterator<String> it = handles.iterator();
		 while (it.hasNext()) { 
			 String secondWindow = (String)(it.next());
			 System.out.println("窗口的信息为：" + secondWindow);
			 if (currentWindow.equals(secondWindow)){
				 continue;
    		}
		
		  driver.switchTo().window(secondWindow);//切换到新窗口
		  break;
		 }
		 //等待页面加载
		 fd.waitPage("//div[@id='content']/ul/li/strong//input[@id='_amount']", 5);
		 while(true){
			 if(IsElementPresent(By.xpath("//div[@id='content']/ul/li/strong//input[@id='_amount']"))){
				 break;
			 }
		 }		 
		 
		 //验证项目金额10000元
		 fd.verifyInfo("//ul[@class='borrowBox']//li[@class='amount']//i//strong","10,000.00");
		 //验证预期年化收益率28.00% +0.50% 
		 fd.verifyInfo("//li[@class='interest']//i","28.00%");
		 //0.50%
		 fd.verifyInfo("//li[@class='interest']//i[@class='rateCount']","+0.50%");
		 
		 //输入投资金额
		 fd.inputInfo("//input[@id='_amount']", "10000");
		 
		 //等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//span[@id='interests']")).getText().length() > 0;
	            }
	        });
		//获取投资期限
		 String day = this.driver.findElement(By.xpath("//li[@class='term']/i/strong")).getText().trim();
		 
		 int investday =  Integer.parseInt(day);
		 float profitFirst = (float) (10000*0.28*investday/365);
		 String strProfitFirst = String.valueOf(profitFirst);
		 String newstrProfitFirst = strProfitFirst.substring(0,strProfitFirst.indexOf("."))+strProfitFirst.substring(strProfitFirst.indexOf("."),strProfitFirst.indexOf(".")+3);
		 
		 float profitSecond = (float) (10000*0.005*investday/365);
		 String strProfitSecond = String.valueOf(profitSecond);
		 String newstrProfitSecond = strProfitSecond.substring(0,strProfitSecond.indexOf("."))+strProfitSecond.substring(strProfitSecond.indexOf("."),strProfitSecond.indexOf(".")+3);
		 
		//截取小数点后两位
		 DecimalFormat decimalFormat=new DecimalFormat(".00");
		 String newprofit = decimalFormat.format(Float.valueOf(newstrProfitFirst) + Float.valueOf(newstrProfitSecond));
		 fd.log("已经截取的小数点后的预期收益为 ："+newprofit);
		 
		 
		 while(true){
	    	 if(IsElementPresent(By.xpath("//input[@id='agre']"))&&IsElementPresent(By.xpath("//span[@id='interests']"))){
	    		 break;
	    	 }
	     }
	    fd.verifyPageInfo("项目详细页","//span[@id='interests']", newprofit);
	     
	     //勾选已经阅读并同意
	     fd.clickButton("//input[@id='agre']");
	     
	     //点击去投资
	     fd.clickButton("//a[@id='btnsave']/i");
	     
	     //等待页面加载,跳转到投资确认页
	     this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@id='besurebutton']//button[@class='besure besure_btn']")).isDisplayed();
	            }
	        });
	     
	     //验证惠理财投资确认页预期收益率
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 //等待页面加载
		     this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return webDriver.findElement(By.xpath("//span[@id='yqsyl_']")).isDisplayed();
		            }
		        }); 
		    cp.verifyInfo("//span[@id='yqsyl_']","28.50%");
	     }else{
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return webDriver.findElement(By.xpath("//span[@id='yqsyl_']")).getText().trim().equals("28.50%");
		            }
		        });
	    	 cp.verifyInfo("//span[@id='yqsyl_']","28.50%");
	     }
	     
	     //验证投资期限(天)
	     cp.verifyInfo("//div[@class='item'][2]//span[2]",day+"天");
	 
	     //验证投资金额(元)
	     cp.verifyInfo("//div[@class='item'][3]//span[2]","¥10000.00");
	     

	     //验证惠理财投资项目名称
	     cp.verifyInfo("//div[@id='investtitle']","投资项目："+proInvestName);
	     
	     //验证实际投资本金10,000.00
	     cp.verifyInfo(".//*[@id='sjtzbj']","10,000.00");
	     
	     //验证总预期收益率
	     cp.verifyInfo("//p[@class='basic-left']","28.50%");
	     
	     //验证总预期收益=基础收益率+投资收益率+加息券收益率
	     //基础收益率
	     float profit1 = (float) (10000*0.28*investday/365);
	     String newprofit1 = String.valueOf(profit1); 
	     newprofit1=newprofit1.substring(0,newprofit1.indexOf("."))+newprofit1.substring(newprofit1.indexOf("."),newprofit1.indexOf(".")+3);
	     cp.log("基础收益率为：" + newprofit1);
	     //投资收益率
	     float profit2 = (float) (10000*0.005*investday/365);
	     String newprofit2 = String.valueOf(profit2);
	     newprofit2=newprofit2.substring(0,newprofit2.indexOf("."))+newprofit2.substring(newprofit2.indexOf("."),newprofit2.indexOf(".")+3);
	     cp.log("投资收益率为：" + newprofit2);
	
	     //截取小数点后两位
	     DecimalFormat decimalFormatSecond=new DecimalFormat(".00");
		 String newprofitAll = decimalFormat.format(Float.valueOf(newprofit1) + Float.valueOf(newprofit2));
		 fd.log("没截位前总预期收益率为："+newprofitAll);
		 
			     
	     //验证总预期收益
	     cp.verifyInfo("//span[@id='yqsy']",newprofitAll);
	     
	     //实际支付金额为：10000-26
	     cp.verifyInfo("//font[@id='sjzfje']","10,000.00");
	     
	     //输入交易密码
	     cp.inputInfo("//input[@id='tpwdid']", this.huilicaiinvestJiaoyipassword);
	     
	     //点击确定投资
	     cp.clickButton("//button[@class='besure besure_btn']");

	     //等待页面加载
	     this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//span[@id='inveamount']")).isDisplayed();
	            }
	        });
	     //本次投资金额,是个BUG已经和功能人员沟通
	     cp.verifyInfo("//span[@id='inveamount']","10,000.00");
	     //本项目累计投资金额
	     cp.verifyInfo("//span[@id='investCount']","10,000.00");
	     //本项目累计投资预计收益
	     cp.verifyInfo("//span[@id='forpay']",newprofitAll);
	     
	     //点击关闭
	     cp.clickButton("//div[@id='investSuccess']/div[2]/b/a");

	     //等待页面加载
	     this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@class='inputMoney']/i[@class='cursor']")).isDisplayed();
	            }
	        });
	     //标的状态为满标或者还款中
	     if(this.driver.findElement(By.xpath("//div[@class='inputMoney']/i[@class='cursor']")).getText().trim().equals("满标")
    			 || this.driver.findElement(By.xpath("//div[@class='inputMoney']/i[@class='cursor']")).getText().trim().equals("还款中")||this.driver.findElement(By.xpath("//div[@class='inputMoney']/i[@class='cursor']")).getText().trim().equals("已满标")){
	    	 fd.log("标的为满标和还款中显示正确");
	     }else{
	    	 fd.log("标的状态不正确应该为满标或者还款中");
	    	 fail("标的状态不正确应该为满标或者还款中");
	     }
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	this.driver.findElement(By.xpath("//li[@id='_topMenu_account']/a")).click();
	     }else{
	    	 this.topNavigationMenu(5,"我的账户");
	     }
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return webDriver.findElement(By.xpath("//li[@id='myInvestment']/a")).isDisplayed();
		            }
		        });
	    	 //点击我的投资
	    	 this.driver.findElement(By.xpath("//li[@id='myInvestment']/a")).click();
	     }else{
	    	 this.leftNavigationMenu(2,"我的投资");
	     }
	     
	     //等待页面加载
	     this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr")).isDisplayed();
	            }
	        });
	   
	     boolean flag = false;
	     List<WebElement> count = this.driver.findElements(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr"));
	     for(int i = 2; i <= count.size(); i++){
	    	 if(this.driver.findElement(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+i+"]/td[1]//a")).getText().trim().equals(proInvestName)){
	    		 //验证我的投资：本金
	    	     fd.verifyInfo("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+i+"]/td[2]","10000.00");
	    	     //验证我的投资：收益
	    	     fd.verifyInfo("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+i+"]/td[3]",newprofitAll);
	    		 flag = true;
	    		 break;
	    	 }
	     }
	     if(flag = false){
	    	 fd.log("我的账户->我的投资，没有找到投资的项目：惠理财-自动化投资-新-周");
	    	 fail("我的账户->我的投资，没有找到投资的项目：惠理财-自动化投资-新-周");
	     }
	       
	   
	    //53元红包
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='payAndDraw']/a")).isDisplayed();
	             }
	         });
	    //点击充值提现
	    	 this.driver.findElement(By.xpath("//li[@id='payAndDraw']/a")).click();
	     }else{
	    	 this.leftNavigationMenu(3,"充值/提现");
	     }
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th")).isDisplayed();
            }
        });
	    List<WebElement> fundrecord = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th")); 
	    cashrecord.verifyTableInfo(new String[] {"序号", "时间", "操作类型","备注","收入","支出","可用余额"},fundrecord,7);
	    List<WebElement> fundrecordsecond = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td"));
	    cashrecord.verifyTableInfo(new String[] {"1",this.getDate(0), "成功投资","投资了["+proInvestName+"]"+"产品,冻结投资金额：￥[10000.00]元","0.00","10,000.00"},fundrecordsecond,6); 
	   
	    this.loginTopNavigationMenu(5,"我的账户");
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
    		//等待页面加载
    		 this.wait.until(new ExpectedCondition<Boolean>() {
    	            @Override
    	            public Boolean apply(WebDriver webDriver) {
    	                return driver.findElement(By.xpath("//li[@id='accountManage_menuItem']/a")).isDisplayed();
    	            }
    	        });
    		//点击账户管理
    		 this.driver.findElement(By.xpath("//li[@id='accountManage_menuItem']/a")).click();
    		//等待页面加载
    		 this.wait.until(new ExpectedCondition<Boolean>() {
    	            @Override
    	            public Boolean apply(WebDriver webDriver) {
    	                return driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).isDisplayed();
    	            }
    	        });
    		 //站内信
    		 this.driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).click();
	    }else{
	    	
	    	this.leftNavigationSubMenu(7,2,"站内信");
	    }
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@id='biaoge']/table/tbody/tr[2]/td[3]/a/b")).isDisplayed();
            }
        });
	    Thread.sleep(2000);
	    //等待页面加载
	     this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@id='biaoge']/table/tbody/tr[2]/td[2]")).isDisplayed();
	            }
	        });
	    myletter.verifLetterInfo("//div[@id='biaoge']/table/tbody/tr[2]/td[3]/a/b","投资申请成功");
	    myletter.clickTitle("//div[@id='biaoge']/table/tbody/tr[2]/td[3]/a/b");
    	myletter.verifyInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.huilicaiinvestName+"女士");
    	myletter.verifyInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","您已成功提交一笔金额为10000元的"+proInvestName+"项目投资申请，项目成立后将立即通知您，敬请关注。现在邀请好友一起理财，更有邀请专享加息、现金奖励等您赚！");

        
	} 
	

}


