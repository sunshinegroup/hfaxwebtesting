package SecondStage.Hfax;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 惠聚财流标投资 extends AccountBaseTest{

	/**
	 *  
	 * @Description 惠聚财募集失败流标站内信 
	 * Created by songxq on 09/12/16.
	 * 
	 */
	
	protected String preInvestAmount; //投资前账户的总资产
	protected String preFreezeAmount; //投资前冻结的金额

	
	@Test
	public void test001_惠聚财投资() {
		
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p")).isDisplayed();
	            }
	        });
		//投资前账户的总资产
		System.out.println("投资前账户的总资产: " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p"));
		preInvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p");
		//assertTrue("投资前账户的总资产不正确", preInvestAmount.equals("XXXXXXXX"));
		
		//投资前冻结的金额
		System.out.println("投资前冻结的金额: " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p"));
		preFreezeAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p");
		//assertTrue("投资前冻结的金额不正确", preFreezeAmount.equals("XXXXXXXX"));
		this.navigateToPage("新手专区");
		
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
		WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
		        
		new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")));
		 //等待惠聚财第一个产品
		assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")).isDisplayed());
		        
		//立即投资
		firstItem.findElement(By.xpath("div[2]/div[2]/a")).click();
		        
		//等待输入金额
		 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='amount']")));
		       
		 //输入投资金额
		 this.driver.findElement(By.xpath(".//*[@id='amount']")).clear();
		 this.driver.findElement(By.xpath(".//*[@id='amount']")).sendKeys("10000");
		     
		 //点击已同意
		 this.driver.findElement(By.xpath(".//*[@id='agre']")).click();

		//点击去投资
		this.driver.findElement(By.linkText("去投资")).click();
		
		//等待实际投资本金(元)
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText();
                return text.trim().equalsIgnoreCase("10,000.00");
            }
        });
      //验证优惠活动下代金券的金额
        System.out.println("【惠聚财确认页优惠活动中的代金券金额】为：" + this.getElementText(".//*[@id='table1']/tbody/tr/td[2]"));
        assertTrue("【惠聚财确认页优惠活动中的代金券金额】为：",this.getElementText(".//*[@id='table1']/tbody/tr/td[2]").equals("100"));
        
        //验证优惠活动下代金券的有效期
        System.out.println("【惠聚财确认页优惠活动中的代金券有效期】为：" + this.getElementText(".//*[@id='table1']/tbody/tr/td[3]"));
        assertTrue("【惠聚财确认页优惠活动中的代金券有效期】为：",this.getElementText(".//*[@id='table1']/tbody/tr/td[3]").equals("2017-12-31"));
        
        //验证优惠活动下的代金券的使用条件
        System.out.println("【惠聚财确认页优惠活动中代金券的使用条件】为：" + this.getElementText(".//*[@id='table1']/tbody/tr/td[4]"));
        assertTrue("【惠聚财确认页优惠活动中代金券的使用条件】为：",this.getElementText(".//*[@id='table1']/tbody/tr/td[4]").equals("使用条件说用"));
        
        //验证优惠活动下的加息券的收益率
        System.out.println("【惠聚财确认页优惠活动中的加息券收益率】为：" + this.getElementText(".//*[@id='table2']/tbody/tr/td[2]/span"));
        assertTrue("【惠聚财确认页优惠活动中的加息券收益率】为：",this.getElementText(".//*[@id='table2']/tbody/tr/td[2]/span").equals("100"));
        
        //验证优惠活动下代金券的有效期
        System.out.println("【惠聚财确认页优惠活动中的加息券有效期】为：" + this.getElementText(".//*[@id='table2']/tbody/tr/td[3]"));
        assertTrue("【惠聚财确认页优惠活动中的加息券有效期】为：",this.getElementText(".//*[@id='table2']/tbody/tr/td[3]").equals("2017-12-31"));
        
        //验证优惠活动下的代金券的使用条件
        System.out.println("【惠聚财确认页优惠活动中加息券的使用条件】为：" + this.getElementText(".//*[@id='table2']/tbody/tr/td[4]"));
        assertTrue("【惠聚财确认页优惠活动中加息券的使用条件】为：",this.getElementText(".//*[@id='table2']/tbody/tr/td[4]").equals("使用条件说明"));
        
        //验证实际投资本金(元)
        System.out.println("【惠聚财确认页】实际投资本金(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim());
        assertTrue("【惠聚财确认页】实际投资本金(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim().equals("10,000.00"));
        
        //验证总预期收益率(%)
        System.out.println("【惠聚财确认页】总预期收益率(%)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim() + "%");
        assertTrue("【惠聚财确认页】总预期收益率(%)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim().equals("800.00"));
        
        //验证总预期收益(元)
        System.out.println("【惠聚财确认页】总预期收益(元)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim());
        assertTrue("【惠聚财确认页】总预期收益(元)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim().equals("74,000.00"));
        
        //验证实际支付金额(元)
        System.out.println("【惠聚财确认页】实际支付金额(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim());
        assertTrue("【惠聚财确认页】实际支付金额(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim().equals("9,900.00"));
        
        
        //输入交易密码
        this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).clear();
        this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).sendKeys(this.jiaoyipassword);
        
        
        //点击确定投资
        this.driver.findElement(By.xpath(".//*[@id='besureInvest']")).click();
        
        
        //等待确认框
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[1]")).getText();
                return text.trim().equalsIgnoreCase("浏览更多项目");
            }
        });
        
        //验证确认框本次投资金额
        System.out.println("【惠聚财确认框】本次投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim());
        assertTrue("【惠聚财确认框】本次投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim().equals("10,000.00"));
        
        //验证本项目累计投资金额
        System.out.println("【惠聚财确认框】本项目累计投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim());
        assertTrue("【惠聚财确认框】本项目累计投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim().equals("10,000.00"));
			
	}
	

}
