package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import SecondStage.Hfax.Page.DBquery;
import SecondStage.Hfax.Page.HuiLiCaiFinanceConfirmPage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceDetailPage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceListPage;
import SecondStage.Hfax.Page.MyInsideLetterPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;

	/**
	 *  
	 * @Description 投资新手标后再次投资提示无法投资
	 * Created by songxq on 06/08/17.
	 * 
	*/
public class TestAfterInvestHuiLiCaiNewHand extends BaseTest{

	private MyRedBagPage bagpage; //我的红包
	private MyInvestTicketPage ticket;//我的投资券
	private MyInsideLetterPage myletter;//站内信
	private MyQueryCashRecordListPage cashrecord; //充值提现
	private HuiLiCaiFinanceListPage hlc; //惠理财投资列表页
	private HuiLiCaiFinanceDetailPage fd; //惠理财投资详细页
	private HuiLiCaiFinanceConfirmPage cp; //惠理财投资确认页
	DBquery db = new DBquery();
	
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        bagpage = new MyRedBagPage(driver,wait); 
        ticket = new MyInvestTicketPage(driver,wait);
        myletter = new MyInsideLetterPage(driver,wait);
        cashrecord = new MyQueryCashRecordListPage(driver,wait);
        hlc = new HuiLiCaiFinanceListPage(driver,wait);
        fd = new HuiLiCaiFinanceDetailPage(driver,wait);
        cp = new HuiLiCaiFinanceConfirmPage(driver,wait);
    }
	@Test
	public void testAfterInvestHuiLiCaiNewHand() throws ClassNotFoundException, SQLException, InterruptedException,ArithmeticException{
	   
		//点击登录
		this.loadPage();
		this.HLClogin(this.huilicaiinvestUsername,this.huilicaiinvestPassword,this.huilicaiinvestPhone);
		//跳转到定期理财
		this.loginTopNavigationMenu1(3,"定期理财");
		//跳转到新手专区	
		this.clickOnSubmenu("新手专区");
		//等待页面加载
	   fd.waitPage(".//*[@id='display_7']/div/div[2]", 5);
       String proInvestNameNew;
	   String proNameNew;
	   String strPathNew = null;
	   while(true){ 
		   this.clickOnSubmenu("新手专区");
		   proNameNew = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_name from sunif.sif_project_info where subject_name like '惠理财-自动化投资-新-周%' and intr_year='28' and subject_type = '0'","subject_name");
		   System.out.println("惠理财-惠-自动化投资-周的名称为："+ proNameNew);
		   Thread.sleep(2000);
		   if(proNameNew != null&&!proNameNew.equals("")){ 
			   this.clickOnSubmenu("新手专区");
			   break;
		} 
	 }
	   proInvestNameNew = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_name from sunif.sif_project_info where subject_name like '惠理财-自动化投资-新-周%' and intr_year='28' and subject_type = '0'","subject_name");
	   for(int i = 0; i < 100; i++){
			Set<String> set = new LinkedHashSet<String>();
			set.add(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a");
			set.add(".//*[@id='display_7']/div/div[3]/div[1]/div[1]/a");
			set.add(".//*[@id='display_7']/div/div[4]/div[1]/div[1]/a");
			set.add(".//*[@id='display_7']/div/div[5]/div[1]/div[1]/a");
			set.add(".//*[@id='display_7']/div/div[6]/div[1]/div[1]/a");
			set.add(".//*[@id='display_7']/div/div[7]/div[1]/div[1]/a");
			set.add(".//*[@id='display_7']/div/div[8]/div[1]/div[1]/a");
			set.add(".//*[@id='display_7']/div/div[9]/div[1]/div[1]/a");
			set.add(".//*[@id='display_7']/div/div[10]/div[1]/div[1]/a");
			set.add(".//*[@id='display_7']/div/div[11]/div[1]/div[1]/a");
			for(Object element: set){
			     if(this.getElementText(element.toString()).equals(proNameNew)){
			    	 strPathNew = element.toString();
			         break;
			     }
			  }
			if(strPathNew != null && this.getElementText(strPathNew).equals(proNameNew)){
				break;
			}else{
			((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("下一页")));
			 this.driver.findElement(By.linkText("下一页")).click();
			 new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a")));
			 assertTrue("新手专区翻页后第一个产品没有显示",this.getElementText(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a")!=null);
			}
		 }
	   String strPathElementNew = strPathNew.substring(0,strPathNew.length() - 16);
	   System.out.println("截取后的为：" + strPathElementNew);	 		 
	 
	   String currentWindow = this.driver.getWindowHandle();
	   System.out.println("第一个窗口的信息：" + currentWindow);
	   hlc.clickButton(strPathElementNew+"/div[2]/div[2]/a");
	   Thread.sleep(3000);
	   Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
		 System.out.println("当前窗口数为："+ handles.size());
		 Iterator<String> it = handles.iterator();
		 while (it.hasNext()) { 
			 String secondWindow = (String)(it.next());
			 System.out.println("窗口的信息为：" + secondWindow);
			 if (currentWindow.equals(secondWindow)){
				 continue;
  		}
		
		  driver.switchTo().window(secondWindow);//切换到新窗口
		  break;
		 }
	   //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='inputMoney']//span[3]")).isDisplayed();
            }
        });
	  cp.verifyInfo("//div[@class='inputMoney']//span[3]","您已投过新手标，这个福利留给新朋友");
	}
}
