package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.DBquery;
import SecondStage.Hfax.Page.HomePage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceDetailPage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceListPage;
import SecondStage.Hfax.Page.MyInsideLetterPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;

/**
 *  
 * @Description 手动发放卡券，代金券的验证
 * Created by songxq on 01/09/17.
 * 
 */

public class TestManualTicketsUserCash extends BaseTest{

	private MyRedBagPage bagpage; //我的红包
	private MyInvestTicketPage ticket;//我的投资券
	private MyInsideLetterPage myletter;//站内信
	private MyQueryCashRecordListPage cashrecord; //充值提现
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        bagpage = new MyRedBagPage(driver,wait);
        ticket = new MyInvestTicketPage(driver,wait);
        myletter = new MyInsideLetterPage(driver,wait);
        cashrecord = new MyQueryCashRecordListPage(driver,wait);
    }

	@Test
	public void testUserCashInfo(){
		Map<String, String> envVars = System.getenv();
		this.loadPage();
        this.HLClogin(this.manualticketsUsername,this.manualticketsPassword,this.manualticketsPhone);
        this.loginTopNavigationMenu(5,"我的账户");
        
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	 //我的福利
			this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_touziquan']")).isDisplayed();
			     }
			 });
			//投资券
			this.driver.findElement(By.xpath("//a[@id='menu_touziquan']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			    @Override
			    public Boolean apply(WebDriver webDriver) {
			        return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
			    }
			});
        }else{
        	this.leftNavigationSubMenu(6,1,"投资券");
        }
       
        ticket.verifyTicketInfo("10","10","0","0");
		this.newSelectTab(1, "未使用的投资券");
		//手动代金券的面值，25元，25元，26元
		ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","26", 0);
		ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","25", 1);
		ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","25", 2);
		
		//手动代金券,可投资于：惠理财 惠投资 惠聚财
		 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 0);
		 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 1);
		 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 2);
		
		//手动代金券，有效期：当前日期，当前日期+4
		 //获取当前日期
		 String getCurrentDate = this.getDate(0);
		 //把当前日期加4天
		 String getDateFour = this.getDate(4);
		 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 0);
		 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 1);
		 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 2);
		
		//手动代金券，获得来源：上传
		 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：上传", 0);
		 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：上传", 1);
		 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：上传", 2);
		
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
			//手动代金券，使用条件：无
			 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"无投资金额和产品期限要求", 0);
			 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"无投资金额和产品期限要求", 1);
			 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"无投资金额和产品期限要求", 2);
		 }else{
			 //手动代金券，使用条件：无
			 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"无投资金额和产品期限要求", 0);
			 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"无投资金额和产品期限要求", 1);
			 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"无投资金额和产品期限要求", 2);
		 }
	}

}
