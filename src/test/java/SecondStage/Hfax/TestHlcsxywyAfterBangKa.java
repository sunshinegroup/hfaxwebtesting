package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.BasePage;
import SecondStage.Hfax.Page.MyInsideLetterPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;
import SecondStage.Hfax.Page.UserInfoPage;
/**
 *  
 * @Description 惠理财寿险业务员绑卡后的验证
 * 验证我的红包，我的投资券，站内信,充值提现
 * 切记：需要执行一条站内信的sql
 * Created by songxq on 22/11/16.
 * 
 * 
 */
public class TestHlcsxywyAfterBangKa extends BaseTest{
	
	private MyRedBagPage bagpage; //我的红包
	private MyInvestTicketPage ticket;//我的投资券
	private MyInsideLetterPage myletter;//站内信
	private MyQueryCashRecordListPage cashrecord; //充值提现
	
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        bagpage = new MyRedBagPage(driver,wait);
        ticket = new MyInvestTicketPage(driver,wait);
        myletter = new MyInsideLetterPage(driver,wait);
        cashrecord = new MyQueryCashRecordListPage(driver,wait);
    }
	@Test
	public void test_AfterBangKaInfo() {
		Map<String, String> envVars = System.getenv();
		this.loadPage();
        this.HLClogin(this.hlcsxywyUsername,this.hlcsxywyPassword,this.hlcsxywyPhone);
        this.loginTopNavigationMenu(5,"我的账户");
        
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
          	 
   		    //我的福利
        	this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']//a")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_redPacket']")).isDisplayed();
			     }
			 });
        	//红包
        	this.driver.findElement(By.xpath("//a[@id='menu_redPacket']")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
  			     @Override
  			     public Boolean apply(WebDriver webDriver) {
  			         return driver.findElement(By.xpath("//ul[@class='repay_list']//li[@class='light']")).isDisplayed();
  			     }
  			 });	
            
        }else{
        	this.leftNavigationSubMenu(6,2,"红包");
        	}
        List<WebElement> table = this.driver.findElements(By.xpath("//tbody/tr[2]/td"));
	    bagpage.verifyTableInfo(new String[] {"￥33.00", "绑卡成功",this.getDate(0),this.getDate(0),"已发放"},table);
	    
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	       	 //我的福利
				this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
				this.wait.until(new ExpectedCondition<Boolean>() {
				     @Override
				     public Boolean apply(WebDriver webDriver) {
				         return driver.findElement(By.xpath("//a[@id='menu_touziquan']")).isDisplayed();
				     }
				 });
				//投资券
				this.driver.findElement(By.xpath("//a[@id='menu_touziquan']")).click();
				this.wait.until(new ExpectedCondition<Boolean>() {
				    @Override
				    public Boolean apply(WebDriver webDriver) {
				        return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
				    }
				});
	       }else{
	    	   this.leftNavigationSubMenu(6,1,"投资券");
	       }
	    ticket.verifyTicketInfo("9","9","0","0");
	    //验证代金券和加息券的面值,31元代金券和3.2%加息券
	    ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","31", 0);
	    ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","3.2", 5);
	    
	    //新增31元代金券,新增3.2%加息券,可用于投资：惠理财 惠投资 惠聚财
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 5);

	    //新增31元代金券,新增3.2%加息券,有效期
	    //获取当前日期
	    String getCurrentDate = this.getDate(0);
	    //把当前日期加4天
	    String getDateFour = this.getDate(4);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 5);
	    
	    //新增31元代金券,新增3.2%加息券，获得来源
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：绑卡成功", 0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：绑卡成功", 5);
	    
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	//新增31元代金券,新增3.2%加息券，使用条件
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 0);
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 5);
	    }else{
	    	  //新增31元代金券,新增3.2%加息券，使用条件
		    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 0);
		    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 5);
	    }
	    
	    //新增31元代金券,新增3.2%加息券，使用限制
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用绑卡代金31元；5-10K,30-60d", 0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用绑卡加息3.2%；5-10K,30-60d", 5);
	    
	    //验证站内信的条数
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
    		//等待页面加载
    		 this.wait.until(new ExpectedCondition<Boolean>() {
    	            @Override
    	            public Boolean apply(WebDriver webDriver) {
    	                return driver.findElement(By.xpath("//li[@id='accountManage_menuItem']")).isDisplayed();
    	            }
    	        });
    		//点击账户管理
    		 this.driver.findElement(By.xpath("//li[@id='accountManage_menuItem']")).click();
    		//等待页面加载
    		 this.wait.until(new ExpectedCondition<Boolean>() {
    	            @Override
    	            public Boolean apply(WebDriver webDriver) {
    	                return driver.findElement(By.xpath("//dl[@id='insideLetter']//a")).isDisplayed();
    	            }
    	        });
    		 //站内信
    		 this.driver.findElement(By.xpath("//dl[@id='insideLetter']//a")).click();
	    	}else{
	    	
	    		this.leftNavigationSubMenu(7,2,"站内信");
	    	}
	    myletter.waitPage(".//*[@id='biaoge']/table/tbody/tr",5);
	    List<WebElement> messages = this.driver.findElements(By.xpath(".//*[@id='biaoge']/table/tbody/tr"));
	    System.out.println("站内信的条数："+ messages.size());
	    if(messages.size() != 4){
	    	fail("惠理财业务员绑卡后站内信条数不正确！");
	    }
	    for(int i = 2 ; i < 5; i++){
	    	if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    		//等待页面加载
	    		 this.wait.until(new ExpectedCondition<Boolean>() {
	    	            @Override
	    	            public Boolean apply(WebDriver webDriver) {
	    	                return driver.findElement(By.xpath("//li[@id='accountManage_menuItem']")).isDisplayed();
	    	            }
	    	        });
	    		//点击账户管理
	    		 this.driver.findElement(By.xpath("//li[@id='accountManage_menuItem']")).click();
	    		//等待页面加载
	    		 this.wait.until(new ExpectedCondition<Boolean>() {
	    	            @Override
	    	            public Boolean apply(WebDriver webDriver) {
	    	                return driver.findElement(By.xpath("//dl[@id='insideLetter']//a")).isDisplayed();
	    	            }
	    	        });
	    		 //站内信
	    		 this.driver.findElement(By.xpath("//dl[@id='insideLetter']//a")).click();
		    }else{
		    	
		    	this.leftNavigationSubMenu(7,2,"站内信");
		    }
	    	while(true){
	    		if(IsElementPresent(By.xpath(".//*[@id='biaoge']/table/tbody/tr["+i+"]/td[3]/a"))){
	    			break;
	    		}
	    	}
		    myletter.waitPage(".//*[@id='biaoge']/table/tbody/tr["+i+"]/td[3]/a",5);
		    myletter.clickTitle(".//*[@id='biaoge']/table/tbody/tr["+i+"]/td[3]/a");
		    myletter.waitPage("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]",5);
		    if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("33元现金红包")){
		    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","红包福利");
			    myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.hlcsxywyName+"女士,恭喜您获得33元现金红包，可立即用于投资或发起提现，欢迎登录惠金所平台查看使用。");
			    
		    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("3.20%加息券")){
		    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","提醒：有您的投资券");
			    myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.hlcsxywyName+"女士,恭喜您获得3.20%加息券,"+getCurrentDate+"至"+this.getDate(4)+"内有效,具体使用规则可登录惠金所官网或APP个人账户查看，欢迎及时使用，如需帮助可致电400-015-8800(9:00-21:00)。");
			    																							     
		    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("31元代金券")){
		    	 myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","提醒：有您的投资券");
		 	     myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.hlcsxywyName+"女士,恭喜您获得31元代金券,"+getCurrentDate+"至"+this.getDate(4)+"内有效,具体使用规则可登录惠金所官网或APP个人账户查看，欢迎及时使用，如需帮助可致电400-015-8800(9:00-21:00)。");
		    }else{
		    	fail("用户绑卡后站内信33元现金红包，3.20%加息券，31元代金券信息显示不正确");
		    }
	    }

	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//ul[@id='account_leftMenu']//li[3]//a")).isDisplayed();
	             } });
	    	//点击充值提现
	    	this.driver.findElement(By.xpath("//ul[@id='account_leftMenu']//li[3]//a")).click();
	    	
	    }else{
	    	this.leftNavigationMenu(3,"充值/提现");
	    }
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th")).isDisplayed();
            }
        });
	    List<WebElement> fundrecord = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th"));
	    cashrecord.verifyTableInfo(new String[] {"序号", "时间", "操作类型","备注","收入","支出","可用余额"},fundrecord,7);
	    List<WebElement> fundrecordfirst = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td"));
	    cashrecord.verifyTableInfo(new String[] {"1",getCurrentDate, "红包福利","投资发放红包","33.00","0.00","56.00"},fundrecordfirst,7);
	}

}
