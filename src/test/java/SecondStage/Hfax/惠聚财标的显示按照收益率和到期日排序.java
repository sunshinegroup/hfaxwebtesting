package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 惠聚财标的显示按照收益率和到期日排序  extends AccountBaseTest{
	/**
	 * @Description 惠聚财标的显示按照收益率和到期日排序
	 * Created by songxq on 21/9/16.
	 */

	protected String proName1 = "PiLiangYue001";
	protected String proName2 = "PiLiangYue002";
	protected String proName3 = "PiLiangYue003";
	protected String proName4 = "PiLiangYue004";
	protected String proName5 = "PiLiangYue005";
	protected String proName6 = "PiLiangYue006";
	                       
	@Test
	public void test001_惠聚财标的显示按照收益率和到期日排序  () {
		
		this.navigateToPage("定期理财");
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
        WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
        
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")));
        //等待惠聚财第一个产品
        assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")).isDisplayed());
        
        //验证第一个标的名字
        System.out.println("第一个标的的名字是：" + this.getElementText(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a"));
        assertTrue("第一个标的排序不正确",this.getElementText(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a").equals(proName1));
        
        //验证预期年化利率
        System.out.println("【惠聚财投资页】第一个标的预期年化利率为: " + this.driver.findElement(By.xpath("//p[@class='benefit-sider-percent']")).getText().trim());
        assertTrue("【惠聚财投资页】第一个标的预期年化利率不正确",this.driver.findElement(By.xpath("//p[@class='benefit-sider-percent']")).getText().trim().equals("699.00%"));
        
        //验证投资期限
        List<WebElement> list = this.driver.findElements(By.xpath("//p[@class='benefit-sider-date']"));
        System.out.println("【惠聚财投资页】第一个标的投资期限为：" + list.get(0).getText().trim());
        assertTrue("【惠聚财投资页】第一个标的投资期限不正确:",list.get(0).getText().trim().equals("333 天"));
        
        
        //验证起投金额
        System.out.println("【惠聚财投资页】第一个标的起投金额为: "+ list.get(1).getText().trim());
        assertTrue("【惠聚财投资页】第一个标的起投金额不正确",list.get(1).getText().trim().equals("1百元"));
        
        //验证还款方式
        System.out.println("【惠聚财投资页】第一个标的还款方式为:"+ list.get(2).getText().trim());
        assertTrue("【惠聚财投资页】第一个标的还款方式不正确",list.get(2).getText().trim().equals("一次性还款"));
        
        //验证项目总金额
        System.out.println("【惠聚财投资页】第一个标的项目总金额为:"+ this.driver.findElement(By.xpath("//p[@class='total-mon listAmount']/i")).getText().trim());
        assertTrue("【惠聚财投资页】第一个标的项目总金额不正确",this.driver.findElement(By.xpath("//p[@class='total-mon listAmount']/i")).getText().trim().equals("1万元"));
        
        //验证项目进度条
        System.out.println("【惠聚财投资页】第一个标的进度条为" + this.getElementText(".//*[@id='display_15']/div/div[2]/div[2]/div[1]/div/span"));
        assertTrue("【惠聚财投资页】第一标的进度条为：",this.getElementText(".//*[@id='display_15']/div/div[2]/div[2]/div[1]/div/span").equals("0%"));
        
        //验证标的状态
        System.out.println("【惠聚财投资页】第一标的状态为：" + this.getElementText(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a"));
        assertTrue("【惠聚财投资页】第一标的状态不正确：",this.getElementText(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a").equals("立即投资"));
        
        //验证第二个标的名字
        System.out.println("第二个标的的名字是：" + this.getElementText(".//*[@id='display_15']/div/div[3]/div[1]/div[1]/a"));
        assertTrue("第二个标的排序不正确",this.getElementText(".//*[@id='display_15']/div/div[3]/div[1]/div[1]/a").equals(proName2));
        
        //验证预期年化利率
        System.out.println("【惠聚财投资页】第二个标的预期年化利率为: " + this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[3]/div[1]/div[2]/ul/li[1]/p[2]")).getText().trim());
        assertTrue("【惠聚财投资页】第二个标的预期年化利率不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[3]/div[1]/div[2]/ul/li[1]/p[2]")).getText().trim().equals("698.00%"));
        
        //验证投资期限
        System.out.println("【惠聚财投资页】第二个标的投资期限为：" + this.getElementText(".//*[@id='display_15']/div/div[3]/div[1]/div[2]/ul/li[2]/p[2]"));
        assertTrue("【惠聚财投资页】第二个标的投资期限不正确:",this.getElementText(".//*[@id='display_15']/div/div[3]/div[1]/div[2]/ul/li[2]/p[2]").equals("333 天"));
        
        
        //验证起投金额
        System.out.println("【惠聚财投资页】第二个标的起投金额为: "+ this.getElementText(".//*[@id='display_15']/div/div[3]/div[1]/div[2]/ul/li[3]/p[2]/i"));
        assertTrue("【惠聚财投资页】第二个标的起投金额不正确",this.getElementText(".//*[@id='display_15']/div/div[3]/div[1]/div[2]/ul/li[3]/p[2]/i").equals("1百元"));
        
        //验证还款方式
        System.out.println("【惠聚财投资页】第二个标的还款方式为:"+ this.getElementText(".//*[@id='display_15']/div/div[3]/div[1]/div[2]/ul/li[4]/p[2]/span"));
        assertTrue("【惠聚财投资页】第二个标的还款方式不正确",this.getElementText(".//*[@id='display_15']/div/div[3]/div[1]/div[2]/ul/li[4]/p[2]/span").equals("一次性还款"));
        
        //验证项目总金额
        System.out.println("【惠聚财投资页】第二个标的项目总金额为:"+ this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[3]/div[2]/div[1]/p[2]/i")).getText().trim());
        assertTrue("【惠聚财投资页】第二个标的项目总金额不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[3]/div[2]/div[1]/p[2]/i")).getText().trim().equals("1万元"));
        
        
        //验证项目进度条
        System.out.println("【惠聚财投资页】第二个标的进度条为" + this.getElementText(".//*[@id='display_15']/div/div[2]/div[2]/div[1]/div/span"));
        assertTrue("【惠聚财投资页】第二个标的进度条为：",this.getElementText(" .//*[@id='display_15']/div/div[3]/div[2]/div[1]/div/span").equals("0%"));
        
        System.out.println("【惠聚财投资页】第二标的状态为：" + this.getElementText(".//*[@id='display_15']/div/div[3]/div[2]/div[2]/a"));
        assertTrue("【惠聚财投资页】第二标的状态不正确：",this.getElementText(".//*[@id='display_15']/div/div[3]/div[2]/div[2]/a").equals("立即投资"));
        
        //验证第三个标的名字
        System.out.println("第三个标的的名字是：" + this.getElementText(".//*[@id='display_15']/div/div[4]/div[1]/div[1]/a"));
        assertTrue("第三个标的排序不正确",this.getElementText(".//*[@id='display_15']/div/div[4]/div[1]/div[1]/a").equals(proName3));
        
        //验证预期年化利率
        System.out.println("【惠聚财投资页】第三个标的预期年化利率为: " + this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[4]/div[1]/div[2]/ul/li[1]/p[2]")).getText().trim());
        assertTrue("【惠聚财投资页】预期年化利率不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[4]/div[1]/div[2]/ul/li[1]/p[2]")).getText().trim().equals("697.00%"));
        
        //验证投资期限
        System.out.println("【惠聚财投资页】第三个标的投资期限为：" + this.getElementText("//*[@id='display_15']/div/div[4]/div[1]/div[2]/ul/li[2]/p[2]"));
        assertTrue("【惠聚财投资页】第三个标的投资期限不正确:",this.getElementText("//*[@id='display_15']/div/div[4]/div[1]/div[2]/ul/li[2]/p[2]").equals("333 天"));
        
        
        //验证起投金额
        System.out.println("【惠聚财投资页】第三个标的起投金额为: "+ this.getElementText(".//*[@id='display_15']/div/div[4]/div[1]/div[2]/ul/li[3]/p[2]/i"));
        assertTrue("【惠聚财投资页】第三个标的起投金额不正确",this.getElementText(".//*[@id='display_15']/div/div[4]/div[1]/div[2]/ul/li[3]/p[2]/i").equals("1百元"));
        
        //验证还款方式
        System.out.println("【惠聚财投资页】第三个标的还款方式为:"+ this.getElementText(".//*[@id='display_15']/div/div[4]/div[1]/div[2]/ul/li[4]/p[2]/span"));
        assertTrue("【惠聚财投资页】第三个标的还款方式不正确",this.getElementText(".//*[@id='display_15']/div/div[4]/div[1]/div[2]/ul/li[4]/p[2]/span").equals("一次性还款"));
        
        //验证项目总金额
        System.out.println("【惠聚财投资页】第三个标的项目总金额为:"+ this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[4]/div[2]/div[1]/p[2]/i")).getText().trim());
        assertTrue("【惠聚财投资页】第三个标的项目总金额不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[4]/div[2]/div[1]/p[2]/i")).getText().trim().equals("1万元"));
        
        System.out.println("【惠聚财投资页】第三个标的进度条为" + this.getElementText(".//*[@id='display_15']/div/div[4]/div[2]/div[1]/div/span"));
        assertTrue("【惠聚财投资页】第三个标的进度条为：",this.getElementText(".//*[@id='display_15']/div/div[4]/div[2]/div[1]/div/span").equals("0%"));
        
        System.out.println("【惠聚财投资页】第三标的状态为：" + this.getElementText(".//*[@id='display_15']/div/div[4]/div[2]/div[2]/a"));
        assertTrue("【惠聚财投资页】第三标的状态不正确：",this.getElementText(".//*[@id='display_15']/div/div[4]/div[2]/div[2]/a").equals("立即投资"));
        
        //验证第四个标的名字
        System.out.println("第四个标的的名字是：" + this.getElementText(".//*[@id='display_15']/div/div[5]/div[1]/div[1]/a"));
        assertTrue("第四个标的排序不正确",this.getElementText(".//*[@id='display_15']/div/div[5]/div[1]/div[1]/a").equals(proName4));
        
        //验证预期年化利率
        System.out.println("【惠聚财投资页】第四个标的预期年化利率为: " + this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[5]/div[1]/div[2]/ul/li[1]/p[2]")).getText().trim());
        assertTrue("【惠聚财投资页】第四个标的预期年化利率不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[5]/div[1]/div[2]/ul/li[1]/p[2]")).getText().trim().equals("600.00%"));
        
        //验证投资期限
        System.out.println("【惠聚财投资页】第四个标的投资期限为：" + this.getElementText(".//*[@id='display_15']/div/div[5]/div[1]/div[2]/ul/li[2]/p[2]"));
        assertTrue("【惠聚财投资页】第四个标的投资期限不正确:",this.getElementText(".//*[@id='display_15']/div/div[5]/div[1]/div[2]/ul/li[2]/p[2]").equals("333 天"));
        
        
        //验证起投金额
        System.out.println("【惠聚财投资页】第四个标的起投金额为: "+ this.getElementText(".//*[@id='display_15']/div/div[5]/div[1]/div[2]/ul/li[3]/p[2]/i"));
        assertTrue("【惠聚财投资页】第四个标的起投金额不正确",this.getElementText(".//*[@id='display_15']/div/div[5]/div[1]/div[2]/ul/li[3]/p[2]/i").equals("1百元"));
        
        //验证还款方式
        System.out.println("【惠聚财投资页】第四个标的还款方式为:"+ this.getElementText(".//*[@id='display_15']/div/div[5]/div[1]/div[2]/ul/li[4]/p[2]/span"));
        assertTrue("【惠聚财投资页】第四个标的还款方式不正确",this.getElementText(".//*[@id='display_15']/div/div[5]/div[1]/div[2]/ul/li[4]/p[2]/span").equals("一次性还款"));
        
        //验证项目总金额
        System.out.println("【惠聚财投资页】第四个标的项目总金额为:"+ this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[5]/div[2]/div[1]/p[2]/i")).getText().trim());
        assertTrue("【惠聚财投资页】第四个标的项目总金额不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[5]/div[2]/div[1]/p[2]/i")).getText().trim().equals("1万元"));
        
        System.out.println("【惠聚财投资页】第四个标的进度条为" + this.getElementText(".//*[@id='display_15']/div/div[5]/div[2]/div[1]/div/span"));
        assertTrue("【惠聚财投资页】第四个标的进度条为：",this.getElementText(".//*[@id='display_15']/div/div[5]/div[2]/div[1]/div/span").equals("0%"));
        
        System.out.println("【惠聚财投资页】第四标的状态为：" + this.getElementText(".//*[@id='display_15']/div/div[5]/div[2]/div[2]/a"));
        assertTrue("【惠聚财投资页】第四标的状态不正确：",this.getElementText(".//*[@id='display_15']/div/div[5]/div[2]/div[2]/a").equals("立即投资"));
        
        //验证第五个标的名字
        System.out.println("第五个标的的名字是：" + this.getElementText(".//*[@id='display_15']/div/div[6]/div[1]/div[1]/a"));
        assertTrue("第五个标的排序不正确",this.getElementText(".//*[@id='display_15']/div/div[6]/div[1]/div[1]/a").equals(proName5));
        
        //验证预期年化利率
        System.out.println("【惠聚财投资页】第五个标的预期年化利率为: " + this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[1]/div[2]/ul/li[1]/p[2]")).getText().trim());
        assertTrue("【惠聚财投资页】第五个标的预期年化利率不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[1]/div[2]/ul/li[1]/p[2]")).getText().trim().equals("600.00%"));
        
        //验证投资期限
        System.out.println("【惠聚财投资页】第五个标的投资期限为：" + this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[1]/div[2]/ul/li[2]/p[2]")).getText().trim());
        assertTrue("【惠聚财投资页】第五个标的投资期限不正确:",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[1]/div[2]/ul/li[2]/p[2]")).getText().trim().equals("334 天"));
        
        
        //验证起投金额
        System.out.println("【惠聚财投资页】第五个标的起投金额为: "+ this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[1]/div[2]/ul/li[3]/p[2]/i")).getText().trim());
        assertTrue("【惠聚财投资页】第五个标的起投金额不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[1]/div[2]/ul/li[3]/p[2]/i")).getText().trim().equals("1百元"));
        
        //验证还款方式
        System.out.println("【惠聚财投资页】第五个标的还款方式为:"+ this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[1]/div[2]/ul/li[4]/p[2]/span")).getText().trim());
        assertTrue("【惠聚财投资页】第五个标的还款方式不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[1]/div[2]/ul/li[4]/p[2]/span")).getText().trim().equals("一次性还款"));
        
        //验证项目总金额
        System.out.println("【惠聚财投资页】第五个标的项目总金额为:"+ this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[2]/div[1]/p[2]/i")).getText().trim());
        assertTrue("【惠聚财投资页】第五个标的项目总金额不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[2]/div[1]/p[2]/i")).getText().trim().equals("1万元"));
        
        
        System.out.println("【惠聚财投资页】第五个标的进度条为:"+ this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[2]/div[1]/div/span")).getText().trim());
        assertTrue("【惠聚财投资页】第五个标的进度条不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[6]/div[2]/div[1]/div/span")).getText().trim().equals("0%"));
        
        System.out.println("【惠聚财投资页】第五标的状态为：" + this.getElementText(".//*[@id='display_15']/div/div[6]/div[2]/div[2]/a"));
        assertTrue("【惠聚财投资页】第五标的状态不正确：",this.getElementText(".//*[@id='display_15']/div/div[6]/div[2]/div[2]/a").equals("立即投资"));
        
        //验证第六个标的名字
        System.out.println("第六个标的的名字是：" + this.getElementText(".//*[@id='display_15']/div/div[7]/div[1]/div[1]/a"));
        assertTrue("第六个标的名字不正确",this.getElementText(".//*[@id='display_15']/div/div[7]/div[1]/div[1]/a").equals(proName6));
        
        
        //验证预期年化利率
        System.out.println("【惠聚财投资页】第六个标的预期年化利率为: " + this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[7]/div[1]/div[2]/ul/li[1]/p[2]")).getText().trim());
        assertTrue("【惠聚财投资页】第六个标的预期年化利率不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[7]/div[1]/div[2]/ul/li[1]/p[2]")).getText().trim().equals("600.00%"));
        
        //验证投资期限
        System.out.println("【惠聚财投资页】第六个标的投资期限为：" + this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[7]/div[1]/div[2]/ul/li[2]/p[2]")).getText().trim());
        assertTrue("【惠聚财投资页】第六个标的投资期限不正确:",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[7]/div[1]/div[2]/ul/li[2]/p[2]")).getText().trim().equals("335 天"));
        
        
        //验证起投金额
        System.out.println("【惠聚财投资页】第六个标的起投金额为: "+ this.getElementText(".//*[@id='display_15']/div/div[7]/div[1]/div[2]/ul/li[3]/p[2]/i"));
        assertTrue("【惠聚财投资页】第六个标的起投金额不正确",this.getElementText(".//*[@id='display_15']/div/div[7]/div[1]/div[2]/ul/li[3]/p[2]/i").equals("1百元"));
        
        //验证还款方式
        System.out.println("【惠聚财投资页】第六个标的还款方式为:"+ this.getElementText(".//*[@id='display_15']/div/div[7]/div[1]/div[2]/ul/li[4]/p[2]/span"));
        assertTrue("【惠聚财投资页】第六个标的还款方式不正确",this.getElementText(".//*[@id='display_15']/div/div[7]/div[1]/div[2]/ul/li[4]/p[2]/span").equals("一次性还款"));
        
        //验证项目总金额
        System.out.println("【惠聚财投资页】第六个标的项目总金额为:"+ this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[7]/div[2]/div[1]/p[2]/i")).getText().trim());
        assertTrue("【惠聚财投资页】第六个标的项目总金额不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[7]/div[2]/div[1]/p[2]/i")).getText().trim().equals("1万元"));
        
        //验证项目进度条
        System.out.println("【惠聚财投资页】第六个标的项目进度条为:"+ this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[7]/div[2]/div[1]/div/span")).getText().trim());
        assertTrue("【惠聚财投资页】第六个标的项目进度条不正确",this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[7]/div[2]/div[1]/div/span")).getText().trim().equals("0%"));
        
        System.out.println("【惠聚财投资页】第六标的状态为：" + this.getElementText(".//*[@id='display_15']/div/div[7]/div[2]/div[2]/a"));
        assertTrue("【惠聚财投资页】第六标的状态不正确：",this.getElementText(".//*[@id='display_15']/div/div[7]/div[2]/div[2]/a").equals("立即投资"));
	}

}
