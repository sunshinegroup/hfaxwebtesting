package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 惠聚财满标搜索页列表页站内信 extends 标的显示_惠聚财{

	/**
	 *  
	 * @Description 惠聚财已满标搜索页,列表页，站内信
	 * Created by songxq on 09/12/16.
	 * 
	 */
	Map<String, String> envVars = System.getenv();


	@Test
	public void test001_惠聚财满标投资() {
		this.navigateToPage("定期理财");
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
        WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
        
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")));
        //等待惠聚财第一个产品
        assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")).isDisplayed());
        
        //验证预期年化利率
        System.out.println("【惠聚财投资页】预期年化利率为: " + this.driver.findElement(By.xpath("//p[@class='benefit-sider-percent']")).getText().trim());
        assertTrue("【惠聚财投资页】预期年化利率不正确",this.driver.findElement(By.xpath("//p[@class='benefit-sider-percent']")).getText().trim().equals("700.00%"));
        
        //验证投资期限
        List<WebElement> list = this.driver.findElements(By.xpath("//p[@class='benefit-sider-date']"));
        System.out.println("【惠聚财投资页】投资期限为：" + list.get(0).getText().trim());
        assertTrue("【惠聚财投资页】投资期限不正确:",list.get(0).getText().trim().equals("333 天"));
        
        
        //验证起投金额
        System.out.println("【惠聚财投资页】起投金额为: "+ list.get(1).getText().trim());
        assertTrue("【惠聚财投资页】起投金额不正确",list.get(1).getText().trim().equals("1百元"));
        
        //验证还款方式
        System.out.println("【惠聚财投资页】还款方式为:"+ list.get(2).getText().trim());
        assertTrue("【惠聚财投资页】还款方式不正确",list.get(2).getText().trim().equals("一次性还款"));
        
        //验证项目总金额
        System.out.println("【惠聚财投资页】项目总金额为:"+ this.driver.findElement(By.xpath("//p[@class='total-mon listAmount']/i")).getText().trim());
        assertTrue("【惠聚财投资页】项目总金额不正确",this.driver.findElement(By.xpath("//p[@class='total-mon listAmount']/i")).getText().trim().equals("1万元"));
        
        firstItem.findElement(By.xpath("div[2]/div[2]/a")).click();
        
        //等待输入金额
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='amount']")));
        
        //验证项目金额
        List<WebElement> listSec = this.driver.findElements(By.xpath("//strong[@class='object-money']"));
        System.out.println("【惠聚财投资详细页】项目金额为:" + listSec.get(0).getText().trim());
        assertTrue("【惠聚财投资详细页】项目金额不正确: ",listSec.get(0).getText().trim().equals("10,000.00"));
       
        //验证预期年化收益率
        System.out.println("【惠聚财投资详细页】预期年化收益率为:" + this.driver.findElement(By.xpath("//li[@class='obj-shouyi interest']/i/strong")).getText().trim());
        assertTrue("【惠聚财投资详细页】预期年化收益率不正确",this.driver.findElement(By.xpath("//li[@class='obj-shouyi interest']/i/strong")).getText().trim().equals("700.00%"));
        
        //验证投资期限
        System.out.println("【惠聚财投资详细页】投资期限为:" + this.driver.findElement(By.xpath("//li[@class='obj-money']/i/strong")).getText().trim());
        assertTrue("【惠聚财投资详细页】投资期限不正确",this.driver.findElement(By.xpath("//li[@class='obj-money']/i/strong")).getText().trim().equals("333"));
        
        //验证起投金额
        System.out.println("【惠聚财投资详细页】起投金额为:" + listSec.get(1).getText().trim());
        assertTrue("【惠聚财投资详细页】起投金额 不正确",listSec.get(1).getText().trim().equals("100.00"));
        
        //验证收益方式
        System.out.println("【惠聚财投资详细页】收益方式为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[2]/em")).getText().trim());
        assertTrue("【惠聚财投资详细页】收益方式不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[2]/em")).getText().trim().equals("一次性还本付息"));
        
        //验证产品风险等级
        System.out.println("【惠聚财投资详细页】产品风险等级为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[3]/em")).getText().trim());
        assertTrue("【惠聚财投资详细页】产品风险等级不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[3]/em")).getText().trim().equals("低风险"));
        
        //验证投资人承受条件
        System.out.println("【惠聚财投资详细页】投资人承受条件为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[4]/em")).getText().trim());
        assertTrue("【惠聚财投资详细页】投资人承受条件不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[4]/em")).getText().trim().equals("保守型"));
       
        //验证递增金额
        System.out.println("【惠聚财投资详细页】递增金额为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[7]/em")).getText().trim());
        assertTrue("【惠聚财投资详细页】递增金额不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[7]/em")).getText().trim().equals("100元"));
        
       //验证项目概况
        assertTrue("【惠聚财投资详细页】项目概况不存在",this.driver.findElement(By.id("one1")).getText().trim().equals("项目概况"));
        this.driver.findElement(By.id("one1")).click();
        
        //验证网上开户协议
        assertTrue("【惠聚财投资详细页】网上开户协议不存在",this.driver.findElement(By.id("one2")).getText().trim().equals("网上开户协议"));
        this.driver.findElement(By.id("one2")).click();
        
        //验证产品说明书
        assertTrue("【惠聚财投资详细页】产品说明书不存在",this.driver.findElement(By.id("one3")).getText().trim().equals("产品说明书"));
        this.driver.findElement(By.id("one3")).click();
        
         //验证风险揭示书
        assertTrue("【惠聚财投资详细页】风险揭示书不存在",this.driver.findElement(By.id("one4")).getText().trim().equals("风险揭示书"));
        this.driver.findElement(By.id("one4")).click();
        
        //验证产品认购协议
        assertTrue("【惠聚财投资详细页】产品认购协议不存在",this.driver.findElement(By.id("one5")).getText().trim().equals("产品认购协议"));
        this.driver.findElement(By.id("one5")).click();
        
        //验证投资金额下面的网上开户协议
        assertTrue("【惠聚财投资详细页】投资金额下的网上开户协议不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc2']")).getText().trim().equals("《网上开户协议》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc2']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
        
        //验证投资金额下面的产品说明书
        assertTrue("【惠聚财投资详细页】投资金额下的产品说明书不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc3']")).getText().trim().equals("《产品说明书》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc3']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
        
        //验证投资金额下面的风险揭示书
        assertTrue("【惠聚财投资详细页】投资金额下的风险揭示书不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc4']")).getText().trim().equals("《风险揭示书》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc4']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
        
        //验证投资金额下面的产品认购协议
        assertTrue("【惠聚财投资详细页】投资金额下的产品认购协议不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc1']")).getText().trim().equals("《产品认购协议》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc1']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
        
        
        //输入投资金额
        this.driver.findElement(By.xpath(".//*[@id='amount']")).clear();
        this.driver.findElement(By.xpath(".//*[@id='amount']")).sendKeys("10000");
       
        
        //验证投资金额下的预期收益
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='interests']")));
        assertTrue("【惠聚财投资详细页】投资金额下预期收益不正确",this.driver.findElement(By.xpath(".//*[@id='interests']")).getText().trim().equals("64,750.00"));
        
        
        //点击已同意
        this.driver.findElement(By.xpath(".//*[@id='agre']")).click();
        
        //点击去投资
        this.driver.findElement(By.linkText("去投资")).click();
        
        //等待实际投资本金(元)
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText();
                return text.trim().equalsIgnoreCase("10,000.00");
            }
        });
        //验证实际投资本金(元)
        System.out.println("【惠聚财确认页】实际投资本金(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim());
        assertTrue("【惠聚财确认页】实际投资本金(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim().equals("10,000.00"));
        
        //验证总预期收益率(%)
        System.out.println("【惠聚财确认页】总预期收益率(%)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim() + "%");
        assertTrue("【惠聚财确认页】总预期收益率(%)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim().equals("700.00"));
        
        //验证总预期收益(元)
        System.out.println("【惠聚财确认页】总预期收益(元)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim());
        assertTrue("【惠聚财确认页】总预期收益(元)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim().equals("64,750.00"));
        
        //验证实际支付金额(元)
        System.out.println("【惠聚财确认页】实际支付金额(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim());
        assertTrue("【惠聚财确认页】实际支付金额(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim().equals("10,000.00"));
        
        
        //输入交易密码
        this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).clear();
        this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).sendKeys(this.jiaoyipassword);
        
        
        //点击确定投资
        this.driver.findElement(By.xpath(".//*[@id='besureInvest']")).click();
        
        
        //等待确认框
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[1]")).getText();
                return text.trim().equalsIgnoreCase("浏览更多项目");
            }
        });
        
        //验证确认框本次投资金额
        System.out.println("【惠聚财确认框】本次投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim());
        assertTrue("【惠聚财确认框】本次投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim().equals("10,000.00"));
        
        //验证本项目累计投资金额
        System.out.println("【惠聚财确认框】本项目累计投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim());
        assertTrue("【惠聚财确认框】本项目累计投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim().equals("10,000.00"));
        
        //验证查看申请是否存在
        assertTrue("【惠聚财确认框】查看申请不存在:",this.driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[2]")).isDisplayed());
        //点击查看申请
        this.driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[2]")).click();
	}

	@Test
	public void test002_搜索页搜索已满标的产品() {
		//等待我的主页
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("我的主页")));
        
        //我的投资->惠聚财
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			this.selectMenuIE(10, "我的投资");
		  
		 }else{
		 
			 this.selectMenu(10, "我的投资");
		 }
        this.selectSubMenu(5, "惠聚财");
        
         //选择已满标的产品
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("borrowstatus")));
        assertTrue("请选择下拉框不存在",this.driver.findElement(By.id("borrowstatus")).getText().contains("--请选择--"));
        this.searchFinancePro("已满标");
        
        //点击搜索
        assertTrue("搜索不存在",this.driver.findElement(By.id("search")).isDisplayed());
        this.driver.findElement(By.id("search")).click();
        
        //验证惠聚财产品列表不为空
        List<WebElement> list = this.driver.findElements(By.xpath("//tr//td[@align='center']//a[@target='_blank']")); 
  	    assertTrue("不存在惠聚财的产品",list.size()>0);
  	    
  	    //验证搜索页的本金(元)
  	    List<WebElement> list1 = this.driver.findElements(By.xpath("//td[@align='center']"));
  	    System.out.println("【惠聚财搜索页】的本金(元)为:"+ list1.get(1).getText().trim());
  	    assertTrue("惠聚财搜索页】的本金(元)不正确",list1.get(1).getText().trim().equals("10,000.00"));
  	    
  	    //验证搜索页的预计收益(元)
  	    System.out.println("【惠聚财搜索页】的预计收益(元)为:"+ list1.get(2).getText().trim());
  	    assertTrue("【惠聚财搜索页】的预计收益(元)不正确",list1.get(2).getText().trim().equals("64,750.00"));
  	    
  	    //验证搜索页的起息时间
  	    System.out.println("【惠聚财搜索页】的起息时间为:"+ list1.get(4).getText().trim());
	    assertTrue("【惠聚财搜索页】的起息时间不正确",list1.get(4).getText().trim().equals("20170102"));
  	    
  	   //验证搜索页的到期时间
	    System.out.println("【惠聚财搜索页】的到期时间为:"+ list1.get(5).getText().trim());
	    assertTrue("【惠聚财搜索页】的到期时间不正确",list1.get(5).getText().trim().equals("20171201"));
  	    
  	    //验证搜索页的标的状态
	    System.out.println("【惠聚财搜索页】的标的状态为:"+ list1.get(6).getText().trim());
	    assertTrue("【惠聚财搜索页】的标的状态不正确",list1.get(6).getText().trim().equals("已满标"));
	    
  	   }

	@Test
	public void test003_列表页搜索已满标的产品() throws InterruptedException {
		showProject(0,"Yuetestproject001","已满标","100%");
	}

	@Test
	public void test004_惠聚财已满标站内信(){
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
	      {
					this.selectMenuIE(4, "站内信");
				  
		}else{
				 
					this.selectMenu(4, "站内信");
		}

		//等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[2]/td[1]")).isDisplayed();
	            }
	       });
		 assertTrue("惠聚财满标站内信状态不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[2]/td[1]").equals("未读"));
		 assertTrue("惠聚财满标标题不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[2]/td[3]/a").equals("投资申请成功"));
		 
		 //点击投资申请成功
		 this.driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[2]/td[3]/a")).click();
		 
		 //等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong")).isDisplayed();
	            }
	       });
	
		 //验证发件人
		 assertTrue("站内信发件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong").equals("管理员"));
		 //验证收件人
		 assertTrue("站内信收件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[2]/td[2]").equals(this.username));
		 //验证标题
		 assertTrue("站内信标题不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[3]/td[2]").equals("投资申请成功"));
		 //获取系统当前日期
		 Date dt=new Date();
	     SimpleDateFormat matter1=new SimpleDateFormat("yyyy-MM-dd");
	     System.out.println("系统当前日期为：" + matter1.format(dt));
		 String date = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[4]/td[2]").substring(0,10);
		 //验证日期
		 assertTrue("站内信日期不正确",date.equals(matter1.format(dt)));
		 
		 //验证内容姓名
		 String username = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").substring(0,7);
		 System.out.println("用户姓名为:" + username);
		 assertTrue("站内信用户姓名不正确",username.equals("尊敬的晓东先生"));
		 //验证项目内容
		 String proContent = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").substring(18,this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").length());
		 System.out.println("项目内容为:" + proContent);
		 assertTrue("站内信项目内容不正确",proContent.equals("您已成功提交一笔金额为10000元的Yuetestproject001项目投资申请,项目成立后将立即通知您,请稍后关注。"));
		
	}
}
