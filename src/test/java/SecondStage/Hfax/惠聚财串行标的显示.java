package SecondStage.Hfax;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 惠聚财串行标的显示  extends 标的显示_惠聚财 {
	/**
	 *  
	 * @throws InterruptedException 
	 * @Description 惠聚财串行标的显示
	 * Created by songxq on 09/22/16.
	 * 
	 */

	@Test
	public void investProHui() throws InterruptedException {
		showProject(0,"ChuanYue001","立即投资","0%");
		investPro("10,000.00","64,750.00",this.jiaoyipassword);
		
		this.navigateToPage("定期理财");
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
		WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
		        
		new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a")));
		//等待惠聚财第一个产品
		assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a")).isDisplayed());
		
		//第一个串行标满标后，第二个产品显示正确
		System.out.println("第二个串行标的名字为："+this.getElementText(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a"));
		assertTrue("第二个串行标的显示不正确：",this.getElementText(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a").equals("ChuanYue002"));

	}

}
