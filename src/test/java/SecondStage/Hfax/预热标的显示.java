package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.regression.account.AccountBaseTest;
/**
 *  
 * @Description 预热标的显示
 * Created by songxq on 09/20/16.
 * 
 */
public class 预热标的显示  extends 标的显示_惠聚财{

	
	@Test
	public void test001_预热标的展示() throws InterruptedException {
        //预热标的等待5分钟：300000
		showProject(300000,"yuReYue001","即将开始","0%");
		//验证标的状态
		System.out.println("【惠聚财投资详细页】标的状态为:" + this.getElementText("//i[@class='cursor']"));
		assertTrue("【惠聚财投资详细页】标的状态不正确",this.getElementText("//i[@class='cursor']").equals("敬请期待"));

		}

}
