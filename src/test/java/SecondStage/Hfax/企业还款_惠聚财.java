package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.regression.business.BusinessBaseTest;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 企业还款_惠聚财 extends BusinessBaseTest{
	
	/**
	 *  
	 * @Description 企业还款_惠聚财
	 * Created by songxq on 23/8/16.
	 * 
	 */
	Map<String, String> envVars = System.getenv();
	
	private WebElement clickRowCol(int rowNumber, int columnNumber) {
		try{
			WebElement rc = this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]"));
			return rc;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
		
    }
	 @Override
	    public void setup() throws Exception {
	        super.setup();
	        //点击我的账户
	        this.driver.findElement(By.xpath(".//*[@id='index5']/a")).click();
	      
	        //等待页面加载
	        wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath(".//*[@id='index5']/a")).isDisplayed();
	            }
	        });
	        //IE菜单选择
	       if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
			 {
				 this.selectLoanMenuIE(7, "还款管理");
			 }else{
				 this.selectMenu(7, "还款管理");
			 }
	    
	    }
	 
	@Test
	public void test001_正在还款中的借款_还款明细_还款() throws InterruptedException{
		//点击正在还款的借款
		 this.selectSubMenu(2, "正在还款的借款");
		 
		//页面加载等待
		 this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//td[@align='center']"));
	            }
	        });
		 
		
		 List<WebElement> list = this.driver.findElements(By.xpath(".//*[@id='detailtable']/tbody/tr[2]/td")); 
		 //验证标的名称
		 System.out.println("标的名称为：" + list.get(0).getText().trim());
		 assertTrue("【正在还款的借款页】标的名称不正确:",list.get(0).getText().trim().contains("Yuetestproject"));
		 
		 //验证借款金额
		 System.out.println("借款金额为：" + list.get(2).getText().trim());
		 assertTrue("【正在还款的借款页】的借款金额不正确:",list.get(2).getText().trim().equals("10000.00元"));
		 
		 //验证年利率
		 System.out.println("年利率为：" + list.get(3).getText().trim());
		 assertTrue("【正在还款的借款页】的年利率不正确:",list.get(3).getText().trim().equals("700.00%"));
		 
		 //验证还款期限
		 System.out.println("还款期限为：" + list.get(4).getText().trim());
		 assertTrue("【正在还款的借款页】的还款期限不正确:",list.get(4).getText().trim().equals("333天"));
		 
		 //验证还款日期
		 System.out.println("还款日期为：" + list.get(5).getText().trim());
		 assertTrue("【正在还款的借款页】的还款日期不正确:",list.get(5).getText().trim().equals("20171201"));
		 
		 //验证应还本息
		 System.out.println("应还本息为：" + list.get(9).getText().trim());
		 assertTrue("【正在还款的借款页】的应还本息不正确:",list.get(9).getText().trim().equals("￥74750.00"));
		 
		 //验证已还本息
		 System.out.println("已还本息为：" + list.get(10).getText().trim());
		 assertTrue("【正在还款的借款页】的已还本息不正确:",list.get(10).getText().trim().equals("￥0.00"));
		 
		 //验证待还本息
		 System.out.println("待还本息为：" + list.get(11).getText().trim());
		 assertTrue("【正在还款的借款页】的待还本息不正确:",list.get(11).getText().trim().equals("￥74750.00"));
		 
		 //点击还款明细
		 this.driver.findElement(By.linkText("还款明细")).click();
		 
		 //页面加载等待
		 this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.linkText("还款"));
	            }
	        });
		 
		 //验证借款金额
		 System.out.println("【正在还款的借款页】--还款明细中:"+ clickRowCol(2,2).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中借款金额不正确:",clickRowCol(2,2).getText().trim().equals("借款金额：￥10000.00"));
		 
		 //验证借款利率
		 System.out.println("【正在还款的借款页】--还款明细中:"+ clickRowCol(3,1).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中借款利率不正确:",clickRowCol(3,1).getText().trim().equals("借款利率：700.00%"));
		 
		 //验证借款期限
		 System.out.println("【正在还款的借款页】--还款明细中:"+ clickRowCol(3,2).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中借款期限不正确:",clickRowCol(3,2).getText().trim().equals("借款期限：333天"));
		 
		 //还款方式
		 System.out.println("【正在还款的借款页】--还款明细中:"+ clickRowCol(4,1).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中还款方式不正确:",clickRowCol(4,1).getText().trim().equals("还款方式： 一次性还款"));
		 
		 //起息日期
		 System.out.println("【正在还款的借款页】--还款明细中:"+ clickRowCol(4,2).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中起息时间不正确:",clickRowCol(4,2).getText().trim().equals("起息日期：20170102"));	
		 
		 
		 List<WebElement> list1 = this.driver.findElements(By.xpath(" .//*[@id='borrow_details']/div[2]/table/tbody/tr[2]/td"));
		
		 //验证table中计划还款日期
		 System.out.println("【正在还款的借款页】--还款明细中计划还款日期:"+ list1.get(1).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中计划还款日期不正确:",list1.get(1).getText().trim().equals("20171201"));	
		 
		 //验证table中计划还款本息
		 System.out.println("【正在还款的借款页】--还款明细中计划还款本息:"+list1.get(2).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中计划还款本息不正确:",list1.get(2).getText().trim().equals("￥74750.00"));
		 
		 //验证table中总还款金额
		 System.out.println("【正在还款的借款页】--还款明细中总还款金额:"+ list1.get(8).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中总还款金额不正确:",list1.get(8).getText().trim().equals("￥74750.00"));
		 
		 //验证table中状态
		 System.out.println("【正在还款的借款页】--还款明细中状态:"+ list1.get(9).getText().trim());
		 assertTrue("【正在还款的借款页】--还款明细中状态不正确:",list1.get(9).getText().trim().equals("未偿还"));
		 
		//点击还款
		 this.driver.findElement(By.linkText("还款")).click();
		 
		 //页面加载等待
		 this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath(".//*[@id='jbox-content']/div/div/div/div/div/div/table/tbody/tr[6]/td[2]/strong"));
	            }
	        });
		 
		 System.out.println("企业用户账户余额为: "+ this.driver.findElement(By.xpath(" .//*[@id='jbox-content']/div/div/div/div/div/div/table/tbody/tr[1]/td[2]/strong")).getText().trim());
	     assertTrue("企业用户账户余额不正确", this.driver.findElement(By.xpath(" .//*[@id='jbox-content']/div/div/div/div/div/div/table/tbody/tr[1]/td[2]/strong")).getText().trim().equals("5565000.00元")); 
		

	     System.out.println("企业用户可用余额为: "+ this.driver.findElement(By.xpath(" .//*[@id='jbox-content']/div/div/div/div/div/div/table/tbody/tr[2]/td[2]/strong")).getText().trim());
	     assertTrue("企业用户可用余额不正确", this.driver.findElement(By.xpath(" .//*[@id='jbox-content']/div/div/div/div/div/div/table/tbody/tr[2]/td[2]/strong")).getText().trim().equals("5565000.00 元"));
	     
	      System.out.println("需还总额为: "+ this.driver.findElement(By.xpath(".//*[@id='jbox-content']/div/div/div/div/div/div/table/tbody/tr[6]/td[2]/strong")).getText().trim());
	     assertTrue("需还总额不正确", this.driver.findElement(By.xpath(".//*[@id='jbox-content']/div/div/div/div/div/div/table/tbody/tr[6]/td[2]/strong")).getText().trim().equals("74750.00元"));
	     
	     //输入交易密码
	     this.driver.findElement(By.xpath(".//*[@id='pwd']")).clear();
	     this.driver.findElement(By.xpath(".//*[@id='pwd']")).sendKeys(this.businessPassword);
	     
	     //输入验证码
	     this.driver.findElement(By.xpath(".//*[@id='code']")).clear();
	     this.driver.findElement(By.xpath(".//*[@id='code']")).sendKeys("1234");
	     
	     //确认还款
	     this.driver.findElement(By.xpath(".//*[@id='btnsave']")).click();
	     
	     //等待警告框弹出
			this.wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver webDriver) {
			        Alert alert = driver.switchTo().alert();  
			        String str = alert.getText().trim();  
			        System.out.println(str);
					return str.equalsIgnoreCase("操作成功!");
				}
			});
			driver.switchTo().alert().accept();
			
	}

	@Test
	public void test002_已还完的借款(){
		this.selectSubMenu(3, "已还完的借款");
		List<WebElement> list = this.driver.findElements(By.xpath(".//*[@id='borrow_details']/table/tbody/tr[2]/td")); 
		
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='borrow_details']/table/tbody/tr[2]/td")).isDisplayed();
	            }
	        });
		 
		 //已还完的借款借款金额
		 System.out.println("【已还完的借款】借款金额为: "+list.get(2).getText().trim());
	     assertTrue("【已还完的借款】借款金额不正确",list.get(2).getText().trim().equals("10000.00元"));
	     
		 //已还完的借款年利率
	     System.out.println("【已还完的借款】借款年利率为: "+list.get(3).getText().trim());
	     assertTrue("【已还完的借款】借款年利率不正确",list.get(3).getText().trim().equals("700.00%"));
		 
		 //已还完的借款还款期限
	     System.out.println("【已还完的借款】还款期限为: "+list.get(4).getText().trim());
	     assertTrue("【已还完的借款】还款期限不正确",list.get(4).getText().trim().equals("333天"));
		 
		 
		 //已还完的应还本息
	     System.out.println("【已还完的借款】应还本息为: "+list.get(9).getText().trim());
	     assertTrue("【已还完的借款】应还本息不正确",list.get(9).getText().trim().equals("￥74750.00"));
		 
		 //已还完的已还本息
	     System.out.println("【已还完的借款】已还本息为: "+list.get(10).getText().trim());
	     assertTrue("【已还完的借款】已还本息不正确",list.get(10).getText().trim().equals("￥74750.00"));
	     
	     //点击还款明细
	     this.driver.findElement(By.linkText("还款明细")).click();
		 
	   //等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[1]/th[1]")).isDisplayed();
	            }
	        });
		 
		 //验证已还完的借款还款明细中的借款金额
		 System.out.println("【已还完的借款还款明细中】借款金额为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[2]/td[2]")).getText().trim());
	     assertTrue("已还完的借款还款明细中】借款金额不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[2]/td[2]")).getText().trim().equals("借款金额：￥10000.00"));
		 
		 //验证已还完的借款还款明细中的借款利率
	     System.out.println("【已还完的借款还款明细中】借款利率为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[1]")).getText().trim());
	     assertTrue("已还完的借款还款明细中】借款利率不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[1]")).getText().trim().equals("借款利率：700.00%"));
		 
		 //验证已还完的借款还款明细中的借款期限
	     System.out.println("【已还完的借款还款明细中】借款期限为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[2]")).getText().trim());
	     assertTrue("已还完的借款还款明细中】借款期限不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[2]")).getText().trim().equals("借款期限：333天"));
	     
	     //验证已还完的借款还款明细中的还款方式
	     System.out.println("【已还完的借款还款明细中】还款方式为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[1]")).getText().trim());
	     assertTrue("已还完的借款还款明细中】还款方式不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[1]")).getText().trim().equals("还款方式： 一次性还款"));
	     
	     //验证已还完的借款还款明细中的起息日期
	     System.out.println("【已还完的借款还款明细中】起息日期为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[2]")).getText().trim());
	     assertTrue("已还完的借款还款明细中】起息日期不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[2]")).getText().trim().equals("起息日期：20170102"));
	     
	     List<WebElement> list1 = this.driver.findElements(By.xpath(" .//*[@id='borrow_details']/div[2]/table/tbody/tr[2]/td"));
	    
	     //验证table中的计划还款日期
	     System.out.println("【已还完的借款】table中的计划还款日期为: "+list1.get(1).getText().trim());
	     assertTrue("【已还完的借款】table中的计划还款日期不正确",list1.get(1).getText().trim().equals("20171201"));
	     
	     //验证table中的计划还款本息
	     System.out.println("【已还完的借款】table中的计划还款本息为: "+list1.get(2).getText().trim());
	     assertTrue("【已还完的借款】table中的计划还款本息不正确",list1.get(2).getText().trim().equals("￥74750.00"));
	     
	     //验证table中的实还本息
	     System.out.println("【已还完的借款】table中的实还本息为: "+list1.get(5).getText().trim());
	     assertTrue("【已还完的借款】table中的实还本息不正确",list1.get(5).getText().trim().equals("￥74750.00"));
	     
	     //验证table中的总还款金额
	     System.out.println("【已还完的借款】table中的总还款金额为: "+list1.get(8).getText().trim());
	     assertTrue("【已还完的借款】table中的总还款金额不正确",list1.get(8).getText().trim().equals("￥74750.00"));
	     
	     //验证table中的还款状态
	     System.out.println("【已还完的借款】table中的还款状态为: "+list1.get(9).getText().trim());
	     assertTrue("【已还完的借款】table中的还款状态不正确",list1.get(9).getText().trim().equals("已偿还"));
	}
	
	 
}
	
	


