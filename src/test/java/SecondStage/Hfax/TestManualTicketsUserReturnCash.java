package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.MyInsideLetterPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;
/**
 *  
 * @Description 手动发放卡券，返现的验证
 * Created by songxq on 01/09/17.
 * 
 */
public class TestManualTicketsUserReturnCash extends BaseTest{

	private MyRedBagPage bagpage; //我的红包
	private MyInvestTicketPage ticket;//我的投资券
	private MyInsideLetterPage myletter;//站内信
	private MyQueryCashRecordListPage cashrecord; //充值提现
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        bagpage = new MyRedBagPage(driver,wait);
        ticket = new MyInvestTicketPage(driver,wait);
        myletter = new MyInsideLetterPage(driver,wait);
        cashrecord = new MyQueryCashRecordListPage(driver,wait);
    }

	@Test
	public void testUserReturnCashInfo() throws InterruptedException{
		Map<String, String> envVars = System.getenv();
		this.loadPage();
        this.HLClogin(this.manualticketsUsername,this.manualticketsPassword,this.manualticketsPhone);
        this.loginTopNavigationMenu(5,"我的账户");
        
        //红包金额：25元,25元，26元
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
       	 
       		    //我的福利
            	this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
            	this.wait.until(new ExpectedCondition<Boolean>() {
   			     @Override
   			     public Boolean apply(WebDriver webDriver) {
   			         return driver.findElement(By.xpath("//a[@id='menu_redPacket']")).isDisplayed();
   			     }
   			 });
            	//红包
            	this.driver.findElement(By.xpath("//a[@id='menu_redPacket']")).click();
            	this.wait.until(new ExpectedCondition<Boolean>() {
      			     @Override
      			     public Boolean apply(WebDriver webDriver) {
      			         return driver.findElement(By.xpath("//li[@id='red']")).isDisplayed();
      			     }
      			 });	
                
       }else{
    	   this.leftNavigationSubMenu(6,2,"红包");
       }
	   
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//tr[@class='bg_color']/th")).isDisplayed();
            }
        });
	    List<WebElement> table = this.driver.findElements(By.xpath("//tr[@class='bg_color']/th"));
	    bagpage.verifyTableInfo(new String[] {"红包金额", "获取来源", "获得时间","发放时间","状态"},table);
	    
	    //等待返现红包
	    while(true){ 
	    	if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	          	 
       		    //我的福利
            	this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
            	this.wait.until(new ExpectedCondition<Boolean>() {
   			     @Override
   			     public Boolean apply(WebDriver webDriver) {
   			         return driver.findElement(By.xpath("//a[@id='menu_redPacket']")).isDisplayed();
   			     }
   			 });
            	//红包
            	this.driver.findElement(By.xpath("//a[@id='menu_redPacket']")).click();
            	this.wait.until(new ExpectedCondition<Boolean>() {
      			     @Override
      			     public Boolean apply(WebDriver webDriver) {
      			         return driver.findElement(By.xpath("//li[@id='red']")).isDisplayed();
      			     }
      			 });	
                
       }else{
    	   this.leftNavigationSubMenu(6,2,"红包");
       }
			if(this.IsElementPresent(By.xpath("//tbody/tr[4]/td[1]"))){ 
			break;
			}
	    }
	    List<WebElement> updatecount = this.driver.findElements(By.xpath("//tbody//tr"));
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//tbody/tr[4]/td[1]")).isDisplayed();
            }
        });
	    cashrecord.log("返现卡券的个数:" + String.valueOf(updatecount.size()-1));
	    Thread.sleep(4000);
	    //验证我的红包返现26元
	    boolean flagOne = false;
	    for(int k1 = 2; k1 <= updatecount.size(); k1++){
	    cashrecord.log("红包的金额为：" + this.driver.findElement(By.xpath("//tbody/tr["+k1+"]/td[1]")).getText().trim());
	    if(this.driver.findElement(By.xpath("//tbody/tr["+k1+"]/td[1]")).getText().trim().equals("￥26.00")){																					  
   		 List<WebElement> tablefirst = this.driver.findElements(By.xpath("//tbody/tr["+k1+"]/td"));
   		 bagpage.verifyTableInfo(new String[] {"￥26.00", "上传",this.getDate(0),this.getDate(0),"已发放"},tablefirst);
   		 flagOne = true;
   		 cashrecord.log("标识为：" + flagOne);
   		 break;}
	    }
	    cashrecord.log("标识为：" + flagOne);
	    if(!flagOne){
	    	fail("我的红包26元返现红包不存在");
	    }
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	       	 
   		    //我的福利
        	this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_redPacket']")).isDisplayed();
			     }
			 });
        	//红包
        	this.driver.findElement(By.xpath("//a[@id='menu_redPacket']")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
  			     @Override
  			     public Boolean apply(WebDriver webDriver) {
  			         return driver.findElement(By.xpath("//li[@id='red']")).isDisplayed();
  			     }
  			 });	
            
	    }else{
	    	 this.leftNavigationSubMenu(6,2,"红包");
	    }
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//tbody/tr[2]/td[1]")).isDisplayed();
            }
        });
	    Thread.sleep(2000);
	    //验证我的红包返现25元
	    boolean flagTwo = false;
	    for(int k2 = 2; k2 <= updatecount.size(); k2++){
		    if(this.driver.findElement(By.xpath("//tbody/tr["+k2+"]/td[1]")).getText().trim().contains("￥25.00")){
	   		 List<WebElement> tablefirst = this.driver.findElements(By.xpath("//tbody/tr["+k2+"]/td"));
	   		 bagpage.verifyTableInfo(new String[] {"￥25.00", "上传",this.getDate(0),this.getDate(0),"已发放"},tablefirst);
	   		 flagTwo = true;
	   		 break;}
		    
		    }
	    if(!flagTwo){
	    	fail("我的红包25元返现红包不存在");
	    }
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	       	 
   		    //我的福利
        	this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_redPacket']")).isDisplayed();
			     }
			 });
        	//红包
        	this.driver.findElement(By.xpath("//a[@id='menu_redPacket']")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
  			     @Override
  			     public Boolean apply(WebDriver webDriver) {
  			         return driver.findElement(By.xpath("//li[@id='red']")).isDisplayed();
  			     }
  			 });	
            
	    }else{
	    	 this.leftNavigationSubMenu(6,2,"红包");
	    }
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//tbody/tr[2]/td[1]")).isDisplayed();
            }
        });
	    Thread.sleep(2000);
	    //统计25元的个数
	    int count1 = 0;
	    //统计26元的个数
	    int count2 = 0;
	    for(int k2 = 2; k2 <= updatecount.size(); k2++){
		    if(this.driver.findElement(By.xpath("//tbody/tr["+k2+"]/td[1]")).getText().trim().contains("￥25.00")){
	   		 	count1++;
		    	}
		    
		    }
	    if(count1 == 2){
	    	cashrecord.log("我的红包25元返现红包个数正确");
	    }else{
	    	fail("我的红包25元返现红包个数不正确");
	    }

	    for(int k3 = 2; k3 <= updatecount.size(); k3++){
		    if(this.driver.findElement(By.xpath("//tbody/tr["+k3+"]/td[1]")).getText().trim().contains("￥26.00")){
	   		 	count2++;
		    	}
		    
		    }
	    if(count2 == 1){
	    	cashrecord.log("我的红包26元返现红包个数正确");
	    }else{
	    	fail("我的红包26元返现红包个数不正确");
	    }
	}

}
