package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import com.hfax.selenium.base.BaseTest;
import com.hfax.selenium.regression.account.AccountBaseTest;
import SecondStage.Hfax.Page.HomePage;


/**
 *  
 * @Description 固定浮动收益在网站首页的展示
 * Created by songxq on 8/11/16.
 * 1.浏览器中输入网站的地址，打开网站首页
 * 
 */
public class HomePage_001 extends BaseTest{

	
	private HomePage homepage;
	@Override
	@Before
	public void setup() throws Exception {
		super.setup();
		homepage = new HomePage(driver,wait);
	}
    @Test
	public void test() throws Exception{
		this.loadPage();
		homepage.verifyGftext();
	}

}
