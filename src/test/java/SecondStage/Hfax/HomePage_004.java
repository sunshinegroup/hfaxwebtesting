package SecondStage.Hfax;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.GFFinanceListPage;

/**
 *  
 * @Description 网站首页，点击“项目名称”的跳转
 * Created by songxq on 9/11/16.
 * 1.浏览器中输入网站的地址，打开网站首页
 * 2.用户未登录
 * 3.固定浮动收益列表中，点击产品的“项目名称”
 * 
 */
public class HomePage_004 extends BaseTest{
	
	private GFFinanceListPage gflistpage;
    @Override
	@Before
	public void setup()throws Exception {
		super.setup();
		gflistpage = new GFFinanceListPage(driver,wait);
	}

	@Test
	public void test() throws Exception {
		this.loadPage();
		this.navigateToPage("定期理财");
		gflistpage.verifyProNameskippage();
	}

}
