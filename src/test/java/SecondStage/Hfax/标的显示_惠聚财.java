package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.javascript.host.Window;
import com.hfax.selenium.regression.account.AccountBaseTest;
/**
 *  
 * @Description 标的显示_惠聚财
 * Created by songxq on 09/22/16.
 * 
 */
public class 标的显示_惠聚财 extends AccountBaseTest{

    //waitTime：等待时间 
	//proName：标的名称
	//proStatus：标的状态 
	//proBar：标的进度条
	Map<String, String> envVars = System.getenv();
	protected void showProject(int waitTime,String proName,String proStatus,String proBar) throws InterruptedException {
		String strPath = null;
		String strPathElement = null;
		//预热标的等待时间为5分钟
//		Thread.sleep(300000);
		Thread.sleep(waitTime);
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
			 
//			 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//li/a[@href='toNewHandFinanceList.do']")));
//		     this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li/a[@href='toNewHandFinanceList.do']"))).click();
			 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//li[@id='index2']//a[@href='toFinanceList.do']")));
		     this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@id='index2']//a[@href='toFinanceList.do']"))).click();
		     Thread.sleep(3000);
			
		 }
		 else{
			 
			 this.navigateToPage("定期理财"); 
		 }
				
		 //点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
		WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
		        
		new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")));
		//等待惠聚财第一个产品
		assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")).isDisplayed());
		
		for(int i = 0; i < 100; i++){
			Set<String> set = new LinkedHashSet<String>();
			set.add(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a");
			set.add(".//*[@id='display_15']/div/div[3]/div[1]/div[1]/a");
			set.add(".//*[@id='display_15']/div/div[4]/div[1]/div[1]/a");
			set.add(".//*[@id='display_15']/div/div[5]/div[1]/div[1]/a");
			set.add(".//*[@id='display_15']/div/div[6]/div[1]/div[1]/a");
			set.add(".//*[@id='display_15']/div/div[7]/div[1]/div[1]/a");
			set.add(".//*[@id='display_15']/div/div[8]/div[1]/div[1]/a");
			set.add(".//*[@id='display_15']/div/div[9]/div[1]/div[1]/a");
			set.add(".//*[@id='display_15']/div/div[10]/div[1]/div[1]/a");
			set.add(".//*[@id='display_15']/div/div[11]/div[1]/div[1]/a");
			for(Object element: set){
			     if(this.getElementText(element.toString()).equals(proName)){
			    	 strPath = element.toString();
			         break;
			     }
			  }
			if(strPath != null && this.getElementText(strPath).equals(proName)){
				break;
			}else{
			((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("下一页")));
			 this.driver.findElement(By.linkText("下一页")).click();
			 new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a")));
			 assertTrue("惠聚财翻页后第一个产品没有显示",this.getElementText(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a")!=null);
			}
		 }
		
		strPathElement = strPath.substring(0,strPath.length() - 16);
		System.out.println("截取后的为：" + strPathElement);
		
		 //验证预期年化利率
        System.out.println("【惠聚财投资页】预期年化利率为: " + this.getElementText(strPathElement + "/div[1]/div[2]/ul/li[1]/p[2]"));
        assertTrue("【惠聚财投资页】预期年化利率不正确",this.getElementText(strPathElement + "/div[1]/div[2]/ul/li[1]/p[2]").equals("700.00%"));
        
        //验证投资期限
        System.out.println("【惠聚财投资页】投资期限为：" + this.getElementText(strPathElement + "/div[1]/div[2]/ul/li[2]/p[2]"));
        assertTrue("【惠聚财投资页】投资期限不正确:",this.getElementText(strPathElement + "/div[1]/div[2]/ul/li[2]/p[2]").equals("333 天"));
        
        
        //验证起投金额
        System.out.println("【惠聚财投资页】起投金额为: "+ this.getElementText(strPathElement + "/div[1]/div[2]/ul/li[3]/p[2]/i"));
        assertTrue("【惠聚财投资页】起投金额不正确",this.getElementText(strPathElement + "/div[1]/div[2]/ul/li[3]/p[2]/i").equals("1百元"));
        
        //验证还款方式
        System.out.println("【惠聚财投资页】还款方式为:"+ this.getElementText(strPathElement + "/div[1]/div[2]/ul/li[4]/p[2]/span"));
        assertTrue("【惠聚财投资页】还款方式不正确",this.getElementText(strPathElement + "/div[1]/div[2]/ul/li[4]/p[2]/span").equals("一次性还款"));
        
        //验证项目总金额
        System.out.println("【惠聚财投资页】项目总金额为:"+  this.getElementText(strPathElement + "/div[2]/div[1]/p[2]/i"));
        assertTrue("【惠聚财投资页】项目总金额不正确",this.getElementText(strPathElement + "/div[2]/div[1]/p[2]/i").equals("1万元"));
        
        //验证投资进度条
        System.out.println("【惠聚财投资页】投资进度条为:"+ this.getElementText(strPathElement + "/div[2]/div[1]/div/span"));
        assertTrue("【惠聚财投资页】投资进度条不正确",this.getElementText(strPathElement + "/div[2]/div[1]/div/span").equals(proBar));
        
        //验证标的状态
        System.out.println("标的状态为："+this.getElementText(strPathElement + "/div[2]/div[2]/a"));
        assertTrue("【惠聚财投资页】标的状态不正确",this.getElementText(strPathElement + "/div[2]/div[2]/a").equals(proStatus));
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	 //得到当前窗口的句柄 
        	String currentWindow = driver.getWindowHandle();
        	this.driver.findElement(By.xpath(strPath)).click();
        	//得到所有窗口的句柄
            Set<String> handles = driver.getWindowHandles();
            //不包括当前窗口
           handles.remove(currentWindow);
            //判断是否存在窗口
            System.out.println("当前窗口个数为：" + handles.size());
            if (handles.size() > 0) {
                  try{
                //定位窗口
                driver.switchTo().window(handles.iterator().next());
              }catch(Exception e){
                         System.out.println("没有第二个窗口");
                       }
              }
       
		 }else{
			 	String currentWindow = driver.getWindowHandle();//获取当前窗口句柄
		        this.driver.findElement(By.xpath(strPath)).click();
		        Thread.sleep(5000);
		        Set<String> handles = driver.getWindowHandles();//获取所有窗口句柄
				 Iterator<String> it = handles.iterator();
				 System.out.println("当前窗口的个数为：" + handles.size());
				 while (it.hasNext()) {
				 if (currentWindow == it.next()) {
				 continue;
				 }
				 WebDriver window = driver.switchTo().window(it.next());//切换到新窗口
				 System.out.println("切换到新窗口");
				 }
			 
		 }
        
		 System.out.println("开始等待页面项目金额元素......");
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
			 Thread.sleep(3000);
			 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath("//ul[@class='borrowBox borrow-prod']/li[@class='obj-object'][1]/span")).isDisplayed();
		            }
		        });
			 System.out.println("IE结束等待页面项目金额元素......");
		 }
		 else{
		 //等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("//ul[@class='borrowBox borrow-prod']//li[1]//strong[@class='object-money']")).isDisplayed();
	            }
	        });
		 System.out.println("firefox,chrome,结束等待页面项目金额元素......");
		 }
//		 //验证标的状态
//		 System.out.println("【惠聚财投资详细页】标的状态为:" + this.getElementText("//i[@class='cursor']"));
//		 assertTrue("【惠聚财投资详细页】标的状态不正确",this.getElementText("//i[@class='cursor']").equals("敬请期待"));
		 
		 //验证项目金额
	      List<WebElement> listSec = this.driver.findElements(By.xpath("//strong[@class='object-money']"));
	      System.out.println("【惠聚财投资详细页】项目金额为:" + listSec.get(0).getText().trim());
	      assertTrue("【惠聚财投资详细页】项目金额不正确: ",listSec.get(0).getText().trim().equals("10,000.00"));
	       
	      //验证预期年化收益率
	     System.out.println("【惠聚财投资详细页】预期年化收益率为:" + this.driver.findElement(By.xpath("//li[@class='obj-shouyi interest']/i/strong")).getText().trim());
	     assertTrue("【惠聚财投资详细页】预期年化收益率不正确",this.driver.findElement(By.xpath("//li[@class='obj-shouyi interest']/i/strong")).getText().trim().equals("700.00%"));
	        
	      //验证投资期限
	     System.out.println("【惠聚财投资详细页】投资期限为:" + this.driver.findElement(By.xpath("//li[@class='obj-money']/i/strong")).getText().trim());
	     assertTrue("【惠聚财投资详细页】投资期限不正确",this.driver.findElement(By.xpath("//li[@class='obj-money']/i/strong")).getText().trim().equals("333"));
	        
	     //验证起投金额
	      System.out.println("【惠聚财投资详细页】起投金额为:" + listSec.get(1).getText().trim());
	     assertTrue("【惠聚财投资详细页】起投金额 不正确",listSec.get(1).getText().trim().equals("100.00"));
	        
	      //验证收益方式
	      System.out.println("【惠聚财投资详细页】收益方式为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[2]/em")).getText().trim());
	      assertTrue("【惠聚财投资详细页】收益方式不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[2]/em")).getText().trim().equals("一次性还本付息"));
	        
	     //验证产品风险等级
	      System.out.println("【惠聚财投资详细页】产品风险等级为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[3]/em")).getText().trim());
	      assertTrue("【惠聚财投资详细页】产品风险等级不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[3]/em")).getText().trim().equals("低风险"));
	        
	     //验证投资人承受条件
	      System.out.println("【惠聚财投资详细页】投资人承受条件为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[4]/em")).getText().trim());
	      assertTrue("【惠聚财投资详细页】投资人承受条件不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[4]/em")).getText().trim().equals("保守型"));
	       
	     //验证递增金额
	    System.out.println("【惠聚财投资详细页】递增金额为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[7]/em")).getText().trim());
	    assertTrue("【惠聚财投资详细页】递增金额不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[7]/em")).getText().trim().equals("100元"));	
	    //窗口最大化
	    this.driver.manage().window().maximize();
	    System.out.println("窗口已经最大化");
	    Thread.sleep(3000);
	    boolean ele =IsElementPresent(By.id("one2"));
	    System.out.println("ele的数值为"+ ele);
	    
	    //滚到制定的元素处
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
			 
	    	  WebElement target = driver.findElement(By.id("one1"));
	  	    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", target);
			
		 }else{
	   
	    	  WebElement target1 = driver.findElement(By.id("one2"));
		  	  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", target1);
		  	  System.out.println("已经滚动到网上开户协议");
	    }
	  	    
        //验证项目概况
        assertTrue("【惠聚财投资详细页】项目概况不存在",this.driver.findElement(By.id("one1")).getText().trim().equals("项目概况"));
        this.driver.findElement(By.id("one1")).click();
        System.out.println("已经点击项目概况");
        
        //验证网上开户协议
        assertTrue("【惠聚财投资详细页】网上开户协议不存在",this.driver.findElement(By.id("one2")).getText().trim().equals("网上开户协议"));
        this.driver.findElement(By.id("one2")).click();
        
        //验证产品说明书
        assertTrue("【惠聚财投资详细页】产品说明书不存在",this.driver.findElement(By.id("one3")).getText().trim().equals("产品说明书"));
        this.driver.findElement(By.id("one3")).click();
        
         //验证风险揭示书
        assertTrue("【惠聚财投资详细页】风险揭示书不存在",this.driver.findElement(By.id("one4")).getText().trim().equals("风险揭示书"));
        this.driver.findElement(By.id("one4")).click();
        
        //验证产品认购协议
        assertTrue("【惠聚财投资详细页】产品认购协议不存在",this.driver.findElement(By.id("one5")).getText().trim().equals("产品认购协议"));
        this.driver.findElement(By.id("one5")).click();
	
	}
	
	//investamount:投资金额
	//actualAmount:实际投资金额
	//ps:交易密码
	protected void investPro(String investamount,String interestAmount,String ps){
		
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			 WebElement element = this.driver.findElement(By.xpath(".//*[@id='index6']"));
			 this.driver.manage().window().maximize();
			 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element); 
		 }
		 else{
			this.driver.manage().window().maximize();
		 //验证投资金额下面的网上开户协议
        assertTrue("【惠聚财投资详细页】投资金额下的网上开户协议不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc2']")).getText().trim().equals("《网上开户协议》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc2']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
        
        //验证投资金额下面的产品说明书
        assertTrue("【惠聚财投资详细页】投资金额下的产品说明书不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc3']")).getText().trim().equals("《产品说明书》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc3']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
        
        //验证投资金额下面的风险揭示书
        assertTrue("【惠聚财投资详细页】投资金额下的风险揭示书不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc4']")).getText().trim().equals("《风险揭示书》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc4']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
        
        //验证投资金额下面的产品认购协议
        assertTrue("【惠聚财投资详细页】投资金额下的产品认购协议不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc1']")).getText().trim().equals("《产品认购协议》"));
        this.driver.findElement(By.xpath("//a[@class='btn_ccc1']")).click();
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
            }
        });
        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
		}
        
        //输入投资金额
        this.driver.findElement(By.xpath(".//*[@id='amount']")).clear();
        this.driver.findElement(By.xpath(".//*[@id='amount']")).sendKeys(investamount);
       
        
        //验证投资金额下的预期收益
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='interests']")));
        assertTrue("【惠聚财投资详细页】投资金额下预期收益不正确",this.driver.findElement(By.xpath(".//*[@id='interests']")).getText().trim().equals(interestAmount));
        
        
        //点击已同意
        this.driver.findElement(By.xpath(".//*[@id='agre']")).click();
        
        //点击去投资
        this.driver.findElement(By.linkText("去投资")).click();
        
        //等待实际投资本金(元)
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText();
                return text.trim().contains(".00");
            }
        });
        //验证实际投资本金(元)
        System.out.println("【惠聚财确认页】实际投资本金(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim());
        assertTrue("【惠聚财确认页】实际投资本金(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim().equals(investamount));
        
        //验证总预期收益率(%)
        System.out.println("【惠聚财确认页】总预期收益率(%)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim() + "%");
        assertTrue("【惠聚财确认页】总预期收益率(%)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim().equals("700.00"));
        
        //验证总预期收益(元)
        System.out.println("【惠聚财确认页】总预期收益(元)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim());
        assertTrue("【惠聚财确认页】总预期收益(元)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim().equals(interestAmount));
        
        //验证实际支付金额(元)
        System.out.println("【惠聚财确认页】实际支付金额(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim());
        assertTrue("【惠聚财确认页】实际支付金额(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim().equals(investamount));
        
        
        //输入交易密码
        this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).clear();
        this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).sendKeys(ps);
        
        
        //点击确定投资
        this.driver.findElement(By.xpath(".//*[@id='besureInvest']")).click();
        
        
        //等待确认框
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text = driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[1]")).getText();
                return text.trim().equalsIgnoreCase("浏览更多项目");
            }
        });
        
        //验证确认框本次投资金额
        System.out.println("【惠聚财确认框】本次投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim());
        assertTrue("【惠聚财确认框】本次投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim().equals(investamount));
        
        //验证本项目累计投资金额
        System.out.println("【惠聚财确认框】本项目累计投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim());
        assertTrue("【惠聚财确认框】本项目累计投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim().equals(investamount));
        
        //验证查看申请是否存在
        assertTrue("【惠聚财确认框】查看申请不存在:",this.driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[2]")).isDisplayed());
        //点击查看申请
        this.driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[2]")).click();
	}
	//查找产品
	public boolean searchPro(String proName){
		String strPathSearch = null;
		this.navigateToPage("定期理财");
		
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
//		WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
		        
		new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div")));
		String firstProText = this.driver.findElement(By.xpath(".//*[@id='display_15']/div")).getText();
		System.out.println("第一个产品的产品信息为：" + firstProText);
         if(firstProText.trim().contains("暂无信息")){
        	 System.out.println("第一个产品的产品信息为：" + firstProText);
      	   return false;
         }
		
		//等待惠聚财第一个产品
		assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")).isDisplayed());
	    
		//输出产品个数
		List<WebElement> list = this.driver.findElements(By.xpath("//div[@class='listBox-Info clearfix']"));
		int proCount = list.size();
		System.out.println("惠聚财产品个数为：" + list.size());
		for(int i = 0; i < 100; i++){
			Set<String> set = new LinkedHashSet<String>();
			for(int j = 2; j < list.size()+2; j++){
			set.add(".//*[@id='display_15']/div/div["+j+"]/div[1]/div[1]/a");
			}
			for(Object element: set){
			     if(this.getElementText(element.toString()).equals(proName)){
			    	 strPathSearch = element.toString();
			         break;
			     }
			  }
			if(strPathSearch != null && this.getElementText(strPathSearch).equals(proName)){
				break;
			}else{
			  if(proCount == 10){
			  ((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			  this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("下一页")));
			  this.driver.findElement(By.linkText("下一页")).click();
			  new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a")));
			  assertTrue("惠聚财翻页后第一个产品没有显示",this.getElementText(".//*[@id='display_15']/div/div[2]/div[1]/div[1]/a")!=null);
				}
			   else{
				   return false;
			   }
			}
		 }
		return true;
	}

}
