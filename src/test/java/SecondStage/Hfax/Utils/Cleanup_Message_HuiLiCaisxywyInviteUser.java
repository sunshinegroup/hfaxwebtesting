package SecondStage.Hfax.Utils;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.hfax.selenium.base.BaseTest;
/**
 *  
 * @Description 惠理财寿险业务员邀请的用户站内信清库
 * Created by songxq on 12/15/16.
 * 
 */
public class Cleanup_Message_HuiLiCaisxywyInviteUser extends BaseTest{
	
	Cleanup_Message cm = new Cleanup_Message();
	@Test
	public void test() throws ClassNotFoundException, SQLException {
		cm.cleanupMessage(this.dbUrl,this.dbUser,this.dbPassword,this.hlcsxywyInviteUsername);
		System.out.println("惠理财寿险业务员邀请的用户站内信清库");
	}

}
