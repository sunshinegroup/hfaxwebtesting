package SecondStage.Hfax.Utils;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.hfax.selenium.base.BaseTest;
/**
 *  
 * @Description 惠理财寿险【续期】业务员站内信清库
 * Created by songxq on 12/19/16.
 * 
 */
public class Cleanup_Message_HuiLiCaisxXQywy extends BaseTest{
	
	Cleanup_Message cm = new Cleanup_Message();
	@Test
	public void test() throws ClassNotFoundException, SQLException {
		cm.cleanupMessage(this.dbUrl,this.dbUser,this.dbPassword,this.hlcsxXQywyUsername);
		System.out.println("惠理财【寿险续期业务员】站内信清库");
	}

}
