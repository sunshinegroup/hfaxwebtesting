package SecondStage.Hfax.Utils;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.hfax.selenium.base.BaseTest;

/**
 *  
 * @Description 惠理财投资的用户站内信清库
 * Created by songxq on 01/12/17.
 * 
 */
public class Cleanup_Message_HuiLiCaiInvestUser extends BaseTest{
	
	Cleanup_Message cm = new Cleanup_Message();
	@Test
	public void test() throws ClassNotFoundException, SQLException {
		cm.cleanupMessage(this.dbUrl,this.dbUser,this.dbPassword,this.huilicaiinvestUsername);
		System.out.println("惠理财投资的用户站内信清库");
	}

}
