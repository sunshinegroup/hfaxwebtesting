package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.GFFinanceListPage;
import SecondStage.Hfax.Page.HuiUserRegister;
import SecondStage.Hfax.Page.MyInsideLetterPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;
import SecondStage.Hfax.Page.UserInfoPage;
/**
 *  
 * @Description 惠理财寿险业务员注册后的验证
 * 验证我的投资券，我的红包，充值提现，站内信,用户设置
 * 切记：需要执行一条站内信的sql
 * Created by songxq on 21/11/16.
 * 
 * 
 */
public class TestHlcsxywyAfterRegister extends BaseTest{

	private MyInvestTicketPage ticket; //我的投资券
	private MyRedBagPage bagpage; //我的红包
	private MyQueryCashRecordListPage cashrecord; //充值提现
	private MyInsideLetterPage myletter;//站内信
	private UserInfoPage userinfo;//用户设置
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        ticket = new MyInvestTicketPage(driver,wait);
        bagpage = new MyRedBagPage(driver,wait);
        cashrecord = new MyQueryCashRecordListPage(driver,wait);
        myletter = new MyInsideLetterPage(driver,wait);
        userinfo = new UserInfoPage(driver,wait);
    }
	
	@Test
	public void test_AfterRegisterInfo() throws Exception {
		Map<String, String> envVars = System.getenv();
		this.loadPage();
        this.HLClogin(this.hlcsxywyUsername,this.hlcsxywyPassword,this.hlcsxywyPhone);
        this.loginTopNavigationMenu(5,"我的账户");
        
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	       	 //我的福利
				this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
				this.wait.until(new ExpectedCondition<Boolean>() {
				     @Override
				     public Boolean apply(WebDriver webDriver) {
				         return driver.findElement(By.xpath("//a[@id='menu_touziquan']")).isDisplayed();
				     }
				 });
				//投资券
				this.driver.findElement(By.xpath("//a[@id='menu_touziquan']")).click();
				this.wait.until(new ExpectedCondition<Boolean>() {
				    @Override
				    public Boolean apply(WebDriver webDriver) {
				        return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
				    }
				});
	       }else{
	    	   this.leftNavigationSubMenu(6,1,"投资券");
	       }
		ticket.verifyTicketInfo("7","7","0","0");
		//验证投资券金额和加息券金额（14元，13元，12元，11元，1.2%，21元）
		ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","14",0);
		ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","13",1);
		ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","12",2);
		ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","11",3);
		ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","1.2",4);
		ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","21",5);
		//点击下一页
	    ticket.clickButton("//div[@class='page']//div[@class='page_list']//a[3]");
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            }
        });
	    //2.2%加息券面值
	    ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","2.2",0);

	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
	             } });
	    	//点击未使用的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='usable']")).click();
	    }else{

	    	this.rightHfaxTabNavigationMenu(1,"未使用的投资券");
	    }
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            } });
	    
		//可投资于
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 1);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 2);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 3);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 4);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 5);
	    //有效期
	    //获取当前日期
	    String getCurrentDate = this.getDate(0);
	    //把当前日期加1天
	    String getDateOne = this.getDate(1);
	    //把当前日期加4天
	    String getDateFour = this.getDate(4);
	    //把当前日期加44天
	    String getDateFortyFour = this.getDate(44);	
	    //有效期：14元投资代金券，13元投资代金券
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 1);
	    //有效期：12元投资代金券
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 2);
	    //有效期：11元投资代金券，1.2%投资加息券，当前日期，当前日期+4
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 3);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 4);
	    //有效期：21元投资券,当前日期+1，当前日期+44
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getDateOne+"~"+getDateFortyFour, 5);
	    //点击下一页
	    ticket.clickButton("//div[@class='page']//div[@class='page_list']//a[3]");
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            }
        });
	    //有效期：2.2%投资加息券，当前日期+1，当前日期+44
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getDateOne+"~"+getDateFortyFour, 0);

	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
	             } });
	    	//点击未使用的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='usable']")).click();
	    }else{
	    	this.rightHfaxTabNavigationMenu(1,"未使用的投资券");
	    }
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            }
        });
	    while(true){
	    	if(IsElementPresent(By.xpath("//div[@class='hfax-use-top clearfix']"))){
	    		break;
	    	}
	    }
	    
	    //获得来源：14元,13元，12元，11元，1.2%，21元
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：用户注册",0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：用户注册",1);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：用户注册",2);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：用户注册",3);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：用户注册",4);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：完成身份认证",5);
	    //点击下一页
	    ticket.clickButton("//div[@class='page']//div[@class='page_list']//a[3]");
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            }
        });
	    //获得来源：2.2%
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：完成身份认证",0);
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
	             } });
	    	//点击未使用的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='usable']")).click();
	    }else{
	    	this.rightHfaxTabNavigationMenu(1,"未使用的投资券");
	    }
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            }
        });
	    
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	
	    	 //使用条件,14元，13元,12元，11元，1.2%，21元
		    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",0);
		    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",1);
		    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",2);
		    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",3);
		    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",4);
		    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",5);
	    	
	    }else{
	    //使用条件,14元，13元,12元，11元，1.2%，21元
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",1);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",2);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",3);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",4);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",5);
	    }
	    
	    //使用限制,14元，13元,12元，11元，1.2%，21元
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用注册代金14元；5-10K,30-60d",0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用注册代金13元；5-10K,30-60d",1);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用注册代金12元；5-10K,30-60d",2);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用注册代金11元；5-10K,30-60d",3);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用注册加息1.2%；5-10K,30-60d",4);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用认证代金21元；5-10K,30-60d",5);
	   
	    //点击下一页
	    ticket.clickButton("//div[@class='page']//div[@class='page_list']//a[3]");
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            }
        });
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",0);	
	    }else{
	    //使用条件：2。2%
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天",0);
	    }
	    //使用限制，2.2%
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用认证加息2.2%；5-10K,30-60d",0);
	    
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
	             } });
	    	//点击未使用的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='usable']")).click();
	    }else{
	    	this.rightHfaxTabNavigationMenu(1,"未使用的投资券");
	    }
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            }
        });
	    
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='used']")).isDisplayed();
	             } });
	    	//点击已使用的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='used']")).click();
	    }else{
	    	this.rightHfaxTabNavigationMenu(2,"已使用的投资券");
	    }
	    this.verifyTextInfo("//tr[@class='data_none']/td/span","当前暂无符合条件的数据");
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='unUsable']")).isDisplayed();
	             } });
	    	//点击失效的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='unUsable']")).click();
	    }else{
	    	this.rightHfaxTabNavigationMenu(3,"失效的投资券");
	    }
	    this.verifyTextInfo("//tr[@class='data_none']/td/span","当前暂无符合条件的数据");

	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	       	 
   		    //我的福利
        	this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_redPacket']")).isDisplayed();
			     }
			 });
        	//红包
        	this.driver.findElement(By.xpath("//a[@id='menu_redPacket']")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
  			     @Override
  			     public Boolean apply(WebDriver webDriver) {
  			         return driver.findElement(By.xpath("//li[@id='red']")).isDisplayed();
  			     }
  			 });	
            
	    }else{
	    	this.leftNavigationSubMenu(6,2,"红包");
	    }
	    List<WebElement> table = this.driver.findElements(By.xpath("//tr[@class='bg_color']/th"));
	    bagpage.verifyTableInfo(new String[] {"红包金额", "获取来源", "获得时间","发放时间","状态"},table);
	    List<WebElement> tablefirst = this.driver.findElements(By.xpath("//tbody//tr[2]/td"));
	    bagpage.verifyTableInfo(new String[] {"￥23.00", "完成身份认证",getCurrentDate,getCurrentDate,"已发放"},tablefirst);

	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='payAndDraw']/a")).isDisplayed();
	             } });
	    	//点击充值提现
	    	this.driver.findElement(By.xpath("//li[@id='payAndDraw']/a")).click();
	    	
	    }else{
	    	this.leftNavigationMenu(3,"充值/提现");
	    }
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th")).isDisplayed();
            }
        });
	    List<WebElement> fundrecord = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th"));
	    cashrecord.verifyTableInfo(new String[] {"序号", "时间", "操作类型","备注","收入","支出","可用余额"},fundrecord,7);
	    List<WebElement> fundrecordfirst = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td"));
	    cashrecord.verifyTableInfo(new String[] {"1",getCurrentDate, "红包福利","投资发放红包","23.00","0.00","23.00"},fundrecordfirst,7);
	    
	    
	    //验证站内信的条数，去掉注册加息券为1.2%的站内信息
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
    		//等待页面加载
    		 this.wait.until(new ExpectedCondition<Boolean>() {
    	            @Override
    	            public Boolean apply(WebDriver webDriver) {
    	                return driver.findElement(By.xpath("//li[@id='accountManage_menuItem']")).isDisplayed();
    	            }
    	        });
    		//点击账户管理
    		 this.driver.findElement(By.xpath("//li[@id='accountManage_menuItem']")).click();
    		//等待页面加载
    		 this.wait.until(new ExpectedCondition<Boolean>() {
    	            @Override
    	            public Boolean apply(WebDriver webDriver) {
    	                return driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).isDisplayed();
    	            }
    	        });
    		 //站内信
    		 this.driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).click();
	    }else{
	    	
	    	this.leftNavigationSubMenu(7,2,"站内信");
	    }
	    while(true){
	    	if(IsElementPresent(By.xpath(".//*[@id='biaoge']/table/tbody/tr"))){
	    		break;
	    	}
	    }
	    myletter.waitPage(".//*[@id='biaoge']/table/tbody/tr",5);
	    List<WebElement> messages = this.driver.findElements(By.xpath(".//*[@id='biaoge']/table/tbody/tr"));
	    System.out.println("站内信的条数："+ messages.size());
	    if(messages.size() != 5){
	    	fail("惠理财业务员注册后站内信条数不正确！");
	    }
	    
	    for(int i = 2; i < 6; i++){
	    	if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    		//等待页面加载
	    		 this.wait.until(new ExpectedCondition<Boolean>() {
	    	            @Override
	    	            public Boolean apply(WebDriver webDriver) {
	    	                return driver.findElement(By.xpath("//li[@id='accountManage_menuItem']")).isDisplayed();
	    	            }
	    	        });
	    		//点击账户管理
	    		 this.driver.findElement(By.xpath("//li[@id='accountManage_menuItem']")).click();
	    		//等待页面加载
	    		 this.wait.until(new ExpectedCondition<Boolean>() {
	    	            @Override
	    	            public Boolean apply(WebDriver webDriver) {
	    	                return driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).isDisplayed();
	    	            }
	    	        });
	    		 //站内信
	    		 this.driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).click();
		    }else{
		    	
		    	this.leftNavigationSubMenu(7,2,"站内信");
		    }
	    //验证站内信中21元代金券,验证站内信中2.20%加息券,验证站内信中红包福利23元,验证站内信中红包福利1.20%加息券,验证站内信中50元代金券
	    myletter.waitPage(".//*[@id='biaoge']/table/tbody/tr["+i+"]/td[3]/a",5);
	    myletter.clickTitle(".//*[@id='biaoge']/table/tbody/tr["+i+"]/td[3]/a");
	    myletter.waitPage("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]",5);
	    if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("21元代金券")){
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","提醒：有您的投资券");
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.hlcsxywyName+"女士,恭喜您获得21元代金券,"+this.getDate(1)+"至"+this.getDate(44)+"内有效,具体使用规则可登录惠金所官网或APP个人账户查看，欢迎及时使用，如需帮助可致电400-015-8800(9:00-21:00)。");	
	    	
	    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("2.20%加息券")){
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","提醒：有您的投资券");
	 	    myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.hlcsxywyName+"女士,恭喜您获得2.20%加息券,"+this.getDate(1)+"至"+this.getDate(44)+"内有效,具体使用规则可登录惠金所官网或APP个人账户查看，欢迎及时使用，如需帮助可致电400-015-8800(9:00-21:00)。");	 	    																			       																			 

	    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("23元现金红包")){
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","红包福利");
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.hlcsxywyName+"女士,恭喜您获得23元现金红包，可立即用于投资或发起提现，欢迎登录惠金所平台查看使用。");
	    	                                                                                                                                                                                                                                                                                                                                
	    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("50元新手专享投资红包")){
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","提醒：有您的投资券");
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","恭喜您注册成功，50元新手专享投资红包已到账，投资时可直接抵用本金！即刻登录惠金所官网或下载惠金所APP开启理财新旅程！如需帮助欢迎致电客服专线400-015-8800(9:00-21:00)。");
	    	                                                                                    
	    }else{
	    	fail("用户注册后验证站内信中21元代金券,2.20%加息券,红包福利23元,50元代金券显示不正确");
	    }    		
	    }	

	    //验证用户设置中的用户姓名和身份证号码
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	//等待页面加载
   		 	this.wait.until(new ExpectedCondition<Boolean>() {
   	            @Override
   	            public Boolean apply(WebDriver webDriver) {
   	                return driver.findElement(By.xpath("//li[@id='accountManage_menuItem']")).isDisplayed();
   	            }
   	        });
   		 	//点击账户管理
   		 	this.driver.findElement(By.xpath("//li[@id='accountManage_menuItem']")).click();
   		 	//等待页面加载
   		 	this.wait.until(new ExpectedCondition<Boolean>() {
   	            @Override
   	            public Boolean apply(WebDriver webDriver) {
   	                return driver.findElement(By.xpath("//dl[@id='userInstall']/a")).isDisplayed();
   	            }
   	        });
   		 	//点击用户设置
   		 	this.driver.findElement(By.xpath("//dl[@id='userInstall']/a")).click();
   		
	    }else{
	    	this.leftNavigationSubMenu(7,1,"用户设置");
	    }
	    String name = this.hlcsxywyName.substring(0,1);
	    System.out.println("姓名为："+ name);
	    String identifyStart = this.hlcsxywyIdentification.substring(0, 4);
	    String identifyEnd = this.hlcsxywyIdentification.substring(14, 18);
	    userinfo.verifyInfo("//div[@class='hfax-user-info']//p[1]","真实姓名 "+name+"**");
	    userinfo.verifyInfo("//div[@class='hfax-user-info']//p[2]","身份证号 "+identifyStart+"**********"+identifyEnd);
	   
	}

}
