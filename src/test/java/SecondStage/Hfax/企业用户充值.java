package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.SuddenDeathBaseTest;

public class 企业用户充值  extends SuddenDeathBaseTest{

	/**
	 *  
	 * @Description 企业用户充值
	 * Created by songxq on 09/14/16.
	 * 
	 */
	//企业用户充值金额
	protected String businessAmount(){
		return this.businessAmount;
	}
	//企业用户交易密码
	protected String businessPassword(){
		return this.businessPassword;
	}
	//企业用户银行卡号
	protected String businessBankCard(){
		return this.businessBankCard;
	}
	 @Override
	    public void setup() throws Exception {
	        super.setup();
	        this.loadPage();
	        this.businessLogin(); //企业用户登录
	    }
	@Test
	public void test001_企业用户充值() {
		Map<String, String> envVars = System.getenv();
		 //点击我的账户
		this.navigateToPage("我的账户");
      
        //等待页面加载
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='index5']/a")).isDisplayed();
            }
        });
        //IE菜单选择
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			 this.selectMenuIE(2, "充值提现");
		 }
        else{
        	this.selectMenu(2, "充值提现");
        }
		 //点击充值
		this.selectSubMenu(2,"充值");
		
		//等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
		        @Override
		        public Boolean apply(WebDriver webDriver) {
		           return webDriver.findElement(By.xpath(".//*[@id='money1']")).isDisplayed();
		          }
		 });
		
		//输入充值金额
		this.driver.findElement(By.xpath(".//*[@id='money1']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='money1']")).sendKeys(businessAmount());
		
		//输入交易密码
		this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).sendKeys(businessPassword());
		
		//点击充值
		this.driver.findElement(By.xpath(".//*[@id='addrecharge']")).click();
		
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.id("moneyjine")).isDisplayed();
	            }
	        });

	        this.driver.findElement(By.xpath("//div[@id='cztxZhongTxtDiv2']/b/a")).click();

	        try {
	            this.driver.switchTo().alert().accept();
	        } catch (Exception e) {}

	        this.wait.until(ExpectedConditions.numberOfWindowsToBe(2));

	        WebDriver cpPayDriver = this.switchToWindow("通联支付");
	        FluentWait<WebDriver> cpPayWait = new WebDriverWait(cpPayDriver, 10000).withTimeout(10, TimeUnit.SECONDS).pollingEvery(100, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);

	        cpPayWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table/tbody/tr[2]/td[2]")));

	        String merchantName = cpPayDriver.findElement(By.xpath("//table/tbody/tr[2]/td[2]")).getText();
	        assertTrue("商户名字不正确", merchantName.contains("通联支付"));

	        String amount = cpPayDriver.findElement(By.xpath("//table/tbody/tr[5]/td[2]")).getText();
	        assertTrue("金额不正确", amount.contains(businessAmount()));

	        cpPayDriver.findElement(By.name("cardNo")).sendKeys(businessBankCard());
	        cpPayDriver.findElement(By.name("bpwd")).sendKeys(businessPassword());
	        cpPayDriver.findElement(By.xpath("//table//a[1]")).click();

	        new WebDriverWait(this.driver,8).until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@class='main']/span")).getText().equalsIgnoreCase("充值成功");
	            }
	        });

	        cpPayDriver.close();

	        this.driver = this.switchToWindow("惠金所");

	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.id("cztxTxt")).isDisplayed();
	            }
	        });

	        this.driver.findElement(By.xpath("//div[@id='cztxTxt']//div[@class='popBox']//a[1]")).click();
	        
	        //等待资金记录
	        this.wait.until(new ExpectedCondition<Boolean>() {
	          @Override
	          public Boolean apply(WebDriver webDriver) {
	              return webDriver.findElement(By.xpath("//div[@class='tabtil']//ul/li[1]")).isDisplayed();
	          }
	      });
	      
	        this.selectSubMenu(1,"资金记录");
	        System.out.println("充值金额的显示为:" + this.driver.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td")).getText().trim());
	        String Rechargeamount = this.driver.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td")).getText().trim();
	        assertTrue("充值不成功", Rechargeamount.length() > 0);
		
	}

}
