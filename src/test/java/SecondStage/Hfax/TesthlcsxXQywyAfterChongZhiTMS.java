package SecondStage.Hfax;

import static org.junit.Assert.*;

import org.junit.Test;
/**
 *  
 * @Description 惠理财寿险【续期】业务员充值后在惠金所综合管理系统中打寿险 【续期】业务员的标签
 * Created by songxq on 12/19/16.
 * 
 * 
 */	
public class TesthlcsxXQywyAfterChongZhiTMS extends TestHlcsxywyAfterChongZhiTMS{

	 @Override
	   protected String searchUserIdentification(){
			return this.hlcsxXQywyIdentification;
		}
	   
	    @Override
		protected String inputUserRole(){
			return "寿险续期业务员";
		}


}
