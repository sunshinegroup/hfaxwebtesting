package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.DBquery;
import SecondStage.Hfax.Page.HuiLiCaiFinanceDetailPage;
import SecondStage.Hfax.Page.HomePage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceListPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;

/**
 *  
 * @Description 首页
 * 手动发布-惠理财-定向委托-溢价-担保-立即发布-自动截标
 * Created by songxq on 15/11/16.
 * 
 */
public class TestHuiSouDongHomePage001 extends BaseTest{
	
	private HomePage hp;
	private HuiLiCaiFinanceListPage hlc;
	private HuiLiCaiFinanceDetailPage fd;
	DBquery db = new DBquery();
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        hp = new HomePage(driver,wait);
        hlc = new HuiLiCaiFinanceListPage(driver,wait);
        fd = new HuiLiCaiFinanceDetailPage(driver,wait);
    }

	@Test
	public void test_HomePage() throws ClassNotFoundException, SQLException, ParseException {
		String strPath = null;
		this.loadPage();
		//验证首页惠理财产品名称
		
		 String proName= db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_name from sunif.sif_project_info where subject_name like '惠理财-惠手动定向溢价%' and intr_year='28'","subject_name");
		 System.out.println("惠理财标的名称为："+ proName);
		 if(proName.equals(""))
		 {
			 fail("手动发布惠理财专区产品没有展示");
		 }
		 for(int i = 0; i < 100; i++){
				Set<String> set = new LinkedHashSet<String>();
				set.add("html/body/div[7]/div[3]/div[1]/div[1]/div[2]/div[1]/div/a");
				set.add("html/body/div[7]/div[3]/div[1]/div[1]/div[3]/div[1]/div/a");
				set.add("html/body/div[7]/div[3]/div[1]/div[1]/div[4]/div[1]/div/a");
				for(Object element: set){
				     if(this.getElementText(element.toString()).equals(proName)){
				    	 strPath = element.toString();
				    	 System.out.println(strPath.substring(0,strPath.length() - 13));
				         break;
				     }
				  }
				if(strPath != null && this.getElementText(strPath).equals(proName)){
					System.out.println(strPath.substring(0,strPath.length() - 13));
					break;
				}else{
					fail("首页手动惠理财产品不存在！");
				}
			 }
		 String strPathElement = strPath.substring(0,strPath.length() - 13);
		 System.out.println("截取后的为：" + strPathElement);	 		 
		
		//验证首页惠理财项目特点
		hp.verifyProDetails(0, "项目特点1",strPathElement+"/div[1]/div/span[1]");
		hp.verifyProDetails(0, "项目特点2",strPathElement+"/div[1]/div/span[2]");
		hp.verifyProDetails(0, "项目特点3",strPathElement+"/div[1]/div/span[3]");
		
		//验证首页惠理财基础利率+促销利率
		hp.verifyProDetails(0,"28.00% +0.50%",strPathElement+"/div[1]/ul/li[1]/p[1]");

		//标的起息日value_date，标的到期日mute_date
		String value_date = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select value_date from sunif.sif_project_info where subject_name="+"'"+proName+"'","value_date");
		System.out.println("value_date：=" + value_date);
		String mute_date = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select mute_date from sunif.sif_project_info where subject_name="+"'"+proName+"'","mute_date");
		System.out.println("value_date：=" + mute_date);
		int intervalDay = this.calcIntervalDay(mute_date.substring(0,10),value_date.substring(0, 10));
		System.out.println("投资期限为：" + intervalDay);
		//验证首页惠理财投资期限
		hp.verifyProDetails(0,intervalDay+"天",strPathElement+"/div[1]/ul/li[2]/p[1]");
		//验证首页惠理财起投金额
		hp.verifyProDetails(0,"1千元",strPathElement+"/div[1]/ul/li[3]/p[1]");
		//验证首页惠理财进度条
		hp.verifyProDetails(0,"0%",strPathElement+"/div[2]/div/span");
		//验证首页惠理财标的状态
		hp.verifyProDetails(0,"立即投资",strPathElement+"/div[2]/p/a");
		

		 
	}

}
