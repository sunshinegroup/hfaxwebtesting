package SecondStage.Hfax;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.GFFinanceListPage;
import SecondStage.Hfax.Page.HomePage;
/**
 *  
 * @Description 网站首页，点击“更多”的跳转
 * Created by songxq on 9/11/16.
 * 1.进入网站首页
 * 2.已登录用户点击“更多”
 * 
 */
public class HomePage_007 extends BaseTest{
	
	private HomePage homepage;
	private GFFinanceListPage gflistpage;
	@Override
	@Before
	public void setup() throws Exception {
		super.setup();
		homepage = new HomePage(driver,wait);
		gflistpage = new GFFinanceListPage(driver,wait);
	}

	@Test
	public void test() {
		this.loadPage();
//		this.GFlogin();
		homepage.clickGDButton();
		gflistpage.verifyGFFinancelistpage();
	}

}
