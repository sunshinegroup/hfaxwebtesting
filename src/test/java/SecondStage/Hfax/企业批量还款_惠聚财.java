package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.business.BusinessBaseTest;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 企业批量还款_惠聚财 extends BusinessBaseTest{
	/**
	 *  
	 * @Description 企业批量还款_惠聚财
	 * Created by songxq on 21/9/16.
	 * 
	 */
	Map<String, String> envVars = System.getenv();
	
	private WebElement clickRowCol(int rowNumber, int columnNumber) {
		try{
			WebElement rc = this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]"));
			return rc;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
		
    }
	 @Override
	    public void setup() throws Exception {
	        super.setup();
	        //点击我的账户
	        this.driver.findElement(By.xpath(".//*[@id='index5']/a")).click();
	      
	        //等待页面加载
	        wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath(".//*[@id='index5']/a")).isDisplayed();
	            }
	        });
	        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
			 {
				 this.selectLoanMenuIE(7, "还款管理");
			 }else{
				 this.selectMenu(7, "还款管理");
			 }
	       
	  	 
	    }
	@Test
	public void test001_正在还款中的借款_批量还款() {
		//点击正在还款的借款
		 this.selectSubMenu(2, "正在还款的借款");
		 
		//页面加载等待
		 this.wait.until(new ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//td[@align='center']"));
	            }
	        });
		 
		 //点击批量还款
		 this.driver.findElement(By.xpath(".//*[@id='searchForm']/a[2]")).click();
		 
		 //等待页面加载
	      wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath(".//*[@id='SelectAll']")).isDisplayed();
	            }
	        });
	      
	     List<WebElement> list = this.driver.findElements(By.xpath(".//*[@id='detailtable']/tbody/tr[2]/td")); 
	      //验证第一个标的还款日期
	     System.out.println("第一个标的的还款日期为:" + list.get(2).getText().trim());
	     assertTrue("第一个标的还款日期不正确",list.get(2).getText().trim().equals("20171201"));
	      
	      //验证第一个标的借款金额
	     System.out.println("第一个标的的借款金额为:" + list.get(3).getText().trim());
	     assertTrue("第一个标的借款金额不正确",list.get(3).getText().trim().equals("10000.00元"));
	      
	      
	      //验证第一个标的还款金额
	     System.out.println("第一个标的的还款金额为:" + list.get(7).getText().trim());
	     assertTrue("第一个标的还款金额不正确",list.get(7).getText().trim().equals("￥74750.00"));
	      
	      //验证第一个标的待还金额
	     System.out.println("第一个标的的待还金额为:" + list.get(8).getText().trim());
	     assertTrue("第一个标的待还金额不正确",list.get(8).getText().trim().equals("￥74750.00"));
	     
	     List<WebElement> list1 = this.driver.findElements(By.xpath(".//*[@id='detailtable']/tbody/tr[3]/td")); 
	     
	     //验证第二个标的还款日期
	     System.out.println("第二个标的的还款日期为:" + list1.get(2).getText().trim());
	     assertTrue("第二个标的还款日期不正确",list1.get(2).getText().trim().equals("20171201"));
	      
	      //验证第二个标的借款金额
	     System.out.println("第二个标的的借款金额为:" + list1.get(3).getText().trim());
	     assertTrue("第二个标的借款金额不正确",list1.get(3).getText().trim().equals("10000.00元"));
	      
	      
	      //验证第二个标的还款金额
	     System.out.println("第二个标的的还款金额为:" + list1.get(7).getText().trim());
	     assertTrue("第二个标的还款金额不正确",list1.get(7).getText().trim().equals("￥74750.00"));
	      
	      //验证第二个标的待还金额
	     System.out.println("第二个标的的待还金额为:" + list1.get(8).getText().trim());
	     assertTrue("第二个标的待还金额不正确",list1.get(8).getText().trim().equals("￥74750.00"));
	   
	     List<WebElement> listRadio = this.driver.findElements(By.xpath("//*[@id='subcheck']"));
	     //勾选连个标的
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
	    	 listRadio.get(0).click();
	    	 listRadio.get(1).click();
////	    	 Actions action = new Actions(driver);
////	    	 action.keyDown(Keys.F5);
////	    	 action.keyUp(Keys.F5);
//	    	 String strCheck = this.driver.findElement(By.xpath(".//*[@id='SelectAll']")).getAttribute("checked");
//	    	 System.out.println("属性值为：" + strCheck);
//	    	 if(strCheck == null ||strCheck.equals("")){
////	    	 this.driver.findElement(By.xpath(".//*[@id='SelectAll']")).click();
//	    	 this.driver.findElement(By.id("SelectAll")).click();
	    	
	    	 while(! this.driver.findElement(By.id("pId")).getAttribute("value").trim().equals("2")){
	    		 this.driver.findElement(By.id("SelectAll")).click();
	    		 if(this.driver.findElement(By.id("pId")).getAttribute("value").trim().equals("2")){
	    			 break;
	    		 }
	    	 }
	    	 
		 }else{
			 this.driver.findElement(By.xpath(".//*[@id='SelectAll']")).click();
		 }
	     //偿还项目数
	     System.out.println("偿还项目数为：" +this.driver.findElement(By.xpath(".//*[@id='pId']")).getAttribute("value"));
	     assertTrue("偿还的项目数不正确",this.driver.findElement(By.xpath(".//*[@id='pId']")).getAttribute("value").trim().equals("2"));
	     
	     //偿还总金额为
	     System.out.println("偿还总金额为：" + this.driver.findElement(By.xpath(".//*[@id='sumAmt']")).getAttribute("value"));
	     assertTrue("偿还的总金额不正确",this.driver.findElement(By.xpath(".//*[@id='sumAmt']")).getAttribute("value").equals("149500.00"));
	     
	     //账户可用余额为
	     System.out.println("账户可用余额为：" + this.driver.findElement(By.xpath(".//*[@id='userBle']")).getAttribute("value"));
	     assertTrue("账户可用余额不正确",this.driver.findElement(By.xpath(".//*[@id='userBle']")).getAttribute("value").equals("5575000.00"));
	     
	     //输入交易密码
	     this.driver.findElement(By.xpath(".//*[@id='pwd']")).clear();
	     this.driver.findElement(By.xpath(".//*[@id='pwd']")).sendKeys(this.businessPassword);
	     
	     //输入验证码
	     this.driver.findElement(By.xpath(".//*[@id='code']")).clear();
	     this.driver.findElement(By.xpath(".//*[@id='code']")).sendKeys("1234");
	     
	     //确认还款
	     this.driver.findElement(By.xpath(".//*[@id='btnsave']")).click();
	     
	     //等待确认框
	     new WebDriverWait(this.driver,10).until(new ExpectedCondition<Boolean>() {
	          @Override
	          public Boolean apply(WebDriver webDriver) {
	          	String text = driver.findElement(By.xpath(".//*[@id='know']")).getText();
	              return text.trim().equalsIgnoreCase("我知道了");
	          }
	      });
	     System.out.println("企业用户还款后的提示信息为：" + this.getElementText(".//*[@id='desc']"));
	     assertTrue("企业用户还款后提示信息不正确",this.getElementText(".//*[@id='desc']").equalsIgnoreCase("成功还款项目2笔,还款总金额为149500.00元"));
	     //点击我知道了
	     this.driver.findElement(By.xpath(".//*[@id='know']")).click();
	}

	@Test
	public void test002_已还完的借款(){
		this.selectSubMenu(3, "已还完的借款");
		List<WebElement> list = this.driver.findElements(By.xpath(".//*[@id='borrow_details']/table/tbody/tr[2]/td")); 
		
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='borrow_details']/table/tbody/tr[2]/td")).isDisplayed();
	            }
	        });
		 
		 //已还完的借款借款金额
		 System.out.println("【已还完的借款】借款金额为: "+list.get(2).getText().trim());
	     assertTrue("【已还完的借款】借款金额不正确",list.get(2).getText().trim().equals("10000.00元"));
	     
		 //已还完的借款年利率
	     System.out.println("【已还完的借款】借款年利率为: "+list.get(3).getText().trim());
	     assertTrue("【已还完的借款】借款年利率不正确",list.get(3).getText().trim().equals("700.00%"));
		 
		 //已还完的借款还款期限
	     System.out.println("【已还完的借款】还款期限为: "+list.get(4).getText().trim());
	     assertTrue("【已还完的借款】还款期限不正确",list.get(4).getText().trim().equals("333天"));
		 
		 
		 //已还完的应还本息
	     System.out.println("【已还完的借款】应还本息为: "+list.get(9).getText().trim());
	     assertTrue("【已还完的借款】应还本息不正确",list.get(9).getText().trim().equals("￥74750.00"));
		 
		 //已还完的已还本息
	     System.out.println("【已还完的借款】已还本息为: "+list.get(10).getText().trim());
	     assertTrue("【已还完的借款】已还本息不正确",list.get(10).getText().trim().equals("￥74750.00"));
	     
	     //点击还款明细
	     this.driver.findElement(By.xpath(".//*[@id='borrow_details']/table/tbody/tr[2]/td[13]")).click();
		 
	   //等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[1]/th[1]")).isDisplayed();
	            }
	        });
		 
		 //验证已还完的借款还款明细中的借款金额
		 System.out.println("【已还完的借款还款明细中】借款金额为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[2]/td[2]")).getText().trim());
	     assertTrue("已还完的借款还款明细中】借款金额不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[2]/td[2]")).getText().trim().equals("借款金额：￥10000.00"));
		 
		 //验证已还完的借款还款明细中的借款利率
	     System.out.println("【已还完的借款还款明细中】借款利率为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[1]")).getText().trim());
	     assertTrue("已还完的借款还款明细中】借款利率不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[1]")).getText().trim().equals("借款利率：700.00%"));
		 
		 //验证已还完的借款还款明细中的借款期限
	     System.out.println("【已还完的借款还款明细中】借款期限为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[2]")).getText().trim());
	     assertTrue("已还完的借款还款明细中】借款期限不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[2]")).getText().trim().equals("借款期限：333天"));
	     
	     //验证已还完的借款还款明细中的还款方式
	     System.out.println("【已还完的借款还款明细中】还款方式为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[1]")).getText().trim());
	     assertTrue("已还完的借款还款明细中】还款方式不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[1]")).getText().trim().equals("还款方式： 一次性还款"));
	     
	     //验证已还完的借款还款明细中的起息日期
	     System.out.println("【已还完的借款还款明细中】起息日期为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[2]")).getText().trim());
	     assertTrue("已还完的借款还款明细中】起息日期不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[2]")).getText().trim().equals("起息日期：20170102"));
	     
	     List<WebElement> list1 = this.driver.findElements(By.xpath(" .//*[@id='borrow_details']/div[2]/table/tbody/tr[2]/td"));
	    
	     //验证table中的计划还款日期
	     System.out.println("【已还完的借款】table中的计划还款日期为: "+list1.get(1).getText().trim());
	     assertTrue("【已还完的借款】table中的计划还款日期不正确",list1.get(1).getText().trim().equals("20171201"));
	     
	     //验证table中的计划还款本息
	     System.out.println("【已还完的借款】table中的计划还款本息为: "+list1.get(2).getText().trim());
	     assertTrue("【已还完的借款】table中的计划还款本息不正确",list1.get(2).getText().trim().equals("￥74750.00"));
	     
	     //验证table中的实还本息
	     System.out.println("【已还完的借款】table中的实还本息为: "+list1.get(5).getText().trim());
	     assertTrue("【已还完的借款】table中的实还本息不正确",list1.get(5).getText().trim().equals("￥74750.00"));
	     
	     //验证table中的总还款金额
	     System.out.println("【已还完的借款】table中的总还款金额为: "+list1.get(8).getText().trim());
	     assertTrue("【已还完的借款】table中的总还款金额不正确",list1.get(8).getText().trim().equals("￥74750.00"));
	     
	     //验证table中的还款状态
	     System.out.println("【已还完的借款】table中的还款状态为: "+list1.get(9).getText().trim());
	     assertTrue("【已还完的借款】table中的还款状态不正确",list1.get(9).getText().trim().equals("已偿还"));
	     
	     this.selectSubMenu(3, "已还完的借款");
		 List<WebElement> list2 = this.driver.findElements(By.xpath(".//*[@id='borrow_details']/table/tbody/tr[3]/td")); 
			
			//等待页面加载
			 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath(".//*[@id='borrow_details']/table/tbody/tr[3]/td")).isDisplayed();
		            }
		        });
			//已还完的借款借款金额
			 System.out.println("【已还完的借款】中第二个标的借款金额为: "+list2.get(2).getText().trim());
		     assertTrue("【已还完的借款】中第二个标的借款金额不正确",list2.get(2).getText().trim().equals("10000.00元"));
		     
			 //已还完的借款年利率
		     System.out.println("【已还完的借款】中第二个标的借款年利率为: "+list2.get(3).getText().trim());
		     assertTrue("【已还完的借款】中第二个标的借款年利率不正确",list2.get(3).getText().trim().equals("700.00%"));
			 
			 //已还完的借款还款期限
		     System.out.println("【已还完的借款】中第二个标的还款期限为: "+list2.get(4).getText().trim());
		     assertTrue("【已还完的借款】中第二个标的还款期限不正确",list2.get(4).getText().trim().equals("333天"));
			 
			 
			 //已还完的应还本息
		     System.out.println("【已还完的借款】中第二个标的应还本息为: "+list2.get(9).getText().trim());
		     assertTrue("【已还完的借款】中第二个标的应还本息不正确",list2.get(9).getText().trim().equals("￥74750.00"));
			 
			 //已还完的已还本息
		     System.out.println("【已还完的借款】中第二个标的已还本息为: "+list2.get(10).getText().trim());
		     assertTrue("【已还完的借款】中第二个标的已还本息不正确",list2.get(10).getText().trim().equals("￥74750.00"));
		     
		     //点击还款明细
		     this.driver.findElement(By.xpath(".//*[@id='borrow_details']/table/tbody/tr[3]/td[13]/a")).click();
			 
		   //等待页面加载
			 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[1]/th[1]")).isDisplayed();
		            }
		        });
			 
			 //验证已还完的借款还款明细中的借款金额
			 System.out.println("【已还完的借款还款明细中】中第二个标的借款金额为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[2]/td[2]")).getText().trim());
		     assertTrue("已还完的借款还款明细中】中第二个标的借款金额不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[2]/td[2]")).getText().trim().equals("借款金额：￥10000.00"));
			 
			 //验证已还完的借款还款明细中的借款利率
		     System.out.println("【已还完的借款还款明细中】中第二个标的借款利率为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[1]")).getText().trim());
		     assertTrue("已还完的借款还款明细中】中第二个标的借款利率不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[1]")).getText().trim().equals("借款利率：700.00%"));
			 
			 //验证已还完的借款还款明细中的借款期限
		     System.out.println("【已还完的借款还款明细中】中第二个标的借款期限为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[2]")).getText().trim());
		     assertTrue("已还完的借款还款明细中】中第二个标的借款期限不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[3]/td[2]")).getText().trim().equals("借款期限：333天"));
		     
		     //验证已还完的借款还款明细中的还款方式
		     System.out.println("【已还完的借款还款明细中】中第二个标的还款方式为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[1]")).getText().trim());
		     assertTrue("已还完的借款还款明细中】中第二个标的还款方式不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[1]")).getText().trim().equals("还款方式： 一次性还款"));
		     
		     //验证已还完的借款还款明细中的起息日期
		     System.out.println("【已还完的借款还款明细中】中第二个标的起息日期为: "+this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[2]")).getText().trim());
		     assertTrue("已还完的借款还款明细中】中第二个标的起息日期不正确",this.driver.findElement(By.xpath(".//*[@id='biaoge2_details']/table/tbody/tr[4]/td[2]")).getText().trim().equals("起息日期：20170102"));
		     
		     List<WebElement> list3 = this.driver.findElements(By.xpath(" .//*[@id='borrow_details']/div[2]/table/tbody/tr[2]/td"));
		    
		     //验证table中的计划还款日期
		     System.out.println("【已还完的借款】中第二个标的table中的计划还款日期为: "+list3.get(1).getText().trim());
		     assertTrue("【已还完的借款】中第二个标的table中的计划还款日期不正确",list3.get(1).getText().trim().equals("20171201"));
		     
		     //验证table中的计划还款本息
		     System.out.println("【已还完的借款】中第二个标的table中的计划还款本息为: "+list3.get(2).getText().trim());
		     assertTrue("【已还完的借款】table中的计划还款本息不正确",list3.get(2).getText().trim().equals("￥74750.00"));
		     
		     //验证table中的实还本息
		     System.out.println("【已还完的借款】中第二个标的table中的实还本息为: "+list3.get(5).getText().trim());
		     assertTrue("【已还完的借款】中第二个标的table中的实还本息不正确",list3.get(5).getText().trim().equals("￥74750.00"));
		     
		     //验证table中的总还款金额
		     System.out.println("【已还完的借款】中第二个标的table中的总还款金额为: "+list3.get(8).getText().trim());
		     assertTrue("【已还完的借款】中第二个标的table中的总还款金额不正确",list3.get(8).getText().trim().equals("￥74750.00"));
		     
		     //验证table中的还款状态
		     System.out.println("【已还完的借款】中第二个标的table中的还款状态为: "+list3.get(9).getText().trim());
		     assertTrue("【已还完的借款】中第二个标的table中的还款状态不正确",list3.get(9).getText().trim().equals("已偿还"));
	     
	}

}
