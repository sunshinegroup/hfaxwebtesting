package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.DBquery;
import SecondStage.Hfax.Page.MyInsideLetterPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;
/**
 *  
 * @Description 手动发放卡券，惠理财投资用户[绑定兑换加息券]
 * Created by songxq on 01/11/17.
 * 
 */
public class TestHuiLiCaiManualTicketsUserBindingTickets extends BaseTest{

	private MyRedBagPage bagpage; //我的红包
	private MyInvestTicketPage ticket;//我的投资券
	private MyInsideLetterPage myletter;//站内信
	private MyQueryCashRecordListPage cashrecord; //充值提现
	DBquery db = new DBquery();
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        bagpage = new MyRedBagPage(driver,wait);
        ticket = new MyInvestTicketPage(driver,wait);
        myletter = new MyInsideLetterPage(driver,wait);
        cashrecord = new MyQueryCashRecordListPage(driver,wait);
    }
	@Test
	public void testUserBindingTickets() throws ClassNotFoundException, SQLException{
		Map<String, String> envVars = System.getenv();
		this.loadPage();
        this.HLClogin(this.huilicaiinvestUsername,this.huilicaiinvestPassword,this.huilicaiinvestPhone);
        this.loginTopNavigationMenu(5,"我的账户");

        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
       	 //我的福利
			this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_touziquan']")).isDisplayed();
			     }
			 });
			//投资券
			this.driver.findElement(By.xpath("//a[@id='menu_touziquan']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			    @Override
			    public Boolean apply(WebDriver webDriver) {
			        return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
			    }
			});
       }else{
    	   this.leftNavigationSubMenu(6,1,"投资券");
       }
        
        //点击绑定投资券
        ticket.clickButton("//a[@id='bindtickets']");
        //等待页面加载(绑定投资券)
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='ticketBox ticketWrap']/h2[@class='clearfix']")).isDisplayed();
            }
        });
	    
	    //输入投资券兑换码
	    //查找绑定券的兑换码，符合条件，投资券的名称：自动化兑换加息 ，兑换码面值：1.90%,状态：未兑换
	    String ticketCode = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select exch_code from sunif.sif_ticket_manager a inner join sunif.sif_ticket_exchange_code b on a.ticket_cd = b.ticket_cd where a.ticket_name = '兑换加息-自动化测试' and a.ticket_rate = '1.9000' and b.status = '1'and rownum = 1","exch_code");	    System.out.println("兑换加息的代码为：" + ticketCode);
	    ticket.inputInfo("//input[@id='ticketsNo']",ticketCode);
	        
	    //输入验证码
	    ticket.inputInfo("//input[@id='checkNo']","1234");
	    
	    //点击确定
	    ticket.clickButton(".//*[@id='bind']/h3/b/a[1]");
	    
	    //弹出加息券绑定成功
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='jiaxi']/strong")).getText().trim().equals("加息券绑定成功");
            }
        });
	    //验证绑定加息券的收益率：1.90%
	    ticket.verifyInfo(".//*[@id='jvalue']/strong","1.90%");
	    
	    //可投资于：惠理财、惠投资、惠聚财
	    ticket.verifyInfo(".//*[@id='project']","惠理财 惠投资 惠聚财");
	    
	    //使用条件：无
	    ticket.verifyInfo(".//*[@id='condition']","无投资金额和产品期限要求");
	    
	    //当前日期，当前日期+4
        //获取当前日期
		 String getCurrentDate = this.getDate(0);
		 //把当前日期加4天
		 String getDateFour = this.getDate(4); 
	    ticket.verifyInfo(".//*[@id='validDate']",getCurrentDate+"~"+getDateFour);
	    
	    //获得来源：兑换
	    ticket.verifyInfo(".//*[@id='source']","兑换");
	    
	    //使用投资券
	    ticket.clickButton(".//*[@id='usebt']/b/a");
	    
	    //跳转到惠理财tab
        this.wait.until(new ExpectedCondition<Boolean>() {
          @Override
          public Boolean apply(WebDriver webDriver) {
              return webDriver.findElement(By.xpath(".//*[@id='tab_7']")).isDisplayed();
          }
          
        });
        while(true){
        	if(IsElementPresent(By.linkText("我的账户"))){
        		break;
        	}
        }
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	this.driver.findElement(By.xpath("//li[@id='_topMenu_account']/a")).click();
        }else{
        this.topNavigationMenu(5,"我的账户");
        }
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
       	 //我的福利
			this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_touziquan']")).isDisplayed();
			     }
			 });
			//投资券
			this.driver.findElement(By.xpath("//a[@id='menu_touziquan']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			    @Override
			    public Boolean apply(WebDriver webDriver) {
			        return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
			    }
			});
       }else{
    	   this.leftNavigationSubMenu(6,1,"投资券");
       }
        
        //点击下一页
	    ticket.clickButton("//div[@class='page_list']//a[4]");
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-left left']")).isDisplayed();
            }
        });
        //查找投资券
	    int j = 0;
	    boolean flag = false;
        for(int i = 1; i < 7; i++){
        	System.out.println("利率为：" + this.driver.findElement(By.xpath("//div[@class='hfax-use-left left']["+i+"]//div[@class='hfax-use-top clearfix']//p[@class='hfax-use-money right']/b")).getText());
        	if(this.driver.findElement(By.xpath("//div[@class='hfax-use-left left']["+i+"]//div[@class='hfax-use-top clearfix']//p[@class='hfax-use-money right']/b")).getText().trim().equals("1.9")){
        		flag = true;
        		j = i;
        		break;
        	}
        }
        if(flag = false){
        	fail("没有找到绑定的加息投资券1.9%");
        }
        //可投资于：惠理财、惠投资、惠聚财
        ticket.verifyInfo("//div[@class='hfax-use-left left']["+j+"]//div[@class='hfax-overtime-info']//p[1]","投资范围： 惠理财 惠投资 惠聚财");
        //有效期：当前日期，当前日期+4
        ticket.verifyInfo("//div[@class='hfax-use-left left']["+j+"]//div[@class='hfax-overtime-info']//p[2]","有效期："+getCurrentDate+"~"+getDateFour);
        //获得来源：兑换
        ticket.verifyInfo("//div[@class='hfax-use-left left']["+j+"]//div[@class='hfax-overtime-info']//p[3]","获得来源：兑换");
        
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	//使用条件：无
        	ticket.verifyInfo("//div[@class='hfax-use-left left']["+j+"]//div[@class='hfax-overtime-info']//p[5]","使用条件："+"无投资金额和产品期限要求");
        }else{
        	//使用条件：无
        	ticket.verifyInfo("//div[@class='hfax-use-left left']["+j+"]//div[@class='hfax-overtime-info']//p[5]","使用条件："+"\n"+"无投资金额和产品期限要求");
        }
	}

}
