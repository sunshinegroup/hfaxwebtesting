package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.HaxTotalManageSystemPage;
import SecondStage.Hfax.Page.MyInsideLetterPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;

public class TestHlcsxywyAfterChongZhiTMS extends BaseTest{

	/**
	 *  
	 * @Description 惠理财寿险业务员充值后在惠金所综合管理系统中打寿险业务员的标签
	 * Created by songxq on 22/11/16.
	 * 
	 * 
	 */		
	
	private HaxTotalManageSystemPage haxtms; //惠金所综合管理系统
	
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        haxtms = new HaxTotalManageSystemPage(driver,wait);
    }
	//查询用户身份证号
	protected String searchUserIdentification(){
		return this.hlcsxywyIdentification;
	}
	//输入用户角色
	protected String inputUserRole(){
		return "寿险业务员";
	}
	@Test
	public void test_AfterRechargeInfo() throws InterruptedException {
		
		//登录惠金所综合管理系统
		this.tmsLogin();
		//点击客服管理
		haxtms.waitPage(".//*[@id='menu']/li[13]",5);
		haxtms.moveToElement(".//*[@id='menu']/li[13]");
		//点击客户标签
		haxtms.clickButton(".//*[@id='menu']/li[13]/ul/li[5]/a");
		haxtms.waitPage(".//*[@id='add_prod_btn']", 5);
		
		//输入证件号码
		haxtms.inputInfo(".//*[@id='idtfno']",searchUserIdentification());
		//点击查询
		haxtms.clickButton(".//*[@id='submit']");
		
		//获取内容
		Thread.sleep(2000);
		String content1 = haxtms.getInfo(".//*[@id='datatable_prod']/tbody/tr/td[3]");
		System.out.println("电子账户为：" + content1);
		
		//点击新增
		Thread.sleep(2000);
		haxtms.clickButton(".//*[@id='add_prod_btn']");
		
		//等待页面加载
		Thread.sleep(2000);
		
		//输入数据
		haxtms.inputInfo(".//*[@id='custna']", content1.trim());
		Thread.sleep(2000);
		
		//点击标签号
		haxtms.clickButton(".//*[@id='s2id_tagscd']/a/span[2]/b");
		Thread.sleep(2000);
		haxtms.inputInfo(".//*[@id='s2id_autogen1_search']",inputUserRole());
		this.driver.findElement(By.xpath(".//*[@id='s2id_autogen1_search']")).sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		 
		//点击保存
		haxtms.clickButton(".//*[@id='m_save_debt']");
		Thread.sleep(2000);
		
		//点击OK
		haxtms.clickButton("//button[@class='btn btn-primary']");
		Thread.sleep(2000);		
		
	}

}
