package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 惠聚财流标站内信产品不展示账户余额 extends 标的显示_惠聚财{

	/**
	 *  
	 * @Description 惠聚财募集失败流标站内信 
	 * Created by songxq on 09/13/16.
	 * 
	 */

	protected String afterTnvestAmount; //投资后账户余额
	protected String afterAvailableAmount; //投资后可用余额
	protected String afterFreezeAmount; //投资后冻结的金额
	protected String afterAllAmount;//投资后账户总资产
	Map<String, String> envVars = System.getenv();
	
	@Test
	public void test001_惠聚财流标后的站内信验证() {
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			this.selectMenuIE(4, "站内信");
		  
		 }else{
		 
			 this.selectMenu(4, "站内信");
		 }
		//等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[2]/td[1]")).isDisplayed();
	            }
	       });
		 assertTrue("惠聚财流标站内信状态不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[2]/td[1]").equals("未读"));
		 assertTrue("惠聚财流标标题不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[2]/td[3]/a").equals("项目流标返款"));
		 
		 //点击投资申请成功
		 this.driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[2]/td[3]/a")).click();
		 
		 //等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong")).isDisplayed();
	            }
	       });
	
		 //验证发件人
		 assertTrue("站内信发件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong").equals("管理员"));
		 //验证收件人
		 assertTrue("站内信收件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[2]/td[2]").equals(this.username));
		 //验证标题
		 assertTrue("站内信标题不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[3]/td[2]").equals("项目流标返款"));
		 //获取系统当前日期
		 Date dt=new Date();
	     SimpleDateFormat matter1=new SimpleDateFormat("yyyy-MM-dd");
	     System.out.println("系统当前日期为：" + matter1.format(dt));
		 String date = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[4]/td[2]").substring(0,10);
		 //验证日期
		 assertTrue("站内信日期不正确",date.equals(matter1.format(dt)));
		 
		 //验证内容姓名
		 String username = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").substring(0,7);
		 System.out.println("用户姓名为:" + username);
		 assertTrue("站内信用户姓名不正确",username.equals("尊敬的晓东先生"));
		 //验证项目内容
		 String proContent = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").substring(8,this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").length());
		 System.out.println("项目内容为:" + proContent);
		 assertTrue("站内信项目内容不正确",proContent.equals("您投资的Yuetestproject001已流标，投资金额10000.00元、100.00元代金券、100.00%加息券已经返还到您的惠金所账户，可立即用于投资，预计下一个工作日12:00后可发起提现。"));
		
	}
	@Test
	public void test002_惠聚财流标产品不展示() throws InterruptedException{
		boolean blstatus;
		blstatus = searchPro("Yuetestproject001");
		if(blstatus == false){
			assertTrue("流标后的产品没有展示",blstatus == false);
		}else{
			fail("流标依然展示");
		}
		
	}
	@Test
	public void test003_验证投资后账户总资产和冻结金额(){
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p")).isDisplayed();
	            }
	        });
		
		//我的账户-->账户余额
		System.out.println("我的账户【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p"));
		afterTnvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p");
		assertTrue("我的账户【账户余额】不正确", afterTnvestAmount.equals("￥ 555,000.00"));
		 
		//我的账户-->账户总资产
		System.out.println("我的账户【账户总资产】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p"));
		afterAllAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p");
		assertTrue("我的账户【账户总资产】不正确", afterAllAmount.equals("￥ 555,000.00"));
		 
		//我的账户-->冻结金额
		System.out.println("我的账户【冻结金额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p"));
		afterFreezeAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p");
		assertTrue("我的账户【冻结金额】不正确", afterFreezeAmount.equals("￥ 0.00"));
		 
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			this.selectMenuIE(2, "充值提现");
		  
		 }else{
		 
		this.selectMenu(2, "充值提现");
		 }
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("//div[@class='tabtil']/ul/li")).isDisplayed();
	            }
	        });
		//投资前【账户余额】
		System.out.println("投资前【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]"));
		afterTnvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]");
		assertTrue("投资前【账户余额】不正确", afterTnvestAmount.equals("￥555,000.00"));
		
		//投资前【可用余额】
		System.out.println("投资前【可用余额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]"));
		afterAvailableAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]");
		assertTrue("投资前【可用余额】为", afterAvailableAmount.equals("￥555,000.00"));
		 
		//投资前【冻结金额】
		System.out.println("投资前【冻结金额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]"));
		afterFreezeAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]");
		assertTrue("投资前【冻结金额】为", afterFreezeAmount.equals("￥0.00"));
	   
	 }

	@Test
	public void test004_代金券和加息券返回(){
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			this.selectMenuIE(16, "我的投资券");
		  
		 }else{
		 
		this.selectMenu(16, "我的投资券");
		 }
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("//div[@class='tickets']/p")).isDisplayed();
	            }
	        });
		List<WebElement> list = this.driver.findElements(By.xpath("//div[@class='tickets']/p/strong"));
		System.out.println("【流标后】的" + list.get(0).getText().trim());
	    assertTrue("【流标后】的代金券没有正常显示",list.get(0).getText().trim().equals("投资代金券"));
	    System.out.println("【流标后】的" + list.get(1).getText().trim());
	    assertTrue("【流标后】的加息券没有正常显示",list.get(1).getText().trim().equals("投资加息券"));
		
	}
}
