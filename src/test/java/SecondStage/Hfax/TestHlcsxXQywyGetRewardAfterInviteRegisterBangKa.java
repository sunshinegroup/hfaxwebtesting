package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.MyInsideLetterPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;
import SecondStage.Hfax.Page.UserInfoPage;

public class TestHlcsxXQywyGetRewardAfterInviteRegisterBangKa extends BaseTest{
/**
 *  
 * @Description 惠理财寿险【续期】业务员【获得推荐好友完成实名认证奖励】
 * 验证我的投资券，我的红包，充值提现，站内信
 * Created by songxq on 12/20/16.
 * 
 * 
 */private MyInvestTicketPage ticket; //我的投资券
	private MyRedBagPage bagpage; //我的红包
	private MyQueryCashRecordListPage cashrecord; //充值提现
	private MyInsideLetterPage myletter;//站内信
	private UserInfoPage userinfo;//用户设置
	
	@Override
	@Before
	 public void setup() throws Exception {
     super.setup();
     ticket = new MyInvestTicketPage(driver,wait);
     bagpage = new MyRedBagPage(driver,wait);
     cashrecord = new MyQueryCashRecordListPage(driver,wait);
     myletter = new MyInsideLetterPage(driver,wait);
     userinfo = new UserInfoPage(driver,wait);
 }
	@Test
	public void test_InviteUserAfterRegisterInfo() throws Exception{
		Map<String, String> envVars = System.getenv();
		this.loadPage();
		this.HLClogin(this.hlcsxXQywyUsername, this.hlcsxXQywyPassword,this.hlcsxXQywyPhone);
		this.loginTopNavigationMenu(5,"我的账户");

		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
         	 //我的福利
  			this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
  			Thread.sleep(1000);
  			boolean ele = this.IsElementPresent(By.xpath("//a[@id='menu_touziquan']"));
  			System.out.println("元素： " + ele);
  			this.wait.until(new ExpectedCondition<Boolean>() {
  			     @Override
  			     public Boolean apply(WebDriver webDriver) {
  			         return driver.findElement(By.xpath("//a[@id='menu_touziquan']")).isDisplayed();
  			     }
  			 });
  			//投资券
  			this.driver.findElement(By.xpath("//a[@id='menu_touziquan']")).click();
  			this.wait.until(new ExpectedCondition<Boolean>() {
  			    @Override
  			    public Boolean apply(WebDriver webDriver) {
  			        return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
  			    }
  			});
         }else{
        	 this.leftNavigationSubMenu(6,1,"投资券");
         }
        //有效期
	    //获取当前日期
	    String getCurrentDate = this.getDate(0);
	    //把当前日期加4天
	    String getDateFour = this.getDate(4);
        //61代金券的面值|可投资于：惠理财 惠投资 惠聚财|有效期|获得来源：推荐好友完成实名认证|使用条件：邀请人被邀认证代金61元；5-10K,30-60d
	    ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","61", 0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：推荐好友完成实名认证", 0);
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 0);
	    }else{
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 0);
	    }
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用邀请人被邀认证代金61元；5-10K,30-60d", 0);
	    
	    //点击下一页
	    ticket.clickButton("//div[@class='page_list']//a[4]");
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            }
        });
	    //6.2%加息券面值|可投资于：惠理财 惠投资 惠聚财|有效期|获得来源：推荐好友完成实名认证|使用条件：邀请人被邀认证加息6.2%；5-10K,30-60d
	    ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","6.2", 1);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 1);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 1);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：推荐好友完成实名认证", 1);
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 1);
	    }else{
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 1);
	    }
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用邀请人被邀认证加息6.2%；5-10K,30-60d", 1);
	    
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
	             } });
	    	//点击未使用的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='usable']")).click();
	    }else{
	    	this.rightHfaxTabNavigationMenu(1,"未使用的投资券");
	    }
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            }
        });
	  
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='used']")).isDisplayed();
	             } });
	    	//点击已使用的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='used']")).click();
	    }else{
	    	this.rightHfaxTabNavigationMenu(2,"已使用的投资券");
	    }
	    this.verifyTextInfo("//tr[@class='data_none']/td/span","当前暂无符合条件的数据");
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='unUsable']")).isDisplayed();
	             } });
	    	//点击失效的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='unUsable']")).click();
	    }else{
	    	this.rightHfaxTabNavigationMenu(3,"失效的投资券");
	    }
	    this.verifyTextInfo("//tr[@class='data_none']/td/span","当前暂无符合条件的数据");
	    
	    //红包金额：63元,30元
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	       	 
   		    //我的福利
        	this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_redPacket']")).isDisplayed();
			     }
			 });
        	//红包
        	this.driver.findElement(By.xpath("//a[@id='menu_redPacket']")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
  			     @Override
  			     public Boolean apply(WebDriver webDriver) {
  			         return driver.findElement(By.xpath("//li[@id='red']")).isDisplayed();
  			     }
  			 });	
            
	    	}else{
	    		this.leftNavigationSubMenu(6,2,"红包");
	    	}
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//tr[@class='bg_color']/th")).isDisplayed();
            }
        });
	    List<WebElement> table = this.driver.findElements(By.xpath("//tr[@class='bg_color']/th"));
	    bagpage.verifyTableInfo(new String[] {"红包金额", "获取来源", "获得时间","发放时间","状态"},table);
	    List<WebElement> count = this.driver.findElements(By.xpath("//tbody//tr"));
	    System.out.println("我的红包条数为：" + String.valueOf(count.size()-1));
	    int flagOne = 0;
	    for(int j = 2; j <= count.size(); j++){
	    if(this.driver.findElement(By.xpath("//tbody/tr["+j+"]/td[2]")).getText().trim().contains("推荐好友完成实名认证")){
   		 List<WebElement> tablefirst = this.driver.findElements(By.xpath("//tbody/tr["+j+"]/td"));
   		 bagpage.verifyTableInfo(new String[] {"￥63.00", "推荐好友完成实名认证",this.getDate(0),this.getDate(0),"已发放"},tablefirst);
   		 break;}
	    	flagOne++;
	    }
	    if(flagOne == (count.size()-1)){
	    	fail("我的红包63元不存在");
	    }
	    int flagTwo = 0;
	    for(int k = 2; k <= count.size(); k++){
		    if(this.driver.findElement(By.xpath("//tbody/tr["+k+"]/td[2]")).getText().trim().contains("寿险续期人员推荐注册奖励")){
	   		 List<WebElement> tablefirst = this.driver.findElements(By.xpath("//tbody/tr["+k+"]/td"));
	   		 bagpage.verifyTableInfo(new String[] {"￥30.00", "寿险续期人员推荐注册奖励",this.getDate(0),this.getDate(0),"已发放"},tablefirst);
	   		 break;}
		    	flagTwo++;
		    }
	    if(flagTwo == (count.size()-1)){
	    	fail("我的红包30元不存在");
	    }
	    
	    //63元,30元
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='payAndDraw']/a")).isDisplayed();
	             } });
	    	//点击充值提现
	    	this.driver.findElement(By.xpath("//li[@id='payAndDraw']/a")).click();
	    	
	    }else{
	    	this.newSelectMenu(3,"充值/提现");
	    }
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th")).isDisplayed();
            }
        });
	    List<WebElement> fundrecord = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th"));
	    cashrecord.verifyTableInfo(new String[] {"序号", "时间", "操作类型","备注","收入","支出","可用余额"},fundrecord,7);
	    List<WebElement> fundrecordfirst = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td"));
	    cashrecord.verifyTableInfo(new String[] {"1",this.getDate(0), "红包福利","投资发放红包","30.00","0.00","30.00"},fundrecordfirst,6); 
	    List<WebElement> fundrecordsecond = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[3]/td"));
	    cashrecord.verifyTableInfo(new String[] {"2",this.getDate(0), "红包福利","投资发放红包","63.00","0.00","63.00"},fundrecordsecond,6); 
	    
	    
	    //站内信
	    String strPhoneStart = this.hlcsxXQywyInvitePhone.substring(0,3);
	    String strPhoneEnd = this.hlcsxXQywyInvitePhone.substring(7,11);
	    System.out.println("惠理财邀请的用户站内信需要的手机号为："+strPhoneStart+"****"+strPhoneEnd);
	    for(int i = 2; i < 6; i++){
	    	if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    		//等待页面加载
	    		 this.wait.until(new ExpectedCondition<Boolean>() {
	    	            @Override
	    	            public Boolean apply(WebDriver webDriver) {
	    	                return driver.findElement(By.xpath("//li[@id='accountManage_menuItem']/a")).isDisplayed();
	    	            }
	    	        });
	    		//点击账户管理
	    		 this.driver.findElement(By.xpath("//li[@id='accountManage_menuItem']/a")).click();
	    		//等待页面加载
	    		 this.wait.until(new ExpectedCondition<Boolean>() {
	    	            @Override
	    	            public Boolean apply(WebDriver webDriver) {
	    	                return driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).isDisplayed();
	    	            }
	    	        });
	    		 //站内信
	    		 this.driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).click();
		    }else{
		    	
		    	this.leftNavigationSubMenu(7,2,"站内信");
		    }
	    //验证站内信中6.2%加息券，61元代金券，63元现金红包,20元现金红包
	    myletter.waitPage(".//*[@id='biaoge']/table/tbody/tr["+i+"]/td[3]/a",5);
	    myletter.clickTitle(".//*[@id='biaoge']/table/tbody/tr["+i+"]/td[3]/a");
	    myletter.waitPage("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]",5);
	    if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("6.20%加息券")){
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","提醒：有您的投资券");
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.hlcsxXQywyName+"先生,恭喜您获得6.20%加息券,"+this.getDate(0)+"至"+this.getDate(4)+"内有效,具体使用规则可登录惠金所官网或APP个人账户查看，欢迎及时使用，如需帮助可致电400-015-8800(9:00-21:00)。");	 	    																			       																			 
	 	
	    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("61元代金券")){
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","提醒：有您的投资券");
	 	    myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.hlcsxXQywyName+"先生,恭喜您获得61元代金券,"+this.getDate(0)+"至"+this.getDate(4)+"内有效,具体使用规则可登录惠金所官网或APP个人账户查看，欢迎及时使用，如需帮助可致电400-015-8800(9:00-21:00)。");
	 	    
	    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("63元现金红包")){
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","红包福利");
	 	    myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.hlcsxXQywyName+"先生,恭喜您获得63元现金红包，可立即用于投资或发起提现，欢迎登录惠金所平台查看使用。");
	 	    
	    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("30元现金红包")){
	    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","红包发放");
	 	    myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.hlcsxXQywyName+"先生您好，您推荐的新用户(手机"+strPhoneStart+"****"+strPhoneEnd+")已成功注册并绑卡，30元现金红包，已到达您的惠金所账户，2个工作日后即可发起提现。");
	 	   
	    }
	    else{
	    	fail("惠理财寿险业务员站内信中6.2%加息券，61元代金券，63元,30元现金红包显示不正确");
	    }    		
	    }
	   
	    
	}

}
