package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 惠聚财差额申购  extends AccountBaseTest {

	/**
	 *  
	 * @Description 惠聚财差额申购
	 * Created by songxq on 09/13/16.
	 * 
	 */
	
	protected String preInvestAmount; //投资前账户余额
	protected String preAvailableAmount; //投资前可用余额
	protected String preFreezeAmount; //投资前冻结的金额
	protected String preAllAmount;//投资前账户总资产
	
	protected String afterTnvestAmount; //投资后账户余额
	protected String afterAvailableAmount; //投资后可用余额
	protected String afterFreezeAmount; //投资后冻结的金额
	protected String afterAllAmount;//投资后账户总资产
	Map<String, String> envVars = System.getenv();
	
	//投资100W

	@Test
	public void test001_惠聚财投资金额不够充值() {
		
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p")).isDisplayed();
	            }
	        });
		
		//我的账户-->账户余额
		System.out.println("我的账户【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p"));
		preInvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p");
		assertTrue("我的账户【账户余额】不正确", preInvestAmount.equals("￥ 555,000.00"));
		 
		//我的账户-->账户总资产
		System.out.println("我的账户【账户总资产】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p"));
		preAllAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p");
		assertTrue("我的账户【账户总资产】不正确", preAllAmount.equals("￥ 555,000.00"));
		 
		//我的账户-->冻结金额
		System.out.println("我的账户【冻结金额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p"));
		preFreezeAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p");
		assertTrue("我的账户【冻结金额】不正确", preFreezeAmount.equals("￥ 0.00"));
		
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			this.selectMenuIE(2, "充值提现");
		  
		 }else{
		 
		this.selectMenu(2, "充值提现");
		 }
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("//div[@class='tabtil']/ul/li")).isDisplayed();
	            }
	        });
		//投资前【账户余额】
		System.out.println("投资前【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]"));
		preInvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]");
		assertTrue("投资前【账户余额】不正确", preInvestAmount.equals("￥555,000.00"));
		
		//投资前【可用余额】
		System.out.println("投资前【可用余额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]"));
		preAvailableAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]");
		assertTrue("投资前【可用余额】为", preAvailableAmount.equals("￥555,000.00"));
		 
		//投资前【冻结金额】
		System.out.println("投资前【冻结金额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]"));
		preFreezeAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]");
		assertTrue("投资前【冻结金额】为", preFreezeAmount.equals("￥0.00"));
		
		this.navigateToPage("定期理财");
		
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
        WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
        
        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")));
        //等待惠聚财第一个产品
        assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")).isDisplayed());
        
        //立即投资
        firstItem.findElement(By.xpath("div[2]/div[2]/a")).click();
        
        //等待输入金额
       this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='amount']")));
       
       //输入投资金额
       this.driver.findElement(By.xpath(".//*[@id='amount']")).clear();
       this.driver.findElement(By.xpath(".//*[@id='amount']")).sendKeys("1000000");
     
 		//点击已同意
		this.driver.findElement(By.xpath(".//*[@id='agre']")).click();

		//点击去投资
		this.driver.findElement(By.linkText("去投资")).click();
		
		//等待实际投资本金(元)
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				String text = driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText();
				return text.trim().equalsIgnoreCase("1,000,000.00");
			}
		});
		System.out.println("充值金额的提示：" + this.getElementText(".//*[@id='lackmoney']"));
		assertTrue("充值信息提示不正确：",this.getElementText(".//*[@id='lackmoney']").equals("账户余额不足,请先充值!充值"));
		
		//点击充值
		this.driver.findElement(By.xpath(".//*[@id='lackmoney']/button")).click();
		
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                String text = webDriver.findElement(By.xpath("//div[@class='tabtil']/ul/li[2]")).getText();
	                return text.equalsIgnoreCase("充值");
	            }
	        });

	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.id("bankInput")).isDisplayed();
	            }
	        });
	        assertTrue("所属银行不正确", this.driver.findElement(By.id("bankInput")).getAttribute("value").contains("中国建设银行"));
	        
	        //输入充值金额
	        this.driver.findElement(By.id("money1")).clear();
	        this.driver.findElement(By.id("money1")).sendKeys("1000000");
	        
	        //输入交易密码
	        this.driver.findElement(By.id("dealpwd")).clear();
	        this.driver.findElement(By.id("dealpwd")).sendKeys(this.jiaoyipassword);
	        
	        //点击提交
	        this.driver.findElement(By.id("addrecharge")).click();
	 

	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.id("moneyjine")).isDisplayed();
	            }
	        });

	        this.driver.findElement(By.xpath("//div[@id='cztxZhongTxtDiv2']/b/a")).click();

	        try {
	            this.driver.switchTo().alert().accept();
	        } catch (Exception e) {}

	        this.wait.until(ExpectedConditions.numberOfWindowsToBe(2));

	        WebDriver cpPayDriver = this.switchToWindow("通联支付");
	        FluentWait<WebDriver> cpPayWait = new WebDriverWait(cpPayDriver, 10000).withTimeout(10, TimeUnit.SECONDS).pollingEvery(100, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);

	        cpPayWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table/tbody/tr[2]/td[2]")));

	        String merchantName = cpPayDriver.findElement(By.xpath("//table/tbody/tr[2]/td[2]")).getText();
	        assertTrue("商户名字不正确", merchantName.contains("通联支付"));

	        String amount = cpPayDriver.findElement(By.xpath("//table/tbody/tr[5]/td[2]")).getText();
	        assertTrue("金额不正确", amount.contains("1000000"));

	        cpPayDriver.findElement(By.name("cardNo")).sendKeys(this.card);
	        cpPayDriver.findElement(By.name("bpwd")).sendKeys(this.password);
	        cpPayDriver.findElement(By.xpath("//table//a[1]")).click();

	        cpPayWait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@class='main']/span")).getText().equalsIgnoreCase("充值成功");
	            }
	        });

	        cpPayDriver.close();

	        this.driver = this.switchToWindow("惠金所");

	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.id("cztxTxt")).isDisplayed();
	            }
	        });

	        this.driver.findElement(By.xpath("//div[@id='cztxTxt']//div[@class='popBox']//a[1]")).click();
	        
	        //等待资金记录
	        this.wait.until(new ExpectedCondition<Boolean>() {
	          @Override
	          public Boolean apply(WebDriver webDriver) {
	              return webDriver.findElement(By.xpath("//div[@class='tabtil']//ul/li[1]")).isDisplayed();
	          }
	      });
	      
	        this.selectSubMenu(1,"资金记录");
	        System.out.println("充值金额的显示为:" + this.driver.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td")).getText().trim());
	        String Rechargeamount = this.driver.findElement(By.xpath("//div[@class='biaoge']/table/tbody/tr[2]/td")).getText().trim();
	        assertTrue("充值不成功", Rechargeamount.length() > 0);
		
	}
	
	 @Test
	 public void test002_惠聚财再次投资(){
			this.navigateToPage("定期理财");
			//点击惠聚财
			WebElement content = this.clickOnSubmenu("惠聚财");
	        WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
	        
	        new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")));
	        //等待惠聚财第一个产品
	        assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")).isDisplayed());
	        
	        //立即投资
	        firstItem.findElement(By.xpath("div[2]/div[2]/a")).click();
	        
	        //等待输入金额
	       this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='amount']")));
	       
	       //输入投资金额
	       this.driver.findElement(By.xpath(".//*[@id='amount']")).clear();
	       this.driver.findElement(By.xpath(".//*[@id='amount']")).sendKeys("1000000");
	     
	 		//点击已同意
			this.driver.findElement(By.xpath(".//*[@id='agre']")).click();

			//点击去投资
			this.driver.findElement(By.linkText("去投资")).click();
			
			//等待实际投资本金(元)
			this.wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver webDriver) {
					String text = driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText();
					return text.trim().equalsIgnoreCase("1,000,000.00");
				}
			});
			  //验证实际投资本金(元)
	        System.out.println("【惠聚财确认页】实际投资本金(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim());
	        assertTrue("【惠聚财确认页】实际投资本金(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim().equals("1,000,000.00"));
	        
	        //验证总预期收益率(%)
	        System.out.println("【惠聚财确认页】总预期收益率(%)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim() + "%");
	        assertTrue("【惠聚财确认页】总预期收益率(%)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim().equals("700.00"));
	        
	        //验证总预期收益(元)
	        System.out.println("【惠聚财确认页】总预期收益(元)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim());
	        assertTrue("【惠聚财确认页】总预期收益(元)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim().equals("6,475,000.00"));
	        
	        //验证实际支付金额(元)
	        System.out.println("【惠聚财确认页】实际支付金额(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim());
	        assertTrue("【惠聚财确认页】实际支付金额(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim().equals("1,000,000.00"));
			
	        //输入交易密码
	        this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).clear();
	        this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).sendKeys(this.jiaoyipassword);
	        
	        //点击确定投资
	        this.driver.findElement(By.xpath(".//*[@id='besureInvest']")).click();
	        
	        //等待确认框
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	String text = driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[1]")).getText();
	                return text.trim().equalsIgnoreCase("浏览更多项目");
	            }
	        });
	        
	 }

	 @Test
	public void test003_搜索惠聚财招标中的产品() {
			//等待我的主页
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("我的主页")));
	        
	        //我的投资->惠聚财
	        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
			 {
				this.selectMenuIE(10, "我的投资");
			  
			 }else{
			 
				 this.selectMenu(10, "我的投资");
			 }
	        this.selectSubMenu(5, "惠聚财");
	        
	         //选择正在招标中的产品
	        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("borrowstatus")));
	        assertTrue("请选择下拉框不存在",this.driver.findElement(By.id("borrowstatus")).getText().contains("--请选择--"));
	        this.searchFinancePro("正在招标中");
	        
	        //点击搜索
	        assertTrue("搜索不存在",this.driver.findElement(By.id("search")).isDisplayed());
	        this.driver.findElement(By.id("search")).click();
	        
	        //验证惠聚财产品列表不为空
	        List<WebElement> list = this.driver.findElements(By.xpath("//tr//td[@align='center']//a[@target='_blank']")); 
	  	    assertTrue("不存在惠聚财的产品",list.size()>0);
	  	    
	  	    //验证搜索页的本金(元)
	  	    List<WebElement> list1 = this.driver.findElements(By.xpath("//td[@align='center']"));
	  	    System.out.println("【惠聚财搜索页】的本金(元)为:"+ list1.get(1).getText().trim());
	  	    assertTrue("惠聚财搜索页】的本金(元)不正确",list1.get(1).getText().trim().equals("1,000,000.00"));
	  	    
	  	    //验证搜索页的预计收益(元)
	  	    System.out.println("【惠聚财搜索页】的预计收益(元)为:"+ list1.get(2).getText().trim());
	  	    assertTrue("【惠聚财搜索页】的预计收益(元)不正确",list1.get(2).getText().trim().equals("6,475,000.00"));
	  	    
	  	    //验证搜索页的起息时间
	  	    System.out.println("【惠聚财搜索页】的起息时间为:"+ list1.get(4).getText().trim());
		    assertTrue("【惠聚财搜索页】的起息时间不正确",list1.get(4).getText().trim().equals("20170102"));
	  	    
	  	   //验证搜索页的到期时间
		    System.out.println("【惠聚财搜索页】的到期时间为:"+ list1.get(5).getText().trim());
		    assertTrue("【惠聚财搜索页】的到期时间不正确",list1.get(5).getText().trim().equals("20171201"));
	  	    
	  	    //验证搜索页的标的状态
		    System.out.println("【惠聚财搜索页】的标的状态为:"+ list1.get(6).getText().trim());
		    assertTrue("【惠聚财搜索页】的标的状态不正确",list1.get(6).getText().trim().equals("正在招标中"));
		    
	  	    
	  } 
	
	 @Test
	 public void test004_验证投资后账户余额可用余额产账户总资产和冻结金额(){
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p")).isDisplayed();
	            }
	        });
		
		//投资后我的账户-->账户余额
		System.out.println("我的账户投资后【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p"));
		afterTnvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p");
		assertTrue("我的账户投资后【账户余额】不正确", afterTnvestAmount.equals("￥ 1,555,000.00"));
		 
		//投资后我的账户-->账户总资产
		System.out.println("我的账户投资后【账户总资产】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p"));
		afterAllAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p");
		assertTrue("我的账户投资后【账户总资产】不正确", afterAllAmount.equals("￥ 8,030,000.00"));
		 
		//投资后我的账户-->冻结金额
		System.out.println("我的账户投资后【冻结金额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p"));
		afterFreezeAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p");
		assertTrue("我的账户投资后【冻结金额】不正确", afterFreezeAmount.equals("￥ 1,000,000.00"));
		 
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			this.selectMenuIE(2, "充值提现");
		  
		 }else{
		 
			 this.selectMenu(2, "充值提现");
		 }
			//等待页面加载
			 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath("//div[@class='tabtil']/ul/li")).isDisplayed();
		            }
		        });
			 
		//投资后【账户余额】
		System.out.println("投资后【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]"));
		afterTnvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]");
		assertTrue("投资后【账户余额】不正确", afterTnvestAmount.equals("￥1,555,000.00"));
			
		//投资后【可用余额】
		System.out.println("投资后【可用余额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]"));
		afterAvailableAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]");
		assertTrue("投资后【可用余额】为", afterAvailableAmount.equals("￥555,000.00"));
			 
		//投资后【冻结金额】
		System.out.println("投资后【冻结金额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]"));
		afterFreezeAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]");
		assertTrue("投资后【冻结金额】为", afterFreezeAmount.equals("￥1,000,000.00"));
		
	   
	 }

}
