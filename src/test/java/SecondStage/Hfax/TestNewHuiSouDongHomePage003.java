package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.DBquery;
import SecondStage.Hfax.Page.HomePage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceDetailPage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceListPage;

public class TestNewHuiSouDongHomePage003 extends BaseTest{
	/**
	 *  
	 * @Description 首页
	 * 手动发布-新手标-收益权转让-非溢价-无担保-预热标-手动审核
	 * Created by songxq on 16/11/16.
	 * 
	 */
	private HomePage hp;
	private HuiLiCaiFinanceListPage hlc;
	private HuiLiCaiFinanceDetailPage fd;
	DBquery db = new DBquery();
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        hp = new HomePage(driver,wait);
        hlc = new HuiLiCaiFinanceListPage(driver,wait);
        fd = new HuiLiCaiFinanceDetailPage(driver,wait);
    }
	@Test
	public void test_HomePage() throws ClassNotFoundException, SQLException, ParseException {
		this.loadPage();
		//验证首页新手标产品名称
		String proName = hp.getNewProName("//a[@class='info-title']");
		String dbproName = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_name from sunif.sif_project_info where subject_name="+"'"+proName+"'","subject_name");
		hp.verifyProName(proName, dbproName);
		
		//验证首页新手标的特点
		fd.verifyProDetail("项目特点1","//p[@class='news_tit']/span[@class='rates']");
		fd.verifyProDetail("新客体验","//p[@class='news_tit']/span[@class='fate']");
		//验证首页新手标的利率
		fd.verifyProDetail("11%","//p[@class='d-qxian d-qxian2']");

		//标的起息日value_date，标的到期日mute_date
		String value_date = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select value_date from sunif.sif_project_info where subject_name="+"'"+proName+"'","value_date");
		System.out.println("value_date：=" + value_date);
		String mute_date = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select mute_date from sunif.sif_project_info where subject_name="+"'"+proName+"'","mute_date");
		System.out.println("value_date：=" + mute_date);
		int intervalDay = this.calcIntervalDay(mute_date.substring(0,10),value_date.substring(0, 10));
		System.out.println("投资期限为：" + intervalDay);
		//验证首页新手标投资期限
		hp.verifyProDetails(0,intervalDay+"天","//div[@class='news_vip']//p[@class='d-qxian']");
		//验证首页新手标起投金额
		hp.verifyProDetails(1,"1千元","//div[@class='news_vip']//p[@class='d-qxian']");
		//验证首页新手标的状态
		fd.verifyProDetail("敬请期待","//div[@class='news_vip']//a[@class='btn tou finish_none']");
		
	}

}
