package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.DBquery;
import SecondStage.Hfax.Page.HomePage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceDetailPage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceListPage;

public class TestHuiAutoKxmhPage008 extends BaseTest{
	/**
	 *  
	 * @Description 专项投资列表页(kxmh--开心麻花)
	 * 自动发布-按周完成比例
	 * Created by songxq on 18/11/16.
	 * 
	 */
	
	private HomePage hp;
	private HuiLiCaiFinanceListPage hlc;
	private HuiLiCaiFinanceDetailPage fd;
	DBquery db = new DBquery();
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        hp = new HomePage(driver,wait);
        hlc = new HuiLiCaiFinanceListPage(driver,wait);
        fd = new HuiLiCaiFinanceDetailPage(driver,wait);
    }

	@Test
public void test_KxmhListPages() throws ClassNotFoundException, SQLException, ParseException, InterruptedException {
		
		String strPath = null;
		//自动发布-周
		//验证开心麻花列表页
		this.loadPage();
		this.HLlogin();
		//跳转到定期理财
		this.newnavigateToPage(2,"定期理财");
		//打开开心麻花的链接
		this.driver.get(this.kxmhUrl);
		//等待页面加载
		 fd.waitPage(".//*[@id='display_11']/div/div[2]", 5);
//		 fd.waitPage(".//*[@id='display_11']/div/div[2]", 5);
//			String proName= db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_name from sunif.sif_project_info where subject_name like '惠理财-开-完成比例%' and intr_year='28'","subject_name");
//				 System.out.println("惠理财-开-完成比例的名称为："+ proName);
//				 if(proName.equals(""))
//				 {
//					 fail("自动发布开心麻花产品没有展示");
//				 }
		 String proName;
		 while(true){ 
			 	this.driver.get(this.kxmhUrl);
				//等待页面加载
				 fd.waitPage(".//*[@id='display_11']/div/div[2]", 5);
			 	proName = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_name from sunif.sif_project_info where subject_name like '惠理财-开-完成比例%' and intr_year='28'","subject_name");
			 	System.out.println("惠理财-开-完成比例的名称为："+ proName);
			 	Thread.sleep(2000);
				if(proName != null&&!proName.equals("")){ 
					this.driver.get(this.kxmhUrl);
					//等待页面加载
					fd.waitPage(".//*[@id='display_11']/div/div[2]", 10);
				break;
				}
		 }
		 Thread.sleep(2000);
				 for(int i = 0; i < 100; i++){
						Set<String> set = new LinkedHashSet<String>();
						set.add(".//*[@id='display_11']/div/div[2]/div[1]/div[1]/a");
						set.add(".//*[@id='display_11']/div/div[3]/div[1]/div[1]/a");
						set.add(".//*[@id='display_11']/div/div[4]/div[1]/div[1]/a");
						set.add(".//*[@id='display_11']/div/div[5]/div[1]/div[1]/a");
						set.add(".//*[@id='display_11']/div/div[6]/div[1]/div[1]/a");
						set.add(".//*[@id='display_11']/div/div[7]/div[1]/div[1]/a");
						set.add(".//*[@id='display_11']/div/div[8]/div[1]/div[1]/a");
						set.add(".//*[@id='display_11']/div/div[9]/div[1]/div[1]/a");
						set.add(".//*[@id='display_11']/div/div[10]/div[1]/div[1]/a");
						set.add(".//*[@id='display_11']/div/div[11]/div[1]/div[1]/a");
						for(Object element: set){
						     if(this.getElementText(element.toString()).equals(proName)){
						    	 strPath = element.toString();
						         break;
						     }
						  }
						if(strPath != null && this.getElementText(strPath).equals(proName)){
							break;
						}else{
						((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
						 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("下一页")));
						 this.driver.findElement(By.linkText("下一页")).click();
						 new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_11']/div/div[2]/div[1]/div[1]/a")));
						 assertTrue("惠理财翻页后第一个产品没有显示",this.getElementText(".//*[@id='display_11']/div/div[2]/div[1]/div[1]/a")!=null);
						}
					 }
		String strPathElement = strPath.substring(0,strPath.length() - 16);
		System.out.println("截取后的为：" + strPathElement);	 	
		//验证专项投资列表页项目特点
		hlc.verifyProDetail("项目特点1",strPathElement+"/div[1]/div[1]/span[1]");
		hlc.verifyProDetail("项目特点2",strPathElement+"/div[1]/div[1]/span[2]");
		hlc.verifyProDetail("项目特点3",strPathElement+"/div[1]/div[1]/span[3]");
		//验证专项投资列表页预期年利化利率
		hp.verifyProDetails(0,"28.00% +0.50%",strPathElement+"//p[@class='benefit-sider-percent']");
		//标的起息日value_date，标的到期日mute_date
		String value_date = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select value_date from sunif.sif_project_info where subject_name="+"'"+proName+"'","value_date");
		System.out.println("value_date：=" + value_date);
		String mute_date = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select mute_date from sunif.sif_project_info where subject_name="+"'"+proName+"'","mute_date");
		System.out.println("value_date：=" + mute_date);
		int intervalDay = this.calcIntervalDay(mute_date.substring(0,10),value_date.substring(0, 10));
		System.out.println("投资期限为：" + intervalDay);
		//验证专项投资列表页投资期限
		hp.verifyProDetails(0,intervalDay+" 天",strPathElement+"/div[1]/div[2]/ul/li[2]/p[2]");
		//验证专项投资列表页的起投金额
		hp.verifyProDetails(0, "1千元",strPathElement+"/div[1]/div[2]/ul/li[3]/p[2]/i");
		//验证专项投资列表页还款方式
		hp.verifyProDetails(0,"一次性还款",strPathElement+"/div[1]/div[2]/ul/li[4]/p[2]/span");
		//验证专项投资列表页进度条
		hlc.verifyProDetail("0%",strPathElement+"/div[2]/div[1]/div/span");
		//验证专项投资列表页项目总金额
		hlc.verifyProDetail("1万元",strPathElement+"/div[2]/div[1]/p[2]/i");
		//验证专项投资列表页标的状态
		hlc.verifyProDetail("立即投资",strPathElement+"/div[2]/div[2]/a");
	}

}
