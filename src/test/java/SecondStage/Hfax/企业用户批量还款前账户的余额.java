package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.business.BusinessBaseTest;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 企业用户批量还款前账户的余额 extends BusinessBaseTest{

	/**
	 *  
	 * @Description 企业用户批量还款前账户的余额
	 * Created by songxq on 20/9/16.
	 * 
	 */
	
		protected String preInvestAmount; //投资前账户余额
		protected String preAvailableAmount; //投资前可用余额
		protected String preFreezeAmount; //投资前冻结的金额
		protected String preAllAmount;//投资前账户总资产
		Map<String, String> envVars = System.getenv();
		
		@Test
		public void test001_企业用户批量还款前账户的余额(){
			//点击我的账户
//			this.driver.findElement(By.linkText("我的账户")).click();
			this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("我的账户"))).click();
			
			//等待页面加载
			 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p")).isDisplayed();
		            }
		        });
			
			//我的账户-->账户余额
			System.out.println("我的账户【账户余额】为 " + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p")).getText().trim());
			preInvestAmount = this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p")).getText().trim();
			assertTrue("我的账户【账户余额】不正确", preInvestAmount.equals("￥ 5,575,000.00"));
			 
			//我的账户-->账户总资产
			System.out.println("我的账户【账户总资产】为 " +  this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p")).getText().trim());
			preAllAmount = this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p")).getText().trim();
			assertTrue("我的账户【账户总资产】不正确", preAllAmount.equals("￥ 5,575,000.00"));
			 
			//我的账户-->冻结金额
			System.out.println("我的账户【冻结金额】为 " + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p")).getText().trim());
			preFreezeAmount = this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p")).getText().trim();
			assertTrue("我的账户【冻结金额】不正确", preFreezeAmount.equals("￥ 0.00"));
			//IE菜单选择
			if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
			 {
				 this.selectMenuIE(2, "充值提现");
			 }else{
				 this.selectMenu(2, "充值提现");
			 }
			//等待页面加载
			 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath("//div[@class='tabtil']/ul/li")).isDisplayed();
		            }
		        });
			//投资前【账户余额】
			System.out.println("投资前【账户余额】为 " + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]")).getText().trim());
			preInvestAmount =  this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]")).getText().trim();
			assertTrue("投资前【账户余额】不正确", preInvestAmount.equals("￥5,575,000.00"));
			
			//投资前【可用余额】
			System.out.println("投资前【可用余额】为: " + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]")).getText().trim());
			preAvailableAmount = this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]")).getText().trim();
			assertTrue("投资前【可用余额】为", preAvailableAmount.equals("￥5,575,000.00"));
			 
			//投资前【冻结金额】
			System.out.println("投资前【冻结金额】为: " + this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]")).getText().trim());
			preFreezeAmount =  this.driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]")).getText().trim();
			assertTrue("投资前【冻结金额】为", preFreezeAmount.equals("￥0.00"));
		}


}
