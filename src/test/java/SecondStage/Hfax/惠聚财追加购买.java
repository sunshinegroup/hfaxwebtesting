package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 惠聚财追加购买  extends AccountBaseTest{

	/**
	 *  
	 * @Description 惠聚财追加购买
	 * Created by songxq on 09/12/16.
	 * 
	 */
	protected String preInvestAmount; //投资前账户余额
	protected String preAvailableAmount; //投资前可用余额
	protected String preFreezeAmount; //投资前冻结的金额
	protected String preAllAmount;//投资前账户总资产
	
	protected String afterTnvestAmount; //投资后账户余额
	protected String afterAvailableAmount; //投资后可用余额
	protected String afterFreezeAmount; //投资后冻结的金额
	protected String afterAllAmount;//投资后账户总资产
	Map<String, String> envVars = System.getenv();

	@Test
	public void test001_惠聚财第一次购买产品() {
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("//div[@class='zh_hz clearfix']")).isDisplayed();
	            }
	        });
		
		//我的账户-->账户余额
		System.out.println("我的账户【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p"));
		preInvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p");
		assertTrue("我的账户【账户余额】不正确", preInvestAmount.equals("￥ 555,000.00"));
		 
		//我的账户-->账户总资产
		System.out.println("我的账户【账户总资产】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p"));
		preAllAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p");
		assertTrue("我的账户【账户总资产】不正确", preAllAmount.equals("￥ 555,000.00"));
		 
		//我的账户-->冻结金额
		System.out.println("我的账户【冻结金额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p"));
		preAllAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p");
		assertTrue("我的账户【冻结金额】不正确", preAllAmount.equals("￥ 0.00"));
		 
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			 this.selectMenuIE(2,"充值提现");
		  
		 }else{
		 
			 this.selectMenu(2, "充值提现");
		 }
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("//div[@class='tabtil']/ul/li")).isDisplayed();
	            }
	        });
		//投资前【账户余额】
		System.out.println("投资前【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]"));
		preInvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]");
		assertTrue("投资前【账户余额】不正确", preInvestAmount.equals("￥555,000.00"));
		
		//投资前【可用余额】
		System.out.println("投资前【可用余额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]"));
		preAvailableAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]");
		assertTrue("投资前【可用余额】为", preAvailableAmount.equals("￥555,000.00"));
		 
		//投资前【冻结金额】
		System.out.println("投资前【冻结金额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]"));
		preFreezeAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]");
		assertTrue("投资前【冻结金额】为", preFreezeAmount.equals("￥0.00"));
		 
		this.navigateToPage("定期理财");
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
	    WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
		        
		new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")));
		//等待惠聚财第一个产品
	    assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")).isDisplayed());
		        
		//验证预期年化利率
		System.out.println("【惠聚财投资页】预期年化利率为: " + this.driver.findElement(By.xpath("//p[@class='benefit-sider-percent']")).getText().trim());
		assertTrue("【惠聚财投资页】预期年化利率不正确",this.driver.findElement(By.xpath("//p[@class='benefit-sider-percent']")).getText().trim().equals("700.00%"));
		        
		//验证投资期限
		List<WebElement> list = this.driver.findElements(By.xpath("//p[@class='benefit-sider-date']"));
		System.out.println("【惠聚财投资页】投资期限为：" + list.get(0).getText().trim());
		assertTrue("【惠聚财投资页】投资期限不正确:",list.get(0).getText().trim().equals("333 天"));
		        
		//验证起投金额
		System.out.println("【惠聚财投资页】起投金额为: "+ list.get(1).getText().trim());
		assertTrue("【惠聚财投资页】起投金额不正确",list.get(1).getText().trim().equals("1百元"));
		        
		//验证还款方式 
		System.out.println("【惠聚财投资页】还款方式为:"+ list.get(2).getText().trim());
	    assertTrue("【惠聚财投资页】还款方式不正确",list.get(2).getText().trim().equals("一次性还款"));
		        
		//验证项目总金额
		System.out.println("【惠聚财投资页】项目总金额为:"+ this.driver.findElement(By.xpath("//p[@class='total-mon listAmount']/i")).getText().trim());
		assertTrue("【惠聚财投资页】项目总金额不正确",this.driver.findElement(By.xpath("//p[@class='total-mon listAmount']/i")).getText().trim().equals("1万元"));
		        
		firstItem.findElement(By.xpath("div[2]/div[2]/a")).click();
		        
		//等待输入金额
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='amount']")));
		        
		//验证项目金额
		List<WebElement> listSec = this.driver.findElements(By.xpath("//strong[@class='object-money']"));
		System.out.println("【惠聚财投资详细页】项目金额为:" + listSec.get(0).getText().trim());
		assertTrue("【惠聚财投资详细页】项目金额不正确: ",listSec.get(0).getText().trim().equals("10,000.00"));
		       
	    //验证预期年化收益率
	    System.out.println("【惠聚财投资详细页】预期年化收益率为:" + this.driver.findElement(By.xpath("//li[@class='obj-shouyi interest']/i/strong")).getText().trim());
	    assertTrue("【惠聚财投资详细页】预期年化收益率不正确",this.driver.findElement(By.xpath("//li[@class='obj-shouyi interest']/i/strong")).getText().trim().equals("700.00%"));
		        
		//验证投资期限
		System.out.println("【惠聚财投资详细页】投资期限为:" + this.driver.findElement(By.xpath("//li[@class='obj-money']/i/strong")).getText().trim());
		assertTrue("【惠聚财投资详细页】投资期限不正确",this.driver.findElement(By.xpath("//li[@class='obj-money']/i/strong")).getText().trim().equals("333"));
		        
		//验证起投金额
		System.out.println("【惠聚财投资详细页】起投金额为:" + listSec.get(1).getText().trim());
		assertTrue("【惠聚财投资详细页】起投金额 不正确",listSec.get(1).getText().trim().equals("100.00"));
		        
		//验证收益方式
		System.out.println("【惠聚财投资详细页】收益方式为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[2]/em")).getText().trim());
		assertTrue("【惠聚财投资详细页】收益方式不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[2]/em")).getText().trim().equals("一次性还本付息"));
		        
		//验证产品风险等级
		System.out.println("【惠聚财投资详细页】产品风险等级为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[3]/em")).getText().trim());
		assertTrue("【惠聚财投资详细页】产品风险等级不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[3]/em")).getText().trim().equals("低风险"));
		        
		//验证投资人承受条件
		System.out.println("【惠聚财投资详细页】投资人承受条件为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[4]/em")).getText().trim());
		assertTrue("【惠聚财投资详细页】投资人承受条件不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[4]/em")).getText().trim().equals("保守型"));
		       
		//验证递增金额
	    System.out.println("【惠聚财投资详细页】递增金额为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[7]/em")).getText().trim());
	    assertTrue("【惠聚财投资详细页】递增金额不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[7]/em")).getText().trim().equals("100元"));
		        
		//验证项目概况
		assertTrue("【惠聚财投资详细页】项目概况不存在",this.driver.findElement(By.id("one1")).getText().trim().equals("项目概况"));
		this.driver.findElement(By.id("one1")).click();
		        
		//验证网上开户协议
		assertTrue("【惠聚财投资详细页】网上开户协议不存在",this.driver.findElement(By.id("one2")).getText().trim().equals("网上开户协议"));
		this.driver.findElement(By.id("one2")).click();
		        
	   //验证产品说明书
	   assertTrue("【惠聚财投资详细页】产品说明书不存在",this.driver.findElement(By.id("one3")).getText().trim().equals("产品说明书"));
	   this.driver.findElement(By.id("one3")).click();
		        
	   //验证风险揭示书
	  assertTrue("【惠聚财投资详细页】风险揭示书不存在",this.driver.findElement(By.id("one4")).getText().trim().equals("风险揭示书"));
	  this.driver.findElement(By.id("one4")).click();
		        
	  //验证产品认购协议
	  assertTrue("【惠聚财投资详细页】产品认购协议不存在",this.driver.findElement(By.id("one5")).getText().trim().equals("产品认购协议"));
     this.driver.findElement(By.id("one5")).click();
		        
     //验证投资金额下面的网上开户协议
	  assertTrue("【惠聚财投资详细页】投资金额下的网上开户协议不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc2']")).getText().trim().equals("《网上开户协议》"));
	  this.driver.findElement(By.xpath("//a[@class='btn_ccc2']")).click();
	  this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
		            }
		        });
	  this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
		        
	  //验证投资金额下面的产品说明书
	   assertTrue("【惠聚财投资详细页】投资金额下的产品说明书不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc3']")).getText().trim().equals("《产品说明书》"));
	   this.driver.findElement(By.xpath("//a[@class='btn_ccc3']")).click();
	   this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
		            }
		        });
		        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
		        
		//验证投资金额下面的风险揭示书
		assertTrue("【惠聚财投资详细页】投资金额下的风险揭示书不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc4']")).getText().trim().equals("《风险揭示书》"));
		this.driver.findElement(By.xpath("//a[@class='btn_ccc4']")).click();
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
			}
		});
		this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();

		//验证投资金额下面的产品认购协议
		assertTrue("【惠聚财投资详细页】投资金额下的产品认购协议不存在",
				this.driver.findElement(By.xpath("//a[@class='btn_ccc1']")).getText().trim().equals("《产品认购协议》"));
		this.driver.findElement(By.xpath("//a[@class='btn_ccc1']")).click();
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
			}
		});
		this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();

		//输入投资金额
		this.driver.findElement(By.xpath(".//*[@id='amount']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='amount']")).sendKeys("200");

		//验证投资金额下的预期收益
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='interests']")));
		assertTrue("【惠聚财投资详细页】投资金额下预期收益不正确",
				this.driver.findElement(By.xpath(".//*[@id='interests']")).getText().trim().equals("1,295.00"));

		//点击已同意
		this.driver.findElement(By.xpath(".//*[@id='agre']")).click();

		//点击去投资
		this.driver.findElement(By.linkText("去投资")).click();
		
		//等待实际投资本金(元)
       this.wait.until(new ExpectedCondition<Boolean>() {
           @Override
           public Boolean apply(WebDriver webDriver) {
           	String text = driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText();
               return text.trim().equalsIgnoreCase("200.00");
           }
       });
        
       //验证产品信息中的预期收益率
       System.out.println("【惠聚财确认页产品信息中的预期收益率】为：" + this.getElementText(".//*[@id='investdetail']/div[1]/span[2]"));
       assertTrue("【惠聚财确认页产品信息中的预期收益率】为：",this.getElementText(".//*[@id='investdetail']/div[1]/span[2]").equals("700.00%"));
       
       //验证产品信息中的投资期限
       System.out.println("【惠聚财确认页产品信息中的投资期限】为：" + this.getElementText(".//*[@id='investdetail']/div[2]/span[2]"));
       assertTrue("【惠聚财确认页产品信息中的投资期限】为：",this.getElementText(".//*[@id='investdetail']/div[2]/span[2]").equals("333天"));
       
       //验证产品信息中的投资金额
       System.out.println("【惠聚财确认页产品信息中的投资金额】为：" + this.getElementText(".//*[@id='investdetail']/div[3]/span[2]"));
       assertTrue("【惠聚财确认页产品信息中的投资金额】为：",this.getElementText(".//*[@id='investdetail']/div[3]/span[2]").equals("¥200.00"));
       
       //验证实际投资本金(元)
       System.out.println("【惠聚财确认页】实际投资本金(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim());
       assertTrue("【惠聚财确认页】实际投资本金(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim().equals("200.00"));
       
       //验证总预期收益率(%)
       System.out.println("【惠聚财确认页】总预期收益率(%)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim() + "%");
       assertTrue("【惠聚财确认页】总预期收益率(%)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim().equals("700.00"));
       
       //验证总预期收益(元)
       System.out.println("【惠聚财确认页】总预期收益(元)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim());
       assertTrue("【惠聚财确认页】总预期收益(元)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim().equals("1,295.00"));
       
       //验证实际支付金额(元)
       System.out.println("【惠聚财确认页】实际支付金额(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim());
       assertTrue("【惠聚财确认页】实际支付金额(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim().equals("200.00"));
       
       
       //输入交易密码
       this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).clear();
       this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).sendKeys(this.jiaoyipassword);
       
       //点击确定投资
       this.driver.findElement(By.xpath(".//*[@id='besureInvest']")).click();
       
       
       //等待确认框
       this.wait.until(new ExpectedCondition<Boolean>() {
           @Override
           public Boolean apply(WebDriver webDriver) {
           	String text = driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[1]")).getText();
               return text.trim().equalsIgnoreCase("浏览更多项目");
           }
       });
       
       //验证确认框本次投资金额
       System.out.println("【惠聚财确认框】本次投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim());
       assertTrue("【惠聚财确认框】本次投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim().equals("200.00"));
       
       //验证本项目累计投资金额
       System.out.println("【惠聚财确认框】本项目累计投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim());
       assertTrue("【惠聚财确认框】本项目累计投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim().equals("200.00"));
       
       //验证查看申请是否存在
       assertTrue("【惠聚财确认框】查看申请不存在:",this.driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[2]")).isDisplayed());
       //点击查看申请
       this.driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[2]")).click();
		        		
		
	}

	@Test
	public void test002_惠聚财第二次购买产品(){
		
		this.navigateToPage("定期理财");
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
	    WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
		        
		new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")));
		//等待惠聚财第一个产品
	    assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")).isDisplayed());
		        
		//验证预期年化利率
		System.out.println("【惠聚财投资页】预期年化利率为: " + this.driver.findElement(By.xpath("//p[@class='benefit-sider-percent']")).getText().trim());
		assertTrue("【惠聚财投资页】预期年化利率不正确",this.driver.findElement(By.xpath("//p[@class='benefit-sider-percent']")).getText().trim().equals("700.00%"));
		        
		//验证投资期限
		List<WebElement> list = this.driver.findElements(By.xpath("//p[@class='benefit-sider-date']"));
		System.out.println("【惠聚财投资页】投资期限为：" + list.get(0).getText().trim());
		assertTrue("【惠聚财投资页】投资期限不正确:",list.get(0).getText().trim().equals("333 天"));
		        
		//验证起投金额
		System.out.println("【惠聚财投资页】起投金额为: "+ list.get(1).getText().trim());
		assertTrue("【惠聚财投资页】起投金额不正确",list.get(1).getText().trim().equals("1百元"));
		        
		//验证还款方式 
		System.out.println("【惠聚财投资页】还款方式为:"+ list.get(2).getText().trim());
	    assertTrue("【惠聚财投资页】还款方式不正确",list.get(2).getText().trim().equals("一次性还款"));
		        
		//验证项目总金额
		System.out.println("【惠聚财投资页】项目总金额为:"+ this.driver.findElement(By.xpath("//p[@class='total-mon listAmount']/i")).getText().trim());
		assertTrue("【惠聚财投资页】项目总金额不正确",this.driver.findElement(By.xpath("//p[@class='total-mon listAmount']/i")).getText().trim().equals("1万元"));
		        
		firstItem.findElement(By.xpath("div[2]/div[2]/a")).click();
		        
		//等待输入金额
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='amount']")));
		        
		//验证项目金额
		List<WebElement> listSec = this.driver.findElements(By.xpath("//strong[@class='object-money']"));
		System.out.println("【惠聚财投资详细页】项目金额为:" + listSec.get(0).getText().trim());
		assertTrue("【惠聚财投资详细页】项目金额不正确: ",listSec.get(0).getText().trim().equals("10,000.00"));
		       
	    //验证预期年化收益率
	    System.out.println("【惠聚财投资详细页】预期年化收益率为:" + this.driver.findElement(By.xpath("//li[@class='obj-shouyi interest']/i/strong")).getText().trim());
	    assertTrue("【惠聚财投资详细页】预期年化收益率不正确",this.driver.findElement(By.xpath("//li[@class='obj-shouyi interest']/i/strong")).getText().trim().equals("700.00%"));
		        
		//验证投资期限
		System.out.println("【惠聚财投资详细页】投资期限为:" + this.driver.findElement(By.xpath("//li[@class='obj-money']/i/strong")).getText().trim());
		assertTrue("【惠聚财投资详细页】投资期限不正确",this.driver.findElement(By.xpath("//li[@class='obj-money']/i/strong")).getText().trim().equals("333"));
		        
		//验证起投金额
		System.out.println("【惠聚财投资详细页】起投金额为:" + listSec.get(1).getText().trim());
		assertTrue("【惠聚财投资详细页】起投金额 不正确",listSec.get(1).getText().trim().equals("100.00"));
		        
		//验证收益方式
		System.out.println("【惠聚财投资详细页】收益方式为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[2]/em")).getText().trim());
		assertTrue("【惠聚财投资详细页】收益方式不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[2]/em")).getText().trim().equals("一次性还本付息"));
		        
		//验证产品风险等级
		System.out.println("【惠聚财投资详细页】产品风险等级为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[3]/em")).getText().trim());
		assertTrue("【惠聚财投资详细页】产品风险等级不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[3]/em")).getText().trim().equals("低风险"));
		        
		//验证投资人承受条件
		System.out.println("【惠聚财投资详细页】投资人承受条件为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[4]/em")).getText().trim());
		assertTrue("【惠聚财投资详细页】投资人承受条件不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[4]/em")).getText().trim().equals("保守型"));
		       
		//验证递增金额
	    System.out.println("【惠聚财投资详细页】递增金额为:" + this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[7]/em")).getText().trim());
	    assertTrue("【惠聚财投资详细页】递增金额不正确",this.driver.findElement(By.xpath("//div[@class='detailLeft']/ul[@class='bidBox']/li[7]/em")).getText().trim().equals("100元"));
		        
		//验证项目概况
		assertTrue("【惠聚财投资详细页】项目概况不存在",this.driver.findElement(By.id("one1")).getText().trim().equals("项目概况"));
		this.driver.findElement(By.id("one1")).click();
		        
		//验证网上开户协议
		assertTrue("【惠聚财投资详细页】网上开户协议不存在",this.driver.findElement(By.id("one2")).getText().trim().equals("网上开户协议"));
		this.driver.findElement(By.id("one2")).click();
		        
	   //验证产品说明书
	   assertTrue("【惠聚财投资详细页】产品说明书不存在",this.driver.findElement(By.id("one3")).getText().trim().equals("产品说明书"));
	   this.driver.findElement(By.id("one3")).click();
		        
	   //验证风险揭示书
	  assertTrue("【惠聚财投资详细页】风险揭示书不存在",this.driver.findElement(By.id("one4")).getText().trim().equals("风险揭示书"));
	  this.driver.findElement(By.id("one4")).click();
		        
	  //验证产品认购协议
	  assertTrue("【惠聚财投资详细页】产品认购协议不存在",this.driver.findElement(By.id("one5")).getText().trim().equals("产品认购协议"));
	  this.driver.findElement(By.id("one5")).click();
		        
      //验证投资金额下面的网上开户协议
	  assertTrue("【惠聚财投资详细页】投资金额下的网上开户协议不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc2']")).getText().trim().equals("《网上开户协议》"));
	  this.driver.findElement(By.xpath("//a[@class='btn_ccc2']")).click();
	  this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
		            }
		        });
	  this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
		        
	  //验证投资金额下面的产品说明书
	   assertTrue("【惠聚财投资详细页】投资金额下的产品说明书不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc3']")).getText().trim().equals("《产品说明书》"));
	   this.driver.findElement(By.xpath("//a[@class='btn_ccc3']")).click();
	   this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
		            }
		        });
		        this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();
		        
		//验证投资金额下面的风险揭示书
		assertTrue("【惠聚财投资详细页】投资金额下的风险揭示书不存在",this.driver.findElement(By.xpath("//a[@class='btn_ccc4']")).getText().trim().equals("《风险揭示书》"));
		this.driver.findElement(By.xpath("//a[@class='btn_ccc4']")).click();
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
			}
		});
		this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();

		//验证投资金额下面的产品认购协议
		assertTrue("【惠聚财投资详细页】投资金额下的产品认购协议不存在",
				this.driver.findElement(By.xpath("//a[@class='btn_ccc1']")).getText().trim().equals("《产品认购协议》"));
		this.driver.findElement(By.xpath("//a[@class='btn_ccc1']")).click();
		this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).isDisplayed();
			}
		});
		this.driver.findElement(By.xpath(".//*[@id='pop1']/h2/a")).click();

		//输入投资金额
		this.driver.findElement(By.xpath(".//*[@id='amount']")).clear();
		this.driver.findElement(By.xpath(".//*[@id='amount']")).sendKeys("200");

		//验证投资金额下的预期收益
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='interests']")));
		assertTrue("【惠聚财投资详细页】投资金额下预期收益不正确",
				this.driver.findElement(By.xpath(".//*[@id='interests']")).getText().trim().equals("1,295.00"));

		//点击已同意
		this.driver.findElement(By.xpath(".//*[@id='agre']")).click();

		//点击去投资
		this.driver.findElement(By.linkText("去投资")).click();
		
		//等待实际投资本金(元)
      this.wait.until(new ExpectedCondition<Boolean>() {
          @Override
          public Boolean apply(WebDriver webDriver) {
          	String text = driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText();
              return text.trim().equalsIgnoreCase("200.00");
          }
      });
       
      //验证产品信息中的预期收益率
      System.out.println("【惠聚财确认页产品信息中的预期收益率】为：" + this.getElementText(".//*[@id='investdetail']/div[1]/span[2]"));
      assertTrue("【惠聚财确认页产品信息中的预期收益率】为：",this.getElementText(".//*[@id='investdetail']/div[1]/span[2]").equals("700.00%"));
      
      //验证产品信息中的投资期限
      System.out.println("【惠聚财确认页产品信息中的投资期限】为：" + this.getElementText(".//*[@id='investdetail']/div[2]/span[2]"));
      assertTrue("【惠聚财确认页产品信息中的投资期限】为：",this.getElementText(".//*[@id='investdetail']/div[2]/span[2]").equals("333天"));
      
      //验证产品信息中的投资金额
      System.out.println("【惠聚财确认页产品信息中的投资金额】为：" + this.getElementText(".//*[@id='investdetail']/div[3]/span[2]"));
      assertTrue("【惠聚财确认页产品信息中的投资金额】为：",this.getElementText(".//*[@id='investdetail']/div[3]/span[2]").equals("¥200.00"));
      
      //验证实际投资本金(元)
      System.out.println("【惠聚财确认页】实际投资本金(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim());
      assertTrue("【惠聚财确认页】实际投资本金(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjtzbj']")).getText().trim().equals("200.00"));
      
      //验证总预期收益率(%)
      System.out.println("【惠聚财确认页】总预期收益率(%)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim() + "%");
      assertTrue("【惠聚财确认页】总预期收益率(%)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsyl']")).getText().trim().equals("700.00"));
      
      //验证总预期收益(元)
      System.out.println("【惠聚财确认页】总预期收益(元)为:" + this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim());
      assertTrue("【惠聚财确认页】总预期收益(元)不正确:",this.driver.findElement(By.xpath("//p[@class='basic-left']/span[@id='yqsy']")).getText().trim().equals("1,295.00"));
      
      //验证实际支付金额(元)
      System.out.println("【惠聚财确认页】实际支付金额(元)为:" + this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim());
      assertTrue("【惠聚财确认页】实际支付金额(元)不正确:",this.driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText().trim().equals("200.00"));
      
      
      //输入交易密码
      this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).clear();
      this.driver.findElement(By.xpath(".//*[@id='dealpwd']")).sendKeys(this.jiaoyipassword);
      
      //点击确定投资
      this.driver.findElement(By.xpath(".//*[@id='besureInvest']")).click();
      
      
      //等待确认框
      this.wait.until(new ExpectedCondition<Boolean>() {
          @Override
          public Boolean apply(WebDriver webDriver) {
          	String text = driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[1]")).getText();
              return text.trim().equalsIgnoreCase("浏览更多项目");
          }
      });
      
      //验证确认框本次投资金额
      System.out.println("【惠聚财确认框】本次投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim());
      assertTrue("【惠聚财确认框】本次投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='inveamount']")).getText().trim().equals("200.00"));
      
      //验证本项目累计投资金额
      System.out.println("【惠聚财确认框】本项目累计投资金额为: "+ this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim());
      assertTrue("【惠聚财确认框】本项目累计投资金额不正确 ",this.driver.findElement(By.xpath(".//*[@id='investCount']")).getText().trim().equals("400.00"));
      
      //验证查看申请是否存在
      assertTrue("【惠聚财确认框】查看申请不存在:",this.driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[2]")).isDisplayed());
      //点击查看申请
      this.driver.findElement(By.xpath(".//*[@id='investSuccess']/p/a[2]")).click();
		
	}
	//项目总额为1W，投资400元，投资进度条为4%
	
	@Test
	public void test003_惠聚财进度条验证(){
		this.navigateToPage("定期理财");
		//点击惠聚财
		WebElement content = this.clickOnSubmenu("惠聚财");
	    WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
		        
		new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")));
		//等待惠聚财第一个产品
	    assertTrue("惠聚财第一个产品不存在", this.driver.findElement(By.xpath(".//*[@id='display_15']/div/div[2]/div[2]/div[2]/a")).isDisplayed());
		
	    //验证进度条是否正确
	    assertTrue("惠聚财产品投资页投资进度条不正确",this.getElementText(".//*[@id='display_15']/div/div[2]/div[2]/div[1]/div/span").equals("4%"));
	    
	    //点击立即投资
	    this.driver.findElement(By.xpath("//a[@class='btn tou']")).click();
	    
	    //验证惠聚财产品投资详细页的进度条是否正确
	    assertTrue("惠聚财产品投资详细页投资进度条不正确",this.getElementText(".//*[@id='jindu']").equals("4"));
	}

	@Test
	public void test004_惠聚财查看收益是否正确(){
		//等待我的主页
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("我的主页")));
        
        //我的投资->惠聚财
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			 this.selectMenuIE(10,"我的投资");
		  
		 }else{
		 
			 this.selectMenu(10, "我的投资");
		 }
       
        this.selectSubMenu(5, "惠聚财");
        
         //选择正在招标中的产品
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("borrowstatus")));
        assertTrue("请选择下拉框不存在",this.driver.findElement(By.id("borrowstatus")).getText().contains("--请选择--"));
        this.searchFinancePro("正在招标中");
        
        //点击搜索
        assertTrue("搜索不存在",this.driver.findElement(By.id("search")).isDisplayed());
        this.driver.findElement(By.id("search")).click();
        
        //验证惠聚财产品列表不为空
        List<WebElement> list = this.driver.findElements(By.xpath("//tr//td[@align='center']//a[@target='_blank']")); 
  	    assertTrue("不存在惠聚财的产品",list.size()>0);
  	    
  	    //验证搜索页的本金(元)
  	    List<WebElement> list1 = this.driver.findElements(By.xpath("//td[@align='center']"));
  	    System.out.println("【惠聚财搜索页】的本金(元)为:"+ list1.get(1).getText().trim());
  	    assertTrue("惠聚财搜索页】的本金(元)不正确",list1.get(1).getText().trim().equals("400.00"));
  	    
  	    //验证搜索页的预计收益(元)
  	    System.out.println("【惠聚财搜索页】的预计收益(元)为:"+ list1.get(2).getText().trim());
  	    assertTrue("【惠聚财搜索页】的预计收益(元)不正确",list1.get(2).getText().trim().equals("2,590.00"));
  	    
  	    //验证搜索页的起息时间
  	    System.out.println("【惠聚财搜索页】的起息时间为:"+ list1.get(4).getText().trim());
	    assertTrue("【惠聚财搜索页】的起息时间不正确",list1.get(4).getText().trim().equals("20170102"));
  	    
  	   //验证搜索页的到期时间
	    System.out.println("【惠聚财搜索页】的到期时间为:"+ list1.get(5).getText().trim());
	    assertTrue("【惠聚财搜索页】的到期时间不正确",list1.get(5).getText().trim().equals("20171201"));
  	    
  	    //验证搜索页的标的状态
	    System.out.println("【惠聚财搜索页】的标的状态为:"+ list1.get(6).getText().trim());
	    assertTrue("【惠聚财搜索页】的标的状态不正确",list1.get(6).getText().trim().equals("正在招标中"));
	}

	 @Test
	 public void test005_验证投资后账户余额可用余额产账户总资产和冻结金额(){
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p")).isDisplayed();
	            }
	        });
		
		//投资后我的账户-->账户余额
		System.out.println("我的账户投资后【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p"));
		afterTnvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p");
		assertTrue("我的账户投资后【账户余额】不正确", afterTnvestAmount.equals("￥ 555,000.00"));
		 
		//投资后我的账户-->账户总资产
		System.out.println("我的账户投资后【账户总资产】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p"));
		afterAllAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[2]/p");
		assertTrue("我的账户投资后【账户总资产】不正确", afterAllAmount.equals("￥ 557,590.00"));
		 
		//投资后我的账户-->冻结金额
		System.out.println("我的账户投资后【冻结金额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p"));
		afterFreezeAmount = this.getElementText("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[4]/p");
		assertTrue("我的账户投资后【冻结金额】不正确", afterFreezeAmount.equals("￥ 400.00"));
		 
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			 this.selectMenuIE(2,"充值提现");
		  
		 }else{
		 
			 this.selectMenu(2, "充值提现");
		 }
       
		//等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath("//div[@class='tabtil']/ul/li")).isDisplayed();
		            }
		  });
			 
		//投资后【账户余额】
		System.out.println("投资后【账户余额】为 " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]"));
		afterTnvestAmount = this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[1]");
		assertTrue("投资后【账户余额】不正确", afterTnvestAmount.equals("￥555,000.00"));
			
		//投资后【可用余额】
		System.out.println("投资后【可用余额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]"));
		afterAvailableAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[2]");
		assertTrue("投资后【可用余额】为", afterAvailableAmount.equals("￥554,600.00"));
			 
		//投资后【冻结金额】
		System.out.println("投资后【冻结金额】为: " + this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]"));
		afterFreezeAmount =  this.getElementText("html/body/div[3]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[2]/td[3]");
		assertTrue("投资后【冻结金额】为", afterFreezeAmount.equals("￥400.00"));
		
	   
	 }
	
}
