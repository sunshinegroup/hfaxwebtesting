package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.BasePage;
import SecondStage.Hfax.Page.DBquery;
import SecondStage.Hfax.Page.HomePage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceDetailPage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceListPage;

public class TestNewHuiSouDongOtherPages004 extends BaseTest{
	/**
	 *  
	 * @Description 列表页,投资详情页,项目概况,投资说明书
	 * 手动发布-新手标-收益权转让-非溢价-无担保-预热标-手动审核
	 * Created by songxq on 16/11/16.
	 * 
	 */
	private HomePage hp;
	private HuiLiCaiFinanceListPage hlc;
	private HuiLiCaiFinanceDetailPage fd;
	DBquery db = new DBquery();
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        hp = new HomePage(driver,wait);
        hlc = new HuiLiCaiFinanceListPage(driver,wait);
        fd = new HuiLiCaiFinanceDetailPage(driver,wait);
	}
	@Test
	public void test_ListPages() throws ClassNotFoundException, SQLException, ParseException {
		Map<String, String> envVars = System.getenv(); 
		String strPath = null;
		this.loadPage();
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
			 
			 this.driver.findElement(By.xpath("//li[@id='_topMenu_dingqilicai']/a")).click();
			 
		 }else{
			//跳转到定期理财
			 this.topNavigationMenu1(3,"定期理财");
			
		 }
		//跳转到新手专区
		this.clickOnSubmenu("新手专区");
		//等待页面加载
		 fd.waitPage(".//*[@id='display_7']/div/div[2]", 5);
		 String proName= db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_name from sunif.sif_project_info where subject_name like '惠理财-新手收益非溢价%' and intr_year='28'","subject_name");
		 System.out.println("新手标的名称为："+ proName);
		 if(proName.equals(""))
		 {
			 fail("手动发布新手专区产品没有展示");
		 }
		 for(int i = 0; i < 100; i++){
				Set<String> set = new LinkedHashSet<String>();
				set.add(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[3]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[4]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[5]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[6]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[7]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[8]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[9]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[10]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[11]/div[1]/div[1]/a");
				for(Object element: set){
				     if(this.getElementText(element.toString()).equals(proName)){
				    	 strPath = element.toString();
				         break;
				     }
				  }
				if(strPath != null && this.getElementText(strPath).equals(proName)){
					break;
				}else{
				((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
				 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("下一页")));
				 this.driver.findElement(By.linkText("下一页")).click();
				 new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a")));
				 assertTrue("惠理财翻页后第一个产品没有显示",this.getElementText(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a")!=null);
				}
			 }
		 String strPathElement = strPath.substring(0,strPath.length() - 16);
		 System.out.println("截取后的为：" + strPathElement);	 		 
		//验证新手标的列表页项目特点
		hlc.verifyProDetail("项目特点1",strPathElement+"/div[1]/div[1]/span[1]");
		hlc.verifyProDetail("项目特点2",strPathElement+"/div[1]/div[1]/span[2]");
		hlc.verifyProDetail("项目特点3",strPathElement+"/div[1]/div[1]/span[3]");
		//验证新手标的列表页预期年利化利率
		hlc.verifyProDetail("28.00%",strPathElement+"//p[@class='benefit-sider-percent']");
		//标的起息日value_date，标的到期日mute_date
		String value_date = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select value_date from sunif.sif_project_info where subject_name like '惠理财-新手收益非溢价%' and intr_year='28'","value_date");
		System.out.println("value_date：=" + value_date);
		String mute_date = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select mute_date from sunif.sif_project_info where subject_name like '惠理财-新手收益非溢价%' and intr_year='28'","mute_date");
		System.out.println("value_date：=" + mute_date);
		int intervalDay = this.calcIntervalDay(mute_date.substring(0,10),value_date.substring(0, 10));
		System.out.println("投资期限为：" + intervalDay);
		//验证新手标的列表页投资期限
		hp.verifyProDetails(0,intervalDay+" 天",strPathElement+"//p[@class='benefit-sider-date']");
		//验证新手标的列表页的起投金额
		hp.verifyProDetails(1, "1千元",strPathElement+"//p[@class='benefit-sider-date']");
		//验证新手标的列表页还款方式
		hp.verifyProDetails(2,"一次性还款",strPathElement+"//p[@class='benefit-sider-date']");
		//验证新手标的列表页进度条
		hlc.verifyProDetail("0%",strPathElement+"/div[2]/div[1]/div/span");
		//验证新手标的列表页项目总金额
		hlc.verifyProDetail("1万元",strPathElement+"/div[2]/div[1]/p[2]/i");
		//验证新手标的列表页标的状态
		hlc.verifyProDetail("即将开始",strPathElement+"/div[2]/div[2]/a");
	
		//惠理财用户登录
		this.HLlogin();
		//跳转到定期理财
		this.loginTopNavigationMenu1(3,"定期理财");
		
		//跳转到新手专区
		this.clickOnSubmenu("新手专区");
		
		while(true){
			if(IsElementPresent(By.xpath(".//*[@id='display_7']/div/div[2]"))){
				break;
			}
		}
		//等待详情页页面加载
		fd.waitPage(".//*[@id='display_7']/div/div[2]", 5);
		//点击新手专区标的名称
		fd.switchWindows(strPathElement+"/div[1]/div[1]/a");
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	this.driver.manage().window().maximize();
        	try {
     			Thread.sleep(5000);
               } catch (InterruptedException e) {
     			// TODO Auto-generated catch block
     			e.printStackTrace();
               }	  
        }
		boolean a = IsElementPresent(By.xpath("//li[@class='amount']/i/strong"));
		System.out.println("验证元素是否存在: " + a);
		//验证新手专区投资详情页项目金额
		hlc.verifyProDetail("10,000.00","//li[@class='amount']/i/strong");
		//验证新手专区投资详情页预期年化收益率
		hlc.verifyProDetail("28.00","//li[@class='interest']/i/strong");
		//验证投资详情页促销利率
//		hlc.verifyProDetail("+0.50%","//i[@class='rateCount']");
		//验证新手专区投资详情页投资期限
		hlc.verifyProDetail(String.valueOf(intervalDay),"//li[@class='term']/i/strong");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//验证新手专区投资详情页投资进度
		hlc.verifyProDetail("0",".//*[@id='jindu']");
		//验证新手专区投资详情页计息日
		hlc.verifyProDetail(value_date.substring(0, 10),"html/body/div[6]/div[2]/div[1]/ul[2]/li[3]/em");
		//验证新手专区投资详情页收益方式
		if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
			
			hlc.verifyProDetail("收益方式： 一次性还本付息","//ul[@class='bidBox']//li[4]");
			
		}else{
			
			hlc.verifyProDetail("收益方式："+"\n"+"一次性还本付息","//ul[@class='bidBox']//li[4]");
		}
		//验证新手专区投资详情页起投金额
		hp.verifyProDetails(0,"1000元","//em[@class='qitou']");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//验证新手专区投资详细页累计投资上限
		hp.verifyProDetails(1,"5000元","//em[@class='qitou']");
		//验证新手专区投资详细页剩余可投金额
		hlc.verifyProDetail("10,000.00","//div[@class='balanceTitle']/strong/em");
		//点击新手专区投资详细页的项目概况
		hlc.clickButton(".//*[@id='one1']");
		
		try {
			//this.driver.navigate().refresh();
			Thread.sleep(5000);			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//等待页面加载
		fd.waitPage(".//*[@id='con_one_1']/h3",5);
		//验证新手专区投资详细页项目概况标题的名称
		hlc.verifyProDetail("项目概况标题1",".//*[@id='con_one_1']/h3");
		//验证新手专区投资详细页项目概况内容
		hlc.verifyProDetail("项目概况内容1",".//*[@id='con_one_1']/ul/li/strong");

		 //点击新手专区投资详细页投资说明书
		 hlc.clickButton(".//*[@id='one2']");
		 //等待页面加载
		 fd.waitPage(".//*[@id='con_one_2']/div/h4[1]",5);
		//验证新手专区投资理财详细页-->投资说明书,项目名称
		 hlc.verifyProDetail("惠理财-新手收益非溢价"+"\n"+"定向委托投资管理交易说明书",".//*[@id='con_one_2']/div/h4[1]");
		 //验证新手专区投资理财详细页-->投资说明书,第一章最后一行：读取发布年+月+日 
		 fd.verifyProDetail(this.getChineseDate(1),".//*[@id='con_one_2']/div/div[1]");
		 												
		 
		 //验证新手专区投资理财详细页--->投资说明书，第三章第一项第4条：【定向委托投资标的成立日】读取定向委托标成立日
		 String dep_val_day = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select dep_val_day from sunif.sif_project_official s inner join sunif.sif_project_info s1 on s1.project_cd= s.project_cd  where s1.subject_name like '惠理财-新手收益非溢价%' and intr_year='28'","dep_val_day");
		 System.out.println(dep_val_day);
		 String dep_val_day1 = dep_val_day.substring(0, 10);
		 String [] dep_val_day2 = dep_val_day1.split("-");
		 System.out.println(dep_val_day2[0] + "年" + dep_val_day2[1] + "月" + dep_val_day2[2] + "日");
		 fd.verifyProDetail("金融产品份额的成立日："+ dep_val_day2[0] + "年" + dep_val_day2[1] + "月" + dep_val_day2[2] + "日",".//*[@id='con_one_2']/div/div[3]");
		 //验证新手专区投资理财详细页--->投资说明书，第三章第一项第5条：【定向委托投资标的到期日】读取定向委托标到期日
		 String dep_matu_day = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select dep_matu_day from sunif.sif_project_official s inner join sunif.sif_project_info s1 on s1.project_cd= s.project_cd  where s1.subject_name like '惠理财-新手收益非溢价%' and intr_year='28'","dep_matu_day");
		 System.out.println(dep_matu_day);
		 String dep_matu_day1 = dep_matu_day.substring(0, 10);
		 String [] dep_matu_day2 = dep_matu_day1.split("-");
		 System.out.println(dep_matu_day2[0] + "年" + dep_matu_day2[1] + "月" + dep_matu_day2[2] + "日");
		 fd.verifyProDetail("金融产品份额的到期日："+ dep_matu_day2[0] + "年" + dep_matu_day2[1] + "月" + dep_matu_day2[2] + "日",".//*[@id='con_one_2']/div/div[3]");
		 
		 //验证新手专区投资理财详细页--->投资说明书，第三章第一项第6条：【定向委托投资标的剩余存续期】读取融资期限
		 fd.verifyProDetail("6.金融产品份额剩余存续期："+intervalDay+"天","//*[@id='con_one_2']/div/div[3]");
		 //验证新手专区投资理财详细页--->投资说明书，第三章第一项第7条：【定向委托投资标的预期收益率】读取产品利率
		 fd.verifyProDetail("金融产品份额预期收益率： 29.00 %/年","//*[@id='con_one_2']/div/div[3]");
		//新手专区验证投资理财详细页--->投资说明书，第三章第二项第1条：【交易名称】读取项目名称
		 fd.verifyProDetail("交易名称：惠理财-新手收益非溢价","//*[@id='con_one_2']/div/div[3]");
		 //验证新手专区投资理财详细页--->投资说明书，第三章第二项第3条：【交易编号】读取标的编号
		 String subject_cd = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_cd from sunif.sif_project_info where subject_name like '惠理财-新手收益非溢价%' and intr_year='28'","subject_cd");
		 fd.verifyProDetail("交易编号："+ subject_cd,"//*[@id='con_one_2']/div/div[3]");
		 //验证新手专区投资理财详细页--->投资说明书，第三章第二项第4条：【受托人】读取借款公司名称
		 fd.verifyProDetail("受托人：测试公司名字00010","//*[@id='con_one_2']/div/div[3]");
		 //验证新手专区投资理财详细页---> 投资说明书，第三章第二项第6条：【委托人最低委托金额】读取起投金额
		 fd.verifyProDetail("委托人最低委托金额：1000元","//*[@id='con_one_2']/div/div[3]");
		 
		 //验证投资理财详细页---> 投资说明书，第三章第二项第7条：【定向委托投资收益起始日】读取起息日
		 String value_date1 = value_date.substring(0, 10);
		 String[] value_date2 = value_date1.split("-"); 
		 fd.verifyProDetail(value_date2[0] +"年" + value_date2[1] + "月" + value_date2[2] + "日","//*[@id='con_one_2']/div/div[3]");	
		 //验证投资理财详细页---> 投资说明书， 第三章第二项第8条：【定向委托投资收益到期日】读取到期日
		 String mute_date1 = mute_date.substring(0, 10);
		 String[] mute_date2 = mute_date1.split("-"); 
		 fd.verifyProDetail(mute_date2[0] +"年" + mute_date2[1] + "月" + mute_date2[2] + "日","//*[@id='con_one_2']/div/div[3]");
		 //验证投资理财详细页---> 投资说明书，第三章第二项第9条：【定向委托投资收益获得期】读取投资期限
		 fd.verifyProDetail("9.定向委托投资收益获得期："+intervalDay+"天","//*[@id='con_one_2']/div/div[3]");
		 
		 //验证投资理财详细页---> 投资说明书，第三章第二项第11条：【管理费率】读取管理费率
//		 fd.verifyProDetail("管理费率：-79.95%/年","//*[@id='con_one_2']/div/div/div[3]");
		 //验证投资理财详细页---> 投资说明书，第三章第二项第12条：【委托人预期收益率】读取投资人利率
		 fd.verifyProDetail("委托人预期收益率： 28.00 %/年","//*[@id='con_one_2']/div/div[3]");
		 //验证投资理财详细页---> 投资说明书，第三章第二项第13条：【户名】读取企业名称、【账户】读取银行账号
		 fd.verifyProDetail("户名：测试公司名字00010","//*[@id='con_one_2']/div/div[3]");
		 fd.verifyProDetail("账户：6211234567890","//*[@id='con_one_2']/div/div[3]");
		 fd.verifyProDetail("开户银行：中国工商银行五道口","//*[@id='con_one_2']/div/div[3]");
	
		// 验证投资理财详细页-->投资管理合同
		 hlc.clickButton(".//*[@id='one4']");
		 //等待页面加载
		 fd.waitPage(".//*[@id='con_one_4']/div[1]/h4",5);
		 //验证投资理财详细页--->投资管理合同：乙方,【公司名称】读取企业名称
		 String companyName = fd.findTextField(5,2);
		 fd.verifyText("测试公司名字00010", companyName);
		 //验证投资理财详细页--->投资管理合同：【惠金所用户名】读取企业用的惠金所登陆名
		 String companyLoginname = fd.findTextField(5,4);
		 fd.verifyText("liang100010", companyLoginname);
		 //验证投资理财详细页--->投资管理合同：【定向委托投资标的金融产品份额】读取融资总额
//		 String totalAmount = fd.findTextField(8,4);
//		 fd.verifyText("7304份", totalAmount);
		 //验证投资理财详细页--->投资管理合同：【定向委托投资标的成立日】读取资产成立日
		 String startDate = fd.findTextField(9,2);
		 fd.verifyText(dep_val_day2[0] + "年" + dep_val_day2[1] + "月" + dep_val_day2[2] + "日", startDate);
		 //验证投资理财详细页--->投资管理合同:【定向委托投资标的到期日】读取资产到期日
		 String endDate = fd.findTextField(9,4);
		 fd.verifyText(dep_matu_day2[0] + "年" + dep_matu_day2[1] + "月" + dep_matu_day2[2] + "日", endDate);
		 //验证投资理财详细页--->投资管理合同:【定向委托投资标的剩余存续期】读取项目投资期限
		 String investDate = fd.findTextField(10,2);
		 fd.verifyText(intervalDay+"天", investDate);
		//验证投资理财详细页--->投资管理合同:【定向委托投资标的预期收益率】读取产品利率
		 String proRate = fd.findTextField(10,4);
		 fd.verifyText("29.00%/年",proRate);
		 //验证投资理财详细页--->投资管理合同:【定向委托投资收益起始日】读取项目起息日
		 String profitStartdate= fd.findTextField(12,2);
		 fd.verifyText(value_date2[0] +"年" + value_date2[1] + "月" + value_date2[2] + "日",profitStartdate);
		//验证投资理财详细页--->投资管理合同:【定向委托投资收益到期日】读取项目到期日
		 String profitEnddate= fd.findTextField(12,4);
		 fd.verifyText(mute_date2[0] +"年" + mute_date2[1] + "月" + mute_date2[2] + "日",profitEnddate);
		 
		//验证投资理财详细页--->投资管理合同:【定向委托投资收益获得期】读取项目投资期限
		 String investDate1= fd.findTextField(13,2);
		 fd.verifyText(String.valueOf(intervalDay),investDate1);
		//验证投资理财详细页--->投资管理合同:【托管机构】读取开户行银行
		 String bankName= fd.findTextField(14,4);
		 fd.verifyText("中国工商银行五道口",bankName);
		//验证投资理财详细页--->投资管理合同:【甲方预期收益率】读取投人利率
		 String rate= fd.findTextField(15,4);
		 fd.verifyText("28.00%"+"/年",rate);
	}

}
