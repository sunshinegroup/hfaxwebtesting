package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 个人用户批量已还完的站内信  extends AccountBaseTest{

	/**
	 * @Description 个人用户批量已还完的站内信
	 * Created by songxq on 20/9/16.
	 */
	Map<String, String> envVars = System.getenv();
	
	@Test
	public void test001_个人用户已还完的站内信() {
		//点击我的账户
		this.driver.findElement(By.linkText("我的账户")).click();
				
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
			  @Override
			  public Boolean apply(WebDriver webDriver) {
			     return driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p")).isDisplayed();
			   }
		});
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			 this.selectMenuIE(4, "站内信");
		 }else{
			 this.selectMenu(4, "站内信");
		 }
		//等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[2]/td[1]")).isDisplayed();
	            }
	       });
		 assertTrue("惠聚财满标站内信状态不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[2]/td[1]").equals("未读"));
		 assertTrue("惠聚财满标标题不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[2]/td[3]/a").equals("项目还款"));
		 
		 //点击项目还款
		 this.driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[2]/td[3]/a")).click();
		 
		 //等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong")).isDisplayed();
	            }
	       });
	
		 //验证发件人
		 assertTrue("站内信发件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong").equals("管理员"));
		 //验证收件人
		 assertTrue("站内信收件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[2]/td[2]").equals(this.username));
		 //验证标题
		 assertTrue("站内信标题不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[3]/td[2]").equals("项目还款"));
		 //获取系统当前日期
		 Date dt=new Date();
	     SimpleDateFormat matter1=new SimpleDateFormat("yyyy-MM-dd");
	     System.out.println("系统当前日期为：" + matter1.format(dt));
		 String date = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[4]/td[2]").substring(0,10);
		 //验证日期
		 assertTrue("站内信日期不正确",date.equals(matter1.format(dt)));
		 
		 //验证内容姓名
		System.out.println("站内信的内容为："+ this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]"));
		assertTrue("项目还款内容不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").equals("尊敬的晓东先生,您投资的Yuetestproject001还款已经完成。应得本金部分为10000.00元，收益金额为64750元，合计本息为74750.00元，资金已到达您的惠金所账户，可立即用于投资，下一工作日12点后可发起提现。"));
		
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
			{
				this.selectMenuIE(4, "站内信");
			}else{
				this.selectMenu(4, "站内信");
			}					
		//等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[2]/td[1]")).isDisplayed();
	            }
	       });
		 assertTrue("惠聚财满标站内信状态不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[3]/td[1]").equals("未读"));
		 assertTrue("惠聚财满标标题不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[3]/td[3]/a").equals("项目还款"));
		 
		 //点击项目还款
		 this.driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[3]/td[3]/a")).click();
		 
		 //等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong")).isDisplayed();
	            }
	       });
	
		 //验证发件人
		 assertTrue("站内信发件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong").equals("管理员"));
		 //验证收件人
		 assertTrue("站内信收件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[2]/td[2]").equals(this.username));
		 //验证标题
		 assertTrue("站内信标题不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[3]/td[2]").equals("项目还款"));
		 //获取系统当前日期
		 Date dt1=new Date();
	     SimpleDateFormat matter2=new SimpleDateFormat("yyyy-MM-dd");
	     System.out.println("系统当前日期为：" + matter2.format(dt1));
		 String date1 = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[4]/td[2]").substring(0,10);
		 //验证日期
		 assertTrue("站内信日期不正确",date1.equals(matter2.format(dt1)));
		 
		 //验证内容姓名
		System.out.println("站内信的内容为："+ this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]"));
		assertTrue("项目还款内容不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").equals("尊敬的晓东先生,您投资的Yuetestproject002还款已经完成。应得本金部分为10000.00元，收益金额为64750元，合计本息为74750.00元，资金已到达您的惠金所账户，可立即用于投资，下一工作日12点后可发起提现。"));
	}

}
