package SecondStage.Hfax;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.GFFinanceListPage;
/**
 *  
 * @Description 固定浮动收益列表页的展示
 * Created by songxq on 9/11/16.
 * 1.已登录用户进入固浮列表页
 * 参照UI，网站右上角显示：快速注册、用户名
 * 
 */
public class ListPage_002 extends BaseTest{

	private GFFinanceListPage gflistpage;
	@Override
	@Before
	public void setup() throws Exception {
		super.setup();
		gflistpage = new GFFinanceListPage(driver,wait);
	}

	@Test
	public void test() {
		this.loadPage();
//		this.GFlogin();
		this.navigateToPage("定期理财");
		gflistpage.clickGFFinancelistButton("gfUser123","安全退出","toHomeInit.do","toLogoutInit.do");
	}

}
