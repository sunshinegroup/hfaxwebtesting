package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.DBquery;
import SecondStage.Hfax.Page.HuiLiCaiFinanceConfirmPage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceDetailPage;
import SecondStage.Hfax.Page.HuiLiCaiFinanceListPage;
import SecondStage.Hfax.Page.MyInsideLetterPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;
/**
 *  
 * @Description 投资惠理财(使用手动发券)
 * Created by songxq on 01/10/17.
 * 
 */
public class TestInvestHuiLiCaiAboutManualTickets extends BaseTest{

	private MyRedBagPage bagpage; //我的红包
	private MyInvestTicketPage ticket;//我的投资券
	private MyInsideLetterPage myletter;//站内信
	private MyQueryCashRecordListPage cashrecord; //充值提现
	private HuiLiCaiFinanceListPage hlc; //惠理财投资列表页
	private HuiLiCaiFinanceDetailPage fd; //惠理财投资详细页
	private HuiLiCaiFinanceConfirmPage cp; //惠理财投资确认页
	DBquery db = new DBquery();
	
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        bagpage = new MyRedBagPage(driver,wait);
        ticket = new MyInvestTicketPage(driver,wait);
        myletter = new MyInsideLetterPage(driver,wait);
        cashrecord = new MyQueryCashRecordListPage(driver,wait);
        hlc = new HuiLiCaiFinanceListPage(driver,wait);
        fd = new HuiLiCaiFinanceDetailPage(driver,wait);
        cp = new HuiLiCaiFinanceConfirmPage(driver,wait);
    }
	@Test
	public void testInvestHuiLiCaiAboutManualTickets() throws ClassNotFoundException, SQLException, InterruptedException,ArithmeticException{
		Map<String, String> envVars = System.getenv();
		String strPath = null;
		this.loadPage();
        this.HLClogin(this.huilicaiinvestUsername,this.huilicaiinvestPassword,this.huilicaiinvestPhone);
        //跳转到定期理财
//  		this.newnavigateToPage(2,"定期理财");
        this.loginTopNavigationMenu1(3,"定期理财");
  		//跳转到惠理财
 		this.clickOnSubmenu("惠理财");
 		//等待页面加载
		fd.waitPage(".//*[@id='display_7']/div/div[2]", 5);
		String proInvestName;
		String proName;
		 while(true){ 
			 this.clickOnSubmenu("惠理财");
			 proName = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_name from sunif.sif_project_info where subject_name like '惠理财-自动化投资-惠-周%' and intr_year='28'","subject_name");
			 System.out.println("惠理财-惠-自动化投资-周的名称为："+ proName);
			 Thread.sleep(2000);
			if(proName != null&&!proName.equals("")){ 
				this.clickOnSubmenu("惠理财");
			break;
			}
		 }
		 proInvestName = db.queryData(this.dbUrl,this.dbUser,this.dbPassword,"select subject_name from sunif.sif_project_info where subject_name like '惠理财-自动化投资-惠-周%' and intr_year='28'","subject_name");
		 for(int i = 0; i < 100; i++){
				Set<String> set = new LinkedHashSet<String>();
				set.add(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[3]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[4]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[5]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[6]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[7]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[8]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[9]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[10]/div[1]/div[1]/a");
				set.add(".//*[@id='display_7']/div/div[11]/div[1]/div[1]/a");
				for(Object element: set){
				     if(this.getElementText(element.toString()).equals(proName)){
				    	 strPath = element.toString();
				         break;
				     }
				  }
				if(strPath != null && this.getElementText(strPath).equals(proName)){
					break;
				}else{
				((JavascriptExecutor)this.driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
				 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("下一页")));
				 this.driver.findElement(By.linkText("下一页")).click();
				 new WebDriverWait(this.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a")));
				 assertTrue("惠理财翻页后第一个产品没有显示",this.getElementText(".//*[@id='display_7']/div/div[2]/div[1]/div[1]/a")!=null);
				}
			 }
		 String strPathElement = strPath.substring(0,strPath.length() - 16);
		 System.out.println("截取后的为：" + strPathElement);
		 
		 String currentWindow = this.driver.getWindowHandle();
		 System.out.println("第一个窗口的信息：" + currentWindow);
		 hlc.clickButton(strPathElement+"/div[2]/div[2]/a");
		 
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
			 Thread.sleep(5000);
		 }
		 Set<String> handles = this.driver.getWindowHandles();//获取所有窗口句柄
		 System.out.println("当前窗口数为："+ handles.size());
		 Iterator<String> it = handles.iterator();
		 while (it.hasNext()) { 
			 String secondWindow = (String)(it.next());
			 System.out.println("窗口的信息为：" + secondWindow);
			 if (currentWindow.equals(secondWindow)){
				 continue;
    		}
		
		  driver.switchTo().window(secondWindow);//切换到新窗口
		  break;
		 }
		 //等待页面加载
		 fd.waitPage("//div[@id='content']/ul/li/strong//input[@id='_amount']", 5);
		 while(true){
			 if(IsElementPresent(By.xpath("//div[@id='content']/ul/li/strong//input[@id='_amount']"))){
				 break;
			 }
		 }		 
		 //验证项目金额10000元
		 fd.verifyInfo("//ul[@class='borrowBox']//li[@class='amount']//i//strong","10,000.00");
		 //验证预期年化收益率28.00% +0.50% 
		 fd.verifyInfo("//li[@class='interest']//i","28.00%");
		 //0.50%
		 fd.verifyInfo("//li[@class='interest']//i[@class='rateCount']","+0.50%");
		 
		 //输入投资金额
		 fd.inputInfo("//input[@id='_amount']", "10000");
		 
		 //等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//span[@id='interests']")).getText().length() > 0;
	            }
	        });
		//获取投资期限
		 String day = this.driver.findElement(By.xpath("//li[@class='term']/i/strong")).getText().trim();
		 
		 int investday =  Integer.parseInt(day);
		 float profitFirst = (float) (10000*0.28*investday/365);
		 String strProfitFirst = String.valueOf(profitFirst);
		 String newstrProfitFirst = strProfitFirst.substring(0,strProfitFirst.indexOf("."))+strProfitFirst.substring(strProfitFirst.indexOf("."),strProfitFirst.indexOf(".")+3);
		 
		 float profitSecond = (float) (10000*0.005*investday/365);
		 String strProfitSecond = String.valueOf(profitSecond);
		 String newstrProfitSecond = strProfitSecond.substring(0,strProfitSecond.indexOf("."))+strProfitSecond.substring(strProfitSecond.indexOf("."),strProfitSecond.indexOf(".")+3);
		 		 
		 DecimalFormat decimalFormat=new DecimalFormat(".00");
		 String profit = decimalFormat.format(Float.valueOf(newstrProfitFirst) + Float.valueOf(newstrProfitSecond));
		 fd.log("已经截取的小数点后的预期收益为："+profit);
	     
	     while(true){
	    	 if(IsElementPresent(By.xpath("//input[@id='agre']"))&&IsElementPresent(By.xpath("//span[@id='interests']"))){
	    		 break;
	    	 }
	     }
	    fd.verifyPageInfo("项目详细页","//span[@id='interests']", profit);
	     //勾选已经阅读并同意
	     fd.clickButton("//input[@id='agre']");
	     while(true){
	    	 if(IsElementPresent(By.xpath("//a[@id='btnsave']/i"))){
	    		 break;
	    	 }
	     }
	     //点击去投资
	     fd.clickButton("//a[@id='btnsave']/i");
	     
	     //等待页面加载,跳转到投资确认页
	     this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@id='besurebutton']//button[@class='besure besure_btn']")).isDisplayed();
	            }
	        });
	     
	     //验证惠理财投资确认页预期收益率
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 Thread.sleep(3000);
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return webDriver.findElement(By.xpath("//span[@id='yqsyl_']")).isDisplayed();
		            }
		        });
	    	 cp.verifyInfo("//span[@id='yqsyl_']","30.40%");
	     
	     }else{
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return webDriver.findElement(By.xpath("//span[@id='yqsyl_']")).getText().trim().equals("30.40%");
		            }
		        });
	    	 cp.verifyInfo("//span[@id='yqsyl_']","30.40%");
	    	 
	     }
	     
	     //验证投资期限(天)
	     cp.verifyInfo("//div[@class='item'][2]//span[2]",day+"天");
	 
	     //验证投资金额(元)
	     cp.verifyInfo("//div[@class='item'][3]//span[2]","¥10000.00");
	     
	     //验证默认勾选第一个代金券,金额为为26元
	     cp.verifyElementAttribute("//table[@id='table1']/tbody/tr[1]//td[@class='t1']//input","checked","true");
	     cp.verifyInfo("//table[@id='table1']/tbody/tr[1]/td[2]","26");
	     
	     //验证默认勾选第一个加息券，1.9%
	     cp.verifyElementAttribute("//table[@id='table1']/tbody/tr[1]//td[@class='t1']//input","checked","true");
	     cp.verifyInfo("//table[@id='table2']/tbody/tr/td[2]","1.9%");
	     
	     //验证惠理财投资项目名称
	     cp.verifyInfo("//div[@id='investtitle']","投资项目："+proInvestName);
	     
	     //验证实际投资本金10,000.00
	     cp.verifyInfo(".//*[@id='sjtzbj']","10,000.00");
	     
	     //验证总预期收益率
	     cp.verifyInfo("//span[@id='yqsyl']","30.40");
	     
	     //验证总预期收益=基础收益率+投资收益率+加息券收益率
	     //基础收益率
	     float profit1 = (float) (10000*0.28*investday/365);
	     String newprofit1 = String.valueOf(profit1); 
	     newprofit1=newprofit1.substring(0,newprofit1.indexOf("."))+newprofit1.substring(newprofit1.indexOf("."),newprofit1.indexOf(".")+3);
	     cp.log("基础收益率为：" + newprofit1);
	     //投资收益率
	     float profit2 = (float) (10000*0.005*investday/365);
	     String newprofit2 = String.valueOf(profit2);
	     newprofit2=newprofit2.substring(0,newprofit2.indexOf("."))+newprofit2.substring(newprofit2.indexOf("."),newprofit2.indexOf(".")+3);
	     cp.log("投资收益率为：" + newprofit2);
	     //加息券收益率
	     float profit3 = (float) (10000*0.019*investday/365);
	     String newprofit3 = String.valueOf(profit3);
	     newprofit3=newprofit3.substring(0,newprofit3.indexOf("."))+newprofit3.substring(newprofit3.indexOf("."),newprofit3.indexOf(".")+3);
	     cp.log("加息券收益率为：" + newprofit3);
	     DecimalFormat decimalFormat1=new DecimalFormat(".00");
		 String newtotalAmount = decimalFormat.format(Float.valueOf(newprofit1) + Float.valueOf(newprofit2)+ Float.valueOf(newprofit3));
		 fd.log("已经截取的小数点后的预期收益为："+newtotalAmount);
	     
	     //验证总预期收益
	     cp.verifyInfo("//span[@id='yqsy']",newtotalAmount);
	     
	     //实际支付金额为：10000-26
	     cp.verifyInfo("//font[@id='sjzfje']","9,974.00");
	     
	     //输入交易密码
	     cp.inputInfo("//input[@id='tpwdid']", this.huilicaiinvestJiaoyipassword);
	     
	     //点击确定投资
	     cp.clickButton("//button[@class='besure besure_btn']");
	     
	     //等待页面加载
	     this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@id='touziquan']/div[2]/b/a[1]")).isDisplayed();
	            }
	        });
	     
	     //点击pop窗口的弹出框
	     cp.clickButton("//div[@id='touziquan']/div[2]/b/a[1]");
	     
	     //等待页面加载
	     this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//span[@id='inveamount']")).isDisplayed();
	            }
	        });
	     //本次投资金额
	     cp.verifyInfo("//span[@id='inveamount']","10,000.00");
	     //本项目累计投资金额
	     cp.verifyInfo("//span[@id='investCount']","10,000.00");
	     //本项目累计投资预计收益
	     cp.verifyInfo("//span[@id='forpay']",newtotalAmount);
	     
	     //点击关闭
	     cp.clickButton("//div[@id='investSuccess']/div[2]/b/a");

	     //等待页面加载
	     this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@class='inputMoney']/i[@class='cursor']")).isDisplayed();
	            }
	        });
	     System.out.println("输出标的状态："+ this.driver.findElement(By.xpath("//div[@class='inputMoney']/i[@class='cursor']")).getText().trim());
	     //标的状态为满标或者还款中
	     if(this.driver.findElement(By.xpath("//div[@class='inputMoney']/i[@class='cursor']")).getText().trim().equals("已满标")
	    		 ||this.driver.findElement(By.xpath("//div[@class='inputMoney']/i[@class='cursor']")).getText().trim().equals("满标") || this.driver.findElement(By.xpath("//div[@class='inputMoney']/i[@class='cursor']")).getText().trim().equals("还款中")){
	    	 fd.log("标的为满标和还款中显示正确");
	     }else{
	    	 fd.log("标的状态不正确应该为满标或者还款中");
	    	 fail("标的状态不正确应该为满标或者还款中");
	     }
 
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.driver.findElement(By.xpath("//li[@id='_topMenu_account']/a")).click();
	     }else{
	    	 this.topNavigationMenu(5,"我的账户");
	     }
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return webDriver.findElement(By.xpath("//li[@id='myInvestment']/a")).isDisplayed();
		            }
		        });
	    	 //点击我的投资
	    	 this.driver.findElement(By.xpath("//li[@id='myInvestment']/a")).click();
	     }else{
	    	 this.leftNavigationMenu(2,"我的投资");
	     }
	     
	     //等待页面加载
	     this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr")).isDisplayed();
	            }
	        });
	   
	     boolean flag = false;
	     List<WebElement> count = this.driver.findElements(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr"));
	     for(int i = 2; i <= count.size(); i++){
	    	 if(this.driver.findElement(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+i+"]/td[1]//a")).getText().trim().equals(proInvestName)){
	    		 //验证我的投资：本金
	    	     fd.verifyInfo("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+i+"]/td[2]","10000.00");
	    	     //验证我的投资：收益
	    	     fd.verifyInfo("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+i+"]/td[3]",newtotalAmount);
	    		 flag = true;
	    		 break;
	    	 }
	     }
	     if(flag = false){
	    	 fd.log("我的账户->我的投资，没有找到投资的项目： 惠理财-自动化投资-惠-周");
	    	 fail("我的账户->我的投资，没有找到投资的项目： 惠理财-自动化投资-惠-周");
	     }
	       
	     this.loginTopNavigationMenu(5,"我的账户");
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	 //我的福利
			this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_touziquan']")).isDisplayed();
			     }
			 });
			//投资券
			this.driver.findElement(By.xpath("//a[@id='menu_touziquan']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			    @Override
			    public Boolean apply(WebDriver webDriver) {
			        return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
			    }
			});
        }else{
        	this.leftNavigationSubMenu(6,1,"投资券");
        }
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
				    @Override
				    public Boolean apply(WebDriver webDriver) {
				        return driver.findElement(By.xpath("//li[@id='used']")).isDisplayed();
				    }
				});
	    	 //已使用的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='used']")).click();
	     }else{
	    	 this.rightHfaxTabNavigationMenu(2,"已使用的投资券");
	     }
	     
	     //查找代金券
	     boolean djflag = false;
	     List<WebElement> count1 = this.driver.findElements(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr"));
	     for(int k = 2; k <= count1.size(); k++){
	    	 if(this.driver.findElement(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+k+"]/td[4]//a")).getText().trim().equals(proInvestName)
	    			 && this.driver.findElement(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+k+"]/td[1]")).getText().trim().equals("投资代金券")){
	    		//验证我的账户->我的福利->投资券->投资代金券（金额(元)/收益率(%)）
	    	     fd.verifyInfo("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+k+"]/td[2]","26.00");
	    	     //验证我的账户->我的福利->投资券->投资代金券（使用时间）
	    	     fd.verifyInfo("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+k+"]/td[3]",this.getDate(0));
	    		 djflag = true;
	    		 break;
	    		 }
	     }
	     if(djflag = false){
	    	 fd.log("我的账户->投资券->已使用的投资券，没有找到投资的项目： 惠理财-自动化投资-惠-周,投资代金券");
	    	 fail("我的账户->投资券->已使用的投资券，没有找到投资的项目： 惠理财-自动化投资-惠-周,投资代金券");
	     }
	     
	     
	     this.loginTopNavigationMenu(5,"我的账户");
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	 //我的福利
			this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_touziquan']")).isDisplayed();
			     }
			 });
			//投资券
			this.driver.findElement(By.xpath("//a[@id='menu_touziquan']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			    @Override
			    public Boolean apply(WebDriver webDriver) {
			        return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
			    }
			});
        }else{
        	this.leftNavigationSubMenu(6,1,"投资券");
        }

	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
				    @Override
				    public Boolean apply(WebDriver webDriver) {
				        return driver.findElement(By.xpath("//li[@id='used']")).isDisplayed();
				    }
				});
	    	 //已使用的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='used']")).click();
	     }else{
	    	 this.rightHfaxTabNavigationMenu(2,"已使用的投资券");
	     }
	     
	     
	     boolean jxflag = false;
	     List<WebElement> count2 = this.driver.findElements(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr"));
	     for(int m = 2; m <= count2.size(); m++){
	    	 if(this.driver.findElement(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+m+"]/td[4]//a")).getText().trim().equals(proInvestName)
	    			 && this.driver.findElement(By.xpath("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+m+"]/td[1]")).getText().trim().equals("投资加息券")){
	    		 //验证我的账户->我的福利->投资券->投资加息券（金额(元)/收益率(%)）
	    	     fd.verifyInfo("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+m+"]/td[2]","1.9%");
	    	     //验证我的账户->我的福利->投资券->投资加息券（使用时间）
	    	     fd.verifyInfo("//div[@class='detail_tab hfax-detail-tab']/table/tbody/tr["+m+"]/td[3]",this.getDate(0));
	    		 jxflag = true;
	    		 break;
	    	 }
	     }
	     if(jxflag = false){
	    	 fd.log("我的账户->投资券->已使用的投资券，没有找到投资的项目： 惠理财-惠-自动化投资-周,投资加息券");
	    	 fail("我的账户->投资券->已使用的投资券，没有找到投资的项目： 惠理财-惠-自动化投资-周,投资加息券");
	     }
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.driver.findElement(By.xpath("//a[@id='_topMenu_account']")).click();
	     }else{
	    	 this.loginTopNavigationMenu(5,"我的账户");
	     }
	     
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
        	 //我的福利
			this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_touziquan']")).isDisplayed();
			     }
			 });
			//投资券
			this.driver.findElement(By.xpath("//a[@id='menu_touziquan']")).click();
			this.wait.until(new ExpectedCondition<Boolean>() {
			    @Override
			    public Boolean apply(WebDriver webDriver) {
			        return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
			    }
			});
        }else{
        	this.leftNavigationSubMenu(6,1,"投资券");
        }
	     if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
				    @Override
				    public Boolean apply(WebDriver webDriver) {
				        return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
				    }
				});
	    	 //点击未使用的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='usable']")).click();
	     }else{
//	    	 this.newSelectTab(1, "未使用的投资券");
	    	 this.rightHfaxTabNavigationMenu(1,"未使用的投资券");
	     }
	     
	     //91元代金券,验证它的面值|可用于投资：惠理财 惠投资 惠聚财|有效期（当前日期，当前日期+4）|获得来源：投资成功|投资成功代金91元；5-10K,30-60d
	    ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","91",0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+this.getDate(0)+"~"+this.getDate(4),0);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：投资成功", 0);
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 0);
	    }else{
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 0);
	    }
	    //点击下一页
	    ticket.clickButton("//div[@class='page_list']//a[4]");
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            }
        });
	    
	    //9.2%加息券,验证它的面值|可用于投资：惠理财 惠投资 惠聚财|有效期（当前日期，当前日期+4）|获得来源：投资成功|投资成功加息9.2%；5-10K,30-60d
	    ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","9.2",3);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 3);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+this.getDate(0)+"~"+this.getDate(4),3);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：投资成功", 3);
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 3);
	    }else{
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 3);
	    }
	    //53元红包
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	  //等待页面加载
		    this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("//li[@id='payAndDraw']/a")).isDisplayed();
	            }
	        });
	    	//充值/提现
		    this.driver.findElement(By.xpath("//li[@id='payAndDraw']/a")).click();
	    }else{
	    	this.leftNavigationMenu(3,"充值/提现");
	    }
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th")).isDisplayed();
            }
        });
	    List<WebElement> fundrecord = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th")); 
	    cashrecord.verifyTableInfo(new String[] {"序号", "时间", "操作类型","备注","收入","支出","可用余额"},fundrecord,7);
	    List<WebElement> fundrecordfirst = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td"));
	    cashrecord.verifyTableInfo(new String[] {"1",this.getDate(0), "红包福利","投资发放红包","53.00","0.00"},fundrecordfirst,6);
	    List<WebElement> fundrecordsecond = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[3]/td"));
	    cashrecord.verifyTableInfo(new String[] {"2",this.getDate(0), "成功投资","投资了["+proInvestName+"]","0.00","9,974.00"},fundrecordsecond,6); 
	   
	    this.loginTopNavigationMenu(5,"我的账户");
	    //站内信的验证
	    for(int i = 2; i < 7; i++){
	    	if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    		//等待页面加载
	    		 this.wait.until(new ExpectedCondition<Boolean>() {
	    	            @Override
	    	            public Boolean apply(WebDriver webDriver) {
	    	                return driver.findElement(By.xpath("//li[@id='accountManage_menuItem']/a")).isDisplayed();
	    	            }
	    	        });
	    		//点击账户管理
	    		 this.driver.findElement(By.xpath("//li[@id='accountManage_menuItem']/a")).click();
	    		//等待页面加载
	    		 this.wait.until(new ExpectedCondition<Boolean>() {
	    	            @Override
	    	            public Boolean apply(WebDriver webDriver) {
	    	                return driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).isDisplayed();
	    	            }
	    	        });
	    		 //站内信
	    		 this.driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).click();
		    }else{
		    	
		    	this.leftNavigationSubMenu(7,2,"站内信");
		    }
		    //验证用户投资成功后，9.2%加息券，5.2%加息券，91代金券，51元代金券，53元红包站内信
		    myletter.waitPage(".//*[@id='biaoge']/table/tbody/tr["+i+"]/td[3]/a",5);
		    myletter.clickTitle(".//*[@id='biaoge']/table/tbody/tr["+i+"]/td[3]/a");
		    myletter.waitPage("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]",5);
		    if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("91元代金券")){
		    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","提醒：有您的投资券");
		    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.huilicaiinvestName+"女士,恭喜您获得91元代金券,"+this.getDate(0)+"至"+this.getDate(4)+"内有效,具体使用规则可登录惠金所官网或APP个人账户查看，欢迎及时使用，如需帮助可致电400-015-8800(9:00-21:00)。");
		    	
		    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("9.20%加息券")){
		    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","提醒：有您的投资券");
		 	    myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.huilicaiinvestName+"女士,恭喜您获得9.20%加息券,"+this.getDate(0)+"至"+this.getDate(4)+"内有效,具体使用规则可登录惠金所官网或APP个人账户查看，欢迎及时使用，如需帮助可致电400-015-8800(9:00-21:00)。");	 	    																			       																			 
		 	  
		    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("5.20%加息券")){
		    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","提醒：有您的投资券");
		    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.huilicaiinvestName+"女士,恭喜您获得5.20%加息券,"+this.getDate(0)+"至"+this.getDate(4)+"内有效,具体使用规则可登录惠金所官网或APP个人账户查看，欢迎及时使用，如需帮助可致电400-015-8800(9:00-21:00)。"); 	    
	
		    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("53元现金红包")){
		    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","红包福利");
		    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.huilicaiinvestName+"女士,恭喜您获得53元现金红包，可立即用于投资或发起提现，欢迎登录惠金所平台查看使用。");
		 	   
		    }else if(this.driver.findElement(By.xpath("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]")).getText().trim().contains("51元代金券")){
		    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//h3","提醒：有您的投资券");
		    	myletter.verifLetterInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.huilicaiinvestName+"女士,恭喜您获得51元代金券,"+this.getDate(0)+"至"+this.getDate(4)+"内有效,具体使用规则可登录惠金所官网或APP个人账户查看，欢迎及时使用，如需帮助可致电400-015-8800(9:00-21:00)。");
		 	   
		    }
		    else{
		    	fail("用户投资成功后，9.2%加息券，5.2%加息券，91代金券，51元代金券，53元红包站内信不正确");
		    }    		
		    }	

	    //点击下一页
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
    		//等待页面加载
    		 this.wait.until(new ExpectedCondition<Boolean>() {
    	            @Override
    	            public Boolean apply(WebDriver webDriver) {
    	                return driver.findElement(By.xpath("//li[@id='accountManage_menuItem']/a")).isDisplayed();
    	            }
    	        });
    		//点击账户管理
    		 this.driver.findElement(By.xpath("//li[@id='accountManage_menuItem']/a")).click();
    		//等待页面加载
    		 this.wait.until(new ExpectedCondition<Boolean>() {
    	            @Override
    	            public Boolean apply(WebDriver webDriver) {
    	                return driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).isDisplayed();
    	            }
    	        });
    		 //站内信
    		 this.driver.findElement(By.xpath("//dl[@id='insideLetter']/a")).click();
	    }else{
	    	
	    	this.leftNavigationSubMenu(7,2,"站内信");
	    }
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='page_list']//a[3]")).isDisplayed();
            }
        });
	    cp.clickButton("//div[@class='page_list']//a[3]");
	    Thread.sleep(2000);
	    //等待页面加载
	     this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[2]/td[3]/a")).isDisplayed();
	            }
	        });
	    myletter.verifLetterInfo(".//*[@id='biaoge']/table/tbody/tr[2]/td[3]/a","投资申请成功");
	    myletter.clickTitle(".//*[@id='biaoge']/table/tbody/tr[2]/td[3]/a");
	    System.out.println("添加项目标识："+proInvestName);
    	myletter.verifyInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","尊敬的"+this.huilicaiinvestName+"女士");
    	myletter.verifyInfo("//div[@id='_accountContainer']//div[@class='letter_detail']//p[2]","您已成功提交一笔金额为10000元的"+proInvestName+"项目投资申请，项目成立后将立即通知您，敬请关注。现在邀请好友一起理财，更有邀请专享加息、现金奖励等您赚！");

	   
	    
	} 
	

}
