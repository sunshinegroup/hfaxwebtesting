package SecondStage.Hfax;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hfax.selenium.essential.JiaoYiMiMa;

/**
 *  
 * @Description 惠投资用户交易密码
 * Created by songxq on 8/11/16.
 * 
 */

public class 惠理财用户交易密码 extends JiaoYiMiMa{

		//惠理财用户登录
		@Override
		protected void userLogin(){
			this.loadPage();
	        this.HLlogin();
		}
		//惠理财用户交易密码
		@Override
		protected String userJiaoyimima(){
			return this.hlJiaoyipassword;
		}

}
