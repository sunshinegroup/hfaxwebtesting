package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.regression.business.BusinessBaseTest;
/**
 *  
 * @Description 企业用户批量已还完的站内信
 * Created by songxq on 09/19/16.
 * 
 */
public class 企业用户批量已还完的站内信 extends BusinessBaseTest{
	Map<String, String> envVars = System.getenv();
	
	@Test
	public void 企业用户已还完的站内信() {
		//点击我的账户
				this.driver.findElement(By.linkText("我的账户")).click();
						
				//等待页面加载
				 this.wait.until(new ExpectedCondition<Boolean>() {
					  @Override
					  public Boolean apply(WebDriver webDriver) {
					     return driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[1]/div[2]/div[2]/ul/li[1]/p")).isDisplayed();
					   }
				});
	 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		{
			this.selectMenuIE(4, "站内信");
		}else{
			this.selectMenu(4, "站内信");
		}					
		
		//等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[2]/td[1]")).isDisplayed();
	            }
	       });
		 assertTrue("企业用户批量还款后站内信状态不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[2]/td[1]").equals("未读"));
		 assertTrue("企业用户批量还款后标题不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[2]/td[3]/a").equals("成功还款"));
		 
		 //点击成功还款
		 this.driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[2]/td[3]/a")).click();
		 
		 //等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong")).isDisplayed();
	            }
	       });
	
		 //验证发件人
		 assertTrue("站内信发件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong").equals("管理员"));
		 //验证收件人
		 assertTrue("站内信收件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[2]/td[2]").equals(this.businessUsername));
		 //验证标题
		 assertTrue("站内信标题不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[3]/td[2]").equals("成功还款"));
		 //获取系统当前日期
		 Date dt=new Date();
	     SimpleDateFormat matter1=new SimpleDateFormat("yyyy-MM-dd");
	     System.out.println("系统当前日期为：" + matter1.format(dt));
		 String date = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[4]/td[2]").substring(0,10);
		 //验证日期
		 assertTrue("站内信日期不正确",date.equals(matter1.format(dt)));
		 
		 //验证内容姓名
		 String username = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").substring(0,14);
		 System.out.println("用户姓名为:" + username);
		 assertTrue("站内信用户姓名不正确",username.equals("尊敬的liang100010"));
		 //验证项目内容
		 String proContent = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").substring(26,this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").length());
		 System.out.println("项目内容为:" + proContent);
		 assertTrue("站内信项目内容不正确",proContent.equals("您针对的标的为：Yuetestproject001的借款已还款￥74750.00元。"));
		 
		 if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
			{
				this.selectMenuIE(4, "站内信");
			}else{
				this.selectMenu(4, "站内信");
			}					
		 
		//等待页面加载
		this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[2]/td[1]")).isDisplayed();
		            }
		       });
			 assertTrue("企业用户批量还款后站内信状态不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[3]/td[1]").equals("未读"));
			 assertTrue("企业用户批量还款后标题不正确",this.getElementText(".//*[@id='biaoge']/table/tbody/tr[3]/td[3]/a").equals("成功还款"));
			 
			 //点击成功还款
			 this.driver.findElement(By.xpath(".//*[@id='biaoge']/table/tbody/tr[3]/td[3]/a")).click();
			 
			 //等待页面加载
			 this.wait.until(new ExpectedCondition<Boolean>() {
		            @Override
		            public Boolean apply(WebDriver webDriver) {
		                return driver.findElement(By.xpath(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong")).isDisplayed();
		            }
		       });
		
			 //验证发件人
			 assertTrue("站内信发件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[1]/td[2]/strong").equals("管理员"));
			 //验证收件人
			 assertTrue("站内信收件人不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[2]/td[2]").equals(this.businessUsername));
			 //验证标题
			 assertTrue("站内信标题不正确",this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[3]/td[2]").equals("成功还款"));
			 //获取系统当前日期
			 Date dt1=new Date();
		     SimpleDateFormat matter2=new SimpleDateFormat("yyyy-MM-dd");
		     System.out.println("系统当前日期为：" + matter1.format(dt1));
			 String date1 = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[4]/td[2]").substring(0,10);
			 //验证日期
			 assertTrue("站内信日期不正确",date.equals(matter1.format(dt)));
			 
			 //验证内容姓名
			 String username1 = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").substring(0,14);
			 System.out.println("用户姓名为:" + username1);
			 assertTrue("站内信用户姓名不正确",username1.equals("尊敬的liang100010"));
			 //验证项目内容
			 String proContent1 = this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").substring(26,this.getElementText(".//*[@id='re_biaoge2']/table/tbody/tr[5]/td[2]").length());
			 System.out.println("项目内容为:" + proContent1);
			 assertTrue("站内信项目内容不正确",proContent1.equals("您针对的标的为：Yuetestproject002的借款已还款￥74750.00元。"));
	}

}
