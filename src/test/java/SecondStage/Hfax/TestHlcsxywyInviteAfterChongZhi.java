package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.hfax.selenium.base.BaseTest;

import SecondStage.Hfax.Page.HaxTotalManageSystemPage;
import SecondStage.Hfax.Page.MyInsideLetterPage;
import SecondStage.Hfax.Page.MyInvestTicketPage;
import SecondStage.Hfax.Page.MyQueryCashRecordListPage;
import SecondStage.Hfax.Page.MyRedBagPage;

public class TestHlcsxywyInviteAfterChongZhi extends BaseTest{

	/**
	 *  
	 * @Description 惠理财寿险业务员【邀请的用户】充值后的验证
	 * eg.充值提现-资金记录,我的投资券,我的红包
	 * Created by songxq on 12/16/16.
	 * 
	 * 
	 */		
	private MyRedBagPage bagpage; //我的红包
	private MyInvestTicketPage ticket;//我的投资券
	private MyInsideLetterPage myletter;//站内信
	private MyQueryCashRecordListPage cashrecord; //充值提现
	
	@Override
	@Before
	 public void setup() throws Exception {
        super.setup();
        bagpage = new MyRedBagPage(driver,wait);
        ticket = new MyInvestTicketPage(driver,wait);
        myletter = new MyInsideLetterPage(driver,wait);
        cashrecord = new MyQueryCashRecordListPage(driver,wait);
    }
	@Test
	public void test_AfterRechargeInfo() throws InterruptedException {
		Map<String, String> envVars = System.getenv();
		this.loadPage();
        this.HLClogin(this.hlcsxywyInviteUsername,this.hlcsxywyInvitePassword,this.hlcsxywyInvitePhone);
        this.loginTopNavigationMenu(5,"我的账户");

        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='payAndDraw']/a")).isDisplayed();
	             } });
	    	//点击充值提现
	    	this.driver.findElement(By.xpath("//li[@id='payAndDraw']/a")).click();
	    	
	    }else{
	    	this.leftNavigationMenu(3,"充值/提现");
	    }
        //等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th")).isDisplayed();
            }
        });
		List<WebElement> fundrecord = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[1]/th"));
		cashrecord.verifyTableInfo(new String[] {"序号", "时间", "操作类型","备注","收入","支出","可用余额"},fundrecord,7);
	    List<WebElement> fundrecordfirst = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[3]/td"));
	    cashrecord.verifyTableInfo(new String[] {"2",this.getDate(0), "充值成功","充值成功,成功充值[10000.00元]","10,000.00","0.00"},fundrecordfirst,6);
	    List<WebElement> fundrecordSecond = this.driver.findElements(By.xpath(".//*[@id='fundRecord']/table/tbody/tr[2]/td"));
	    cashrecord.verifyTableInfo(new String[] {"1",this.getDate(0), "红包福利","投资发放红包","43.00","0.00"},fundrecordSecond,6);
	    
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
         	 //我的福利
  			this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
  			this.wait.until(new ExpectedCondition<Boolean>() {
  			     @Override
  			     public Boolean apply(WebDriver webDriver) {
  			         return driver.findElement(By.xpath("//a[@id='menu_touziquan']")).isDisplayed();
  			     }
  			 });
  			//投资券
  			this.driver.findElement(By.xpath("//a[@id='menu_touziquan']")).click();
  			this.wait.until(new ExpectedCondition<Boolean>() {
  			    @Override
  			    public Boolean apply(WebDriver webDriver) {
  			        return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
  			    }
  			});
         }else{
        	 this.leftNavigationSubMenu(6,1,"投资券");
         }
		ticket.verifyTicketInfo("13","13","0","0");
	    //获取当前日期
		String getCurrentDate = this.getDate(0);
	    //把当前日期加4天
	    String getDateFour = this.getDate(4);
		 //新增41元代金券面值|可用于投资：惠理财 惠投资 惠聚财|有效期|获得来源：首次充值|使用条件：充值代金41元；5-10K,30-60d
		ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","41",1);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 1);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 1);
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：首次充值", 1);
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 1);
	    }else{
	    	ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 1);
	    }
	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用充值代金41元；5-10K,30-60d",1);
	    
	    //点击下一页
	    ticket.clickButton("//div[@class='page_list']//a[4]");
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
            }
        });
	    //新增4.2%加息券面值|可用于投资：惠理财 惠投资 惠聚财|有效期|获得来源：首次充值|使用条件：充值加息4.2%；5-10K,30-60d
  		ticket.verifyCouponDetailInfo("//p[@class='hfax-use-money right']/b","4.2",2);
  	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[1]","投资范围： 惠理财 惠投资 惠聚财", 2);
  	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[2]","有效期："+getCurrentDate+"~"+getDateFour, 2);
  	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[3]","获得来源：首次充值", 2);
  	  if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
  	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 2);
  	  }else{
  		 ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[5]","使用条件："+"\n"+"投资金额5000元至1万元"+"\n"+"产品期限30天至60天", 2);
  	  }
  	    ticket.verifyCouponDetailInfo("//div[@class='hfax-overtime-info']/p[4]","使用限制：新手专享产品不可使用充值加息4.2%；5-10K,30-60d", 2);
  	    
  	  if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	    	 this.wait.until(new ExpectedCondition<Boolean>() {
	             @Override
	             public Boolean apply(WebDriver webDriver) {
	                 return driver.findElement(By.xpath("//li[@id='usable']")).isDisplayed();
	             } });
	    	//点击未使用的投资券
	    	 this.driver.findElement(By.xpath("//li[@id='usable']")).click();
	    }else{
	    	this.rightHfaxTabNavigationMenu(1,"未使用的投资券");
	    }
	    //等待页面加载
	    this.wait.until(new ExpectedCondition<Boolean>() {
          @Override
          public Boolean apply(WebDriver webDriver) {
              return driver.findElement(By.xpath("//div[@class='hfax-use-top clearfix']")).isDisplayed();
          }
      });
	        
	    //我的红包
	    if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie")){
	       	 
   		    //我的福利
        	this.driver.findElement(By.xpath("//li[@id='welfare_menuItem']")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
			     @Override
			     public Boolean apply(WebDriver webDriver) {
			         return driver.findElement(By.xpath("//a[@id='menu_redPacket']")).isDisplayed();
			     }
			 });
        	//红包
        	this.driver.findElement(By.xpath("//a[@id='menu_redPacket']")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
  			     @Override
  			     public Boolean apply(WebDriver webDriver) {
  			         return driver.findElement(By.xpath("//li[@id='red']")).isDisplayed();
  			     }
  			 });	
            
	    	}else{
	    		this.leftNavigationSubMenu(6,2,"红包");
	    	}
	    List<WebElement> table = this.driver.findElements(By.xpath("//tbody/tr[1]/th"));
	    bagpage.verifyTableInfo(new String[] {"红包金额", "获取来源", "获得时间","发放时间","状态"},table);
	    List<WebElement> tablefirst = this.driver.findElements(By.xpath("//tbody/tr[2]/td"));
	    bagpage.verifyTableInfo(new String[] {"￥43.00", "首次充值",this.getDate(0),this.getDate(0),"已发放"},tablefirst);
			
	}

}
