package SecondStage.Hfax;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.hfax.selenium.base.SuddenDeathBaseTest;
import com.hfax.selenium.regression.account.AccountBaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 企业用户绑卡 extends SuddenDeathBaseTest{

	/**
	 *  
	 * @Description 企业用户绑卡
	 * Created by songxq on 09/14/16.
	 * 
	 */
	//企业用户银行卡号
	protected String businessBankCard(){
		return this.businessBankCard;
	}
	//企业用户交易密码
	protected String businessPassword(){
		return this.businessPassword;
	}
	 @Override
	    public void setup() throws Exception {
	        super.setup();
	        this.loadPage();
	        this.businessLogin(); //企业用户登录
	        
	    }

	@Test
	public void test001_企业用户绑卡() {
	Map<String, String> envVars = System.getenv();
		 //点击我的账户
		this.navigateToPage("我的账户");
      
        //等待页面加载
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='index5']/a")).isDisplayed();
            }
        });
        //IE菜单选择
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
		 {
			 this.selectMenuIE(2,"充值提现");
		 }
        else{
        	
        	this.selectMenu(2, "充值提现");
        }
		  //等待页面加载
				 this.wait.until(new ExpectedCondition<Boolean>() {
			            @Override
			            public Boolean apply(WebDriver webDriver) {
			                return driver.findElement(By.xpath("//div[@class='tabtil']/ul/li[2]")).isDisplayed();
			            }
			        });
        //点击充值
		this.driver.findElement(By.xpath("//div[@class='tabtil']/ul/li[2]")).click();
		//等待绑卡提示框
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='sure_btn']")));
		  if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("ie"))
			 {
			  	this.driver.findElement(By.xpath("//a[@class='sure_btn']")).click();
			 }
		 else
		{
			 if(driver.getPageSource().contains("请您先绑定银行卡！")){
		
						//点击立即绑定
			this.driver.findElement(By.xpath("//a[@class='sure_btn']")).click();
			
		}else{
			 fail("企业用户绑卡提示信息不正确");
		}
		}
		//等待页面加载
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='addbank']")));
		
		//验证银行户名
		assertTrue("银行户名不存在：",this.driver.findElement(By.xpath(".//*[@id='cardUserName1']")).getText().trim().length() > 0);
		
		//下拉框选择中国的建设银行
		 Select selectBank = new Select(this.driver.findElement(By.xpath(".//*[@id='bankName2_']")));  
		 selectBank.selectByIndex(1);  
		
		 //输入企业用户银行卡号
		//等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return driver.findElement(By.xpath("//div[@class='boxmain2']")).isDisplayed();
	            }
	        });
		this.driver.findElement(By.id("bankCard2")).clear();
		System.out.println("企业用户银行卡号：" + businessBankCard());
		this.driver.findElement(By.id("bankCard2")).sendKeys(businessBankCard());
		 
		 //输入企业用户交易密码
		 this.driver.findElement(By.id("jiaoyimima_")).clear();
		 System.out.println("企业用户交易密码：" + businessPassword());
		 this.driver.findElement(By.id("jiaoyimima_")).sendKeys(businessPassword());
		 
		 //点击提交
		 this.driver.findElement(By.xpath(".//*[@id='addbank']")).click();
		 
		 
		 //等待页面加载
		 this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='checkSure']")));
		 if(driver.getPageSource().contains("银行卡绑定")){
			 //点击确定
			 this.driver.findElement(By.xpath(".//*[@id='checkSure']")).click();
		 }else{
			 
			 fail("企业用户绑卡后提示信息不正确");
		 }
		 //等待页面加载
		 this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                return webDriver.findElement(By.id("checkSuccess")).isDisplayed();
	            }
	       });
		assertTrue("企业用户绑卡失败",this.driver.findElement(By.id("checkSuccess")).isDisplayed());
	}

}
