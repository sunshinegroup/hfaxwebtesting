package SecondStage.Hfax;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hfax.selenium.regression.account.AccountBaseTest;

public class 投资惠投资标的50次 extends AccountBaseTest {

	//QA2环境：http://10.10.92.48:18082
	//等待实际支付金额和投资金额需要注意,此次投资金额为100。00元
	@Test
	public void test001_投资惠投资标的50次(){

		for(int i = 0; i < 50; i++)
		{this.navigateToPage("新手专区");
		//点击惠投资
		WebElement content = this.clickOnSubmenu("惠投资");
		
	    WebElement firstItem = content.findElements(By.className("listBox-Info")).get(0);
	    new WebDriverWait(this.driver,8).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='display_6']/div/div[2]")));
	    //点击惠投资第一个产品
	    assertTrue("惠投资第一个产品不存在", this.driver.findElement(By.xpath("//div[@id='display_6']/div/div[2]")).isDisplayed());
	    firstItem.findElement(By.xpath("i/div/div[2]/a")).click();
	    //等待金额输入
	    this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='amount']")));
	    
	    WebElement input = this.driver.findElement(By.xpath("//input[@id='amount']"));
        input.clear();
        input.sendKeys("100");
        
        //勾选已阅读并同意《定向委托投资管理协议》
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='agre']")));
        assertTrue("《定向委托投资管理协议》不存在",this.driver.findElement(By.xpath("//input[@id='agre']")).isDisplayed());
        this.driver.findElement(By.xpath("//input[@id='agre']")).click();
        
        //点击去投资
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@id='btnsave']/i")));
        this.driver.findElement(By.xpath("//a[@id='btnsave']/i")).click();
        
        //等待实际支付金额
        this.wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				String text = driver.findElement(By.xpath(".//*[@id='sjzfje']")).getText();
				return text.trim().contains("100.00");
			}
		});
      
        
        //投资红包
        if(this.IsElementPresent(By.xpath("//div[@id='usecoupon']/div/input[1]")))
        {
        	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='usecoupon']/div/input[1]")));
        	this.driver.findElement(By.xpath("//div[@id='usecoupon']/div/input[1]")).click();
        	this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    return webDriver.findElement(By.xpath(".//*[@id='tpwdid']")).isDisplayed();
                }
            });
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).clear();
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
        	}
        else{
        	this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElement(By.xpath(".//*[@id='tpwdid']")).isDisplayed();
            }
        });
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).clear();
        	this.driver.findElement(By.xpath(".//*[@id='tpwdid']")).sendKeys(this.jiaoyipassword);
        }
        
       
          this.driver.findElement(By.xpath(".//*[@id='besureInvest']")).click();
        
          this.wait.until(new ExpectedCondition<Boolean>() {
              @Override
              public Boolean apply(WebDriver webDriver) {
                  return webDriver.findElement(By.xpath(".//*[@id='investSuccess_new']")).isDisplayed();
              }
          });
          
          //等待查看申请
          this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='investSuccess_new']/p/a[1]")));
          
          //验证浏览更多项目
          assertTrue("惠投资成功后浏览更多项目",this.driver.findElement(By.xpath(".//*[@id='investSuccess_new']/p/a[1]")).isDisplayed());
          
          //验证查看申请
          assertTrue("惠投资成功后查看申请",this.driver.findElement(By.xpath(".//*[@id='investSuccess_new']/p/a[2]")).isDisplayed());
          
          //点击查看申请
          this.driver.findElement(By.xpath(".//*[@id='investSuccess_new']/p/a[2]")).click();
          
         //等待新手专区
          this.wait.until(new ExpectedCondition<Boolean>() {
              @Override
              public Boolean apply(WebDriver webDriver) {
                  return webDriver.findElement(By.xpath(".//*[@id='index7']/a")).isDisplayed();
              }
          });
         
          
		}
        
        
	}

}
