package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_被邀请人认证_加息 extends 卡券管理_注册_加息{
    @Override
    protected String name() {
        return "被邀请人认证加息";
    }
    @Override
    protected String value(){
    	return "8.2";
    }
    @Override
    protected String tip() {
        return "被邀请人认证加息8.2%；5-10K,30-60d";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='7']")).click();
    }

}
