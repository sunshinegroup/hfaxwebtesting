package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_投资_返现 extends 卡券管理_认证_返现{
    @Override
    protected String name() {
        return "投资返现";
    }
    @Override
    protected String value(){
    	return "53";
    }
    @Override
    protected String tip() {
        return "绑卡返现33元";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='4']")).click();
    }
}
