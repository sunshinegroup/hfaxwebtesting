package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_寿险_返现 extends 卡券管理_认证_返现{
    @Override
    protected String name() {
        return "寿险返现";
    }
    @Override
    protected String value(){
    	return "20";
    }
    @Override
    protected String tip() {
        return "寿险返现20元";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='9']")).click();
    }

}
