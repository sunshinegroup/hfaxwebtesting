package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_邀请人被邀投资_返现 extends 卡券管理_认证_返现{
    @Override
    protected String name() {
        return "邀请人被邀投资返现";
    }
    @Override
    protected String value(){
    	return "73";
    }
    @Override
    protected String tip() {
        return "请人被邀投资返现73元";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='6']")).click();
    }

}
