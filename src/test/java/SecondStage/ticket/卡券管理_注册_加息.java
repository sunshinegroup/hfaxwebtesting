package SecondStage.ticket;

import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

public class 卡券管理_注册_加息 extends ConsoleBaseTest{
	private void clickDownArrow(int tableNumber, int rowNumber, int columnNumber) {
        this.driver.findElement(By.xpath(".//*[@id='show_form']/table[" + tableNumber + "]/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
    }

    private WebElement findTextField(int tableNumber, int rowNumber, int columnNumber) {
        return this.driver.findElement(By.xpath(".//*[@id='show_form']/table[" + tableNumber + "]/tbody/tr[" + rowNumber +
                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
    }    
    protected String name() {
        return "注册加息";
    }
    protected String value(){
    	return "1.2";
    }
    protected String tip() {
        return "注册加息1.2%；5-10K,30-60d";
    }
    protected void type(){
    	this.driver.findElements(By.xpath("//div[@class='combobox-item' and @value='1']")).get(2).click();
    }
    
    @Test
    public void 加息() throws Exception {
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[5]/ul/li[4]/div/span[4]")));

        //点击卡券手动发送
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[5]/ul/li[1]/div/span[4]")).click();
        
        // 等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.linkText("增加")).getText();
                return text2.equalsIgnoreCase("增加");
            }
        });

        //点击增加
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        this.driver.findElement(By.linkText("增加")).click();
        
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='show_form']/table/tbody/tr[1]/td[3]")));
        String text2 = driver.findElement(By.xpath(".//*[@id='show_form']/table/tbody/tr[1]/td[3]")).getText().substring(0, 5);
        assertTrue("新增项目信息页面加载失败", text2.equalsIgnoreCase("投资券编号"));
        
        //投资券名称
        List<WebElement> ticket = this.driver.findElements(By.id("ticket_name"));
      	ticket.get(1).sendKeys(name());
        
        //投资券类型
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='show_form']/table[1]/tbody/tr[2]/td[4]/span/span/span")));
        WebElement textField = this.findTextField(1, 2, 4);
        this.clickDownArrow(1, 2, 4);
        textField = this.findTextField(1, 2, 4);
        textField.click();
        textField.sendKeys(Keys.ARROW_UP);
        textField.sendKeys(Keys.ENTER);
        
        //适用投资金额限制下限
        this.driver.findElement(By.id("min_invest")).sendKeys("5000");
        
        //发放条件分类
        this.clickDownArrow(1, 11, 2);
        textField = this.findTextField(1, 11, 2);
        textField.click();
        type();
        
        //投资券金额
        this.driver.findElement(By.id("ticket_rate")).sendKeys(value());
        
        //使用渠道
        this.clickDownArrow(1, 4, 2);
        textField = this.findTextField(1, 4, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ARROW_DOWN);
        this.driver.findElement(By.id("remark")).click();
        
        //备注
        this.driver.findElement(By.id("remark")).sendKeys(tip());
        
        //适用商品大类
        this.clickDownArrow(1, 4, 4);
        textField = this.findTextField(1, 4, 4);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ARROW_UP);
        textField.sendKeys(Keys.ARROW_UP);
        this.driver.findElement(By.id("remark")).click();
        
        //适用投资金额限制上限
        this.driver.findElement(By.id("max_invest")).sendKeys("10000");
        
        //适用产品期限下限
        this.driver.findElement(By.id("min_period")).sendKeys("30");
        
        //适用产品期限上限
        this.driver.findElement(By.id("max_period")).sendKeys("60");
        
        //发放开始时间
        this.clickDownArrow(1, 7, 2);
        textField = this.findTextField(1, 7, 2);
        textField.click();

        //确定日期
      	Date dNow = new Date();   //当前时间
      	Date dAfter = new Date();
      	Calendar calendar = Calendar.getInstance(); //得到日历
      	calendar.setTime(dNow);//把当前时间赋给日历
      	SimpleDateFormat sdf=new SimpleDateFormat("yyyy,M,d"); //设置时间格式
      	String defaultStartDate0 = sdf.format(dNow);    //格式化当天日期
      	String date = "//td[@abbr='" + defaultStartDate0 + "']";
      	System.out.println(date);
      	
      	this.driver.findElement(By.xpath(date)).click();
        
      	this.driver.findElement(By.id("remark")).sendKeys("");
      	
        //发放结束时间
        this.clickDownArrow(1, 8, 4);
        textField = this.findTextField(1, 8, 4);
        textField.click();

      	calendar.setTime(dNow);//把当前时间赋给日历
      	calendar.add(Calendar.DAY_OF_MONTH, +45);  //加45天
      	dAfter = calendar.getTime();   //得到45天后时间

      	String defaultStartDate = sdf.format(dAfter);    //格式化后40天
      	
      	String date2 = "//td[@abbr='" + defaultStartDate + "']";
      	List<WebElement> selectDate = this.driver.findElements(By.xpath("//div[@class = 'calendar-nextmonth']"));
        try{
	      	//选择下个月
	      	selectDate.get(1).click();
	
	      	this.driver.findElement(By.xpath(date2)).click();
        }catch(Exception e){
          	//选择下下个月
          	selectDate.get(1).click();

          	this.driver.findElement(By.xpath(date2)).click();	
        }
        
        this.driver.findElement(By.id("remark")).sendKeys("");
        
        //投资券生效时间类型
        this.clickDownArrow(1, 8, 2);
        textField = this.findTextField(1, 8, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //使用条件描述
        this.driver.findElement(By.id("use_condition")).sendKeys(tip());
        
        //投资券失效时间类型
        this.clickDownArrow(1, 9, 2);
        textField = this.findTextField(1, 9, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        this.driver.findElement(By.id("remark")).sendKeys("");
        
        //失效天数
        this.driver.findElement(By.id("invalid_days")).sendKeys("5");
        
        //是否开启状态
        this.clickDownArrow(1, 11, 4);
        textField = this.findTextField(1, 11, 4);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
       
        this.driver.findElement(By.id("remark")).sendKeys("");
        
        //点击保存
        this.driver.findElement(By.linkText("保存")).click();
        
        //点击确定
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath("html/body/div[19]/div[2]/div[2]")).getText();
                return text5.equalsIgnoreCase("操作成功");
            }
        });
        this.driver.findElement(By.linkText("确定")).click();
    }
}
