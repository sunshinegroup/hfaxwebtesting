package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_被邀请人认证_返现 extends 卡券管理_认证_返现{
    @Override
    protected String name() {
        return "被邀请人认证返现";
    }
    @Override
    protected String value(){
    	return "83";
    }
    @Override
    protected String tip() {
        return "被邀请人认证返现83元";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='7']")).click();
    }

}
