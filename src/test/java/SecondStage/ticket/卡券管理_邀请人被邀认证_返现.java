package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_邀请人被邀认证_返现  extends 卡券管理_认证_返现{
    @Override
    protected String name() {
        return "邀请人被邀认证返现";
    }
    @Override
    protected String value(){
    	return "63";
    }
    @Override
    protected String tip() {
        return "邀请人被邀认证返现63元";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='5']")).click();
    }
}
