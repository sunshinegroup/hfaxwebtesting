package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_绑卡_代金 extends 卡券管理_注册_代金{
    @Override
    protected String name() {
        return "绑卡代金";
    }
    @Override
    protected String value(){
    	return "31";
    }
    @Override
    protected String tip() {
        return "绑卡代金31元；5-10K,30-60d";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='10']")).click();
    }
    
}
