package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_充值_返现 extends 卡券管理_认证_返现{
    @Override
    protected String name() {
        return "充值返现";
    }
    @Override
    protected String value(){
    	return "43";
    }
    @Override
    protected String tip() {
        return "充值返现43元";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='3']/preceding-sibling::*[@value='2']")).click();
    }
}
