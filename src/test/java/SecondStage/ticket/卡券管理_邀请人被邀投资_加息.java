package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_邀请人被邀投资_加息 extends 卡券管理_注册_加息{
    @Override
    protected String name() {
        return "邀请人被邀投资加息";
    }
    @Override
    protected String value(){
    	return "7.2";
    }
    @Override
    protected String tip() {
        return "邀请人被邀投资加息7.2%；5-10K,30-60d";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='6']")).click();
    }

}
