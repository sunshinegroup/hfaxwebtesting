package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_被邀请人认证_代金 extends 卡券管理_注册_代金{
    @Override
    protected String name() {
        return "被邀请人认证代金";
    }
    @Override
    protected String value(){
    	return "81";
    }
    @Override
    protected String tip() {
        return "被邀请人认证代金81元；5-10K,30-60d";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='7']")).click();
    }

}
