package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_充值_代金  extends 卡券管理_注册_代金{
    @Override
    protected String name() {
        return "充值代金";
    }
    @Override
    protected String value(){
    	return "41";
    }
    @Override
    protected String tip() {
        return "充值代金41元；5-10K,30-60d";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='3']/preceding-sibling::*[@value='2']")).click();
    }
    
}
