package SecondStage.ticket;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

public class 卡券管理_注册_代金_14元  extends 卡券管理_注册_代金{
	@Override
    protected String value(){
    	return "14";
    }
	@Override
    protected String tip() {
        return "注册代金14元；5-10K,30-60d";
    }
}
