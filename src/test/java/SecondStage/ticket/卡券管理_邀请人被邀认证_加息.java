package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_邀请人被邀认证_加息 extends 卡券管理_注册_加息{
    @Override
    protected String name() {
        return "邀请人被邀认证加息";
    }
    @Override
    protected String value(){
    	return "6.2";
    }
    @Override
    protected String tip() {
        return "邀请人被邀认证加息6.2%；5-10K,30-60d";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='5']")).click();
    }
}
