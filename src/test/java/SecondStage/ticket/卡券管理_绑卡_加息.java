package SecondStage.ticket;

import org.openqa.selenium.By;


public class 卡券管理_绑卡_加息 extends 卡券管理_注册_加息{
    @Override
    protected String name() {
        return "绑卡加息";
    }
    @Override
    protected String value(){
    	return "3.2";
    }
    @Override
    protected String tip() {
        return "绑卡加息3.2%；5-10K,30-60d";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='10']")).click();
    }
    
}
