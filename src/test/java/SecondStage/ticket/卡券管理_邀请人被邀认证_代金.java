package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_邀请人被邀认证_代金 extends 卡券管理_注册_代金{
    @Override
    protected String name() {
        return "邀请人被邀认证代金";
    }
    @Override
    protected String value(){
    	return "61";
    }
    @Override
    protected String tip() {
        return "邀请人被邀认证代金61元；5-10K,30-60d";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='5']")).click();
    }
}
