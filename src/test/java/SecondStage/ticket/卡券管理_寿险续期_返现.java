package SecondStage.ticket;

import org.openqa.selenium.By;

public class 卡券管理_寿险续期_返现 extends 卡券管理_认证_返现{
    @Override
    protected String name() {
        return "寿险续期返现";
    }
    @Override
    protected String value(){
    	return "30";
    }
    @Override
    protected String tip() {
        return "寿险续期返现30元";
    }
    @Override
    protected void type(){
    	this.driver.findElement(By.xpath("//div[@value='3']/preceding-sibling::*[@value='11']")).click();
    }

}
