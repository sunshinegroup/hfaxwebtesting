package SecondStage.console_惠聚财;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;
//项目金额200W，起投金额100W，
public class 惠聚财_产品录入_起投大额标的  extends ConsoleBaseTest{
	private void clickDownArrow(int rowNumber, int columnNumber) {
        this.driver.findElement(By.xpath(".//*[@id='edit_table']/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
    }

    private WebElement findTextField(int rowNumber, int columnNumber) {
        return this.driver.findElement(By.xpath(".//*[@id='edit_table']/tbody/tr[" + rowNumber +
                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
    }

    protected String lookupName() {
        return "中关村1号";
    }
    Map<String, String> envVars = System.getenv();
    
    @Test
    public void 单个新增() throws Exception{
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[2]/div/span[4]")));
        
        //点击产品录入
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[2]/div/span[4]")).click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[1]/span[1]")));

        // 等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.linkText("单个新增")).getText();
                return text2.equalsIgnoreCase("单个新增");
            }
        });
        
        //点击增加
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        this.driver.findElement(By.linkText("单个新增")).click();
        
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='edit_table']/tbody/tr[1]/td[1]")));
        String text2 = driver.findElement(By.xpath(".//*[@id='edit_table']/tbody/tr[1]/td[1]")).getText();
        assertTrue("新增项目信息页面加载失败", text2.equalsIgnoreCase("产品类型："));
        
        //产品类型
        this.clickDownArrow(1, 2);
        WebElement textField = this.findTextField(1, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //产品风险评级
	    this.clickDownArrow(32, 2);
	    textField = this.findTextField(32, 2);
	    textField.click();
	    textField.sendKeys(Keys.ARROW_DOWN);
	    textField.sendKeys(Keys.ENTER);
	    
        //定向委托投资收益起始日----2017-01-02
        this.clickDownArrow(9, 3);
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("chrome")){
      	List<WebElement> selectDate = this.driver.findElements(By.xpath("//div[@class='calendar-title']"));
      	selectDate.get(0).click();
      	//选择年份
      	List<WebElement> selectDate1 = this.driver.findElements(By.xpath("//input[@class='calendar-menu-year']"));
      	selectDate1.get(0).clear();
      	selectDate1.get(0).sendKeys("2017");
      	//选择月份（1月份）
      	List<WebElement> selectDate2 = this.driver.findElements(By.xpath("//td[@class = 'calendar-menu-month']"));
      	selectDate2.get(0).click();
      	//选择日期（2号）
      	this.driver.findElement(By.xpath("//td[@class='calendar-day' and @abbr='2017,1,2']")).click();
      	        	
      }else{
      	List<WebElement> selectDate = this.driver.findElements(By.xpath("//div[@class='calendar-title']"));
      	selectDate.get(1).click();
      	//选择年份
      	List<WebElement> selectDate1 = this.driver.findElements(By.xpath("//input[@class='calendar-menu-year']"));
      	selectDate1.get(1).clear();
      	selectDate1.get(1).sendKeys("2017");
      	//选择月份（1月份）
      	List<WebElement> selectDate2 = this.driver.findElements(By.xpath("//td[@class = 'calendar-menu-month']"));
      	selectDate2.get(0).click();
      	//选择日期（2号）
      	this.driver.findElement(By.xpath("//td[@class='calendar-day' and @abbr='2017,1,2']")).click();
      	
      }
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
        this.driver.findElement(By.linkText("确定")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
    	
        //定向委托投资收益到期日----2017-12-01
        this.clickDownArrow(10, 3);
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("chrome")){
        	List<WebElement> selectDate3 = this.driver.findElements(By.xpath("//div[@class='calendar-title']"));
        	selectDate3.get(1).click();
        	//选择年份 
        	List<WebElement> selectDate1 = this.driver.findElements(By.xpath("//input[@class='calendar-menu-year']"));
        	selectDate1.get(1).clear();
        	selectDate1.get(1).sendKeys("2017");
        	//选择月份（12月份）
        	List<WebElement> selectDate5 = this.driver.findElements(By.xpath("//td[@class='calendar-menu-month']"));
        	for(int i = 0 ; i < selectDate5.size(); i++){
        		if(selectDate5.get(i).getText().equals("十二月")){
        			selectDate5.get(i).click();
        			break;
        			}
        		}
        	//选择日期（1号）
        	this.driver.findElement(By.xpath("//td[@class='calendar-day' and @abbr='2017,12,1']")).click();
        	
        }else{
        	List<WebElement> selectDate3 = this.driver.findElements(By.xpath("//div[@class='calendar-title']"));
        	selectDate3.get(2).click();
        	//选择年份 
        	List<WebElement> selectDate1 = this.driver.findElements(By.xpath("//input[@class='calendar-menu-year']"));
        	selectDate1.get(2).clear();
        	selectDate1.get(2).sendKeys("2017");
        	//选择月份（12月份）
//        	List<WebElement> selectDate5 = this.driver.findElements(By.xpath("//td[@class='calendar-menu-month']"));
        	List<WebElement> selectDate5 = this.driver.findElements(By.xpath("//td[contains(@class,'calendar-menu-month')]"));
        	for(int i = 0 ; i < selectDate5.size(); i++){
        		if(selectDate5.get(i).getText().equals("十二月")){
        			selectDate5.get(i).click();
        			break;
        			}
        		}
        	//选择日期（1号）
        	this.driver.findElement(By.xpath("//td[@class='calendar-day' and @abbr='2017,12,1']")).click();
        }
    	
    	//预期年化收益率
    	this.driver.findElement(By.id("invest_intr")).sendKeys("700");
    	
    	//宽限期（天）
    	this.driver.findElement(By.id("grace_period")).sendKeys("10");
    	
    	//产品名称 
    	this.driver.findElement(By.id("subject_name")).click();
    	
    	//等待最后收款日弹窗
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
        this.driver.findElement(By.linkText("确定")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
        
    	this.driver.findElement(By.id("subject_name")).sendKeys(yueProjectName);
    	
    	//起投金额（元） 
    	this.driver.findElement(By.id("min_amount")).sendKeys("1000000");
    	
    	//托管银行
        this.driver.findElement(By.id("deposit_bank")).sendKeys("南京银行珠江支行");
        
        //年化单位
        this.clickDownArrow(26, 2);
        textField = this.findTextField(26, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //递增金额
        this.driver.findElement(By.id("increase_amount")).sendKeys("100");
        
        //募集结束时间
        this.clickDownArrow(28, 2);
        if(envVars.containsKey("BROWSERTYPE") && envVars.get("BROWSERTYPE").equalsIgnoreCase("chrome")){
            List<WebElement> label1 = this.driver.findElements(By.xpath("//div[@class='calendar-nextmonth']"));
            label1.get(2).click();
            List<WebElement> label5 = this.driver.findElements(By.xpath("//div[@class='calendar-body']/table/tbody/tr[3]/td[2]"));
            label5.get(2).click();
        }else{
//            List<WebElement> label1 = this.driver.findElements(By.xpath("//div[@class='calendar-nextmonth']"));
//            label1.get(4).click();
            List<WebElement> label5 = this.driver.findElements(By.xpath("//div[@class='calendar-body']/table/tbody/tr[3]/td[2]"));
            label5.get(4).click();
        }
        this.driver.findElement(By.linkText("确定")).click();
        
        //还款企业
        this.driver.findElement(By.xpath(".//*[@id='s2id_repay_company']/a/span[2]/b")).click();
        this.driver.findElement(By.id("s2id_autogen1_search")).sendKeys(businessLegalName);
        this.driver.findElement(By.id("s2id_autogen1_search")).sendKeys(Keys.ENTER);
        
        //最大投资金额
        this.driver.findElement(By.id("max_amount")).sendKeys("1100000");
        
        //还款方式
        this.clickDownArrow(31, 2);
        textField = this.findTextField(31, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //项目金额（元）
        this.driver.findElement(By.id("subject_amount")).sendKeys("2000000");
        
        //投资人风险等级
        this.clickDownArrow(35, 2);
        textField = this.findTextField(35, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
        //产品代码 
        this.driver.findElement(By.id("product_cd")).sendKeys(yueCode);
        
        //是否促销 
        this.clickDownArrow(36, 2);
        textField = this.findTextField(36, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
       
        
        //投资方向介绍 
        this.driver.findElement(By.id("invest_produce")).sendKeys("自动化测试标的abc321");
        
        //产品托管费率 
        this.driver.findElement(By.id("commission_rate")).sendKeys("5");
        
        //产品管理费率
        this.driver.findElement(By.id("manager_rate")).sendKeys("5");
        
        //平台管理费率
        this.driver.findElement(By.id("platform_rate")).sendKeys("5");
        
        //技术服务费率
        this.driver.findElement(By.id("technical_rate")).sendKeys("5");
        
        //项目风险
        this.driver.findElement(By.id("project_risk")).sendKeys("autotesta321");
        
        //行业风险
        this.driver.findElement(By.id("sector_risk")).sendKeys("autotesta321");
        
        //是否支持转让
        this.clickDownArrow(57, 2);
        textField = this.findTextField(57, 2);
        textField.click();
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ARROW_DOWN);
        textField.sendKeys(Keys.ENTER);
        
//        //持X天可发起
//        this.driver.findElement(By.id("transfer_day")).sendKeys("10");
//        
//        //离到期X天不可发起
//        this.driver.findElement(By.id("not_transday")).sendKeys("10");
//        
//        //转让手续费
//        this.driver.findElement(By.id("transfer_fee")).sendKeys("5");
        
        //资产来源
        this.driver.findElement(By.id("asset_source")).sendKeys("autotest321");
        
        //出让企业
        this.driver.findElement(By.xpath(".//*[@id='s2id_sell_company']/a/span[2]/b")).click();
        this.driver.findElement(By.id("s2id_autogen2_search")).sendKeys(businessLegalName);
        this.driver.findElement(By.id("s2id_autogen2_search")).sendKeys(Keys.ENTER);
        
        //产品简称
        this.driver.findElement(By.id("short_name")).sendKeys(yueProjectName);
        
        //点击保存
        this.driver.findElement(By.linkText("保存")).click();
        
        //点击确定
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath("html/body/div[32]/div[2]/div[2]")).getText();
                return text5.equalsIgnoreCase("操作成功");
            }
        });
        this.driver.findElement(By.linkText("确定")).click();
    }
}
