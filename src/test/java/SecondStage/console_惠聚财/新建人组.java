package SecondStage.console_惠聚财;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

public class 新建人组 extends ConsoleBaseTest {
	@Test
    public void 新建人组() throws Exception {
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[1]/div/span[4]")));

        //点击人组管理
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[1]/div/span[4]")).click();

        // 等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
                return text2.equalsIgnoreCase("1");
            }
        });

        //点击新增人组
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        this.driver.findElement(By.linkText("新增人组")).click();
        
        //等待弹窗
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("add_tags_name")));
        
        //输入组名称
        this.driver.findElement(By.id("add_tags_name")).sendKeys("AutoTest");
        
        //输入用途备注
        this.driver.findElement(By.id("add_remark")).sendKeys("AutoTest");
        
        //点击提交
        this.driver.findElement(By.linkText("提交")).click();
        
        //弹窗确认
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
        this.driver.findElement(By.linkText("确定")).click();
//        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
        
        //弹窗确认
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("html/body/div[9]/div[2]/div[2]")));
        
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text2 = webDriver.findElement(By.xpath("html/body/div[9]/div[2]/div[2]")).getText();
                return text2.equalsIgnoreCase("操作已完成");
            }
        });
        this.driver.findElement(By.linkText("确定")).click();
        
	}
}
