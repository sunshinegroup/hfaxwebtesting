package SecondStage.console_惠聚财;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

public class 惠聚财_批量审核 extends ConsoleBaseTest{
	protected String lookupName() {
        return "PiLiangYue00";
    }
    @Test
    public void 惠聚财_批量审核() throws Exception {
    	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[3]/div/span[4]")));
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[3]/div/span[4]")).click();

        //等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
                return text2.equalsIgnoreCase("1");
            }
        });
        
        //查询项目
        this.driver.findElement(By.id("subject_name")).sendKeys(lookupName());
        this.driver.findElement(By.linkText("查询")).click();
        
        //勾选全部项目
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
        
        //点击批量通过
        this.driver.findElement(By.linkText("批量通过")).click();
        
        //点击确定 
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath("html/body/div[36]/div[2]/div[2]")).getText();
                return text5.equalsIgnoreCase("操作成功");
            }
        });
        this.driver.findElement(By.linkText("确定")).click();
    }
}
