package SecondStage.console_惠聚财;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;
import com.hfax.selenium.regression.console.惠聚财_产品上架;

public class 惠聚财_预热标上架 extends ConsoleBaseTest{
	 private void clickDownArrow(int rowNumber, int columnNumber) {
	        this.driver.findElement(By.xpath(".//*[@id='show_form']/table/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
	    }

	    private WebElement findTextField(int rowNumber, int columnNumber) {
	        return this.driver.findElement(By.xpath(".//*[@id='show_form']/table/tbody/tr[" + rowNumber +
	                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
	    }
	    
	    protected String lookupName() {
	        return "yuReYue001";
	    }
 @Test
 public void 惠聚财_批量上架() throws Exception {
 	 this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[4]/div/span[4]")));
 	 this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[4]/div/span[4]")).click();

	        //等待页面加载
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                driver.switchTo().defaultContent();
	                driver.switchTo().frame(0);
	                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
	                return text2.equalsIgnoreCase("1");
	            }
	        });
	        
	        //查询项目
	        this.driver.findElement(By.id("subject_name1")).sendKeys(lookupName());
	        this.driver.findElement(By.linkText("查询")).click();
	        
	        //勾选全部项目
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //设置上架信息
	        this.driver.findElement(By.linkText("设置上架信息")).click();
	        
	        //等待弹出页
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='show_form']/table/tbody/tr[1]/td[1]")));
	        
	        //发售渠道
	        this.clickDownArrow(5, 2);
	        WebElement textField = this.findTextField(5, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        this.driver.findElement(By.xpath(".//*[@id='addGoundInfo']/div/div/div[2]/div/div")).click();
	        
	        //预览时间
	        this.clickDownArrow(2, 2);
	        textField = this.findTextField(2, 2);
	        textField.click();
	        this.driver.findElement(By.xpath(".//span[@class='spinner']/input")).click();
	        List<WebElement> selectDate0 = this.driver.findElements(By.xpath(".//span[@class='spinner-arrow-up']"));
	        selectDate0.get(0).click();
	        selectDate0.get(0).click();
	        selectDate0.get(0).click();
	        this.driver.findElement(By.linkText("确定")).click();
	        
	        //上架时间
	        this.clickDownArrow(3, 2);
	      	List<WebElement> selectDate = this.driver.findElements(By.xpath("//div[@class='calendar-title']"));
	      	selectDate.get(1).click();
	      	//选择年份
	      	List<WebElement> selectDate1 = this.driver.findElements(By.xpath("//input[@class='calendar-menu-year']"));
	      	selectDate1.get(1).clear();
	      	selectDate1.get(1).sendKeys("2016");
	      	//选择月份（12月份）
	      	List<WebElement> selectDate2 = this.driver.findElements(By.xpath("//td[@abbr='12']"));
	      	selectDate2.get(0).click();
	      	//选择日期（20号）
	      	this.driver.findElement(By.xpath("//td[@abbr='2016,12,20']")).click();
	      	this.driver.findElement(By.linkText("确定")).click();
	        
	        //特定人组
	        this.driver.findElement(By.id("s2id_autogen2")).sendKeys("公共组");
	        this.driver.findElement(By.id("s2id_autogen2")).sendKeys(Keys.ENTER);
	        
	        //是否热卖
	        this.clickDownArrow(6, 2);
	        textField = this.findTextField(6, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ENTER);
	        
	        //点击提交
	        this.driver.findElement(By.linkText("提交")).click();
	        
	        //点击确定
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	String text5 = driver.findElement(By.xpath("html/body/div[15]/div[2]/div[2]")).getText();
	                return text5.equalsIgnoreCase("执行成功");
	            }
	        });
	        this.driver.findElement(By.linkText("确定")).click();	
 }
}
