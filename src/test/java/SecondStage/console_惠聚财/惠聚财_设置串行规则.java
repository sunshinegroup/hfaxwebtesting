package SecondStage.console_惠聚财;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

public class 惠聚财_设置串行规则 extends ConsoleBaseTest{

    protected String lookupName() {
        return "ChuanYue001";
    }
    Map<String, String> envVars = System.getenv();
    @Test
    public void 设置串行规则(){
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[3]/div/span[4]")));
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[3]/div/span[4]")).click();

        //等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
                return text2.equalsIgnoreCase("1");
            }
        });
        
        //检查目标项目位置
        String textSearch = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
        if (textSearch.compareTo(lookupName()) != 0) {
            //查询项目
            this.driver.findElement(By.id("subject_name")).sendKeys(lookupName());
            this.driver.findElement(By.linkText("查询")).click();

            this.wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
                    return text2.equalsIgnoreCase(lookupName());
                }
            });

            String text3 = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div")).getText();
            assertTrue("查询功能不正常", text3.equals(lookupName()));
        }
        
        //抓取产品编号
        String id = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[3]/div")).getText();
        int i = Integer.parseInt(id.substring(1,id.length()));
        
        //编辑xls
        String fileName = "chuan.xlsx";
        ClassLoader loader = ConsoleBaseTest.class.getClassLoader();
        File file = new File(loader.getResource(fileName).getFile());
        assertTrue("无法读取文件: " + fileName, file != null);

        String filePath = file.getAbsolutePath();
        assertTrue("文件路径为空: " + fileName, filePath.length() > 0);
        try {   
            XSSFWorkbook xwb = new XSSFWorkbook(new FileInputStream(filePath));  
  
	        XSSFSheet xSheet = xwb.getSheetAt(0);  
	        for(int j = 1; j < 10; j++){
	        String idx = "Z" + (i - 1 + j);
	        XSSFRow row = xSheet.getRow(j);   
	        XSSFCell cell = row.getCell(3);
	        
	        String str = cell.getStringCellValue();  
	        str = str.replace(str, idx);
	        
            cell.setCellType(XSSFCell.CELL_TYPE_STRING);   
            cell.setCellValue(str);     
	        }
  
            FileOutputStream out = new FileOutputStream(filePath);  
            xwb.write(out);  
            out.close();  
  
        } catch (Exception e) {  
            e.printStackTrace();  
        } 
        
        //关闭产品审核
        this.driver.switchTo().defaultContent();
        this.driver.findElement(By.xpath(".//*[@id='tabsContainer']/div[1]/div[3]/ul/li[2]/a[2]")).click();
        
        //点击设置串行规则
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[7]/div/span[4]")).click();
        
        //等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.linkText("上传新建串行")).getText();
                return text2.equalsIgnoreCase("上传新建串行");
            }
        });
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@class='datagrid-mask']")));
        //点击上传
        this.driver.findElement(By.linkText("上传新建串行")).click();
        
        //输入上传你路径
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("file_name")));
        WebElement uploadField = this.driver.findElement(By.id("file_name"));
        ((JavascriptExecutor)this.driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", uploadField);
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("file_name")));
        uploadField.clear();
        ((JavascriptExecutor)this.driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", this.driver.findElement(By.id("fileToUpload")));
        this.driver.findElement(By.id("fileToUpload")).sendKeys(filePath);
        this.driver.findElement(By.xpath(".//*[@id='fileup']/button")).click();
        
        //弹窗确认
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
            	String text5 = driver.findElement(By.xpath("html/body/div[11]/div[2]/div[1]")).getText();
                return text5.equalsIgnoreCase("文件上传成功");
            }
        });
        
        //点击确定
        this.driver.findElement(By.linkText("确定")).click();
    }
}
