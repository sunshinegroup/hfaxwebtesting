package SecondStage.console_惠聚财;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

public class 人组用户导入 extends ConsoleBaseTest{
	@Test
    public void 新建人组() throws Exception {
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[1]/div/span[4]")));

        //点击人组管理
        this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[1]/div/span[4]")).click();

        // 等待页面加载
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(0);
                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
                return text2.equalsIgnoreCase("1");
            }
        });
        
        //搜索用户组
        this.driver.findElement(By.id("tags_name")).sendKeys("AutoTest");
        this.driver.findElement(By.xpath(".//*[@id='qry']/span/span")).click();
        
        this.wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div/a")).getText();
                return text2.equalsIgnoreCase("AutoTest");
            }
        });

        String text3 = this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div/a")).getText();
        assertTrue("查询功能不正常", text3.equals("AutoTest"));
        
        //选择人组
        this.driver.findElement(By.xpath(".//*[@id='datagrid-row-r1-2-0']/td[2]/div/a")).click();
        
        //等待弹窗
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("人组用户导入")));
        
        //点击人组用户导入
        this.driver.findElement(By.linkText("人组用户导入")).click();
        
        //上传文件
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("file_name")));
        String fileName = "renzu.xlsx";
        ClassLoader loader = ConsoleBaseTest.class.getClassLoader();
        File file = new File(loader.getResource(fileName).getFile());
        assertTrue("无法读取文件: " + fileName, file != null);

        String filePath = file.getAbsolutePath();
        assertTrue("文件路径为空: " + fileName, filePath.length() > 0);
//        String filePath = "D:\\webdirver\\hfaxwebtesting\\src\\test\\Resources\\gf_kd3333.xlsx";
        
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.id("file_name")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("file_name")));
        WebElement uploadField = this.driver.findElement(By.id("file_name"));
        ((JavascriptExecutor)this.driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", uploadField);
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("file_name")));
        uploadField.clear();

        
        ((JavascriptExecutor)this.driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", this.driver.findElement(By.id("fileToUpload")));
        this.driver.findElement(By.id("fileToUpload")).sendKeys(filePath);
        this.driver.findElement(By.xpath(".//*[@id='fileup']/button")).click();
        
        //弹窗确认
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("确定")));
        this.driver.findElement(By.linkText("确定")).click();
        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("确定")));
	}
}
