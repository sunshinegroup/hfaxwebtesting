package SecondStage.console_惠聚财;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hfax.selenium.regression.console.ConsoleBaseTest;

public class 惠聚财_设置串行属性 extends ConsoleBaseTest{
	 private void clickDownArrow(int rowNumber, int columnNumber) {
	        this.driver.findElement(By.xpath(".//*[@id='pro_form']/table/tbody/tr[" + rowNumber + "]/td[" + columnNumber + "]//span[contains(@class, 'combo-arrow')]")).click();
	    }

	    private WebElement findTextField(int rowNumber, int columnNumber) {
	        return this.driver.findElement(By.xpath(".//*[@id='pro_form']/table/tbody/tr[" + rowNumber +
	                "]/td[" + columnNumber + "]//input[contains(@class, 'combo-text')]"));
	    }
	    
	    protected String lookupName() {
	        return "ChuanYue00";
	    }
	    @Test
	    public void 惠聚财_设置串行属性() throws Exception {
	    	this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[6]/div/span[4]")));
	    	this.driver.findElement(By.xpath(".//*[@id='mainmenutree']/li[9]/ul/li[6]/div/span[4]")).click();

	        //等待页面加载
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	                driver.switchTo().defaultContent();
	                driver.switchTo().frame(0);
	                String text2 = webDriver.findElement(By.xpath(".//*[@id='datagrid-row-r1-1-0']/td/div")).getText();
	                return text2.equalsIgnoreCase("1");
	            }
	        });
	        
	        //查询项目
	        this.driver.findElement(By.id("subject_name1")).sendKeys(lookupName());
	        this.driver.findElement(By.linkText("查询")).click();
	        
	        //勾选全部项目
	        this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='datagrid-mask']")));
	        this.driver.findElement(By.xpath(".//div[@class='datagrid-header-check']/input")).click();
	        
	        //点击设置串行产品属性
	        this.driver.findElement(By.linkText("设置串行产品属性")).click();
	        
	        //等待弹出页
	        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='pro_form']/table/tbody/tr[2]/td[2]")));
	        
	        //发售渠道
	        this.clickDownArrow(4, 2);
	        WebElement textField = this.findTextField(4, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ARROW_DOWN);
	        this.clickDownArrow(4, 2);
	        
	        //特定人组
	        this.driver.findElement(By.id("s2id_autogen2")).sendKeys("公共组");
	        this.driver.findElement(By.id("s2id_autogen2")).sendKeys(Keys.ENTER);
	        
	        //是否热卖
	        this.clickDownArrow(5, 2);
	        textField = this.findTextField(5, 2);
	        textField.click();
	        textField.sendKeys(Keys.ARROW_DOWN);
	        textField.sendKeys(Keys.ENTER);
	        
	        //点击提交
	        this.driver.findElement(By.linkText("提交")).click();
	        
	        //点击确定
	        this.wait.until(new ExpectedCondition<Boolean>() {
	            @Override
	            public Boolean apply(WebDriver webDriver) {
	            	String text5 = driver.findElement(By.xpath("html/body/div[13]/div[2]/div[2]")).getText();
	                return text5.equalsIgnoreCase("操作成功");
	            }
	        });
	        this.driver.findElement(By.linkText("确定")).click();	
	    }
	        
}
