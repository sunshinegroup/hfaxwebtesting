﻿
/*sunif删除订单信息：手机号，登录名，身份证*/

delete from SUNIF.SIF_BOOK sb
 where member_cd in (select member_cd
                       from SUNIF.SIF_MEMBER m
                      where m.tel_number = '&1'
                         or m.member_alias = '' || '&2'
                         or m.cert_no = '' || '&3');

--新手标2.SUNIF.SIF_SUBJECT_MEMBER
/*sunif删除标的投资人记录表：手机号，登录名，身份证  */
delete from SUNIF.SIF_SUBJECT_MEMBER ss
 where member_cd in (select member_cd
                       from SUNIF.SIF_MEMBER m
                      where m.tel_number = '&1'
                         or m.member_alias = '' || '&2'
                         or m.cert_no = '' || '&3');
--1.sunif.sif_member_detail
/*sunif 删除会员详细信息：手机号，登录名， 身份证 */
update sunif.sif_member_detail l
   set l.Corp_No     = dbms_random.string('', 10),
       l.BUSS_NO     = dbms_random.string('', 4),
       l.OPEN_PERMIT = dbms_random.string('', 4),
       l.CORPER_NAME = dbms_random.string('', 4),
       l.corp_name = dbms_random.string('', 4)
 where l.member_cd in (select member_cd
                         from SUNIF.SIF_MEMBER m
                        where m.member_type = 2
                          and m.tel_number = '&1' 
                           or m.member_alias = '' || '&2' 
                           or m.cert_no = '' || '&3'); 
update sunif.sif_member_detail_tmp l
   set l.Corp_No     = dbms_random.string('', 10),
       l.BUSS_NO     = dbms_random.string('', 4),
       l.OPEN_PERMIT = dbms_random.string('', 4),
       l.CORPER_NAME = dbms_random.string('', 4),
       l.corp_name = dbms_random.string('', 4)
 where l.member_cd in (select member_cd
                         from SUNIF.SIF_MEMBER m
                        where m.member_type = 2
                          and m.tel_number = '&1' 
                           or m.member_alias = '' || '&2' 
                           or m.cert_no = '' || '&3'); 

--2.sunif.sif_member_pwd
DELETE FROM  sunif.sif_member_pwd pd
 where pd.member_cd in (select member_cd
                          from SUNIF.SIF_MEMBER m
                         where m.member_type = 2
                           and m.tel_number = '&1'
                            or m.member_alias = '' || '&2'
                            or m.cert_no = '' || '&3');
--3./*删除登录手机号*/
delete from sunif.sif_member_login
 where member_cd in (select member_cd                     
                       from SUNIF.SIF_MEMBER m
                      where m.tel_number = '&1'
                         or m.member_alias = '' || '&2'
                         or m.cert_no = '' || '&3');

--4./*修改运营用户信息：手机、身份证*/
update SUNIF.SIF_MEMBER m
   set m.member_name=dbms_random.string('', 20),
       m.cert_no = dbms_random.string('', 20),
       m.tel_number   = '13000000000',
       m.member_alias = dbms_random.string('', 20)
 where m.tel_number = '&1'
 or m.member_alias = '' || '&2'
 or m.cert_no = '' || '&3';


commit;
exit
