update cbmain_user.kna_acct b
   set b.onlnbl = '&2'
 where b.custno = (select v.custno
                     from cbmain_user.cif_cust v
                    where v.custpt = '&1');

delete from cbmain_user.fca_acct d
 where d.custac = (select a.account_no
                     from sunif.sif_member a
                    where a.member_alias = '&1')and d.subjcd like 'Z16%';
commit;
exit