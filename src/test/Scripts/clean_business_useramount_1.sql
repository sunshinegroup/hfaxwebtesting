update cbmain_user.kna_acct b
   set b.onlnbl = '&2'
 where b.custno = (select v.custno
                     from cbmain_user.cif_corp v
                    where v.custpt = '&1');
                    
update cbmain_user.kna_acct b
   set b.onlnbl = '&3'
 where b.custno = (select v.custno
                     from cbmain_user.cif_corp v
                    where v.custpt = '&1');
commit;
exit