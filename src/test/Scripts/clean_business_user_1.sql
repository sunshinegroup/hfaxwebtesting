﻿
/*修改电子账号*/
delete cbmain_user.Kna_Cust kc
   where kc.custac in (select m.account_no
                       from SUNIF.SIF_MEMBER m
                      where m.member_type = 2
                        and m.tel_number = '&1'
                         or m.member_alias = '' || '&2'
                         or m.cert_no = '' || '&3');
/*删除银行卡信息*/
delete from cbmain_user.kna_cacd a                         
 where exists (select 1                                    
          from cbmain_user.cif_cust cc, cbmain_user.kna_cust kc      
         where cc.custno = kc.custno                       
           and kc.custac = a.custac                        
           and (cc.teleno = '&1'
            or cc.custpt = '' || '&2' 
            or cc.idtfno = '' || '&3'))
 	    or a.cardno=''||'&4';  

/*修改核心用户信息：手机、身份证*/
update cbmain_USER.CIF_CORP cc
   set cc.epcotl= dbms_random.string('', 11),
       cc.custpt= dbms_random.string('', 10),
       cc.cropcd= dbms_random.string('', 10),
       cc.idtfno = dbms_random.string('',20),
       cc.opcfno = dbms_random.string('',10) 
 where cc.epcotl = '&1'
    or cc.custpt = '' || '&2'
    or cc.idtfno = '' || '&3';

commit;
exit
