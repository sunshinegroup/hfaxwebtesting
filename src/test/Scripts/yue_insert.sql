insert into cbmain_user.fca_gdsk_repy m
  (m.corpno,
   m.subjcd,
   m.ygprod,
   m.trandt,
   m.custna,
   m.idtfno,
   m.bookcd,
   m.totlam,
   m.prinam,
   m.prciam,
   m.assetd,
   m.repydt,
   m.finsfg,
   m.allwfg,
   m.datetm,
   m.timetm)
  select '001',
         a.subjcd,
         a.wbprod,
         to_char(sysdate, 'yyyymmdd'),
         b.acctna,
         b.idtfno,
         b.acctno,
         b.ndrcpr + round(b.ndrcpr*substr(a.loantm,1,length(a.loantm)-1)*a.invsrt/c.inperi/100,2),
         b.ndrcpr,
         round(b.ndrcpr*substr(a.loantm,1,length(a.loantm)-1)*a.invsrt/c.inperi/100,2),
         a.jczcno,
         to_char(sysdate, 'yyyymmdd'),
         'N','N',
         to_char(sysdate, 'yyyymmdd'),
         (SYSDATE - TO_DATE('1970-1-1 8', 'YYYY-MM-DD HH24')) * 86400000
    from cbmain_user.fca_subj a, cbmain_user.fca_acct b, cbmain_user.fca_subj_addi c
   where a.subjcd = b.subjcd and a.subjcd = c.subjcd and a.fcppcd = '030010004' and a.wbprod in ('&1'); 
commit;
exit