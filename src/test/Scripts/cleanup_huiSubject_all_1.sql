delete from sunif.sif_subject t where t.subject_name like '&1%';
delete from sunif.sif_subject_channel t1
 where t1.subject_cd in (select t.subject_cd
                           from sunif.sif_subject t
                          where t.subject_name like '&1%');
delete from sunif.sif_subject_hexin_status t1
 where t1.subject_cd in (select t.subject_cd
                           from sunif.sif_subject t
                          where t.subject_name like '&1%');
delete from sunif.sif_subject_member t1
 where t1.subject_cd in (select t.subject_cd
                           from sunif.sif_subject t
                          where t.subject_name like '&1%');
delete from sunif.sif_subject_sched t1
 where t1.subject_cd in (select t.subject_cd
                           from sunif.sif_subject t
                          where t.subject_name like '&1%');
delete from sunif.sif_subject_sched t where t.schd_name = '&1%';
delete from sunif.sif_subject_sched_cmfr t1
 where t1.subject_cd in (select t.subject_cd
                           from sunif.sif_subject t
                          where t.subject_name like '&1%');
delete from sunif.sif_subject_serial t1
 where t1.subject_cd in (select t.subject_cd
                           from sunif.sif_subject t
                          where t.subject_name like '&1%');
delete from sunif.sif_subject_tags t1
 where t1.subject_cd in (select t.subject_cd
                           from sunif.sif_subject t
                          where t.subject_name like '&1%');
delete from cbmain_user.fca_subj t where t.lntitl like '&1%';
commit;
exit